Ext.Loader.setConfig({
  enabled: true,
  paths: {
    DukeSource: "app",
    countries: "resources/js/countries",
    "Ext.ux": "resources/ux"
  }
});

Ext.define("DukeSource.Application", {
  name: "DukeSource",
  extend: "Ext.app.Application",
  controllers: ["DukeSource.controller.main.ControllerTreeMain"],
  stores: [
    "DukeSource.store.TreeStore",
    "DukeSource.store.Names",
    "DukeSource.store.Hidden",
    "DukeSource.store.StyleFields",
    "DukeSource.store.AllowBlank",
    "DukeSource.store.Reports"
  ],
  requires: [
    "DukeSource.global.GiroConstants",
    "DukeSource.global.GiroMessages",
    "DukeSource.global.DirtyView",
    "DukeSource.lib.Ajax",
    "DukeSource.view.override.GridExcel",
    "Ext.ux.grid.Printer"
  ],
  loadController: function(e) {
    if (!Ext.ClassManager.isCreated(e)) {
      var t = this.getController(e);
      t.init();
    }
  },
  launch: function() {
    var self = this;
    Ext.getStore("DukeSource.store.Names").load({
      callback: function(records, operation, success) {
        self.createLayout();
      }
    });
  },
  createLayout: function() {
    this.menuBar = Ext.create("DukeSource.view.main.MenuBar", { height: 50 });
    this.centerPanel = Ext.create("DukeSource.view.main.Main", {
      region: "center"
    });
    Ext.create("Ext.container.Viewport", {
      layout: "fit",
      items: [
        {
          xtype: "panel",
          border: false,
          layout: "border",
          items: [this.centerPanel],
          tbar: this.menuBar
        }
      ]
    });
  }
});
