Ext.define("DukeSource.global.TreeFilter", {
  alias: "TreeFilter",
  filterByText: function(e) {
    this.filterBy(e, "text");
  },
  filterBy: function(e, t) {
    this.clearFilter();
    var n = this.getView(),
      r = this,
      i = [];
    this.getRootNode().cascadeBy(
      function(n, s) {
        var o = this;
        if (
          o &&
          o.data[t] &&
          o.data[t]
            .toString()
            .toLowerCase()
            .indexOf(e.toLowerCase()) > -1
        ) {
          r.expandPath(o.getPath());
          while (o.parentNode) {
            i.push(o.id);
            o = o.parentNode;
          }
        }
      },
      null,
      [r, n]
    );
    this.getRootNode().cascadeBy(
      function(e, t) {
        var n = t.getNodeByRecord(this);
        if (n && !Ext.Array.contains(i, this.id)) {
          Ext.get(n).setDisplayed("none");
        }
      },
      null,
      [r, n]
    );
  },
  clearFilter: function() {
    var e = this.getView();
    this.getRootNode().cascadeBy(
      function(e, t) {
        var n = t.getNodeByRecord(this);
        if (n) {
          Ext.get(n).setDisplayed("table-row");
        }
      },
      null,
      [this, e]
    );
  },
  updateTreeView: function(tree, fn) {
    var view = tree.getView();
    view.getStore().loadRecords(fn(tree.getRootNode()));
    view.refresh();
  },
  collapseAll: function(tree) {
    this.updateTreeView(tree, function(root) {
      root.cascadeBy(function(node) {
        if (!node.isRoot() || tree.rootVisible) {
          node.data.expanded = false;
        }
      });
      return tree.rootVisible ? [root] : root.childNodes;
    });
  },
  expandAll: function(tree) {
    this.updateTreeView(tree, function(root) {
      var nodes = [];
      root.cascadeBy(function(node) {
        if (!node.isRoot() || tree.rootVisible) {
          node.data.expanded = true;
          nodes.push(node);
        }
      });
      return nodes;
    });
  }
});
