Ext.define("DukeSource.global.GiroMessages", {
  singleton: true,
  TITLE_WARNING: "Advertencia",
  TITLE_MESSAGE: "Mensaje",
  TITLE_CONFIRM: "Confirmar",
  EVENT_NOT_APPROVED: "EL evento no est&aacute; aprobado",
  TITLE_ERROR: "Atenci&oacute;n",
  TITLE_AUDIT: "Auditor&iacute;a",
  MESSAGE_EXIT: "Est&aacute; seguro que desea cerrar el espacio de trabajo?",
  MESSAGE_DELETE: "Est&aacute; seguro que desea eliminar?",
  MESSAGE_COMPLETE:
    "Existe error en los campos ingresados, verifique los marcados en rojo",
  MESSAGE_REQUIRE: "Este campo es obligatorio",
  MESSAGE_EMPTY: "El campo no puede estar en blanco, por favor complete",
  MESSAGE_ITEM: "Seleccione por favor un registro",
  MESSAGE_GRID_NUMBER: "No existe informaci&oacute;n a mostrar",
  MESSAGE_GRID_PAGE: "Mostrando {0} - {1} de {2}",
  MESSAGE_ITEM_CHECK: "El ITEM debe estar chekeado",
  MESSAGE_MAX_CHARACTER: "El n&uacute;mero de m&aacute;ximo de caracteres es ",
  MESSAGE_SAVING: "GIRO - TOPRISK Guardando...",
  MESSAGE_LOADING: "GIRO - TOPRISK Cargando...",
  MESSAGE_ADVANCE_VERIFIED:
    "El avance no puede ser modificado, ya fue revisado",
  MESSAGE_ERROR_LOAD: "Error al cargar o no existen los datos",
  MESSAGE_FIELDS_ERROR:
    "Complete por favor los campos obligatorios o corriga su valor",
  MESSAGE_PLAN_REPROGRAM:
    "El plan de acci&oacute;n est&aacute; siendo reprogramado",
  MESSAGE_EVENT_REFERENCE_CIRCLE:
    "No es posible continuar, el evento se encuentra seleccionado (referencia circular)",
  MESSAGE_EVENT_SIMPLE_ADD:
    "No es posible continuar, el evento debe ser de tipo SIMPLE",
  MESSAGE_EVENT_IS_SUB_EVENT:
    "No es posible continuar, el registro seleccionado ya es un SUB-EVENTO",
  MESSAGE_KRI_EXIST_LIMIT:
    "Existen evaluaciones realizadas con los umbrales actuales.<br>Haga clic en <b>NUEVOS UMBRALES</b> para registrar umbrales nuevos.",
  MESSAGE_KRI_NEW_LIMIT:
    "Se limpiar&aacute;n los umbrales para su nuevo registro, desea continuar?",
  MESSAGE_KRI_INCOMPLETE:
    "La configuraci&oacute;n del kri est&aacute; incompleta.<br> Culmine la configuraci&oacute;n para registrar la evaluaci&oacute;n",
  MESSAGE_KRI_NOT_FIND: "KRI no encontrado, seleccione un KRI",
  MESSAGE_KRI_EVALUATION_EXIST:
    "El KRI tiene evaluaciones registradas, desea continuar elimin&aacute;ndolas?",
  MESSAGE_RISK_DELETE_INVALID:
    "El riesgo debe estar en estado inv&aacute;lido para ser eliminado"
});
