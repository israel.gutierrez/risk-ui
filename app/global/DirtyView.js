Ext.define("DukeSource.global.DirtyView", {
  statics: {
    selectToComboBox: function(e, t, n, r, i) {
      t.store.getProxy().extraParams = {
        propertyFind: r,
        valueFind: e.getValue(),
        propertyOrder: i
      };
      t.store.getProxy().url = n;
      t.down("pagingtoolbar").moveFirst();
    },
    searchPaginationGridToEnter: function(e, t, n, r, i, s) {
      t.store.getProxy().extraParams = {
        propertyFind: i,
        valueFind: e.getValue(),
        propertyOrder: s
      };
      t.store.getProxy().url = r;
      n.moveFirst();
    },

    searchPaginationGridNormal: function(e, t, n, r, i, s) {
      t.store.getProxy().extraParams = {
        propertyFind: i,
        valueFind: e,
        propertyOrder: s
      };
      t.store.getProxy().url = r;
      n.moveFirst();
    },
    loadGridDefault: function(e) {
      e.getStore().loadData([], false);
    },
    deleteElementToGrid: function(e, t) {
      var row = e.getSelectionModel().getSelection()[0];
      if (row === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        Ext.MessageBox.show({
          title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
          msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
          icon: Ext.Msg.WARNING,
          buttonText: { yes: "Si" },
          buttons: Ext.MessageBox.YESNO,
          fn: function(n) {
            if (n === "yes") {
              Ext.Ajax.request({
                method: "POST",
                url: t,
                params: { jsonData: Ext.JSON.encode(row.data) },
                success: function(t) {
                  t = Ext.decode(t.responseText);
                  if (t.success) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      t.message,
                      Ext.Msg.INFO
                    );
                    e.getStore().load();
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      t.message,
                      Ext.Msg.WARNING
                    );
                  }
                },
                failure: function() {}
              });
            }
          }
        });
      }
    },
    serchGridToEnter: function(e, t, n, r, i) {
      t.getStore().load({
        url: n,
        params: { propertyFind: r, valueFind: e.getValue(), propertyOrder: i }
      });
    },
    getSelectedRowIndex: function(e) {
      var t = e.getSelectionModel().getSelection();
      var n = e.getStore();
      return n.indexOf(t[0]);
    },
    requestFieldByAjax: function(e, t, n, r, i) {
      Ext.Ajax.request({
        method: "POST",
        url: t,
        params: { valueFind: e.getValue() },
        success: function(e) {
          e = Ext.decode(e.responseText);
          if (e.success) {
            n.getForm().setValues(e.data);
            r.setText(e.data[i]);
          } else {
            r.setText(e.mensaje);
          }
        },
        failure: function(e) {}
      });
    },
    existUserByUserName: function(e, t, n, r) {
      Ext.Ajax.request({
        method: "POST",
        url: t,
        params: { userName: e.getValue(), propertyName: e.name },
        scope: this,
        success: function(e) {
          e = Ext.decode(e.responseText);
          if (e.success) {
            this.messageToFocus(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              e.mensaje,
              Ext.Msg.INFO,
              r
            );
          } else {
            this.messageResetFieldFocus(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              e.mensaje,
              Ext.Msg.WARNING,
              n
            );
          }
        },
        failure: function(e) {}
      });
    },
    existUserByDni: function(e, t, n, r) {
      Ext.Ajax.request({
        method: "POST",
        url: t,
        params: { documentNumber: e.getValue() },
        scope: this,
        success: function(e) {
          e = Ext.decode(e.responseText);
          if (e.success) {
            this.messageToFocus(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              e.mensaje,
              Ext.Msg.INFO,
              r
            );
          } else {
            this.messageResetFieldFocus(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              e.mensaje,
              Ext.Msg.INFO,
              n
            );
          }
        },
        failure: function(e) {}
      });
    },
    saveDataToForm: function(e, t, n) {
      if (
        e
          .down("form")
          .getForm()
          .isValid()
      ) {
        Ext.Ajax.request({
          waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
          method: "POST",
          url: t,
          params: { jsonData: Ext.JSON.encode(e.down("form").getValues()) },
          scope: this,
          success: n,
          failure: function() {}
        });
      } else {
        this.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    focusEventEnterObligatory: function(e, t, n) {
      if (e.getValue() == "") {
        e.focus(false, 100);
        e.emptyText = "ESTE CAMPO ES OBLIGATORIO";
        e.applyEmptyText();
      } else {
        if (t.getKey() == t.ENTER || t.getKey() == t.TAB) {
          n.focus(false, 100);
        } else {
        }
      }
    },
    focusEventEnter: function(e, t, n) {
      if (t.getKey() == t.ENTER || t.getKey() == t.TAB) {
        n.focus(false, 100);
      } else {
      }
    },
    focusEvent: function(e) {
      e.focus(false, 100);
    },
    showWindowAuditory: function(e, t) {
      var r = e.getSelectionModel().getSelection()[0];
      if (r === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        Ext.Ajax.request({
          method: "POST",
          url: t,
          params: { jsonData: Ext.JSON.encode(r.data) },
          success: function(e) {
            e = Ext.decode(e.responseText);
            if (e.success) {
              Ext.create("DukeSource.view.risk.util.WindowAuditory", {
                modal: true
              }).show();
              var t = Ext.ComponentQuery.query("WindowAuditory form")[0];
              t.getForm().setValues(e.data);
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                e.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      }
    },
    showAuditory: function(e, t) {
      var r = e.getSelectionModel().getSelection()[0];
      if (r === undefined) {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
      } else {
        Ext.Ajax.request({
          method: "POST",
          url: t,
          params: {
            id: r.data.id
          },
          success: function(e) {
            e = Ext.decode(e.responseText);
            if (e.success) {
              Ext.create("DukeSource.view.risk.util.WindowAuditory", {
                modal: true
              }).show();
              var t = Ext.ComponentQuery.query("WindowAuditory form")[0];
              t.getForm().setValues(e.data);
            } else {
              DukeSource.global.DirtyView.messageWarning(e.message);
            }
          },
          failure: function() {}
        });
      }
    },
    generateReportXLSPDF: function(e, t) {
      var n = new Ext.Window({
        modal: true,
        width: 900,
        height: 600,
        layout: "fit",
        items: [{ xtype: "component", autoEl: { tag: "iframe", src: e + t } }]
      }).show();
      n.on("minimize", function(e) {
        e.collapse();
      });
    },
    messageToFocus: function(e, t, n, r) {
      Ext.MessageBox.show({
        title: e,
        msg: t,
        icon: n,
        buttons: Ext.MessageBox.OK,
        closable: false,
        fn: function(e) {
          if (e == "ok") {
            r.focus(false, 100);
          }
        }
      });
    },
    messageResetFieldFocus: function(e, t, n, r) {
      Ext.MessageBox.show({
        title: e,
        msg: t,
        icon: n,
        buttons: Ext.MessageBox.OK,
        closable: false,
        fn: function(e) {
          if (e == "ok") {
            r.focus(false, 100);
            r.reset();
          }
        }
      });
    },
    warningAlert: function(t) {
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_WARNING,
        msg: t,
        animEl: "elId",
        icon: Ext.Msg.WARNING,
        buttons: Ext.MessageBox.OK
      });
    },
    messageAlert: function(e, t, n) {
      Ext.MessageBox.show({
        title: e,
        msg: t,
        animEl: "elId",
        icon: n,
        buttons: Ext.MessageBox.OK
      });
    },
    messageNormal: function(t) {
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_MESSAGE,
        msg: t,
        animEl: "elId",
        icon: Ext.Msg.INFO,
        buttons: Ext.MessageBox.OK
      });
    },
    messageWarning: function(t) {
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_WARNING,
        msg: t,
        animEl: "elId",
        icon: Ext.Msg.WARNING,
        buttons: Ext.MessageBox.OK
      });
    },

    messageOut: function(e) {
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_WARNING,
        msg: DukeSource.global.GiroMessages.MESSAGE_EXIT,
        icon: Ext.Msg.QUESTION,
        buttonText: { yes: "Si" },
        buttons: Ext.MessageBox.YESNO,
        fn: function(t) {
          if (t === "yes") {
            e.doClose();
          }
        }
      });
    },
    YesNoWarning: function(t) {
      var a = Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_WARNING,
        msg: t,
        icon: Ext.Msg.WARNING,
        buttonText: { yes: "Si" },
        buttons: Ext.MessageBox.YESNO,
        fn: function(t) {
          if (t === "yes") {
            a.close();
          }
        }
      });
    },
    verifyLoadController: function(e) {
      if (Ext.ClassManager.isCreated(e)) {
      } else {
        DukeSource.getApplication().loadController(e);
      }
    },
    toReadOnly: function(e) {
      e.setFieldStyle(
        "background-color: #D8D8D8; background-image: none; cursor:default;"
      );
      e.setReadOnly(true);
    },
    changeObligatoryElement: function(e) {
      e.setFieldStyle("background-color: #d9ffdb; background-image: none;");
      e.setReadOnly(false);
    },

    colorToElement: function(e, t) {
      e.setFieldStyle(
        "background-color: #" + t + "; background-image: none; color:#000000;"
      );
    },
    printElementTogrid: function(e) {
      Ext.ux.grid.Printer.printAutomatically = false;
      Ext.ux.grid.Printer.print(e);
    },
    createMatrix: function(e, t, n) {
      var r = n.getStore();
      for (var i = 0; i < e.length; i++) {
        if (i === 0) {
          e[i]["renderer"] = function(e, t, n) {
            return RISK_AXIS_Y === "frequency"
              ? n.get("cell1").descriptionFrequency
              : n.get("cell1").descriptionImpact;
          };
        } else {
          e[i]["renderer"] = function(e, t, n, r, i, s, o) {
            t.tdAttr =
              'style="background-color: #' +
              n.get(DirtyView.getDataIndex(o, i)).colour +
              '!important;height:60px;"';
            return Ext.util.Format.number(
              n.get(DirtyView.getDataIndex(o, i)).valueCell,
              "0,0.00"
            );
          };
        }
      }
      r.model.setFields(t);
      n.reconfigure(r, e);
      r.removeAll();
    },
    getDataIndex: function(e, t) {
      return e.getHeaderAtIndex(t).dataIndex;
    },
    downloadFileAttachment: function(e, t, n, r) {
      for (var i = 0; i < r.length; i++) {
        var s = e.store.getAt(t).get(r[i]);
        if (i == 0) {
          n = n + "?" + r[i] + "=" + s;
        } else {
          n = n + "&" + r[i] + "=" + s;
        }
      }
      Ext.core.DomHelper.append(document.body, {
        tag: "iframe",
        id: "downloadIframe",
        frameBorder: 0,
        width: 0,
        height: 0,
        css: "x-hidden",
        src: n
      }).show();
    },
    createGroup: function(finalField, response, i) {
      finalField.description = response.data[i].abbreviation;
      finalField.labelText = response.data[i].groupControlDescription;
      finalField.idHidden = "idFinalScore" + response.data[i].abbreviation;
      finalField.idHiddenValue =
        response.data[i].idFinalScore === undefined
          ? "id"
          : response.data[i].idFinalScore;
      finalField.idItemValue = "value" + response.data[i].abbreviation;
      finalField.idItemValueValue =
        response.data[i].effectiveValue === undefined
          ? ""
          : response.data[i].effectiveValue;
      finalField.idItemGroup = "percent" + response.data[i].abbreviation;
      finalField.idItemGroupValue =
        response.data[i].effectivePercent === undefined
          ? ""
          : response.data[i].effectivePercent;
      finalField.idComboScore = "score" + response.data[i].abbreviation;
      finalField.idComboScoreValue = response.data[i].scoreReduction;
      finalField.comboScoreColor =
        response.data[i].scoreReductionColor === undefined
          ? "FFF"
          : response.data[i].scoreReductionColor;
      finalField.comboScoreColor =
        response.data[i].scoreReductionColor === undefined
          ? "FFF"
          : response.data[i].scoreReductionColor;
      finalField.percentage = response.data[i].percentageGroup;
      finalField.totalItems = response.data.length;
      finalField.icon = "maintainance";
    },
    createScoreFinal: function(response) {
      var finalField = {};
      finalField.labelText = "SCORE FINAL";
      finalField.idItemValue = "valueScoreControl";
      finalField.idItemValueValue =
        response.data[0].scoreControl === undefined
          ? ""
          : response.data[0].scoreControl;
      finalField.idItemGroup = "percentScoreControl";
      finalField.idItemGroupValue =
        response.data[0].valueControl === undefined
          ? ""
          : response.data[0].valueControl;
      finalField.idComboScore = "scoreControl";
      finalField.idComboScoreValue = response.data[0].idScoreControl;
      finalField.comboScoreColor =
        response.data[0].colorScoreControl === undefined
          ? "FFF"
          : response.data[0].colorScoreControl;
      finalField.icon = "operations";
      return finalField;
    },
    createButton: function(windows) {
      var win = windows.down("#containerResult");
      win.add({
        xtype: "button",
        text: "RIESGOS",
        hidden: true,
        iconCls: "risk",
        handler: function() {
          var viewRisk = Ext.create(
            "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowControleModifyRisk",
            {
              title: "RIESGOS MITIGADOS",
              modal: true,
              buttons: [
                {
                  text: "SALIR",
                  scope: this,
                  handler: function() {
                    viewRisk.close();
                  },
                  iconCls: "logout"
                }
              ]
            }
          ).show();
          var gridDetailControl = viewRisk.down("grid");
          gridDetailControl.store.getProxy().extraParams = {
            idControl: windows.down("#idControl").getValue()
          };
          gridDetailControl.store.getProxy().url =
            "http://localhost:9000/giro/showListRiskAffectedByControl.htm";
          gridDetailControl.down("pagingtoolbar").moveFirst();
        }
      });
    },
    createItemsToFinalScore: function(windows, items) {
      var win = windows.down("#containerResult");
      win.add({
        xtype: "container",
        itemId: "container" + items.description,
        layout: {
          type: "vbox",
          align: "stretch"
        },
        flex: 1,
        items: [
          {
            xtype: "container",
            height: 26,
            itemId: "first",
            layout: {
              type: "hbox"
            },
            flex: 1,
            items: [
              {
                xtype: "textfield",
                name: items.idHidden,
                nameIntern: "idFinalScore",
                value: items.idHiddenValue,
                hidden: true
              },
              {
                xtype: "textfield",
                nameIntern: "description",
                itemId: "activityControl" + items.description,
                name: "activityControl" + items.description,
                hidden: true
              },
              {
                xtype:"UpperCaseTextFieldReadOnly",
                value: items.idItemGroupValue,
                itemId: items.idItemGroup,
                name: items.idItemGroup,
                flex: 1,
                labelWidth: 95,
                nameIntern: "effectivePercent",
                allowBlank: false,
                fieldLabel: items.labelText
              },
              {
                xtype: "button",
                iconCls: items.icon,
                hidden: true,
                handler: function() {
                  var win = Ext.create(
                    "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowControlActivity",
                    {
                      title: "Actividades de ejecución",
                      modal: true,
                      buttons: [
                        {
                          text: "Guardar",
                          handler: function() {
                            windows
                              .down("#activityControl" + items.description)
                              .setValue(win.down("htmleditor").getValue());
                            win.close();
                          },
                          iconCls: "save"
                        },
                        {
                          text: "Salir",
                          scope: this,
                          handler: function() {
                            win.close();
                          },
                          iconCls: "logout"
                        }
                      ]
                    }
                  ).show();
                  win
                    .down("htmleditor")
                    .setValue(
                      windows
                        .down("#activityControl" + items.description)
                        .getValue()
                    );
                }
              }
            ]
          },
          {
            xtype: "container",
            itemId: "second",
            height: 26,
            layout: {
              type: "hbox"
            },
            flex: 1,
            items: [
              {
                xtype:"UpperCaseTextFieldReadOnly",
                itemId: items.idItemValue,
                flex: 2,
                allowBlank: false,
                labelWidth: 95,
                nameIntern: "effectiveValue",
                fieldLabel: "Rating " + items.labelText,
                value: items.idItemValueValue,
                name: items.idItemValue
              },
              {
                xtype: "ViewComboScoreControl",
                itemId: items.idComboScore,
                fieldStyle:
                  "background-color: #" +
                  items.comboScoreColor +
                  "; background-image: none; color:#000000;",
                readOnly: true,
                flex: 1.2,
                hidden: items.labelText !== "SCORE FINAL",
                allowBlank: false,
                name: items.idComboScore,
                nameIntern: "scoreReduction",
                percentage: items.percentage,
                totalItems: items.totalItems,
                listeners: {
                  render: function(cbo) {
                    cbo.store.load({
                      url:
                        "http://localhost:9000/giro/showListScoreControlActivesComboBox.htm",
                      params: {
                        propertyFind: "typeControl",
                        valueFind: items.typeControl,
                        propertyOrder: "description"
                      },
                      callback: function() {
                        if (items.idComboScoreValue !== undefined)
                          cbo.setValue(items.idComboScoreValue);
                      }
                    });
                  }
                }
              }
            ]
          }
        ]
      });
    },
    findAdvancedUserGeneric: function(form, grid) {
      if (form.getForm().isValid()) {
        var limit = ",";
        var codeUser =
          form.down("#idUser").getRawValue() === undefined
            ? ""
            : form.down("#idUser").getRawValue();
        var doiUser =
          form.down("#doiUser").getValue() === undefined
            ? ""
            : form.down("#doiUser").getValue();
        var userName =
          form.down("#nameUser").getValue() === undefined
            ? ""
            : form.down("#nameUser").getValue();
        var agency =
          form.down("#comboAgency").getValue() === undefined
            ? ""
            : form.down("#comboAgency").getValue();
        var area =
          form.down("#comboWorkArea").getValue() === undefined
            ? ""
            : form.down("#comboWorkArea").getValue();
        var jobPlace =
          form.down("#comboJobPlace").getValue() === undefined
            ? ""
            : form.down("#comboJobPlace").getValue();
        var category =
          form.down("#comboCategory").getValue() === undefined
            ? ""
            : form.down("#comboCategory").getValue();
        var userState =
          form.down("#comboStateUser").getValue() === undefined
            ? ""
            : form.down("#comboStateUser").getValue();
        if (userName !== undefined) {
          userName = "%" + userName + "%";
        }
        grid.store.getProxy().extraParams = {
          fields:
            "usu.username" +
            limit +
            "usu.numberDocument" +
            limit +
            "usu.fullName" +
            limit +
            "usu.agency.id" +
            limit +
            "usu.workArea.id" +
            limit +
            "usu.jobPlace.id" +
            limit +
            "usu.jobPlace.category" +
            limit +
            "usu.state",
          values:
            codeUser +
            limit +
            doiUser +
            limit +
            userName +
            limit +
            agency +
            limit +
            area +
            limit +
            jobPlace +
            limit +
            category +
            limit +
            userState,
          types:
            "String" +
            limit +
            "String" +
            limit +
            "String" +
            limit +
            "Integer" +
            limit +
            "Integer" +
            limit +
            "Integer" +
            limit +
            "String" +
            limit +
            "String",
          operators:
            "equal" +
            limit +
            "equal" +
            limit +
            "like" +
            limit +
            "equal" +
            limit +
            "equal" +
            limit +
            "equal" +
            limit +
            "like" +
            limit +
            "equal",
          searchIn: "User"
        };
        grid.store.getProxy().url =
          "http://localhost:9000/giro/advancedSearchUser.htm";
        grid.down("pagingtoolbar").moveFirst();
      } else {
        this.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.WARNING
        );
      }
    },
    createWindowSearch: function(grid, deleteAction, auditAction) {
      var record = grid.getSelectionModel().getSelection()[0];

      if (record === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        var win = Ext.create(
          "DukeSource.view.risk.User.windows.WindowUserAssigned",
          {
            modal: true,
            record: record,
            deleteAction: deleteAction,
            auditAction: auditAction
          }
        );
        win.show();
      }
      return win;
    }
  }
});
