Ext.define('CalendarsExtensible', {
    constructor: function () {
        return {
            "calendars": [{
                "id": 1,
                "title": "Visitas",
                "color": 2
            }, {
                "id": 2,
                "title": "Capacitaciones",
                "color": 22
            }, {
                "id": 3,
                "title": "Evaluaciones",
                "color": 7
            }, {
                "id": 4,
                "title": "Auditoria",
                "color": 26
            }]
        };
    }
});

Ext.define('EventsCalendar', {
    constructor: function () {
        var today = Ext.Date.clearTime(new Date),
            makeDate = function (d, h, m, s) {
                d = d * 86400;
                h = (h || 0) * 3600;
                m = (m || 0) * 60;
                s = (s || 0);
                return Ext.Date.add(today, Ext.Date.SECOND, d + h + m + s);
            };

        return {
            "evts": [{
                "id": 1001,
                "cid": 1,
                "title": "Visita agencia Lima",
                "start": makeDate(-20, 10),
                "end": makeDate(-10, 15),
                "notes": "Have fun"
            }, {
                "id": 1002,
                "cid": 2,
                "title": "Capacitación en eventos de pérdida",
                "start": makeDate(0, 11, 30),
                "end": makeDate(0, 13),
                "loc": "Chuy's!",
                "url": "http : //tesla-t.com",
                "notes": "GIRO",
                "rem": "15"
            }, {
                "id": 1003,
                "cid": 3,
                "title": "Project Deal-Tesla",
                "start": makeDate(0, 15),
                "end": makeDate(0, 15)
            }, {
                "id": 1004,
                "cid": 1,
                "title": "Revisión de visitas",
                "start": today,
                "end": today,
                "notes": "Need to get a gift",
                "ad": true
            }, {
                "id": 1005,
                "cid": 2,
                "title": "Capacitación eventos de pérdida",
                "start": makeDate(-12),
                "end": makeDate(10, 0, 0, -1),
                "ad": true
            }, {
                "id": 1006,
                "cid": 3,
                "title": "Visita área de canales eletrónicos",
                "start": makeDate(5),
                "end": makeDate(7, 0, 0, -1),
                "ad": true,
                "rem": "2880"
            }, {
                "id": 1007,
                "cid": 1,
                "title": "Reunión equipo Tesla",
                "start": makeDate(0, 9),
                "end": makeDate(0, 9, 30),
                "notes": "Get cash on the way"
            }, {
                "id": 1008,
                "cid": 3,
                "title": "Envio de propuesta Falabella",
                "start": makeDate(-30),
                "end": makeDate(-28),
                "ad": true
            }, {
                "id": 1009,
                "cid": 2,
                "title": "Exposición GIRO",
                "start": makeDate(-2, 13),
                "end": makeDate(-2, 18),
                "loc": "ABC Inc.",
                "rem": "60"
            }, {
                "id": 1010,
                "cid": 3,
                "title": "Visita área de logística",
                "start": makeDate(-2),
                "end": makeDate(3, 0, 0, -1),
                "ad": true
            }, {
                "id": 1011,
                "cid": 1,
                "title": "Reunión Team Tesla",
                "start": makeDate(2, 19),
                "end": makeDate(2, 23),
                "notes": "Don't forget the buy tablets to Tesla Team",
                "rem": "60"
            }, {
                "id": 1012,
                "cid": 4,
                "title": "Propuesta auditoría",
                "start": makeDate(8, 8),
                "end": makeDate(10, 17)
            }, {
                "id": 1013,
                "cid": 4,
                "title": "Reunión Felix",
                "start": makeDate(5, 10),
                "end": makeDate(5, 12)
            }]
        }
    }
});

var calendarStore = Ext.create('Extensible.calendar.data.MemoryCalendarStore', {
    data: Ext.create('CalendarsExtensible')
});

var eventStore = Ext.create('Extensible.calendar.data.MemoryEventStore', {
    data: Ext.create('EventsCalendar'),
    autoMsg: false
});

Ext.define('DukeSource.view.calendar.OperationalCalendarPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.OperationalCalendarPanel',
    layout: 'border',
    //renderTo: 'calendar-ct',
    initComponent: function () {
        var me = this;
        this.items = [
            {
                id: 'app-west',
                region: 'west',
                width: 179,
                style: {
                    borderRight: '1px solid #99BCE8'
                },
                border: false,
                items: [
                    {
                        xtype: 'datepicker',
                        id: 'app-nav-picker',
                        cls: 'ext-cal-nav-picker',
                        listeners: {
                            'select': {
                                fn: function (dp, dt) {
                                    Ext.getCmp('app-calendar').setStartDate(dt);
                                },
                                scope: this
                            }
                        }
                    },
                    {
                        xtype: 'extensible.calendarlist',
                        store: calendarStore,
                        border: false,
                        width: 178
                    }
                ]
            },
            {
                xtype: 'extensible.calendarpanel',
                eventStore: eventStore,
                calendarStore: calendarStore,
                border: false,
                id: 'app-calendar',
                region: 'center',
                title: 'CALENDARIO-GIRO',
                titleAlign: 'center',
                activeItem: 3, // month view

                // Any generic view options that should be applied to all sub views:
                viewConfig: {
                    //enableFx: false,
                    //ddIncrement: 10, //only applies to DayView and subclasses, but convenient to put it here
                    //viewStartHour: 6,
                    //viewEndHour: 18,
                    //minEventDisplayMinutes: 15
                    showTime: false
                },

                // View options specific to a certain view (if the same options exist in viewConfig
                // they will be overridden by the view-specific config):
                monthViewCfg: {
                    showHeader: true,
                    showWeekLinks: true,
                    showWeekNumbers: true
                },

                multiWeekViewCfg: {
                    //weekCount: 3
                },

                // Some optional CalendarPanel configs to experiment with:
                //readOnly: true,
                //showDayView: false,
                //showMultiDayView: true,
                //showWeekView: false,
                //showMultiWeekView: false,
                //showMonthView: false,
                //showNavBar: false,
                //showTodayText: false,
                //showTime: false,
                //editModal: true,
                //enableEditDetails: false,
                //title: 'My Calendar', // the header of the calendar, could be a subtitle for the app

                listeners: {
                    'eventclick': {
                        fn: function (vw, rec, el) {
                            me.clearMsg();
                        },
                        scope: this
                    },
                    'eventover': function (vw, rec, el) {
                        //console.log('Entered evt rec='+rec.data[Extensible.calendar.data.EventMappings.Title.name]', view='+ vw.id +',
                        // el='+el.id);
                    },
                    'eventout': function (vw, rec, el) {
                        //console.log('Leaving evt rec='+rec.data[Extensible.calendar.data.EventMappings.Title.name]+', view='+ vw.id +',
                        // el='+el.id);
                    },
                    'eventadd': {
                        fn: function (cp, rec) {
                            me.showMsg('Event ' + rec.data[Extensible.calendar.data.EventMappings.Title.name] + ' was added');
                        },
                        scope: this
                    },
                    'eventupdate': {
                        fn: function (cp, rec) {
                            me.showMsg('Event ' + rec.data[Extensible.calendar.data.EventMappings.Title.name] + ' was updated');
                        },
                        scope: this
                    },
                    'eventdelete': {
                        fn: function (cp, rec) {
                            me.showMsg('Event ' + rec.data[Extensible.calendar.data.EventMappings.Title.name] + ' was deleted');
                        },
                        scope: this
                    },
                    'eventcancel': {
                        fn: function (cp, rec) {
                            // edit canceled
                        },
                        scope: this
                    },
                    'viewchange': {
                        fn: function (p, vw, dateInfo) {
                            if (dateInfo) {
                                me.updateTitle(dateInfo.viewStart, dateInfo.viewEnd);
                            }
                        },
                        scope: this
                    },
                    'dayclick': {
                        fn: function (vw, dt, ad, el) {
                            me.clearMsg();
                        },
                        scope: this
                    },
                    'rangeselect': {
                        fn: function (vw, dates, onComplete) {
                            me.clearMsg();
                        },
                        scope: this
                    },
                    'eventmove': {
                        fn: function (vw, rec) {
                            var mappings = Extensible.calendar.data.EventMappings,
                                time = rec.data[mappings.IsAllDay.name] ? '' : ' \\a\\t g:i a';

                            rec.commit();

                            me.showMsg('Event ' + rec.data[mappings.Title.name] + ' was moved to ' +
                                Ext.Date.format(rec.data[mappings.StartDate.name], ('F jS' + time)));
                        },
                        scope: this
                    },
                    'eventresize': {
                        fn: function (vw, rec) {
                            rec.commit();
                            me.showMsg('Event ' + rec.data[Extensible.calendar.data.EventMappings.Title.name] + ' was updated');
                        },
                        scope: this
                    },
                    'eventdelete': {
                        fn: function (win, rec) {
                            eventStore.remove(rec);
                            me.showMsg('Event ' + rec.data[Extensible.calendar.data.EventMappings.Title.name] + ' was deleted');
                        },
                        scope: this
                    },
                    'initdrag': {
                        fn: function (vw) {
                            // do something when drag starts
                        },
                        scope: this
                    }
                }
            }
        ];
        this.callParent();
    },

    updateTitle: function (startDt, endDt) {
        var p = Ext.getCmp('app-calendar'),
            fmt = Ext.Date.format;

        if (Ext.Date.clearTime(startDt).getTime() == Ext.Date.clearTime(endDt).getTime()) {
            p.setTitle(fmt(startDt, 'F j, Y'));
        }
        else if (startDt.getFullYear() == endDt.getFullYear()) {
            if (startDt.getMonth() == endDt.getMonth()) {
                p.setTitle(fmt(startDt, 'F j') + ' - ' + fmt(endDt, 'j, Y'));
            }
            else {
                p.setTitle(fmt(startDt, 'F j') + ' - ' + fmt(endDt, 'F j, Y'));
            }
        }
        else {
            p.setTitle(fmt(startDt, 'F j, Y') + ' - ' + fmt(endDt, 'F j, Y'));
        }
    },

    showMsg: function (msg) {
        //Ext.getCmp('app-msg').update(msg).removeCls('x-hidden');
    },

    clearMsg: function () {
        //Ext.getCmp('app-msg').update('').addCls('x-hidden');
    }

});
