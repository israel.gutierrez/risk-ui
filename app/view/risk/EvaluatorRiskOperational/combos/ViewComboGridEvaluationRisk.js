Ext.define("ModelComboGridEvaluationRisk", {
  extend: "Ext.data.Model",
  fields: [
    "idVersionEvaluation",
    "idRisk",
    "managerRisk",
    "fullName",
    "description",
    "nameEvaluation",
    "dateEvaluation",
    "state",
    "stateCheck",
    "idMaxExposition",
    "maxExposition",
    "typeMatrix",
    "descriptionTypeMatrix",
    "validity"
  ]
});
var StoreComboGridEvaluationRisk = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboGridEvaluationRisk",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showVersionEvaluationActives.htm",
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.combos.ViewComboGridEvaluationRisk",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboGridEvaluationRisk",
    queryMode: "local",
    displayField: "description",
    valueField: "idVersionEvaluation",

    editable: false,
    forceSelection: true,
    store: StoreComboGridEvaluationRisk,
    tpl: Ext.create(
      "Ext.XTemplate",
      '<tpl for=".">',
      '<div class="x-boundlist-item">{state} | {maxExposition} | {dateEvaluation} | {description}</div>',
      "</tpl>"
    ),

    displayTpl: Ext.create(
      "Ext.XTemplate",
      '<tpl for=".">',
      "{state} | {maxExposition} | {dateEvaluation} | {description}",
      "</tpl>"
    )
  }
);
