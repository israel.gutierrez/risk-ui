Ext.define("ModelGridPreviousIdentifyRiskOperational", {
  extend: "Ext.data.Model",
  fields: [
    "idRiskEvaluationMatrix",
    "typeRiskEvaluation",
    "unity",
    "analyst",
    "manager",
    "nameEvaluation",
    {
      name: "dateEvaluation",
      type: "date",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y");
      }
    },
    "nameTypeRiskEvaluation",
    "nameAnalyst",
    "nameManager",
    "descriptionUnity",
    "agency",
    "indicatorAttachment",
    {
      name: "numberRisk",
      type: "int"
    }
  ]
});

var StoreGridPreviousIdentifyRiskOperational = Ext.create("Ext.data.Store", {
  model: "ModelGridPreviousIdentifyRiskOperational",
  groupField: "nameTypeRiskEvaluation",
  autoLoad: false,
  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelPreviousIdentifyRiskOperational",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelPreviousIdentifyRiskOperational",
    itemId: "ViewPanelPreviousIdentifyRiskOperational",
    layout: "fit",
    border: false,
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            itemId: "previousRiskGrid",
            flex: 2,
            title: "Ambitos de evaluación",
            titleAlign: "center",
            store: StoreGridPreviousIdentifyRiskOperational,
            features: [
              {
                ftype: "grouping",
                groupHeaderTpl: "Evaluación : {name}"
              }
            ],
            loadMask: true,
            columnLines: true,
            padding: "2 2 2 2",
            tbar: [
              {
                text: "Nuevo",
                iconCls: "add",
                scale: "medium",
                cls: "my-btn",
                overCls: "my-over",
                handler: function(button) {
                  Ext.create(
                    "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowPreviousRisk",
                    {
                      modal: true
                    }
                  ).show();
                }
              },
              "-",
              {
                text: "Modificar",
                iconCls: "modify",
                scale: "medium",
                cls: "my-btn",
                overCls: "my-over",
                handler: function() {
                  var gridRisk = me.down("grid");
                  if (gridRisk.getSelectionModel().getCount() === 0) {
                    DukeSource.global.DirtyView.messageWarning(
                      DukeSource.global.GiroMessages.MESSAGE_ITEM
                    );
                  } else {
                    var win = Ext.create(
                      "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowPreviousRisk",
                      {
                        modal: true
                      }
                    );
                    win
                      .down("#agency")
                      .getStore()
                      .load();
                    win
                      .down("#typeRiskEvaluation")
                      .getStore()
                      .load();
                    win
                      .down("form")
                      .getForm()
                      .loadRecord(
                        gridRisk.getSelectionModel().getSelection()[0]
                      );
                    win.show();
                  }
                }
              },
              "-",
              {
                text: "Eliminar",
                iconCls: "delete",
                scale: "medium",
                cls: "my-btn",
                overCls: "my-over",
                action: "deletePreviousRiskOperational"
              },
              "-",
              {
                text: "Ver riesgos",
                iconCls: "ir",
                scale: "medium",
                cls: "my-btn",
                overCls: "my-over",
                action: "riskPreviousRiskOperational"
              },
              {
                xtype: "combobox",
                anchor: "100%",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                name: "typeViewRisk",
                itemId: "typeViewRisk",
                msgTarget: "side",
                fieldLabel: "Ver riesgos por",
                editable: false,
                queryMode: "local",
                displayField: "description",
                valueField: "value",
                store: {
                  fields: ["value", "description"],
                  pageSize: 9999,
                  autoLoad: true,
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url:
                      "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                    extraParams: {
                      propertyOrder: "description",
                      propertyFind: "identified",
                      valueFind: "RISK_TYPE_VIEW"
                    },
                    reader: {
                      type: "json",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                },
                listeners: {
                  select: function(cbo) {
                    me = Ext.ComponentQuery.query(
                      "ViewPanelPreviousIdentifyRiskOperational"
                    )[0];

                    var namePanel;

                    if (cbo.getValue() === "1") {
                      me.destroy();
                      namePanel =
                        "DukeSource.view.risk.EvaluatorRiskOperational.PanelProcessRiskEvaluation";
                    } else if (cbo.getValue() === "2") {
                      me.destroy();
                      namePanel =
                        "DukeSource.view.risk.EvaluatorRiskOperational.PanelProductRiskEvaluation";
                    } else if (cbo.getValue() === "3") {
                      me.destroy();
                      namePanel =
                        "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelPreviousIdentifyRiskOperational";
                    }

                    var panel = Ext.create(namePanel, {
                      title: "EVRO",
                      closeAction: "destroy",
                      closable: true
                    });
                    DukeSource.getApplication().centerPanel.addPanel(panel);
                  },
                  afterrender: function(cbo) {
                    cbo.setValue("3");
                  }
                }
              },
              "->",
              {
                text: "Auditoría",
                iconCls: "auditory",
                scale: "medium",
                cls: "my-btn",
                overCls: "my-over",
                action: "auditoryPreviousRiskOperational"
              }
            ],
            columns: [
              { xtype: "rownumberer", width: 25, sortable: false },
              {
                header: "Nombre de evaluaci&oacute;n",
                dataIndex: "nameEvaluation",
                flex: 1.5,
                renderer: function(value, metaData, record) {
                  var type = "";
                  if (record.get("indicatorAttachment") === "S") {
                    return (
                      '<div style="display:inline-block;"><i class="tesla even-attachment"></i></div><div style="display:inline-block;position:absolute;">' +
                      "<span>" +
                      value +
                      type +
                      "</span> " +
                      "</div>"
                    );
                  } else {
                    return (
                      '<div style="display:inline-block;"></div><div style="display:inline-block;position:absolute;">' +
                      "<span>" +
                      value +
                      type +
                      "</span> " +
                      "</div>"
                    );
                  }
                }
              },
              {
                header: "Número de riesgos",
                dataIndex: "numberRisk",
                width: 80,
                align: "center"
              },
              {
                header: "Gerencia",
                dataIndex: "descriptionUnity",
                width: 320,
                tdCls: "process-columnFree",
                renderer: function(value) {
                  var s = value.split(" &#8702; ");
                  var path = "";
                  for (var i = 0; i < s.length; i++) {
                    var text = s[i];
                    text = s[i] + "<br>";
                    path = path + " &#8702; " + text;
                  }
                  return path;
                }
              },
              {
                header: "Analista",
                dataIndex: "nameAnalyst",
                flex: 1
              },
              {
                header: "Fecha",
                align: "center",
                dataIndex: "dateEvaluation",
                width: 90,
                renderer: Ext.util.Format.dateRenderer("d/m/Y")
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: 20,
              store: StoreGridPreviousIdentifyRiskOperational,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              items: [
                "-",
                {
                  text: "Exportar",
                  iconCls: "excel",
                  handler: function(b, e) {
                    b.up("grid").downloadExcelXml();
                  }
                }
              ],
              doRefresh: function() {
                var me = this,
                  current = me.store.currentPage;
                var grid = me.up("grid");
                if (me.fireEvent("beforechange", me, current) !== false) {
                  me.store.loadPage(current);
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/showListRiskEvaluationMatrixActives.htm",
                    "description",
                    "idRiskEvaluationMatrix"
                  );
                }
              },
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            },
            listeners: {
              render: function() {
                var me = this;
                DukeSource.global.DirtyView.searchPaginationGridNormal(
                  "",
                  me,
                  me.down("pagingtoolbar"),
                  "http://localhost:9000/giro/showListRiskEvaluationMatrixActives.htm",
                  "description",
                  "idRiskEvaluationMatrix"
                );
              },
              itemdblclick: function(grid, record, item, index, e) {
                var app = DukeSource.application;
                app
                  .getController(
                    "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelPreviousIdentifyRiskOperational"
                  )
                  ._onShowPreviousRisk(grid, record);
              }
            }
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
