var impactStore = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboPercentReduction",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/findPercentReduction.htm",
    extraParams: {
      propertyOrder: "valuePercent",
      valueFind: "I",
      propertyFind: "typePercent"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
impactStore.load();
var frequencyStore = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboPercentReduction",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/findPercentReduction.htm",
    extraParams: {
      propertyOrder: "valuePercent",
      valueFind: "F",
      propertyFind: "typePercent"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
frequencyStore.load();

Ext.define("ModelGridPanelDebility", {
  extend: "Ext.data.Model",
  fields: [
    "idRiskWeaknessDetail",
    "description",
    "factorRisk",
    "descriptionFactorRisk",
    "average",
    "weight",
    "idRisk",
    "versionCorrelative",
    "idWeakness",
    "codeWeakness",
    "eventOne",
    "eventTwo",
    "eventThree",
    "idRelation",
    "totalRelation"
  ]
});
var StoreGridPanelDebility = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelDebility",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("ModelGridPanelControl", {
  extend: "Ext.data.Model",
  fields: [
    "idDetailControl",
    "idControl",
    "codeControl",
    "idRisk",
    "versionCorrelative",
    "idRiskWeaknessDetail",
    "scoreControl",
    "idVersionControl",
    "percentScoreControl",
    "dateEvaluation",
    "descriptionControl",
    "colorQualification",
    "nameQualification",
    "scoreDesign",
    "typeControl",
    "descriptionEvaluation",
    "scoreExecution",
    "colorScoreControlDesign",
    "colorScoreControlExecution",
    "weightControl",
    "focusControl",
    "finalValueControl",
    "checkEvidence"
  ]
});
var StoreGridPanelControl = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelControl",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("ModelGridActionPlan", {
  extend: "Ext.data.Model",
  fields: [
    "category",
    "checked",
    "codeRisk",
    "codeWeakness",
    "dateAssign",
    "description",
    "descriptionEmployment",
    "descriptionEntity",
    "descriptionJobPlace",
    "descriptionPriority",
    "descriptionProcess",
    "descriptionRisk",
    "descriptionTypeDocument",
    "descriptionWeakness",
    "descriptionWorkArea",
    "emailUserEndAssignment",
    "employment",
    "entitySender",
    "idActionPlan",
    "idAgreement",
    "codeAgreement",
    "idDetailDocument",
    "idDocument",
    "idProcess",
    "idRisk",
    "idRiskWeaknessDetail",
    "idWeakness",
    "jobPlace",
    "leaf",
    "nameStateDocument",
    "observation",
    "percentage",
    "priority",
    "stateDocument",
    "typeDocument",
    "fullNameEmitted",
    "userNameEmitted",
    "emailEmitted",
    "fullNameReceptor",
    "userNameReceptor",
    "emailReceptor",
    "versionCorrelative",
    "workArea",
    "idDocumentWeakness",
    "codePlan"
  ]
});
var StoreGridActionPlan = Ext.create("Ext.data.Store", {
  model: "ModelGridActionPlan",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("ModelGridMatrixByBusinessLine", {
  extend: "Ext.data.Model",
  fields: []
});
var StoreGridMatrixByBusinessLine = Ext.create("Ext.data.Store", {
  model: "ModelGridMatrixByBusinessLine",
  autoLoad: false,
  sorters: [],
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("ModelPanelEventsToRiskOperational", {
  extend: "Ext.data.Model",
  fields: [
    "idEvent",
    "codeEvent",
    "workArea",
    "isRiskEventIncidents",
    "idBusinessLineTwo",
    "businessLineOne",
    "factorRisk",
    "idEventThree",
    "eventTwo",
    "eventOne",
    "managerRisk",
    "agency",
    "currency",
    "riskType",
    "businessLineThree",
    "actionPlan",
    "process",
    "idSubProcess",
    "insuranceCompany",
    "eventState",
    "eventType",
    "lossType",
    "accountingEntryLoss",
    "accountingEntryRecoveryLoss",
    "descriptionLargeEvent",
    "descriptionShortEvent",
    "totalLossRecovery",
    "totalLoss",
    "colorEventState",
    "nameEventState",
    {
      name: "dateOccurrence",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    "entityPenalizesLoss",
    "grossLoss",
    "lossRecovery",
    "sureAmount",
    "netLoss",
    "typeChange",
    "typeEvent",
    "nameEventThree",
    "nameEventTwo",
    "nameEventOne",
    "nameFactorRisk",
    "nameAction",
    "nameCurrency",
    "descriptionShort",
    "idPlanAccountSureAmountRecovery",
    "planAccountSureAmountRecovery",
    "planAccountLossRecovery",
    "idPlanAccountLossRecovery",
    "bookkeepingEntryLossRecovery",
    "bookkeepingEntrySureAmountRecovery",
    "amountOrigin",
    "codeGeneralIncidents",
    "cause",
    "fileAttachment",
    "codesRiskAssociated",
    "indicatorAtRisk",
    "idRelationEventRisk",
    "idRisk",
    "versionCorrelative"
  ]
});
var StorePanelEventsToRiskOperational = Ext.create("Ext.data.Store", {
  model: "ModelPanelEventsToRiskOperational",
  autoLoad: true,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("ModelGridPanelControlAssignedToEvent", {
  extend: "Ext.data.Model",
  fields: [
    "versionCorrelative",
    "valueScoreControl",
    "percentScoreControl",
    "nameQualification",
    "idRiskWeaknessDetail",
    "idRisk",
    "idDetailControl",
    "idControl",
    "codeControl",
    "descriptionControl",
    "colorQualification"
  ]
});
var StoreGridPanelControlAssignetToEvent = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelControlAssignedToEvent",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("ModelDebilityEventAssigned", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "relationEventRisk",
    "idRiskWeaknessDetail",
    "idRisk",
    "versionCorrelative",
    "weight",
    "weakness",
    "descriptionWeakness",
    "codeWeakness",
    "descriptionFactorRisk",
    "state"
  ]
});
var StoreDebilityEventAssigned = Ext.create("Ext.data.Store", {
  model: "ModelDebilityEventAssigned",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

function loadReachedRisk(me, idRisk, idVersionRisk, row) {
  DukeSource.lib.Ajax.request({
    method: "POST",
    url: "http://localhost:9000/giro/findReachedRiskById.htm",
    params: {
      idReachedRisk: me.down("#idReachedRisk").getValue(),
      idActionPlan: row.get("idDocument")
    },
    scope: this,
    success: function(response) {
      response = Ext.decode(response.responseText);

      function settingColor(a, color) {
        a.setFieldStyle(
          "background-color: #" +
            color +
            " !important; background-image: none;color:#000000;"
        );
      }

      if (response.success) {
        var bean = response.data;
        var winReached = Ext.create(
          "DukeSource.view.risk.EvaluatorRiskOperational.windows.WindowReachedRisk",
          {
            modal: true,
            backPanel: me,
            idRisk: idRisk,
            idVersionRisk: idVersionRisk,
            idMatrixResidual: me.down("#idMatrixResidual").getValue(),
            idActionPlan: row.get("idDocument")
          }
        );
        winReached.setTitle(
          winReached.title +
            ' &#8702; <span style="color: red">' +
            me.down("#codeRisk").getValue() +
            "</span>"
        );

        if (response.data["id"] !== undefined) {
          winReached
            .down("form")
            .getForm()
            .setValues(bean);
          winReached
            .down("#descriptionFrequencyReached")
            .setValue(bean["descriptionFrequency"]);
          winReached
            .down("#descriptionImpactReached")
            .setValue(bean["descriptionImpact"]);

          winReached
            .down("#reachedValue")
            .setFieldLabel("Nivel - " + bean["descriptionMatrix"]);
          settingColor(winReached.down("#reachedValue"), bean["colorMatrix"]);
        }
        winReached.show();
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          response.message,
          Ext.Msg.WARNING
        );
      }
    },
    failure: function() {}
  });
}

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelEvaluationRiskOperational",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelEvaluationRiskOperational",
    requires: [
      "DukeSource.view.risk.parameter.combos.ViewComboTypeMatrix",
      "DukeSource.view.risk.parameter.combos.ViewComboFrequency",
      "DukeSource.view.risk.parameter.combos.ViewComboFrequencyFeature",
      "DukeSource.view.risk.parameter.combos.ViewComboImpact",
      "DukeSource.view.risk.parameter.combos.ViewComboDetailControlType",
      "DukeSource.view.risk.parameter.combos.ViewComboScoreControl",
      "DukeSource.view.risk.parameter.combos.ViewComboImpactFeature"
    ],
    itemId: "viewPanelEvaluationRiskOperational",
    layout: {
      type: "vbox",
      align: "stretch"
    },
    tbar: [
      {
        xtype: "button",
        scale: "medium",
        cls: "my-btn",
        overCls: "my-over",
        hidden: true,
        iconCls: "detail",
        text: "Identificación",
        handler: function(btn) {
          var parent = btn.up("panel");
          var win = Ext.create(
            "DukeSource.view.risk.IdentificationRiskOperational.windows.ViewDetailIdentifyRiskOperational",
            {
              modal: true,
              width: 1000,
              height: 600,
              idRisk: parent.idRisk
            }
          );
          win.show();
          var grid = win.down("grid");
          grid.store.proxy.url =
            "http://localhost:9000/giro/showListIdentificationActivesByRisk.htm?nameView=ViewPanelGenericIdentifyRiskOperational";
          grid.store.proxy.extraParams = {
            idRisk: parent.idRisk
          };
          grid.down("pagingtoolbar").moveFirst();
        }
      },
      {
        xtype: "button",
        text: "Salir",
        scale: "medium",
        cls: "my-btn",
        overCls: "my-over",
        iconCls: "logout",
        action: "goOutEvaluationPanel"
      }
    ],
    initComponent: function() {
      var me = this;
      var rowIdentify = me.rowIdentify;

      var idRisk = me.idRisk;
      var idTypeMatrix = me.idTypeMatrix;
      var idVersionRisk = rowIdentify.get("versionCorrelative");
      var idOperationalRiskExposition = rowIdentify.get(
        "idOperationalRiskExposition"
      );
      var typeProcess = rowIdentify.get("processType");
      var process = rowIdentify.get("process");
      var productFactor = rowIdentify.get("productFactor");
      var subProcess = rowIdentify.get("subProcess");

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            titleAlign: "center",
            itemId: "headFormEvaluationRisk",
            border: false,
            bodyStyle: "background:#d7e4f3",
            bodyPadding: 5,
            items: [
              {
                xtype: "container",
                height: 27,
                layout: "hbox",
                items: [
                  {
                    xtype: "textfield",
                    flex: 1,
                    padding: "0 5 0 0",
                    fieldLabel: "RIESGO",
                    fieldCls: "textFieldSmall",
                    labelWidth: 65,
                    name: "description",
                    itemId: "description"
                  },
                  {
                    xtype:"UpperCaseTextFieldReadOnly",
                    fieldCls: "codeIncident",
                    width: 150,
                    fieldLabel: "CODIGO",
                    itemId: "codeRisk",
                    name: "codeRisk",
                    labelWidth: 55
                  }
                ]
              },
              {
                xtype: "container",
                height: 27,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "textfield",
                    name: "idReachedRisk",
                    itemId: "idReachedRisk",
                    hidden: true
                  },
                  {
                    xtype: "displayfield",
                    fieldLabel: "PROCESO",
                    flex: 1.4,
                    padding: "0 5 0 0",
                    labelWidth: 65,
                    itemId: "descriptionProcess",
                    name: "descriptionProcess",
                    fieldCls: "style-for-url"
                  },
                  {
                    xtype: "button",
                    iconCls: "search",
                    text: "DETALLE",
                    hidden: true,
                    handler: function() {
                      var win = Ext.create(
                        "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowResumeEvaluationProcess"
                      );
                      Ext.Ajax.request({
                        method: "POST",
                        url:
                          "http://localhost:9000/giro/existEvaluationProcess.htm",
                        params: {
                          idProcess: process,
                          idSubProcess: subProcess,
                          id: typeProcess + "-" + process + "-" + subProcess,
                          idRisk: idRisk,
                          idVersionEvaluation: idVersionRisk
                        },
                        success: function(response) {
                          response = Ext.decode(response.responseText);
                          if (response.success) {
                            var dataQualificationProcess = Ext.JSON.decode(
                              response.qualificationProcessData
                            );
                            if (response.data === "S") {
                              var gridProcess = win.down(
                                "#calificationProcessGrid"
                              );
                              gridProcess.store.getProxy().extraParams = {
                                propertyFind: "description",
                                id:
                                  "myTree/" +
                                  typeProcess +
                                  "/" +
                                  process +
                                  "/" +
                                  subProcess,
                                propertyOrder: "description"
                              };
                              gridProcess.store.getProxy().url =
                                "http://localhost:9000/giro/showListFeatureProcessToEvaluate.htm";
                              gridProcess.down("pagingtoolbar").moveFirst();
                              win
                                .down("#formCalificationProcess")
                                .getForm()
                                .setValues(dataQualificationProcess);
                              win.show();
                            }
                          } else {
                            DukeSource.global.DirtyView.messageAlert(
                              DukeSource.global.GiroMessages.TITLE_WARNING,
                              response.mensaje,
                              Ext.Msg.ERROR
                            );
                          }
                        },
                        failure: function() {}
                      });
                    },
                    width: 70
                  },
                  {
                    xtype:"UpperCaseTextFieldReadOnly",
                    flex: 0.7,
                    padding: "0 0 0 50",
                    fieldLabel: "EVALUACIÓN",
                    fieldCls: "fieldBlue",
                    itemId: "nameEvaluation",
                    name: "nameEvaluation",
                    labelWidth: 72
                  },
                  {
                    xtype: "button",
                    width: 25,
                    iconCls: "maintainance",
                    handler: function() {
                      var window = Ext.create(
                        "DukeSource.view.risk.EvaluatorRiskOperational.windows.WindowModifyVersionEvaluation",
                        {
                          modal: true,
                          idRisk: idRisk,
                          backPanel: me,
                          idVersionRisk: idVersionRisk,
                          description: me.down("#nameEvaluation").getValue()
                        }
                      );
                      window.show();
                    }
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    name: "idTreatmentRisk",
                    itemId: "idTreatmentRisk"
                  },
                  {
                    xtype:"UpperCaseTextFieldReadOnly",
                    flex: 0.5,
                    padding: "0 0 0 10",
                    fieldCls: "fieldBlue",
                    name: "descriptionTreatment",
                    itemId: "descriptionTreatment",
                    fieldLabel: "TRATAMIENTO",
                    labelWidth: 85
                  },
                  {
                    xtype: "button",
                    width: 80,
                    iconCls: "treatment",
                    text: "DEFINIR",
                    tooltip: "Definir el tratamiento del riesgo",
                    handler: function() {
                      if (me.down("#idMatrixResidual").getValue() === "") {
                        DukeSource.global.DirtyView.messageWarning(
                          "Antes de Continuar, complete la Evaluación por favor"
                        );
                      } else {
                        var win = Ext.create(
                          "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowTreatment",
                          {
                            modal: true,
                            idRisk: idRisk,
                            originTreatment: "EVALUATION"
                          }
                        );
                        if (
                          me.down("#descriptionTreatment").getValue() === ""
                        ) {
                          win.down("#idTreatmentRisk").setValue("id");
                          win.down("textfield[name=idRisk]").setValue(idRisk);
                          win
                            .down("textfield[name=versionCorrelative]")
                            .setValue(idVersionRisk);
                          win
                            .down("ViewComboRiskAnswer")
                            .getStore()
                            .load({
                              callback: function() {
                                win.show();
                                win
                                  .down("ViewComboRiskAnswer")
                                  .focus(false, 200);
                              }
                            });
                        } else {
                          DukeSource.lib.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/findTreatmentRiskById.htm",
                            params: {
                              idTreatmentRisk: me
                                .down("#idTreatmentRisk")
                                .getValue()
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              if (response.success) {
                                win
                                  .down("ViewComboRiskAnswer")
                                  .getStore()
                                  .load({
                                    callback: function() {
                                      win
                                        .down("form")
                                        .getForm()
                                        .setValues(response.data);
                                      win.show();
                                    }
                                  });
                              } else {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_ERROR,
                                  response.mensaje,
                                  Ext.Msg.ERROR
                                );
                              }
                            },
                            failure: function() {}
                          });
                        }
                      }
                    }
                  }
                ]
              },
              {
                xtype: "container",
                padding: "5 0 0 0",
                layout: {
                  type: "hbox",
                  defaultMargins: {
                    top: 0,
                    right: 5,
                    bottom: 0,
                    left: 0
                  }
                },
                items: [
                  {
                    xtype: "fieldset",
                    flex: 1,
                    padding: 7,
                    collapsible: true,
                    title: "RIESGO INHERENTE",
                    layout: {
                      type: "hbox"
                    },
                    items: [
                      {
                        xtype:"UpperCaseTextFieldReadOnly",
                        flex: 1,
                        fieldLabel: "Frecuencia",
                        labelAlign: "top",
                        fieldCls: "textFieldSmall",
                        name: "descriptionFrequency",
                        itemId: "descriptionFrequency"
                      },
                      {
                        xtype:"UpperCaseTextFieldReadOnly",
                        flex: 1,
                        fieldLabel: "Impacto",
                        labelAlign: "top",
                        fieldCls: "textFieldSmall",
                        name: "descriptionImpact",
                        itemId: "descriptionImpact"
                      },
                      {
                        xtype: "NumberDecimalNumberObligatory",
                        flex: 1,
                        readOnly: true,
                        margin: "0 0 0 5",
                        labelSeparator: "",
                        fieldLabel: "Nivel",
                        labelAlign: "top",
                        name: "valueRiskInherent",
                        itemId: "valueRiskInherent"
                      }
                    ]
                  },
                  {
                    xtype: "fieldset",
                    flex: 0.8,
                    collapsible: true,
                    padding: 7,
                    title: "CONTROL TOTAL",
                    layout: {
                      type: "hbox"
                    },
                    items: [
                      {
                        xtype: "container",
                        name: "containerDetailControl",
                        hidden: hidden("VPER_CTN_detailControl"),
                        layout: {
                          type: "hbox"
                        },
                        flex: 1,
                        items: [
                          {
                            xtype: "numberfield",
                            readOnly: true,
                            decimalPrecision: DECIMAL_PRECISION,
                            fieldCls: "textFieldSmall",
                            name: "averageFrequency",
                            itemId: "averageFrequency",
                            fieldLabel: "Reduc.Frecuencia",
                            labelAlign: "top",
                            flex: 1
                          },
                          {
                            xtype: "numberfield",
                            readOnly: true,
                            decimalPrecision: DECIMAL_PRECISION,
                            fieldCls: "textFieldSmall",
                            name: "averageImpact",
                            itemId: "averageImpact",
                            fieldLabel: "Reduc.Impacto",
                            labelAlign: "top",
                            margin: "0 0 0 5",
                            flex: 1
                          }
                        ]
                      },
                      {
                        xtype: "container",
                        layout: {
                          type: "hbox"
                        },
                        flex: 1,
                        name: "containerResumeControl",
                        hidden: hidden("VPER_CTN_resumeControl"),
                        items: [
                          {
                            xtype:"UpperCaseTextFieldReadOnly",
                            name: "scoreDescriptionControl",
                            itemId: "scoreDescriptionControl",
                            fieldLabel: "Efectividad",
                            labelAlign: "top",
                            margin: "0 0 0 5",
                            fieldCls: "textFieldSmall",
                            flex: 1
                          },
                          {
                            xtype: "NumberDecimalNumberObligatory",
                            readOnly: true,
                            name: "evaluationFinalControl",
                            itemId: "evaluationFinalControl",
                            fieldLabel: "%",
                            labelAlign: "top",
                            flex: 0.5,
                            listeners: {
                              change: function(txt, newValue, oldvalue) {}
                            }
                          }
                        ]
                      }
                    ]
                  },
                  {
                    xtype: "fieldset",
                    flex: 0.5,
                    padding: 7,
                    hidden:
                      HIDDEN_FIEL_SET_REDUCTION ===
                      DukeSource.global.GiroConstants.TRUE,
                    collapsible: true,
                    title: "REDUCCION",
                    layout: {
                      type: "hbox"
                    },
                    items: [
                      {
                        xtype: "combobox",
                        queryMode: "local",
                        value: "X",
                        displayField: "description",
                        valueField: "id",
                        labelAlign: "top",
                        editable: false,
                        fieldLabel: "Reducción",
                        name: "resumeFocus",
                        itemId: "resumeFocus",
                        store: {
                          fields: ["id", "description"],
                          data: [
                            { id: "X", description: "Ninguno" },
                            { id: "F", description: "Frecuencia" },
                            { id: "I", description: "Impacto" },
                            { id: "IF", description: "Ambos" }
                          ]
                        },
                        listeners: {
                          select: function(cbo) {
                            DukeSource.lib.Ajax.request({
                              method: "POST",
                              url:
                                "http://localhost:9000/giro/reductionAllControlToRisk.htm",
                              params: {
                                idRisk: idRisk,
                                idVersionRisk: idVersionRisk,
                                allReduction: me
                                  .down("#evaluationFinalControl")
                                  .getValue(),
                                focus: cbo.getValue()
                              },
                              scope: this,
                              success: function(response) {
                                response = Ext.decode(response.responseText);
                                if (response.success) {
                                  setColorControlAndResidual(me, response);
                                  me.down("#headFormEvaluationRisk")
                                    .getForm()
                                    .setValues(response.data);
                                } else {
                                  DukeSource.global.DirtyView.messageWarning(response.message);
                                }
                              },
                              failure: function() {
                                DukeSource.global.DirtyView.messageWarning(response.message);
                              }
                            });
                          }
                        }
                      }
                    ]
                  },
                  {
                    xtype: "fieldset",
                    title: "RIESGO RESIDUAL",
                    padding: 7,
                    flex: 1,
                    collapsible: true,
                    layout: {
                      type: "hbox"
                    },
                    items: [
                      {
                        xtype:"UpperCaseTextFieldReadOnly",
                        flex: 1,
                        fieldLabel: "Frecuencia",
                        labelAlign: "top",
                        fieldCls: "textFieldSmall",
                        name: "descriptionFrequencyResidual",
                        itemId: "descriptionFrequencyResidual"
                      },
                      {
                        xtype:"UpperCaseTextFieldReadOnly",
                        flex: 1,
                        fieldLabel: "Impacto",
                        labelAlign: "top",
                        fieldCls: "textFieldSmall",
                        name: "descriptionImpactResidual",
                        itemId: "descriptionImpactResidual"
                      },
                      {
                        xtype: "NumberDecimalNumberObligatory",
                        flex: 1,
                        readOnly: true,
                        fieldLabel: "Nivel",
                        labelSeparator: "",
                        margin: "0 0 0 5",
                        labelAlign: "top",
                        name: "valueLostResidual",
                        itemId: "valueLostResidual"
                      },
                      {
                        xtype: "textfield",
                        name: "idMatrixResidual",
                        itemId: "idMatrixResidual",
                        hidden: true
                      }
                    ]
                  },
                  {
                    xtype: "fieldset",
                    title: "RIESGO OBJETIVO",
                    flex: 1,
                    padding: 7,
                    collapsible: true,
                    hidden: hidden("VPER_FDS_targetRisk"),
                    layout: {
                      type: "hbox"
                    },
                    items: [
                      {
                        xtype: "textfield",
                        hidden: true,
                        name: "idTargetRisk",
                        itemId: "idTargetRisk"
                      },
                      {
                        xtype:"UpperCaseTextFieldReadOnly",
                        flex: 1,
                        fieldLabel: "Frecuencia",
                        labelAlign: "top",
                        fieldCls: "textFieldSmall",
                        name: "descriptionTargetFrequency",
                        itemId: "descriptionTargetFrequency"
                      },
                      {
                        xtype:"UpperCaseTextFieldReadOnly",
                        flex: 1,
                        fieldLabel: "Impacto",
                        labelAlign: "top",
                        fieldCls: "textFieldSmall",
                        name: "descriptionTargetImpact",
                        itemId: "descriptionTargetImpact"
                      },
                      {
                        xtype: "NumberDecimalNumberObligatory",
                        flex: 1,
                        readOnly: true,
                        fieldLabel: "Nivel",
                        margin: "0 0 0 5",
                        labelAlign: "top",
                        name: "valueTargetRisk",
                        itemId: "valueTargetRisk"
                      },
                      {
                        xtype: "container",
                        layout: "vbox",
                        items: [
                          {
                            xtype: "label",
                            text: "Def:",
                            style: {
                              fontSize: "11px"
                            }
                          },
                          {
                            xtype: "button",
                            iconCls: "target",
                            margin: "4 0 0 0",
                            tooltip: "Definir el riesgo objetivo",
                            handler: function() {
                              DukeSource.lib.Ajax.request({
                                method: "POST",
                                url:
                                  "http://localhost:9000/giro/findTargetRiskById.htm",
                                params: {
                                  idTargetRisk: me
                                    .down("#idTargetRisk")
                                    .getValue()
                                },
                                scope: this,
                                success: function(response) {
                                  response = Ext.decode(response.responseText);

                                  function settingColor(a, color) {
                                    a.setFieldStyle(
                                      "background-color: #" +
                                        color +
                                        " !important; background-image: none;color:#000000;"
                                    );
                                  }

                                  if (response.success) {
                                    var bean = response.data;
                                    var winTarget = Ext.create(
                                      "DukeSource.view.risk.EvaluatorRiskOperational.windows.WindowTargetRisk",
                                      {
                                        modal: true,
                                        backPanel: me,
                                        idRisk: idRisk,
                                        idVersionRisk: idVersionRisk
                                      }
                                    );
                                    winTarget.setTitle(
                                      winTarget.title +
                                        ' &#8702; <span style="color: red">' +
                                        me.down("#codeRisk").getValue() +
                                        "</span>"
                                    );

                                    if (response.data["id"] !== undefined) {
                                      winTarget
                                        .down("form")
                                        .getForm()
                                        .setValues(bean);
                                      winTarget
                                        .down("#descriptionFrequencyTarget")
                                        .setValue(bean["descriptionFrequency"]);
                                      winTarget
                                        .down("#descriptionImpactTarget")
                                        .setValue(bean["descriptionImpact"]);

                                      winTarget
                                        .down("#targetValue")
                                        .setFieldLabel(
                                          "Nivel-" + bean["descriptionMatrix"]
                                        );
                                      settingColor(
                                        winTarget.down("#targetValue"),
                                        bean["colorMatrix"]
                                      );
                                    }
                                    winTarget.show();
                                  } else {
                                    DukeSource.global.DirtyView.messageAlert(
                                      DukeSource.global.GiroMessages
                                        .TITLE_WARNING,
                                      response.message,
                                      Ext.Msg.WARNING
                                    );
                                  }
                                },
                                failure: function() {}
                              });
                            }
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            xtype: "tabpanel",
            flex: 1,
            border: false,
            activeTab: 0,
            padding: "0 1 0 1",
            style: "background:#d7e4f3",
            items: [
              {
                xtype: "container",
                border: false,
                title: "Causas - Riesgo inherente",
                layout: {
                  type: "hbox",
                  align: "stretch"
                },
                items: [
                  {
                    xtype: "gridpanel",
                    region: "center",
                    style: "border-right:1px solid #99bce8",
                    flex: 1.2,
                    border: false,
                    name: "riskDebilityStart",
                    itemId: "riskDebilityStart",
                    title: "Causas por riesgo",
                    titleAlign: "center",
                    store: StoreGridPanelDebility,
                    loadMask: true,
                    columnLines: true,
                    columns: [
                      {
                        xtype: "rownumberer",
                        width: 25,
                        sortable: false
                      },
                      {
                        header: "Código",
                        dataIndex: "codeWeakness",
                        width: 60
                      },
                      {
                        header: "Descripción",
                        dataIndex: "description",
                        flex: 1
                      },
                      {
                        header: "Factor",
                        align: "center",
                        hidden: hidden("WRWFactorRisk"),
                        dataIndex: "descriptionFactorRisk",
                        width: 100,
                        tdCls: "process-columnFree",
                        renderer: function(value, metaData, record) {
                          var s = value.split(" &#8702; ");
                          var path = "";
                          for (var i = 0; i < s.length; i++) {
                            var text = s[i];
                            text = s[i] + "<br>";
                            path = path + " &#8702; " + text;
                          }
                          return path;
                        }
                      },
                      {
                        header: "Peso",
                        align: "center",
                        hidden:
                          DukeSource.global.GiroConstants.MODE_RESIDUAL_RC ===
                          MODE_RESIDUAL,
                        dataIndex: "weight",
                        width: 70
                      },
                      {
                        header: "Control",
                        align: "center",
                        hidden:
                          DukeSource.global.GiroConstants.MODE_RESIDUAL_RC ===
                          MODE_RESIDUAL,
                        dataIndex: "average",
                        width: 70
                      }
                    ],
                    tbar: [
                      {
                        xtype: "button",
                        itemId: "buttonNewWeaknessInherint",
                        text: "Nuevo",
                        iconCls: "add",
                        scale: "medium",
                        cls: "my-btn",
                        overCls: "my-over",
                        handler: function(button) {
                          if (
                            Ext.ComponentQuery.query(
                              "ViewWindowDebilityEntry"
                            )[0] === undefined
                          ) {
                            var win = Ext.create(
                              "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDebilityEntry",
                              {
                                origin: "Inherint",
                                modal: true
                              }
                            ).show();
                            win
                              .down("textfield[name=versionCorrelative]")
                              .setValue(idVersionRisk);
                          }
                        }
                      },
                      {
                        xtype: "button",
                        text: "Modificar",
                        iconCls: "modify",
                        scale: "medium",
                        cls: "my-btn",
                        overCls: "my-over",
                        handler: function(button) {
                          var grid = Ext.ComponentQuery.query(
                            "ViewPanelEvaluationRiskOperational"
                          )[0].down("#riskDebilityStart");
                          var row = grid.getSelectionModel().getSelection()[0];
                          if (row === undefined) {
                            DukeSource.global.DirtyView.messageAlert(
                              DukeSource.global.GiroMessages.TITLE_WARNING,
                              "Seleccione una causa indentificada por favor",
                              Ext.Msg.WARNING
                            );
                          } else {
                            var window = Ext.create(
                              "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDebilityEntry",
                              {
                                origin: "Inherint",
                                modal: true
                              }
                            );
                            window
                              .down("form")
                              .getForm()
                              .loadRecord(row);

                            if (row.get("eventOne") != null) {
                              var comboTwo = window.down("ViewComboEventTwo");
                              var comboTree = window.down(
                                "ViewComboEventThree"
                              );
                              window
                                .down("ViewComboEventOne")
                                .getStore()
                                .load({
                                  callback: function() {
                                    window
                                      .down("ViewComboEventOne")
                                      .setValue(row.get("eventOne"));
                                    comboTwo.setDisabled(false);
                                    comboTwo.getStore().load({
                                      url:
                                        "http://localhost:9000/giro/showListEventTwoActivesComboBox.htm",
                                      params: {
                                        valueFind: row.get("eventOne")
                                      },
                                      callback: function(cbo) {
                                        comboTwo.setValue(row.get("eventTwo"));
                                        comboTree.setDisabled(false);
                                        comboTree.getStore().load({
                                          url:
                                            "http://localhost:9000/giro/showListEventThreeActivesComboBox.htm",
                                          params: {
                                            valueFind: row.get("eventOne"),
                                            valueFind2: row.get("eventTwo")
                                          },
                                          callback: function() {
                                            comboTree.setValue(
                                              row.get("eventThree")
                                            );
                                          }
                                        });
                                      }
                                    });
                                  }
                                });
                            }
                            window.show();

                            var comboGridEvaluation = Ext.ComponentQuery.query(
                              "ViewPanelIdentifyRiskOperational"
                            )[0].down("ViewComboGridEvaluationRisk");
                            if (
                              comboGridEvaluation
                                .findRecordByValue(
                                  comboGridEvaluation.getValue()
                                )
                                .get("state") === "N"
                            ) {
                              window
                                .down("#buttonSearchWeakness")
                                .setDisabled(true);
                            }
                          }
                        }
                      },
                      {
                        xtype: "button",
                        origin: "Inherint",
                        itemId: "buttonDeleteWeaknessInherint",
                        text: "Eliminar",
                        iconCls: "delete",
                        scale: "medium",
                        cls: "my-btn",
                        overCls: "my-over",
                        action: "deleteDebilityRisk"
                      }
                    ],
                    bbar: {
                      xtype: "pagingtoolbar",
                      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                      store: StoreGridPanelDebility,
                      displayInfo: true,
                      displayMsg:
                        DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                      emptyMsg:
                        DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
                      doRefresh: function() {
                        var me = this,
                          current = me.store.currentPage;
                        var gralPanel = me.up(
                          "ViewPanelEvaluationRiskOperational"
                        );
                        if (
                          me.fireEvent("beforechange", me, current) !== false
                        ) {
                          me.store.loadPage(current);
                          DukeSource.global.DirtyView.loadGridDefault(
                            gralPanel.down("grid[name=riskControl]")
                          );
                        }
                      }
                    },
                    listeners: {
                      render: function() {
                        var me = this;
                        me.store.getProxy().extraParams = {
                          idRisk: idRisk,
                          versionCorrelative: idVersionRisk
                        };
                        me.store.getProxy().url =
                          "http://localhost:9000/giro/findRiskWeaknessDetail.htm";
                        me.down("pagingtoolbar").moveFirst();
                      },
                      beforerender: function(view) {
                        view.headerCt.border = 1;
                      }
                    }
                  },
                  {
                    xtype: "form",
                    itemId: "formRiskInherint",
                    border: false,
                    margin: "0 0 0 2    ",
                    style: "border-left:1px solid #99bce8",
                    autoScroll: true,
                    split: true,
                    flex: 1,
                    bodyPadding: 10,
                    title: "Riesgo inherente",
                    titleAlign: "center",
                    dockedItems: [
                      {
                        xtype: "toolbar",
                        dock: "top",
                        items: [
                          {
                            xtype: "button",
                            action: "saveAnalisysExposition",
                            iconCls: "save",
                            text: "Guardar",
                            scale: "medium",
                            cls: "my-btn",
                            overCls: "my-over"
                          }
                        ]
                      }
                    ],
                    items: [
                      {
                        xtype: "fieldset",
                        title: "Evaluación Inherente",
                        padding: 5,
                        name: "inherentEvaluation",
                        layout: {
                          type: "vbox"
                        },
                        items: [
                          {
                            xtype: "ViewComboTypeMatrix",
                            fieldLabel: "Tipo de matriz",
                            labelWidth: 130,
                            width: 350,
                            allowBlank: false,
                            hidden:
                              TYPE_MATRIX_SIZE ===
                              DukeSource.global.GiroConstants.ONE_S,
                            msgTarget: "side",
                            fieldCls: "obligatoryTextField",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "typeMatrix",
                            itemId: "typeMatrix",
                            tpl: Ext.create(
                              "Ext.XTemplate",
                              '<tpl for=".">',
                              '<div class="x-boundlist-item">{description} | {stateApproved}</div>',
                              "</tpl>"
                            ),
                            displayTpl: Ext.create(
                              "Ext.XTemplate",
                              '<tpl for=".">',
                              "{description} | {stateAssigned}",
                              "</tpl>"
                            ),
                            listeners: {
                              select: function(cbo) {
                                idTypeMatrix = cbo.getValue();
                              }
                            }
                          },
                          {
                            xtype: "container",
                            flex: 1,
                            height: 26,
                            name: "containerFrequency",
                            layout: {
                              align: "top",
                              type: "hbox"
                            },
                            items: [
                              {
                                xtype: "ViewComboFrequency",
                                allowBlank: false,
                                readOnly: true,
                                msgTarget: "side",
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                labelWidth: 130,
                                fieldLabel: "Probabilidad",
                                name: "idFrequency",
                                itemId: "idFrequency",
                                listeners: {
                                  render: function(cbo) {
                                    if (idTypeMatrix !== "") {
                                      cbo.getStore().load({
                                        params: {
                                          idTypeMatrix: idTypeMatrix,
                                          idOperationalRiskExposition: idOperationalRiskExposition
                                        }
                                      });
                                    }
                                  }
                                }
                              },
                              {
                                xtype: "textarea",
                                hidden: true,
                                name: "reasonFrequency",
                                itemId: "reasonFrequency"
                              },
                              {
                                xtype: "button",
                                text: "Seleccionar",
                                iconCls: "select",
                                handler: function() {
                                  if (
                                    me.down("#typeMatrix").getValue() === null
                                  ) {
                                    DukeSource.global.DirtyView.messageWarning(
                                      "Seleccione el tipo de matriz"
                                    );
                                  } else {
                                    var win = Ext.create(
                                      "DukeSource.view.risk.EvaluatorRiskOperational.windows.WindowSelectFrequency",
                                      {
                                        idTypeMatrix: me
                                          .down("#typeMatrix")
                                          .getValue(),
                                        idOperationalRiskExposition: idOperationalRiskExposition,
                                        idFrequency: me
                                          .down("#idFrequency")
                                          .getValue(),
                                        backPanel: me,
                                        isVersionActive: me.isVersionActive
                                      }
                                    );

                                    win.show(undefined, function() {
                                      DukeSource.lib.Ajax.request({
                                        waitMsg:
                                          DukeSource.global.GiroMessages
                                            .MESSAGE_LOADING,
                                        method: "POST",
                                        url:
                                          "http://localhost:9000/giro/showFrequencyFeatureFullJson.htm",
                                        params: {
                                          idTypeMatrix: me
                                            .down("#typeMatrix")
                                            .getValue(),
                                          maxExposition: idOperationalRiskExposition,
                                          idRisk: idRisk,
                                          idVersionEvaluation: idVersionRisk,
                                          start: 0,
                                          limit:
                                            DukeSource.global.GiroConstants
                                              .ITEMS_PAGE
                                        },
                                        success: function(response) {
                                          response = Ext.decode(
                                            response.responseText
                                          );
                                          if (response.success) {
                                            win.createCombos(response);
                                          } else {
                                            DukeSource.global.DirtyView.messageWarning(
                                              response.message
                                            );
                                          }
                                        }
                                      });
                                    });
                                  }
                                }
                              }
                            ]
                          },
                          {
                            xtype: "container",
                            flex: 1,
                            height: 26,
                            name: "containerImpact",
                            layout: {
                              align: "top",
                              type: "hbox"
                            },
                            items: [
                              {
                                xtype: "ViewComboImpact",
                                allowBlank: false,
                                readOnly: true,
                                msgTarget: "side",
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                labelWidth: 130,
                                fieldLabel: "Impacto",
                                name: "idImpact",
                                itemId: "idImpact",
                                listeners: {
                                  render: function(cbo) {
                                    if (idTypeMatrix !== "") {
                                      cbo.getStore().load({
                                        params: {
                                          idTypeMatrix: idTypeMatrix,
                                          idOperationalRiskExposition: idOperationalRiskExposition
                                        }
                                      });
                                    }
                                  }
                                }
                              },
                              {
                                xtype: "textarea",
                                hidden: true,
                                fieldLabel: "Sustento impacto",
                                itemId: "reasonImpact",
                                name: "reasonImpact"
                              },
                              {
                                xtype: "button",
                                text: "Seleccionar",
                                iconCls: "select",
                                handler: function() {
                                  if (
                                    me.down("#typeMatrix").getValue() === null
                                  ) {
                                    DukeSource.global.DirtyView.messageWarning(
                                      "Seleccione el tipo de matriz"
                                    );
                                  } else {
                                    var win = Ext.create(
                                      "DukeSource.view.risk.EvaluatorRiskOperational.windows.WindowSelectImpact",
                                      {
                                        idTypeMatrix: me
                                          .down("#typeMatrix")
                                          .getValue(),
                                        idOperationalRiskExposition: idOperationalRiskExposition,
                                        idImpact: me
                                          .down("#idImpact")
                                          .getValue(),
                                        backPanel: me,
                                        isVersionActive: me.isVersionActive
                                      }
                                    );

                                    win.show(undefined, function() {
                                      DukeSource.lib.Ajax.request({
                                        waitMsg:
                                          DukeSource.global.GiroMessages
                                            .MESSAGE_LOADING,
                                        method: "POST",
                                        url:
                                          "http://localhost:9000/giro/showImpactFeatureFullJson.htm",
                                        params: {
                                          idTypeMatrix: me
                                            .down("#typeMatrix")
                                            .getValue(),
                                          maxExposition: idOperationalRiskExposition,
                                          idRisk: idRisk,
                                          idVersionEvaluation: idVersionRisk,
                                          start: 0,
                                          limit:
                                            DukeSource.global.GiroConstants
                                              .ITEMS_PAGE
                                        },
                                        success: function(response) {
                                          response = Ext.decode(
                                            response.responseText
                                          );
                                          if (response.success) {
                                            win.createCombos(response);
                                          } else {
                                            DukeSource.global.DirtyView.messageWarning(
                                              response.message
                                            );
                                          }
                                        }
                                      });
                                    });
                                  }
                                }
                              }
                            ]
                          },
                          {
                            xtype: "container",
                            name: "containerResultInherent",
                            flex: 1,
                            height: 26,
                            layout: {
                              type: "hbox"
                            },
                            items: [
                              {
                                xtype: "UpperCaseTextFieldObligatory",
                                labelWidth: 130,
                                readOnly: true,
                                name: "descriptionScaleRisk",
                                itemId: "descriptionScaleRisk",
                                padding: "0 5 0 0",
                                fieldLabel: "Riesgo inherente"
                              },
                              {
                                xtype: "NumberDecimalNumberObligatory",
                                itemId: "valueRiskInherentForm",
                                name: "valueRiskInherentForm"
                              },
                              {
                                xtype: "textfield",
                                name: "codeMatrix",
                                itemId: "codeMatrix",
                                hidden: true
                              },
                              {
                                xtype: "textfield",
                                name: "idMatrix",
                                itemId: "idMatrix",
                                hidden: true
                              },
                              {
                                xtype: "textfield",
                                name: "scaleRisk",
                                itemId: "scaleRisk",
                                hidden: true
                              }
                            ]
                          },
                          {
                            xtype: "NumberDecimalNumber",
                            labelWidth: 130,
                            itemId: "expectedLoss",
                            name: "expectedLoss",
                            allowBlank: blank("VPEExpectedLoss"),
                            value: 0,
                            hidden: hidden("VPEExpectedLoss"),
                            fieldCls: "textFieldSmall",
                            padding: "0 5 0 0",
                            fieldLabel:
                              getName("VPEExpectedLoss") + " (" + symbol + ")"
                          }
                        ]
                      },
                      {
                        xtype: "fieldset",
                        title: "Pérdidas vinculadas",
                        padding: 5,
                        name: "eventsAssociated",
                        layout: {
                          type: "hbox"
                        },
                        items: [
                          {
                            xtype: "NumberDecimalNumber",
                            labelWidth: 130,
                            readOnly: true,
                            itemId: "amountEventLost",
                            name: "amountEventLost",
                            fieldCls: "numberPositive",
                            padding: "0 5 0 0",
                            fieldLabel: "Monto total (" + symbol + ")"
                          },
                          {
                            xtype: "textfield",
                            readOnly: true,
                            fieldLabel: "Ocurrencias",
                            fieldCls: "numberPositive",
                            fieldStyle: "text-align: center;",
                            itemId: "numberEventLost",
                            name: "numberEventLost"
                          }
                        ]
                      }
                    ]
                  }
                ]
              },
              {
                xtype: "container",
                border: false,
                title: "Controles - Plan de acción",
                layout: {
                  type: "hbox",
                  align: "stretch"
                },
                items: [
                  {
                    xtype: "panel",
                    layout: "accordion",
                    border: false,
                    split: true,
                    margin: "0 0 0 2",
                    style: "border-left:1px solid #99bce8",
                    flex: 1,
                    items: [
                      {
                        xtype: "gridpanel",
                        itemId: "riskDebility",
                        name: "riskDebility",
                        title:
                          '<span style="color: #0a387e; font-weight: bold"> Causas por riesgo </span>',
                        titleAlign: "center",
                        store: StoreGridPanelDebility,
                        columnLines: true,
                        viewConfig: {
                          stripeRows: true
                        },
                        loadMask: true,
                        columns: [
                          {
                            xtype: "rownumberer",
                            width: 25,
                            sortable: false
                          },
                          {
                            header: "Código",
                            dataIndex: "codeWeakness",
                            width: 60
                          },
                          {
                            header: "Descripción",
                            dataIndex: "description",
                            flex: 1
                          },
                          {
                            header: "Factor",
                            align: "center",
                            hidden: hidden("WRWFactorRisk"),
                            dataIndex: "descriptionFactorRisk",
                            width: 100
                          },
                          {
                            header: "Peso",
                            align: "center",
                            hidden:
                              DukeSource.global.GiroConstants
                                .MODE_RESIDUAL_RC === MODE_RESIDUAL,
                            dataIndex: "weight",
                            width: 70
                          },
                          {
                            header: "Control",
                            align: "center",
                            hidden:
                              DukeSource.global.GiroConstants
                                .MODE_RESIDUAL_RC === MODE_RESIDUAL,
                            dataIndex: "average",
                            width: 70
                          }
                        ],
                        dockedItems: [
                          {
                            xtype: "toolbar",
                            dock: "top",
                            border: false,
                            items: [
                              {
                                text: "Nuevo",
                                itemId: "buttonNewWeaknessControl",
                                iconCls: "add",
                                scale: "medium",
                                cls: "my-btn",
                                overCls: "my-over",
                                handler: function(button) {
                                  var win = Ext.create(
                                    "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDebilityEntry",
                                    {
                                      origin: "Control",
                                      modal: true
                                    }
                                  ).show();
                                  win
                                    .down("textfield[name=versionCorrelative]")
                                    .setValue(idVersionRisk);
                                }
                              },
                              {
                                text: "Modificar",
                                iconCls: "modify",
                                scale: "medium",
                                cls: "my-btn",
                                overCls: "my-over",
                                handler: function() {
                                  var grid = Ext.ComponentQuery.query(
                                    "ViewPanelEvaluationRiskOperational"
                                  )[0].down("#riskDebility");
                                  var record = grid
                                    .getSelectionModel()
                                    .getSelection()[0];

                                  if (record === undefined) {
                                    DukeSource.global.DirtyView.messageAlert(
                                      DukeSource.global.GiroMessages
                                        .TITLE_WARNING,
                                      "Seleccione una causa indentificada por favor",
                                      Ext.Msg.WARNING
                                    );
                                  } else {
                                    var window = Ext.create(
                                      "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDebilityEntry",
                                      {
                                        origin: "Control",
                                        modal: true
                                      }
                                    );
                                    window
                                      .down("form")
                                      .getForm()
                                      .loadRecord(record);
                                    if (
                                      record.get("totalRelation") !==
                                        undefined &&
                                      record.get("totalRelation") * 1 > 0
                                    ) {
                                      window
                                        .down("#idRelation")
                                        .setReadOnly(true);
                                    }

                                    var comboTwo = window.down(
                                      "ViewComboEventTwo"
                                    );
                                    var comboTree = window.down(
                                      "ViewComboEventThree"
                                    );
                                    window
                                      .down("ViewComboEventOne")
                                      .getStore()
                                      .load({
                                        callback: function(cbo) {
                                          window
                                            .down("ViewComboEventOne")
                                            .setValue(record.get("eventOne"));
                                          comboTwo.setDisabled(false);
                                          comboTwo.getStore().load({
                                            url:
                                              "http://localhost:9000/giro/showListEventTwoActivesComboBox.htm",
                                            params: {
                                              valueFind: record.get("eventOne")
                                            },
                                            callback: function() {
                                              comboTwo.setValue(
                                                record.get("eventTwo")
                                              );
                                              comboTree.setDisabled(false);
                                              comboTree.getStore().load({
                                                url:
                                                  "http://localhost:9000/giro/showListEventThreeActivesComboBox.htm",
                                                params: {
                                                  valueFind: record.get(
                                                    "eventOne"
                                                  ),
                                                  valueFind2: record.get(
                                                    "eventTwo"
                                                  )
                                                },
                                                callback: function() {
                                                  comboTree.setValue(
                                                    record.get("eventThree")
                                                  );
                                                  window.show();
                                                }
                                              });
                                            }
                                          });
                                        }
                                      });
                                    var comboGridEvaluation = Ext.ComponentQuery.query(
                                      "ViewPanelIdentifyRiskOperational"
                                    )[0].down("ViewComboGridEvaluationRisk");
                                    if (
                                      comboGridEvaluation
                                        .findRecordByValue(
                                          comboGridEvaluation.getValue()
                                        )
                                        .get("state") === "N"
                                    ) {
                                      window
                                        .down("#buttonSearchWeakness")
                                        .setDisabled(true);
                                    }
                                  }
                                }
                              },
                              {
                                origin: "Control",
                                itemId: "buttonDeleteWeaknessControl",
                                text: "Eliminar",
                                scale: "medium",
                                cls: "my-btn",
                                overCls: "my-over",
                                iconCls: "delete",
                                action: "deleteDebilityRisk"
                              }
                            ]
                          }
                        ],
                        bbar: {
                          xtype: "pagingtoolbar",
                          pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                          store: StoreGridPanelDebility,
                          displayInfo: true,
                          displayMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                          emptyMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
                          doRefresh: function() {
                            var me = this,
                              current = me.store.currentPage;
                            var gralPanel = me.up(
                              "ViewPanelEvaluationRiskOperational"
                            );
                            if (
                              me.fireEvent("beforechange", me, current) !==
                              false
                            ) {
                              me.store.loadPage(current);
                              DukeSource.global.DirtyView.loadGridDefault(
                                gralPanel.down("grid[name=riskControl]")
                              );
                            }
                          }
                        },
                        listeners: {
                          render: function() {
                            var me = this;
                            me.store.getProxy().extraParams = {
                              idRisk: idRisk,
                              versionCorrelative: idVersionRisk
                            };
                            me.store.getProxy().url =
                              "http://localhost:9000/giro/findRiskWeaknessDetail.htm";
                            me.down("pagingtoolbar").moveFirst();
                          },
                          beforerender: function(view) {
                            view.headerCt.border = 1;
                          }
                        }
                      },
                      {
                        xtype: "gridpanel",
                        itemId: "riskConsequence",
                        name: "riskConsequence",
                        title:
                          '<span style="color: #0a387e; font-weight: bold"> Consecuencias por riesgo </span>',
                        titleAlign: "center",
                        hidden: true,
                        store: StoreGridPanelDebility,
                        columnLines: true,
                        viewConfig: {
                          stripeRows: true
                        },
                        loadMask: true,
                        columns: [
                          {
                            xtype: "rownumberer",
                            width: 25,
                            sortable: false
                          },
                          {
                            header: "Código",
                            dataIndex: "codeWeakness",
                            width: 60
                          },
                          {
                            header: "Descripción",
                            dataIndex: "description",
                            flex: 1
                          },
                          {
                            header: "Factor",
                            align: "center",
                            hidden: hidden("WRWFactorRisk"),
                            dataIndex: "nameFactorRisk",
                            width: 100
                          },
                          {
                            header: "Peso",
                            align: "center",
                            hidden:
                              DukeSource.global.GiroConstants
                                .MODE_RESIDUAL_RC === MODE_RESIDUAL,
                            dataIndex: "weight",
                            width: 70
                          },
                          {
                            header: "Control",
                            align: "center",
                            hidden:
                              DukeSource.global.GiroConstants
                                .MODE_RESIDUAL_RC === MODE_RESIDUAL,
                            dataIndex: "average",
                            width: 70
                          }
                        ],
                        dockedItems: [
                          {
                            xtype: "toolbar",
                            dock: "top",
                            border: false,
                            items: [
                              {
                                text: "Nuevo",
                                iconCls: "add",
                                scale: "medium",
                                cls: "my-btn",
                                overCls: "my-over",
                                handler: function(button) {}
                              },
                              {
                                text: "Modificar",
                                iconCls: "modify",
                                scale: "medium",
                                cls: "my-btn",
                                overCls: "my-over",
                                handler: function() {}
                              },
                              {
                                text: "Eliminar",
                                iconCls: "delete",
                                scale: "medium",
                                cls: "my-btn",
                                overCls: "my-over",
                                handler: function() {}
                              }
                            ]
                          }
                        ],
                        bbar: {
                          xtype: "pagingtoolbar",
                          pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                          store: StoreGridPanelDebility,
                          displayInfo: true,
                          displayMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                          emptyMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
                          doRefresh: function() {}
                        },
                        listeners: {}
                      }
                    ]
                  },
                  {
                    xtype: "panel",
                    layout: "accordion",
                    border: false,
                    split: true,
                    margin: "0 0 0 2",
                    style: "border-left:1px solid #99bce8",
                    flex: 1.2,
                    items: [
                      {
                        xtype: "gridpanel",
                        itemId: "riskControl",
                        name: "riskControl",
                        title:
                          '<span style="color: #0a387e; font-weight: bold">' +
                          getName("VPETitleGridControl") +
                          "</span>",
                        titleAlign: "center",
                        store: StoreGridPanelControl,
                        columnLines: true,
                        viewConfig: {
                          stripeRows: true
                        },
                        loadMask: true,
                        columns: [
                          {
                            header: "Código",
                            dataIndex: "codeControl",
                            width: 60
                          },
                          {
                            header: "Control",
                            dataIndex: "descriptionControl",
                            flex: 2.4
                          },
                          {
                            header: "Calificación",
                            align: "center",
                            dataIndex: "nameQualification",
                            flex: 0.8,
                            renderer: function(value, metaData, record) {
                              metaData.tdAttr =
                                'style="background-color: #' +
                                record.get("colorQualification") +
                                ' !important;"';
                              return (
                                '<span style="font-size: 10px"><b>' +
                                record.get("percentScoreControl") +
                                "</b></span><br>" +
                                value
                              );
                            }
                          },
                          {
                            header: "Peso",
                            align: "center",
                            hidden: hidden("VPEWeightControl"),
                            dataIndex: "weightControl",
                            flex: 0.4,
                            renderer: function(value) {
                              return "<span>" + value + "%</span>";
                            }
                          },
                          {
                            header: "Valor Final",
                            align: "center",
                            hidden: hidden("VPEFinalValueControl"),
                            dataIndex: "finalValueControl",
                            flex: 0.4
                          },
                          {
                            header: "Foco",
                            align: "center",
                            hidden: hidden("WECFocusControl"),
                            dataIndex: "focusControl",
                            flex: 0.5,
                            renderer: function(value) {
                              value =
                                value === "I"
                                  ? "Impacto"
                                  : value === "F"
                                  ? "Frecuencia"
                                  : "Ambos";
                              return value;
                            }
                          }
                        ],
                        editControl: function(grid, rowControl) {
                          var diff;
                          if (rowControl === undefined) {
                            DukeSource.global.DirtyView.messageWarning(
                              "Seleccione un control identificado por favor"
                            );
                          } else {
                            var idControl = rowControl.get("idControl");
                            var idVersionControl = rowControl.get(
                              "idVersionControl"
                            );

                            var window = Ext.create(
                              "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowControlEntryResume",
                              {
                                modal: true,
                                origin: "catalogControl",
                                isVersionActive: me.isVersionActive
                              }
                            );
                            window
                              .down("form")
                              .getForm()
                              .loadRecord(rowControl);
                            DukeSource.lib.Ajax.request({
                              method: "POST",
                              url:
                                "http://localhost:9000/giro/showFinalScoreControlByVersion.htm",
                              params: {
                                idControl: idControl,
                                idVersionControl: idVersionControl,
                                start: "0",
                                limit: "100"
                              },
                              success: function(response) {
                                response = Ext.decode(response.responseText);
                                if (response.success) {
                                  var lastScore =
                                    response.data[1]["valueControl"];
                                  diff =
                                    parseFloat(lastScore) !==
                                    parseFloat(
                                      rowControl.get("finalValueControl")
                                    );

                                  window.show(undefined, function() {
                                    for (
                                      var i = 0;
                                      i < response.data.length;
                                      i++
                                    ) {
                                      var scoreGroup = {};
                                      DukeSource.global.DirtyView.createGroup(
                                        scoreGroup,
                                        response,
                                        i
                                      );

                                      scoreGroup.typeControl = rowControl.get(
                                        "typeControl"
                                      );
                                      window.numberItems = response.data.length;
                                      window.groups = response.data;
                                      DukeSource.global.DirtyView.createItemsToFinalScore(
                                        window,
                                        scoreGroup
                                      );
                                    }
                                    window.setWidth(
                                      response.data.length > 2
                                        ? 310 * response.data.length
                                        : 760
                                    );

                                    var scoreFinal = DukeSource.global.DirtyView.createScoreFinal(
                                      response
                                    );
                                    scoreFinal.typeControl = rowControl.get(
                                      "typeControl"
                                    );

                                    DukeSource.global.DirtyView.createItemsToFinalScore(
                                      window,
                                      scoreFinal
                                    );
                                    window
                                      .down("#" + scoreFinal.idItemValue)
                                      .setFieldLabel("Rating Total");

                                    DukeSource.global.DirtyView.createButton(window);
                                  });
                                } else {
                                  DukeSource.global.DirtyView.messageWarning(response.message);
                                }
                              }
                            });

                            DukeSource.lib.Ajax.request({
                              method: "POST",
                              url:
                                "http://localhost:9000/giro/findControlByVersion.htm",
                              params: {
                                idControl: idControl,
                                idVersionControl: idVersionControl,
                                start: "0",
                                limit: "25"
                              },
                              success: function(response) {
                                var form = window.down("form");
                                response = Ext.decode(response.responseText);
                                if (response.success) {
                                  form.getForm().setValues(response.data);
                                  form
                                    .down("#description")
                                    .setValue(
                                      rowControl.get("descriptionControl")
                                    );
                                  form
                                    .down("#idRiskWeaknessDetail")
                                    .setValue(
                                      rowControl.get("idRiskWeaknessDetail")
                                    );
                                  form
                                    .down("#idRisk")
                                    .setValue(rowControl.get("idRisk"));
                                  form
                                    .down("#versionCorrelative")
                                    .setValue(
                                      rowControl.get("versionCorrelative")
                                    );
                                } else {
                                  DukeSource.global.DirtyView.messageWarning(response.message);
                                }
                                window
                                  .down("#descriptionEvaluation")
                                  .focus(false, 100);
                              },
                              failure: function() {}
                            });
                            var grid1 = window.down("grid");
                            grid1.store.getProxy().extraParams = {
                              idControl: idControl,
                              idVersionControl: idVersionControl
                            };
                            grid1.store.getProxy().url =
                              "http://localhost:9000/giro/showListControlTypeByVersion.htm";
                            grid1.down("pagingtoolbar").doRefresh();
                          }
                        },
                        dockedItems: [
                          {
                            xtype: "toolbar",
                            dock: "top",
                            border: false,
                            items: [
                              {
                                text: "Nuevo",
                                itemId: "buttonNewControl",
                                iconCls: "add",
                                scale: "medium",
                                cls: "my-btn",
                                overCls: "my-over",
                                handler: function() {
                                  var panel = Ext.ComponentQuery.query(
                                    "ViewPanelEvaluationRiskOperational"
                                  )[0];
                                  var valueRiskInherent = panel
                                    .down("#valueRiskInherent")
                                    .getValue();
                                  if (valueRiskInherent == null) {
                                    DukeSource.global.DirtyView.messageWarning(
                                      "Defina el riesgo inherente"
                                    );
                                  } else {
                                    var grid = panel.down("#riskDebility");
                                    var rowDebility = grid
                                      .getSelectionModel()
                                      .getSelection()[0];
                                    if (rowDebility === undefined) {
                                      DukeSource.global.DirtyView.messageWarning(
                                        "Seleccione una causa identificada por favor"
                                      );
                                    } else {
                                      var win = Ext.create(
                                        "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowControlEntryResume",
                                        {
                                          modal: true,
                                          origin: "catalogControl",
                                          isVersionActive: me.isVersionActive
                                        }
                                      );
                                      var form = win.down("form");
                                      DukeSource.lib.Ajax.request({
                                        method: "POST",
                                        url:
                                          "http://localhost:9000/giro/showListGroupControlTypeActives.htm",
                                        params: {
                                          propertyOrder: "description",
                                          start: "0",
                                          limit: "25"
                                        },
                                        success: function(response) {
                                          response = Ext.decode(
                                            response.responseText
                                          );
                                          if (response.success) {
                                            win.show();
                                            form
                                              .down("#idRiskWeaknessDetail")
                                              .setValue(
                                                rowDebility.get(
                                                  "idRiskWeaknessDetail"
                                                )
                                              );
                                            form
                                              .down("#idRisk")
                                              .setValue(
                                                rowDebility.get("idRisk")
                                              );
                                            form
                                              .down("#versionCorrelative")
                                              .setValue(
                                                rowDebility.get(
                                                  "versionCorrelative"
                                                )
                                              );
                                          } else {
                                            DukeSource.global.DirtyView.messageWarning(
                                              response.message
                                            );
                                          }
                                        }
                                      });
                                      var grid1 = win.down("grid");
                                      grid1.store.getProxy().url =
                                        "http://localhost:9000/giro/loadGridDefault.htm";
                                      grid1.down("pagingtoolbar").moveFirst();
                                    }
                                  }
                                }
                              },
                              {
                                text: "Modificar",
                                itemId: "btnDetailControl",
                                scale: "medium",
                                cls: "my-btn",
                                overCls: "my-over",
                                iconCls: "detail",
                                handler: function() {
                                  var gridControl = me.down("#riskControl");
                                  var rowControl = gridControl
                                    .getSelectionModel()
                                    .getSelection()[0];
                                  gridControl.editControl(
                                    gridControl,
                                    rowControl
                                  );
                                }
                              },
                              {
                                text: "Eliminar",
                                itemId: "buttonDeleteControl",
                                scale: "medium",
                                cls: "my-btn",
                                overCls: "my-over",
                                iconCls: "delete",
                                action: "deleteControlRisk"
                              }
                            ]
                          }
                        ],
                        bbar: {
                          xtype: "pagingtoolbar",
                          pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                          store: StoreGridPanelControl,
                          displayInfo: true,
                          displayMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                          emptyMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                        },
                        listeners: {
                          beforerender: function(view) {
                            view.headerCt.border = 1;
                          },
                          itemdblclick: function(grid, rowControl) {
                            this.editControl(grid, rowControl);
                          }
                        }
                      },
                      {
                        xtype: "gridpanel",
                        name: "gridActionPlan",
                        itemId: "gridActionPlan",
                        title:
                          '<span style="color: #0a387e; font-weight: bold">Planes de acción</span>',
                        titleAlign: "center",
                        loadMask: true,
                        store: StoreGridActionPlan,
                        columnLines: true,
                        border: false,
                        columns: [
                          {
                            xtype: "rownumberer",
                            width: 25,
                            sortable: false
                          },
                          {
                            header: "Código",
                            dataIndex: "codePlan",
                            width: 80
                          },
                          {
                            header: "Plan de acción",
                            dataIndex: "description",
                            width: 400
                          },
                          {
                            header: "Estado",
                            dataIndex: "nameStateDocument",
                            width: 100
                          },
                          {
                            header: "Responsable",
                            dataIndex: "fullNameReceptor",
                            width: 160
                          }
                        ],
                        dockedItems: [
                          {
                            xtype: "toolbar",
                            dock: "top",
                            border: false,
                            items: [
                              {
                                text: "Vincular",
                                scale: "medium",
                                cls: "my-btn",
                                overCls: "my-over",
                                itemId: "newActionPlan",
                                iconCls: "add",
                                handler: function() {
                                  var grid = Ext.ComponentQuery.query(
                                    "ViewPanelEvaluationRiskOperational"
                                  )[0].down("#riskDebility");
                                  var rowDebility = grid
                                    .getSelectionModel()
                                    .getSelection()[0];
                                  if (rowDebility === undefined) {
                                    DukeSource.global.DirtyView.messageWarning(
                                      "Seleccione una causa identificada por favor"
                                    );
                                  } else {
                                    if (
                                      me
                                        .down("#valueRiskInherent")
                                        .getValue() != null
                                    ) {
                                      var win = Ext.create(
                                        "DukeSource.view.fulfillment.window.ViewWindowSearchActionPlan",
                                        {
                                          buttonAlign: "center",
                                          modal: true,
                                          buttons: [
                                            {
                                              text: "Vincular",
                                              iconCls: "relation",
                                              scale: "medium",
                                              handler: function() {
                                                saveRelationActionPlan(
                                                  win,
                                                  rowDebility
                                                );
                                              }
                                            },
                                            {
                                              text: "Salir",
                                              scale: "medium",
                                              iconCls: "logout",
                                              handler: function() {
                                                win.close();
                                              }
                                            }
                                          ]
                                        }
                                      );
                                      win.show();
                                    } else {
                                      DukeSource.global.DirtyView.messageWarning(
                                        "Defina el riesgo inherente"
                                      );
                                    }
                                  }
                                }
                              },
                              {
                                text: "Desvincular",
                                scale: "medium",
                                cls: "my-btn",
                                overCls: "my-over",
                                itemId: "deleteActionPlan",
                                iconCls: "delete",
                                handler: function() {
                                  var grid = Ext.ComponentQuery.query(
                                    "ViewPanelEvaluationRiskOperational"
                                  )[0].down("#gridActionPlan"); //panel.down('grid[name=riskDebility]');
                                  var row = grid
                                    .getSelectionModel()
                                    .getSelection()[0];
                                  if (row === undefined) {
                                    DukeSource.global.DirtyView.messageAlert(
                                      DukeSource.global.GiroMessages
                                        .TITLE_WARNING,
                                      "SELECCIONE UN PLAN DE ACCION POR FAVOR",
                                      Ext.Msg.WARNING
                                    );
                                  } else {
                                    Ext.MessageBox.show({
                                      title:
                                        DukeSource.global.GiroMessages
                                          .TITLE_WARNING,
                                      msg:
                                        "Estas seguro que desea desvincular este plan de accion?",
                                      icon: Ext.Msg.QUESTION,
                                      buttonText: {
                                        yes: "Si",
                                        no: "No"
                                      },
                                      buttons: Ext.MessageBox.YESNO,
                                      fn: function(btn) {
                                        if (btn === "yes") {
                                          DukeSource.lib.Ajax.request({
                                            method: "POST",
                                            url:
                                              "http://localhost:9000/giro/deleteActionPlanWeakness.htm?nameView=PanelProcessRiskEvaluation",
                                            params: {
                                              idDocumentWeakness: row.get(
                                                "idDocumentWeakness"
                                              ),
                                              idDocument: row.get("idDocument"),
                                              idRisk: row.get("idRisk")
                                            },
                                            success: function(response) {
                                              response = Ext.decode(
                                                response.responseText
                                              );
                                              if (response.success) {
                                                DukeSource.global.DirtyView.messageAlert(
                                                  DukeSource.global.GiroMessages
                                                    .TITLE_MESSAGE,
                                                  response.mensaje,
                                                  Ext.Msg.INFO
                                                );
                                                grid.getStore().load();
                                              } else {
                                                DukeSource.global.DirtyView.messageAlert(
                                                  DukeSource.global.GiroMessages
                                                    .TITLE_WARNING,
                                                  response.mensaje,
                                                  Ext.Msg.ERROR
                                                );
                                              }
                                            },
                                            failure: function() {}
                                          });
                                        } else {
                                        }
                                      }
                                    });
                                  }
                                }
                              },
                              {
                                text: "Ver detalle",
                                scale: "medium",
                                hidden: true,
                                cls: "my-btn",
                                overCls: "my-over",
                                iconCls: "detail",
                                action: "viewDetailActionPlan"
                              },
                              {
                                text: "Mitigación del plan",
                                scale: "medium",
                                cls: "my-btn",
                                overCls: "my-over",
                                iconCls: "table_row_delete",
                                handler: function() {
                                  var gridActionPlan = me.down(
                                    "#gridActionPlan"
                                  );
                                  var row = gridActionPlan
                                    .getSelectionModel()
                                    .getSelection()[0];

                                  if (row === undefined) {
                                    DukeSource.global.DirtyView.messageWarning(
                                      "Seleccione un plan de acción por favor"
                                    );
                                  } else {
                                    loadReachedRisk(
                                      me,
                                      idRisk,
                                      idVersionRisk,
                                      row
                                    );
                                  }
                                }
                              }
                            ]
                          }
                        ],
                        bbar: {
                          xtype: "pagingtoolbar",
                          pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                          store: StoreGridActionPlan,
                          displayInfo: true,
                          displayMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                          emptyMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                        },
                        listeners: {
                          render: function() {
                            var me = this;
                            DukeSource.global.DirtyView.loadGridDefault(me);
                          },
                          beforerender: function(view) {
                            view.headerCt.border = 1;
                          }
                        }
                      }
                    ]
                  }
                ]
              },
              {
                xtype: "container",
                border: false,
                title: "Matrices",
                layout: {
                  type: "hbox",
                  align: "stretch"
                },
                items: [
                  {
                    xtype: "gridpanel",
                    border: false,
                    title: "Matriz - evolución",
                    titleAlign: "center",
                    style: "border-right:1px solid #99bce8",
                    flex: 1,
                    split: true,
                    name: "matrixByBusinessLineVersion",
                    itemId: "matrixByBusinessLineVersion",
                    store: StoreGridMatrixByBusinessLine,
                    loadMask: true,
                    columnLines: true,
                    columns: [],
                    tbar: [
                      {
                        text: "Recargar datos",
                        scale: "medium",
                        cls: "my-btn",
                        overCls: "my-over",
                        iconCls: "operations",
                        handler: function() {
                          var grid2 = me.down("#matrixByBusinessLineVersion");
                          grid2.productFactor = productFactor;
                          loadMatrixRisk(
                            grid2,
                            idOperationalRiskExposition,
                            me.down("#typeMatrix").getValue(),
                            "equivalentValue",
                            idRisk
                          );
                        }
                      },
                      {
                        xtype: "ViewComboGridEvaluationRisk",
                        width: 470,
                        hidden: true,
                        labelWidth: 60,
                        padding: 5,
                        name: "comboVersionRisk",
                        itemId: "comboVersionRisk",
                        fieldLabel: "Evaluación",
                        listeners: {
                          select: function(cbo) {
                            var grid2 = me.down("#matrixByBusinessLineVersion");
                            var item = cbo.findRecord(
                              cbo.valueField,
                              cbo.getValue()
                            );
                            loadMatrixRisk(
                              grid2,
                              item.data["idMaxExposition"],
                              me.down("#typeMatrix").getValue(),
                              "equivalentValue",
                              idRisk
                            );
                          }
                        }
                      }
                    ],
                    listeners: {
                      render: function() {
                        var grid2 = this;
                        grid2.productFactor = productFactor;
                        loadMatrixRisk(
                          grid2,
                          idOperationalRiskExposition,
                          me.down("#typeMatrix").getValue(),
                          "equivalentValue",
                          idRisk
                        );
                      },
                      beforerender: function(view) {
                        view.headerCt.border = 1;
                      }
                    }
                  },
                  {
                    xtype: "panel",
                    title: "Gráfica de evolución del riesgo residual",
                    titleAlign: "center",
                    style: "border-left:1px solid #99bce8",
                    split: true,
                    border: false,
                    hidden: hidden("VPER_TBP_GraphicEvolution"),
                    margin: "0 0 0 5",
                    layout: "fit",
                    flex: 1,
                    tbar: [
                      {
                        text: "Recargar datos",
                        scale: "medium",
                        cls: "my-btn",
                        overCls: "my-over",
                        iconCls: "operations",
                        handler: function() {
                          me.down("#chartColumnRisk")
                            .getStore()
                            .load();
                        }
                      }
                    ],
                    items: [
                      {
                        xtype: "chart",
                        itemId: "chartColumnRisk",
                        store: Ext.create("Ext.data.Store", {
                          fields: [
                            "descriptionCorrelative",
                            "descriptionScaleInherent",
                            "descriptionScaleResidual",
                            "inherentRiskValue",
                            "levelColourInherent",
                            "levelColourResidual",
                            "valueResidualRisk",
                            "versionCorrelative"
                          ],
                          autoLoad: true,
                          proxy: {
                            actionMethods: {
                              create: "POST",
                              read: "POST",
                              update: "POST"
                            },
                            type: "ajax",
                            url:
                              "http://localhost:9000/giro/getRiskByEvolutionGraphics.htm",
                            extraParams: {
                              idRisk: idRisk
                            },
                            reader: {
                              totalProperty: "totalCount",
                              root: "data",
                              successProperty: "success"
                            }
                          }
                        }),
                        animate: true,
                        shadow: true,
                        insetPadding: 20,
                        legend: {
                          visible: true,
                          position: "right",
                          labelFont: "8px Arial"
                        },
                        axes: [
                          {
                            type: "Numeric",
                            position: "left",
                            fields: ["inherentRiskValue", "valueResidualRisk"],
                            roundToDecimal: false,
                            label: {
                              renderer: function(s) {
                                return Ext.util.Format.number(s, "0,0");
                              }
                            },
                            grid: true
                          },
                          {
                            type: "Category",
                            position: "bottom",
                            fields: ["descriptionCorrelative"]
                          }
                        ],
                        series: [
                          {
                            type: "column",
                            label: {
                              display: "outside",
                              "text-anchor": "middle",
                              field: ["inherentRiskValue", "valueResidualRisk"],
                              renderer: function(s) {
                                return Ext.util.Format.number(s, "0,0");
                              },
                              orientation: "horizontal",
                              font: "bold 14px Arial"
                            },
                            axis: "bottom",
                            gutter: 80,
                            xField: "descriptionCorrelative",
                            yField: ["inherentRiskValue", "valueResidualRisk"],
                            title: ["RIESGO INHERENTE", "RIESGO RESIDUAL"],
                            style: {
                              opacity: 0.93
                            }
                          }
                        ]
                      }
                    ]
                  }
                ]
              },
              {
                xtype: "container",
                border: false,
                title: "Pérdidas vinculadas",
                layout: {
                  type: "hbox",
                  align: "stretch"
                },
                items: [
                  {
                    xtype: "gridpanel",
                    itemId: "riskEventsToRisk",
                    style: "border-right:1px solid #99bce8",
                    flex: 1,
                    border: false,
                    titleAlign: "center",
                    store: StorePanelEventsToRiskOperational,
                    loadMask: true,
                    columnLines: true,
                    columns: [
                      { xtype: "rownumberer", width: 25, sortable: false },
                      {
                        header: "Código",
                        align: "left",
                        dataIndex: "codeEvent",
                        width: 110,
                        renderer: function(value, metaData, record) {
                          var state;
                          var type = "";
                          metaData.tdAttr =
                            'style="background-color: #' +
                            record.get("colorEventState") +
                            ' !important; height:40px;"';
                          state =
                            '<br><span style="font-size:9px">' +
                            record.get("nameEventState") +
                            "</span>";
                          if (record.get("eventType") === "2") {
                            type =
                              '<i class="tesla even-stack2" style="margin-left:18px;line-height: 0.2 !important;"></i>';
                          } else if (record.get("eventState") === "X") {
                            metaData.tdAttr =
                              'style="background-color: #cccccc !important; height:40px;"';
                            type =
                              '<i class="tesla even-blocked-24" style="margin-left:18px;line-height: 0.2 !important;"></i>';
                          }

                          if (record.get("fileAttachment") === "S") {
                            return (
                              '<div style="display:inline-block;height:20px;width:20px;padding:2px;"><i class="tesla even-attachment"></i></div><div style="display:inline-block;position:absolute;">' +
                              '<span style="font-size: 14px;">' +
                              value +
                              type +
                              "</span> " +
                              state +
                              "</div>"
                            );
                          } else {
                            return (
                              '<div style="display:inline-block;height:20px;width:20px;padding:2px;"></div><div style="display:inline-block;position:absolute;">' +
                              '<span style="font-size: 14px;">' +
                              value +
                              type +
                              "</span> " +
                              state +
                              "</div>"
                            );
                          }
                        }
                      },
                      {
                        header: "Descripción corta",
                        align: "left",
                        dataIndex: "descriptionShort",
                        width: 200
                      },
                      {
                        header: "Pérdida total",
                        dataIndex: "totalLoss",
                        align: "center",
                        tdCls: "custom-column",
                        xtype: "numbercolumn",
                        format: "0,0.00",
                        width: 100
                      },
                      {
                        header: "Monto total recuperado",
                        dataIndex: "totalLossRecovery",
                        align: "center",
                        xtype: "numbercolumn",
                        format: "0,0.00",
                        width: 100
                      },
                      {
                        header: "Pérdida neta",
                        dataIndex: "netLoss",
                        tdCls: "custom-column",
                        align: "center",
                        xtype: "numbercolumn",
                        format: "0,0.00",
                        width: 100
                      },
                      {
                        header: "Fecha de ocurrencia",
                        align: "center",
                        dataIndex: "dateOccurrence",
                        xtype: "datecolumn",
                        format: "d/m/Y H:i",
                        width: 90,
                        renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
                      },
                      {
                        header: "Factor de riesgo",
                        align: "center",
                        dataIndex: "nameFactorRisk",
                        width: 90
                      }
                    ],
                    tbar: [
                      {
                        xtype: "button",
                        itemId: "buttonNewEventToRisk",
                        text: "Vincular",
                        scale: "medium",
                        cls: "my-btn",
                        overCls: "my-over",
                        iconCls: "add",
                        handler: function(button) {
                          var window = Ext.create(
                            "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowEventRelationRisk",
                            {
                              maximized: true
                            }
                          );
                          window.show();
                        }
                      },
                      {
                        xtype: "button",
                        itemId: "buttonDeleteEventToRisk",
                        text: "Eliminar",
                        scale: "medium",
                        cls: "my-btn",
                        overCls: "my-over",
                        iconCls: "delete",
                        action: "deleteEventToRiskInRisk"
                      }
                    ],
                    bbar: {
                      xtype: "pagingtoolbar",
                      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                      store: StorePanelEventsToRiskOperational,
                      displayInfo: true,
                      displayMsg:
                        DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                      emptyMsg:
                        DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
                      doRefresh: function() {
                        var grid = this,
                          current = grid.store.currentPage;
                        if (
                          grid.fireEvent("beforechange", grid, current) !==
                          false
                        ) {
                          grid.store.loadPage(current);
                          DukeSource.global.DirtyView.loadGridDefault(
                            me.down("#riskDebilityAssigned")
                          );
                          DukeSource.global.DirtyView.loadGridDefault(
                            me.down("#gridControlAssigned")
                          );
                        }
                      }
                    },
                    listeners: {
                      render: function() {
                        var grid = this;
                        grid.store.getProxy().url =
                          "http://localhost:9000/giro/showRelationRiskEvent.htm";
                        grid.store.getProxy().extraParams = {
                          idRisk: idRisk,
                          versionEvaluation: idVersionRisk
                        };
                        grid.down("pagingtoolbar").moveFirst();
                      },
                      itemclick: function(grid, record) {
                        var grid2 = me.down("#riskDebilityAssigned");
                        grid2.store.getProxy().extraParams = {
                          idRiskEvent: record.get("idRelationEventRisk"),
                          idRisk: record.get("idRisk"),
                          versionCorrelative: record.get("versionCorrelative")
                        };
                        grid2.store.getProxy().url =
                          "http://localhost:9000/giro/findRelationWeaknessEvent.htm";
                        grid2.down("pagingtoolbar").moveFirst();
                        DukeSource.global.DirtyView.loadGridDefault(
                          me.down("#gridControlAssigned")
                        );
                      },
                      beforerender: function(view) {
                        view.headerCt.border = 1;
                      }
                    }
                  },
                  {
                    xtype: "container",
                    style: "border-left:1px solid #99bce8",
                    border: true,
                    flex: 1,
                    margin: "0 0 0 5",
                    split: true,
                    layout: {
                      type: "vbox",
                      align: "stretch"
                    },
                    items: [
                      {
                        xtype: "gridpanel",
                        flex: 0.8,
                        itemId: "riskDebilityAssigned",
                        title: "Causas",
                        titleAlign: "center",
                        store: StoreDebilityEventAssigned,
                        loadMask: true,
                        border: false,
                        columnLines: true,
                        columns: [
                          { xtype: "rownumberer", width: 25, sortable: false },
                          {
                            header: "Código",
                            dataIndex: "codeWeakness",
                            width: 60
                          },
                          {
                            header: "Descripción",
                            dataIndex: "descriptionWeakness",
                            flex: 2
                          },
                          {
                            header: "Factor",
                            align: "center",
                            dataIndex: "descriptionFactorRisk",
                            width: 80
                          }
                        ],
                        bbar: {
                          xtype: "pagingtoolbar",
                          pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                          store: StoreDebilityEventAssigned,
                          displayInfo: true,
                          displayMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                          emptyMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
                          doRefresh: function() {
                            var grid = this,
                              current = grid.store.currentPage;
                            if (
                              grid.fireEvent("beforechange", grid, current) !==
                              false
                            ) {
                              grid.store.loadPage(current);
                              DukeSource.global.DirtyView.loadGridDefault(
                                me.down("#gridControlAssigned")
                              );
                            }
                          }
                        },
                        listeners: {
                          render: function() {
                            var grid = this;
                            DukeSource.global.DirtyView.loadGridDefault(grid);
                          },
                          itemclick: function(grid, record) {
                            var grid = me.down("#gridControlAssigned");
                            grid.store.getProxy().extraParams = {
                              idWeaknessEvent: record.get("id")
                            };
                            grid.store.getProxy().url =
                              "http://localhost:9000/giro/showRelationDetailControlEvent.htm";
                            grid.down("pagingtoolbar").moveFirst();
                          }
                        }
                      },
                      {
                        xtype: "gridpanel",
                        itemId: "gridControlAssigned",
                        flex: 1,
                        split: true,
                        border: false,
                        titleAlign: "center",
                        title: "Controles fallidos",
                        store: StoreGridPanelControlAssignetToEvent,
                        columns: [
                          {
                            xtype: "rownumberer",
                            width: 25,
                            sortable: false
                          },
                          {
                            header: "Código",
                            dataIndex: "codeControl",
                            width: 60
                          },
                          {
                            header: "CALIF.",
                            align: "center",
                            dataIndex: "nameQualification",
                            width: 90,
                            renderer: function(value, metaData, record) {
                              if (value !== " ") {
                                metaData.tdAttr =
                                  'style="background-color: #' +
                                  record.get("colorQualification") +
                                  ' !important;"';
                                return "<span>" + value + "</span>";
                              }
                            }
                          },
                          {
                            header: "Control",
                            dataIndex: "descriptionControl",
                            flex: 1
                          }
                        ],
                        bbar: {
                          xtype: "pagingtoolbar",
                          pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                          store: StoreGridPanelControlAssignetToEvent,
                          displayInfo: true,
                          displayMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                          emptyMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                        },
                        listeners: {
                          render: function() {
                            var grid = this;
                            DukeSource.global.DirtyView.loadGridDefault(grid);
                          }
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ],
        listeners: {
          activate: function(view) {
            if (!me.isVersionActive) {
              me.disableButtons(view);
            }
          }
        }
      });

      me.callParent(arguments);
    },

    riskInherent: function(me, typeMatrix) {
      var frequency = me.down("ViewComboFrequency").getValue();
      var impact = me.down("ViewComboImpact").getValue();
      if (frequency != null && impact != null) {
        DukeSource.lib.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/findEvaluationRiskInMatrix.htm",
          params: {
            frequency: frequency,
            impact: impact,
            idTypeMatrix: typeMatrix
          },
          scope: this,
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              if (response.data["ghostIndicator"] === "S") {
                DukeSource.global.DirtyView.messageWarning(
                  "EL RIESGO ES: " +
                    response.data["descriptionScaleRisk"] +
                    " - " +
                    response.data["valueMatrix"] +
                    " Seleccione otro"
                );
                me.down("#descriptionScaleRisk").reset();
                me.down("#descriptionScaleRisk").setFieldStyle(
                  "background-color:  #d9ffdb; background-image: none;color:#000000;"
                );
                me.down("#valueRiskInherentForm").reset();
                me.down("#valueRiskInherentForm").setFieldStyle(
                  "background-color: #d9ffdb; background-image: none;color:#000000;"
                );
              } else {
                me.down("#valueRiskInherentForm").setValue(
                  response.data["valueMatrix"]
                );
                me.down("#valueRiskInherentForm").setFieldStyle(
                  "background-color: #" +
                    response.data["color"] +
                    "; background-image: none;color:#000000;"
                );
                me.down("#scaleRisk").setValue(response.data["idScaleRisk"]);
                me.down("#idMatrix").setValue(response.data["idMatrix"]);
                me.down("#codeMatrix").setValue(
                  response.data["idBusinessLineOne"]
                );
                me.down("#descriptionScaleRisk").setValue(
                  response.data["descriptionScaleRisk"]
                );
                me.down("#descriptionScaleRisk").setFieldStyle(
                  "background-color: #" +
                    response.data["color"] +
                    "; background-image: none;color:#000000;"
                );
              }
            } else {
              DukeSource.global.DirtyView.messageWarning(response.message);
            }
          },
          failure: function() {}
        });
      }
    },

    disableButtons: function(me) {
      me.down("#headFormEvaluationRisk")
        .query(".button")
        .forEach(function(c) {
          c.setDisabled(true);
        });
      me.down("tabpanel")
        .query(".toolbar[dock=top], .combo")
        .forEach(function(c) {
          c.setDisabled(true);
        });
      me.down("#btnDetailControl").setDisabled(false);
    }
  }
);

function saveRelationActionPlan(win, rowDebility) {
  var recordActionPlan = win
    .down("grid")
    .getSelectionModel()
    .getSelection()[0];
  var gridAction = Ext.ComponentQuery.query(
    "ViewPanelEvaluationRiskOperational"
  )[0].down("#gridActionPlan");
  var records = gridAction.getStore().getRange();
  var total = gridAction.getStore().getCount();

  if (recordActionPlan === undefined) {
    DukeSource.global.DirtyView.messageWarning("Seleccione un plan de acción por favor");
  } else {
    var equals;
    for (var int = 0; int < total; int++) {
      if (
        records[int].get("idDocument") === recordActionPlan.get("idDocument")
      ) {
        equals = true;
        break;
      } else {
        equals = false;
      }
    }
    if (equals) {
      DukeSource.global.DirtyView.messageWarning(
        "Plan de acción asignado, seleccione otro por favor"
      );
    } else {
      DukeSource.lib.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/saveActionPlanRelationRisk.htm",
        params: {
          idRisk: rowDebility.get("idRisk"),
          idDocument: recordActionPlan.get("idDocument"),
          idWeakness: rowDebility.get("idWeakness")
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            Ext.MessageBox.show({
              title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
              msg: response.message + ", desea vincular más planes de acción ?",
              icon: Ext.Msg.QUESTION,
              buttonText: {
                yes: "Si"
              },
              buttons: Ext.MessageBox.YESNO,
              fn: function(btn) {
                if (btn !== "yes") {
                  win.close();
                }
              }
            });
            var gridActionPlan = Ext.ComponentQuery.query(
              "ViewPanelEvaluationRiskOperational"
            )[0].down("#gridActionPlan");
            gridActionPlan.store.getProxy().extraParams = {
              stateDocument: "",
              idRisk: rowDebility.get("idRisk"),
              idWeakness: rowDebility.get("idWeakness")
            };
            gridActionPlan.store.getProxy().url =
              "http://localhost:9000/giro/showDefaultFilesByStateAndIdRisk.htm?nameView=PanelProcessRiskEvaluation";
            gridActionPlan.down("pagingtoolbar").moveFirst();
          } else {
            DukeSource.global.DirtyView.messageWarning(response.message);
          }
        }
      });
    }
  }
}

function loadMatrixRisk(
  grid,
  idOperationalRiskExposition,
  idTypeMatrix,
  equivalentValue,
  idRisk
) {
  DukeSource.lib.Ajax.request({
    method: "POST",
    url: "http://localhost:9000/giro/buildHeaderAxisX.htm",
    params: {
      valueFind: idOperationalRiskExposition,
      idTypeMatrix: idTypeMatrix,
      propertyOrder: equivalentValue
    },
    success: function(response) {
      response = Ext.decode(response.responseText);
      if (response.success) {
        var columns = Ext.JSON.decode(response.data);
        var fields = Ext.JSON.decode(response.fields);
        DukeSource.global.DirtyView.createMatrix(columns, fields, grid);
        grid.store.getProxy().extraParams = {
          idOperationalRiskExposition: idOperationalRiskExposition,
          idTypeMatrix: idTypeMatrix
        };
        grid.store.getProxy().url =
          "http://localhost:9000/giro/findMatrixAndLoad.htm";
        grid.getStore().load({
          callback: function() {
            rebuildMatrixByFactor(grid, grid.productFactor);
            DukeSource.lib.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/buildMatrixByRiskVersion.htm",
              params: {
                maxExposition: idOperationalRiskExposition,
                businessLine: idTypeMatrix,
                idRisk: idRisk
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  var data1 = Ext.JSON.decode(response.data1);
                  var data2 = Ext.JSON.decode(response.data2);
                  var data3 = Ext.JSON.decode(response.data3);
                  var data4 = Ext.JSON.decode(response.data4);

                  for (var i = 0; i < data1.length; i++) {
                    var valueMatrix = data1[i]["valueMatrix"];
                    var codeRisk = data1[i]["codeRisk"];
                    var correlativeEvaluation = data1[i]["versionEvaluation"];
                    var positionX = data1[i]["positionX"] + 1;
                    var positionY = data1[i]["positionY"];
                    var cell = grid.getView().getCellByPosition({
                      row: positionY,
                      column: positionX
                    });
                    var value = cell.dom.textContent;
                    if (
                      (value = Ext.util.Format.number(valueMatrix, "0,0.00"))
                    ) {
                      cell.insertHtml(
                        "afterBegin",
                        '<button title="Riesgo Inherente" type="button" class="buttonRiskInherent">' +
                          "RI-" +
                          correlativeEvaluation +
                          "</button>",
                        true
                      );
                    }
                  }
                  for (i = 0; i < data2.length; i++) {
                    valueMatrix = data2[i]["valueMatrix"];
                    codeRisk = data2[i]["codeRisk"];
                    correlativeEvaluation = data2[i]["correlativeEvaluation"];
                    positionX = data2[i]["positionX"] + 1;
                    positionY = data2[i]["positionY"];
                    cell = grid.getView().getCellByPosition({
                      row: positionY,
                      column: positionX
                    });
                    value = cell.dom.textContent;
                    if ((value = valueMatrix.toString().concat(".00"))) {
                      cell.insertHtml(
                        "afterBegin",
                        '<button title="Riesgo Residual" type="button" class="buttonRiskResidual">' +
                          "RR-" +
                          correlativeEvaluation +
                          "</button>",
                        true
                      );
                    }
                  }
                  for (i = 0; i < data3.length; i++) {
                    valueMatrix = data3[i]["valueMatrix"];
                    codeRisk = data3[i]["codeRisk"];
                    correlativeEvaluation = data3[i]["versionEvaluation"];
                    positionX = data3[i]["positionX"] + 1;
                    positionY = data3[i]["positionY"];
                    cell = grid.getView().getCellByPosition({
                      row: positionY,
                      column: positionX
                    });
                    value = cell.dom.textContent;
                    if ((value = valueMatrix.toString().concat(".00"))) {
                      cell.insertHtml(
                        "afterBegin",
                        '<button title="Riesgo Objetivo" type="button" class="buttonTargetRisk">' +
                          "RO-" +
                          correlativeEvaluation +
                          "</button>",
                        true
                      );
                    }
                  }
                  for (i = 0; i < data4.length; i++) {
                    valueMatrix = data4[i]["valueMatrix"];
                    codeRisk = data4[i]["codeRisk"];
                    correlativeEvaluation = data4[i]["versionEvaluation"];
                    positionX = data4[i]["positionX"] + 1;
                    positionY = data4[i]["positionY"];
                    cell = grid.getView().getCellByPosition({
                      row: positionY,
                      column: positionX
                    });
                    value = cell.dom.textContent;
                    if ((value = valueMatrix.toString().concat(".00"))) {
                      cell.insertHtml(
                        "afterBegin",
                        '<button title="Riesgo alcanzado luego de la ejecución los planes de acción" type="button" class="buttonTargetRisk">' +
                          "RA-" +
                          correlativeEvaluation +
                          "</button>",
                        true
                      );
                    }
                  }
                }
              }
            });
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_ERROR,
          response.mensaje,
          Ext.Msg.ERROR
        );
        grid.getView().refresh();
      }
    },
    failure: function() {}
  });
}

function rebuildMatrixByFactor(grid, f) {
  var matrix = grid.store.data;

  for (var i = 0; i < matrix.length; i++) {
    var row = Object.values(matrix.items[i].data);

    for (var j = 0; j < row.length; j++) {
      row[j]["valueCell"] =
        (parseFloat(row[j]["valueCell"]) * parseFloat(f)) / 100;
    }
  }
  grid.getView().refresh();
}

function settingColor(a, color) {
  color = color === undefined ? "d9ffdb" : color;
  a.setFieldStyle("background-color: #" + color + ";");
}

function setColorControlAndResidual(panel, response) {
  panel.down("#scoreDescriptionControl").reset();
  panel.down("#evaluationFinalControl").reset();
  panel.down("#valueLostResidual").reset();
  panel.down("#descriptionFrequencyResidual").reset();
  panel.down("#descriptionImpactResidual").reset();

  settingColor(
    panel.down("#evaluationFinalControl"),
    response.data["colourControl"]
  );
  settingColor(
    panel.down("#valueLostResidual"),
    response.data["colourLostResidual"]
  );
  panel
    .down("#valueRiskInherent")
    .setFieldLabel("Nivel - " + response.data["descriptionScaleRisk"]);
  panel
    .down("#valueLostResidual")
    .setFieldLabel("Nivel - " + response.data["descriptionScaleResidual"]);
  panel.down("#idMatrixResidual").setValue(response.data["idMatrixResidual"]);
  panel.resumeEvaluation = response.data;
}
