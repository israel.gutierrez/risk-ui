Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.AddMenuTreatment', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.AddMenuTreatment',
    width: 150,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: []
        });

        me.callParent(arguments);
    }
});