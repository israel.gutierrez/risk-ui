Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelReportConsolidateTreatment', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelReportConsolidateTreatment',
    border: false,
    layout: 'fit',

    initComponent: function () {
        var me = this;
        this.items = [
            {
                xtype: 'container',
                name: 'generalContainer',
                border: false,
                layout: {
                    align: 'stretch',
                    type: 'border'
                },
                items: [
                    {
                        xtype: 'panel',
                        padding: '2 2 2 0',
                        flex: 3,
                        name: 'panelReport',
                        region: 'center',
                        titleAlign: 'center',
                        title: 'CONSOLIDADO DE RIESGOS POR TRATAMIENTO POR LINEAS DE NEGOCIO',
                        layout: 'fit'
                    },
                    {
                        xtype: 'panel',
                        padding: '2 0 2 2',
                        region: 'west',
                        collapseDirection: 'right',
                        collapsed: false,
                        collapsible: true,
                        split: true,
                        flex: 1.2,
                        layout: 'anchor',
                        title: 'PARAMETROS',
                        items: [
                            {
                                xtype: 'container',
                                anchor: '100%',
                                layout: {
                                    type: 'anchor'
                                },
                                padding: '5',
                                items: [
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'FECHA DESDE',
                                        allowBlank: false,
                                        fieldCls: 'obligatoryTextField',
                                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                        labelWidth: 130,
                                        format: 'Y-m-d',
                                        anchor: '100%',
                                        name: 'dateInit'
                                    },
                                    {
                                        xtype: 'datefield',
                                        fieldLabel: 'FECHA HASTA',
                                        allowBlank: false,
                                        fieldCls: 'obligatoryTextField',
                                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                        labelWidth: 130,
                                        format: 'Y-m-d',
                                        anchor: '100%',
                                        name: 'dateEnd'
                                    },
                                    {
                                        xtype: 'ViewComboTypeRiskEvaluation',
                                        flex: 1,
                                        allowBlank: false,
                                        labelWidth: 130,
                                        multiSelect: true,
                                        anchor: '100%',
                                        msgTarget: 'side',
                                        fieldCls: 'obligatoryTextField',
                                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                        fieldLabel: 'TIPO EVALUACIÓN',
                                        name: 'typeRiskEvaluation'
                                    },
                                    {
                                        xtype: 'ViewComboBusinessLineOne',
                                        fieldLabel: 'LINEA NEGOCIO 1',
                                        queryMode: 'local',
                                        listeners: {
                                            render: function (cbo) {
                                                cbo.getStore().load({
                                                        callback: function () {
                                                            cbo.store.add({
                                                                idBusinessLineOne: 'T',
                                                                description: 'TODOS'
                                                            });
                                                            cbo.setValue('T');
                                                        }
                                                    }
                                                );
                                            }
                                        },
                                        labelWidth: 130,
                                        emptyText: 'Seleccionar',
                                        forceSelection: false,
                                        anchor: '100%',
                                        name: 'businessLineOne'
                                    },
                                    {
                                        xtype: 'container',
                                        layout: {
                                            align: 'stretch',
                                            pack: 'center',
                                            padding: '10 10 10 75',
                                            type: 'hbox'
                                        },
                                        items: [
                                            {
                                                xtype: 'button',
                                                scale: 'medium',
                                                text: 'GENERAR',
                                                iconCls: 'pdf',
                                                action: 'generateReportConsolidateTreatmentPdf'
                                            },
                                            {
                                                xtype: 'button',
                                                scale: 'medium',
                                                text: 'GENERAR',
                                                iconCls: 'excel',
                                                action: 'generateReportConsolidateTreatmentXls'
                                            }
                                        ]
                                    }

                                ]
                            }
                        ]
                    }
                ]

            }
        ];
        this.callParent();
    }
});



