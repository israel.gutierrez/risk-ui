Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelReportRiskInherintResidual",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelReportRiskInherintResidual",
    border: false,
    layout: "fit",

    initComponent: function() {
      var me = this;
      this.items = [
        {
          xtype: "container",
          name: "generalContainer",
          border: false,
          layout: {
            align: "stretch",
            type: "border"
          },
          items: [
            {
              xtype: "panel",
              padding: "2 2 2 0",
              flex: 3,
              name: "panelReport",
              region: "center",
              titleAlign: "center",
              title: "RIESGOS INHRENTES/RESIDUALES POR TIPO DE PROCESO",
              layout: "fit"
            },
            {
              xtype: "panel",
              padding: "2 0 2 2",
              region: "west",
              collapseDirection: "right",
              collapsed: false,
              collapsible: true,
              split: true,
              flex: 1.2,
              layout: "anchor",
              title: "PARAMETROS",
              items: [
                {
                  xtype: "container",
                  anchor: "100%",
                  layout: {
                    type: "anchor"
                  },
                  padding: "5",
                  items: [
                    {
                      xtype: "datefield",
                      fieldLabel: "FECHA DESDE",
                      allowBlank: false,
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      labelWidth: 130,
                      format: "d/m/Y",
                      anchor: "100%",
                      name: "dateInit"
                    },
                    {
                      xtype: "datefield",
                      fieldLabel: "FECHA HASTA",
                      allowBlank: false,
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      labelWidth: 130,
                      format: "d/m/Y",
                      anchor: "100%",
                      name: "dateEnd"
                    },
                    {
                      xtype: "ViewComboTypeRiskEvaluation",
                      flex: 1,
                      allowBlank: false,
                      msgTarget: "side",
                      fieldCls: "obligatoryTextField",
                      labelWidth: 130,
                      anchor: "100%",
                      multiSelect: true,
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      fieldLabel: "TIPO EVAL",
                      name: "typeRiskEvaluation"
                    },
                    {
                      xtype: "ViewComboProcessType",
                      fieldLabel: "MACROPROCESO",
                      queryMode: "local",
                      labelWidth: 130,
                      anchor: "100%",
                      allowBlank: false,
                      msgTarget: "side",
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      name: "processType",
                      listeners: {
                        render: function(cbo) {
                          cbo.getStore().load({
                            callback: function() {
                              cbo.store.add({
                                idProcessType: "T",
                                description: "TODOS"
                              });
                            }
                          });
                        },
                        select: function(cbo) {
                          me.down("ViewComboProcess").setDisabled(false);
                          if (cbo.getValue() == "T") {
                            me.down("ViewComboProcess")
                              .getStore()
                              .removeAll();
                            me.down("ViewComboProcess").reset();
                            me.down("ViewComboProcess").store.add({
                              idProcess: "T",
                              description: "TODOS"
                            });
                          } else {
                            me.down("ViewComboProcess")
                              .getStore()
                              .load({
                                url:
                                  "http://localhost:9000/giro/showListProcessActives.htm",
                                params: {
                                  valueFind: cbo.getValue()
                                },
                                callback: function() {
                                  me.down("ViewComboProcess").reset();
                                  me.down("ViewComboProcess").store.add({
                                    idProcess: "T",
                                    description: "TODOS"
                                  });
                                  //                                                        me.down('ViewComboProcess').setValue('%');
                                }
                              });
                          }
                        }
                      }
                    },
                    {
                      xtype: "ViewComboProcess",
                      anchor: "100%",
                      fieldLabel: "PROCESO",
                      labelWidth: 130,
                      allowBlank: false,
                      msgTarget: "side",
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      disabled: true,
                      name: "process"
                    },
                    {
                      xtype: "container",
                      layout: {
                        align: "stretch",
                        pack: "center",
                        padding: "10 10 10 75",
                        type: "hbox"
                      },
                      items: [
                        {
                          xtype: "button",
                          scale: "medium",
                          text: "GENERAR",
                          iconCls: "pdf",
                          action: "generateReportRiskInheritResidualPdf"
                        },
                        {
                          xtype: "button",
                          scale: "medium",
                          text: "GENERAR",
                          iconCls: "excel",
                          action: "generateReportRiskInheritResidualXls"
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ];
      this.callParent();
    }
  }
);
