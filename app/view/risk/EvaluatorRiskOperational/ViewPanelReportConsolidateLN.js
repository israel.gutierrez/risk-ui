Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelReportConsolidateLN",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelReportConsolidateLN",
    border: false,
    layout: "fit",

    initComponent: function() {
      var me = this;
      this.items = [
        {
          xtype: "container",
          name: "generalContainer",
          border: false,
          layout: {
            align: "stretch",
            type: "border"
          },
          items: [
            {
              xtype: "panel",
              padding: "2 2 2 0",
              flex: 3,
              name: "panelReport",
              region: "center",
              layout: "fit"
            },
            {
              xtype: "form",
              padding: "2 0 2 2",
              region: "west",
              collapseDirection: "right",
              collapsed: false,
              collapsible: true,
              split: true,
              flex: 1.2,
              title: "CONSOLIDADO DE RIESGOS POR LINEA DE NEGOCIO",
              items: [
                {
                  xtype: "container",
                  layout: {
                    type: "anchor"
                  },
                  padding: "5",
                  items: [
                    {
                      xtype: "datefield",
                      fieldLabel: "FECHA EVAL. DESDE",
                      allowBlank: false,
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      labelWidth: 130,
                      format: "Y-m-d",
                      anchor: "100%",
                      name: "dateInit"
                    },
                    {
                      xtype: "datefield",
                      fieldLabel: "FECHA EVAL. HASTA",
                      allowBlank: false,
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      labelWidth: 130,
                      format: "Y-m-d",
                      anchor: "100%",
                      name: "dateEnd"
                    },
                    {
                      xtype: "ViewComboTypeRiskEvaluation",
                      flex: 1,
                      allowBlank: false,
                      labelWidth: 130,
                      anchor: "100%",
                      msgTarget: "side",
                      multiSelect: true,
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      fieldLabel: "TIPO EVALUACIÓN",
                      name: "typeRiskEvaluation"
                    },
                    {
                      xtype: "ViewComboBusinessLineOne",
                      fieldLabel: "LINEA NEGOCIO 1",
                      listeners: {
                        select: function(cbo) {
                          me.down("ViewComboBusinessLineTwo")
                            .getStore()
                            .load({
                              url:
                                "http://localhost:9000/giro/showListBusinessLineTwoActivesComboBox.htm",
                              params: {
                                valueFind: cbo.getValue()
                              }
                            });
                        }
                      },
                      labelWidth: 130,
                      emptyText: "Seleccionar",
                      forceSelection: false,
                      anchor: "100%",
                      name: "businessLineOne"
                    },
                    {
                      xtype: "ViewComboBusinessLineTwo",
                      fieldLabel: "LINEA NEGOCIO 2",
                      labelWidth: 130,
                      emptyText: "Seleccionar",
                      forceSelection: false,
                      anchor: "100%",
                      name: "businessLineTwo"
                    },
                    {
                      xtype: "container",
                      layout: {
                        align: "stretch",
                        pack: "center",
                        padding: "10 10 10 80",
                        type: "hbox"
                      },
                      items: [
                        {
                          xtype: "button",
                          scale: "medium",
                          text: "GENERAR",
                          iconCls: "pdf",
                          action: "generateReportConsolidateLN"
                        },
                        {
                          xtype: "button",
                          scale: "medium",
                          text: "GENERAR",
                          iconCls: "excel",
                          action: "generateReportRiskConsolidateLNXls"
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ];
      this.callParent();
    }
  }
);
