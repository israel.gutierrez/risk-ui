Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelReportFormatARO",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelReportFormatARO",
    border: false,
    layout: "fit",

    initComponent: function() {
      var me = this;
      this.items = [
        {
          xtype: "container",
          name: "generalContainer",
          itemId: "generalContainer",
          border: false,
          layout: {
            align: "stretch",
            type: "border"
          },
          items: [
            {
              xtype: "panel",
              padding: "2 2 2 0",
              flex: 3,
              name: "panelReport",
              region: "center",
              layout: "fit"
            },
            {
              xtype: "form",
              padding: "2 0 2 2",
              region: "west",
              split: true,
              flex: 1,
              height: 400,
              titleAlign: "center",
              layout: "anchor",
              title: "REPORTE FORMATO ARO",
              items: [
                {
                  xtype: "container",
                  anchor: "100%",
                  layout: {
                    type: "anchor"
                  },
                  fieldDefaults: {
                    labelCls: "changeSizeFontToEightPt",
                    fieldCls: "changeSizeFontToEightPt"
                  },
                  padding: "5",
                  items: [
                    {
                      xtype: "combobox",
                      allowBlank: false,
                      queryMode: "local",
                      editable: false,
                      typeAhead: true,
                      forceSelection: true,
                      msgTarget: "side",
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      anchor: "100%",
                      fieldLabel: "MAXIMA EXPOSICION",
                      name: "idMaxExposition",
                      itemId: "idMaxExposition",
                      flex: 1,
                      labelWidth: 130,
                      displayField: "maxAmount",
                      valueField: "idOperationalRiskExposition",
                      fieldStyle: "text-transform:uppercase",
                      store: {
                        fields: ["idOperationalRiskExposition", "maxAmount"],
                        proxy: {
                          actionMethods: {
                            create: "POST",
                            read: "POST",
                            update: "POST"
                          },
                          type: "ajax",
                          url:
                            "http://localhost:9000/giro/findOperationalRiskExposition.htm",
                          extraParams: {
                            propertyOrder: "validity",
                            valueFind: "S",
                            propertyFind: "state"
                          },
                          reader: {
                            type: "json",
                            root: "data",
                            successProperty: "success"
                          }
                        }
                      },
                      listeners: {
                        render: function(cbo) {
                          cbo.getStore().load();
                        },
                        specialkey: function(c, d) {
                          DukeSource.global.DirtyView.focusEventEnterObligatory(
                            c,
                            d,
                            me.down("#idRiskEvaluationMatrix")
                          );
                        }
                      }
                    },
                    {
                      xtype: "combobox",
                      allowBlank: false,
                      queryMode: "local",
                      editable: false,
                      typeAhead: true,
                      forceSelection: true,
                      msgTarget: "side",
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      anchor: "100%",
                      fieldLabel: "EVALUACIÓN",
                      name: "idRiskEvaluationMatrix",
                      itemId: "idRiskEvaluationMatrix",
                      flex: 1,
                      labelWidth: 130,
                      displayField: "nameEvaluation",
                      valueField: "idRiskEvaluationMatrix",
                      fieldStyle: "text-transform:uppercase",
                      store: {
                        fields: ["idRiskEvaluationMatrix", "nameEvaluation"],
                        proxy: {
                          actionMethods: {
                            create: "POST",
                            read: "POST",
                            update: "POST"
                          },
                          type: "ajax",
                          url:
                            "http://localhost:9000/giro/showListRiskEvaluationMatrixActives.htm",
                          extraParams: {
                            propertyOrder: "description"
                          },
                          reader: {
                            type: "json",
                            root: "data",
                            successProperty: "success"
                          }
                        }
                      },
                      listeners: {
                        render: function(cbo) {
                          cbo.getStore().load({
                            callback: function() {
                              cbo.store.add({
                                idRiskEvaluationMatrix: "T",
                                nameEvaluation: "TODOS"
                              });
                              cbo.setValue("T");
                            }
                          });
                        },
                        specialkey: function(c, d) {
                          DukeSource.global.DirtyView.focusEventEnterObligatory(
                            c,
                            d,
                            me.down("#workArea")
                          );
                        }
                      }
                    },
                    {
                      xtype: "datefield",
                      fieldLabel: "FECHA EVAL. DESDE:",
                      allowBlank: false,
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      labelWidth: 130,
                      format: "d/m/Y",
                      anchor: "100%",
                      name: "dateInit"
                    },
                    {
                      xtype: "datefield",
                      fieldLabel: "FECHA EVAL. HASTA:",
                      allowBlank: false,
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      labelWidth: 130,
                      format: "d/m/Y",
                      anchor: "100%",
                      name: "dateEnd"
                    },
                    {
                      xtype: "ViewComboTypeRiskEvaluation",
                      flex: 1,
                      allowBlank: false,
                      multiSelect: true,
                      msgTarget: "side",
                      fieldCls: "obligatoryTextField",
                      labelWidth: 130,
                      anchor: "100%",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      fieldLabel: "TIPO EVAL",
                      name: "typeRiskEvaluation"
                    },
                    {
                      xtype: "ViewComboProcessType",
                      labelWidth: 130,
                      anchor: "100%",
                      fieldLabel: "MACROPROCESO",
                      emptyText: "Seleccionar",
                      name: "processType",
                      hidden: hidden("RPCBProcessType"),
                      forceSelection: false,
                      listeners: {
                        render: function(cbo) {
                          cbo.getStore().load();
                        },
                        select: function(cbo) {
                          me.down("ViewComboProcess")
                            .getStore()
                            .load({
                              url:
                                "http://localhost:9000/giro/showListProcessActives.htm",
                              params: {
                                valueFind: cbo.getValue()
                              },
                              callback: function() {
                                me.down("ViewComboProcess").setDisabled(false);
                                me.down("ViewComboProcess").reset();
                              }
                            });
                        }
                      }
                    },
                    {
                      xtype: "ViewComboProcess",
                      labelWidth: 130,
                      anchor: "100%",
                      fieldLabel: "PROCESO",
                      emptyText: "Seleccionar",
                      forceSelection: false,
                      msgTarget: "side",
                      name: "process",
                      hidden: hidden("RPCBProcess")
                    },
                    {
                      xtype: "ViewComboBusinessLineOne",
                      fieldLabel: "LINEA NEGOCIO 1",
                      listeners: {
                        select: function(cbo) {
                          me.down("ViewComboBusinessLineTwo")
                            .getStore()
                            .load({
                              url:
                                "http://localhost:9000/giro/showListBusinessLineTwoActivesComboBox.htm",
                              params: {
                                valueFind: cbo.getValue()
                              },
                              callback: function() {
                                me.down("ViewComboBusinessLineTwo").reset();
                              }
                            });
                        }
                      },
                      hidden: hidden("RPCBBusinessLineOne"),
                      labelWidth: 130,
                      emptyText: "Seleccionar",
                      forceSelection: false,
                      anchor: "100%",
                      name: "businessLineOne"
                    },
                    {
                      xtype: "ViewComboBusinessLineTwo",
                      fieldLabel: "LINEA NEGOCIO 2",
                      labelWidth: 130,
                      emptyText: "Seleccionar",
                      forceSelection: false,
                      anchor: "100%",
                      hidden: hidden("RPCBBusinessLineTwo"),
                      name: "businessLineTwo"
                    }
                  ]
                }
              ],
              bodyPadding: 5,
              tbar: [
                {
                  xtype: "button",
                  scale: "medium",
                  text: "EXCEL PLANO",
                  iconCls: "excel",
                  action: "generateReportFormatPlainXls"
                },
                {
                  xtype: "button",
                  scale: "medium",
                  text: "FORMATO ARO",
                  iconCls: "excel",
                  action: "generateReportFormatAROXls"
                }
              ]
            }
          ]
        }
      ];
      this.callParent();
    }
  }
);
