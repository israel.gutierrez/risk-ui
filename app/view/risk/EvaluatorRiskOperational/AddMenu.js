Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.AddMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.AddMenu',
    width: 150,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
//                {
//                    xtype: 'menuitem',
//                    text: 'Analisis / Exposicion',
//                    iconCls: 'add',
//                    handler:function(grid){
//
//                }}
//                {
//                    xtype:'menuitem',
//                    text:'Evaluar',
//                    handler:function(){
//                        var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowVersionEvaluationRisk',{
//                            modal:true//,
//                        }).show();
//                    }
//                },
//                {
//                    xtype: 'menuitem',
//                    text: 'Debilidad',
//                    iconCls: 'add',
//                    handler:function(){
//                        var panel = Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0];
////                        var gridDebility = panel.down('grid[name=riskDebility]');
//                        var grid = panel.down('grid[name=riskIdentify]');
//                            var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDebilityResume',{
//                                modal:true//,
//                            }).show();
//                        window.down('grid').store.getProxy().extraParams = {idRisk:grid.getSelectionModel().getSelection()[0].get('idRisk')};
//                        window.down('grid').store.getProxy().url = 'findRiskWeaknessDetail.htm';
//                        window.down('grid').down('pagingtoolbar').moveFirst();
//
//
//                    }
//                },
//                {
//                    xtype: 'menuitem',
//                    text: 'Tratamiento',
//                    iconCls: 'modify'
//                },
//                {
//                    xtype: 'menuitem',
//                    text: 'Mstriz por riesgo',
//                    iconCls: 'modify',
//                    handler:function(){
//                        var panel = Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0];
////                        var gridDebility = panel.down('grid[name=riskDebility]');
//                        var grid = panel.down('grid[name=riskIdentify]');
//                        var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowMatrixByRisk',{
//                            modal:true//,
//                        }).show();
//                        window.down('grid').store.getProxy().extraParams = {idRisk:grid.getSelectionModel().getSelection()[0].get('idRisk')};
//                        window.down('grid').store.getProxy().url = 'getMatrix.htm';
//                        window.down('grid').down('pagingtoolbar').moveFirst();
//
//
//                    }
//                },
//                {
//                    xtype: 'menuitem',
//                    text: 'Modificar',
//                    iconCls: 'modify'
//                },
//                {
//                    xtype: 'menuitem',
//                    text: 'Eliminar',
//                    iconCls: 'delete'
//                }

            ]
        });

        me.callParent(arguments);
    }
});