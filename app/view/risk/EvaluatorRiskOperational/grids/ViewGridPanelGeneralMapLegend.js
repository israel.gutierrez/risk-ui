Ext.require(['Ext.ux.ColorField']);
Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.grids.ViewGridPanelGeneralMapLegend', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ViewGridPanelGeneralMapLegend',
    store: 'risk.EvaluatorRiskOperational.grids.StoreGridPanelMapGeneralLegend',
    loadMask: true,
    columnLines: true,
    bbar: {
        xtype: 'pagingtoolbar',
        pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
        store: 'risk.EvaluatorRiskOperational.grids.StoreGridPanelMapGeneralLegend',
        displayInfo: true,
        displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
        items: ['-', {action: 'exportScaleRiskPdf', iconCls: 'pdf'}, '-', {
            action: 'exportScaleRiskExcel',
            iconCls: 'excel'
        }, '-', {
            xtype:"UpperCaseTrigger",
            fieldLabel: 'FILTRAR',
            action: 'searchTriggerGridScaleRisk',
            labelWidth: 60,
            width: 300
        }],
        emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {},
    initComponent: function () {
        this.plugins = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToMoveEditor: 1,
            saveBtnText: 'GUARDAR',
            cancelBtnText: 'CANCELAR',
            autoCancel: false,
            completeEdit: function () {
                var me = this;
                var grid = me.grid;
                var selModel = grid.getSelectionModel();
                var record = selModel.getLastSelected();
                if (me.editing && me.validateEdit()) {
                    me.editing = false;
                    me.fireEvent('edit', me, me.context);
                }
            }
        });
        this.columns = [
            {xtype: 'rownumberer', width: 25, sortable: false}
            ,
            {
                header: 'NIVEL DE CALIFICACION', dataIndex: 'descriptionRisk', align: 'center', flex: 1.5, width: 40,
                renderer: function (value, metaData, record) {
                    metaData.tdAttr = '<span style="font-size:13px;color:#eeeeee;text-align:center;background-color: #' + record.get('colourInherent') + '"> ' + record.get('descriptionRisk') + '</span';
                }
            }
            ,
            {
                header: 'NUMERO DE RIESGOS', dataIndex: 'valueInherent', align: 'center', flex: 1.5, width: 40,
                renderer: function (value, metaData, record) {
                    metaData.tdAttr = '<span style="font-size:12px;text-align:center;"> ' + record.get('valueInherent') + '</span';
                }
            }
            ,
            {
                header: '% CONCENTRACION', align: 'center', dataIndex: 'valueResidual', flex: 1.5,
                xtype: 'numbercolumn', format: '0,0.00',
                renderer: function (value, metaData, record) {
                    metaData.tdAttr = '<span style="font-size:12px;text-align:center;"> ' + record.get('valueResidual') + '%' + '</span';
                }
            }
        ];
        this.callParent(arguments);
    }
});

