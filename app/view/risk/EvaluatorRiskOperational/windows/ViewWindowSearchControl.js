Ext.define("ModelSearchControl", {
  extend: "Ext.data.Model",
  fields: [
    "idControl",
    "idProcess",
    "jobPlace",
    "jobPlaceDescription",
    "idSubProcess",
    "idManagerRisk",
    "descriptionControl",
    "typeProcess",
    "fullName",
    "nameIdProcess",
    "nameIdSubProcess",
    "nameTypeProcess",
    "codeControl",
    "state",
    "stateEvaluated",
    "typeControl",
    "stateEvaluatedControl",
    "checkEvidence",
    "descriptionEvaluation",
    "colorQualification",
    "valueScoreControl"
  ]
});

var storeSearchControl = Ext.create("Ext.data.Store", {
  model: "ModelSearchControl",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowSearchControl",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowSearchControl",
    layout: {
      align: "stretch",
      type: "vbox"
    },
    anchorSize: 100,
    title: "BUSCAR CONTROL",
    titleAlign: "center",
    width: 900,
    height: 550,
    tbar: [
      {
        xtype: "button",
        text: "Nuevo",
        iconCls: "add",
        scale: "medium",
        cls: "my-btn",
        overCls: "my-over",
        handler: function() {
          Ext.create(
            "DukeSource.view.risk.parameter.controls.ViewWindowRegisterControl",
            {
              modal: true
            }
          ).show();
        }
      },
      {
        xtype: "button",
        text: "Evaluar",
        iconCls: "evaluate",
        scale: "medium",
        cls: "my-btn",
        overCls: "my-over",
        action: "evaluateControlRisk"
      }
    ],
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            height: 45,
            padding: "2 2 2 2",
            bodyPadding: 10,
            items: [
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "combobox",
                    value: "2",
                    labelWidth: 40,
                    width: 190,
                    fieldLabel: "TIPO",
                    store: [
                      ["1", "Código"],
                      ["2", "Descripción"]
                    ]
                  },
                  {
                    xtype:"UpperCaseTextField",
                    flex: 2,
                    listeners: {
                      afterrender: function(field) {
                        field.focus(false, 200);
                      },
                      specialkey: function(field, e) {
                        var property = "";
                        if (
                          Ext.ComponentQuery.query(
                            "ViewWindowSearchControl combobox"
                          )[0].getValue() == "1"
                        ) {
                          property = "c.codeControl";
                        } else {
                          property = "c.description";
                        }
                        if (e.getKey() === e.ENTER) {
                          var grid = me.down("grid");
                          var toolbar = grid.down("pagingtoolbar");
                          DukeSource.global.DirtyView.searchPaginationGridToEnter(
                            field,
                            grid,
                            grid.down("pagingtoolbar"),
                            "http://localhost:9000/giro/findControl.htm",
                            property,
                            "c.idControl"
                          );
                        }
                      }
                    }
                  }
                ]
              }
            ]
          },
          {
            xtype: "container",
            flex: 3,
            padding: 2,
            layout: {
              align: "stretch",
              type: "hbox"
            },
            items: [
              {
                xtype: "gridpanel",
                padding: "0 1 0 0",
                store: storeSearchControl,
                flex: 1,
                titleAlign: "center",
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    dataIndex: "codeControl",
                    width: 100,
                    align: "center",
                    text: "Código"
                  },
                  {
                    header: "Calificación",
                    align: "center",
                    dataIndex: "descriptionEvaluation",
                    hidden: hidden("WRCDescriptionEvaluation"),
                    width: 100,
                    renderer: function(value, metaData, record) {
                      metaData.tdAttr =
                        'style="background-color: #' +
                        record.get("colorQualification") +
                        ' !important;"';
                      return (
                        record.get("descriptionEvaluation") +
                        " - " +
                        record.get("valueScoreControl")
                      );
                    }
                  },
                  {
                    dataIndex: "descriptionControl",
                    flex: 5,
                    text: "Descripción"
                  },
                  {
                    dataIndex: "jobPlaceDescription",
                    flex: 2,
                    text: "Responsable"
                  },
                  {
                    dataIndex: "stateEvaluated",
                    width: 100,
                    text: "Estado",
                    renderer: function(value, meta) {
                      if (value === "N") {
                        return (
                          '<span style="color:red;">' +
                          "Sin evaluar" +
                          "</span>"
                        );
                      } else if (value === "S") {
                        return "Evaluado";
                      }
                    }
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: storeSearchControl,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  render: function() {
                    var me = this;
                    DukeSource.global.DirtyView.searchPaginationGridNormal(
                      "%",
                      me,
                      me.down("pagingtoolbar"),
                      "http://localhost:9000/giro/findControl.htm",
                      "c.description",
                      "c.idControl"
                    );
                  }
                }
              }
            ]
          }
        ],
        buttonAlign: "center",
        buttons: [
          {
            text: "Salir",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
