Ext.define('ModelUserActives', {
    extend: 'Ext.data.Model',
    fields: ['idFeaturesProcess'
        , 'descriptionFeaturesProcess'
        , 'idValueFeaturesProcess'
        , 'value'
        , 'typeItem'
    ]
});

var storeUserActives = Ext.create('Ext.data.Store', {
    model: 'ModelUserActives',
    proxy: {
        actionMethods: {
            create: 'POST',
            read: 'POST',
            update: 'POST'
        },
        type: 'ajax',
        url: 'http://localhost:9000/giro/loadGridDefault.htm',
        reader: {
            totalProperty: 'totalCount',
            root: 'data',
            successProperty: 'success'
        }
    }
});

Ext.define('ModelComboFeatureProcess', {
    extend: 'Ext.data.Model',
    fields: [
        'idValuationFeatureProcess',
        'description',
        'valueFeature'
    ]
});
var StoreComboFeatureProcess = Ext.create('Ext.data.Store', {
    extend: 'Ext.data.Store',
    model: 'ModelComboFeatureProcess',
    pageSize: 9999,
    autoLoad: false,
    proxy: {
        actionMethods: {
            create: 'POST',
            read: 'POST',
            update: 'POST'
        },
        type: 'ajax',
        url: 'http://localhost:9000/giro/findValuationFeatureProcess.htm',
        extraParams: {
            valueFind: '1'
        },
        reader: {
            type: 'json',
            root: 'data',
            successProperty: 'success'
        }
    }
});

//Ext.define('ModelComboFeatureProcess',{
//    extend: 'Ext.data.Model',
//    fields: [
//        'idValuationFeatureProcess',
//        'description',
//        'valueFeature'
//    ]
//});

Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowAnalysisExhibition', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowAnalysisExhibition',
    height: 523,
    width: 750,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'AN&Aacute;LISIS - EXPOSICI&Oacute;N INHERENTE',
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    height: 171,
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'EVALUACI&Oacute;N DE SUB-PROCESO',
                            items: [
                                {
                                    xtype: 'container',
                                    flex: 3,
                                    padding: '2 2 2 2',
                                    layout: {
                                        align: 'stretch',
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'gridpanel',
                                            padding: '0 1 0 0',
                                            height: 170,
                                            store: storeUserActives,
                                            flex: 1,
                                            titleAlign: 'center',
                                            plugins: [
                                                Ext.create('Ext.grid.plugin.CellEditing', {
                                                    clicksToEdit: 1,
                                                    listeners: {
                                                        'beforeedit': function (e) {
                                                            var Frequency = Ext.getStore(StoreComboFeatureProcess);
                                                            Frequency.loadData([], false);
                                                            var store = Frequency.load({
                                                                url: 'http://localhost:9000/giro/findValuationFeatureProcess.htm',
                                                                params: {
                                                                    valueFind: me.down('grid').getSelectionModel().getSelection()[0].get('idFeaturesProcess')
                                                                }
                                                            });
                                                        },
                                                        'edit': function (e) {
                                                        }
                                                    }
                                                })
                                            ],
                                            columns: [
                                                {
                                                    xtype: 'rownumberer',
                                                    width: 25,
                                                    sortable: false
                                                },
                                                {
                                                    dataIndex: 'idFeaturesProcess',
                                                    width: 120,
                                                    text: 'C&Oacute;DIGO'
                                                },
                                                {
                                                    dataIndex: 'descriptionFeaturesProcess',
                                                    flex: 1,
                                                    text: 'CARACTER&Iacute;STICA'
                                                },
                                                {
                                                    header: 'VALOR CARACTER&Iacute;STICA',
                                                    dataIndex: 'idValueFeaturesProcess',
                                                    flex: 1,
                                                    editor: {
                                                        xtype: 'ViewComboFeatureProcess',
                                                        store: StoreComboFeatureProcess,
                                                        flex: 1,
                                                        msgTarget: 'side',
                                                        fieldCls: 'obligatoryTextField',
                                                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                        allowBlank: false,
                                                        listeners: {
                                                            select: function (cbo, record) {
                                                                me.down('grid').getSelectionModel().getSelection()[0].set('value', record[0].data['valueFeature']);
                                                                calification(me, me.down('grid'));
                                                            }
                                                        }
                                                    },
                                                    renderer: function (val, metadata, record) {

                                                        var Frequency = Ext.getStore(StoreComboFeatureProcess);
                                                        var index = Frequency.findExact('idValuationFeatureProcess', val);
                                                        if (index != -1) {
                                                            var rs = Frequency.getAt(index).data;
                                                            return rs.description;
                                                        }
                                                    }
                                                },
                                                {
                                                    header: 'CALIFICACI&Oacute;N',
                                                    dataIndex: 'value'

                                                }
                                            ],
                                            bbar: {
                                                xtype: 'pagingtoolbar',
                                                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                                                store: storeUserActives,
                                                displayInfo: true,
                                                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                                                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                                            }

                                        }
                                    ]
                                },
                                {
                                    xtype: 'form',
                                    height: 45,
//                                    padding: '2 2 2 2',
                                    bodyPadding: 10,
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            itemId: 'id',
                                            name: 'id',
                                            hidden: true
                                        },
                                        {
                                            xtype: 'container',
                                            anchor: '100%',
                                            height: 26,
                                            layout: {
                                                type: 'hbox'
                                            },
                                            items: [
                                                {
                                                    xtype: 'ViewComboProcessQualification',
                                                    fieldCls: 'readOnlyText',
                                                    readOnly: true,
                                                    name: 'idProcessQualification',
                                                    padding: '0 10 0 0',
                                                    flex: 1.5,
                                                    fieldLabel: 'CALIFICACI&Oacute;N'
                                                },
                                                {
                                                    xtype:"UpperCaseTextFieldReadOnly",
                                                    name: 'valueQualification',
                                                    flex: 1
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },

                        {
                            xtype: 'fieldset',
                            title: 'EXPOSICI&Oacute;N INHERENTE',
                            items: [
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: {
                                        align: 'top',
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'container',
                                            flex: 1,
                                            height: 26,
                                            layout: {
                                                type: 'hbox'
                                            },
                                            items: [
                                                {
                                                    xtype: 'ViewComboFrequency',
                                                    flex: 1,
                                                    allowBlank: false,
                                                    msgTarget: 'side',
                                                    fieldCls: 'obligatoryTextField',
                                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                    padding: '0 5 0 0',
//                                                    labelWidth:'74',
                                                    fieldLabel: 'FRECUENCIA',
                                                    name: 'idFrequency',
                                                    listeners: {
                                                        select: function (cbo, record) {
                                                            cbo.up('window').down('UpperCaseTextFieldReadOnly[name=percentageFrequency]').setValue(record[0].data['percentage']);
                                                            riskInherint(cbo);
                                                            var panel = cbo.up('window');
//                                                            var comboFrequency = panel.down('ViewComboDetailFrequencyFeature');
                                                            panel.down('ViewComboFrequencyFeature').reset();
                                                            panel.down('UpperCaseTextArea[name=descriptionFrequencyFeature]').reset();

//                                                            comboFrequency.getStore().load({
//                                                                url: 'findDetailFrequencyFeature.htm',
//                                                                params:{
//                                                                    valueFind:cbo.getValue(),
//                                                                    propertyFind:'id.idFrequency',
//                                                                    propertyOrder:'description'
//                                                                },
//                                                                callback: function(cbo) {
//                                                                    comboFrequency.reset();
//                                                                }
//                                                            });
//                                                            comboFrequency.setDisabled(false);
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype:"UpperCaseTextFieldReadOnly",
                                                    name: 'percentageFrequency',
                                                    width: 40
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            flex: 1,
                                            height: 26,
                                            layout: {
                                                type: 'hbox'
                                            },
                                            items: [
                                                {
                                                    xtype: 'ViewComboImpact',
                                                    allowBlank: false,
                                                    msgTarget: 'side',
                                                    fieldCls: 'obligatoryTextField',
                                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                    flex: 1,
//                                                    labelWidth:'74',
                                                    padding: '0 5 0 5',
                                                    fieldLabel: 'IMPACTO',
                                                    name: 'idImpact',
                                                    listeners: {
                                                        select: function (cbo, record) {
                                                            cbo.up('window').down('UpperCaseTextFieldReadOnly[name=percentageImpact]').setValue(record[0].data['percentage']);
                                                            riskInherint(cbo);
                                                            var panel = cbo.up('window');
//                                                            var comboImpact = panel.down('ViewComboDetailImpactFeature');
                                                            panel.down('ViewComboImpactFeature').reset();
                                                            panel.down('UpperCaseTextArea[name=descriptionImpactFeature]').reset();
//                                                            comboImpact.getStore().load({
//                                                                url: 'findDetailImpactFeature.htm',
//                                                                params:{
//                                                                    valueFind:cbo.getValue(),
//                                                                    propertyFind:'id.idImpact',
//                                                                    propertyOrder:'description'
//                                                                },
//                                                                callback: function(cbo) {
//                                                                    comboImpact.reset();
//                                                                }
//                                                            });
//                                                            comboImpact.setDisabled(false);
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype:"UpperCaseTextFieldReadOnly",
                                                    name: 'percentageImpact',
                                                    width: 40
                                                }
                                            ]
                                        }
//                                        ,
//                                        {
//                                            xtype: 'ViewComboScaleRisk',
//                                            flex: 1,
//                                            padding: '0 0 0 5',
//                                            fieldLabel: 'RI.INHERENTE'
//                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: {
                                        align: 'top',
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'container',
                                            flex: 1,
                                            height: 26,
                                            layout: {
                                                type: 'hbox'
//                                                ,
//                                                align: 'stretch'
                                            },
                                            items: [
                                                {
                                                    xtype: 'ViewComboFrequencyFeature',
                                                    flex: 1,
                                                    allowBlank: false,
                                                    msgTarget: 'side',
                                                    fieldCls: 'obligatoryTextField',
                                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
//                                                    padding: '0 5 0 0',
//                                                    labelWidth:'74',
                                                    fieldLabel: 'CRITERIO FREC.',
//                                                    disabled:true,
                                                    name: 'idFrequencyFeature',
                                                    listeners: {
                                                        select: function (cbo) {
                                                            Ext.Ajax.request({
                                                                method: 'POST',
                                                                url: 'http://localhost:9000/giro/findDescriptionByIdDetailFrequencyFeature.htm',
                                                                params: {
                                                                    idFrequency: cbo.up('window').down('ViewComboFrequency').getValue(),
                                                                    idFrequencyFeature: cbo.getValue()
                                                                },
                                                                scope: this,
                                                                success: function (response) {
                                                                    response = Ext.decode(response.responseText);
                                                                    if (response.success) {
                                                                        cbo.up('window').down('UpperCaseTextArea[name=descriptionFrequencyFeature]').setValue(response.data);
                                                                    } else {
                                                                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, response.mensaje, Ext.Msg.ERROR);

                                                                    }
                                                                },
                                                                failure: function () {
                                                                }
                                                            });
                                                        }
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            flex: 1,
                                            height: 26,
                                            layout: {
                                                type: 'hbox'
                                            },
                                            items: [
                                                {
                                                    xtype: 'ViewComboImpactFeature',
                                                    allowBlank: false,
                                                    msgTarget: 'side',
                                                    fieldCls: 'obligatoryTextField',
                                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                    flex: 1,
//                                                    labelWidth:'74',
                                                    padding: '0 0 0 5',
                                                    fieldLabel: 'CRITERIO IMP.',
//                                                    disabled:true,
                                                    name: 'idImpactFeature',
                                                    listeners: {
                                                        select: function (cbo) {
                                                            Ext.Ajax.request({
                                                                method: 'POST',
                                                                url: 'http://localhost:9000/giro/findDescriptionByIdDetailImpactFeature.htm',
                                                                params: {
                                                                    idImpact: cbo.up('window').down('ViewComboImpact').getValue(),
                                                                    idImpactFeature: cbo.getValue()
                                                                },
                                                                scope: this,
                                                                success: function (response) {
                                                                    response = Ext.decode(response.responseText);
                                                                    if (response.success) {
                                                                        cbo.up('window').down('UpperCaseTextArea[name=descriptionImpactFeature]').setValue(response.data);
                                                                    } else {
                                                                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, response.mensaje, Ext.Msg.ERROR);

                                                                    }
                                                                },
                                                                failure: function () {
                                                                }
                                                            });
                                                        }
                                                    }
                                                }
                                            ]
                                        }
//                                        ,
//                                        {
//                                            xtype: 'ViewComboScaleRisk',
//                                            flex: 1,
//                                            padding: '0 0 0 5',
//                                            fieldLabel: 'RI.INHERENTE'
//                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    height: 65,
                                    layout: {
                                        align: 'top',
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'container',
                                            flex: 1,
//                                            height: 40,
                                            layout: {
                                                type: 'hbox'
//                                                align: 'stretch'
                                            },
                                            items: [
                                                {
                                                    xtype: 'UpperCaseTextArea',
                                                    flex: 1,
                                                    height: 60,
                                                    readOnly: true,
//                                                    allowBlank:false,
//                                                    padding: '0 5 0 0',
//                                                    labelWidth:'74',
                                                    fieldLabel: 'DESCRIPCI&Oacute;N',
//                                                    disabled:true,
                                                    name: 'descriptionFrequencyFeature'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            flex: 1,
//                                            height: 40,
                                            layout: {
                                                type: 'hbox'
                                            },
                                            items: [
                                                {
                                                    xtype: 'UpperCaseTextArea',
//                                                    allowBlank:false,
                                                    readOnly: true,
                                                    height: 60,
                                                    flex: 1,
//                                                    labelWidth:'74',
                                                    padding: '0 0 0 5',
                                                    fieldLabel: 'DESCRIPCI&Oacute;N',
//                                                    disabled:true,
                                                    name: 'descriptionImpactFeature'
                                                }
                                            ]
                                        }
//                                        ,
//                                        {
//                                            xtype: 'ViewComboScaleRisk',
//                                            flex: 1,
//                                            padding: '0 0 0 5',
//                                            fieldLabel: 'RI.INHERENTE'
//                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    height: 26,
                                    layout: {
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'UpperCaseTextFieldObligatory',
//                                            allowBlank:false,
//                                            msgTarget: 'side',
//                                            fieldCls:'obligatoryTextField',
//                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            readOnly: true,
                                            flex: 1,
                                            name: 'descriptionScaleRisk',
//                                            padding: '0 5 0 0',
                                            fieldLabel: 'RI.INHERENTE'
                                        },
                                        {
                                            xtype: 'NumberDecimalNumberObligatory',
                                            padding: '0 0 0 5',
                                            name: 'valueLost',
                                            flex: 1
                                        },
                                        {
                                            xtype: 'textfield',
                                            name: 'codeMatrix',
                                            hidden: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            name: 'idMatrix',
                                            hidden: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            name: 'scaleRisk',
                                            hidden: true
                                        }
//                                        {
//                                            xtype: 'NumericField',
//                                            readOnly:true,
//                                            name:'valueRiskInherent',
//                                            decimalPrecision: 2,
//                                            alwaysDisplayDecimals: true,
//                                            flex: 1//,
////                                            width:30
//                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    action: 'saveAnalisysExposition',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });

        me.callParent(arguments);
    }

});

function calification(window, grid) {
    var comboQualification = window.down('ViewComboProcessQualification');
    var recordss = grid.getStore().getRange();
    var calf = 0;
    var total = grid.getStore().getCount();
    for (var int = 0; int < total; int++) {
        calf = calf + recordss[int].get('value') * 1;
    }
    var valueCombo = Math.round((calf) / 5);
    for (var i = 0; i < comboQualification.store.data.items.length; i++) {
        if (valueCombo == comboQualification.store.data.items[i].data.valueQualification) {
            comboQualification.setValue(comboQualification.store.data.items[i].data.idProcessQualification);
            window.down('UpperCaseTextFieldReadOnly[name=valueQualification]').setValue(valueCombo + '');
        }
    }
};

function riskInherint(cbo) {
    //
    var frequency = cbo.up('window').down('ViewComboFrequency').getValue();
    var impact = cbo.up('window').down('ViewComboImpact').getValue();
    if (frequency != null && impact != null) {
        Ext.Ajax.request({
            method: 'POST',
            url: 'http://localhost:9000/giro/findEvaluationRiskInMatrix.htm',
            params: {
                frequency: frequency,
                impact: impact
            },
            scope: this,
            success: function (response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                    cbo.up('window').down('NumberDecimalNumberObligatory').setValue(response.data['valueMatrix']);
                    cbo.up('window').down('textfield[name=scaleRisk]').setValue(response.data['idScaleRisk']);
                    cbo.up('window').down('textfield[name=idMatrix]').setValue(response.data['idMatrix']);
                    cbo.up('window').down('textfield[name=codeMatrix]').setValue(response.data['idBusinessLineOne']);
                    cbo.up('window').down('UpperCaseTextFieldObligatory[name=descriptionScaleRisk]').setValue(response.data['descriptionScaleRisk']);
                    cbo.up('window').down('NumberDecimalNumberObligatory').setFieldStyle('background-color: #' + response.data['color'] + '; background-image: none;color:#000000;');
                    cbo.up('window').down('UpperCaseTextFieldObligatory[name=descriptionScaleRisk]').setFieldStyle('background-color: #' + response.data['color'] + '; background-image: none;color:#000000;');

                } else {
                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, response.mensaje, Ext.Msg.ERROR);

                }
            },
            failure: function () {
            }
        });
    }

}