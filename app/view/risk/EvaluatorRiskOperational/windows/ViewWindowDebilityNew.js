Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDebilityNew', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowDebilityNew',
    width: 500,
    layout: {
        type: 'fit'
    },
    title: 'INGRESAR CAUSA',
    titleAlign: 'center',
    border: false,
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'textfield',
                            hidden: true,
                            name: 'versionCorrelative'
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            value: 'id',
                            name: 'idRiskWeaknessDetail'
                        },
                        {
                            xtype: 'container',
                            height: 84,
                            anchor: '100%',
                            layout: {
                                type: 'hbox',
                                align: 'middle'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    value: 'id',
                                    name: 'idWeakness',
                                    hidden: true
                                },
                                {
                                    xtype: 'UpperCaseTextArea',
                                    height: 75,
                                    flex: '1',
                                    maxLength: 1000,
                                    allowBlank: false,
                                    fieldLabel: 'Descripción',
                                    name: 'description',
                                    listeners: {
                                        afterrender: function (field) {
                                            field.focus(false, 200);
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ],
            buttonAlign: 'center',
            buttons: [
                {
                    text: 'Guardar',
                    scale: 'medium',
                    action: 'saveNewDebility',
                    iconCls: 'save'
                },
                {
                    text: 'Salir',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });
        me.callParent(arguments);
    }
});