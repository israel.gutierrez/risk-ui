Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.WindowModifyVersionEvaluation",
  {
    extend: "Ext.window.Window",
    alias: "widget.WindowModifyVersionEvaluation",
    /*     requires: [
      "DukeSource.view.risk.parameter.combos.WindowModifyVersionEvaluation"
    ], */
    height: 150,
    width: 500,
    layout: {
      type: "fit"
    },
    title: "VERSION DE EVALUACI&Oacute;N",
    titleAlign: "center",
    border: false,
    initComponent: function() {
      var me = this;
      var idRisk = me.idRisk;
      var backPanel = me.backPanel;
      var idVersionRisk = me.idVersionRisk;
      var description = me.description;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            title: "",
            items: [
              {
                xtype: "textfield",
                hidden: true,
                name: "versionCorrelative"
              },
              {
                xtype: "textfield",
                hidden: true,
                name: "idRisk"
              },
              {
                xtype: 'UpperCaseTextArea',
                anchor: "100%",
                maxLengthText: DukeSource.global.GiroMessages.MESSAGE_MAX_CHARACTER + 300,
                maxLength: 300,
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                allowBlank: false,
                fieldCls: "obligatoryTextField",
                labelWidth: 160,
                height: 70,
                value: description,
                fieldLabel: "DESCRIPCI&Oacute;N",
                name: "description",
                itemId: "description"
              }
            ]
          }
        ],
        buttons: [
          {
            text: "GUARDAR",
            iconCls: "save",
            handler: function(btn) {
              Ext.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/updateVersionEvaluationRisk.htm?nameView=PanelProcessRiskEvaluation",
                params: {
                  idRisk: idRisk,
                  idVersionRisk: idVersionRisk,
                  description: me.down("#description").getValue()
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    backPanel
                      .down("#nameEvaluation")
                      .setValue(me.down("#description").getValue());
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      response.message,
                      Ext.Msg.INFO
                    );
                    me.close();
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_ERROR,
                      response.message,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function() {}
              });
            }
          },
          {
            text: "SALIR",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
