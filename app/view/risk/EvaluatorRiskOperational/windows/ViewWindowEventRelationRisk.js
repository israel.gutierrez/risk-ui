Ext.require(["Ext.ux.CheckColumn"]);
Ext.require(["Ext.ux.CheckedColumn"]);
Ext.define("ModelGridPanelControlEvent", {
  extend: "Ext.data.Model",
  fields: [
    "idDetailControl",
    "idControl",
    "idRisk",
    "versionCorrelative",
    "idRiskWeaknessDetail",
    "scoreControl",
    "percentScoreControl",
    "dateEvaluation",
    "descriptionControl",
    "colorQualification",
    "nameQualification",
    "scoreDesign",
    "scoreExecution",
    "colorScoreControlDesign",
    "colorScoreControlExecution"
  ]
});

var StoreGridPanelControlEvent = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelControlEvent",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("ModelEventToRisk", {
  extend: "Ext.data.Model",
  fields: [
    "idEvent",
    "codeEvent",
    "workArea",
    "isRiskEventIncidents",
    "idBusinessLineTwo",
    "businessLineOne",
    "factorRisk",
    "idEventThree",
    "eventTwo",
    "eventOne",
    "managerRisk",
    "agency",
    "currency",
    "riskType",
    "businessLineThree",
    "actionPlan",
    "process",
    "idSubProcess",
    "insuranceCompany",
    "eventState",
    "eventType",
    "lossType",
    "accountingEntryLoss",
    "accountingEntryRecoveryLoss",
    "descriptionLargeEvent",
    "descriptionShortEvent",
    "totalLossRecovery",
    "totalLoss",
    "colorEventState",
    "nameEventState",
    {
      name: "dateOccurrence",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    "entityPenalizesLoss",
    "grossLoss",
    "lossRecovery",
    "sureAmount",
    "netLoss",
    "typeChange",
    "typeEvent",
    "nameEventThree",
    "nameEventTwo",
    "nameEventOne",
    "nameFactorRisk",
    "nameAction",
    "nameCurrency",
    "descriptionShort",
    "idPlanAccountSureAmountRecovery",
    "planAccountSureAmountRecovery",
    "planAccountLossRecovery",
    "idPlanAccountLossRecovery",
    "bookkeepingEntryLossRecovery",
    "bookkeepingEntrySureAmountRecovery",
    "amountOrigin",
    "codeGeneralIncidents",
    "cause",
    "fileAttachment",
    "codesRiskAssociated",
    "numberRiskAssociated",
    "indicatorAtRisk"
  ]
});

var storeEventToRisk = Ext.create("Ext.data.Store", {
  model: "ModelEventToRisk",
  autoLoad: true,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/getAllEventMaster.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("ModelWeaknessRiskEvent", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "relationEventRisk",
    "idRiskWeaknessDetail",
    "idRisk",
    "versionCorrelative",
    "idWeakness",
    "description",
    "codeWeakness",
    "nameFactorRisk",
    "state"
  ]
});

var StoreWeaknessRiskEvent = Ext.create("Ext.data.Store", {
  model: "ModelWeaknessRiskEvent",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowEventRelationRisk",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowEventRelationRisk",
    requires: ["DukeSource.view.risk.util.search.AdvancedSearchEvent"],
    layout: {
      align: "stretch",
      type: "vbox"
    },
    anchorSize: 100,
    title: "Eventos de pérdida  -  controles",
    titleAlign: "center",
    width: 1000,
    height: 580,
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "container",
            flex: 3,
            padding: "2 2 2 2",
            layout: {
              align: "stretch",
              type: "border"
            },
            items: [
              {
                xtype: "AdvancedSearchEvent",
                padding: "0 0 0 2",
                region: "west",
                collapseDirection: "left",
                collapsed: true,
                collapsible: true,
                split: true,
                width: 350,
                title: "Búsqueda avanzada",
                buttons: [
                  {
                    xtype: "button",
                    scale: "medium",
                    text: "Buscar",
                    iconCls: "search",
                    action: "searchAdvancedEventEvaluation",
                    margin: "0 5 0 0"
                  },
                  {
                    xtype: "button",
                    scale: "medium",
                    text: "Limpiar",
                    iconCls: "clear",
                    margin: "0 5 0 0",
                    handler: function() {
                      me.down("AdvancedSearchEvent")
                        .down("form")
                        .getForm()
                        .reset();
                    }
                  }
                ]
              },
              {
                xtype: "gridpanel",
                padding: "0 1 0 0",
                store: storeEventToRisk,
                itemId: "gridEventToRisk",
                title: "Base de eventos de pérdida",
                titleAlign: "center",
                flex: 1,
                columnLines: true,
                region: "center",
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false,
                    locked: true
                  },
                  {
                    header: "Código",
                    align: "left",
                    dataIndex: "codeEvent",
                    width: 110,
                    locked: true,
                    renderer: function(value, metaData, record) {
                      var state;
                      var type = "";
                      metaData.tdAttr =
                        'style="background-color: #' +
                        record.get("colorEventState") +
                        ' !important; height:40px;"';
                      state =
                        '<br><span style="font-size:9px">' +
                        record.get("nameEventState") +
                        "</span>";
                      if (record.get("eventType") == "2") {
                        type =
                          '<i class="tesla even-stack2" style="margin-left:18px;line-height: 0.2 !important;"></i>';
                      } else if (record.get("eventState") == "X") {
                        metaData.tdAttr =
                          'style="background-color: #cccccc !important; height:40px;"';
                        type =
                          '<i class="tesla even-blocked-24" style="margin-left:18px;line-height: 0.2 !important;"></i>';
                      }

                      if (record.get("fileAttachment") == "S") {
                        return (
                          '<div style="display:inline-block;height:20px;width:20px;padding:2px;"><i class="tesla even-attachment"></i></div><div style="display:inline-block;position:absolute;">' +
                          '<span style="font-size: 14px;">' +
                          value +
                          type +
                          "</span> " +
                          state +
                          "</div>"
                        );
                      } else {
                        return (
                          '<div style="display:inline-block;height:20px;width:20px;padding:2px;"></div><div style="display:inline-block;position:absolute;">' +
                          '<span style="font-size: 14px;">' +
                          value +
                          type +
                          "</span> " +
                          state +
                          "</div>"
                        );
                      }
                    }
                  },
                  {
                    header: "Descripción corta",
                    align: "left",
                    dataIndex: "descriptionShort",
                    width: 270
                  },
                  {
                    header: "Montos",
                    align: "center",
                    columns: [
                      {
                        header: "Pérdida total",
                        dataIndex: "totalLoss",
                        align: "center",
                        tdCls: "custom-column",
                        xtype: "numbercolumn",
                        format: "0,0.00",
                        width: 100
                      },
                      {
                        header: "Total recuperado",
                        dataIndex: "totalLossRecovery",
                        align: "center",
                        xtype: "numbercolumn",
                        format: "0,0.00",
                        width: 100
                      },
                      {
                        header: "Pérdida neta",
                        dataIndex: "netLoss",
                        tdCls: "custom-column",
                        align: "center",
                        xtype: "numbercolumn",
                        format: "0,0.00",
                        width: 100
                      }
                    ]
                  },
                  {
                    header: "Fecha de ocurrencia",
                    align: "center",
                    dataIndex: "dateOccurrence",
                    xtype: "datecolumn",
                    format: "d/m/Y H:i",
                    width: 90,
                    renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
                  },
                  {
                    header: "Factor de riesgo",
                    align: "center",
                    dataIndex: "nameFactorRisk",
                    width: 90
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: storeEventToRisk,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                }
              },
              {
                xtype: "container",
                region: "east",
                flex: 1,
                split: true,
                padding: "2 2 2 0",
                layout: {
                  type: "vbox",
                  align: "stretch"
                },
                items: [
                  {
                    xtype: "gridpanel",
                    flex: 0.8,
                    itemId: "riskDebilityEvent",
                    title: "CAUSAS",
                    titleAlign: "center",
                    store: StoreWeaknessRiskEvent,
                    loadMask: true,
                    columnLines: true,
                    columns: [
                      {
                        xtype: "rownumberer",
                        width: 25,
                        sortable: false
                      },
                      {
                        header: "Código",
                        dataIndex: "codeWeakness",
                        width: 60
                      },
                      {
                        header: "Descripción",
                        dataIndex: "description",
                        flex: 2
                      },
                      {
                        header: "Factor",
                        align: "center",
                        dataIndex: "nameFactorRisk",
                        width: 80
                      }
                    ],

                    bbar: {
                      xtype: "pagingtoolbar",
                      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                      store: StoreWeaknessRiskEvent,
                      displayInfo: true,
                      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
                      doRefresh: function() {
                        var grid = this,
                          current = grid.store.currentPage;
                        if (
                          grid.fireEvent("beforechange", grid, current) !==
                          false
                        ) {
                          grid.store.loadPage(current);
                          DukeSource.global.DirtyView.loadGridDefault(
                            me.down("#riskControlEvent")
                          );
                        }
                      }
                    },
                    listeners: {
                      render: function() {
                        var grid = this;
                        grid.store.getProxy().extraParams = {
                          idRisk: Ext.ComponentQuery.query(
                            "ViewPanelEvaluationRiskOperational"
                          )[0].idRisk,
                          versionCorrelative: Ext.ComponentQuery.query(
                            "ViewPanelEvaluationRiskOperational"
                          )[0].versionCorrelative
                        };
                        grid.store.getProxy().url =
                          "http://localhost:9000/giro/findRiskWeaknessDetail.htm";
                        grid.down("pagingtoolbar").moveFirst();
                        DukeSource.global.DirtyView.loadGridDefault(me.down("#riskControlEvent"));
                      },
                      itemclick: function(view, record) {
                        var grid = me.down("#riskControlEvent");
                        grid.store.getProxy().extraParams = {
                          idRiskWeaknessDetail: record.get(
                            "idRiskWeaknessDetail"
                          ),
                          idRisk: record.get("idRisk"),
                          versionCorrelative: record.get("versionCorrelative")
                        };
                        grid.store.getProxy().url =
                          "http://localhost:9000/giro/findDetailControl.htm";
                        grid.down("pagingtoolbar").moveFirst();
                      }
                    }
                  },
                  {
                    xtype: "gridpanel",
                    itemId: "riskControlEvent",
                    title: "Controles",
                    region: "east",
                    split: true,
                    titleAlign: "center",
                    flex: 1,
                    store: StoreGridPanelControlEvent,
                    loadMask: true,
                    columnLines: true,
                    columns: [
                      { xtype: "rownumberer", width: 25, sortable: false },
                      {
                        text: "SEL",
                        dataIndex: "indicatorSave",
                        width: 30,
                        xtype: "checkedcolumn",
                        store: StoreGridPanelControlEvent,
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        stopSelection: false
                      },
                      {
                        header: "Control",
                        dataIndex: "descriptionControl",
                        flex: 2
                      },
                      {
                        header: "Calificación",
                        align: "center",
                        dataIndex: "nameQualification",
                        flex: 1,
                        renderer: function(value, metaData, record) {
                          metaData.tdAttr =
                            'style="background-color: #' +
                            record.get("colorQualification") +
                            ' !important;"';
                          return "<span>" + value + "</span>";
                        }
                      },
                      {
                        header: "Valor",
                        align: "center",
                        dataIndex: "percentScoreControl",
                        flex: 1,
                        renderer: function(value, metaData, record) {
                          metaData.tdAttr =
                            'style="background-color: #' +
                            record.get("colorQualification") +
                            ' !important;"';
                          return "<span>" + value + "</span>";
                        }
                      }
                    ],
                    bbar: {
                      xtype: "pagingtoolbar",
                      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                      store: StoreGridPanelControlEvent,
                      displayInfo: true,
                      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                    },

                    listeners: {
                      render: function() {
                        var grid = this;
                        DukeSource.global.DirtyView.loadGridDefault(grid);
                      }
                    }
                  }
                ]
              }
            ]
          }
        ],
        buttonAlign: "center",
        buttons: [
          {
            text: "Vincular",
            scale: "medium",
            iconCls: "relation",
            handler: function() {
              var gridCause = me.down("#riskDebilityEvent");
              var arrayDataToSave = [];
              var records = me
                .down("#riskControlEvent")
                .getStore()
                .getRange();
              Ext.Array.each(records, function(record, index, countriesItSelf) {
                if (record.get("indicatorSave") == true) {
                  arrayDataToSave.push(record.data);
                }
              });
              if (
                me
                  .down("#gridEventToRisk")
                  .getSelectionModel()
                  .getCount() == 0
              ) {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_MESSAGE,
                  "Seleccione el EVENTO",
                  Ext.Msg.INFO
                );
              } else {
                var panelRisk = Ext.ComponentQuery.query(
                  "ViewPanelEvaluationRiskOperational"
                )[0];

                DukeSource.lib.Ajax.request({
                  method: "POST",
                  url:
                    "http://localhost:9000/giro/saveRelationEventRisk.htm?nameView=PanelProcessRiskEvaluation",
                  params: {
                    jsonData: Ext.JSON.encode(arrayDataToSave),
                    idEvent: me
                      .down("#gridEventToRisk")
                      .getSelectionModel()
                      .getSelection()[0]
                      .get("idEvent"),
                    idWeakness:
                      gridCause.getSelectionModel().getSelection()[0] ==
                      undefined
                        ? ""
                        : gridCause
                            .getSelectionModel()
                            .getSelection()[0]
                            .get("idWeakness"),
                    idRisk: panelRisk.idRisk,
                    versionRisk: panelRisk.versionCorrelative,
                    idRiskWeaknessDetail:
                      gridCause.getSelectionModel().getSelection()[0] ==
                      undefined
                        ? ""
                        : me
                            .down("#riskDebilityEvent")
                            .getSelectionModel()
                            .getSelection()[0]
                            .get("idRiskWeaknessDetail")
                  },
                  success: function(response) {
                    response = Ext.decode(response.responseText);

                    if (response.success) {
                      DukeSource.global.DirtyView.messageAlert(
                        DukeSource.global.GiroMessages.TITLE_MESSAGE,
                        response.message,
                        Ext.Msg.INFO
                      );
                      panelRisk
                        .down("#riskEventsToRisk")
                        .getStore()
                        .load();
                      panelRisk
                        .down("#amountEventLost")
                        .setValue(response.data["amountEventLost"]);
                      panelRisk
                        .down("#numberEventLost")
                        .setValue(response.data["numberEventLost"]);
                    } else {
                      DukeSource.global.DirtyView.messageAlert(
                        DukeSource.global.GiroMessages.TITLE_ERROR,
                        response.message,
                        Ext.Msg.ERROR
                      );
                    }
                  },
                  failure: function() {}
                });
              }
            }
          },
          {
            text: "Salir",
            scale: "medium",
            handler: function() {
              me.close();
            },
            iconCls: "logout"
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
