Ext.define("ModelWeaknessIncidentAll", {
  extend: "Ext.data.Model",
  fields: [
    "idRelated",
    "descriptionRelated",
    "idRisk",
    "idVersionEvaluation",
    "idRiskWeaknessDetail",
    "idRelation",
    "idWeaknessEvent"
  ]
});

Ext.define("ModelWeaknessIncidentSelected", {
  extend: "Ext.data.Model",
  fields: [
    "idRelated",
    "idRiskWeaknessDetail",
    "idRisk",
    "idVersionEvaluation",
    "descriptionRelated",
    "idRelation",
    "idWeaknessEvent"
  ]
});
var StoreWeaknessIncidentAll = Ext.create("Ext.data.Store", {
  model: "ModelWeaknessIncidentAll",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/getDetailIncidentsByState.htm",
    extraParams: {
      stateIncident: "I"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

var StoreWeaknessIncidentSelected = Ext.create("Ext.data.Store", {
  model: "ModelWeaknessIncidentSelected",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListUserRolesActives.htm",
    extraParams: {
      propertyFind: "description",
      valueFind: "",
      propertyOrder: "description"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowAssignIncidentToDebility",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowAssignIncidentToDebility",
    layout: {
      align: "stretch",
      type: "vbox"
    },
    height: 500,
    width: 1000,
    closable: false,
    anchorSize: 100,
    titleAlign: "center",

    initComponent: function() {
      var riskIndicator = this.riskIndicator;
      var debility = this.debility;
      var origin = this.origin;
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            height: 45,
            padding: "2 2 2 2",
            bodyPadding: 10,
            items: [
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "combobox",
                    labelWidth: 80,
                    width: 300,
                    displayField: "description",
                    valueField: "idIncidentGroup",

                    editable: false,
                    forceSelection: true,
                    fieldLabel: "GRUPO INC",
                    store: {
                      fields: ["idIncidentGroup", "description"],
                      proxy: {
                        actionMethods: {
                          create: "POST",
                          read: "POST",
                          update: "POST"
                        },
                        type: "ajax",
                        url:
                          "http://localhost:9000/giro/showListIncidentGroupActives.htm",
                        extraParams: {
                          propertyOrder: "description"
                        },
                        reader: {
                          type: "json",
                          root: "data",
                          successProperty: "success"
                        }
                      }
                    },
                    listeners: {
                      specialkey: function(f, e) {
                        DukeSource.global.DirtyView.focusEventEnterObligatory(
                          f,
                          e,
                          me.down("#idBusinessLineOne")
                        );
                      }
                    }
                  },
                  {
                    xtype:"UpperCaseTextField",
                    padding: "0 0 0 10",
                    flex: 2,
                    listeners: {
                      specialkey: function(field, e) {
                        var property = "";
                        if (e.getKey() === e.ENTER) {
                          var grid = me.down("grid");

                          grid.store.getProxy().extraParams = {
                            versionCorrelative:
                              debility.data["versionCorrelative"],
                            idRiskWeaknessDetail:
                              debility.data["idRiskWeaknessDetail"],
                            idRisk: debility.data["idRisk"],
                            idRelation: debility.data["idRelation"],
                            propertyFind: "descriptionShort",
                            valueFind: field.getValue(),
                            idIncidentGroup: me.down("combobox").getValue(),
                            propertyOrder: "idRisk"
                          };
                          grid.store.getProxy().url =
                            "http://localhost:9000/giro/findWeaknessEventNoRelational.htm";
                          grid.down("pagingtoolbar").moveFirst();
                        }
                      }
                    }
                  }
                ]
              }
            ]
          },
          {
            xtype: "container",
            flex: 3,
            padding: "2 2 2 2",
            layout: {
              align: "stretch",
              type: "hbox"
            },
            items: [
              {
                xtype: "gridpanel",
                padding: "0 1 0 0",
                itemId: "gridWeaknessIncidentAll",
                store: StoreWeaknessIncidentAll,
                loadMask: true,
                columnLines: true,
                flex: 1,
                title: "DISPONIBLES",
                titleAlign: "center",
                multiSelect: true,
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    dataIndex: "descriptionRelated",
                    flex: 5,
                    text: "DESCRIPCION"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  store: StoreWeaknessIncidentAll,
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  render: function(grid) {
                    grid.store.getProxy().extraParams = {
                      versionCorrelative: debility.data["versionCorrelative"],
                      idRiskWeaknessDetail:
                        debility.data["idRiskWeaknessDetail"],
                      idRisk: debility.data["idRisk"],
                      idRelation: debility.data["idRelation"]
                    };
                    grid.store.getProxy().url =
                      "http://localhost:9000/giro/findWeaknessEventNoRelational.htm";
                    grid.down("pagingtoolbar").moveFirst();
                  },
                  itemdblclick: function(grid, record, item, index, e, eOpts) {
                    Ext.MessageBox.show({
                      title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                      msg: "ESTA SEGURO DE AGREGAR EL INCIDENTE:",
                      icon: Ext.Msg.QUESTION,
                      buttonText: {
                        yes: "Si"
                      },
                      buttons: Ext.MessageBox.YESNO,
                      fn: function(btn) {
                        if (btn == "yes") {
                          Ext.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/saveWeaknessEventAndIncidents.htm?nameView=PanelProcessRiskEvaluation",
                            params: {
                              jsonData: Ext.JSON.encode(record.data)
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              if (response.success) {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_MESSAGE,
                                  response.mensaje,
                                  Ext.Msg.INFO
                                );
                                grid.getStore().load();
                                me.down("#gridWeaknessIncidentSelected")
                                  .getStore()
                                  .load();
                              } else {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_ERROR,
                                  response.mensaje,
                                  Ext.Msg.ERROR
                                );
                              }
                            },
                            failure: function() {}
                          });
                        }
                      }
                    });
                  }
                }
              },
              {
                xtype: "gridpanel",
                padding: "0 0 0 1",
                itemId: "gridWeaknessIncidentSelected",
                store: StoreWeaknessIncidentSelected,
                loadMask: true,
                columnLines: true,
                multiSelect: true,
                flex: 1,
                title: "ASIGNADOS",
                titleAlign: "center",
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    dataIndex: "descriptionRelated",
                    flex: 1,
                    text: "DESCRIPCION"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  store: StoreWeaknessIncidentSelected,
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  afterrender: function(grid) {
                    grid.store.getProxy().extraParams = {
                      versionCorrelative: debility.data["versionCorrelative"],
                      idRiskWeaknessDetail:
                        debility.data["idRiskWeaknessDetail"],
                      idRisk: debility.data["idRisk"],
                      idRelation: debility.data["idRelation"]
                    };
                    grid.store.getProxy().url =
                      "http://localhost:9000/giro/findWeaknessEventRelational.htm";
                    grid.down("pagingtoolbar").moveFirst();
                  },
                  itemdblclick: function(grid, record, item, index, e, eOpts) {
                    Ext.MessageBox.show({
                      title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                      msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
                      icon: Ext.Msg.QUESTION,
                      buttonText: {
                        yes: "Si"
                      },
                      buttons: Ext.MessageBox.YESNO,
                      fn: function(btn) {
                        if (btn == "yes") {
                          Ext.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/deleteWeaknessEventAndIncidents.htm?nameView=PanelProcessRiskEvaluation",
                            params: {
                              jsonData: Ext.JSON.encode(record.data)
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              if (response.success) {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_MESSAGE,
                                  response.mensaje,
                                  Ext.Msg.INFO
                                );
                                grid.getStore().load();
                                me.down("#gridWeaknessIncidentAll")
                                  .getStore()
                                  .load();
                              } else {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_ERROR,
                                  response.mensaje,
                                  Ext.Msg.ERROR
                                );
                              }
                            },
                            failure: function() {}
                          });
                        }
                      }
                    });
                  }
                }
              }
            ]
          }
        ],
        buttons: [
          {
            text: "SALIR",
            iconCls: "logout",
            handler: function() {
              if (me.origin == "Inherint") {
                Ext.ComponentQuery.query(
                  "ViewPanelEvaluationRiskOperational"
                )[0]
                  .down("#riskDebilityStart")
                  .getStore()
                  .load();
              } else {
                Ext.ComponentQuery.query(
                  "ViewPanelEvaluationRiskOperational"
                )[0]
                  .down("#riskDebility")
                  .getStore()
                  .load();
              }
              me.close();
            }
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
