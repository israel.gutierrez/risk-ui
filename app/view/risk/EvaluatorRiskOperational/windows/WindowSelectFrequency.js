Ext.define('SelectFrequency', {
    extend: 'Ext.data.Model',
    fields: [
        'idFrequency',
        'idFrequencyFeature',
        'idSelected',
        'description',
        'alias',
        'equivalentValueFrequency',
        'descriptionFrequencyFeature',
        'descriptionFrequency'
    ]
});

Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.windows.WindowSelectFrequency', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowSelectFrequency',
    width: 500,
    border: true,
    modal: true,
    layout: {
        type: 'fit'
    },
    initComponent: function () {
        var me = this;
        var idTypeMatrix = this.idTypeMatrix;
        var idOperationalRiskExposition = this.idOperationalRiskExposition;
        var idFrequency = this.idFrequency;
        var backPanel = this.backPanel;

        Ext.applyIf(me, {
            items: [

                {
                    xtype: 'container',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'form',
                            itemId: 'formFeatures',
                            flex: 1.1,
                            padding: 2,
                            bodyPadding: 5,
                            name: 'formFeatures',
                            layout: {
                                type: 'hbox',
                                pack: 'center',
                                defaultMargins: {
                                    top: 0,
                                    right: 5,
                                    bottom: 0,
                                    left: 0
                                }
                            }
                        },
                        {
                            xtype: 'container',
                            itemId: 'containerResult',
                            name: 'containerResult',
                            padding: '5 0 0 10',
                            flex: 1,
                            layout: {
                                type: 'hbox',
                                pack: 'center'
                            },
                            items: [
                                {
                                    xtype: 'ViewComboFrequency',
                                    allowBlank: false,
                                    readOnly: true,
                                    msgTarget: 'side',
                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                    labelWidth: 130,
                                    labelAlign: 'top',
                                    fieldLabel: 'Nivel de Frecuencia',
                                    listeners: {
                                        render: function (cbo) {
                                            if (idTypeMatrix !== '')
                                                cbo.getStore().load({
                                                    params: {
                                                        idTypeMatrix: idTypeMatrix,
                                                        idOperationalRiskExposition: idOperationalRiskExposition
                                                    },
                                                    callback: function () {
                                                        cbo.setValue(idFrequency);
                                                    }
                                                });
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'textarea',
                            padding: 5,
                            name: 'reasonFrequency',
                            itemId: 'reasonFrequency',
                            labelWidth: 130,
                            maxLength: 2000,
                            height: 60,
                            labelAlign: 'top',
                            fieldLabel: 'Sustento Frecuencia',
                            hidden: hidden('VPEBtnReasonFrequency'),
                            value: backPanel.down('#reasonFrequency').getValue()
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Continuar',
                    iconCls: 'table_go',
                    disabled: !me.isVersionActive,
                    scale: 'medium',
                    handler: function () {
                        var frequencyBackPanel = backPanel.down('#idFrequency');

                        frequencyBackPanel.getStore().load({
                                params: {
                                    idTypeMatrix: idTypeMatrix,
                                    idOperationalRiskExposition: idOperationalRiskExposition
                                },
                                callback: function () {
                                    if (me.down('form').getForm().isValid()) {
                                        frequencyBackPanel.setValue(me.down('ViewComboFrequency').getValue());
                                        backPanel.riskInherent(backPanel, idTypeMatrix);
                                        backPanel.featureFrequencyIds = me.featureFrequencyIds;
                                        backPanel.frequencyIds = me.frequencyIds;
                                        backPanel.down('#reasonFrequency').setValue(me.down('#reasonFrequency').getValue());
                                        me.close();
                                    } else {
                                        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
                                    }
                                }
                            }
                        );
                    }
                },
                {
                    text: 'Cancelar',
                    scale: 'medium',
                    iconCls: 'cancel',
                    handler: function () {
                        me.close();
                    }
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    },

    createCombos: function (json) {
        var me = this;
        var frequencyIds = [];
        var featureIds = [];
        var container = me.down('#formFeatures');

        var data = json.data;

        for (var i = 0; i < data.length; i++) {
            var item = data[i];

            container.add({
                xtype: 'combobox',
                fieldLabel: item['description'],
                displayField: 'alias',
                allowBlank: false,
                editable: false,
                labelAlign: 'top',
                value: data.length === 1 ? me.idFrequency : item['idSelected'],
                valueField: 'idFrequency',
                store: {
                    model: 'SelectFrequency',
                    data: item['children'],
                    proxy: {
                        type: "memory"
                    }
                },
                listeners: {
                    select: function () {
                        calculateFrequency(me, frequencyIds, featureIds)
                    }
                }
            });

        }
    }
});

function calculateFrequency(me, frequencyIds, featureIds) {
    frequencyIds.splice(0, frequencyIds.length);
    featureIds.splice(0, featureIds.length);

    me.down('#formFeatures').query('.combobox').forEach(function (c) {
        frequencyIds.push(c.getValue());

        var record = c.findRecord(c.valueField, c.getValue());
        if (record) {
            featureIds.push(record.data['idFrequencyFeature'])
        }
    });

    var frequency = Math.max.apply(null, frequencyIds);
    me.down('ViewComboFrequency').setValue(frequency.toString());
    me.featureFrequencyIds = {data: featureIds};
    me.frequencyIds = {data: frequencyIds};
}