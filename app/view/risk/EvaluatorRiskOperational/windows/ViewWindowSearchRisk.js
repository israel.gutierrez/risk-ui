Ext.define("ModelSearchRisk", {
  extend: "Ext.data.Model",
  fields: ["idRisk", "description", "codeRisk", "state"]
});

var storeSearchRisk = Ext.create("Ext.data.Store", {
  model: "ModelSearchRisk",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowSearchRisk",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowSearchRisk",
    layout: {
      align: "stretch",
      type: "vbox"
    },
    anchorSize: 100,
    title: "BUSCAR RIESGO",
    titleAlign: "center",
    width: 900,
    height: 580,
    tbar: [
      {
        xtype: "button",
        text: "NUEVO",
        iconCls: "add",
        scale: "medium",
        cls: "my-btn",
        overCls: "my-over",
        handler: function() {
          var catalogoRisk = Ext.create(
            "DukeSource.view.risk.parameter.windows.ViewWindowCatalogRisk",
            {
              modal: true
            }
          ).show();
          catalogoRisk.down("#description").focus(false, 200);
        }
      }
    ],
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            height: 45,
            padding: "2 2 2 2",
            bodyPadding: 10,
            items: [
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "combobox",
                    value: "2",
                    labelWidth: 80,
                    width: 200,
                    fieldLabel: "BUSCAR POR",
                    store: [
                      ["1", "Código"],
                      ["2", "Descripción"]
                    ]
                  },
                  {
                    xtype:"UpperCaseTextField",
                    flex: 2,
                    listeners: {
                      render: function(f) {
                        f.focus(false, 500);
                      },
                      specialkey: function(field, e) {
                        var property = "";
                        if (
                          Ext.ComponentQuery.query(
                            "ViewWindowSearchRisk combobox"
                          )[0].getValue() == "1"
                        ) {
                          property = "codeRisk";
                        } else {
                          property = "description";
                        }
                        if (e.getKey() === e.ENTER) {
                          var grid = me.down("grid");
                          DukeSource.global.DirtyView.searchPaginationGridToEnter(
                            field,
                            grid,
                            grid.down("pagingtoolbar"),
                            "http://localhost:9000/giro/findCatalogRisk.htm",
                            property,
                            "description"
                          );
                        }
                      }
                    }
                  }
                ]
              }
            ]
          },
          {
            xtype: "container",
            flex: 3,
            padding: "2 2 2 2",
            layout: {
              align: "stretch",
              type: "hbox"
            },
            items: [
              {
                xtype: "gridpanel",
                padding: "0 1 0 0",
                store: storeSearchRisk,
                flex: 1,
                titleAlign: "center",
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    dataIndex: "codeRisk",
                    width: 100,
                    align: "center",
                    text: "CODIGO"
                  },
                  {
                    dataIndex: "description",
                    flex: 5,
                    text: "DESCRIPCION"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: storeSearchRisk,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  render: function() {
                    var me = this;
                    DukeSource.global.DirtyView.searchPaginationGridNormal(
                      "",
                      me,
                      me.down("pagingtoolbar"),
                      "http://localhost:9000/giro/showListCatalogRiskActives.htm",
                      "description",
                      "idRisk"
                    );
                  }
                }
              }
            ]
          }
        ],
        buttonAlign: "center",
        buttons: [
          {
            text: "SALIR",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
