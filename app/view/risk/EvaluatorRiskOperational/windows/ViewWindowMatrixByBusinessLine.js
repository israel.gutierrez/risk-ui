Ext.define("ModelGridMatrixByBusinessLine", {
  extend: "Ext.data.Model",
  fields: [
    //             'cell1'
    //            ,'cell2'
    //            ,'cell3'
    //            ,'cell4'
    //            ,'cell5'
  ]
});

var StoreGridMatrixByBusinessLine = Ext.create("Ext.data.Store", {
  model: "ModelGridMatrixByBusinessLine",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowMatrixByBusinessLine",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowMatrixByBusinessLine",
    layout: "fit",
    anchorSize: 100,
    title: "",
    titleAlign: "center",
    width: 820,
    height: 370,
    initComponent: function() {
      var idFrequencyResidual = this.idFrequencyResidual;
      var idImpactResidual = this.idImpactResidual;
      var idFrequency = this.idFrequency;
      var idImpact = this.idImpact;
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            padding: "2 2 2 2",
            name: "riskDebility",
            store: StoreGridMatrixByBusinessLine,
            loadMask: true,
            columnLines: true,
            columns: [
              //                        {header: 'FRECUENCIA',  align:'right', dataIndex: 'description',flex:1.5,tdCls: 'wrap'}
              //                        ,{header: 'MENOR', dataIndex: 'cell1',align:'right',flex:1,renderer: function(value, metaData, record)
              //                        {
              //                            metaData.tdAttr = 'style="background-color: #'+record.get('cell1').colour+' !important;height:40px"';
              //                            if((idFrequencyResidual==record.get('cell1').frequency && idImpactResidual==record.get('cell1').impact) &&
              //                               (idFrequency==record.get('cell1').frequency && idImpact==record.get('cell1').impact))
              //                            {
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell1').valueCell, '0,0.00') +"<img src='images/bullet-blue-red.png' />"+ '</span>';
              //                            }else if(idFrequency==record.get('cell1').frequency && idImpact==record.get('cell1').impact){
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell1').valueCell, '0,0.00') +"<img src='images/bullet-blue.png' />"+ '</span>';
              //                            }else if(idFrequencyResidual==record.get('cell1').frequency && idImpactResidual==record.get('cell1').impact){
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell1').valueCell, '0,0.00') +"<img src='images/bullet-red.png' />"+ '</span>';
              //                            }else{
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell1').valueCell, '0,0.00') + '</span>';
              //                            }
              //                        }}
              //                        ,{header: 'BAJO', dataIndex: 'cell2',align:'right',flex:1,renderer: function(value, metaData, record)
              //                        {
              //                            metaData.tdAttr = 'style="background-color: #'+record.get('cell2').colour+' !important;height:40px"';
              //                            if((idFrequencyResidual==record.get('cell2').frequency && idImpactResidual==record.get('cell2').impact) &&
              //                                (idFrequency==record.get('cell2').frequency && idImpact==record.get('cell2').impact))
              //                            {
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell2').valueCell, '0,0.00') +"<img src='images/bullet-blue-red.png' />"+ '</span>';
              //                            }else if(idFrequency==record.get('cell2').frequency && idImpact==record.get('cell2').impact){
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell2').valueCell, '0,0.00') +"<img src='images/bullet-blue.png' />"+ '</span>';
              //                            }else if(idFrequencyResidual==record.get('cell2').frequency && idImpactResidual==record.get('cell2').impact){
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell2').valueCell, '0,0.00') +"<img src='images/bullet-red.png' />"+ '</span>';
              //                            }else{
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell2').valueCell, '0,0.00') + '</span>';
              //                            }
              //                        }}
              //                        ,{header: 'MEDIO', dataIndex: 'cell3',align:'right',flex:1,renderer: function(value, metaData, record)
              //                        {
              //                            metaData.tdAttr = 'style="background-color: #'+record.get('cell3').colour+' !important;height:40px"';
              //                            if((idFrequencyResidual==record.get('cell3').frequency && idImpactResidual==record.get('cell3').impact) &&
              //                                (idFrequency==record.get('cell3').frequency && idImpact==record.get('cell3').impact))
              //                            {
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell3').valueCell, '0,0.00') +"<img src='images/bullet-blue-red.png' />"+ '</span>';
              //                            }else if(idFrequency==record.get('cell3').frequency && idImpact==record.get('cell3').impact){
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell3').valueCell, '0,0.00') +"<img src='images/bullet-blue.png' />"+ '</span>';
              //                            }else if(idFrequencyResidual==record.get('cell3').frequency && idImpactResidual==record.get('cell3').impact){
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell3').valueCell, '0,0.00') +"<img src='images/bullet-red.png' />"+ '</span>';
              //                            }else{
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell3').valueCell, '0,0.00') + '</span>';
              //                            }
              //                        }}
              //                        ,{header: 'MAYOR', dataIndex: 'cell4',align:'right',flex:1,renderer: function(value, metaData, record)
              //                        {
              //                            metaData.tdAttr = 'style="background-color: #'+record.get('cell4').colour+' !important;height:40px"';
              //                            if((idFrequencyResidual==record.get('cell4').frequency && idImpactResidual==record.get('cell4').impact) &&
              //                                (idFrequency==record.get('cell4').frequency && idImpact==record.get('cell4').impact))
              //                            {
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell4').valueCell, '0,0.00') +"<img src='images/bullet-blue-red.png' />"+ '</span>';
              //                            }else if(idFrequency==record.get('cell4').frequency && idImpact==record.get('cell4').impact){
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell4').valueCell, '0,0.00') +"<img src='images/bullet-blue.png' />"+ '</span>';
              //                            }else if(idFrequencyResidual==record.get('cell4').frequency && idImpactResidual==record.get('cell4').impact){
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell4').valueCell, '0,0.00') +"<img src='images/bullet-red.png' />"+ '</span>';
              //                            }else{
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell4').valueCell, '0,0.00') + '</span>';
              //                            }
              //                        }}
              //                        ,{header: 'CRITICO', dataIndex: 'cell5',align:'right',flex:1,renderer: function(value, metaData, record)
              //                        {
              //                            metaData.tdAttr = 'style="background-color: #'+record.get('cell5').colour+' !important;height:40px"';
              //                            if((idFrequencyResidual==record.get('cell5').frequency && idImpactResidual==record.get('cell5').impact) &&
              //                                (idFrequency==record.get('cell5').frequency && idImpact==record.get('cell5').impact))
              //                            {
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell5').valueCell, '0,0.00') +"<img src='images/bullet-blue-red.png' />"+ '</span>';
              //                            }else if(idFrequency==record.get('cell5').frequency && idImpact==record.get('cell5').impact){
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell5').valueCell, '0,0.00') +"<img src='images/bullet-blue.png' />"+ '</span>';
              //                            }else if(idFrequencyResidual==record.get('cell5').frequency && idImpactResidual==record.get('cell5').impact){
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell5').valueCell, '0,0.00') +"<img src='images/bullet-red.png' />"+ '</span>';
              //                            }else{
              //                                return '<span style="color:#949898;">' + Ext.util.Format.number(record.get('cell5').valueCell, '0,0.00') + '</span>';
              //                            }
              //                        }}
            ],
            //                    tbar:[
            //                        {
            //                            xtype: 'button',
            //                            text: 'NUEVO',
            //                            iconCls:'add',
            //                            handler:function(button){
            //                                var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDebilityEntry',{
            //                                    modal:true
            //                                }).show();
            //                            }
            //                        },
            //                        {
            //                            xtype:'button',
            //                            text:'MODIFICAR',
            //                            iconCls:'modify',
            //                            handler:function(button){
            //                                var panel = button.up('ViewPanelIdentifyRiskOperational');
            //                                var grid = panel.down('grid[name=riskDebility]');
            //                                if (grid.getSelectionModel().getCount() == 0) {
            //                                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING,'SELECCIONE UNA DEBILIDAD INDENTIFICADA POR FAVOR',Ext.Msg.WARNING);
            //                                } else {
            //                                    var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDebilityEntry',{
            //                                        modal:true
            //                                    });
            //                                    window.down('form').getForm().loadRecord(grid.getSelectionModel().getSelection()[0]);
            //                                    window.show();
            //                                }
            //                            }
            //                        },
            //                        {
            //                            xtype:'button',
            //                            text:'ELIMINAR',
            //                            iconCls:'delete',
            //                            handler:function(button){
            //                                var panel = button.up('ViewPanelIdentifyRiskOperational');
            //                                var grid = panel.down('grid[name=riskDebility]');
            //                                if (grid.getSelectionModel().getCount() == 0) {
            //                                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING,'SELECCIONE UNA DEBILIDAD INDENTIFICADA POR FAVOR',Ext.Msg.WARNING);
            //                                } else {
            //                                    DukeSource.global.DirtyView.deleteElementToGrid(grid,'deleteRiskWeaknessDetail.htm?nameView=PanelProcessRiskEvaluation');
            //                                }
            //                            }
            //                        }
            //                    ]
            //                    ,
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridMatrixByBusinessLine,
              displayInfo: true,
              items: [
                {
                  xtype: "textfield",
                  readOnly: true,
                  labelWidth: 70,
                  width: 130,
                  fieldLabel: "R.INHERENTE",
                  fieldStyle:
                    "background-color: #3F94D5; background-image: none;"
                },
                "--",
                {
                  xtype: "textfield",
                  readOnly: true,
                  labelWidth: 65,
                  width: 125,
                  fieldLabel: "R.RESIDUAL",
                  fieldStyle:
                    "background-color: #D53F3F; background-image: none;"
                }
              ],
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            },
            listeners: {
              render: function() {
                var grid = this;
                DukeSource.global.DirtyView.loadGridDefault(grid);
              }
            }
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
