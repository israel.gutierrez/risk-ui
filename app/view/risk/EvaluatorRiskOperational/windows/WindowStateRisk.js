Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.WindowStateRisk",
  {
    extend: "Ext.window.Window",
    alias: "widget.WindowStateRisk",
    height: 200,
    width: 500,
    layout: {
      type: "fit"
    },
    title: "Estado del riesgo",
    border: false,
    initComponent: function() {
      var me = this;
      var idRisk = me.idRisk;
      var innerGrid = me.grid;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            title: "",
            items: [
              {
                xtype: "textfield",
                hidden: true,
                name: "versionCorrelative",
                itemId: "versionCorrelative"
              },
              {
                xtype: "textfield",
                hidden: true,
                value: idRisk,
                name: "risk",
                itemId: "risk"
              },
              {
                xtype: "textfield",
                hidden: true,
                itemId: "id",
                value: "id",
                name: "id"
              },
              {
                xtype: "combobox",
                anchor: "100%",
                fieldLabel: "Estado",
                editable: false,
                allowBlank: false,
                name: "stateRisk",
                itemId: "stateRisk",
                fieldCls: "obligatoryTextField",
                displayField: "description",
                valueField: "id",
                store: {
                  fields: ["id", "description", "sequence"],
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url: "http://localhost:9000/giro/findStateIncident.htm",
                    extraParams: {
                      propertyFind: "si.typeIncident",
                      valueFind: DukeSource.global.GiroConstants.RISK,
                      propertyOrder: "si.sequence"
                    },
                    reader: {
                      type: "json",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                }
              },
              {
                xtype: 'UpperCaseTextArea',
                anchor: "100%",
                allowBlank: false,
                fieldCls: "obligatoryTextField",
                maxLengthText: DukeSource.global.GiroMessages.MESSAGE_MAX_CHARACTER + 300,
                maxLength: 300,
                height: 70,
                fieldLabel: "Observación",
                name: "description"
              }
            ]
          }
        ],
        buttons: [
          {
            text: "Guardar",
            scale: "medium",
            iconCls: "save",
            handler: function(btn) {
              var form = me.down("form");
              if (form.getForm().isValid()) {
                DukeSource.lib.Ajax.request({
                  waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                  method: "POST",
                  url:
                    "http://localhost:9000/giro/saveChangeStateRisk.htm?nameView=PanelProcessRiskEvaluation",
                  params: {
                    jsonData: Ext.JSON.encode(form.getValues())
                  },
                  success: function(response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                      me.close();
                      var win = Ext.ComponentQuery.query(
                        "ViewPanelIdentifyRiskOperational"
                      )[0];
                      var grid = win.down("grid");
                      grid.down("pagingtoolbar").moveFirst();

                      DukeSource.global.DirtyView.messageNormal(response.message);
                    } else {
                      DukeSource.global.DirtyView.messageWarning(response.message);
                    }
                  }
                });
              } else {
                DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
              }
            }
          },
          {
            text: "Salir",
            scale: "medium",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ],
        buttonAlign: "center",
        listeners: {
          afterrender: function() {
            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/findChangeStateRiskByRisk.htm?nameView=PanelProcessRiskEvaluation",
              params: {
                idRisk: idRisk
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  me.down("#stateRisk")
                    .getStore()
                    .load({
                      callback: function() {
                        me.down("form")
                          .getForm()
                          .setValues(response.data);
                        me.show();
                      }
                    });
                } else {
                  // DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_ERROR, response.message, Ext.Msg.ERROR);
                }
              },
              failure: function() {}
            });
          }
        }
      });

      me.callParent(arguments);
    }
  }
);
