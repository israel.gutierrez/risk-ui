Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowConfigurationMatrix', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowConfigurationMatrix',
    maximized: true,
    closable: false,
    border: false,
    modal: true,
    layout: {
        type: 'fit'
    },
    titleAlign: 'center',
    title: 'Configuración general de matrices',
    initComponent: function () {
        var stateVerification = this.stateVerification;
        var stateConfig = this.stateConfig;
        var win = this;
        Ext.applyIf(win, {
            items: [
                {
                    xtype: 'panel',
                    height: 171,
                    layout: 'fit',
                    padding: '2 2 2 2',
                    tbar: [
                        {
                            xtype: 'ViewComboOperationalRiskExposition',
                            labelWidth: 140,
                            fieldLabel: 'Patrimonio efectivo',
                            readOnly: true
                        }, '-',
                        {
                            xtype: 'ViewComboTypeMatrix',
                            itemId: 'idConfigurationTypeMatrix',
                            labelWidth: 100,
                            width: 400,
                            editable: false,
                            emptyText: 'Seleccione..',
                            fieldLabel: 'Tipo de matriz',
                            listeners: {
                                select: function (cbo) {
                                    win.down('#idConfigurationFrequency').setDisabled(false);
                                    cbo.setReadOnly(true);
                                }
                            }
                        }, '-',
                        {
                            text: 'Probabilidad',
                            disabled: true,
                            itemId: 'idConfigurationFrequency',
                            action: 'configurationFrequency'
                        }, '-',
                        {
                            text: 'Impacto',
                            disabled: true,
                            itemId: 'idConfigurationImpact',
                            action: 'configurationImpact'
                        }, '-',
                        {
                            text: 'Matriz',
                            disabled: true,
                            iconCls: 'matrix',
                            itemId: 'idConfigurationMatrix',
                            action: 'configurationMatrix'
                        }, '->',
                        {
                            text: 'Salir',
                            handler: function () {
                                Ext.ComponentQuery.query('ViewPanelConfigurationGeneralMatrix ViewGridPanelRegisterConfigurationMatrix')[0].getStore().load();
                                win.close();
                            },
                            iconCls: 'logout'
                        }
                    ],
                    items: []
                }
            ],
            buttons: [
                {
                    text: 'SALIR',
                    scale: 'medium',
                    handler: function () {
                        Ext.ComponentQuery.query('ViewPanelConfigurationGeneralMatrix ViewGridPanelRegisterConfigurationMatrix')[0].getStore().load();
                        win.close();
                    },
                    iconCls: 'logout'
                }
            ]
        });

        win.callParent(arguments);
    }

});

