Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowPreviousRisk",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowPreviousRisk",
    border: false,
    width: 400,
    layout: "fit",
    title: "Ambito de evaluaci&oacute;n",
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            title: "",
            items: [
              {
                xtype: "textfield",
                hidden: true,
                name: "idRiskEvaluationMatrix",
                value: "id"
              },
              {
                xtype: "textfield",
                hidden: true,
                itemId: "unity",
                name: "unity"
              },
              {
                xtype: "textfield",
                hidden: true,
                itemId: "costCenter",
                name: "costCenter"
              },
              {
                xtype: "UpperCaseTextFieldObligatory",
                anchor: "100%",
                fieldLabel: "Nombre del grupo",
                name: "nameEvaluation",
                enforceMaxLength: true,
                maxLength: 400,
                listeners: {
                  afterrender: function(field) {
                    field.focus(false, 200);
                  },
                  specialkey: function(f, e) {
                    DukeSource.global.DirtyView.focusEventEnterObligatory(
                      f,
                      e,
                      me.down("datefield[name=dateEvaluation]")
                    );
                  }
                }
              },
              {
                xtype: "combobox",
                fieldLabel: "Tipo evaluación",
                name: "typeRiskEvaluation",
                itemId: "typeRiskEvaluation",
                allowBlank: false,
                msgTarget: "side",
                fieldCls: "obligatoryTextField",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                displayField: "description",
                valueField: "idTypeRiskEvaluation",
                store: {
                  fields: ["idTypeRiskEvaluation", "description"],
                  pageSize: 999,
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url:
                      "http://localhost:9000/giro/showListTypeRiskEvaluationActivesComboBox.htm",
                    extraParams: {
                      propertyOrder: "description"
                    },
                    reader: {
                      type: "json",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                }
              },
              {
                xtype: "datefield",
                format: "d/m/Y",
                allowBlank: false,
                msgTarget: "side",
                fieldCls: "obligatoryTextField",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                fieldLabel: "Fecha",
                enforceMaxLength: true,
                maxLength: 10,
                name: "dateEvaluation",
                maxValue: new Date(),
                listeners: {
                  specialkey: function(f, e) {
                    DukeSource.global.DirtyView.focusEventEnter(
                      f,
                      e,
                      me.down("#workArea")
                    );
                  }
                }
              },
              {
                xtype: "displayfield",
                anchor: "100%",
                fieldLabel: getName("EWCBUnity"),
                itemId: "descriptionUnity",
                name: "descriptionUnity",
                padding: "4 0 4 0",
                value: "(Seleccionar)",
                fieldCls: "style-for-url",
                listeners: {
                  afterrender: function(view) {
                    view.getEl().on("click", function() {
                      Ext.create(
                        "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeOrganization",
                        {
                          backWindow: me,
                          descriptionUnity: "descriptionUnity",
                          unity: "unity",
                          costCenter: "costCenter"
                        }
                      ).show();
                    });
                  }
                }
              },
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "textfield",
                    hidden: true,
                    value: userName,
                    name: "analyst"
                  },
                  {
                    xtype:"UpperCaseTextFieldReadOnly",
                    flex: 1,
                    allowBlank: false,
                    value: fullName,
                    fieldLabel: "Analista r.o.",
                    name: "nameAnalyst"
                  },
                  {
                    xtype: "button",
                    width: 26,
                    iconCls: "search",
                    itemId: "searchAnalystPrevious",
                    handler: function(button) {
                      var panel = button.up("panel");
                      var win = Ext.create(
                        "DukeSource.view.risk.util.search.SearchUser",
                        {
                          category: "ANALISTA",
                          modal: true
                        }
                      ).show();
                      win.down("grid").on("itemdblclick", function() {
                        panel.down("textfield[name=analyst]").setValue(
                          win
                            .down("grid")
                            .getSelectionModel()
                            .getSelection()[0]
                            .get("userName")
                        );
                        panel
                          .down("UpperCaseTextFieldReadOnly[name=nameAnalyst]")
                          .setValue(
                            win
                              .down("grid")
                              .getSelectionModel()
                              .getSelection()[0]
                              .get("fullName")
                          );
                        me.down("button[action=showPanelIdentifyRisk]").focus(
                          false,
                          100
                        );
                        win.close();
                      });
                    }
                  }
                ]
              },
              {
                xtype: "combobox",
                fieldLabel: "Agencia",
                name: "agency",
                itemId: "agency",
                plugins: ["ComboSelectCount"],
                displayField: "description",
                valueField: "idAgency",
                fieldStyle: "text-transform:uppercase",
                store: {
                  fields: ["idAgency", "description"],
                  pageSize: 999,
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url:
                      "http://localhost:9000/giro/showListAgencyActivesComboBox.htm",
                    extraParams: {
                      propertyOrder: "a.description"
                    },
                    reader: {
                      type: "json",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                }
              },
              {
                xtype: "filefield",
                anchor: "100%",
                itemId: "text",
                name: "text",
                fieldLabel: "Adjunto"
              },
              {
                xtype: "textfield",
                hidden: true,
                itemId: "numberRisk",
                name: "numberRisk",
                value: 0
              }
            ]
          }
        ],
        buttonAlign: "center",
        buttons: [
          {
            text: "Guardar",
            action: "showPanelIdentifyRisk",
            scale: "medium",
            iconCls: "save"
          },
          {
            text: "Salir",
            handler: function() {
              me.close();
            },
            scale: "medium",
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
