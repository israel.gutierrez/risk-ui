Ext.define("ModelFileAttach", {
  extend: "Ext.data.Model",
  fields: ["correlative", "idRisk", "nameFile", "fullName"]
});

var storeFileAttach = Ext.create("Ext.data.Store", {
  model: "ModelFileAttach",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListFileAttachmentsDetail.htm",
    extraParams: {
      //            idDocument:Ext.ComponentQuery.query('ViewPanelDocumentPending grid')[0].getSelectionModel().getSelection()[0].get('idDocument'),
      //            idDetailDocument:Ext.ComponentQuery.query('ViewPanelDocumentPending grid')[0].getSelectionModel().getSelection()[0].get('idDetailDocument'),
      //            propertyOrder:'idDetailDocument'
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
//storeFileAttach.load();

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDownloadReportToRisk",
  {
    extend: "Ext.window.Window",
    border: false,
    alias: "widget.ViewWindowDownloadReportToRisk",
    height: 383,
    width: 589,
    layout: {
      type: "fit"
    },
    title: "DOCUMENTOS ADJUNTOS",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "textfield",
            //                    itemId : 'id',
            name: "idRisk",
            hidden: true
          },
          {
            xtype: "gridpanel",
            store: storeFileAttach,
            flex: 1,
            columns: [
              {
                xtype: "rownumberer",
                width: 25,
                sortable: false
              },
              {
                dataIndex: "correlative",
                width: 60,
                text: "CODIGO"
              },
              {
                dataIndex: "fullName",
                flex: 1,
                text: "NOMBRE"
              },
              {
                dataIndex: "nameFile",
                flex: 1,
                text: "ARCHIVO"
              },
              {
                xtype: "actioncolumn",
                header: "DOCUMENTO",
                align: "center",
                width: 80,
                items: [
                  {
                    icon: "images/page_white_put.png",
                    handler: function(grid, rowIndex) {
                      Ext.core.DomHelper.append(document.body, {
                        tag: "iframe",
                        id: "downloadIframe",
                        frameBorder: 0,
                        width: 0,
                        height: 0,
                        css: "display:none;visibility:hidden;height:0px;",
                        src:
                          "http://localhost:9000/giro/downloadFileAttachmentsRisk.htm?correlative=" +
                          grid.store.getAt(rowIndex).get("correlative") +
                          "&idRisk=" +
                          grid.store.getAt(rowIndex).get("idRisk") +
                          "&nameFile=" +
                          grid.store.getAt(rowIndex).get("nameFile")
                      });
                      //                                    new Ext.Window({
                      //                                        modal:true,
                      //                                        width : 900,
                      //                                        height: 600,
                      //                                        layout : 'fit',
                      //                                        items : [
                      //                                            {
                      //                                                xtype : 'component',
                      //                                                autoEl : {
                      //                                                    tag : 'iframe',
                      //                                                    src: 'http://localhost:9000/giro/downloadFileAttachmentsRisk.htm?correlative='+grid.store.getAt(rowIndex).get('correlative')+'&idRisk='+grid.store.getAt(rowIndex).get('idRisk')+'&nameFile='+grid.store.getAt(rowIndex).get('nameFile')
                      //                                                }
                      //                                            }
                      //                                        ]
                      //                                    }).show();
                    }
                  }
                ]
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: 50,
              store: storeFileAttach,
              items: [
                {
                  xtype:"UpperCaseTrigger",
                  width: 120,
                  action: "searchGridAllAgency"
                }
              ]
            },
            listeners: {
              render: function() {
                var me = this;
                me.store.getProxy().extraParams = {
                  idRisk: Ext.ComponentQuery.query(
                    "ViewWindowDownloadReportToRisk"
                  )[0]
                    .down("textfield[name=idRisk]")
                    .getValue()
                };
                me.store.getProxy().url =
                  "http://localhost:9000/giro/showListFileAttachmentsRiskActives.htm";
                me.down("pagingtoolbar").moveFirst();
              }
            }
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
