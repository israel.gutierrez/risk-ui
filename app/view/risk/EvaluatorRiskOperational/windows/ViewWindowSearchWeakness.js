Ext.define("ModelUserActives", {
  extend: "Ext.data.Model",
  fields: ["idWeakness", "codeWeakness", "description", "state"]
});

var storeUserActives = Ext.create("Ext.data.Store", {
  model: "ModelUserActives",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowSearchWeakness",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowSearchWeakness",
    layout: {
      align: "stretch",
      type: "vbox"
    },
    anchorSize: 100,
    title: "BUSCAR CAUSA",
    titleAlign: "center",
    width: 800,
    height: 500,
    tbar: [
      {
        text: "Nuevo",
        iconCls: "add",
        scale: "medium",
        cls: "my-btn",
        overCls: "my-over",
        handler: function() {
          var window = Ext.create(
            "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDebilityNew",
            { modal: true }
          ).show();
        }
      }
    ],
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            height: 45,
            padding: "2 2 2 2",
            bodyPadding: 10,
            items: [
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "combobox",
                    value: "2",
                    labelWidth: 40,
                    width: 190,
                    fieldLabel: "Tipo",
                    store: [
                      ["1", "Código"],
                      ["2", "Descripción"]
                    ]
                  },
                  {
                    xtype:"UpperCaseTextField",
                    flex: 2,

                    listeners: {
                      afterrender: function(field) {
                        field.focus(false, 200);
                      },
                      specialkey: function(field, e) {
                        var property = "";
                        if (
                          Ext.ComponentQuery.query(
                            "ViewWindowSearchWeakness combobox"
                          )[0].getValue() == "1"
                        ) {
                          property = "codeWeakness";
                        } else {
                          property = "description";
                        }
                        if (e.getKey() === e.ENTER) {
                          var grid = me.down("grid");
                          var toolbar = grid.down("pagingtoolbar");
                          DukeSource.global.DirtyView.searchPaginationGridToEnter(
                            field,
                            grid,
                            grid.down("pagingtoolbar"),
                            "http://localhost:9000/giro/findWeakness.htm",
                            property,
                            "idWeakness"
                          );
                        }
                      }
                    }
                  }
                ]
              }
            ]
          },
          {
            xtype: "gridpanel",
            padding: 2,
            store: storeUserActives,
            flex: 1,
            titleAlign: "center",
            columns: [
              {
                xtype: "rownumberer",
                width: 25,
                sortable: false
              },
              {
                dataIndex: "codeWeakness",
                width: 120,
                text: "Código"
              },
              {
                dataIndex: "description",
                flex: 1,
                text: "Descripción"
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: storeUserActives,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            },
            listeners: {
              render: function() {
                var me = this;
                DukeSource.global.DirtyView.searchPaginationGridNormal(
                  "",
                  me,
                  me.down("pagingtoolbar"),
                  "http://localhost:9000/giro/findWeakness.htm",
                  "description",
                  "idWeakness"
                );
              }
            }
          }
        ],
        buttons: [
          {
            text: "Salir",
            iconCls: "logout",
            scale: "medium",
            cls: "my-btn",
            overCls: "my-over",
            handler: function() {
              me.close();
            }
          }
        ],
        buttonAlign: "center"
      });
      me.callParent(arguments);
    }
  }
);
