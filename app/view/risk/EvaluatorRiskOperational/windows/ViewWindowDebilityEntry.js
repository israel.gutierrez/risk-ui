Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDebilityEntry', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowDebilityEntry',
    requires: [
        'DukeSource.view.risk.parameter.combos.ViewComboFactorRisk'
        , 'DukeSource.view.risk.parameter.combos.ViewComboEventOne'
        , 'DukeSource.view.risk.parameter.combos.ViewComboEventTwo'
        , 'DukeSource.view.risk.parameter.combos.ViewComboEventThree'
    ],
    width: 500,
    layout: {
        type: 'fit'
    },
    title: 'Ingresar causa',
    titleAlign: 'center',
    border: false,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    fieldDefaults: {
                        labelCls: 'changeSizeFontToEightPt',
                        fieldCls: 'changeSizeFontToEightPt'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            hidden: true,
                            name: 'versionCorrelative'
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            value: 'id',
                            name: 'idRiskWeaknessDetail'
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            value: 'id',
                            name: 'idWeakness'

                        },
                        {
                            xtype:"UpperCaseTextFieldReadOnly",
                            name: 'codeWeakness',
                            fieldCls: 'readOnlyText',
                            anchor: '100%',
                            fieldLabel: 'Código'
                        },
                        {
                            xtype: 'container',
                            height: 84,
                            anchor: '100%',
                            layout: {
                                type: 'hbox',
                                align: 'middle'
                            },
                            items: [

                                {
                                    xtype: 'UpperCaseTextArea',
                                    height: 75,
                                    flex: 1,
                                    readOnly: true,
                                    maxLengthText: DukeSource.global.GiroMessages.MESSAGE_MAX_CHARACTER + 800,
                                    maxLength: 800,
                                    allowBlank: false,
                                    fieldLabel: 'Descripción',
                                    name: 'description'
                                },
                                {
                                    xtype: 'button',
                                    width: 67,
                                    height: 75,
                                    textAlign: 'center',
                                    itemId: 'buttonSearchWeakness',
                                    text: 'Buscar',
                                    iconCls: 'search',
                                    handler: function () {
                                        var countryWeakness = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowSearchWeakness', {
                                            modal: true
                                        }).show();
                                        countryWeakness.down('grid').on('itemdblclick', function (view, rec) {
                                            var gridWeakness = Ext.ComponentQuery.query('ViewPanelEvaluationRiskOperational')[0].down('#riskDebilityStart');
                                            var arguments = gridWeakness.getStore().data.items;
                                            var flag = false;
                                            for (var i = 0; i < arguments.length; i++) {
                                                var obj = arguments[i];
                                                if (obj.data['codeWeakness'] === rec.get('codeWeakness')) {
                                                    flag = true;
                                                    break;
                                                }
                                            }
                                            if (flag) {
                                                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING,
                                                    'La causa ya esta asignada al riesgo, asigne otra',
                                                    Ext.Msg.WARNING);
                                            } else {
                                                me.down('textfield[name=idWeakness]').setValue(rec.get('idWeakness'));
                                                me.down('textfield[name=description]').setValue(rec.get('description'));
                                                me.down('UpperCaseTextFieldReadOnly[name=codeWeakness]').setValue(rec.get('codeWeakness'));
                                                me.down('textfield[name=description]').setReadOnly(true);
                                                countryWeakness.close();
                                            }

                                        });
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'numberfield',
                            fieldLabel: 'Peso (%)',
                            maxValue: 100,
                            minvalue: 0,
                            allowBlank: false,
                            hidden: hidden('WRWWeight'),
                            value: 100,
                            name: 'weight',
                            itemId: 'weight',
                            fieldCls: 'numberPositive'
                        },
                        {
                            xtype: 'displayfield',
                            hidden: hidden('WDE_DSF_DescriptionFactorRisk'),
                            anchor: '100%',
                            padding: '2 0 2 0',
                            fieldLabel: 'Factor de riesgo',
                            itemId: 'descriptionFactorRisk',
                            name: 'descriptionFactorRisk',
                            value: '(Seleccionar)',
                            fieldCls: 'style-for-url',
                            listeners: {
                                afterrender: function (view) {
                                    view.getEl().on('click', function () {
                                        Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeFactorRisk', {
                                            winParent: me
                                        }).show();
                                    })
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            itemId: 'factorRisk',
                            name: 'factorRisk'
                        },
                        {
                            xtype: 'ViewComboEventOne',
                            anchor: '70%',
                            hidden: hidden('WRWEventOne'),
                            fieldLabel: 'Tipo de evento n.1',
                            action: 'selectEventOneRisk',
                            name: 'eventOne'
                        },
                        {
                            xtype: 'ViewComboEventTwo',
                            hidden: hidden('WRWEventTwo'),
                            anchor: '70%',
                            disabled: true,
                            fieldLabel: 'Tipo de evento n.2',
                            action: 'selectEventTwoRisk',
                            name: 'eventTwo'
                        },
                        {
                            xtype: 'ViewComboEventThree',
                            anchor: '70%',
                            disabled: true,
                            hidden: hidden('WRWEventThree'),
                            fieldLabel: 'Tipo de evento n.3',
                            name: 'eventThree'
                        },
                        {
                            xtype: 'textfield',
                            name: 'average',
                            value: '0',
                            hidden: true
                        }
                    ]
                }
            ],
            buttonAlign: 'center',
            buttons: [
                {
                    text: 'Guardar',
                    scale: 'medium',
                    action: 'saveDebilityRiskOperational',
                    iconCls: 'save'
                },
                {
                    text: 'Salir',
                    scale: 'medium',
                    handler: function () {
                        me.close()
                    },
                    iconCls: 'logout'
                }
            ]
        });
        me.callParent(arguments);
    }
});