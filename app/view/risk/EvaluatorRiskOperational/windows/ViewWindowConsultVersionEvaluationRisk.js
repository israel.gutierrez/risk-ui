Ext.define("ModelFileAttach", {
  extend: "Ext.data.Model",
  fields: [
    "idVersionEvaluation",
    "idRisk",
    "managerRisk",
    "fullName",
    "description",
    "dateEvaluation",
    "state",
    "stateCheck"
  ]
});

var storeFileAttach = Ext.create("Ext.data.Store", {
  model: "ModelFileAttach",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListFileAttachmentsDetail.htm",
    extraParams: {},
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowConsultVersionEvaluationRisk",
  {
    extend: "Ext.window.Window",
    border: false,
    alias: "widget.ViewWindowConsultVersionEvaluationRisk",
    height: 383,
    width: 589,
    layout: {
      type: "fit"
    },
    title: "DOCUMENTOS ADJUNTOS",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "textfield",
            itemId: "id",
            name: "id",
            hidden: true
          },
          {
            xtype: "gridpanel",
            store: storeFileAttach,
            flex: 1,
            columns: [
              {
                xtype: "rownumberer",
                width: 25,
                sortable: false
              },
              {
                dataIndex: "idVersionEvaluation",
                width: 60,
                text: "CODIGO"
              },
              {
                dataIndex: "fullName",
                flex: 1,
                text: "RESPONSABLE"
              },
              {
                dataIndex: "dateEvaluation",
                flex: 1,
                text: "FECHA"
              },
              {
                dataIndex: "description",
                flex: 1,
                text: "VERSION"
              },
              {
                xtype: "actioncolumn",
                header: "IR VERSION",
                align: "center",
                width: 80,
                items: [
                  {
                    icon: "images/ir.png",
                    handler: function(grid, rowIndex) {
                      Ext.Ajax.request({
                        method: "POST",
                        url:
                          "http://localhost:9000/giro/consultAnyVersionEvaluation.htm",
                        params: {
                          idVersionEvaluation: grid.store
                            .getAt(rowIndex)
                            .get("idVersionEvaluation"),
                          idRisk: grid.store.getAt(rowIndex).get("idRisk")
                        },
                        success: function(response) {
                          response = Ext.decode(response.responseText);
                          if (response.success) {
                            var grid2 = Ext.ComponentQuery.query(
                              "ViewPanelIdentifyRiskOperational grid[name=riskIdentify]"
                            )[0];
                            grid2.store.getProxy().extraParams = {
                              idRiskEvaluationMatrix: grid2
                                .getSelectionModel()
                                .getSelection()[0]
                                .get("idRiskEvaluationMatrix")
                            };
                            grid2.store.getProxy().url =
                              "http://localhost:9000/giro/getRiskByVersionEvaluation.htm";
                            grid2.down("pagingtoolbar").moveFirst();
                            me.close();
                          } else {
                            DukeSource.global.DirtyView.messageAlert(
                              DukeSource.global.GiroMessages.TITLE_WARNING,
                              response.mensaje,
                              Ext.Msg.ERROR
                            );
                          }
                        },
                        failure: function() {}
                      });
                    }
                  }
                ]
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: 50,
              store: storeFileAttach,
              items: [
                {
                  xtype:"UpperCaseTrigger",
                  width: 120,
                  action: "searchGridAllAgency"
                }
              ]
            }
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
