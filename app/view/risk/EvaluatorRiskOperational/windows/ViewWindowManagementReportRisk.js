Ext.define("ModelFileAttach", {
  extend: "Ext.data.Model",
  fields: ["correlative", "idRisk", "nameFile", "fullName"]
});

var storeFileAttach = Ext.create("Ext.data.Store", {
  model: "ModelFileAttach",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListFileAttachmentsDetail.htm",
    extraParams: {},
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowManagementReportRisk",
  {
    extend: "Ext.window.Window",
    border: false,
    alias: "widget.ViewWindowManagementReportRisk",
    height: 383,
    width: 589,

    layout: {
      align: "stretch",
      type: "vbox"
    },
    title: "Documentos adjuntos",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            padding: "0 0 2 0",
            bodyPadding: "10",
            items: [
              {
                xtype: "textfield",
                name: "idRisk",
                hidden: true
              },
              {
                xtype: "filefield",
                anchor: "100%",
                allowBlank: false,
                msgTarget: "side",
                fieldCls: "obligatoryTextField",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                itemId: "text",
                name: "text",
                buttonConfig: {
                  iconCls: "tesla even-attachment"
                },
                buttonText: "",
                fieldLabel: "Archivo"
              }
            ],
            bbar: [
              "->",
              "-",
              {
                text: "Guardar",
                iconCls: "save",
                handler: function() {
                  var form = me.down("form");
                  var grid = me.down("grid");
                  if (form.getForm().isValid()) {
                    form.getForm().submit({
                      url:
                        "http://localhost:9000/giro/saveFileAttachmentsRisk.htm?nameView=PanelProcessRiskEvaluation",
                      waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
                      method: "POST",
                      success: function(form, action) {
                        var valor = Ext.decode(action.response.responseText);
                        if (valor.success) {
                          DukeSource.global.DirtyView.messageNormal(valor.message);

                          grid.store.getProxy().extraParams = {
                            idRisk: Ext.ComponentQuery.query(
                              "ViewWindowManagementReportRisk"
                            )[0]
                              .down("textfield[name=idRisk]")
                              .getValue()
                          };
                          grid.store.getProxy().url =
                            "http://localhost:9000/giro/showListFileAttachmentsRiskActives.htm";
                          grid.down("pagingtoolbar").doRefresh();
                        } else {
                          DukeSource.global.DirtyView.messageWarning(
                            DukeSource.global.GiroMessages.TITLE_WARNING
                          );
                        }
                      },
                      failure: function(form, action) {
                        var valor = Ext.decode(action.response.responseText);
                        if (!valor.success) {
                          DukeSource.global.DirtyView.messageWarning(valor.message);
                        }
                      }
                    });
                  } else {
                    DukeSource.global.DirtyView.messageWarning(
                      DukeSource.global.GiroMessages.TITLE_WARNING
                    );
                  }
                }
              },
              "-"
            ]
          },
          {
            xtype: "gridpanel",
            store: storeFileAttach,
            flex: 1,
            columns: [
              {
                dataIndex: "correlative",
                width: 60,
                text: "Código"
              },
              {
                dataIndex: "fullName",
                flex: 1,
                text: "Nombre"
              },
              {
                dataIndex: "nameFile",
                flex: 1,
                text: "Archivo"
              },
              {
                xtype: "actioncolumn",
                header: "Documento",
                align: "center",
                width: 80,
                items: [
                  {
                    icon: "images/page_white_put.png",
                    handler: function(grid, rowIndex) {
                      Ext.core.DomHelper.append(document.body, {
                        tag: "iframe",
                        id: "downloadIframe",
                        frameBorder: 0,
                        width: 0,
                        height: 0,
                        css: "display:none;visibility:hidden;height:0px;",
                        src:
                          "http://localhost:9000/giro/downloadFileAttachmentsRisk.htm?correlative=" +
                          grid.store.getAt(rowIndex).get("correlative") +
                          "&idRisk=" +
                          grid.store.getAt(rowIndex).get("idRisk") +
                          "&nameFile=" +
                          grid.store.getAt(rowIndex).get("nameFile")
                      });
                    }
                  }
                ]
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: 50,
              store: storeFileAttach,
              items: [
                {
                  xtype:"UpperCaseTrigger",
                  width: 120,
                  action: "searchGridAllAgency"
                }
              ]
            },
            listeners: {
              render: function() {
                var me = this;
                me.store.getProxy().extraParams = {
                  idRisk: Ext.ComponentQuery.query(
                    "ViewWindowManagementReportRisk"
                  )[0]
                    .down("textfield[name=idRisk]")
                    .getValue()
                };
                me.store.getProxy().url =
                  "http://localhost:9000/giro/showListFileAttachmentsRiskActives.htm";
                me.down("pagingtoolbar").moveFirst();
              }
            }
          }
        ],
        buttons: [
          {
            text: "Salir",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);
