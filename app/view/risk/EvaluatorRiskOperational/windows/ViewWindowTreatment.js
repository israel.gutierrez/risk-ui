Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowTreatment', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowTreatment',
    requires: ['DukeSource.view.risk.parameter.combos.ViewComboRiskAnswer'],
    width: 400,
    layout: {
        type: 'fit'
    },
    title: 'TRATAMIENTO',
    titleAlign: 'center',
    border: false,
    initComponent: function () {
        var me = this;
        var idRisk = me.idRisk;
        var originTreatment = me.originTreatment;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'textfield',
                            hidden: true,
                            name: 'versionCorrelative'
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            name: 'idRisk'
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            itemId: 'idTreatmentRisk',
                            value: 'id',
                            name: 'idTreatmentRisk'
                        },
                        {
                            xtype: 'ViewComboRiskAnswer',
                            anchor: '100%',
                            labelWidth: 120,
                            msgTarget: 'side',
                            fieldCls: 'obligatoryTextField',
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            allowBlank: false,
                            fieldLabel: 'Respuesta al riesgo',
                            name: 'riskAnswer'
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            maxLengthText: DukeSource.global.GiroMessages.MESSAGE_MAX_CHARACTER + 300,
                            maxLength: 300,
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            labelWidth: 120,
                            height: 70,
                            fieldLabel: 'Descripci&oacute;n mecanismo',
                            name: 'description'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    action: 'saveTreatmentRiskOperational',
                    scale: 'medium',
                    iconCls: 'save'
                },
                {
                    text: 'Salir',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});
