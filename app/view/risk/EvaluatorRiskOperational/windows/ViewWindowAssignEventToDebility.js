Ext.define("ModelWeaknessEventAll", {
  extend: "Ext.data.Model",
  fields: [
    "idRelated",
    "descriptionRelated",
    "idRisk",
    "idVersionEvaluation",
    "idRiskWeaknessDetail",
    "idRelation",
    "idWeaknessEvent"
  ]
});
Ext.define("ModelWeaknessEventSelected", {
  extend: "Ext.data.Model",
  fields: [
    "idRelated",
    "idRiskWeaknessDetail",
    "idRisk",
    "idVersionEvaluation",
    "descriptionRelated",
    "idRelation",
    "idWeaknessEvent"
  ]
});
var StoreWeaknessEventAll = Ext.create("Ext.data.Store", {
  model: "ModelWeaknessEventAll",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/getDetailIncidentsByState.htm",
    extraParams: {
      stateIncident: "I"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
//storeRolAll.load();

var StoreWeaknessEventSelected = Ext.create("Ext.data.Store", {
  model: "ModelWeaknessEventSelected",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListUserRolesActives.htm",
    extraParams: {
      propertyFind: "description",
      valueFind: "",
      propertyOrder: "description"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
//storeRolSelected.load();

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowAssignEventToDebility",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowAssignEventToDebility",
    layout: {
      align: "stretch",
      type: "vbox"
    },
    height: 500,
    width: 1000,
    closable: false,
    anchorSize: 100,
    titleAlign: "center",

    initComponent: function() {
      var riskIndicator = this.riskIndicator;
      var debility = this.debility;
      var origin = this.origin;
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            height: 45,
            padding: "2 2 2 2",
            bodyPadding: 10,
            items: [
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "combobox",
                    value: "2",
                    labelWidth: 40,
                    width: 190,
                    padding: "0 10 0 0",
                    fieldLabel: "TIPO",
                    store: [
                      //                                        ['1','CODIGO'],
                      ["2", "DESCRIPCION"]
                    ]
                  },
                  {
                    xtype:"UpperCaseTextField",
                    flex: 2,
                    listeners: {
                      specialkey: function(field, e) {
                        var property = "";
                        if (
                          Ext.ComponentQuery.query(
                            "ViewWindowAssignEventToDebility combobox"
                          )[0].getValue() == "1"
                        ) {
                          property = "codeRisk";
                        } else {
                          property = "descriptionShortEvent";
                        }
                        if (e.getKey() === e.ENTER) {
                          var grid = me.down("grid");
                          grid.store.getProxy().extraParams = {
                            versionCorrelative:
                              debility.data["versionCorrelative"],
                            idRiskWeaknessDetail:
                              debility.data["idRiskWeaknessDetail"],
                            idRisk: debility.data["idRisk"],
                            idRelation: debility.data["idRelation"],
                            propertyFind: property,
                            valueFind: field.getValue(),
                            propertyOrder: "idRisk"
                          };
                          grid.store.getProxy().url =
                            "http://localhost:9000/giro/findWeaknessEventNoRelational.htm";
                          grid.down("pagingtoolbar").moveFirst();
                        }
                      }
                    }
                  }
                ]
              }
            ]
          },
          {
            xtype: "container",
            flex: 3,
            padding: "2 2 2 2",
            layout: {
              align: "stretch",
              type: "hbox"
            },
            items: [
              {
                xtype: "gridpanel",
                padding: "0 1 0 0",
                itemId: "gridWeaknessEventAll",
                store: StoreWeaknessEventAll,
                loadMask: true,
                columnLines: true,
                flex: 1,
                title: "DISPONIBLES",
                titleAlign: "center",
                multiSelect: true,
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    dataIndex: "descriptionRelated",
                    flex: 5,
                    text: "DESCRIPCION"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  store: StoreWeaknessEventAll,
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  render: function(grid) {
                    grid.store.getProxy().extraParams = {
                      versionCorrelative: debility.data["versionCorrelative"],
                      idRiskWeaknessDetail:
                        debility.data["idRiskWeaknessDetail"],
                      idRisk: debility.data["idRisk"],
                      idRelation: debility.data["idRelation"]
                    };
                    grid.store.getProxy().url =
                      "http://localhost:9000/giro/findWeaknessEventNoRelational.htm";
                    grid.down("pagingtoolbar").moveFirst();
                  },
                  itemdblclick: function(grid, record, item, index, e, eOpts) {
                    Ext.MessageBox.show({
                      title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                      msg: "ESTA SEGURO DE ASIGNAR EL EVENTO:",
                      icon: Ext.Msg.QUESTION,
                      buttonText: {
                        yes: "Si"
                      },
                      buttons: Ext.MessageBox.YESNO,
                      fn: function(btn) {
                        if (btn == "yes") {
                          Ext.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/saveWeaknessEventAndIncidents.htm?nameView=PanelProcessRiskEvaluation",
                            params: {
                              jsonData: Ext.JSON.encode(record.data)
                              //                                                        idTypeMatrix:panel.idTypeMatrix
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              if (response.success) {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_MESSAGE,
                                  response.mensaje,
                                  Ext.Msg.INFO
                                );
                                grid.getStore().load();
                                me.down("#gridWeaknessEventSelected")
                                  .getStore()
                                  .load();
                                //                                                            grid.store.getProxy().extraParams = {
                                //                                                                versionCorrelative:debility.data['versionCorrelative'],
                                //                                                                idRiskWeaknessDetail:debility.data['idRiskWeaknessDetail'],
                                //                                                                idRisk:debility.data['idRisk'],
                                //                                                                idRelation:debility.data['idRelation']
                                //                                                            };
                                //                                                            grid.store.getProxy().url = 'findWeaknessEventNoRelational.htm';
                                //                                                            grid.down('pagingtoolbar').moveFirst();
                              } else {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_ERROR,
                                  response.mensaje,
                                  Ext.Msg.ERROR
                                );
                              }
                            },
                            failure: function() {}
                          });
                        }
                      }
                    });
                  }
                }
              },
              {
                xtype: "gridpanel",
                padding: "0 0 0 1",
                itemId: "gridWeaknessEventSelected",
                store: StoreWeaknessEventSelected,
                loadMask: true,
                columnLines: true,
                multiSelect: true,
                flex: 1,
                title: "ASIGNADOS",
                titleAlign: "center",
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    dataIndex: "descriptionRelated",
                    flex: 1,
                    text: "ROL"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  store: StoreWeaknessEventSelected,
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  afterrender: function(grid) {
                    grid.store.getProxy().extraParams = {
                      versionCorrelative: debility.data["versionCorrelative"],
                      idRiskWeaknessDetail:
                        debility.data["idRiskWeaknessDetail"],
                      idRisk: debility.data["idRisk"],
                      idRelation: debility.data["idRelation"]
                    };
                    grid.store.getProxy().url =
                      "http://localhost:9000/giro/findWeaknessEventRelational.htm";
                    grid.down("pagingtoolbar").moveFirst();
                    //                                    var me = this;
                    //                                    grid.store.getProxy().extraParams = {idKeyRiskIndicator: propertyFind, valueFind: value, propertyOrder: propertyOrder};
                    //                                    grid.store.getProxy().url = 'findWeaknessEventRelational.htm';
                    //                                    grid.down('pagingtoolbar').moveFirst();
                    //                                    DukeSource.global.DirtyView.searchPaginationGridNormal('',me, me.down('pagingtoolbar'),'showListCatalogRiskActives.htm','description','idRisk')
                  },
                  itemdblclick: function(grid, record, item, index, e, eOpts) {
                    Ext.MessageBox.show({
                      title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                      msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
                      icon: Ext.Msg.QUESTION,
                      buttonText: {
                        yes: "Si"
                      },
                      buttons: Ext.MessageBox.YESNO,
                      fn: function(btn) {
                        if (btn == "yes") {
                          Ext.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/deleteWeaknessEventAndIncidents.htm?nameView=PanelProcessRiskEvaluation",
                            params: {
                              jsonData: Ext.JSON.encode(record.data)
                              //                                                        idTypeMatrix:panel.idTypeMatrix
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              if (response.success) {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_MESSAGE,
                                  response.mensaje,
                                  Ext.Msg.INFO
                                );
                                grid.getStore().load();
                                me.down("#gridWeaknessEventAll")
                                  .getStore()
                                  .load();
                                //                                                            grid.store.getProxy().extraParams = {
                                //                                                                versionCorrelative:debility.data['versionCorrelative'],
                                //                                                                idRiskWeaknessDetail:debility.data['idRiskWeaknessDetail'],
                                //                                                                idRisk:debility.data['idRisk'],
                                //                                                                idRelation:debility.data['idRelation']
                                //                                                            };
                                //                                                            grid.store.getProxy().url = 'findWeaknessEventRelational.htm';
                                //                                                            grid.down('pagingtoolbar').moveFirst();
                              } else {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_ERROR,
                                  response.mensaje,
                                  Ext.Msg.ERROR
                                );
                              }
                            },
                            failure: function() {}
                          });
                        }
                      }
                    });
                  }
                }
              }
            ]
          }
        ],
        buttons: [
          {
            text: "SALIR",
            iconCls: "logout",
            //                    scope: this,
            //                    handler: this.close
            handler: function() {
              if (me.origin == "Inherint") {
                Ext.ComponentQuery.query(
                  "ViewPanelEvaluationRiskOperational"
                )[0]
                  .down("#riskDebilityStart")
                  .getStore()
                  .load();
              } else {
                Ext.ComponentQuery.query(
                  "ViewPanelEvaluationRiskOperational"
                )[0]
                  .down("#riskDebility")
                  .getStore()
                  .load();
              }
              me.close();
            }
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
