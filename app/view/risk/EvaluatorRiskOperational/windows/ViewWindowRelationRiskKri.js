Ext.define("ModelRiskAll", {
  extend: "Ext.data.Model",
  fields: [
    "codeRisk",
    "catalogRisk",
    "keyRiskIndicator",
    "nameKeyRiskIndicator",
    "indicatorQualification",
    "codeKri",
    "importance"
  ]
});

Ext.define("ModelRiskSelected", {
  extend: "Ext.data.Model",
  fields: [
    "abbreviation",
    "calculatingFrequency",
    "codeKri",
    "codesRisk",
    "coverage",
    "description",
    "descriptionCalculatingFrequency",
    "factorRisk",
    "formCalculating",
    "fullName",
    "idActivity",
    "idKeyRiskIndicator",
    "indicatorConfiguration",
    "indicatorCriteria",
    "indicatorKri",
    "indicatorNormalizer",
    "indicatorQualification",
    "initPeriod",
    "initYear",
    "jobPlace",
    "managerRisk",
    "modeEvaluation",
    "month",
    "nameKri",
    "numberRiskAssociated",
    "process",
    "remarks",
    "state",
    "subProcess",
    "typeProcess",
    "unitMeasure",
    "workArea",
    "year",
    "yearMonth"
  ]
});
var storeRiskAll = Ext.create("Ext.data.Store", {
  model: "ModelRiskAll",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListRolesActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

var storeRiskSelected = Ext.create("Ext.data.Store", {
  model: "ModelRiskSelected",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListUserRolesActives.htm",
    extraParams: {
      propertyFind: "description",
      valueFind: "",
      propertyOrder: "description"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowRelationRiskKri",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowRelationRiskKri",
    layout: "fit",
    height: 600,
    width: 1000,
    titleAlign: "center",

    initComponent: function() {
      var riskIndicator = this.riskIndicator;
      var idRisk = this.idRisk;
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "container",
            padding: 1,
            layout: {
              align: "stretch",
              type: "hbox"
            },
            items: [
              {
                xtype: "container",
                flex: 1,
                layout: {
                  type: "vbox",
                  align: "stretch"
                },
                items: [
                  {
                    xtype: "form",
                    height: 70,
                    padding: 2,
                    bodyPadding: 10,
                    items: [
                      {
                        xtype: "container",
                        anchor: "100%",
                        height: 26,
                        layout: {
                          type: "hbox"
                        },
                        items: [
                          {
                            xtype: "combobox",
                            value: "2",
                            labelWidth: 65,
                            width: 190,
                            padding: "0 10 0 0",
                            fieldLabel: "TIPO",
                            store: [
                              ["1", "CODIGO"],
                              ["2", "DESCRIPCION"]
                            ]
                          },
                          {
                            xtype:"UpperCaseTextField",
                            flex: 2,
                            listeners: {
                              specialkey: function(field, e) {
                                var property = "";
                                if (
                                  Ext.ComponentQuery.query(
                                    "ViewWindowSearchRiskByKri combobox"
                                  )[0].getValue() == "1"
                                ) {
                                  property = "codeRisk";
                                } else {
                                  property = "description";
                                }
                                if (e.getKey() === e.ENTER) {
                                  var grid = me.down("grid");
                                  grid.store.getProxy().extraParams = {
                                    propertyFind: property,
                                    valueFind: field.getValue(),
                                    propertyOrder: "idRisk",
                                    keyRiskIndicator: riskIndicator
                                  };
                                  grid.store.getProxy().url =
                                    "http://localhost:9000/giro/findRelationRiskKri.htm";
                                  grid.down("pagingtoolbar").moveFirst();
                                }
                              }
                            }
                          }
                        ]
                      },
                      {
                        xtype: "combobox",
                        labelWidth: 65,
                        fieldCls: "obligatoryTextField",
                        width: 190,
                        name: "importance",
                        itemId: "importance",
                        fieldLabel: "PRINCIPAL",
                        store: [
                          ["S", "SI"],
                          ["N", "NO"]
                        ]
                      }
                    ]
                  },
                  {
                    xtype: "gridpanel",
                    padding: "0 0 0 1",
                    itemId: "gridRiskSelected",
                    store: storeRiskSelected,
                    loadMask: true,
                    columnLines: true,
                    multiSelect: true,
                    flex: 1,
                    title: "KRIS DISPONIBLES",
                    titleAlign: "center",
                    columns: [
                      {
                        xtype: "rownumberer",
                        width: 25,
                        sortable: false
                      },
                      {
                        dataIndex: "codeKri",
                        width: 60,
                        text: "CODIGO"
                      },
                      {
                        dataIndex: "description",
                        width: 350,
                        text: "KRI"
                      },
                      {
                        dataIndex: "codesRisk",
                        width: 120,
                        text: "RIESGOS"
                      }
                    ],
                    viewConfig: {
                      getRowClass: function(record) {
                        return record.get("indicatorQualification") == "S"
                          ? "finish-row"
                          : "pending-row";
                      }
                    },
                    bbar: {
                      xtype: "pagingtoolbar",
                      pageSize: 50,
                      store: storeRiskSelected
                    },
                    listeners: {
                      render: function(grid) {
                        DukeSource.global.DirtyView.searchPaginationGridNormal(
                          "",
                          grid,
                          grid.down("pagingtoolbar"),
                          "http://localhost:9000/giro/showListKeyRiskIndicatorActives.htm",
                          "",
                          "idKeyRiskIndicator"
                        );
                      },
                      itemdblclick: function(grid, record, item, index, e) {
                        if (me.down("#importance").getValue() == undefined) {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_MESSAGE,
                            "SELECCIONE SI ES PRINCIPAL, O NO",
                            Ext.Msg.ERROR
                          );
                        } else {
                          Ext.MessageBox.show({
                            title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                            msg: "Esta seguro que desea relacionar el KRI",
                            icon: Ext.Msg.QUESTION,
                            buttonText: {
                              yes: "Si"
                            },
                            buttons: Ext.MessageBox.YESNO,
                            fn: function(btn) {
                              if (btn == "yes") {
                                DukeSource.lib.Ajax.request({
                                  method: "POST",
                                  url:
                                    "http://localhost:9000/giro/saveRelationRiskKri.htm?nameView=PanelProcessRiskEvaluation",
                                  params: {
                                    keyRiskIndicator:
                                      record.raw["idKeyRiskIndicator"],
                                    idRisk: idRisk,
                                    importance: me
                                      .down("#importance")
                                      .getValue()
                                  },
                                  success: function(response) {
                                    response = Ext.decode(
                                      response.responseText
                                    );
                                    if (response.success) {
                                      DukeSource.global.DirtyView.messageAlert(
                                        DukeSource.global.GiroMessages
                                          .TITLE_MESSAGE,
                                        response.mensaje,
                                        Ext.Msg.INFO
                                      );
                                      me.down("#gridRiskAll")
                                        .down("pagingtoolbar")
                                        .moveFirst();
                                    } else {
                                      DukeSource.global.DirtyView.messageAlert(
                                        DukeSource.global.GiroMessages
                                          .TITLE_WARNING,
                                        response.mensaje,
                                        Ext.Msg.ERROR
                                      );
                                    }
                                  },
                                  failure: function() {}
                                });
                              }
                            }
                          });
                        }
                      }
                    }
                  }
                ]
              },
              {
                xtype: "gridpanel",
                padding: 1,
                itemId: "gridRiskAll",
                store: storeRiskAll,
                loadMask: true,
                columnLines: true,
                flex: 1,
                title: "KRIS ASIGNADOS",
                titleAlign: "center",
                multiSelect: true,
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    dataIndex: "codeKri",
                    width: 100,
                    align: "center",
                    text: "CODIGO"
                  },
                  {
                    dataIndex: "importance",
                    width: 90,
                    align: "center",
                    text: "PRINCIPAL",
                    renderer: function(
                      value,
                      metaData,
                      record,
                      row,
                      col,
                      store,
                      gridView
                    ) {
                      if (value == "S") return "SI";
                      else return "NO";
                    }
                  },
                  {
                    dataIndex: "nameKeyRiskIndicator",
                    flex: 500,
                    text: "KRI"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: 50,
                  store: storeRiskAll,
                  items: [
                    {
                      xtype:"UpperCaseTrigger",
                      width: 120,
                      action: "searchGridAllRole"
                    }
                  ]
                },
                viewConfig: {
                  getRowClass: function(record) {
                    return record.get("indicatorQualification") == "S"
                      ? "finish-row"
                      : "pending-row";
                  }
                },
                listeners: {
                  render: function(grid) {
                    grid.store.getProxy().extraParams = {
                      idRisk: idRisk
                    };
                    grid.store.getProxy().url =
                      "http://localhost:9000/giro/findListKriAssignedByRisk.htm";
                    grid.down("pagingtoolbar").moveFirst();
                  },
                  itemdblclick: function(grid, record, item, index, e) {
                    Ext.MessageBox.show({
                      title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                      msg: "Esta seguro que desea eliminar el KRI",
                      icon: Ext.Msg.QUESTION,
                      buttonText: {
                        yes: "Si"
                      },
                      buttons: Ext.MessageBox.YESNO,
                      fn: function(btn) {
                        if (btn == "yes") {
                          DukeSource.lib.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/deleteRelationRiskKri.htm?nameView=PanelProcessRiskEvaluation",
                            params: {
                              idKri: record.raw["keyRiskIndicator"],
                              idRisk: idRisk
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              if (response.success) {
                                me.down("#gridRiskAll")
                                  .down("pagingtoolbar")
                                  .moveFirst();
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_MESSAGE,
                                  response.mensaje,
                                  Ext.Msg.INFO
                                );
                              } else {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_WARNING,
                                  response.mensaje,
                                  Ext.Msg.ERROR
                                );
                              }
                            },
                            failure: function() {}
                          });
                        }
                      }
                    });
                  }
                }
              }
            ]
          }
        ],
        buttons: [
          {
            text: "SALIR",
            scale: "medium",
            iconCls: "logout",
            scope: this,
            handler: this.close
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
