Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowRegisterDetailImpact', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowRegisterDetailImpact',
    requires: [
        'Ext.form.Panel',
        'Ext.form.field.TextArea'
    ],

    //height: 356,
    width: 600,
    border: false,
    layout: 'fit',
    titleAlign: 'center',
    title: 'DETALLE CUANTIFICACI&Oacute;N',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'NumberDecimalNumberObligatory',
                            anchor: '60%',
                            fieldLabel: 'P&Eacute;RDIDA DIRECTA',
                            labelWidth: 120
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            fieldLabel: 'COMENTARIO',
                            labelWidth: 120
                        },
                        {
                            xtype: 'NumberDecimalNumberObligatory',
                            anchor: '60%',
                            fieldLabel: 'P&Eacute;RDIDA INDIRECTA',
                            labelWidth: 120
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            fieldLabel: 'COMENTARIO',
                            labelWidth: 120
                        },
                        {
                            xtype: 'NumberDecimalNumberObligatory',
                            anchor: '60%',
                            fieldLabel: 'LUCRO CESANTE',
                            labelWidth: 120
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            fieldLabel: 'COMENTARIO',
                            labelWidth: 120
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });

        me.callParent(arguments);
    }

});
