Ext.define("ModelGridMatrixByGroupRisk", {
  extend: "Ext.data.Model",
  fields: ["descriptionScaleResidual", "levelColourResidual", "totalRisk"]
});

var StoreGridMatrixByGroupRisk = Ext.create("Ext.data.Store", {
  model: "ModelGridMatrixByGroupRisk",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowMatrixByGroupRisk",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowMatrixByGroupRisk",
    layout: "fit",
    anchorSize: 100,
    title: "MATRIZ GENERAL POR GRUPO RIESGO",
    titleAlign: "center",
    //    width:820,
    //    height:350,
    initComponent: function() {
      var me = this;
      var idRiskEvaluationMatrix = this.idRiskEvaluationMatrix;
      Ext.applyIf(me, {
        dockedItems: [
          {
            xtype: "toolbar",
            dock: "top",
            items: [
              {
                xtype: "ViewComboTypeMatrix",
                width: 400,
                fieldLabel: "TIPO DE MATRIZ",
                padding: "0 5 0 0",
                allowBlank: false,
                msgTarget: "side",
                fieldCls: "obligatoryTextField",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                //                                    action: 'selectProcessTypeRisk',
                name: "typeMatrix",
                tpl: Ext.create(
                  "Ext.XTemplate",
                  '<tpl for=".">',
                  '<div class="x-boundlist-item">{description} | {stateApproved}</div>',
                  "</tpl>"
                ),
                // template for the content inside text field
                displayTpl: Ext.create(
                  "Ext.XTemplate",
                  '<tpl for=".">',
                  "{description} | {stateAssigned}",
                  "</tpl>"
                ),
                listeners: {
                  render: function(cbo) {
                    cbo.getStore().load();
                  }
                }
              },
              {
                xtype: "ViewComboOperationalRiskExposition",
                labelWidth: 150,
                fieldLabel: "PATRIMONIO EFECTIVO",
                listeners: {
                  select: function(cbo, records) {},
                  render: function(me) {
                    me.store.load();
                  }
                }
              },
              "-",
              {
                xtype: "button",
                iconCls: "save",
                text: "GENERAR",
                handler: function() {
                  Ext.Ajax.request({
                    method: "POST",
                    url:
                      "http://localhost:9000/giro/getRiskByGraphicsByBusiness.htm",
                    params: {
                      idOperationalRiskExposition: me
                        .down("ViewComboOperationalRiskExposition")
                        .getValue(),
                      idTypeMatrix: me.down("ViewComboTypeMatrix").getValue(),
                      idRiskEvaluationMatrix: idRiskEvaluationMatrix
                    },
                    success: function(response) {
                      response = Ext.decode(response.responseText);
                      if (response.success) {
                        me.down("chart")
                          .getStore()
                          .loadData(response.data);
                        me.down("chart").redraw();
                      } else {
                        DukeSource.global.DirtyView.messageAlert(
                          DukeSource.global.GiroMessages.TITLE_ERROR,
                          response.mensaje,
                          Ext.Msg.ERROR
                        );
                      }
                    },
                    failure: function() {}
                  });
                }
              }
            ]
          }
        ],
        items: [
          {
            xtype: "chart",
            padding: "2 2 2 0",
            tbar: [{ text: "NOMBRE" }],
            flex: 1,
            store: Ext.create("Ext.data.Store", {
              model: "ModelGridMatrixByGroupRisk",
              //                        autoLoad:true,
              proxy: {
                actionMethods: {
                  create: "POST",
                  read: "POST",
                  update: "POST"
                },
                type: "ajax",
                url:
                  "http://localhost:9000/giro/getRiskByGraphicsByBusiness.htm",
                //                            extraParams:{
                //                                idRisk:idRisk
                //                            },
                reader: {
                  totalProperty: "totalCount",
                  root: "data",
                  successProperty: "success"
                }
              }
            }),
            region: "east",
            animate: true,
            shadow: true,
            insetPadding: 20,
            legend: {
              visible: true,
              position: "right",
              labelFont: "8px Arial"
            },
            axes: [
              {
                type: "Numeric",
                position: "left",
                fields: ["totalRisk"],
                //                                        title: 'RATIO',
                roundToDecimal: false,
                label: {
                  renderer: Ext.util.Format.numberRenderer("0")
                  //                                                renderer: function(v) {
                  //                                                    return String(v);
                  //                                                }
                },
                grid: true
              },

              {
                type: "Category",
                position: "bottom",
                fields: ["descriptionScaleResidual"]
                //                                        ,
                //                                        title: 'TIEMPO'
              }
            ],
            series: [
              {
                type: "column",
                label: {
                  display: "outside",
                  "text-anchor": "middle",
                  field: ["totalRisk"],
                  renderer: Ext.util.Format.numberRenderer("0"),
                  orientation: "horizontal",
                  //                                            color: '#333'
                  font: "bold 14px Arial"
                },
                renderer: function(sprite, storeItem, barAttr, i, store) {
                  var colors = ["#0000FF", "#00FF00", "#FFFF00", "FF0000"]; //,'#D5544F','#5288DB'
                  barAttr.fill = colors[i % colors.length];
                  return barAttr;
                },
                axis: "bottom",
                gutter: 80,
                xField: "descriptionScaleResidual",
                yField: ["totalRisk"],
                //                            title: ['RIESGO INHERENTE','RIESGO RESIDUAL'],
                style: {
                  opacity: 0.93
                }
              }
            ]
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
