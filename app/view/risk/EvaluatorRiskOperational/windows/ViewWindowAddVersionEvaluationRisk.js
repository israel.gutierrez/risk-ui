Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowAddVersionEvaluationRisk",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowAddVersionEvaluationRisk",
    width: 350,
    layout: {
      type: "fit"
    },
    border: false,
    title: "Versi&oacute;n de evaluaci&oacute;n",
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            title: "",
            items: [
              {
                xtype: "textfield",
                name: "idVersionEvaluation",
                hidden: true,
                value: "id"
              },
              {
                xtype: "textfield",
                name: "idRisk",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "idMaxExposition",
                hidden: true,
                value: idMaxExpositionActive
              },
              {
                xtype: "textfield",
                hidden: true,
                value: "N",
                name: "isImport"
              },
              /*{
                            xtype: 'container',
                            height: 27,
                            layout: {
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'radiogroup',
                                    flex: 2,
                                    fieldLabel: 'IMPORTARCIÓN',
                                    items: [
                                        {
                                            xtype: 'radiofield',
                                            checked: true,
                                            inputValue: 'none',
                                            name: 'typeImport',
                                            boxLabel: 'NO'

                                        },
                                        {
                                            xtype: 'radiofield',
                                            inputValue: 'partial',
                                            name: 'typeImport',
                                            boxLabel: 'PARCIAL'
                                        },
                                        {
                                            xtype: 'radiofield',
                                            inputValue: 'full',
                                            name: 'typeImport',
                                            boxLabel: 'TOTAL'
                                        }
                                    ],
                                    listeners: {
                                        change: function (obj, newValue, oldValue) {
                                            if (newValue.typeImport == 'none') {
                                                me.down('#idBackVersionEvaluation').setVisible(false);
                                            } else {
                                                me.down('#idBackVersionEvaluation').setVisible(true);
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'ViewComboVersionEvaluation',
                                    itemId: 'idBackVersionEvaluation',
                                    flex: 1,
                                    hidden: true,
                                    name: 'idBackVersionEvaluation'
                                }
                            ]
                        },*/
              {
                xtype: "textfield",
                fieldCls: "readOnlyText",
                readOnly: true,
                anchor: "100%",
                name: "description",
                fieldLabel: "Evaluaci&oacute;n",
                value:
                  me.record.get("versionCorrelative") === ""
                    ? 1
                    : parseInt(me.record.get("versionCorrelative")) + 1,
                listeners: {
                  afterrender: function(field) {
                    field.focus(false, 200);
                  }
                }
              },
              {
                xtype:"UpperCaseTextFieldReadOnly",
                anchor: "100%",
                fieldLabel: "Responsable",
                name: "fullName",
                value: fullName
              },
              {
                xtype: "datefield",
                anchor: "100%",
                msgTarget: "side",
                fieldCls: "obligatoryTextField",
                format: "d/m/Y",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                allowBlank: false,
                value: new Date(),
                maxValue: new Date(),
                name: "dateEvaluation",
                fieldLabel: "Fecha"
              }
            ]
          }
        ],
        buttons: [
          {
            text: "Guardar",
            scale: "medium",
            iconCls: "save",
            handler: function() {
              var panel = Ext.ComponentQuery.query(
                "ViewPanelIdentifyRiskOperational"
              )[0];
              var gridRiskIdentify = panel.down("#riskIdentify");
              var idRisk = gridRiskIdentify
                .getSelectionModel()
                .getSelection()[0]
                .get("idRisk");
              if (
                me
                  .down("form")
                  .getForm()
                  .isValid()
              ) {
                DukeSource.lib.Ajax.request({
                  waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                  method: "POST",
                  url:
                    "http://localhost:9000/giro/saveVersionEvaluation.htm?nameView=PanelProcessRiskEvaluation",
                  params: {
                    jsonData: Ext.JSON.encode(me.down("form").getValues())
                  },
                  scope: this,
                  success: function(response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                      DukeSource.global.DirtyView.messageNormal(
                        response.message
                      );
                      var identifyIndex = DukeSource.global.DirtyView.getSelectedRowIndex(
                        gridRiskIdentify
                      );
                      gridRiskIdentify.getStore().load({
                        callback: function() {
                          if (identifyIndex != -1) {
                            gridRiskIdentify
                              .getSelectionModel()
                              .select(identifyIndex);
                          } else {
                          }
                        }
                      });
                      var combo = panel
                        .down("ViewComboGridEvaluationRisk")
                        .setVisible(true);
                      combo.getStore().load({
                        url:
                          "http://localhost:9000/giro/showVersionEvaluationActives.htm",
                        params: {
                          idRisk: idRisk
                        },
                        callback: function(records) {
                          Ext.Array.each(records, function(record) {
                            if (record.data["state"] == "S") {
                              combo.setValue(
                                record.data["idVersionEvaluation"]
                              );
                            }
                          });
                        }
                      });
                      me.close();
                    } else {
                      DukeSource.global.DirtyView.messageWarning(
                        response.message
                      );
                    }
                  },
                  failure: function() {}
                });
              } else {
                DukeSource.global.DirtyView.messageWarning(
                  DukeSource.global.GiroMessages.MESSAGE_COMPLETE
                );
              }
            }
          },
          {
            text: "Salir",
            scale: "medium",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
