Ext.define("ModelGridDashBoardManagerRisk", {
  extend: "Ext.data.Model",
  fields: [
    "idRisk",
    "idRiskEvaluationMatrix",
    "descriptionRisk", //catalog
    "descriptionProcess",
    "description", //definition
    "idCatalogRisk",
    "codeRisk",
    "descriptionFrequency",
    "descriptionImpact",
    "descriptionScaleRisk",
    "levelColour",
    "colourLost", //description risk inherint
    "valueLost", // amount risk inherint
    "percentageReductionControl",
    "scoreDescriptionControl",
    "colourControl",
    "idFrequencyReduction",
    "idImpactReduction",
    "frequencyReduction",
    "impactReduction",
    "descriptionFrequencyResidual",
    "descriptionImpactResidual",
    "descriptionScaleResidual",
    "levelColourResidual",
    "colourLostResidual",
    "valueLostResidual",
    "idFrequency",
    "equivalentFrequency",
    "idImpact",
    "equivalentImpact",
    "idFrequencyFeature",
    "idImpactFeature",
    "versionCorrelative",
    "versionState",
    "versionStateCheck",
    "evaluationFinalControl",
    "idOperationalRiskExposition",
    "maxAmount",
    "year",
    "dateEvaluation",
    "businessLineOne",
    "processType",
    "process",
    "subProcess",
    "descriptionProcess",
    "descriptionSubProcess",
    "descriptionBusinessLineOne",
    "idMatrix", // idMatrix Inherent
    "idMatrixResidual",
    "idFrequencyResidual",
    "idImpactResidual",
    "nameEvaluation",
    "typeMatrix",
    "valueResidualRisk",
    "descriptionTreatment",
    "idTreatmentRisk",
    "amountEventLost",
    "numberEventLost",
    "codesEventLost",
    "codesKri",
    "numberKriAssociated",
    "valueKri",
    "descriptionKri",
    "colorKri",
    "numberActionPlanAssociated",
    "avgActionPlan",
    "codesActionPLan",
    "numberIncidents",
    "codesIncidents",
    "reasonFrequency",
    "reasonImpact"
  ]
});

var StoreGridDashBoardManagerRisk = Ext.create("Ext.data.Store", {
  model: "ModelGridDashBoardManagerRisk",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDashboardManagerRisk",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowDashboardManagerRisk",
    layout: "fit",
    titleAlign: "center",
    width: 1100,
    height: 500,
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            padding: 2,
            store: StoreGridDashBoardManagerRisk,
            loadMask: true,
            title: "Riesgos",
            titleAlign: "center",
            columnLines: true,
            columns: [
              { header: "Id", align: "center", dataIndex: "idRisk", width: 50 },
              {
                header: "C&oacute;digo",
                dataIndex: "codeRisk",
                align: "center",
                width: 60
              },
              {
                header: "Versi&oacute;n",
                dataIndex: "versionCorrelative",
                align: "center",
                width: 60
              },
              {
                header: "Proceso",
                dataIndex: "descriptionProcess",
                width: 230,
                tdCls: "process-columnFree",
                renderer: function(value, metaData, record) {
                  var s = value.split(" &#8702; ");
                  var path = "";
                  for (var i = 0; i < s.length; i++) {
                    var text = s[i];
                    text = s[i] + "<br>";
                    path = path + " &#8702; " + text;
                  }
                  return path;
                }
              },
              {
                header: "Descripci&oacute;n del riesgo",
                dataIndex: "description",
                width: 350
              },

              {
                header: "Riesgo inherente",
                align: "center",
                dataIndex: "descriptionScaleRisk",
                width: 100,
                renderer: function(value, metaData, record) {
                  if (value !== " ") {
                    metaData.tdAttr =
                      'style="background-color: #' +
                      record.get("levelColour") +
                      ' !important;"';
                    return "<span>" + value + "</span>";
                  }
                }
              },
              {
                header: "Control",
                align: "center",
                dataIndex: "scoreDescriptionControl",
                width: 100,
                renderer: function(value, metaData, record) {
                  if (value !== " ") {
                    metaData.tdAttr =
                      'style="background-color: #' +
                      record.get("colourControl") +
                      ' !important;"';
                    return "<span>" + value + "</span>";
                  }
                }
              },
              {
                header: "Riesgo residual",
                align: "center",
                dataIndex: "descriptionScaleResidual",
                width: 90,
                renderer: function(value, metaData, record) {
                  if (value !== " ") {
                    metaData.tdAttr =
                      'style="background-color: #' +
                      record.get("levelColourResidual") +
                      ' !important;"';
                    return "<span>" + value + "</span>";
                  }
                }
              },
              {
                header: "Tratamiento",
                align: "center",
                dataIndex: "descriptionTreatment",
                width: 100
              },
              {
                header: "Nº de planes",
                align: "center",
                dataIndex: "numberActionPlanAssociated",
                width: 45
              },
              {
                header: "Avance",
                align: "center",
                dataIndex: "avgActionPlan",
                width: 110,
                renderer: function(value, metaData, record) {
                  return viewAvgActionPlan(value, record, metaData);
                }
              },
              {
                header: "Planes",
                align: "center",
                dataIndex: "codesActionPLan",
                width: 120,
                renderer: function(a) {
                  return "<span>" + a + "</span>";
                }
              },
              {
                header: "Eventos",
                align: "center",
                dataIndex: "codesEventLost",
                width: 120
              }
            ],

            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridDashBoardManagerRisk,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              items: [
                "-",
                {
                  text: "Exportar",
                  iconCls: "excel",
                  handler: function(b, e) {
                    b.up("grid").downloadExcelXml();
                  }
                }
              ],
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            }
          }
        ],
        buttonAlign: "center",
        buttons: [
          {
            text: "Salir",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);

function viewAvgActionPlan(v) {
  var tmpValue = v / 100;
  var tmpText = v + "%";
  var progressRenderer = (function() {
    var b = new Ext.ProgressBar();
    if (tmpValue <= 0.3334) {
      b.baseCls = "x-taskBar";
    } else if (tmpValue <= 0.6667) {
      b.baseCls = "x-taskBar-medium";
    } else {
      b.baseCls = "x-taskBar-high";
    }
    return function(pValue, pText) {
      b.updateProgress(pValue, pText, true);
      return Ext.DomHelper.markup(b.getRenderTree());
    };
  })(tmpValue, tmpText);
  return progressRenderer(tmpValue, tmpText);
}
