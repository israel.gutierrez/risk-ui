Ext.define("ModelGridPanelRiskOperational", {
  extend: "Ext.data.Model",
  fields: [
    "idRisk",
    "idRiskEvaluationMatrix",
    "descriptionRisk",
    "codeRisk",
    "descriptionFrequency",
    "descriptionImpact",
    "descriptionScaleRisk",
    "levelColour",
    "colourLost", //description risk inherint
    "valueLost", // amount risk inherint
    "percentageReductionControl",
    "scoreDescriptionControl",
    "colourControl",
    "idFrequencyReduction",
    "idImpactReduction",
    "frequencyReduction",
    "impactReduction",
    "descriptionFrequencyResidual",
    "descriptionImpactResidual",
    "descriptionScaleResidual",
    "levelColourResidual",
    "colourLostResidual",
    "valueLostResidual",
    "idFrequency",
    "equivalentFrequency",
    "idImpact",
    "equivalentImpact",
    "idFrequencyFeature",
    "idImpactFeature",
    "versionCorrelative",
    "versionState",
    "versionStateCheck",
    "evaluationFinalControl",
    "idOperationalRiskExposition",
    "businessLineOne",
    "processType",
    "process",
    "subProcess",
    "descriptionProcess",
    "descriptionBusinessLineOne",
    "idMatrix", // idMatrix Inherent
    "idMatrixResidual",
    "idFrequencyResidual",
    "idImpactResidual",
    "nameEvaluation",
    "typeMatrix",
    "valueResidualRisk",
    "descriptionRiskAnswer",
    "idTreatmentRisk"
  ]
});

var StoreGridPanelRiskOperational = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelRiskOperational",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowControleModifyRisk",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowControleModifyRisk",
    height: 650,
    width: 900,
    border: false,
    layout: {
      type: "fit"
    },
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            margins: "2 2 2 2",
            itemId: "riskIdentify",
            name: "riskIdentify",
            store: StoreGridPanelRiskOperational,
            loadMask: true,
            columnLines: true,
            flex: 2,
            plugins: [
              Ext.create("Ext.grid.plugin.CellEditing", {
                clicksToEdit: 1
              })
            ],
            columns: [
              { xtype: "rownumberer", width: 25, sortable: false },
              { header: "C&Oacute;DIGO", dataIndex: "codeRisk", width: 50 },
              {
                header: "RIESGO<br>IDENTIFICADO<br>",
                dataIndex: "descriptionRisk",
                width: 350
              },
              {
                header: "INHERENTE",
                columns: [
                  {
                    header: "FRECUENCIA",
                    align: "center",
                    dataIndex: "descriptionFrequency",
                    width: 90
                  },
                  {
                    header: "IMPACTO",
                    align: "center",
                    dataIndex: "descriptionImpact",
                    width: 90
                  },
                  {
                    header: "RIESGO",
                    align: "center",
                    dataIndex: "descriptionScaleRisk",
                    width: 90,
                    renderer: function(value, metaData, record) {
                      if (value != " ") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("levelColour") +
                          ' !important;"';
                        return "<span>" + value + "</span>";
                      }
                    }
                  },
                  {
                    header: "P&Eacute;RDIDA",
                    align: "right",
                    dataIndex: "valueLost",
                    xtype: "numbercolumn",
                    format: "0,0.00",
                    width: 90,
                    renderer: function(value, metaData, record) {
                      if (value != "") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("colourLost") +
                          ' !important;"';
                        return (
                          "<span>" +
                          Ext.util.Format.number(value, "0,0.00") +
                          "</span>"
                        );
                      }
                    }
                  }
                ]
              },
              {
                header: "RESIDUAL",
                columns: [
                  {
                    header: "FRECUENCIA",
                    align: "center",
                    dataIndex: "descriptionFrequencyResidual",
                    width: 90
                  },
                  {
                    header: "IMPACTO",
                    align: "center",
                    dataIndex: "descriptionImpactResidual",
                    width: 90
                  },
                  {
                    header: "RIESGO",
                    align: "center",
                    dataIndex: "descriptionScaleResidual",
                    width: 90,
                    renderer: function(value, metaData, record) {
                      if (value != " ") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("levelColourResidual") +
                          ' !important;"';
                        return "<span>" + value + "</span>";
                      }
                    }
                  },
                  {
                    header: "P&Eacute;RDIDA",
                    align: "right",
                    dataIndex: "valueLostResidual",
                    xtype: "numbercolumn",
                    format: "0,0.00",
                    width: 90,
                    renderer: function(value, metaData, record) {
                      if (value != "") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("colourLostResidual") +
                          ' !important;"';
                        return (
                          "<span>" +
                          Ext.util.Format.number(value, "0,0.00") +
                          "</span>"
                        );
                      }
                    }
                  }
                ]
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridPanelRiskOperational,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              items: [
                "-",
                {
                  xtype:"UpperCaseTrigger",
                  fieldLabel: "FILTRAR",
                  action: "searchTriggerGridRiskIdentify",
                  labelWidth: 60,
                  width: 300
                }
              ],
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            }
            //,
            //listeners:{
            //    render:function(){
            //        var me = this;
            //        me.store.getProxy().extraParams = {idControl:''};
            //        me.store.getProxy().url = 'http://localhost:9000/giro/showListDetailRiskDetailControl.htm';
            //        me.down('toolbar').moveFirst();
            //        //DirtyView.searchPaginationGridNormal('',me, me.down('pagingtoolbar'),'http://localhost:9000/giro/findAdjustmentFactorBusiness.htm','state','state')
            //    }
            //}
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
