Ext.define("ModelGridMatrixByRisk", {
  extend: "Ext.data.Model",
  fields: []
});

var StoreGridMatrixByRisk = Ext.create("Ext.data.Store", {
  model: "ModelGridMatrixByRisk",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowMatrixByRisk",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowMatrixByRisk",
    layout: "fit",
    anchorSize: 100,
    title: "MATRIZ GENERAL POR RIESGO",
    titleAlign: "center",
    width: 820,
    height: 350,
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            padding: "2 2 2 2",
            name: "riskDebility",
            store: StoreGridMatrixByRisk,
            loadMask: true,
            columnLines: true,
            columns: [],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridMatrixByRisk,
              displayInfo: true,
              items: [
                {
                  xtype: "textfield",
                  readOnly: true,
                  labelWidth: 70,
                  width: 130,
                  fieldLabel: "R.INHERENTE",
                  fieldStyle:
                    "background-color: #3F94D5; background-image: none;"
                },
                "--",
                {
                  xtype: "textfield",
                  readOnly: true,
                  labelWidth: 65,
                  width: 125,
                  fieldLabel: "R.RESIDUAL",
                  fieldStyle:
                    "background-color: #D53F3F; background-image: none;"
                }
              ],
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            },
            listeners: {
              render: function() {
                var grid = this;
                DukeSource.global.DirtyView.loadGridDefault(grid);
              }
            }
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
