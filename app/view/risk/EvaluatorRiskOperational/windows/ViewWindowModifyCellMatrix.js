Ext.require(["Ext.ux.ColorField"]);
var sm = new Ext.selection.CheckboxModel({
  checkOnly: true
});

Ext.define("ModelGridGhostScaleRisk", {
  extend: "Ext.data.Model",
  fields: [
    "idScaleRisk",
    "description",
    "operationalRiskExposition",
    "idTypeMatrix",
    "levelColour"
  ]
});

var RiskGridGhostScaleRisk = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelGridGhostScaleRisk",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowModifyCellMatrix",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowModifyCellMatrix",
    height: 250,
    modal: true,
    width: 600,
    layout: {
      type: "fit"
    },
    title: "CONFIGURAR CELDA",
    titleAlign: "center",
    tbar: [
      "-",
      {
        xtype: "radiogroup",
        width: 200,
        fieldLabel: "RIESGO FANTASMA",
        vertical: true,
        items: [
          { boxLabel: "SI", name: "riskGhost", inputValue: "S" },
          { boxLabel: "NO", name: "riskGhost", inputValue: "N", checked: true }
        ],
        listeners: {
          change: function(field, newValue) {
            if (newValue["riskGhost"] == "S") {
              Ext.ComponentQuery.query(
                "ViewWindowModifyCellMatrix grid"
              )[0].setDisabled(true);
            } else {
              Ext.ComponentQuery.query(
                "ViewWindowModifyCellMatrix grid"
              )[0].setDisabled(false);
            }
          }
        }
      },
      "-"
    ],
    initComponent: function() {
      var idMatrix = this.idMatrix;
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            store: RiskGridGhostScaleRisk,
            padding: "2 2 2 2",
            titleAlign: "center",
            loadMask: true,
            columnLines: true,
            selModel: sm,
            columns: [
              {
                header: "C&Oacute;DIGO",
                align: "center",
                dataIndex: "idScaleRisk",
                width: 70
              },
              {
                header: "DESCRIPCI&Oacute;N",
                dataIndex: "description",
                flex: 1.5,
                editor: {
                  xtype:"UpperCaseTextField",
                  allowBlank: false
                }
              },
              {
                header: "COLOR",
                align: "center",
                dataIndex: "levelColour",
                flex: 2,
                editor: {
                  xtype: "colorfield",
                  allowBlank: false
                },
                renderer: function(value) {
                  return (
                    '<div  style="background-color:#' +
                    value +
                    "; color:#" +
                    value +
                    ';"> ' +
                    value +
                    "</div>"
                  );
                }
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: RiskGridGhostScaleRisk,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            },
            listeners: {
              render: function() {
                var windowConfiguration = Ext.ComponentQuery.query(
                  "ViewWindowConfigurationMatrix"
                )[0];
                var me = this;
                me.store.getProxy().extraParams = {
                  idTypeMatrix: windowConfiguration
                    .down("ViewComboTypeMatrix")
                    .getValue(),
                  idOperationalRiskExposition: windowConfiguration
                    .down("ViewComboOperationalRiskExposition")
                    .getValue()
                };
                me.store.getProxy().url =
                  "http://localhost:9000/giro/findInitialScaleRisk.htm";
                me.down("pagingtoolbar").moveFirst();
              }
            }
          }
        ],
        buttons: [
          {
            text: "GUARDAR",
            scale: "medium",
            action: "saveCellMatrix",
            iconCls: "save"
          },
          {
            text: "SALIR",
            scale: "medium",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);
