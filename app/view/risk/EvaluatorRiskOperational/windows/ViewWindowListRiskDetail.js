Ext.define("ModelGridPanelRisks", {
  extend: "Ext.data.Model",
  fields: [
    "idRisk",
    "idRiskEvaluationMatrix",
    "descriptionRisk",
    "description",
    "codeRisk",
    "descriptionFrequency",
    "descriptionImpact",
    "descriptionScaleRisk",
    "levelColour",
    "colourLost", //description risk inherint
    "valueLost", // amount risk inherint
    "percentageReductionControl",
    "scoreDescriptionControl",
    "colourControl",
    "idFrequencyReduction",
    "idImpactReduction",
    "frequencyReduction",
    "impactReduction",
    "descriptionFrequencyResidual",
    "descriptionImpactResidual",
    "descriptionScaleResidual",
    "levelColourResidual",
    "colourLostResidual",
    "valueLostResidual",
    "idFrequency",
    "equivalentFrequency",
    "idImpact",
    "equivalentImpact",
    "idFrequencyFeature",
    "idImpactFeature",
    "versionCorrelative",
    "versionState",
    "versionStateCheck",
    "evaluationFinalControl",
    "idOperationalRiskExposition",
    "businessLineOne",
    "processType",
    "process",
    "subProcess",
    "descriptionProcess",
    "descriptionBusinessLineOne",
    "idMatrix", // idMatrix Inherent
    "idMatrixResidual",
    "idFrequencyResidual",
    "idImpactResidual",
    "nameEvaluation",
    "typeMatrix",
    "valueResidualRisk",
    "descriptionRiskAnswer",
    "idTreatmentRisk",
    "amountEventLost",
    "numberEventLost"
  ]
});

var StoreGridPanelRisks = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelRisks",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowListRiskDetail",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowListRiskDetail",
    border: false,
    title: "RIESGOS",
    titleAlign: "center",
    height: 600,
    layout: "fit",
    width: 986,
    initComponent: function() {
      var me = this;
      Ext.apply(me, {
        items: [
          {
            xtype: "gridpanel",
            store: StoreGridPanelRisks,
            columns: [
              { xtype: "rownumberer", width: 25, sortable: false },
              { header: "C&Oacute;DIGO", dataIndex: "codeRisk", width: 50 },
              {
                header: "RIESGO<br>IDENTIFICADO<br>",
                dataIndex: "descriptionRisk",
                width: 350
              },
              {
                header: "INHERENTE",
                columns: [
                  {
                    header: "FRECUENCIA",
                    align: "center",
                    dataIndex: "descriptionFrequency",
                    width: 90
                  },
                  {
                    header: "IMPACTO",
                    align: "center",
                    dataIndex: "descriptionImpact",
                    width: 90
                  },
                  {
                    header: "RIESGO",
                    align: "center",
                    dataIndex: "descriptionScaleRisk",
                    width: 90,
                    renderer: function(value, metaData, record) {
                      if (value != " ") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("levelColour") +
                          ' !important;"';
                        return "<span>" + value + "</span>";
                      }
                    }
                  },
                  {
                    header: "P&Eacute;RDIDA",
                    align: "right",
                    dataIndex: "valueLost",
                    xtype: "numbercolumn",
                    format: "0,0.00",
                    width: 90,
                    renderer: function(value, metaData, record) {
                      if (value != "") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("colourLost") +
                          ' !important;"';
                        return (
                          "<span>" +
                          Ext.util.Format.number(value, "0,0.00") +
                          "</span>"
                        );
                      }
                    }
                  }
                ]
              },
              {
                header: "CONTROL",
                columns: [
                  //
                  {
                    header: "% REDUCCI&Oacute;N",
                    align: "right",
                    dataIndex: "evaluationFinalControl",
                    xtype: "numbercolumn",
                    format: "0,0.00",
                    width: 95,
                    renderer: function(value, metaData, record) {
                      if (value != "") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("colourControl") +
                          ' !important;"';
                        return (
                          "<span>" +
                          Ext.util.Format.number(value, "0,0.00") +
                          "</span>"
                        );
                      }
                    }
                  },
                  {
                    header: "SCORE",
                    align: "center",
                    dataIndex: "percentageReductionControl",
                    width: 90,
                    renderer: function(value, metaData, record) {
                      if (value != " ") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("colourControl") +
                          ' !important;"';
                        return "<span>" + value + "</span>";
                      }
                    }
                  },
                  {
                    header: "CALIFICACI&Oacute;N",
                    align: "center",
                    dataIndex: "scoreDescriptionControl",
                    width: 90,
                    renderer: function(value, metaData, record) {
                      if (value != " ") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("colourControl") +
                          ' !important;"';
                        return "<span>" + value + "</span>";
                      }
                    }
                  }
                ]
              },
              {
                header: "% REDUCCI&Oacute;N",
                columns: [
                  {
                    header: "FRECUENCIA",
                    align: "center",
                    dataIndex: "frequencyReduction",
                    width: 90
                  },
                  {
                    header: "IMPACTO",
                    align: "center",
                    dataIndex: "impactReduction",
                    width: 90
                  }
                ]
              },
              {
                header: "RESIDUAL",
                columns: [
                  {
                    header: "FRECUENCIA",
                    align: "center",
                    dataIndex: "descriptionFrequencyResidual",
                    width: 90
                  },
                  {
                    header: "IMPACTO",
                    align: "center",
                    dataIndex: "descriptionImpactResidual",
                    width: 90
                  },
                  {
                    header: "RIESGO",
                    align: "center",
                    dataIndex: "descriptionScaleResidual",
                    width: 90,
                    renderer: function(value, metaData, record) {
                      if (value != " ") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("levelColourResidual") +
                          ' !important;"';
                        return "<span>" + value + "</span>";
                      }
                    }
                  },
                  {
                    header: "P&Eacute;RDIDA",
                    align: "right",
                    dataIndex: "valueResidualRisk",
                    xtype: "numbercolumn",
                    format: "0,0.00",
                    width: 90,
                    renderer: function(value, metaData, record) {
                      if (value != "") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("colourLostResidual") +
                          ' !important;"';
                        return (
                          "<span>" +
                          Ext.util.Format.number(value, "0,0.00") +
                          "</span>"
                        );
                      }
                    }
                  }
                ]
              },
              {
                header: "TRATAMIENTO",
                align: "center",
                dataIndex: "descriptionRiskAnswer",
                width: 100
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridPanelRisks,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            }
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
