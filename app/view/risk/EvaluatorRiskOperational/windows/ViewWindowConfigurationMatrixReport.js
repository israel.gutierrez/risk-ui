Ext.require(["Ext.ux.ColorField"]);
Ext.define("ModelGridCriteriaFrequency", {
  extend: "Ext.data.Model",
  fields: []
});
Ext.define("ModelGridCriteriaImpact", {
  extend: "Ext.data.Model",
  fields: []
});

var StoreGridCriteriaFrequency = Ext.create("Ext.data.Store", {
  model: "ModelGridCriteriaFrequency",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
var StoreGridCriteriaImpact = Ext.create("Ext.data.Store", {
  model: "ModelGridCriteriaImpact",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("ModelGridResumeFrequency", {
  extend: "Ext.data.Model",
  fields: [
    "idFrequency",
    "description",
    { name: "percentage", type: "number" },
    { name: "valueMinimal", type: "number" },
    { name: "valueMaximo", type: "number" },
    "equivalentValue"
  ]
});

var StoreGridResumeFrequency = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelGridResumeFrequency",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("ModelGridResumeImpact", {
  extend: "Ext.data.Model",
  fields: [
    "idImpact",
    "description",
    "equivalentValue",
    "percentage",
    "operationalRiskExposition",
    { name: "valueMinimal", type: "number" },
    { name: "valueMaximo", type: "number" },
    { name: "maxAmount", type: "number" },
    { name: "average", type: "number" },
    "idOperationalRiskExposition"
  ]
});
var StoreGridResumeImpact = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelGridResumeImpact",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("ModelGridResumeScaleRisk", {
  extend: "Ext.data.Model",
  fields: [
    "idScaleRisk",
    "description",
    "operationalRiskExposition",
    "idTypeMatrix",
    { name: "percentageExposition", type: "number" },
    "state",
    { name: "rangeInf", type: "number" },
    { name: "rangeSup", type: "number" },
    "levelColour",
    { name: "businessLineOne", type: "number" },
    { name: "operationalRiskExposition", type: "number" },
    { name: "valueOperationalRiskExposition", type: "number" }
  ]
});

var StoreGridResumeScaleRisk = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelGridResumeScaleRisk",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("ModelGridResumeMatrix", {
  extend: "Ext.data.Model",
  fields: []
});

var StoreGridResumeMatrix = Ext.create("Ext.data.Store", {
  model: "ModelGridResumeMatrix",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowConfigurationMatrixReport",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowConfigurationMatrixReport",
    //    height: 295,
    //    width: 1000,
    //    maximizable:true,
    maximized: true,
    border: false,
    modal: true,
    layout: {
      type: "vbox",
      align: "stretch"
    },
    titleAlign: "center",
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          //                {
          //                    xtype: 'gridpanel',
          //                    height:100,
          //                    title: 'CRITERIOS PARA FRECUENCIA / PROBABILIDAD',
          //                    store:StoreGridCriteriaFrequency,
          //                    padding:'0 0 2 0',
          //                    loadMask: true,
          //                    split:true,
          //                    columnLines: true,
          //                    columns: []
          //                },
          //                {
          //                    xtype: 'gridpanel',
          //                    height:100,
          //                    title: 'CRITERIOS PARA IMPACTO',
          //                    hideHeaders: true,
          //                    store:StoreGridCriteriaImpact,
          //                    padding:'0 0 2 0',
          //                    loadMask: true,
          //                    split:true,
          //                    columnLines: true,
          //                    columns: []
          //                },
          {
            xtype: "container",
            flex: 1,
            padding: "2 0 0 0",
            layout: "border",
            //                    layout: {
            //                        type: 'hbox',
            //                        align: 'stretch'
            //                    },
            items: [
              {
                xtype: "container",
                region: "center",
                flex: 1,
                layout: {
                  type: "vbox",
                  align: "stretch"
                },
                items: [
                  {
                    xtype: "container",
                    flex: 1,
                    layout: {
                      type: "hbox",
                      align: "stretch"
                    },
                    items: [
                      {
                        xtype: "gridpanel",
                        width: 360,
                        padding: "0 2 0 0",
                        titleAlign: "center",
                        itemId: "itemIdResumeFrequency",
                        columnLines: true,
                        store: StoreGridResumeFrequency,
                        title: "FRECUENCIA",
                        columns: [
                          { xtype: "rownumberer", width: 25, sortable: false },
                          {
                            header: "DESCRIPCI&Oacute;N",
                            dataIndex: "description",
                            width: 100
                          },
                          {
                            header: "EQU.",
                            dataIndex: "equivalentValue",
                            align: "center",
                            width: 35
                          },
                          {
                            header: "INFERIOR",
                            dataIndex: "valueMinimal",
                            xtype: "numbercolumn",
                            format: "0,0.00",
                            align: "center",
                            width: 70
                          },
                          {
                            header: "SUPERIOR",
                            dataIndex: "valueMaximo",
                            xtype: "numbercolumn",
                            format: "0,0.00",
                            align: "center",
                            width: 70
                          },
                          {
                            header: "(%)",
                            align: "center",
                            dataIndex: "percentage",
                            xtype: "numbercolumn",
                            format: "0,0.00",
                            width: 50
                          }
                        ]
                      },
                      {
                        xtype: "gridpanel",
                        flex: 1,
                        titleAlign: "center",
                        itemId: "itemIdResumeImpact",
                        store: StoreGridResumeImpact,
                        columnLines: true,
                        title: "IMPACTO",
                        columns: [
                          { xtype: "rownumberer", width: 25, sortable: false },
                          {
                            header: "DESCRIPCI&Oacute;N",
                            dataIndex: "description",
                            width: 100
                          },
                          {
                            header: "EQU.",
                            align: "center",
                            dataIndex: "equivalentValue",
                            width: 35
                          },
                          {
                            header: "(%)",
                            align: "center",
                            dataIndex: "percentage",
                            xtype: "numbercolumn",
                            format: "0,0.00",
                            width: 50
                          },
                          {
                            header: "INFERIOR",
                            align: "right",
                            dataIndex: "valueMinimal",
                            xtype: "numbercolumn",
                            format: "0,0.00",
                            width: 90
                          },
                          {
                            header: "SUPERIOR",
                            align: "right",
                            dataIndex: "valueMaximo",
                            xtype: "numbercolumn",
                            format: "0,0.00",
                            width: 90
                          },
                          {
                            header: "PROMEDIO",
                            align: "right",
                            dataIndex: "average",
                            xtype: "numbercolumn",
                            format: "0,0.00",
                            width: 90
                          }
                        ]
                      }
                    ]
                  },
                  {
                    xtype: "gridpanel",
                    flex: 1,
                    titleAlign: "center",
                    itemId: "itemIdResumeScaleMatrix",
                    columnLines: true,
                    store: StoreGridResumeScaleRisk,
                    padding: "2 0 0 0",
                    title: "ESCALA DE NIVELES DE RIESGOS",
                    columns: [
                      { xtype: "rownumberer", width: 25, sortable: false },
                      {
                        header: "C&Oacute;DIGO",
                        align: "center",
                        dataIndex: "idScaleRisk",
                        width: 70
                      },
                      {
                        header: "DESCRIPCI&Oacute;N",
                        dataIndex: "description",
                        flex: 1
                      },
                      {
                        header: "% MAXIMA<br> EXPOSICI&Oacute;N<br>",
                        dataIndex: "percentageExposition",
                        align: "center",
                        flex: 1,
                        xtype: "numbercolumn",
                        format: "0,0.00"
                      },
                      {
                        header: "L&Iacute;MITE P&Eacute;RDIDA<br> INFERIOR<br>",
                        align: "right",
                        dataIndex: "rangeInf",
                        flex: 1,
                        xtype: "numbercolumn",
                        format: "0,0.00"
                      },
                      {
                        header: "L&Iacute;MITE P&Eacute;RDIDA<br> SUPERIOR<br>",
                        align: "right",
                        dataIndex: "rangeSup",
                        flex: 1,
                        xtype: "numbercolumn",
                        format: "0,0.00"
                      },
                      {
                        header: "COLOR",
                        align: "center",
                        dataIndex: "levelColour",
                        flex: 1,
                        renderer: function(value) {
                          return (
                            '<div  style="background-color:#' +
                            value +
                            "; color:#" +
                            value +
                            ';"> ' +
                            value +
                            "</div>"
                          );
                        }
                      }
                    ]
                  }
                ]
              },
              {
                xtype: "gridpanel",
                region: "east",
                split: true,
                width: 600,
                titleAlign: "center",
                itemId: "itemIdResumeMatrix",
                store: StoreGridResumeMatrix,
                title: "MATRIZ CONFIGURADA",
                columns: [],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: StoreGridResumeMatrix,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                }
              }
            ]
          }
        ]
        //            ,
        //            buttons:[
        //                {
        //                    text:'SALIR',
        //                    handler: function(){
        ////                        Ext.ComponentQuery.query('ViewPanelConfigurationGeneralMatrix ViewGridPanelRegisterConfigurationMatrix')[0].getStore().load();
        //                        me.close();
        //                    },
        //                    iconCls:'logout'
        //                }
        //            ]
      });

      me.callParent(arguments);
    }
  }
);
