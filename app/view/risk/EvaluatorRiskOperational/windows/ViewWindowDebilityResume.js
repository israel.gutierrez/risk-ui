Ext.define("ModelGridPanelDebility", {
  extend: "Ext.data.Model",
  fields: [
    "idRiskWeaknessDetail",
    "risk",
    "description",
    "state",
    "factorRisk",
    "nameFactorRisk",
    "average"
  ]
});

var StoreGridPanelDebility = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelDebility",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
//storeUserActives.load();

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDebilityResume",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowDebilityResume",
    //    layout: {
    //        align: 'stretch',
    //        type: 'vbox'
    //    },
    layout: "fit",
    anchorSize: 100,
    title: "DEBILIDADES POR RIESGO",
    titleAlign: "center",
    width: 800,
    height: 500,
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          //                {
          //                    xtype: 'form',
          //                    height: 45,
          //                    padding: '2 2 2 2',
          //                    bodyPadding: 10,
          //                    items: [
          //                        {
          //                            xtype: 'container',
          //                            anchor: '100%',
          //                            height: 26,
          //                            layout: {
          //                                type: 'hbox'
          //                            },
          //                            items: [
          //                                {
          //                                    xtype:'combobox',
          //                                    value:'2',
          //                                    labelWidth:40,
          //                                    width:190,
          ////                                    flex:1,
          //                                    fieldLabel:'TIPO',
          //                                    store: [
          //                                        ['1','USUARIO'],
          //                                        ['2','NOMBRE COMPLETO']
          //                                    ]
          //                                },
          //                                {
          //                                    xtype:"UpperCaseTextField",
          //                                    //    action:'searchUserActives',
          //                                    flex: 2,
          //
          ////                                    fieldLabel: 'BUSCAR PER',
          //                                    listeners:{
          //                                        afterrender: function(field) {
          //                                            field.focus(false, 200);
          //                                        },
          //                                        'specialkey':function(field,e){
          //                                            var property = '';
          //                                            if(Ext.ComponentQuery.query('SearchUser combobox')[0].getValue()== '1'){property='username'}else{property='fullName'}
          //                                            if(e.getKey() === e.ENTER){
          //                                                var grid = me.down('grid');
          //                                                var toolbar = grid.down('pagingtoolbar');
          //                                                DukeSource.global.DirtyView.searchPaginationGridToEnter(field,grid,toolbar,'http://localhost:9000/giro/findMatchUser.htm',property,'username');
          //                                            }
          //
          //                                        }
          //                                    }
          //                                }
          //                            ]
          //                        }
          //                    ]
          //                },
          //                {
          //                    xtype: 'container',
          //                    flex: 3,
          //                    padding:'2 2 2 2',
          //                    layout: {
          //                        align: 'stretch',
          //                        type: 'hbox'
          //                    },
          //                    items: [
          //                        {
          //                            xtype:'gridpanel',
          //                            padding:'2 2 2 2',
          //                            name:'riskDebility',
          //                            store: StoreGridPanelDebility,
          //                            loadMask: true,
          //                            columnLines: true,
          //                            region:'center',
          //                            flex:1,
          //                            padding: '0 2 2 2',
          //                            columns: [
          //                                {xtype: 'rownumberer', width: 25, sortable: false}
          //                                ,{header: 'DESCRIPCION',  dataIndex: 'description', flex:2,tdCls: 'wrap'}
          //                                ,{header: 'FACTOR', dataIndex: 'nameFactorRisk',flex:1}
          //                                ,{header: 'CONTROl', dataIndex: 'average',flex:1}
          //                            ],
          //                            tbar:[
          //                                {
          //                                    xtype: 'button',
          //                                    text: 'NUEVO',
          //                                    iconCls:'add',
          //                                    handler:function(button){
          //                                        var panel = button.up('ViewPanelIdentifyRiskOperational');
          //                                        var grid = panel.down('grid[name=riskIdentify]');
          //                                        if (grid.getSelectionModel().getCount() == 0) {
          //                                            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING,'SELECCIONE UN RIESGO INDENTIFICADO POR FAVOR',Ext.Msg.WARNING);
          //                                        } else {
          //                                            var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDebilityEntry',{
          //                                                modal:true
          //                                            }).show();
          //                                        }
          //
          //                                    }
          //                                },
          //                                {
          //                                    xtype:'button',
          //                                    text:'MODIFICAR',
          //                                    iconCls:'modify',
          //                                    handler:function(button){
          //                                        var panel = button.up('ViewPanelIdentifyRiskOperational');
          //                                        var grid = panel.down('grid[name=riskDebility]');
          //                                        if (grid.getSelectionModel().getCount() == 0) {
          //                                            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING,'SELECCIONE UNA DEBILIDAD INDENTIFICADA POR FAVOR',Ext.Msg.WARNING);
          //                                        } else {
          //                                            var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDebilityEntry',{
          //                                                modal:true
          //                                            });
          //                                            window.down('form').getForm().loadRecord(grid.getSelectionModel().getSelection()[0]);
          //                                            window.show();
          //                                        }
          //                                    }
          //                                },
          //                                {
          //                                    xtype:'button',
          //                                    text:'ELIMINAR',
          //                                    iconCls:'delete',
          //                                    handler:function(button){
          //                                        var panel = button.up('ViewPanelIdentifyRiskOperational');
          //                                        var grid = panel.down('grid[name=riskDebility]');
          //                                        if (grid.getSelectionModel().getCount() == 0) {
          //                                            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING,'SELECCIONE UNA DEBILIDAD INDENTIFICADA POR FAVOR',Ext.Msg.WARNING);
          //                                        } else {
          //                                            DukeSource.global.DirtyView.deleteElementToGrid(grid,'http://localhost:9000/giro/deleteRiskWeaknessDetail.htm?nameView=PanelProcessRiskEvaluation');
          //                                        }
          //                                    }
          //                                }
          //                            ],
          //                            bbar:{
          //                                xtype: 'pagingtoolbar',
          //                                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
          //                                store: StoreGridPanelDebility,
          //                                displayInfo: true,
          //                                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
          ////                                        items:['-',{action:'exportFulFillmentPdf',iconCls:'pdf'},'-',{action:'exportFulFillmentExcel',iconCls:'excel'},'-',{ xtype:"UpperCaseTrigger",fieldLabel:'FILTRAR',action:'searchTriggerGridFulFillment',labelWidth:60,width:300}],
          //                                emptyMsg:  DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          //                            }
          //                            ,
          //                            listeners:{
          //                                render:function(){
          //                                    var grid = this;
          //                                    DukeSource.global.DirtyView.loadGridDefault(grid);
          //
          //                                }
          //                            }
          //
          //                        }
          //                    ]
          //                }
          {
            xtype: "gridpanel",
            padding: "2 2 2 2",
            name: "riskDebility",
            store: StoreGridPanelDebility,
            loadMask: true,
            columnLines: true,
            //                    region:'center',
            //                    flex:1,
            //                    padding: '0 2 2 2',
            columns: [
              { xtype: "rownumberer", width: 25, sortable: false },
              {
                header: "DESCRIPCION",
                dataIndex: "description",
                flex: 4,
                tdCls: "wrap"
              },
              { header: "FACTOR", dataIndex: "nameFactorRisk", flex: 1 },
              { header: "CONTROl", dataIndex: "average", flex: 0.5 }
            ],
            tbar: [
              {
                xtype: "button",
                text: "NUEVO",
                iconCls: "add",
                handler: function(button) {
                  //                                var panel = Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0];
                  //                                var grid = panel.down('grid[name=riskIdentify]');
                  //                                if (grid.getSelectionModel().getCount() == 0) {
                  //                                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING,'SELECCIONE UN RIESGO INDENTIFICADO POR FAVOR',Ext.Msg.WARNING);
                  //                                } else {
                  var window = Ext.create(
                    "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDebilityEntry",
                    {
                      modal: true
                    }
                  ).show();
                  //                                }
                }
              },
              {
                xtype: "button",
                text: "MODIFICAR",
                iconCls: "modify",
                handler: function(button) {
                  var panel = button.up("ViewPanelIdentifyRiskOperational");
                  var grid = panel.down("grid[name=riskDebility]");
                  if (grid.getSelectionModel().getCount() == 0) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      "SELECCIONE UNA DEBILIDAD INDENTIFICADA POR FAVOR",
                      Ext.Msg.WARNING
                    );
                  } else {
                    var window = Ext.create(
                      "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDebilityEntry",
                      {
                        modal: true
                      }
                    );
                    window
                      .down("form")
                      .getForm()
                      .loadRecord(grid.getSelectionModel().getSelection()[0]);
                    window.show();
                  }
                }
              },
              {
                xtype: "button",
                text: "ELIMINAR",
                iconCls: "delete",
                handler: function(button) {
                  var panel = button.up("ViewPanelIdentifyRiskOperational");
                  var grid = panel.down("grid[name=riskDebility]");
                  if (grid.getSelectionModel().getCount() == 0) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      "SELECCIONE UNA DEBILIDAD INDENTIFICADA POR FAVOR",
                      Ext.Msg.WARNING
                    );
                  } else {
                    DukeSource.global.DirtyView.deleteElementToGrid(
                      grid,
                      "http://localhost:9000/giro/deleteRiskWeaknessDetail.htm?nameView=PanelProcessRiskEvaluation"
                    );
                  }
                }
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridPanelDebility,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              //                                        items:['-',{action:'exportFulFillmentPdf',iconCls:'pdf'},'-',{action:'exportFulFillmentExcel',iconCls:'excel'},'-',{ xtype:"UpperCaseTrigger",fieldLabel:'FILTRAR',action:'searchTriggerGridFulFillment',labelWidth:60,width:300}],
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            },
            listeners: {
              render: function() {
                var grid = this;
                DukeSource.global.DirtyView.loadGridDefault(grid);
              }
            }
          }
        ],
        buttons: [
          {
            text: "SALIR",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
