Ext.define("ModelQualificationProcessResume", {
  extend: "Ext.data.Model",
  fields: [
    "idFeaturesProcess",
    "descriptionFeaturesProcess",
    "descriptionValueFeatureProcess",
    "idValueFeaturesProcess",
    "value",
    "typeItem"
  ]
});

var storeQualificationProcessResume = Ext.create("Ext.data.Store", {
  model: "ModelQualificationProcessResume",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowResumeEvaluationProcess",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowResumeEvaluationProcess",
    height: 350,
    width: 650,
    layout: {
      type: "fit"
    },
    border: false,
    modal: true,
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "container",
            flex: 1,
            region: "center",
            layout: {
              type: "vbox",
              align: "stretch"
            },
            items: [
              {
                xtype: "gridpanel",
                itemId: "calificationProcessGrid",
                padding: "0 0 2 0",
                title: "CALIFICACION DEL PROCESO",
                store: storeQualificationProcessResume,
                flex: 1,
                titleAlign: "center",
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    dataIndex: "descriptionFeaturesProcess",
                    flex: 1,
                    text: "CARACTER&Iacute;STICA"
                  },
                  {
                    dataIndex: "descriptionValueFeatureProcess",
                    flex: 1,
                    text: "VALOR CARACTER&Iacute;STICA"
                  },
                  {
                    header: "CALIFICACI&Oacute;N",
                    align: "center",
                    dataIndex: "value"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: storeQualificationProcessResume,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                }
              },
              {
                xtype: "form",
                itemId: "formCalificationProcess",
                height: 45,
                bodyPadding: 10,
                items: [
                  {
                    xtype: "textfield",
                    itemId: "id",
                    name: "id",
                    hidden: true
                  },
                  {
                    xtype: "container",
                    anchor: "100%",
                    height: 26,
                    layout: {
                      type: "hbox"
                    },
                    items: [
                      {
                        xtype:
                         "UpperCaseTextFieldReadOnly",
                        name: "description",
                        padding: "0 10 0 0",
                        flex: 2,
                        labelWidth: 180,
                        fieldLabel: "VALOR DE CALIFICACI&Oacute;N"
                      },
                      {
                        xtype:
                         "UpperCaseTextFieldReadOnly",
                        name: "valueQualification",
                        flex: 1
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ],
        buttons: [
          {
            text: "SALIR",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
