Ext.define("ModelFileAttach", {
  extend: "Ext.data.Model",
  fields: ["correlative", "idDetailControl", "nameFile", "fullName"]
});

var storeFileAttach = Ext.create("Ext.data.Store", {
  model: "ModelFileAttach",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListFileAttachmentsDetail.htm",
    extraParams: {
      //            idDocument:Ext.ComponentQuery.query('ViewPanelDocumentPending grid')[0].getSelectionModel().getSelection()[0].get('idDocument'),
      //            idDetailDocument:Ext.ComponentQuery.query('ViewPanelDocumentPending grid')[0].getSelectionModel().getSelection()[0].get('idDetailDocument'),
      //            propertyOrder:'idDetailDocument'
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
//storeFileAttach.load();

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowManagementReportControl",
  {
    extend: "Ext.window.Window",
    border: false,
    alias: "widget.ViewWindowManagementReportControl",
    height: 383,
    width: 589,

    layout: {
      align: "stretch",
      type: "vbox"
    },
    title: "DOCUMENTOS ADJUNTOS",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            padding: "0 0 2 0",
            bodyPadding: "10",
            items: [
              {
                xtype: "textfield",
                name: "idDetailControl",
                hidden: true
              },
              {
                xtype: "filefield",
                anchor: "100%",
                allowBlank: false,
                msgTarget: "side",
                fieldCls: "obligatoryTextField",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                itemId: "text",
                name: "text",
                fieldLabel: "ARCHIVO"
              }
            ],
            bbar: [
              "->",
              "-",
              {
                text: "ADJUNTAR",
                iconCls: "save",
                handler: function() {
                  //                        var window = button.up('window');
                  var form = me.down("form");
                  var grid = me.down("grid");
                  if (form.getForm().isValid()) {
                    form.getForm().submit({
                      url:
                        "http://localhost:9000/giro/saveFileAttachmentControl.htm?nameView=PanelProcessRiskEvaluation",
                      waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
                      method: "POST",
                      success: function(form, action) {
                        var valor = Ext.decode(action.response.responseText);
                        if (valor.success) {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_MESSAGE,
                            valor.mensaje,
                            Ext.Msg.INFO
                          );
                          grid.store.getProxy().extraParams = {
                            idDetailControl: Ext.ComponentQuery.query(
                              "ViewWindowManagementReportControl"
                            )[0]
                              .down("textfield[name=idDetailControl]")
                              .getValue()
                          };
                          grid.store.getProxy().url =
                            "http://localhost:9000/giro/showListFileAttachmentControlActives.htm";
                          grid.down("pagingtoolbar").moveFirst();
                          //                                                me.close();
                        } else {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_ERROR,
                            valor.mensaje,
                            Ext.Msg.ERROR
                          );
                        }
                      },
                      failure: function(form, action) {
                        var valor = Ext.decode(action.response.responseText);
                        if (!valor.success) {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_ERROR,
                            valor.mensaje,
                            Ext.Msg.ERROR
                          );
                        }
                      }
                    });
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
                      Ext.Msg.ERROR
                    );
                  }
                }
              },
              "-"
            ]
          },
          {
            xtype: "gridpanel",
            store: storeFileAttach,
            flex: 1,
            columns: [
              {
                xtype: "rownumberer",
                width: 25,
                sortable: false
              },
              {
                dataIndex: "correlative",
                width: 60,
                text: "CODIGO"
              },
              {
                dataIndex: "fullName",
                flex: 1,
                text: "NOMBRE"
              },
              {
                dataIndex: "nameFile",
                flex: 1,
                text: "ARCHIVO"
              },
              {
                xtype: "actioncolumn",
                header: "DOCUMENTO",
                align: "center",
                width: 80,
                items: [
                  {
                    icon: "images/page_white_put.png",
                    handler: function(grid, rowIndex) {
                      Ext.core.DomHelper.append(document.body, {
                        tag: "iframe",
                        id: "downloadIframe",
                        frameBorder: 0,
                        width: 0,
                        height: 0,
                        css: "display:none;visibility:hidden;height:0px;",
                        src:
                          "http://localhost:9000/giro/downloadFileAttachmentsDetailControl.htm?correlative=" +
                          grid.store.getAt(rowIndex).get("correlative") +
                          "&idDetailControl=" +
                          grid.store.getAt(rowIndex).get("idDetailControl") +
                          "&nameFile=" +
                          grid.store.getAt(rowIndex).get("nameFile")
                      });
                      //                                    new Ext.Window({
                      //                                        modal:true,
                      //                                        width : 900,
                      //                                        height: 600,
                      //                                        layout : 'fit',
                      //                                        items : [
                      //                                            {
                      //                                                xtype : 'component',
                      //                                                autoEl : {
                      //                                                    tag : 'iframe',
                      //                                                    src: 'http://localhost:9000/giro/downloadFileAttachmentsDetailControl.htm?correlative='+grid.store.getAt(rowIndex).get('correlative')+'&idDetailControl='+grid.store.getAt(rowIndex).get('idDetailControl')+'&nameFile='+grid.store.getAt(rowIndex).get('nameFile')
                      //                                                }
                      //                                            }
                      //                                        ]
                      //                                    }).show();
                    }
                  }
                ]
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: 50,
              store: storeFileAttach,
              items: [
                {
                  xtype:"UpperCaseTrigger",
                  width: 120,
                  action: "searchGridAllAgency"
                }
              ]
            },
            listeners: {
              render: function() {
                var me = this;
                me.store.getProxy().extraParams = {
                  idDetailControl: Ext.ComponentQuery.query(
                    "ViewWindowManagementReportControl"
                  )[0]
                    .down("textfield[name=idDetailControl]")
                    .getValue()
                };
                me.store.getProxy().url =
                  "http://localhost:9000/giro/showListFileAttachmentControlActives.htm";
                me.down("pagingtoolbar").moveFirst();
              }
            }
          }
        ],
        buttons: [
          {
            text: "SALIR",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
