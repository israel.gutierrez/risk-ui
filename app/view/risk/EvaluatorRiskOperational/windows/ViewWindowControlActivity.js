Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowControlActivity', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowControlActivity',
    height: 400,
    width: 600,
    border: false,
    layout: {
        type: 'fit'
    },
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    border: false,
                    layout: 'fit',
                    items: [
                        {
                            xtype: 'htmleditor',
                            anchor: '100%'
                        }
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
});