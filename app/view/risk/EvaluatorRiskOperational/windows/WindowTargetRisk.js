function reductionTargetRisk(me) {
  var form = me.down("form");
  var backPanel = me.backPanel;
  DukeSource.lib.Ajax.request({
    waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
    method: "POST",
    url: "http://localhost:9000/giro/reductionTargetRisk.htm",
    params: {
      jsonData: Ext.JSON.encode(form.getForm().getValues())
    },
    success: function(response) {
      response = Ext.decode(response.responseText);
      var bean = response.data;

      if (response.success) {
        backPanel.down("#idTargetRisk").setValue(bean["id"]);
        backPanel
          .down("#descriptionTargetFrequency")
          .setValue(bean["descriptionFrequency"]);
        backPanel
          .down("#descriptionTargetImpact")
          .setValue(bean["descriptionImpact"]);
        backPanel
          .down("#valueTargetRisk")
          .setValue(Ext.util.Format.number(bean["targetValue"], "0.00"));
        backPanel
          .down("#valueTargetRisk")
          .setFieldLabel("Nivel-" + bean["descriptionMatrix"]);
        backPanel
          .down("#valueTargetRisk")
          .setFieldStyle("background-color: #" + bean["colorMatrix"] + ";");

        form.getForm().setValues(bean);
        form
          .down("#descriptionFrequencyTarget")
          .setValue(bean["descriptionFrequency"]);
        form
          .down("#descriptionImpactTarget")
          .setValue(bean["descriptionImpact"]);

        form
          .down("#targetValue")
          .setFieldLabel("Nivel-" + bean["descriptionMatrix"]);
        form
          .down("#targetValue")
          .setFieldStyle("background-color: #" + bean["colorMatrix"] + ";");
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          response.message,
          Ext.Msg.ERROR
        );
      }
    },
    failure: function() {}
  });
}

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.WindowTargetRisk",
  {
    extend: "Ext.window.Window",
    alias: "widget.WindowTargetRisk",
    width: 850,
    layout: "fit",
    title: "Definición del riesgo objetivo",
    requires: [],
    border: false,
    initComponent: function() {
      var me = this;
      var backPanel = me.backPanel;
      var idRisk = me.idRisk;
      var idVersionRisk = me.idVersionRisk;
      var style = backPanel.down("#valueLostResidual").fieldStyle;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 5,
            items: [
              {
                xtype: "container",
                layout: {
                  type: "hbox",
                  defaultMargins: {
                    top: 0,
                    right: 5,
                    bottom: 0,
                    left: 0
                  }
                },
                items: [
                  {
                    xtype: "fieldset",
                    title: "Riesgo residual",
                    padding: 5,
                    flex: 1.1,
                    collapsible: true,
                    layout: {
                      type: "hbox"
                    },
                    items: [
                      {
                        xtype:
                         "UpperCaseTextFieldReadOnly",
                        flex: 1,
                        fieldLabel: "Frecuencia",
                        labelAlign: "top",
                        value: backPanel
                          .down("#descriptionFrequencyResidual")
                          .getValue(),
                        fieldCls: "textFieldSmall",
                        name: "descriptionFrequencyResidual",
                        itemId: "descriptionFrequencyResidual"
                      },
                      {
                        xtype:
                         "UpperCaseTextFieldReadOnly",
                        flex: 1,
                        fieldLabel: "Impacto",
                        labelAlign: "top",
                        value: backPanel
                          .down("#descriptionImpactResidual")
                          .getValue(),
                        fieldCls: "textFieldSmall",
                        name: "descriptionImpactResidual",
                        itemId: "descriptionImpactResidual"
                      },
                      {
                        xtype: "NumberDecimalNumberObligatory",
                        flex: 1,
                        readOnly: true,
                        fieldLabel: backPanel
                          .down("#valueLostResidual")
                          .getFieldLabel(),
                        value: backPanel.down("#valueLostResidual").getValue(),
                        margin: "0 0 0 5",
                        fieldStyle:
                          style.substr(0, style.length - 1) + " !important;",
                        fieldCls: "textFieldSmall-np",
                        labelSeparator: "",
                        labelAlign: "top",
                        name: "valueLostResidual",
                        itemId: "valueLostResidual"
                      }
                    ]
                  },
                  {
                    xtype: "fieldset",
                    flex: 0.6,
                    padding: 5,
                    collapsible: true,
                    title: "% Reducción",
                    layout: {
                      type: "hbox"
                    },
                    items: [
                      {
                        xtype: "NumberDecimalNumberObligatory",
                        name: "reductionFrequency",
                        itemId: "reductionFrequency",
                        value: 0,
                        flex: 1,
                        fieldLabel: "Frecuencia",
                        labelAlign: "top"
                      },
                      {
                        xtype: "NumberDecimalNumberObligatory",
                        name: "reductionImpact",
                        itemId: "reductionImpact",
                        value: 0,
                        flex: 1,
                        labelAlign: "top",
                        fieldLabel: "Impacto"
                      }
                    ]
                  },
                  {
                    xtype: "container",
                    flex: 0.35,
                    height: 70,
                    layout: {
                      type: "vbox",
                      align: "center",
                      pack: "center"
                    },
                    items: [
                      {
                        xtype: "button",
                        text: "Ejecutar",
                        iconCls: "table_go",
                        handler: function() {
                          reductionTargetRisk(me);
                        }
                      }
                    ]
                  },
                  {
                    xtype: "fieldset",
                    flex: 1.1,
                    padding: 5,
                    title: "Riesgo objetivo",
                    layout: "hbox",
                    items: [
                      {
                        xtype:
                         "UpperCaseTextFieldReadOnly",
                        flex: 1,
                        fieldLabel: "Frecuencia",
                        labelAlign: "top",
                        fieldCls: "textFieldSmall",
                        name: "descriptionFrequencyTarget",
                        itemId: "descriptionFrequencyTarget"
                      },
                      {
                        xtype:
                         "UpperCaseTextFieldReadOnly",
                        flex: 1,
                        fieldLabel: "Impacto",
                        labelAlign: "top",
                        fieldCls: "textFieldSmall",
                        name: "descriptionImpactTarget",
                        itemId: "descriptionImpactTarget"
                      },
                      {
                        xtype: "NumberDecimalNumberObligatory",
                        readOnly: true,
                        flex: 1,
                        margin: "0 0 0 5",
                        fieldCls: "textFieldSmall-np",
                        labelAlign: "top",
                        name: "targetValue",
                        itemId: "targetValue",
                        labelSeparator: "",
                        fieldLabel: "Nivel"
                      }
                    ]
                  }
                ]
              },
              {
                xtype: 'UpperCaseTextArea',
                anchor: "100%",
                allowBlank: true,
                fieldCls: "textFieldSmall",
                height: 50,
                flex: 1,
                maxLength: 800,
                name: "description",
                itemId: "description",
                fieldLabel: "Comentarios",
                labelAlign: "top"
              },
              {
                xtype: "textfield",
                itemId: "scaleRisk",
                name: "scaleRisk",
                hidden: true
              },
              {
                xtype: "textfield",
                itemId: "risk",
                name: "risk",
                value: idRisk,
                hidden: true
              },
              {
                xtype: "textfield",
                itemId: "versionRisk",
                name: "versionRisk",
                value: idVersionRisk,
                hidden: true
              },
              {
                xtype: "textfield",
                itemId: "id",
                name: "id",
                value: "id",
                hidden: true
              },
              {
                xtype: "textfield",
                itemId: "matrix",
                name: "matrix",
                hidden: true
              },
              {
                xtype: "textfield",
                itemId: "typeMatrix",
                name: "typeMatrix",
                hidden: true
              },
              {
                xtype: "textfield",
                itemId: "typeMatrixScaleRisk",
                name: "typeMatrixScaleRisk",
                hidden: true
              }
            ]
          }
        ],
        buttons: [
          {
            text: "Salir",
            scope: this,
            handler: this.close,
            scale: "medium",
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);
