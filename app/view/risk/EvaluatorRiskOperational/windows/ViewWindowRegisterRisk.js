Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowRegisterRisk', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowRegisterRisk',
    requires: [
        'DukeSource.view.risk.parameter.combos.ViewComboBusinessLineOne'
        , 'DukeSource.view.risk.parameter.combos.ViewComboBusinessLineTwo'
        , 'DukeSource.view.risk.parameter.combos.ViewComboBusinessLineThree'
        , 'DukeSource.view.risk.parameter.combos.ViewComboRiskType'
    ],
    width: 750,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'Ingreso nuevo riesgo',
    modal: true,
    initComponent: function () {
        var me = this;
        var link = this.link;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    fieldDefaults: {
                        labelCls: 'changeSizeFontToEightPt',
                        fieldCls: 'changeSizeFontToEightPt'
                    },
                    items: [
                        {
                            xtype: 'container',
                            height: 27,
                            layout: {
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    name: 'idRisk',
                                    itemId: 'idRisk'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    name: 'idCatalogRisk',
                                    itemId: 'idCatalogRisk'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    name: 'state',
                                    value: 'S',
                                    itemId: 'state'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    name: 'idOperationalRiskExposition',
                                    itemId: 'idOperationalRiskExposition',
                                    value: idMaxExpositionActive
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'versionCorrelative',
                                    name: 'versionCorrelative'
                                },
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: {
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype:"UpperCaseTextField",
                                            flex: 1.5,
                                            fieldLabel: 'Código riesgo',
                                            readOnly: true,
                                            fieldCls: 'fieldBlue',
                                            name: 'codeRisk'
                                        },
                                        {
                                            xtype:"UpperCaseTextFieldReadOnly",
                                            padding: '0 0 0 5',
                                            name: 'nameEvaluation',
                                            labelWidth: 80,
                                            itemId: 'nameEvaluation',
                                            flex: 2.5,
                                            fieldCls: 'fieldBlue',
                                            fieldLabel: 'Evaluación'
                                        },
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'Fecha',
                                            padding: '0 0 0 5',
                                            labelAlign: 'right',
                                            readOnly: true,
                                            labelWidth: 50,
                                            flex: 1.5,
                                            fieldCls: 'fieldBlue',
                                            name: 'dateEvaluation',
                                            itemId: 'dateEvaluation'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    flex: 0.2,
                                    layout: {
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            readOnly: true,
                                            hidden: true,
                                            flex: 1,
                                            padding: '0 0 0 5',
                                            fieldLabel: 'Máxima exposición',
                                            fieldCls: 'fieldBlue',
                                            name: 'maxAmount',
                                            itemId: 'maxAmount'
                                        },
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'Año',
                                            padding: '0 0 0 5',
                                            labelAlign: 'right',
                                            labelWidth: 40,
                                            flex: 0.5,
                                            readOnly: true,
                                            fieldCls: 'fieldBlue',
                                            name: 'year',
                                            itemId: 'year'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            height: 45,
                            hidden: hidden('WRR_PANEL_CatalogRisk'),
                            layout: {
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'UpperCaseTextArea',
                                    anchor: '100%',
                                    height: 40,
                                    maxLengthText: DukeSource.global.GiroMessages.MESSAGE_MAX_CHARACTER + 1000,
                                    fieldLabel: 'Riesgo catálogo<br><span style="color: #0a56c8">(Clic en Buscar)</span>',
                                    fieldCls: 'readOnlyText',
                                    flex: 1,
                                    itemId: 'descriptionCatalogRisk',
                                    name: 'descriptionCatalogRisk',
                                    allowBlank: blank('WRR_TBX_DescriptionCatalogRisk'),
                                    enforceMaxLength: true,
                                    readOnly: true,
                                    maxLength: 1000

                                },
                                {
                                    xtype: 'button',
                                    itemId: 'buttonSearchRisk',
                                    width: 67,
                                    height: 40,
                                    textAlign: 'center',
                                    text: 'Buscar',
                                    iconCls: 'search',
                                    listeners: {
                                        afterrender: function (field) {
                                            field.focus(false, 200);
                                        }
                                    },
                                    handler: function () {
                                        var windowRisk = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowSearchRisk', {
                                            modal: true
                                        }).show();
                                        windowRisk.down('grid').on('itemdblclick', function (grid, record) {
                                            me.down('#descriptionCatalogRisk').setValue(record.get('description'));
                                            me.down('#idCatalogRisk').setValue(record.get('idRisk'));
                                            windowRisk.close();
                                            me.down('#description').focus(false, 200);

                                        });
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            height: 40,
                            maxLengthText: DukeSource.global.GiroMessages.MESSAGE_MAX_CHARACTER + 1000,
                            fieldLabel: 'Riesgo (Definición)',
                            allowBlank: false,
                            fieldCls: 'obligatoryTextField',
                            flex: '1',
                            name: 'description',
                            itemId: 'description',
                            maxLength: 1000

                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            height: 40,
                            fieldLabel: getName('WRRComments'),
                            allowBlank: blank('WRRComments'),
                            hidden: hidden('WRRComments'),
                            fieldCls: styleField('WRRComments'),
                            name: 'comments',
                            maxLength: 2000

                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    flex: 1.2,
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'idProcessType',
                                            name: 'idProcessType'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'idProcess',
                                            name: 'idProcess'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'idSubProcess',
                                            name: 'idSubProcess'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'idActivity',
                                            name: 'idActivity'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'product',
                                            name: 'product'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'unity',
                                            name: 'unity'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'codeProcess',
                                            name: 'codeProcess'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'costCenter',
                                            name: 'costCenter'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'factorRisk',
                                            name: 'factorRisk'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'businessLineOne',
                                            name: 'businessLineOne'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'businessLineTwo',
                                            name: 'businessLineTwo'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'businessLineThree',
                                            name: 'businessLineThree'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'eventOne',
                                            name: 'eventOne'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'eventTwo',
                                            name: 'eventTwo'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'eventThree',
                                            name: 'eventThree'
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            itemId: 'descriptionEventOne',
                                            name: 'descriptionEventOne',
                                            fieldLabel: getName('RISK_WRR_DescriptionEventOne'),
                                            allowBlank: blank('RISK_WRR_DescriptionEventOne'),
                                            hidden: hidden('RISK_WRR_DescriptionEventOne'),
                                            height: 18,
                                            value: '(Seleccionar)',
                                            fieldCls: 'style-for-url',
                                            listeners: {
                                                afterrender: function (view) {
                                                    view.getEl().on('click', function () {
                                                        Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeTypeEvent', {
                                                            winParent: me
                                                        }).show();
                                                    })
                                                }
                                            }
                                        },

                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            fieldLabel: 'Proceso',
                                            padding: '2 0 2 0',
                                            itemId: 'descriptionProcess',
                                            name: 'descriptionProcess',
                                            allowBlank: false,
                                            value: '(Seleccionar)',
                                            fieldCls: 'style-for-url',
                                            listeners: {
                                                afterrender: function (view) {
                                                    view.getEl().on('click', function () {
                                                        var window = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeProcess', {
                                                            backWindow: me
                                                        });
                                                        window.show();
                                                    })
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            fieldLabel: 'Producto',
                                            itemId: 'descriptionProduct',
                                            name: 'descriptionProduct',
                                            hidden: hidden('RISK_WRR_DSF_DescriptionProduct'),
                                            padding: '2 0 2 0',
                                            allowBlank: false,
                                            value: '(Seleccionar)',
                                            fieldCls: 'style-for-url',
                                            listeners: {
                                                afterrender: function (view) {
                                                    view.getEl().on('click', function () {
                                                        Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeProduct', {
                                                            winParent: me
                                                        }).show();
                                                    })
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            fieldLabel: 'Área',
                                            itemId: 'descriptionUnity',
                                            name: 'descriptionUnity',
                                            hidden: hidden('WRREventDescriptionUnity'),
                                            padding: '2 0 2 0',
                                            value: '(Seleccionar)',
                                            fieldCls: 'style-for-url',
                                            listeners: {
                                                afterrender: function (view) {
                                                    view.getEl().on('click', function () {
                                                        Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeOrganization', {
                                                            backWindow: me,
                                                            descriptionUnity: 'descriptionUnity',
                                                            unity: 'unity',
                                                            costCenter: 'costCenter'
                                                        }).show();
                                                    })
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'displayfield',
                                            hidden: hidden('RISK_WRR_DSF_DescriptionFactorRisk'),
                                            anchor: '100%',
                                            padding: '2 0 2 0',
                                            fieldLabel: 'Factor de riesgo',
                                            itemId: 'descriptionFactorRisk',
                                            name: 'descriptionFactorRisk',
                                            value: '(Seleccionar)',
                                            fieldCls: 'style-for-url',
                                            listeners: {
                                                afterrender: function (view) {
                                                    view.getEl().on('click', function () {
                                                        Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeFactorRisk', {
                                                            winParent: me
                                                        }).show();
                                                    })
                                                }
                                            }
                                        },
                                        {
                                            xtype: "combobox",
                                            msgTarget: "side",
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            fieldLabel: "Agencia",
                                            hidden: hidden('RISK_WRR_CBX_Agency'),
                                            plugins: ['ComboSelectCount'],
                                            name: "agency",
                                            itemId: "agency",
                                            anchor: '100%',
                                            queryMode: 'local',
                                            forceSelection: true,
                                            displayField: "description",
                                            valueField: "idAgency",
                                            store: {
                                                autoLoad: true,
                                                fields: ["idAgency", "description"],
                                                proxy: {
                                                    actionMethods: {
                                                        create: "POST",
                                                        read: "POST",
                                                        update: "POST"
                                                    },
                                                    type: "ajax",
                                                    url: "http://localhost:9000/giro/showListAgencyActivesComboBox.htm",
                                                    extraParams: {
                                                        propertyOrder: "a.description"
                                                    },
                                                    reader: {
                                                        type: "json",
                                                        root: "data",
                                                        successProperty: "success"
                                                    }
                                                }
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            height: 18,
                                            padding: '0 0 0 10',
                                            hidden: hidden('RISK_WRR_DescriptionBusinessLineOne'),
                                            fieldLabel: getName('RISK_WRR_DescriptionBusinessLineOne'),
                                            itemId: 'descriptionBusinessLineOne',
                                            name: 'descriptionBusinessLineOne',
                                            value: '(Seleccionar)',
                                            fieldCls: 'style-for-url',
                                            allowBlank: false,
                                            listeners: {
                                                afterrender: function (view) {
                                                    view.getEl().on('click', function () {
                                                        Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeBusinessLine', {
                                                            winParent: me
                                                        }).show();
                                                    })
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'ViewComboRiskType',
                                            flex: 1,
                                            fieldLabel: getName('WRRRiskType'),
                                            allowBlank: blank('WRRRiskType'),
                                            hidden: hidden('WRRRiskType'),
                                            fieldCls: styleField('WRRRiskType'),
                                            padding: '0 0 0 10',
                                            msgTarget: 'side',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'riskType',
                                            listeners: {
                                                specialkey: function (f, e) {
                                                    DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('button[action=saveRiskOperationalDetail]'))
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            flex: 1,
                                            padding: '0 0 0 10',
                                            fieldLabel: getName('WRREnvironmentLegal'),
                                            allowBlank: true,
                                            hidden: hidden('WRREnvironmentLegal'),
                                            editable: false,
                                            forceSelection: true,
                                            name: 'environmentLegal',
                                            itemId: 'environmentLegal',
                                            queryMode: 'local',
                                            displayField: 'description',
                                            valueField: 'value',
                                            store: {
                                                fields: ['value', 'description'],
                                                sorters: [
                                                    {
                                                        property: 'description',
                                                        direction: 'ASC'
                                                    }
                                                ],
                                                pageSize: 9999,
                                                autoLoad: true,
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                    extraParams: {
                                                        propertyOrder: 'description',
                                                        propertyFind: 'identified',
                                                        valueFind: 'LEGAL_RISK'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            anchor: '100%',
                                            fieldLabel: getName('WRREventClass'),
                                            allowBlank: blank('WRREventClass'),
                                            hidden: hidden('WRREventClass'),
                                            fieldCls: styleField('WRREventClass'),
                                            padding: '0 0 0 10',
                                            editable: false,
                                            name: 'eventClass',
                                            itemId: 'eventClass',
                                            queryMode: 'local',
                                            displayField: 'description',
                                            valueField: 'value',
                                            store: {
                                                fields: ['value', 'description'],
                                                sorters: [
                                                    {
                                                        property: 'description',
                                                        direction: 'ASC'
                                                    }
                                                ],
                                                pageSize: 9999,
                                                autoLoad: true,
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                    extraParams: {
                                                        propertyOrder: 'description',
                                                        propertyFind: 'identified',
                                                        valueFind: 'RISK_EVENT_CLASS'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: "combobox",
                                            name: "idRiskEvaluationMatrix",
                                            itemId: "idRiskEvaluationMatrix",
                                            padding: '0 0 0 10',
                                            fieldLabel: getName('WRRRiskEvaluationMatrix'),
                                            allowBlank: blank('WRRRiskEvaluationMatrix'),
                                            hidden: hidden('WRRRiskEvaluationMatrix'),
                                            fieldCls: styleField('WRRRiskEvaluationMatrix'),
                                            plugins: ['ComboSelectCount'],
                                            editable: false,
                                            forceSelection: true,
                                            displayField: "nameEvaluation",
                                            valueField: "idRiskEvaluationMatrix",
                                            store: {
                                                autoLoad: true,
                                                fields: ["idRiskEvaluationMatrix", "nameEvaluation"],
                                                proxy: {
                                                    actionMethods: {
                                                        create: "POST",
                                                        read: "POST",
                                                        update: "POST"
                                                    },
                                                    type: "ajax",
                                                    url: "http://localhost:9000/giro/showListRiskEvaluationMatrixActives.htm",
                                                    extraParams: {
                                                        propertyOrder: "nameEvaluation",
                                                        start: 0,
                                                        limit: 999
                                                    },
                                                    reader: {
                                                        type: "json",
                                                        root: "data",
                                                        successProperty: "success"
                                                    }
                                                },
                                                listeners: {
                                                    load: function (a, b, c) {
                                                        if (c) {
                                                            me.down('#idRiskEvaluationMatrix').setValue(me.idRiskEvaluationMatrix);
                                                        }
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            flex: 1,
                                            padding: '0 0 0 10',
                                            fieldLabel: getName('WRRTypeEventLost'),
                                            allowBlank: blank('WRRTypeEventLost'),
                                            hidden: hidden('WRRTypeEventLost'),
                                            fieldCls: styleField('WRRTypeEventLost'),
                                            editable: false,
                                            forceSelection: true,
                                            name: 'typeEventLost',
                                            itemId: 'typeEventLost',
                                            queryMode: 'local',
                                            displayField: 'description',
                                            valueField: 'value',
                                            store: {
                                                fields: ['value', 'description'],
                                                sorters: [
                                                    {
                                                        property: 'description',
                                                        direction: 'ASC'
                                                    }
                                                ],
                                                pageSize: 9999,
                                                autoLoad: true,
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                    extraParams: {
                                                        propertyOrder: 'description',
                                                        propertyFind: 'identified',
                                                        valueFind: 'RISK_EVENT_LOSS_TYPE'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            flex: 1,
                                            padding: '0 0 0 10',
                                            fieldLabel: getName('WRROriginRisk'),
                                            allowBlank: blank('WRROriginRisk'),
                                            hidden: hidden('WRROriginRisk'),
                                            fieldCls: styleField('WRROriginRisk'),
                                            editable: false,
                                            forceSelection: true,
                                            name: 'originRisk',
                                            itemId: 'originRisk',
                                            queryMode: 'local',
                                            displayField: 'description',
                                            valueField: 'value',
                                            store: {
                                                fields: ['value', 'description'],
                                                sorters: [
                                                    {
                                                        property: 'description',
                                                        direction: 'ASC'
                                                    }
                                                ],
                                                pageSize: 9999,
                                                autoLoad: true,
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                    extraParams: {
                                                        propertyOrder: 'description',
                                                        propertyFind: 'identified',
                                                        valueFind: 'RISK_ORIGIN'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            flex: 1,
                                            padding: '0 0 0 10',
                                            fieldLabel: getName('WRRMaterializedRisk'),
                                            allowBlank: blank('WRRMaterializedRisk'),
                                            hidden: hidden('WRRMaterializedRisk'),
                                            fieldCls: styleField('WRRMaterializedRisk'),
                                            editable: false,
                                            forceSelection: true,
                                            name: 'materializedRisk',
                                            itemId: 'materializedRisk',
                                            queryMode: 'local',
                                            displayField: 'description',
                                            valueField: 'value',
                                            store: {
                                                fields: ['value', 'description'],
                                                pageSize: 9999,
                                                autoLoad: true,
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                    extraParams: {
                                                        propertyOrder: 'description',
                                                        propertyFind: 'identified',
                                                        valueFind: 'RISK_MATERIALIZED'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            buttonAlign: 'center',
            buttons: [
                {
                    text: 'Salir',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                },
                {
                    text: 'Guardar',
                    action: 'saveRiskOperationalDetail',
                    link: link,
                    scale: 'medium',
                    iconCls: 'save'
                }
            ]
        });

        me.callParent(arguments);
    },
    close: function () {
        if (this.fireEvent('beforeclose', this) !== false) {
            var me = this;
            if (me.actionType === 'new') {
                DukeSource.global.DirtyView.messageOut(me);
            } else {
                me.doClose();
            }
        }
    }
});