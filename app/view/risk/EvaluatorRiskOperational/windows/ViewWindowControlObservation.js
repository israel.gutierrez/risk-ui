Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowControlObservation', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowControlObservation',
    width: 500,
    border: false,
    layout: {
        type: 'fit'
    },
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [

                {
                    xtype: 'form',
                    border: false,
                    layout: 'fit',
                    items: [
                        {
                            xtype: 'UpperCaseTextArea',
                            name: 'bio',
                            anchor: '100%',
                            maxLength: 500
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
