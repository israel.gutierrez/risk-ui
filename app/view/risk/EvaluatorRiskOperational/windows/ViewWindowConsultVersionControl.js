Ext.define("ModelVersionControl", {
  extend: "Ext.data.Model",
  fields: [
    "idVersionControl",
    "idControl",
    "dateEvaluation",
    "descriptionControl",
    "description",
    "userEvaluation",
    "stateEvaluation",
    "finalScore",
    "scoreControl",
    "descriptionScoreControl",
    "codeColor"
  ]
});

var storeVersionControl = Ext.create("Ext.data.Store", {
  model: "ModelVersionControl",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    extraParams: {},
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowConsultVersionControl",
  {
    extend: "Ext.window.Window",
    border: false,
    alias: "widget.ViewWindowConsultVersionControl",
    height: 450,
    width: 850,
    layout: {
      type: "fit"
    },
    title: "EVALUACIONES DE LOS CONTROLES",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "textfield",
            itemId: "id",
            name: "id",
            hidden: true
          },
          {
            xtype: "gridpanel",
            store: storeVersionControl,
            flex: 1,
            columns: [
              {
                xtype: "rownumberer",
                width: 25,
                sortable: false
              },
              {
                dataIndex: "idVersionControl",
                width: 40,
                text: "CODIGO"
              },
              {
                dataIndex: "userEvaluation",
                flex: 1.3,
                text: "RESPONSABLE"
              },
              {
                dataIndex: "dateEvaluation",
                flex: 0.5,
                text: "FECHA"
              },
              {
                dataIndex: "description",
                flex: 1,
                text: "VERSION"
              },
              {
                dataIndex: "finalScore",
                flex: 1,
                text: "SCORE CONTROL"
              },
              {
                dataIndex: "descriptionScoreControl",
                flex: 1,
                text: "CALIFICACION",
                renderer: function(value, metaData, record) {
                  if (value != " ") {
                    metaData.tdAttr =
                      'style="background-color: #' +
                      record.get("codeColor") +
                      ' !important;"';
                    return "<span>" + value + "</span>";
                  }
                }
              },
              {
                xtype: "actioncolumn",
                header: "IR VERSION",
                align: "center",
                width: 80,
                items: [
                  {
                    icon: "images/ir.png",
                    handler: function(grid, rowIndex) {
                      var rowControl = grid.store.getAt(rowIndex);
                      var idControl = grid.store
                        .getAt(rowIndex)
                        .get("idControl");
                      var idVersionControl = grid.store
                        .getAt(rowIndex)
                        .get("idVersionControl");
                      var stateEvaluation = grid.store
                        .getAt(rowIndex)
                        .get("stateEvaluation");
                      var windows = Ext.ComponentQuery.query(
                        "ViewWindowControlEntry"
                      )[0];
                      windows.setTitle(
                        windows.title +
                          "-" +
                          grid.store.getAt(rowIndex).get("description") +
                          "-" +
                          grid.store.getAt(rowIndex).get("descriptionControl")
                      );
                      windows.down("#containerResult").removeAll();
                      DukeSource.lib.Ajax.request({
                        method: "POST",
                        url:
                          "http://localhost:9000/giro/showFinalScoreControlByVersion.htm",
                        params: {
                          idControl: idControl,
                          idVersionControl: idVersionControl,
                          start: "0",
                          limit: "100"
                        },
                        success: function(response) {
                          response = Ext.decode(response.responseText);
                          if (response.success) {
                            for (var i = 0; i < response.data.length; i++) {
                              var scoreByGroup = {};
                              DukeSource.global.DirtyView.createGroup(scoreByGroup, response, i);
                              scoreByGroup.typeControl = row.get("typeControl");
                              windows.numberItems = response.data.length;
                              windows.groups = response.data;
                              DukeSource.global.DirtyView.createItemsToFinalScore(
                                windows,
                                scoreByGroup
                              );
                            }

                            windows.setWidth(
                              response.data.length > 2
                                ? 310 * response.data.length
                                : 760
                            );

                            var finalField = DukeSource.global.DirtyView.createScoreFinal(
                              response
                            );
                            DukeSource.global.DirtyView.createItemsToFinalScore(
                              windows,
                              finalField
                            );
                            DukeSource.global.DirtyView.createButton(windows);
                          } else {
                            DukeSource.global.DirtyView.messageWarning(response.message);
                          }
                        },
                        failure: function() {}
                      });
                      DukeSource.lib.Ajax.request({
                        method: "POST",
                        url:
                          "http://localhost:9000/giro/findControlByVersion.htm",
                        params: {
                          idControl: idControl,
                          idVersionControl: idVersionControl,
                          start: "0",
                          limit: "25"
                        },
                        success: function(response) {
                          var form = windows.down("form");
                          response = Ext.decode(response.responseText);
                          if (response.success) {
                            form
                              .down("#dateEvaluation")
                              .setValue(response.data.dateEvaluation);
                            form
                              .down("#userEvaluation")
                              .setValue(response.data.userEvaluation);
                            form
                              .down("#descriptionEvaluation")
                              .setValue(response.data.descriptionEvaluation);
                            form
                              .down("#idVersionControl")
                              .setValue(response.data.idVersionControl);
                          } else {
                            DukeSource.global.DirtyView.messageAlert(
                              DukeSource.global.GiroMessages.TITLE_WARNING,
                              response.mensaje,
                              Ext.Msg.WARNING
                            );
                          }
                          windows
                            .down("#descriptionEvaluation")
                            .focus(false, 100);
                        },
                        failure: function() {}
                      });
                      var grid1 = windows.down("grid");
                      grid1.store.getProxy().extraParams = {
                        idControl: idControl,
                        idVersionControl: idVersionControl
                      };
                      grid1.store.getProxy().url =
                        "http://localhost:9000/giro/showListControlTypeByVersion.htm";
                      grid1.down("pagingtoolbar").moveFirst();
                      if (stateEvaluation == "N") {
                        windows
                          .down("button[action=saveEvaluationControlParameter]")
                          .setDisabled(true);
                      } else {
                        windows
                          .down("button[action=saveEvaluationControlParameter]")
                          .setDisabled(false);
                      }
                      me.close();
                    }
                  }
                ]
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: 50,
              store: storeVersionControl,
              items: [
                {
                  xtype:"UpperCaseTrigger",
                  width: 120,
                  action: "searchGridAllVersionControl"
                }
              ]
            }
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
