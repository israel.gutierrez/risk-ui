Ext.define('SelectImpact', {
    extend: 'Ext.data.Model',
    fields: [
        'idImpact',
        'idImpactFeature',
        'idSelected',
        'description',
        'alias',
        'equivalentValueImpact',
        'descriptionImpactFeature',
        'descriptionImpact'
    ]
});

Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.windows.WindowSelectImpact', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowSelectImpact',
    width: 600,
    border: true,
    modal: true,
    layout: {
        type: 'fit'
    },
    initComponent: function () {
        var me = this;
        var idTypeMatrix = this.idTypeMatrix;
        var idOperationalRiskExposition = this.idOperationalRiskExposition;
        var idImpact = this.idImpact;
        var backPanel = this.backPanel;

        Ext.applyIf(me, {
            items: [

                {
                    xtype: 'form',
                    border: false,
                    bodyPadding: 5,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'container',
                            itemId: 'formFeatures',
                            flex: 1.1,
                            name: 'formFeatures',
                            padding: 2,
                            bodyPadding: 5,
                            layout: {
                                type: 'hbox',
                                pack: 'center',
                                align: 'stretch',
                                defaultMargins: {
                                    top: 0,
                                    right: 5,
                                    bottom: 0,
                                    left: 0
                                }
                            }
                        },
                        {
                            xtype: 'container',
                            itemId: 'containerResult',
                            name: 'containerResult',
                            flex: 1,
                            layout: {
                                type: 'hbox',
                                pack: 'center'
                            },
                            items: [
                                {
                                    xtype: 'ViewComboImpact',
                                    padding: '5 0 0 10',
                                    allowBlank: false,
                                    readOnly: true,
                                    msgTarget: 'side',
                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                    labelWidth: 130,
                                    labelAlign: 'top',
                                    fieldLabel: 'Nivel de Impacto',
                                    listeners: {
                                        render: function (cbo) {
                                            if (idTypeMatrix !== '')
                                                cbo.getStore().load({
                                                    params: {
                                                        idTypeMatrix: idTypeMatrix,
                                                        idOperationalRiskExposition: idOperationalRiskExposition
                                                    },
                                                    callback: function () {
                                                        cbo.setValue(idImpact);
                                                    }
                                                });
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            name: 'reasonImpact',
                            itemId: 'reasonImpact',
                            labelWidth: 130,
                            padding: 5,
                            maxLength: 2000,
                            height: 60,
                            labelAlign: 'top',
                            fieldLabel: 'Sustento Impacto',
                            hidden: hidden('VPEBtnReasonImpact'),
                            value: backPanel.down('#reasonImpact').getValue()
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Continuar',
                    iconCls: 'table_go',
                    disabled: !me.isVersionActive,
                    scale: 'medium',
                    handler: function () {
                        var impactBackPanel = backPanel.down('#idImpact');
                        impactBackPanel.getStore().load({
                                params: {
                                    idTypeMatrix: idTypeMatrix,
                                    idOperationalRiskExposition: idOperationalRiskExposition
                                },
                                callback: function () {
                                    if (me.down('form').getForm().isValid()) {
                                        impactBackPanel.setValue(me.down('ViewComboImpact').getValue());
                                        backPanel.riskInherent(backPanel, idTypeMatrix);
                                        backPanel.featureImpactIds = me.featureImpactIds;
                                        backPanel.impactIds = me.impactIds;
                                        backPanel.down('#reasonImpact').setValue(me.down('#reasonImpact').getValue());
                                        me.close();
                                    }
                                    else {
                                        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
                                    }
                                }
                            }
                        );
                    }
                },
                {
                    text: 'Cancelar',
                    scale: 'medium',
                    iconCls: 'cancel',
                    handler: function () {
                        me.close();
                    }
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    },
    createCombos: function (json) {
        var me = this;
        var impactIds = [];
        var featureIds = [];
        var container = me.down('#formFeatures');

        var data = json.data;

        for (var i = 0; i < data.length; i++) {
            var item = data[i];

            container.add({
                xtype: 'combobox',
                fieldLabel: item['description'],
                displayField: 'alias',
                allowBlank: true,
                editable:false,
                labelAlign: 'top',
                value: data.length === 1 ? me.idImpact : item['idSelected'],
                trigger1Cls: 'x-form-clear-trigger',
                trigger2Cls: 'x-form-arrow-trigger',
                onTrigger1Click: function () {
                    this.reset();
                    this.clearValue();
                    this.setValue(null);
                    calculateImpact(me, impactIds, featureIds)
                },
                flex: 1,
                valueField: 'idImpact',
                store: {
                    model: 'SelectImpact',
                    data: item['children'],
                    proxy: {
                        type: "memory"
                    }
                },
                listeners: {
                    select: function () {
                        calculateImpact(me, impactIds, featureIds)
                    }
                }
            });

        }
    }

});

function calculateImpact(me, impactIds, featureIds) {
    impactIds.splice(0, impactIds.length);
    featureIds.splice(0, featureIds.length);

    me.down('#formFeatures').query('.combobox').forEach(function (c) {
        impactIds.push(c.getValue());

        var record = c.findRecord(c.valueField, c.getValue());
        if (record) {
            featureIds.push(record.data['idImpactFeature'])
        }
    });

    var impact = Math.max.apply(null, impactIds);
    me.down('ViewComboImpact').setValue(impact.toString());

    me.featureImpactIds = {data: featureIds};
    me.impactIds = {data: impactIds};
}