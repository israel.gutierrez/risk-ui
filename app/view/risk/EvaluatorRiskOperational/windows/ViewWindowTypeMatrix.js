var sm = new Ext.selection.CheckboxModel({
  checkOnly: true
});

Ext.define("ModelGridTypeMatrix", {
  extend: "Ext.data.Model",
  fields: ["idTypeMatrix", "description"]
});

var StoreGridTypeMatrix = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelGridTypeMatrix",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowTypeMatrix",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowTypeMatrix",
    height: 300,
    modal: true,
    width: 600,
    layout: {
      type: "fit"
    },
    tbar: [
      "-",
      {
        xtype: "ViewComboOperationalRiskExposition",
        labelWidth: 140,
        width: 250,
        readOnly: true,
        fieldLabel: "Patrimonio efectivo"
      }
    ],
    initComponent: function() {
      var idMatrix = this.idMatrix;
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            store: StoreGridTypeMatrix,
            padding: "2 2 2 2",
            titleAlign: "center",
            loadMask: true,
            columnLines: true,
            selModel: sm,
            columns: [
              {
                header: "C&oacute;digo",
                align: "center",
                dataIndex: "idTypeMatrix",
                width: 70
              },
              {
                header: "Descripci&oacute;n",
                dataIndex: "description",
                flex: 1
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridTypeMatrix,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            }
          }
        ],
        buttons: [
          {
            text: "Salir",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          },
          {
            text: "Guardar",
            scale: "medium",
            action: "saveReplicationTypeMatrix",
            iconCls: "save"
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);
