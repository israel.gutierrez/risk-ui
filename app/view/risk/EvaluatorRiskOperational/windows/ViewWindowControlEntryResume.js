Ext.define("ModelFeatureControl", {
  extend: "Ext.data.Model",
  fields: [
    "idControlType",
    "weightedControl",
    "description",
    "idDetailControlType",
    "valueTypificationControl",
    "descriptionGroupControlType",
    "idGroupControlType",
    "typificationControl"
  ]
});

var storeFeatureControl = Ext.create("Ext.data.Store", {
  model: "ModelFeatureControl",
  groupField: "descriptionGroupControlType",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListControlTypeToEvaluateControl.htm",
    extraParams: {
      idDetailControl: "0"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowControlEntryResume",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowControlEntryResume",
    width: 860,
    border: false,
    layout: {
      type: "fit"
    },
    title: "Detalle de control",

    configWeightControl: function(record) {
      var me = this;
      var weightControl = me.down("#weightControl");
      DukeSource.global.DirtyView.changeObligatoryElement(weightControl);

      if (CONTROL_USE_WEIGHT === "true") {
        weightControl.setValue("");
      } else {
        weightControl.setValue("100");
      }

      if (record.get("checkEvidence") === "N") {
        weightControl.setValue("0");
        DukeSource.global.DirtyView.toReadOnly(weightControl);
      }
    },

    configFocusControl: function(control) {
      var me = this;
      var focusControl = me.down("#focusControl");

      if (RISK_CONTROL_AFFECT !== "D") {
        DukeSource.global.DirtyView.toReadOnly(focusControl);

        if (RISK_CONTROL_AFFECT === "IF")
          focusControl.setValue(RISK_CONTROL_AFFECT);
        else
          focusControl.setValue(control.get("typeControl") === "C" ? "F" : "I");
      }
    },

    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 5,
            title: "",
            items: [
              {
                xtype: "textfield",
                name: "idRisk",
                itemId: "idRisk",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "versionCorrelative",
                itemId: "versionCorrelative",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "idControl",
                itemId: "idControl",
                hidden: true
              },
              {
                xtype: "textfield",
                itemId: "idDetailControl",
                name: "idDetailControl",
                value: "id",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "idVersionControl",
                itemId: "idVersionControl",
                value: "id",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "idRiskWeaknessDetail",
                itemId: "idRiskWeaknessDetail",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "typeControl",
                itemId: "typeControl",
                hidden: true
              },
              {
                xtype: "container",
                height: 25,
                anchor: "100%",
                layout: {
                  type: "hbox",
                  align: "middle"
                },
                items: [
                  {
                    xtype:"UpperCaseTextFieldReadOnly",
                    anchor: "100%",
                    flex: 1,
                    fieldLabel: "Código control",
                    readonly: true,
                    fieldCls: "readOnlyText",
                    itemId: "codeControl",
                    name: "codeControl"
                  },
                  {
                    xtype: "datefield",
                    anchor: "100%",
                    fieldLabel: "Fecha evaluación",
                    flex: 1,
                    padding: "0 0 0 5",
                    value: new Date(),
                    format: "d/m/Y",
                    readOnly: true,
                    fieldCls: "readOnlyText",
                    name: "dateEvaluation",
                    itemId: "dateEvaluation"
                  },
                  {
                    xtype:"UpperCaseTextFieldReadOnly",
                    anchor: "100%",
                    fieldLabel: "Usuario evaluación",
                    flex: 1.4,
                    padding: "0 0 0 5",
                    readOnly: true,
                    value: fullName,
                    fieldCls: "readOnlyText",
                    name: "userEvaluation",
                    itemId: "userEvaluation"
                  }
                ]
              },
              {
                xtype: "container",
                height: 64,
                anchor: "100%",
                layout: {
                  type: "hbox",
                  align: "middle"
                },
                items: [
                  {
                    xtype: 'UpperCaseTextArea',
                    height: 50,
                    flex: "1",
                    maxLength: 500,
                    readOnly: true,
                    allowBlank: false,
                    fieldLabel: "Descripci&oacute;n",
                    itemId: "description",
                    name: "description"
                  },
                  {
                    xtype: "button",
                    width: 67,
                    height: 50,
                    textAlign: "center",
                    itemId: "buttonSearchControl",
                    text: "Buscar",
                    iconCls: "search",
                    handler: function() {
                      var win = me;

                      var searchControl = Ext.create(
                        "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowSearchControl",
                        {
                          modal: true
                        }
                      ).show();
                      searchControl
                        .down("grid")
                        .on("itemdblclick", function(grid, record) {
                          var idControl = record.get("idControl");
                          win.configFocusControl(record);
                          win.configWeightControl(record);

                          if (record.get("stateEvaluated") === "N") {
                            DukeSource.global.DirtyView.messageWarning(
                              "Debe evaluar el control"
                            );
                          } else {
                            win.down("#containerResult").removeAll();

                            DukeSource.lib.Ajax.request({
                              method: "POST",
                              url:
                                "http://localhost:9000/giro/showFinalScoreControlActive.htm",
                              params: {
                                idControl: idControl,
                                start: "0",
                                limit: "100"
                              },
                              success: function(response) {
                                response = Ext.decode(response.responseText);
                                if (response.success) {
                                  for (
                                    var i = 0;
                                    i < response.data.length;
                                    i++
                                  ) {
                                    var scoreGroup = {};
                                    DukeSource.global.DirtyView.createGroup(
                                      scoreGroup,
                                      response,
                                      i
                                    );

                                    scoreGroup.typeControl = record.get(
                                      "typeControl"
                                    );
                                    win.numberItems = response.data.length;
                                    win.groups = response.data;
                                    DukeSource.global.DirtyView.createItemsToFinalScore(
                                      win,
                                      scoreGroup
                                    );
                                  }

                                  win.setWidth(
                                    response.data.length > 2
                                      ? 310 * response.data.length
                                      : 760
                                  );

                                  var scoreFinal = DukeSource.global.DirtyView.createScoreFinal(
                                    response
                                  );
                                  scoreFinal.typeControl = record.get(
                                    "typeControl"
                                  );

                                  DukeSource.global.DirtyView.createItemsToFinalScore(
                                    win,
                                    scoreFinal
                                  );
                                  win
                                    .down("#" + scoreFinal.idItemValue)
                                    .setFieldLabel("Rating Total");

                                  DukeSource.global.DirtyView.createButton(win);
                                  win
                                    .down("form")
                                    .getForm()
                                    .loadRecord(record);
                                  win
                                    .down("#descriptionEvaluation")
                                    .setValue(null);
                                } else {
                                  DukeSource.global.DirtyView.messageWarning(
                                    response.message
                                  );
                                }
                              }
                            });
                            DukeSource.lib.Ajax.request({
                              method: "POST",
                              url:
                                "http://localhost:9000/giro/findControlById.htm",
                              params: {
                                propertyFind: "idControl",
                                valueFind: idControl,
                                propertyOrder: "description",
                                start: "0",
                                limit: "25"
                              },
                              success: function(response) {
                                var form = win.down("form");
                                response = Ext.decode(response.responseText);
                                if (response.success) {
                                  form.getForm().setValues(response.data);
                                  form
                                    .down("#description")
                                    .setValue(
                                      response.data["descriptionControl"]
                                    );
                                } else {
                                  DukeSource.global.DirtyView.messageWarning(
                                    response.message
                                  );
                                }
                                win
                                  .down("#descriptionEvaluation")
                                  .focus(false, 100);
                              }
                            });

                            var grid1 = win.down("grid");
                            grid1.store.getProxy().extraParams = {
                              idControl: idControl
                            };
                            grid1.store.getProxy().url =
                              "http://localhost:9000/giro/showListControlTypeToEvaluateControl.htm";
                            grid1.down("pagingtoolbar").doRefresh();
                            searchControl.close();
                          }
                        });
                    }
                  }
                ]
              },
              {
                xtype: "container",
                layout: {
                  type: "hbox",
                  align: "stretch"
                },
                height: 26,
                items: [
                  {
                    xtype: "combobox",
                    displayField: "description",
                    valueField: "value",
                    editable: false,
                    readOnly: true,
                    padding: "0 5 0 0",
                    hidden: hidden("WECCheckEvidence"),
                    fieldCls: "readOnlyText",
                    fieldLabel: getName("WECCheckEvidence"),
                    name: "checkEvidence",
                    itemId: "checkEvidence",
                    forceSelection: true,
                    store: {
                      fields: ["value", "description"],
                      pageSize: 9999,
                      autoLoad: true,
                      proxy: {
                        actionMethods: {
                          create: "POST",
                          read: "POST",
                          update: "POST"
                        },
                        type: "ajax",
                        url:
                          "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                        extraParams: {
                          propertyOrder: "description",
                          propertyFind: "identified",
                          valueFind: "CONTROL_PIVOT_FACTOR"
                        },
                        reader: {
                          type: "json",
                          root: "data",
                          successProperty: "success"
                        }
                      }
                    }
                  },
                  {
                    xtype: "combobox",
                    allowBlank: true,
                    editable: false,
                    hidden: hidden("WECDescriptionEvaluationResume"),
                    padding: "0 5 0 0",
                    fieldLabel: "Valorización",
                    itemId: "descriptionEvaluation",
                    name: "descriptionEvaluation",
                    queryMode: "local",
                    displayField: "description",
                    valueField: "value",
                    store: {
                      fields: ["value", "description"],
                      pageSize: 9999,
                      autoLoad: true,
                      proxy: {
                        actionMethods: {
                          create: "POST",
                          read: "POST",
                          update: "POST"
                        },
                        type: "ajax",
                        url:
                          "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                        extraParams: {
                          propertyOrder: "description",
                          propertyFind: "identified",
                          valueFind: "CONTROL_FIELD_AUX"
                        },
                        reader: {
                          type: "json",
                          root: "data",
                          successProperty: "success"
                        }
                      }
                    }
                  },
                  {
                    xtype: "NumberDecimalNumberObligatory",
                    fieldLabel: getName("CTRL_WRCE_NMBF_WeightControl"),
                    decimalPrecision: 0,
                    name: "weightControl",
                    padding: "0 5 0 0",
                    itemId: "weightControl",
                    allowBlank: blank("CTRL_WRCE_NMBF_WeightControl"),
                    hidden: hidden("CTRL_WRCE_NMBF_WeightControl"),
                    minValue: 0,
                    maxValue: 100
                  },
                  {
                    xtype: "combobox",
                    queryMode: "local",
                    displayField: "description",
                    valueField: "id",
                    editable: false,
                    hidden: hidden("WECFocusControl"),
                    allowBlank: blank("WECFocusControl"),
                    fieldCls: "obligatoryTextField",
                    fieldLabel: "Foco",
                    name: "focusControl",
                    itemId: "focusControl",
                    store: {
                      fields: ["id", "description"],
                      data: [
                        { id: "F", description: "Frecuencia" },
                        { id: "I", description: "Impacto" },
                        { id: "IF", description: "Ambos" }
                      ]
                    }
                  }
                ]
              },
              {
                xtype: "fieldset",
                title: "Evaluaci&oacute;n del control",
                margin: "5 0 0 0",
                items: [
                  {
                    xtype: "container",
                    flex: 3,
                    padding: 2,
                    layout: {
                      align: "stretch",
                      type: "hbox"
                    },
                    items: [
                      {
                        xtype: "gridpanel",
                        height: 300,
                        padding: "0 1 0 0",
                        store: storeFeatureControl,
                        features: [
                          {
                            ftype: "grouping",
                            groupHeaderTpl: "CONTROL : {name}"
                          }
                        ],
                        flex: 1,
                        titleAlign: "center",
                        columns: [
                          {
                            xtype: "rownumberer",
                            width: 25,
                            sortable: false
                          },
                          {
                            dataIndex: "description",
                            width: 120,
                            text: "Caracter&iacute;stica"
                          },
                          {
                            dataIndex: "weightedControl",
                            width: 80,
                            align: "center",
                            text: "Ponderado"
                          },
                          {
                            header: "Valor caracter&iacute;stica",
                            flex: 1,
                            dataIndex: "typificationControl"
                          },
                          {
                            header: "Calificaci&oacute;n",
                            width: 90,
                            align: "center",
                            dataIndex: "valueTypificationControl"
                          }
                        ],
                        bbar: {
                          xtype: "pagingtoolbar",
                          pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                          store: storeFeatureControl,
                          displayInfo: true,
                          displayMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                          emptyMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                        }
                      }
                    ]
                  },
                  {
                    xtype: "container",
                    bodyPadding: 3,
                    items: [
                      {
                        xtype: "container",
                        itemId: "containerResult",
                        anchor: "100%",
                        defaults: { margin: "0 10 0 0" },
                        layout: {
                          type: "hbox",
                          align: "stretch"
                        },
                        items: []
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ],
        buttonAlign: "center",
        buttons: [
          {
            text: "Guardar",
            disabled: !me.isVersionActive,
            action: "saveControlRisk",
            scale: "medium",
            iconCls: "save"
          },
          {
            text: "Salir",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
