Ext.define("ModelRiskAll", {
  extend: "Ext.data.Model",
  fields: ["codeRisk", "catalogRisk", "keyRiskIndicator", "nameCatalogRisk"]
});

var storeRiskAll = Ext.create("Ext.data.Store", {
  model: "ModelRiskAll",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListRolesActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowActionPlanDetail",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowActionPlanDetail",
    layout: "fit",
    height: 500,
    width: 800,
    anchorSize: 100,
    titleAlign: "center",
    border: false,
    initComponent: function() {
      var riskIndicator = this.riskIndicator;
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            padding: "0 1 0 0",
            itemId: "gridRiskAll",
            store: storeRiskAll,
            loadMask: true,
            columnLines: true,
            multiSelect: true,
            columns: [
              {
                xtype: "rownumberer",
                width: 25,
                sortable: false
              },
              {
                dataIndex: "codeRisk",
                width: 100,
                align: "center",
                text: "CODIGO"
              },
              {
                dataIndex: "nameCatalogRisk",
                flex: 5,
                text: "DESCRIPCION"
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: 50,
              store: storeRiskAll,
              items: [
                {
                  xtype:"UpperCaseTrigger",
                  width: 120,
                  action: "searchGridAllRole"
                }
              ]
            }
          }
        ],
        buttons: [
          {
            text: "SALIR",
            iconCls: "logout",
            scope: this,
            handler: this.close
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
