Ext.define("ModelComboDetailControlType", {
  extend: "Ext.data.Model",
  fields: [
    "idDetailControlType",
    "idControlType",
    "typificationControl",
    "weightedControl",
    "valueTypificationControl",
    "valuetypificationControl"
  ]
});

var storeComboDetailControlType = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboDetailControlType",
  pageSize: 100,
  autoLoad: true,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/findDetailFeatureControl.htm",
    extraParams: {
      propertyFind: "1"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("ModelFeatureControl", {
  extend: "Ext.data.Model",
  fields: [
    "idControlType",
    "weightedControl",
    "description",
    "idDetailControlType",
    "valueTypificationControl",
    "descriptionGroupControlType",
    "idGroupControlType",
    "typificationControl",
    "activityControl",
    "percentageGroup"
  ]
});

var storeFeatureControl = Ext.create("Ext.data.Store", {
  model: "ModelFeatureControl",
  groupField: "descriptionGroupControlType",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListControlTypeToEvaluateControl.htm",
    extraParams: {
      idDetailControl: "0"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowControlEntry",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowControlEntry",
    width: 760,
    border: false,
    layout: {
      type: "fit"
    },
    title: "DETALLE DE CONTROL",
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            itemId: "formHeaderControl",
            bodyPadding: 5,
            title: "",
            fieldDefaults: {
              labelCls: "changeSizeFontToEightPt",
              fieldCls: "changeSizeFontToEightPt"
            },
            items: [
              {
                xtype: "textfield",
                name: "idRisk",
                itemId: "idRisk",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "versionCorrelative",
                itemId: "versionCorrelative",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "idControl",
                itemId: "idControl",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "typeControl",
                itemId: "typeControl",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "idDetailControl",
                itemId: "idDetailControl",
                value: "id",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "idVersionControl",
                itemId: "idVersionControl",
                value: "id",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "idRiskWeaknessDetail",
                itemId: "idRiskWeaknessDetail",
                hidden: true
              },
              {
                xtype: "container",
                height: 25,
                anchor: "100%",
                layout: {
                  type: "hbox",
                  align: "middle"
                },
                items: [
                  {
                    xtype:"UpperCaseTextFieldReadOnly",
                    anchor: "100%",
                    labelWidth: 140,
                    flex: 1,
                    fieldLabel: "C&oacute;digo",
                    readonly: true,
                    fieldCls: "readOnlyText",
                    itemId: "codeControl",
                    name: "codeControl"
                  },
                  {
                    xtype: "datefield",
                    anchor: "100%",
                    padding: "0 0 0 5",
                    fieldLabel: "Fecha evaluación",
                    flex: 1,
                    value: new Date(),
                    format: "d/m/Y",
                    readOnly: true,
                    fieldCls: "readOnlyText",
                    name: "dateEvaluation",
                    itemId: "dateEvaluation"
                  },
                  {
                    xtype:"UpperCaseTextFieldReadOnly",
                    anchor: "100%",
                    fieldLabel: "Usuario evaluación",
                    padding: "0 0 0 5",
                    flex: 1.4,
                    readOnly: true,
                    value: fullName,
                    fieldCls: "readOnlyText",
                    name: "userEvaluation",
                    itemId: "userEvaluation"
                  }
                ]
              },
              {
                xtype: 'UpperCaseTextArea',
                labelWidth: 140,
                padding: "5 0 0 0",
                height: 50,
                anchor: "100%",
                maxLength: 500,
                readOnly: true,
                allowBlank: false,
                fieldCls: "readOnlyText",
                fieldLabel: "Descripci&oacute;n del control",
                itemId: "descriptionControl",
                name: "descriptionControl"
              },
              {
                xtype: "combobox",
                displayField: "description",
                valueField: "value",
                value: CONTROL_PIVOT_FACTOR_DEFAULT_VALUE,
                editable: false,
                hidden: hidden("WECCheckEvidence"),
                allowBlank: blank("WECCheckEvidence"),
                fieldCls: styleField("WECCheckEvidence"),
                fieldLabel: getName("WECCheckEvidence"),
                name: "checkEvidence",
                itemId: "checkEvidence",
                labelWidth: 140,
                forceSelection: true,
                store: {
                  fields: ["value", "description"],
                  pageSize: 9999,
                  autoLoad: true,
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url:
                      "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                    extraParams: {
                      propertyOrder: "description",
                      propertyFind: "identified",
                      valueFind: "CONTROL_PIVOT_FACTOR"
                    },
                    reader: {
                      type: "json",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                },
                listeners: {
                  afterrender: function(cbo) {
                    cbo.setValue(CONTROL_PIVOT_FACTOR_DEFAULT_VALUE);
                  }
                }
              },
              {
                xtype: 'UpperCaseTextArea',
                labelWidth: 140,
                height: 35,
                anchor: "100%",
                maxLengthText:
                  DukeSource.global.GiroMessages.MESSAGE_MAX_CHARACTER + "2000",
                maxLength: 2000,
                allowBlank: true,
                hidden: hidden("WECDescriptionEvaluation"),
                fieldLabel: "DESCRIPCI&Oacute;N DE EVALUACIÓN",
                itemId: "descriptionEvaluation",
                name: "descriptionEvaluation"
              },
              {
                xtype: "fieldset",
                padding: 5,
                title: "EVALUACI&Oacute;N DE CONTROL",
                items: [
                  {
                    xtype: "container",
                    padding: "2 2 2 2",
                    layout: {
                      align: "stretch",
                      type: "hbox"
                    },
                    items: [
                      {
                        xtype: "gridpanel",
                        itemId: "gridEvaluationControl",
                        height: 300,
                        padding: "0 1 0 0",
                        store: storeFeatureControl,
                        features: [
                          {
                            ftype: "grouping",
                            groupHeaderTpl: "CONTROL : {name}"
                          }
                        ],
                        flex: 1,
                        titleAlign: "center",
                        plugins: [
                          Ext.create("Ext.grid.plugin.CellEditing", {
                            clicksToEdit: 1,
                            listeners: {
                              beforeedit: function(editor, e) {
                                var storeDetail = Ext.getStore(
                                  storeComboDetailControlType
                                );
                                storeDetail.loadData([], false);
                                storeDetail.load({
                                  url:
                                    "http://localhost:9000/giro/findDetailControlType.htm",
                                  params: {
                                    propertyFind: e.record.get("idControlType"),
                                    propertyOrder: "valuetypificationControl"
                                  }
                                });
                              }
                            }
                          })
                        ],
                        columns: [
                          {
                            xtype: "rownumberer",
                            width: 25,
                            sortable: false
                          },
                          {
                            dataIndex: "description",
                            width: 270,
                            text: "Característica"
                          },
                          {
                            dataIndex: "weightedControl",
                            width: 80,
                            align: "center",
                            text: "Ponderado"
                          },
                          {
                            header: "Eval de la característica",
                            tdCls: "custom-column",
                            dataIndex: "idDetailControlType",
                            flex: 1,
                            editor: {
                              xtype: "ViewComboDetailControlType",
                              store: storeComboDetailControlType,
                              msgTarget: "side",
                              fieldCls: "obligatoryTextField",
                              blankText:
                                DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                              allowBlank: false,
                              listeners: {
                                select: function(cbo, record) {
                                  me.down("grid")
                                    .getSelectionModel()
                                    .getSelection()[0]
                                    .set(
                                      "valueTypificationControl",
                                      record[0].data["valuetypificationControl"]
                                    );
                                  me.down("grid")
                                    .getSelectionModel()
                                    .getSelection()[0]
                                    .set(
                                      "typificationControl",
                                      record[0].data["typificationControl"]
                                    );

                                  if (
                                    me.down("#typeControl").getValue() ===
                                    DukeSource.global.GiroConstants
                                      .MITIGATION_CONTROL
                                  ) {
                                    qualificationControl(
                                      me,
                                      me.down("grid"),
                                      cbo
                                    );
                                  } else {
                                    qualificationInsurance(
                                      me,
                                      me.down("grid"),
                                      cbo
                                    );
                                  }
                                }
                              }
                            },
                            renderer: function(val, metadata, record) {
                              return record.get("typificationControl");
                            }
                          },
                          {
                            header: "Calificación",
                            width: 90,
                            align: "center",
                            dataIndex: "valueTypificationControl",
                            renderer: function(val, metadata, record) {
                              return record.get("valueTypificationControl");
                            }
                          }
                        ],
                        bbar: {
                          xtype: "pagingtoolbar",
                          pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                          store: storeFeatureControl,
                          displayInfo: true,
                          displayMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                          emptyMsg:
                            DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                        }
                      }
                    ]
                  },
                  {
                    xtype: "form",
                    bodyPadding: 3,
                    items: [
                      {
                        xtype: "container",
                        itemId: "containerResult",
                        defaults: { margin: "0 10 0 0" },
                        layout: {
                          type: "hbox",
                          align: "stretch"
                        },
                        items: []
                      }
                    ]
                  }
                ]
              }
            ],
            dockedItems: [
              {
                xtype: "toolbar",
                dock: "top",
                hidden: true,
                items: [
                  {
                    xtype: "button",
                    text: "Iniciar nueva evaluación",
                    iconCls: "add",
                    action: "newVersionControl",
                    scale: "medium"
                  },
                  {
                    xtype: "button",
                    text: "Consultar evaluaciones",
                    iconCls: "consult",
                    action: "consultVersionControl",
                    scale: "medium"
                  }
                ]
              }
            ]
          }
        ],
        buttonAlign: "center",
        buttons: [
          {
            text: "Guardar",
            iconCls: "save",
            itemId: "saveEvaluationControl",
            scale: "medium",
            handler: function() {
              var arrayScoreGroups = me.buildObjectGroups();

              var records = me
                .down("grid")
                .getStore()
                .getRange();
              var arrayDataToSave = [];
              var flag = true;
              Ext.Array.each(records, function(record) {
                arrayDataToSave.push(record.data);
                if (record.data["valueTypificationControl"] === "") {
                  flag = false;
                }
              });

              var panelPermission = "";
              var gridPermission = "";
              if (flag) {
                if (me.origin === "evaluationRisk") {
                  panelPermission = "PanelProcessRiskEvaluation";
                  gridPermission = Ext.ComponentQuery.query(
                    "ViewWindowSearchControl grid"
                  )[0];
                } else {
                  panelPermission = "ViewPanelRegisterControl";
                  gridPermission = Ext.ComponentQuery.query(
                    "ViewPanelRegisterControl ViewGridPanelRegisterControl"
                  )[0];
                }
                DukeSource.lib.Ajax.request({
                  waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                  method: "POST",
                  url:
                    "http://localhost:9000/giro/saveEvaluationControlParameter.htm?nameView=" +
                    panelPermission,
                  params: {
                    grid: Ext.JSON.encode(arrayDataToSave),
                    jsonData: Ext.JSON.encode(
                      me.down("#formHeaderControl").getValues()
                    ),
                    scoreGroups: Ext.JSON.encode(arrayScoreGroups)
                  },
                  scope: this,
                  success: function(response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                      DukeSource.global.DirtyView.messageNormal(
                        response.message
                      );
                      gridPermission.getStore().load();
                      me.close();
                    } else {
                      DukeSource.global.DirtyView.messageWarning(
                        response.message
                      );
                    }
                  },
                  failure: function() {}
                });
              } else {
                DukeSource.global.DirtyView.messageWarning(
                  "Debe realizar la evaluación de todas las características del Control"
                );
              }
            }
          },
          {
            text: "Salir",
            scope: this,
            handler: this.close,
            iconCls: "logout",
            scale: "medium"
          }
        ]
      });

      me.callParent(arguments);
    },

    buildObjectGroups: function() {
      var win = this;
      var array = [];

      for (var i = 0; i < win.numberItems; i++) {
        var objectGroup = {};

        var group = win.groups[i];
        var name = group.abbreviation;

        var containerGroup = win.down("#container" + name);

        containerGroup.query(".combobox,.textfield").forEach(function(c) {
          objectGroup[c.nameIntern] = c.getValue();
          objectGroup["idGroupControlType"] = group["idGroupControl"];
        });
        array.push(objectGroup);
      }
      return array;
    }
  }
);

function qualificationInsurance(win, grid, cbo) {
  var numberFeatures = grid.getStore().getCount();
  var valueSelect;
  var weightSelect;
  var subTotal = 1;

  for (var int = 0; int < numberFeatures; int++) {
    var item = grid.store.data.items[int].data;

    valueSelect =
      item.valueTypificationControl === ""
        ? 1
        : parseFloat(item.valueTypificationControl);
    weightSelect =
      parseFloat(item.weightedControl) / parseFloat(CONTROL_MAX_VALUE_FEATURE);
    subTotal = subTotal * (valueSelect * weightSelect);
  }
  subTotal = subTotal / 100;

  defineLevel(
    win.down("#scoreCover"),
    win.down("#percentCover"),
    win.down("#valueCover"),
    parseFloat(subTotal.toFixed(2))
  );
  defineFinalLevel(
    win.down("#scoreControl"),
    win.down("#percentScoreControl"),
    win.down("#valueScoreControl"),
    parseFloat(subTotal.toFixed(2))
  );
}

function qualificationControl(win, grid, cbo) {
  var total = grid.getStore().getCount();

  for (var i = 0; i < win.numberItems; i++) {
    var group = win.groups[i];
    var name = group.abbreviation;

    var sumProductValue = 0;

    for (var int = 0; int < total; int++) {
      var item = grid.store.data.items[int].data;
      var valueSelect;
      var weightSelect;

      if (group["idGroupControl"] === item["idGroupControlType"]) {
        valueSelect =
          item.valueTypificationControl === ""
            ? 0
            : parseFloat(item.valueTypificationControl);
        weightSelect =
          parseFloat(item.weightedControl) /
          parseFloat(CONTROL_MAX_VALUE_FEATURE);

        sumProductValue = sumProductValue + valueSelect * weightSelect;
      }
    }
    defineLevel(
      win.down("#score" + name),
      win.down("#percent" + name),
      win.down("#value" + name),
      parseFloat(sumProductValue.toFixed(2))
    );
  }

  valueFinalControl(
    win.down("#scoreControl"),
    win.down("#percentScoreControl"),
    win.down("#valueScoreControl"),
    win
  );
}

function defineLevel(combo, percent, value, valueTotal) {
  var valueScore = "";

  if (MODE_CONTROL === "AVG") {
    var valueForGroup = valueTotal / (combo.percentage / 100);
  } else {
    valueForGroup = valueTotal * (combo.percentage / 100);
  }

  for (var i = 0; i < combo.store.data.items.length; i++) {
    var itemGroup = combo.store.data.items[i].data;

    var ranInf = valueTotal === 0 ? 0.1 : valueTotal;

    if (
      ranInf > itemGroup.minEffectiveness &&
      valueTotal <= itemGroup.maxEffectiveness
    ) {
      combo.setValue(itemGroup.idScoreControl);
      percent.setValue(valueTotal);
      value.setValue(valueForGroup.toFixed(2));

      valueScore = itemGroup.scoreControl;
      combo.setFieldStyle(
        "background-color: #" +
          itemGroup.codeColour +
          "; background-image: none;color:#000000;"
      );
    }
  }
  return valueScore;
}

function defineFinalLevel(combo, percent, value, valueTotal) {
  var valueScore = "";
  for (var i = 0; i < combo.store.data.items.length; i++) {
    var itemFinal = combo.store.data.items[i].data;
    var ranInf = valueTotal === 0 ? 0.1 : valueTotal;

    if (
      ranInf > itemFinal.minEffectiveness &&
      valueTotal <= itemFinal.maxEffectiveness
    ) {
      combo.setValue(itemFinal.idScoreControl);
      value.setValue(itemFinal.scoreControl);
      valueScore = itemFinal.scoreControl;
      combo.setFieldStyle(
        "background-color: #" +
          itemFinal.codeColour +
          "; background-image: none;color:#000000;"
      );
    }
    percent.setValue(valueTotal);
  }
  return valueScore;
}

function valueFinalControl(cbo, percent, value, win) {
  var valueTotal = "0.00";

  for (var i = 0; i < win.numberItems; i++) {
    var group = win.groups[i];
    var name = group.abbreviation;

    var percentGroup = parseFloat(group["percentageGroup"]) / 100;
    var valueGroup = parseFloat(win.down("#percent" + name).getValue());

    if (MODE_CONTROL === "AVG") {
      valueTotal = parseFloat(valueTotal) + valueGroup / percentGroup;
    } else {
      valueTotal = parseFloat(valueTotal) + valueGroup * percentGroup;
    }
  }

  if (MODE_CONTROL === "AVG") {
    valueTotal = valueTotal / win.numberItems;
  }

  defineFinalLevel(cbo, percent, value, valueTotal.toFixed(2));
}
