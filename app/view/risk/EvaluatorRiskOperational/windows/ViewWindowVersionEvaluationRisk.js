Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowVersionEvaluationRisk",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowVersionEvaluationRisk",
    width: 350,
    layout: {
      type: "fit"
    },
    border: false,
    title: "Versión de la evaluación",
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            title: "",
            items: [
              {
                xtype: "textfield",
                hidden: true,
                value: "N",
                name: "isImport"
              },
              {
                xtype: "textfield",
                name: "idVersionEvaluation",
                hidden: true,
                value: "id"
              },
              {
                xtype: "textfield",
                name: "idRisk",
                itemId: "idRisk",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "idMaxExposition",
                value: idMaxExpositionActive,
                hidden: true
              },
              {
                xtype: "textfield",
                fieldCls: "readOnlyText",
                readOnly: true,
                anchor: "100%",
                name: "description",
                value:
                  me.record.get("versionCorrelative") === ""
                    ? 1
                    : parseInt(me.record.get("versionCorrelative")) + 1,
                fieldLabel: "Evaluación",
                listeners: {
                  afterrender: function(field) {
                    field.focus(false, 200);
                  }
                }
              },
              {
                xtype:"UpperCaseTextFieldReadOnly",
                anchor: "100%",
                fieldLabel: "Responsable",
                name: "fullName",
                value: fullName
              },
              {
                xtype: "datefield",
                msgTarget: "side",
                anchor: "100%",
                fieldCls: "obligatoryTextField",
                format: "d/m/Y",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                allowBlank: false,
                value: new Date(),
                maxValue: new Date(),
                name: "dateEvaluation",
                fieldLabel: "Fecha"
              }
            ]
          }
        ],
        buttons: [
          {
            text: "Guardar",
            scale: "medium",
            iconCls: "save",
            handler: function() {
              var panel = Ext.ComponentQuery.query(
                "ViewPanelIdentifyRiskOperational"
              )[0];
              var gridIdentify = panel.down("#riskIdentify");

              var idRisk = me.down("#idRisk").getValue();

              if (
                me
                  .down("form")
                  .getForm()
                  .isValid()
              ) {
                DukeSource.lib.Ajax.request({
                  method: "POST",
                  url:
                    "http://localhost:9000/giro/saveVersionEvaluation.htm?nameView=PanelProcessRiskEvaluation",
                  params: {
                    jsonData: Ext.JSON.encode(me.down("form").getValues())
                  },
                  success: function(response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                      var identifyIndex = DukeSource.global.DirtyView.getSelectedRowIndex(
                        gridIdentify
                      );
                      gridIdentify.getStore().load({
                        callback: function() {
                          if (identifyIndex !== -1) {
                            gridIdentify
                              .getSelectionModel()
                              .select(identifyIndex);
                          } else {
                          }
                        }
                      });

                      panel.down("#buttonIr").setVisible(true);
                      panel
                        .down("ViewComboGridEvaluationRisk")
                        .setVisible(true);
                      var comboVersion = panel.down(
                        "ViewComboGridEvaluationRisk"
                      );

                      comboVersion.getStore().load({
                        url:
                          "http://localhost:9000/giro/showVersionEvaluationActives.htm",
                        params: {
                          idRisk: idRisk
                        },
                        callback: function(records) {
                          Ext.Array.each(records, function(record) {
                            if (record.data["state"] === "S") {
                              comboVersion.setValue(
                                record.data["idVersionEvaluation"]
                              );
                            }
                          });
                        }
                      });

                      Ext.MessageBox.show({
                        title: DukeSource.global.GiroMessages.TITLE_MESSAGE,
                        msg:
                          response.message +
                          ", desea continuar con la evaluaci&oacute;n?",
                        icon: Ext.Msg.QUESTION,
                        buttonText: { yes: "Si" },
                        buttons: Ext.MessageBox.YESNO,
                        fn: function(t) {
                          if (t === "yes") {
                            var record = gridIdentify
                              .getSelectionModel()
                              .getSelection()[0];
                            DukeSource.getApplication()
                              .getController(
                                "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelIdentifyRiskOperational"
                              )
                              ._onButtonGoEvaluation(record);
                          }
                        }
                      });

                      me.close();
                    } else {
                      DukeSource.global.DirtyView.messageWarning(
                        response.message
                      );
                    }
                  }
                });
              } else {
                DukeSource.global.DirtyView.messageWarning(
                  DukeSource.global.GiroMessages.MESSAGE_COMPLETE
                );
              }
            }
          },
          {
            text: "Salir",
            scale: "medium",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);
