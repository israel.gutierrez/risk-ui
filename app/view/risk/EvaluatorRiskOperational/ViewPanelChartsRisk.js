Ext.draw.engine.ImageExporter.defaultUrl = "ImageExportService";
Ext.define("ModelGridMatrix", {
  extend: "Ext.data.Model",
  fields: ["cell1", "cell2", "cell3", "cell4", "cell5"]
});

var StoreGridMatrixEvolution = Ext.create("Ext.data.Store", {
  model: "ModelGridMatrix",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

var StoreGridMatrixOriginal = Ext.create("Ext.data.Store", {
  model: "ModelGridMatrix",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelChartsRisk",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelChartsRisk",
    border: false,
    layout: "fit",
    requires: [
      "DukeSource.view.risk.parameter.combos.ViewComboOperationalRiskExposition",
      "DukeSource.view.risk.parameter.combos.ViewComboTypeMatrix",
      "DukeSource.view.dashboard.HeatMapRisk"
    ],
    removeAllChild: function(remove) {
      var me = this;
      if (remove) {
        me.down("#containerReport").removeAll();
      }
    },
    addChildGrids: function(add) {
      var me = this;
      if (add) {
        me.down("#containerReport").add(
          {
            xtype: "gridpanel",
            name: "matrixOriginal",
            itemId: "matrixOriginal",
            border: false,
            height: 400,
            width: 700,
            padding: 10,
            store: StoreGridMatrixOriginal,
            loadMask: true,
            columnLines: true,
            columns: [],
            bbar: {
              xtype: "pagingtoolbar",
              hidden: true,
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridMatrixOriginal,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            },
            listeners: {
              render: function() {
                var grid = this;
                DukeSource.global.DirtyView.loadGridDefault(grid);
              }
            }
          },
          {
            xtype: "gridpanel",
            name: "matrixEvolution",
            height: 400,
            width: 700,
            padding: 10,
            itemId: "matrixEvolution",
            title:
              "Evolutivo del riesgo residual la fecha: " +
              Ext.Date.format(new Date(), "d/m/Y"),
            border: false,
            store: StoreGridMatrixEvolution,
            loadMask: true,
            columnLines: true,
            columns: [],
            bbar: {
              xtype: "pagingtoolbar",
              hidden: true,
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridMatrixEvolution,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            },
            listeners: {
              render: function() {
                var grid = this;
                DukeSource.global.DirtyView.loadGridDefault(grid);
              }
            }
          }
        );
      }
    },

    initComponent: function() {
      var me = this;
      this.items = [
        {
          xtype: "container",
          name: "generalContainer",
          border: false,
          layout: {
            align: "stretch",
            type: "border"
          },
          items: [
            {
              xtype: "panel",
              padding: "2 2 2 0",
              flex: 3,
              border: false,
              itemId: "panelReport",
              titleAlign: "center",
              region: "center",
              layout: {
                type: "hbox",
                align: "stretch"
              },
              items: [
                {
                  xtype: "panel",
                  flex: 1,
                  collapsible: true,
                  collapseDirection: "left",
                  region: "west",
                  title: "Filtros",
                  items: [
                    {
                      xtype: "form",
                      border: false,
                      hidden: true,
                      bodyPadding: 5,
                      fieldDefaults: {
                        labelCls: "changeSizeFontToEightPt",
                        fieldCls: "changeSizeFontToEightPt"
                      },
                      items: [
                        {
                          xtype: "textfield",
                          name: "nameReport",
                          itemId: "nameReport",
                          hidden: true
                        },
                        {
                          xtype: "textfield",
                          name: "nameFile",
                          itemId: "nameFile",
                          hidden: true
                        },
                        {
                          xtype: "ViewComboOperationalRiskExposition",
                          itemId: "maxExposition",
                          name: "maxExposition",
                          fieldLabel: "Máxima exposición",
                          fieldCls: "obligatoryTextField",
                          allowBlank: true,
                          queryMode: "remote"
                        },
                        {
                          xtype: "ViewComboTypeMatrix",
                          fieldLabel: "Matriz",
                          queryMode: "remote",
                          hidden: true,
                          msgTarget: "side",
                          name: "typeMatrix",
                          itemId: "typeMatrix",
                          listeners: {
                            select: function(cbo, record) {
                              var panel = cbo.up("ViewPanelChartsRisk");
                              var gridOriginal = panel.down("#matrixOriginal");
                              var gridEvolution = panel.down(
                                "#matrixEvolution"
                              );
                              Ext.Ajax.request({
                                method: "POST",
                                url:
                                  "http://localhost:9000/giro/buildHeaderAxisX.htm",
                                params: {
                                  valueFind: panel
                                    .down("#maxExposition")
                                    .getValue(),
                                  propertyOrder: "equivalentValue",
                                  idTypeMatrix: cbo.getValue()
                                },
                                success: function(response) {
                                  response = Ext.decode(response.responseText);
                                  if (response.success) {
                                    var columns = Ext.JSON.decode(
                                      response.data
                                    );
                                    var fields = Ext.JSON.decode(
                                      response.fields
                                    );
                                    DukeSource.global.DirtyView.createMatrix(
                                      columns,
                                      fields,
                                      gridOriginal
                                    );
                                    DukeSource.global.DirtyView.createMatrix(
                                      columns,
                                      fields,
                                      gridEvolution
                                    );
                                  } else {
                                    DukeSource.global.DirtyView.messageWarning(response.message);
                                    gridOriginal.getView().refresh();
                                    gridEvolution.getView().refresh();
                                  }
                                },
                                failure: function() {}
                              });
                              gridOriginal.store.getProxy().extraParams = {
                                idOperationalRiskExposition: panel
                                  .down("#maxExposition")
                                  .getValue(),
                                idTypeMatrix: cbo.getValue()
                              };
                              gridOriginal.store.getProxy().url =
                                "http://localhost:9000/giro/findMatrixAndLoad.htm";
                              gridOriginal.down("pagingtoolbar").moveFirst();

                              gridEvolution.store.getProxy().extraParams = {
                                idOperationalRiskExposition: panel
                                  .down("#maxExposition")
                                  .getValue(),
                                idTypeMatrix: cbo.getValue()
                              };
                              gridEvolution.store.getProxy().url =
                                "http://localhost:9000/giro/findMatrixAndLoad.htm";
                              gridEvolution.down("pagingtoolbar").moveFirst();
                            }
                          }
                        },
                        {
                          xtype: "datefield",
                          fieldLabel: "F. de reporte desde",
                          blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                          format: "d/m/Y",
                          hidden: true,
                          itemId: "dateInit",
                          name: "dateInit"
                        },
                        {
                          xtype: "datefield",
                          fieldLabel: "F. de reporte hasta",
                          blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                          format: "d/m/Y",
                          hidden: true,
                          name: "dateEnd",
                          itemId: "dateEnd"
                        }
                      ],
                      buttons: [
                        {
                          text: "Limpiar",
                          scale: "medium",
                          iconCls: "clear",
                          handler: function() {
                            me.down("form")
                              .getForm()
                              .reset();
                          }
                        },
                        {
                          text: "Ejecutar",
                          itemId: "buttonExecute",
                          scale: "medium",
                          iconCls: "table_go"
                        }
                      ]
                    }
                  ]
                },
                {
                  xtype: "container",
                  itemId: "containerReport",
                  flex: 3,
                  autoScroll: true,
                  margins: "0 0 0 2",
                  style: {
                    background: "#dde8f4",
                    border: "#99bce8 solid 1px !important"
                  },
                  layout: {
                    type: "table",
                    columns: 2
                  }
                }
              ]
            },
            {
              xtype: "panel",
              title: "Reportes",
              region: "west",
              width: 250,
              height: 300,
              collapsible: true,
              layout: "accordion",
              margins: "2",
              items: [
                {
                  xtype: "treepanel",
                  itemId: "treepanel",
                  rootVisible: false,
                  root: {
                    expanded: true,
                    children: [
                      {
                        text: "Consolidado de mapas de calor",
                        leaf: true,
                        nameDownload: "all-risk",
                        nameReport: "chart-profile",
                        typeMatrix: false,
                        removeChildReport: true,
                        addChildGrid: false,
                        dateInit: false,
                        dateEnd: false,
                        buttonExecute: "executeProfileRisk"
                      },
                      {
                        text: "Mapa de riesgos - Evolución",
                        leaf: true,
                        nameDownload: "Evolución mapa de riesgos",
                        nameReport: "evolution-risk",
                        typeMatrix: true,
                        dateInit: true,
                        removeChildReport: true,
                        addChildGrid: true,
                        dateEnd: true,
                        buttonExecute: "executeEvolutionRisk"
                      }
                    ]
                  },
                  listeners: {
                    itemclick: function(grid, record, item, index, e) {
                      me.removeAllChild(record.raw.removeChildReport);
                      me.addChildGrids(record.raw.addChildGrid);

                      me.down("form")
                        .getForm()
                        .reset();
                      me.down("form").setVisible(true);
                      me.down("#nameFile").setValue(record.raw.nameDownload);
                      me.down("#nameReport").setValue(record.raw.nameReport);
                      me.down("#typeMatrix").setVisible(record.raw.typeMatrix);
                      me.down("#dateInit").setVisible(record.raw.dateInit);
                      me.down("#dateEnd").setVisible(record.raw.dateEnd);
                      me.down("#buttonExecute").action =
                        record.raw.buttonExecute;
                    }
                  }
                }
              ]
            }
          ]
        }
      ];
      this.callParent();
    }
  }
);
