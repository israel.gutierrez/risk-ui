Ext.define("ModelGridProcessRisk", {
  extend: "Ext.data.Model",
  fields: [
    "idProcess",
    "idActivity",
    "idSubProcess",
    "idProcessType",
    "processType",
    "descriptionProcessType",
    "descriptionSubProcess",
    "descriptionProcess",
    "descriptionProduct",
    "descriptionLong",
    "abbreviation",
    "description",
    "state",
    "text",
    "parent",
    "evaluator",
    "transactional",
    "input",
    "output",
    "generateProduct",
    "access",
    "importance"
  ]
});

var StoreGridProcessRisk = Ext.create("Ext.data.TreeStore", {
  model: "ModelGridProcessRisk",
  autoLoad: false,
  proxy: {
    type: "ajax",
    url:
      category === DukeSource.global.GiroConstants.GESTOR
        ? "http://localhost:9000/giro/showListProcessAssigned.htm"
        : "http://localhost:9000/giro/showListProcessTypeActives.htm",
    extraParams: {
      depth: "0",
      check: false,
      username: userName
    }
  },
  folderSort: true,
  sorters: [
    {
      property: "text",
      direction: "ASC"
    }
  ],
  root: {
    text: "",
    id: "root",
    node: "root",
    depth: "0",
    expanded: true
  }
});

function showRisk() {
  var tree = Ext.ComponentQuery.query("PanelProcessRiskEvaluation")[0];
  var node = tree.getSelectionModel().getSelection()[0];

  if (node === undefined) {
    DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
  } else {
    if (
      !node.get("access") &&
      category === DukeSource.global.GiroConstants.GESTOR
    ) {
      DukeSource.global.DirtyView.messageWarning("No tiene acceso a este nivel de proceso");
    } else {
      tree.disable();
      DukeSource.global.DirtyView.verifyLoadController(
        "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelIdentifyRiskOperational"
      );

      var k = Ext.create(
        "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelIdentifyRiskOperational",
        {
          closable: false,
          title: "Evaluación de riesgo &#8702; 1",
          callerPanel: "PanelProcessRiskEvaluation",
          nodeProcess: node,
          border: false
        }
      );

      DukeSource.getApplication().centerPanel.addPanel(k);

      var gridRisk = k.down("grid");

      gridRisk.store.getProxy().extraParams = {
        idProcessType: node.get("idProcessType"),
        idProcess: node.get("idProcess"),
        idSubProcess: node.get("idSubProcess"),
        idActivity: node.get("idActivity")
      };

      gridRisk.store.getProxy().url =
        "http://localhost:9000/giro/getRiskByProcess.htm";
      gridRisk.down("pagingtoolbar").doRefresh();
      gridRisk.setTitle(
        "" +
          gridRisk.title +
          " &#8702; " +
          '<span style="color: #de3838">' +
          node.get("descriptionProcess") +
          "</span>"
      );
    }
  }
}

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.PanelProcessRiskEvaluation",
  {
    extend: "Ext.tree.Panel",
    alias: "widget.PanelProcessRiskEvaluation",
    store: StoreGridProcessRisk,
    useArrows: true,
    multiSelect: true,
    mixins: {
      treeFilter: "DukeSource.global.TreeFilter"
    },
    singleExpand: false,
    rootVisible: false,
    statics: {
      valueNode: undefined
    },
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        columns: [
          {
            xtype: "treecolumn",
            text: "Procesos",
            width: 800,
            sortable: true,
            dataIndex: "text"
          }
        ],
        tbar: [
          {
            xtype:"UpperCaseTextField",
            labelWidth: 60,
            padding: 2,
            fieldLabel: "Buscar",
            width: 260,
            emptyText: "Buscar",
            enableKeyEvents: true,
            listeners: {
              afterrender: function(e) {
                e.focus(false, 200);
              },
              keyup: function(e, t) {
                me.expandAll(this);
                me.filterByText(e.getValue());
                if (e.getValue() === "") {
                  me.collapseAll(this);
                }
              }
            }
          },
          {
            text: "Continuar",
            iconCls: "ir",
            cls: "my-btn",
            overCls: "my-over",
            handler: function() {
              showRisk();
            }
          },
          {
            xtype: "combobox",
            padding: "0 0 0 5",
            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
            name: "typeViewRisk",
            itemId: "typeViewRisk",
            msgTarget: "side",
            fieldLabel: "Ver riesgos por",
            editable: false,
            queryMode: "local",
            displayField: "description",
            valueField: "value",
            hidden: hidden("PPRE_CB_groups"),
            store: {
              fields: ["value", "description"],
              pageSize: 9999,
              autoLoad: true,
              proxy: {
                actionMethods: {
                  create: "POST",
                  read: "POST",
                  update: "POST"
                },
                type: "ajax",
                url:
                  "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                extraParams: {
                  propertyOrder: "description",
                  propertyFind: "identified",
                  valueFind: "RISK_TYPE_VIEW"
                },
                reader: {
                  type: "json",
                  root: "data",
                  successProperty: "success"
                }
              }
            },
            listeners: {
              select: function(cbo) {
                var me = Ext.ComponentQuery.query(
                  "PanelProcessRiskEvaluation"
                )[0];
                var namePanel;

                if (cbo.getValue() === "1") {
                  me.destroy();
                  namePanel =
                    "DukeSource.view.risk.EvaluatorRiskOperational.PanelProcessRiskEvaluation";
                } else if (cbo.getValue() === "2") {
                  me.destroy();
                  namePanel =
                    "DukeSource.view.risk.EvaluatorRiskOperational.PanelProductRiskEvaluation";
                } else if (cbo.getValue() === "3") {
                  me.destroy();
                  namePanel =
                    "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelPreviousIdentifyRiskOperational";
                }

                var panel = Ext.create(namePanel, {
                  title: "EVRO",
                  closeAction: "destroy",
                  closable: true
                });
                DukeSource.getApplication().centerPanel.addPanel(panel);
              },
              afterrender: function(cbo) {
                cbo.setValue("1");
              }
            }
          }
        ],
        listeners: {
          render: function(view) {
            view.headerCt.border = 1;
          },
          itemdblclick: function() {
            showRisk();
          }
        }
      });
      me.callParent(arguments);
    }
  }
);
