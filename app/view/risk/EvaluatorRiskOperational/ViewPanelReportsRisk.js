Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelReportsRisk",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelReportsRisk",
    requires: [
      "DukeSource.view.risk.parameter.combos.ViewComboOperationalRiskExposition",
      "DukeSource.view.risk.parameter.combos.ViewComboTypeMatrix"
    ],
    border: false,
    layout: "fit",
    initComponent: function() {
      var me = this;
      this.items = [
        {
          xtype: "container",
          name: "generalContainer",
          border: false,
          layout: {
            align: "stretch",
            type: "border"
          },
          items: [
            {
              xtype: "panel",
              padding: "2 2 2 0",
              flex: 3,
              border: false,
              itemId: "panelReport",
              titleAlign: "center",
              region: "center",
              layout: {
                type: "hbox",
                align: "stretch"
              },
              items: [
                {
                  xtype: "panel",
                  itemId: "panelFilter",
                  flex: 1.5,
                  title: "Filtros",
                  items: [
                    {
                      xtype: "container",
                      hidden: true,
                      items: [
                        {
                          xtype: "textfield",
                          name: "nameReport",
                          itemId: "nameReport",
                          hidden: true
                        },
                        {
                          xtype: "textfield",
                          name: "nameFile",
                          itemId: "nameFile",
                          hidden: true
                        }
                      ]
                    },
                    {
                      xtype: "form",
                      border: false,
                      hidden: true,
                      bodyPadding: 5,
                      fieldDefaults: {
                        labelCls: "changeSizeFontToEightPt",
                        fieldCls: "changeSizeFontToEightPt"
                      },
                      items: [
                        {
                          xtype: "textfield",
                          hidden: true,
                          itemId: "product",
                          name: "product"
                        },
                        {
                          xtype: "textfield",
                          hidden: true,
                          itemId: "idProcessType",
                          name: "idProcessType"
                        },
                        {
                          xtype: "textfield",
                          hidden: true,
                          itemId: "idProcess",
                          name: "idProcess"
                        },
                        {
                          xtype: "textfield",
                          hidden: true,
                          itemId: "idSubProcess",
                          name: "idSubProcess"
                        },
                        {
                          xtype: "textfield",
                          hidden: true,
                          itemId: "idActivity",
                          name: "idActivity"
                        },
                        {
                          xtype: "textfield",
                          hidden: true,
                          itemId: "codeProcess",
                          name: "codeProcess"
                        },
                        {
                          xtype: "ViewComboOperationalRiskExposition",
                          itemId: "maxExposition",
                          name: "maxExposition",
                          fieldLabel: "Máxima exposición",
                          fieldCls: "obligatoryTextField",
                          allowBlank: true,
                          queryMode: "remote"
                        },
                        {
                          xtype: "ViewComboTypeMatrix",
                          fieldLabel: "Matriz",
                          queryMode: "remote",
                          hidden: true,
                          msgTarget: "side",
                          name: "typeMatrix",
                          itemId: "typeMatrix"
                        },
                        {
                          xtype: "displayfield",
                          fieldLabel: "Proceso",
                          itemId: "descriptionProcess",
                          name: "descriptionProcess",
                          value: "(Seleccionar)",
                          height: 22,
                          fieldCls: "style-for-url",
                          listeners: {
                            afterrender: function(view) {
                              view.getEl().on("click", function() {
                                var window = Ext.create(
                                  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeProcess",
                                  {
                                    backWindow: me
                                  }
                                );
                                window.show();
                              });
                            }
                          }
                        },
                        {
                          xtype: "displayfield",
                          anchor: "100%",
                          fieldLabel: "Producto",
                          itemId: "descriptionProduct",
                          name: "descriptionProduct",
                          padding: "2 0 2 0",
                          allowBlank: false,
                          value: "(Seleccionar)",
                          fieldCls: "style-for-url",
                          listeners: {
                            afterrender: function(view) {
                              view.getEl().on("click", function() {
                                Ext.create(
                                  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeProduct",
                                  {
                                    winParent: me
                                  }
                                ).show();
                              });
                            }
                          }
                        },
                        {
                          xtype: "datefield",
                          fieldLabel: "F. de reporte desde",
                          blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                          format: "d/m/Y",
                          hidden: true,
                          itemId: "dateInit",
                          name: "dateInit"
                        },
                        {
                          xtype: "datefield",
                          fieldLabel: "F. de reporte hasta",
                          blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                          format: "d/m/Y",
                          hidden: true,
                          name: "dateEnd",
                          itemId: "dateEnd"
                        },
                        {
                          xtype: "combobox",
                          fieldLabel: "Tipo evaluación",
                          name: "typeRiskEvaluation",
                          itemId: "typeRiskEvaluation",
                          msgTarget: "side",
                          hidden: hidden("VPRR_CBX_TypeRiskEvaluation"),
                          blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                          displayField: "description",
                          valueField: "idTypeRiskEvaluation",
                          store: {
                            fields: ["idTypeRiskEvaluation", "description"],
                            pageSize: 999,
                            proxy: {
                              actionMethods: {
                                create: "POST",
                                read: "POST",
                                update: "POST"
                              },
                              type: "ajax",
                              url:
                                "http://localhost:9000/giro/showListTypeRiskEvaluationActivesComboBox.htm",
                              extraParams: {
                                propertyOrder: "description"
                              },
                              reader: {
                                type: "json",
                                root: "data",
                                successProperty: "success"
                              }
                            }
                          }
                        }
                      ],
                      buttons: [
                        {
                          text: "Limpiar",
                          scale: "medium",
                          iconCls: "clear",
                          handler: function() {
                            me.down("form")
                              .getForm()
                              .reset();
                          }
                        },
                        {
                          text: "Generar",
                          scale: "medium",
                          iconCls: "excel",
                          action: "generateReportsRisk"
                        }
                      ]
                    }
                  ]
                },
                {
                  xtype: "container",
                  itemId: "containerReport",
                  flex: 3,
                  margins: "0 0 0 2",
                  style: {
                    background: "#dde8f4",
                    border: "#99bce8 solid 1px !important"
                  },
                  layout: "fit",
                  items: []
                }
              ]
            },
            {
              xtype: "panel",
              title: "Reportes",
              region: "west",
              width: 250,
              height: 300,
              collapsible: true,
              layout: "accordion",
              margins: "2",
              items: [
                {
                  xtype: "menu",
                  floating: false,
                  bodyStyle: "background-color:#f3f7fb !important;",
                  items: [
                    {
                      text: "Matriz de riesgos resumen",
                      leaf: true,
                      hidden: hidden("RIE_REP_IME_reportAROLight"),
                      name: "reportAROLight",
                      iconCls: "application",
                      nameDownload: "Matriz de riesgos",
                      nameReport: nameReport("RiskReportPartialARO"),
                      typeMatrix: true,
                      descriptionProcess: true,
                      descriptionProduct: true,
                      dateInit: false,
                      dateEnd: false,
                      typeRiskEvaluation: true
                    },
                    {
                      text: "Matriz de riesgos(ARO)",
                      leaf: true,
                      iconCls: "application",
                      nameDownload: "Matriz de riesgos completa",
                      hidden: hidden("RiskReportARO"),
                      nameReport: nameReport("RiskReportARO"),
                      typeMatrix: true,
                      descriptionProcess: true,
                      descriptionProduct: true,
                      dateInit: false,
                      dateEnd: false,
                      typeRiskEvaluation: true
                    },
                    {
                      text: "Ranking de riesgos por catálogo",
                      leaf: true,
                      iconCls: "application",
                      nameDownload: "Ranking_catalogo",
                      nameReport: nameReport("RankingRiskByCatalog"),
                      typeMatrix: true,
                      descriptionProcess: false,
                      descriptionProduct: false,
                      dateInit: false,
                      dateEnd: false,
                      typeRiskEvaluation: true
                    },
                    {
                      text: "Ranking de riesgos",
                      leaf: true,
                      nameDownload: "Ranking_riesgos",
                      iconCls: "application",
                      nameReport: nameReport("RankingRiskDetail"),
                      typeMatrix: true,
                      descriptionProcess: false,
                      descriptionProduct: false,
                      dateInit: false,
                      dateEnd: false,
                      typeRiskEvaluation: true
                    },
                    {
                      text: "Ranking de riesgos por proceso",
                      leaf: true,
                      iconCls: "application",
                      nameDownload: "Ranking_proceso",
                      nameReport: nameReport("RankingRiskByProcess"),
                      typeMatrix: true,
                      descriptionProcess: false,
                      descriptionProduct: false,
                      dateInit: false,
                      dateEnd: false,
                      typeRiskEvaluation: true
                    },
                    {
                      text: "Afectación patrimonial pérdidas",
                      leaf: true,
                      iconCls: "application",
                      hidden: hidden("PatrimonialAffectationEventLoss"),
                      nameDownload: "Afectacion_patrimonial_perdidas",
                      nameReport: nameReport("PatrimonialAffectationEventLoss"),
                      typeMatrix: false,
                      descriptionProcess: false,
                      descriptionProduct: false,
                      dateInit: true,
                      dateEnd: true,
                      typeRiskEvaluation: false
                    },
                    {
                      text: "Afectación patrimonial riesgos",
                      leaf: true,
                      iconCls: "application",
                      hidden: hidden("PatrimonialAffectationRisk"),
                      nameDownload: "Afectacion_patrimonial_riesgos",
                      nameReport: nameReport("PatrimonialAffectationRisk"),
                      typeMatrix: false,
                      descriptionProcess: false,
                      descriptionProduct: false,
                      dateInit: true,
                      dateEnd: true,
                      typeRiskEvaluation: false
                    }
                  ],
                  listeners: {
                    click: function(menu, item, e) {
                      me.down("form")
                        .getForm()
                        .reset();
                      me.down("form").setVisible(true);
                      me.down("#nameFile").setValue(item.nameDownload);
                      me.down("#nameReport").setValue(item.nameReport);
                      me.down("#typeMatrix").setVisible(item.typeMatrix);
                      me.down("#descriptionProcess").setVisible(
                        item.descriptionProcess
                      );
                      me.down("#descriptionProduct").setVisible(
                        item.descriptionProduct
                      );
                      me.down("#dateInit").setVisible(item.dateInit);
                      me.down("#dateEnd").setVisible(item.dateEnd);
                      me.down("#panelFilter").setTitle(
                        "Filtros - " + item.text
                      );
                    }
                  }
                }
              ]
            }
          ]
        }
      ];
      this.callParent();
    }
  }
);
