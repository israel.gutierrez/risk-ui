Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.AddMenuPreviousRisk', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.AddMenuPreviousRisk',
    width: 150,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: []
        });

        me.callParent(arguments);
    }
});