Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.AddMenuControl', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.AddMenuControl',
    width: 150,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: []
        });

        me.callParent(arguments);
    }
});