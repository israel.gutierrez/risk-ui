Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelMapGeneral', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelReportChartMatrix',
    border: false,
    layout: 'fit',
    initComponent: function () {
        var me = this;
        this.items = [
            {
                xtype: 'container',
                name: 'generalContainer',
                border: false,
                layout: {
                    align: 'stretch',
                    type: 'border'
                },
                items: [
                    {
                        xtype: 'panel',
                        padding: '2 2 2 0',
                        flex: 3,
                        itemId: 'panelReport',
                        name: 'panelReport',
                        titleAlign: 'center',
                        region: 'center',
                        layout: 'fit',
                        items: []
                    },
                    {
                        xtype: 'panel',
                        padding: '2 0 2 2',
                        region: 'west',
                        layout: 'anchor',
                        split: true,
                        collapseDirection: 'right',
                        collapsed: false,
                        collapsible: true,
                        flex: 1.2,
                        title: 'FILTRO DE BUSQUEDA',
                        items: [
                            {
                                xtype: 'container',
                                anchor: '100%',
                                layout: {
                                    type: 'anchor'
                                },
                                padding: '5',
                                items: [
                                    {
                                        xtype: 'ViewComboTypeRiskEvaluation',
                                        flex: 1,
                                        anchor: '100%',
                                        labelWidth: 120,
                                        allowBlank: false,
                                        msgTarget: 'side',
                                        multiSelect: true,
                                        fieldCls: 'obligatoryTextField',
                                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                        fieldLabel: 'TIPO EVALUACIÓN',
                                        name: 'typeRiskEvaluation'
                                    },
                                    {
                                        xtype: 'ViewComboBusinessLineOne',
                                        flex: 1,
                                        anchor: '100%',
                                        fieldLabel: 'L&Iacute;NEA NEG. 1',
                                        queryMode: 'local',
                                        labelWidth: 120,
                                        allowBlank: false,
                                        msgTarget: 'side',
                                        fieldCls: 'obligatoryTextField',
                                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                        action: 'selectBusinessLineOneRisk',
                                        name: 'businessLineOne',
                                        listeners: {
                                            render: function (cbo) {
                                                cbo.getStore().load({
                                                        callback: function () {
                                                            cbo.store.add({
                                                                idBusinessLineOne: 'T',
                                                                description: 'TODOS'
                                                            });
                                                            cbo.setValue('T');
                                                        }
                                                    }
                                                );
                                            }
                                        }
                                    },
                                    {
                                        xtype: 'container',
                                        layout: {
                                            align: 'stretch',
                                            padding: '10 10 10 65',
                                            pack: 'center',
                                            type: 'hbox'
                                        },
                                        items: [
                                            {
                                                xtype: 'button',
                                                scale: 'medium',
                                                text: 'GENERAR',
                                                iconCls: 'pdf',
                                                action: 'generateReportChartGeneralMapPdf'
                                            },
                                            {
                                                xtype: 'button',
                                                scale: 'medium',
                                                text: 'GENERAR',
                                                iconCls: 'excel',
                                                action: 'generateReportChartGeneralMapXls'
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]

            }
        ];
        this.callParent();
    }
});

