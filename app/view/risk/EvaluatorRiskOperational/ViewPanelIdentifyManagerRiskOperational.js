Ext.define("ModelIdentifyManagerRiskOperational", {
  extend: "Ext.data.Model",
  fields: [
    "idRisk",
    "idRiskEvaluationMatrix",
    "descriptionRisk", //catalog
    "descriptionProcess",
    "description", //definition
    "idCatalogRisk",
    "codeRisk",
    "descriptionFrequency",
    "descriptionImpact",
    "descriptionScaleRisk",
    "levelColour",
    "colourLost", //description risk inherint
    "valueLost", // amount risk inherint
    "percentageReductionControl",
    "scoreDescriptionControl",
    "colourControl",
    "idFrequencyReduction",
    "idImpactReduction",
    "frequencyReduction",
    "impactReduction",
    "descriptionFrequencyResidual",
    "descriptionImpactResidual",
    "descriptionScaleResidual",
    "levelColourResidual",
    "colourLostResidual",
    "valueLostResidual",
    "idFrequency",
    "equivalentFrequency",
    "idImpact",
    "equivalentImpact",
    "idFrequencyFeature",
    "idImpactFeature",
    "versionCorrelative",
    "versionState",
    "versionStateCheck",
    "evaluationFinalControl",
    "idOperationalRiskExposition",
    "maxAmount",
    "year",
    "dateEvaluation",
    "businessLineOne",
    "processType",
    "process",
    "subProcess",
    "descriptionProcess",
    "descriptionBusinessLineOne",
    "idMatrix", // idMatrix Inherent
    "idMatrixResidual",
    "idFrequencyResidual",
    "idImpactResidual",
    "nameEvaluation",
    "typeMatrix",
    "valueResidualRisk",
    "descriptionRiskAnswer",
    "idTreatmentRisk",
    "amountEventLost",
    "numberEventLost",
    "codesEventLost",
    "codesKri",
    "numberKriAssociated",
    "valueKri",
    "descriptionKri",
    "colorKri",
    "numberActionPlanAssociated",
    "avgActionPlan",
    "codesActionPLan",
    "numberIncidents",
    "codesIncidents",
    "reasonFrequency",
    "reasonImpact"
  ]
});

var StoreIdentifyManagerRiskOperational = Ext.create("Ext.data.Store", {
  model: "ModelIdentifyManagerRiskOperational",
  autoLoad: false,
  pageSize: 50,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelIdentifyManagerRiskOperational",
  {
    extend: "Ext.container.Container",
    alias: "widget.ViewPanelIdentifyManagerRiskOperational",
    layout: {
      align: "stretch",
      type: "border"
    },
    itemId: "viewPanelIdentifyManagerRiskOperational",
    initComponent: function() {
      var me = this;
      var idRiskEvaluationMatrix = this.idRiskEvaluationMatrix;
      var nameEvaluation = this.nameEvaluation;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            region: "center",
            margins: "2 0 2 2",
            itemId: "riskIdentify",
            name: "riskIdentify",
            store: StoreIdentifyManagerRiskOperational,
            loadMask: true,
            columnLines: true,
            flex: 2,
            titleAlign: "center",
            plugins: [
              Ext.create("Ext.grid.plugin.CellEditing", {
                clicksToEdit: 1
              })
            ],
            columns: [
              {
                header: "Id",
                dataIndex: "idRisk",
                align: "center",
                locked: true,
                width: 50
              },
              {
                header: "C&oacute;digo",
                locked: true,
                dataIndex: "codeRisk",
                align: "center",
                width: 140
              },
              {
                header: "Versi&oacute;n",
                locked: true,
                dataIndex: "versionCorrelative",
                align: "center",
                width: 60
              },
              {
                header: "Proceso",
                locked: false,
                dataIndex: "descriptionProcess",
                width: 320,
                tdCls: "process-columnFree",
                renderer: function(value, metaData, record) {
                  var s = value.split(" &#8702; ");
                  var path = "";
                  for (var i = 0; i < s.length; i++) {
                    var text = s[i];
                    text = s[i] + "<br>";
                    path = path + " &#8702; " + text;
                  }
                  return path;
                }
              },
              {
                header: "Riesgo",
                locked: false,
                dataIndex: "description",
                width: 350
              },
              {
                header: "Inherente",
                columns: [
                  {
                    header: "Frecuencia",
                    align: "center",
                    dataIndex: "descriptionFrequency",
                    width: 100
                  },
                  {
                    header: "Impacto",
                    align: "center",
                    dataIndex: "descriptionImpact",
                    width: 110
                  },
                  {
                    header: "Riesgo",
                    align: "center",
                    dataIndex: "descriptionScaleRisk",
                    width: 100,
                    renderer: function(value, metaData, record) {
                      if (value != " ") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("levelColour") +
                          ' !important;"';
                        return "<span>" + value + "</span>";
                      }
                    }
                  }
                ]
              },
              {
                header: "Control",
                columns: [
                  {
                    header: "Calificaci&oacute;n",
                    align: "center",
                    dataIndex: "scoreDescriptionControl",
                    width: 100,
                    renderer: function(value, metaData, record) {
                      if (value != " ") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("colourControl") +
                          ' !important;"';
                        return "<span>" + value + "</span>";
                      }
                    }
                  }
                ]
              },
              {
                header: "Residual",
                columns: [
                  {
                    header: "Frecuencia",
                    align: "center",
                    dataIndex: "descriptionFrequencyResidual",
                    width: 100
                  },
                  {
                    header: "Impacto",
                    align: "center",
                    dataIndex: "descriptionImpactResidual",
                    width: 110
                  },
                  {
                    header: "Riesgo",
                    align: "center",
                    dataIndex: "descriptionScaleResidual",
                    width: 90,
                    renderer: function(value, metaData, record) {
                      if (value != " ") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("levelColourResidual") +
                          ' !important;"';
                        return "<span>" + value + "</span>";
                      }
                    }
                  }
                ]
              },
              {
                header: "Tratamiento",
                align: "center",
                dataIndex: "descriptionRiskAnswer",
                width: 100
              },
              {
                header: "Plan de acci&oacute;n",
                columns: [
                  {
                    header: "Nro",
                    align: "center",
                    dataIndex: "numberActionPlanAssociated",
                    width: 45
                  },
                  {
                    header: "Avance",
                    align: "center",
                    dataIndex: "avgActionPlan",
                    width: 110,
                    renderer: function(value, metaData, record) {
                      return viewAvgActionPlan(value, record, metaData);
                    }
                  },
                  {
                    header: "Planes",
                    align: "center",
                    dataIndex: "codesActionPLan",
                    width: 120,
                    renderer: function(a) {
                      return "<span>" + a + "</span>";
                    }
                  }
                ]
              },

              {
                header: "Eventos",
                columns: [
                  {
                    header: "Eventos",
                    align: "center",
                    dataIndex: "codesEventLost",
                    width: 120
                  }
                ]
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreIdentifyManagerRiskOperational,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            }
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);

function viewAvgActionPlan(v) {
  var tmpValue = v / 100;
  var tmpText = v + "%";
  var progressRenderer = (function() {
    var b = new Ext.ProgressBar();
    if (tmpValue <= 0.3334) {
      b.baseCls = "x-taskBar";
    } else if (tmpValue <= 0.6667) {
      b.baseCls = "x-taskBar-medium";
    } else {
      b.baseCls = "x-taskBar-high";
    }
    return function(pValue, pText) {
      b.updateProgress(pValue, pText, true);
      return Ext.DomHelper.markup(b.getRenderTree());
    };
  })(tmpValue, tmpText);
  return progressRenderer(tmpValue, tmpText);
}
