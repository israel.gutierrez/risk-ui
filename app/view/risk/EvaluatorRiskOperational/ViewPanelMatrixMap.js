Ext.define("ModelGridMatrixByRisk", {
  extend: "Ext.data.Model",
  fields: ["cell1", "cell2", "cell3", "cell4", "cell5"]
});

var StoreGridMatrixByRisk = Ext.create("Ext.data.Store", {
  model: "ModelGridMatrixByRisk",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelMatrixMap", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelMatrixMap",
  height: 734,
  width: 1167,
  title: "",
  layout: {
    type: "vbox",
    align: "stretch",
    padding: ""
  },
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          flex: 0.4,
          border: false,
          bodyStyle:
            "border-color:#99bce8;background-image:none;background-color:#d3e1f1;background-image:-webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#dfe9f5),color-stop(100%,#d3e1f1));background-image:-webkit-linear-gradient(top,#dfe9f5,#d3e1f1);background-image:-moz-linear-gradient(top,#dfe9f5,#d3e1f1);background-image:-o-linear-gradient(top,#dfe9f5,#d3e1f1);background-image:-ms-linear-gradient(top,#dfe9f5,#d3e1f1);background-image:linear-gradient(top,#dfe9f5,#d3e1f1);",
          layout: {
            type: "hbox",
            align: "stretch"
          },
          fieldDefaults: {
            labelCls: "changeSizeFontToEightPt",
            fieldCls: "changeSizeFontToEightPt"
          },
          items: [
            {
              xtype: "container",
              flex: 0
            },
            {
              xtype: "fieldset",
              flex: 1,
              border: false,
              padding: "10 10 0 10",
              title: "CONSTRUCCION MATRIZ",
              items: [
                {
                  xtype: "ViewComboOperationalRiskExposition",
                  fieldLabel: "MAXIMA EXPOSICION",
                  fieldCls: "obligatoryTextField",
                  anchor: "100%",
                  allowBlank: false,
                  queryMode: "remote",
                  labelWidth: 130
                },
                {
                  xtype: "ViewComboTypeMatrix",
                  fieldLabel: "TIPO DE MATRIZ",
                  queryMode: "remote",
                  allowBlank: false,
                  anchor: "100%",
                  msgTarget: "side",
                  fieldCls: "obligatoryTextField",
                  name: "typeMatrix",
                  labelWidth: 130,
                  listeners: {
                    select: function(cbo, record) {
                      var panel = cbo.up("ViewPanelMatrixMap");
                      var grid2 = panel.down("grid[name=matrixGeneral]");
                      Ext.Ajax.request({
                        method: "POST",
                        url: "http://localhost:9000/giro/buildHeaderAxisX.htm",
                        params: {
                          valueFind: panel
                            .down("ViewComboOperationalRiskExposition")
                            .getValue(),
                          propertyOrder: "equivalentValue",
                          idTypeMatrix: cbo.getValue()
                        },
                        success: function(response) {
                          response = Ext.decode(response.responseText);
                          if (response.success) {
                            var columns = Ext.JSON.decode(response.data);
                            var fields = Ext.JSON.decode(response.fields);
                            DukeSource.global.DirtyView.createMatrix(columns, fields, grid2);
                          } else {
                            DukeSource.global.DirtyView.messageWarning(response.message);
                            grid2.getView().refresh();
                          }
                        },
                        failure: function() {}
                      });
                      grid2.store.getProxy().extraParams = {
                        idOperationalRiskExposition: panel
                          .down("ViewComboOperationalRiskExposition")
                          .getValue(),
                        idTypeMatrix: cbo.getValue()
                      };
                      grid2.store.getProxy().url =
                        "http://localhost:9000/giro/findMatrixAndLoad.htm";
                      grid2.down("pagingtoolbar").moveFirst();
                    }
                  }
                },
                {
                  xtype: "radiogroup",
                  width: 400,
                  anchor: "100%",
                  allowBlank: false,
                  fieldLabel: "",
                  items: [
                    {
                      xtype: "radio",
                      itemId: "checkInherent",
                      boxLabel: "RIESGO INHERENTE",
                      padding: 5,
                      name: "typeRisk",
                      boxLabelAlign: "before"
                    },
                    {
                      xtype: "radio",
                      itemId: "checkResidual",
                      boxLabel: "RIESGO RESIDUAL",
                      name: "typeRisk",
                      padding: 5,
                      boxLabelAlign: "before"
                    }
                  ]
                }
              ]
            },
            {
              xtype: "fieldset",
              flex: 1,
              border: false,
              padding: "10 10 0 10",
              title: "BUSQUEDA DE RIESGOS",
              items: [
                {
                  xtype: "combobox",
                  labelWidth: 130,
                  anchor: "100%",
                  fieldLabel: "ESTADO",
                  editable: false,
                  name: "stateRisk",
                  itemId: "stateRisk",
                  displayField: "description",
                  valueField: "id",
                  store: {
                    fields: ["id", "description", "sequence"],
                    proxy: {
                      actionMethods: {
                        create: "POST",
                        read: "POST",
                        update: "POST"
                      },
                      type: "ajax",
                      url: "http://localhost:9000/giro/findStateIncident.htm",
                      extraParams: {
                        propertyFind: "si.typeIncident",
                        valueFind: DukeSource.global.GiroConstants.RISK,
                        propertyOrder: "si.sequence"
                      },
                      reader: {
                        type: "json",
                        root: "data",
                        successProperty: "success"
                      }
                    }
                  }
                },
                {
                  xtype: "ViewComboBusinessLineOne",
                  anchor: "100%",
                  forceSelection: false,
                  fieldLabel: "LINEA DE NEGOCIO",
                  labelWidth: 130
                },
                {
                  xtype: "ViewComboProcess",
                  queryMode: "remote",
                  anchor: "100%",
                  forceSelection: false,
                  fieldLabel: "PROCESO",
                  labelWidth: 130,
                  listeners: {
                    select: function(cbo) {
                      var panel = cbo.up("ViewPanelMatrixMap");
                      var comboSubProcess = panel.down("ViewComboSubProcess");
                      comboSubProcess.getStore().load({
                        url:
                          "http://localhost:9000/giro/showListSubProcessActives.htm",
                        params: {
                          valueFind: cbo.getValue()
                        },
                        callback: function(cbo) {
                          comboSubProcess.reset();
                        }
                      });
                      comboSubProcess.setDisabled(false);
                    }
                  }
                },
                {
                  xtype: "container",
                  layout: {
                    type: "hbox",
                    align: "middle"
                  },
                  items: [
                    {
                      xtype: "ViewComboSubProcess",
                      flex: 1,
                      fieldLabel: "SUB PROCESO",
                      forceSelection: false,
                      disabled: true,
                      margin: "0 5 0 0",
                      labelWidth: 130
                    },
                    {
                      xtype: "button",
                      scale: "medium",
                      iconCls: "search",
                      action: "identifiedMapRiskInMatrix",
                      text: "BUSCAR",
                      margin: "0 5 0 0"
                    },
                    {
                      xtype: "button",
                      scale: "medium",
                      iconCls: "clear",
                      text: "LIMPIAR",
                      handler: function(btn) {
                        me.down("ViewComboProcess").reset();
                        me.down("ViewComboSubProcess").reset();
                        me.down("ViewComboBusinessLineOne").reset();
                        me.down("#checkInherent").reset();
                        me.down("#checkResidual").reset();
                        me.down("grid")
                          .getView()
                          .refresh();
                      }
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          xtype: "container",
          flex: 1,
          title: "MATRIZ DE RIESGOS",
          layout: {
            type: "hbox",
            align: "stretch"
          },
          items: [
            {
              xtype: "gridpanel",
              name: "matrixGeneral",
              store: StoreGridMatrixByRisk,
              flex: 1,
              loadMask: true,
              columnLines: true,
              columns: [],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: StoreGridMatrixByRisk,
                displayInfo: true,
                items: [
                  {
                    iconCls: "print",
                    handler: function() {
                      DukeSource.global.DirtyView.printElementTogrid(
                        Ext.ComponentQuery.query("ViewPanelMatrixMap")[0].down(
                          "grid"
                        )
                      );
                    }
                  },
                  {
                    xtype: "textfield",
                    readOnly: true,
                    labelWidth: 70,
                    width: 130,
                    fieldLabel: "R.INHERENTE",
                    fieldStyle:
                      "background-color: #FFF; background-image: none;"
                  },
                  "--",
                  {
                    xtype: "textfield",
                    readOnly: true,
                    labelWidth: 65,
                    width: 125,
                    fieldLabel: "R.RESIDUAL",
                    fieldStyle:
                      "background-color: #000; background-image: none;"
                  }
                ],
                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              },
              listeners: {
                render: function() {
                  var grid = this;
                  DukeSource.global.DirtyView.loadGridDefault(grid);
                }
              }
            }
          ]
        }
      ]
    });

    me.callParent(arguments);
  }
});
