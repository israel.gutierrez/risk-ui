Ext.define('ModelGridPanelRiskOperational', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'idRisk', type: 'int', convert: function (value) {
                return parseInt(value);
            }
        }
        , 'idRiskEvaluationMatrix'
        , 'descriptionCatalogRisk'
        , 'descriptionProcess'
        , 'description'
        , 'comments'
        , 'idCatalogRisk'
        , 'codeRisk'
        , 'descriptionFrequency'
        , 'descriptionImpact'
        , 'descriptionScaleRisk'
        , 'levelColour'
        , 'colourLost'
        , 'valueRiskInherent'
        , 'percentageReductionControl'
        , 'scoreDescriptionControl'
        , 'colourControl'
        , 'idFrequencyReduction'
        , 'idImpactReduction'
        , 'frequencyReduction'
        , 'impactReduction'
        , 'descriptionFrequencyResidual'
        , 'descriptionImpactResidual'
        , 'descriptionScaleResidual'
        , 'levelColourResidual'
        , 'colourLostResidual'
        , 'valueLostResidual'
        , 'idFrequency'
        , 'equivalentFrequency'
        , 'idImpact'
        , 'equivalentImpact'
        , 'idFrequencyFeature'
        , 'idImpactFeature'
        , 'versionCorrelative'
        , 'versionState'
        , 'versionStateCheck'
        , 'isVersionActive'
        , 'evaluationFinalControl'
        , 'idOperationalRiskExposition'
        , 'maxAmount'
        , 'year'
        , 'dateEvaluation'
        , 'businessLineOne'
        , 'idProcessType'
        , 'idProcess'
        , 'idSubProcess'
        , 'idActivity'
        , 'descriptionProcess'
        , 'descriptionBusinessLineOne'
        , 'idMatrix'
        , 'idMatrixResidual'
        , 'idFrequencyResidual'
        , 'idImpactResidual'
        , 'nameEvaluation'
        , 'typeMatrix'
        , 'valueResidualRisk'
        , 'descriptionTreatment'
        , 'idTreatmentRisk'
        , 'amountEventLost'
        , 'numberEventLost'
        , 'codesEventLost'
        , 'codesKri'
        , 'numberKriAssociated'
        , 'valueKri'
        , 'descriptionKri'
        , 'colorKri'
        , 'numberActionPlanAssociated'
        , 'avgActionPlan'
        , 'codesActionPLan'
        , 'numberIncidents'
        , 'codesIncidents'
        , 'reasonFrequency'
        , 'reasonImpact'
        , 'stateRisk'
        , 'colorState'
        , 'descriptionState'
        , 'sequenceStateRisk'
        , 'indicatorAttachment'
        , 'idTargetRisk'
        , 'descriptionTargetFrequency'
        , 'descriptionTargetImpact'
        , 'descriptionScaleTargetRisk'
        , 'valueTargetRisk'
        , 'colorTargetRisk'
        , 'idReachedRisk'
        , 'descriptionReachedRisk'
        , 'reachedFrequency'
        , 'descriptionReachedFrequency'
        , 'reachedImpact'
        , 'descriptionReachedImpact'
        , 'idMatrixReached'
        , 'descriptionScaleReachedRisk'
        , 'colorReachedRisk'
        , 'valueReachedRisk'
        , 'expectedLoss'
        , 'averageImpact'
        , 'averageFrequency'
        , 'numberControls'
        , 'controlCodes'
        , 'expectedLoss'
        , 'productFactor'
        , 'resumeFocus'
    ]
});

var StoreGridPanelRiskOperational = Ext.create('Ext.data.Store', {
    model: 'ModelGridPanelRiskOperational',
    autoLoad: false,
    pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
    proxy: {
        actionMethods: {
            create: 'POST',
            read: 'POST',
            update: 'POST'
        },
        type: 'ajax',
        url: 'http://localhost:9000/giro/loadGridDefault.htm',
        reader: {
            totalProperty: 'totalCount',
            root: 'data',
            successProperty: 'success'
        }
    }
});

Ext.define('DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelIdentifyRiskOperational', {
    extend: 'Ext.container.Container',
    alias: 'widget.ViewPanelIdentifyRiskOperational',
    requires: [
        'DukeSource.view.risk.util.search.AdvancedSearchRisk'
        , 'DukeSource.view.risk.EvaluatorRiskOperational.combos.ViewComboGridEvaluationRisk'
    ],
    layout: {
        align: 'stretch',
        type: 'border'
    },
    itemId: 'viewPanelIdentifyRiskOperational',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'AdvancedSearchRisk',
                    region: 'west',
                    padding: '2 0 2 2',
                    collapsed: true,
                    hidden: true,
                    collapseDirection: 'right',
                    buttonAlign: 'center',
                    tbar: [
                        {
                            text: 'Buscar',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            iconCls: 'search',
                            handler: function () {
                                var process = me.down('#process').getValue().length === 0 ? '' : '(' + me.down('#process').getValue() + ')';
                                var processType = me.down('#processType').getValue() === null ? '' : me.down('#processType').getValue();
                                var fields = 'r.idRisk, r.codeRisk,pt.idProcessType,p.idProcess';
                                var values = me.down('#idRisk').getValue() + ';' + me.down('#codeRisk').getValue() + ';' + processType + ';' + process;
                                var types = 'Long,String,Long,Long';
                                var operator = 'equal,equal,equal,multiple';
                                me.down('grid').store.getProxy().extraParams = {
                                    fields: fields,
                                    values: values,
                                    types: types,
                                    operators: operator,
                                    condition: me.down('#conditionCombo').getValue(),
                                    search: 'full',
                                    isLast: 'true'
                                };
                                me.down('grid').store.getProxy().url = 'http://localhost:9000/giro/findMatchRisk.htm';
                                me.down('grid').down('pagingtoolbar').moveFirst();
                            }
                        },
                        {
                            text: 'Limpiar',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            iconCls: 'clear',
                            handler: function () {
                                me.down('#idRisk').reset();
                                me.down('#processType').reset();
                                me.down('#process').reset();
                                me.down('#codeRisk').reset();
                                me.down('grid').store.getProxy().extraParams = {idRiskEvaluationMatrix: Ext.ComponentQuery.query('ViewPanelPreviousIdentifyRiskOperational')[0].down('grid').getSelectionModel().getSelection()[0].get('idRiskEvaluationMatrix')};
                                me.down('grid').store.getProxy().url = 'http://localhost:9000/giro/getRiskByRiskEvaluationMatrix.htm';
                                me.down('grid').down('pagingtoolbar').moveFirst();
                            }
                        }
                    ]
                },
                {
                    xtype: 'gridpanel',
                    region: 'center',
                    margins: '2 0 2 2',
                    tbar: [
                        {
                            xtype: 'button',
                            iconCls: 'add',
                            text: 'Nuevo',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            action: 'newRiskOperational'
                        }, '-',
                        {
                            xtype: 'button',
                            itemId: 'buttonEvaluation',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            hidden: true,
                            iconCls: 'newEval',
                            text: 'Versión',
                            action: 'startEvaluation'
                        }, '-',
                        {
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            itemId: 'buttonIr',
                            hidden: true,
                            iconCls: 'ir',
                            text: 'Ir a evaluación',
                            action: 'buttonGoEvaluation'
                        }, '-',
                        {
                            xtype: 'button',
                            iconCls: 'logout',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            text: 'Salir',
                            action: 'buttonGoPreviousEvaluation'
                        }, '-',
                        {
                            xtype: 'ViewComboGridEvaluationRisk',
                            width: 470,
                            labelWidth: 60,
                            hidden: true,
                            name: 'comboVersionRisk',
                            itemId: 'comboVersionRisk',
                            fieldLabel: 'Versión',
                            action: 'comboGridSelection'
                        }, '->',
                        {
                            xtype: 'button',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            iconCls: 'auditory',
                            text: 'Auditoría',
                            handler: function (btn) {
                                var grid = btn.up('panel').down('grid');
                                DukeSource.global.DirtyView.showAuditory(grid, 'http://localhost:9000/giro/findAuditRisk.htm');
                            }
                        },
                        {
                            xtype: 'button',
                            iconCls: 'logout',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            text: 'Salir',
                            action: 'buttonGoPreviousEvaluation'
                        }
                    ],
                    itemId: 'riskIdentify',
                    name: 'riskIdentify',
                    store: StoreGridPanelRiskOperational,
                    loadMask: true,
                    columnLines: true,
                    flex: 2,
                    title: 'EVALUACIÓN DE RIESGOS',
                    plugins: [
                        Ext.create('Ext.grid.plugin.CellEditing', {
                            clicksToEdit: 1
                        })
                    ],
                    columns: [
                        {
                            header: 'Id',
                            locked: true,
                            dataIndex: 'idRisk',
                            width: 60,
                            renderer: function (value, metaData, record) {
                                var style = '';
                                if (record.get('indicatorAttachment') === 'S') {
                                    style = 'tesla even-attachment'
                                }
                                return '<span style="width: 20px">' + value + '</span><span class="' + style + '" style="margin-left: 10px"></span>';
                            }
                        },
                        {
                            header: 'Código',
                            locked: true,
                            dataIndex: 'codeRisk',
                            width: 140,
                            renderer: function (value, metaData, record) {
                                var state = record.get('descriptionState');
                                var style = state === 'Aceptado' ? 'even-check-circle-o' : state === 'No aceptado' ? 'even-blocked-risk' : 'even-times-circle-o';
                                return '<span class="' + style + '" style="margin-left: 18px"></span><span style="width: 40px; padding-left: 5px;">' + value + '</span>';
                            }
                        },
                        {
                            header: 'Evaluación',
                            locked: false,
                            dataIndex: 'versionCorrelative',
                            align: 'center',
                            width: 70
                        },
                        {
                            header: 'Proceso',
                            locked: false,
                            dataIndex: 'descriptionProcess',
                            width: 320,
                            tdCls: 'process-columnFree',
                            renderer: function (value) {
                                var s = value.split(' &#8702; ');
                                var path = "";
                                for (var i = 0; i < s.length; i++) {
                                    var text = s[i];
                                    text = s[i] + '<br>';
                                    path = path + ' &#8702; ' + text;
                                }
                                return path;
                            }
                        },
                        {
                            header: 'Riesgo catálogo',
                            hidden: hidden('WRR_PANEL_CatalogRisk'),
                            dataIndex: 'descriptionCatalogRisk',
                            width: 270
                        },
                        {
                            header: 'Definición',
                            dataIndex: 'description',
                            width: 350,
                            hidden: hidden('RPCDescription')
                        },
                        {
                            header: 'INHERENTE',
                            columns: [
                                {
                                    header: 'Frecuencia',
                                    align: 'center',
                                    dataIndex: 'descriptionFrequency',
                                    width: 100
                                },
                                {
                                    header: 'Impacto',
                                    align: 'center',
                                    dataIndex: 'descriptionImpact',
                                    width: 100
                                },
                                {
                                    header: 'Riesgo',
                                    align: 'center',
                                    dataIndex: 'descriptionScaleRisk',
                                    width: 100,
                                    renderer: function (value, metaData, record) {
                                        if (value !== ' ') {
                                            metaData.tdAttr = 'style="background-color: #' + record.get('levelColour') + ' !important;"';
                                            return '<span style="color: #484545">' + value + '</span>';
                                        }
                                    }
                                },
                                {
                                    header: 'Pérdida',
                                    align: 'right',
                                    dataIndex: 'valueRiskInherent',
                                    xtype: 'numbercolumn',
                                    format: '0,0.00',
                                    width: 90,
                                    renderer: function (value, metaData, record) {
                                        if (value !== '') {
                                            metaData.tdAttr = 'style="background-color: #' + record.get('colourLost') + ' !important;"';
                                            return '<span style="color: #484545">' + Ext.util.Format.number(value, '0,0.00') + '</span>';
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            header: 'CONTROL',
                            columns: [
                                {
                                    xtype: 'numbercolumn',
                                    header: 'Reduc.Impacto',
                                    decimalPrecision: DECIMAL_PRECISION,
                                    align: 'center',
                                    dataIndex: 'averageImpact',
                                    hidden: hidden('VPIR_GRD_averageImpact'),
                                    width: 100
                                },
                                {
                                    xtype: 'numbercolumn',
                                    header: 'Reduc.Frecuencia',
                                    decimalPrecision: DECIMAL_PRECISION,
                                    align: 'center',
                                    dataIndex: 'averageFrequency',
                                    hidden: hidden('VPIR_GRD_averageFrequency'),
                                    width: 100
                                },
                                {
                                    xtype: 'numbercolumn',
                                    header: 'Reducción',
                                    hidden: hidden('VPIR_GRD_evaluationFinalControl'),
                                    align: 'right',
                                    dataIndex: 'evaluationFinalControl',
                                    format: '0,0.00',
                                    width: 95,
                                    renderer: function (value, metaData, record) {
                                        if ((record.get('colourControl') !== '')) {
                                            metaData.tdAttr = 'style="background-color: #' + record.get('colourControl') + ' !important;"';
                                            return '<span style="color: #484545">' + Ext.util.Format.number(value, '0,0.00') + '</span>';
                                        }
                                    }
                                },
                                {
                                    header: 'Score',
                                    align: 'center',
                                    dataIndex: 'percentageReductionControl',
                                    hidden: hidden('VPIR_GRD_percentReductionControl'),
                                    width: 90,
                                    renderer: function (value, metaData, record) {
                                        metaData.tdAttr = 'style="background-color: #' + record.get('colourControl') + ' !important;"';
                                        return '<span style="color: #484545">' + value + '</span>';
                                    }
                                },
                                {
                                    header: 'Calificación',
                                    align: 'center',
                                    dataIndex: 'scoreDescriptionControl',
                                    hidden: hidden('VPIR_GRD_scoreDescriptionControl'),
                                    width: 100,
                                    renderer: function (value, metaData, record) {
                                        metaData.tdAttr = 'style="background-color: #' + record.get('colourControl') + ' !important;"';
                                        return '<span style="color: #484545">' + value + '</span>';
                                    }
                                },
                                {
                                    header: 'Cant',
                                    align: 'center',
                                    dataIndex: 'numberControls',
                                    hidden: hidden('VPIR_GRD_numberControls'),
                                    width: 45
                                },
                                {
                                    header: 'Códigos',
                                    align: 'center',
                                    dataIndex: 'controlCodes',
                                    hidden: hidden('VPIR_GRD_controlCodes'),
                                    width: 120
                                },
                                {
                                    xtype: 'actioncolumn',
                                    align: 'center',
                                    width: 45,
                                    hidden: hidden('VPIR_GRD_goToControls'),
                                    header: 'Ir',
                                    items: [
                                        {
                                            icon: 'images/ir.png',
                                            handler: function (grid, rowIndex) {
                                                var rowRisk = grid.store.getAt(rowIndex);
                                                me.openPanelControl(rowRisk)
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        // {
                        //     header: '% REDUCCI&Oacute;N', columns: [
                        //         {
                        //             header: 'Frecuencia', align: 'center', dataIndex: 'frequencyReduction',
                        //             hidden: methodology == DukeSource.global.GiroConstants.METHODOLOGY_INITIAL,
                        //             width: 90
                        //         },
                        //         {
                        //             header: 'Impacto', align: 'center', dataIndex: 'impactReduction',
                        //             hidden: methodology == DukeSource.global.GiroConstants.METHODOLOGY_INITIAL,
                        //             width: 90
                        //         }
                        //     ]
                        // },
                        {
                            header: 'RESIDUAL',
                            columns: [
                                {
                                    header: 'Frecuencia',
                                    align: 'center',
                                    dataIndex: 'descriptionFrequencyResidual',
                                    width: 100
                                },
                                {
                                    header: 'Impacto',
                                    align: 'center',
                                    dataIndex: 'descriptionImpactResidual',
                                    width: 110
                                },
                                {
                                    header: 'Riesgo',
                                    align: 'center',
                                    dataIndex: 'descriptionScaleResidual',
                                    width: 90,
                                    renderer: function (value, metaData, record) {
                                        if (value !== ' ') {
                                            metaData.tdAttr = 'style="background-color: #' + record.get('levelColourResidual') + ' !important;"';
                                            return '<span style="color: #484545">' + value + '</span>';
                                        }
                                    }
                                },
                                {
                                    header: 'Pérdida',
                                    align: 'right',
                                    dataIndex: 'valueLostResidual',
                                    xtype: 'numbercolumn',
                                    format: '0,0.00',
                                    width: 90,
                                    renderer: function (value, metaData, record) {
                                        if (value !== '') {
                                            metaData.tdAttr = 'style="background-color: #' + record.get('colourLostResidual') + ' !important;"';
                                            return '<span style="color: #484545">' + Ext.util.Format.number(value, '0,0.00') + '</span>';
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            header: 'Tratamiento',
                            align: 'center',
                            dataIndex: 'descriptionTreatment',
                            width: 100
                        },
                        {
                            header: 'Plan de acción',
                            columns: [
                                {
                                    header: 'Cant',
                                    align: 'center',
                                    dataIndex: 'numberActionPlanAssociated',
                                    width: 45
                                },
                                {
                                    header: 'Avance',
                                    align: 'center',
                                    dataIndex: 'avgActionPlan',
                                    width: 110,
                                    renderer: function (value, metaData, record, row, col, store, gridView) {
                                        return viewAvgActionPlan(value, record, metaData)
                                    }
                                },
                                {
                                    header: 'Planes',
                                    align: 'center',
                                    dataIndex: 'codesActionPLan',
                                    width: 120,
                                    renderer: function (a) {
                                        return '<span>' + a + "</span>"
                                    }
                                }
                                // ,
                                // {
                                //     xtype: 'actioncolumn',
                                //     align: 'center',
                                //     width: 45,
                                //     header: 'Ir',
                                //     items: [
                                //         {
                                //             icon: 'images/ir.png',
                                //             handler: function (grid, rowIndex) {
                                //                 var rowRisk = grid.store.getAt(rowIndex);
                                //                 if (Ext.ComponentQuery.query('ViewPanelDocumentPending')[0] == undefined) {
                                //                     DukeSource.global.DirtyView.verifyLoadController('DukeSource.controller.fulfillment.ControllerPanelDocumentPending');
                                //                     var k = Ext.create('DukeSource.view.fulfillment.ViewPanelDocumentPending', {
                                //                         title: 'PLANES DE ACCION',
                                //                         closable: true,
                                //                         border: false
                                //                     });
                                //                     locateActionPlan(k, rowRisk);
                                //                 } else {
                                //                     var k1 = Ext.ComponentQuery.query('ViewPanelDocumentPending')[0];
                                //                     locateActionPlan(k1, rowRisk);
                                //                 }
                                //             }
                                //         }
                                //     ]
                                // }
                            ]
                        },
                        // {
                        //     header: 'Kris', columns: [
                        //         {
                        //             header: 'Cant', align: 'center', dataIndex: 'numberKriAssociated', width: 45
                        //         },
                        //         {
                        //             header: 'Valor', align: 'center', dataIndex: 'valueKri', width: 45
                        //         },
                        //         {
                        //             header: 'Estado', align: 'center', dataIndex: 'descriptionKri', width: 90,
                        //             renderer: function (value, metaData, record) {
                        //                 if (value != ' ') {
                        //                     metaData.tdAttr = 'style="background-color: #' + record.get('colorKri') + ' !important;"';
                        //                     return '<span>' + value + '</span>';
                        //                 }
                        //             }
                        //         },
                        //         {
                        //             xtype: 'actioncolumn',
                        //             header: 'Ver',
                        //             align: 'center',
                        //             width: 50,
                        //             items: [
                        //                 {
                        //                     icon: 'images/kri.png',
                        //                     handler: function (view, rowIndex, colIndex, item, e, record, row) {
                        //                         var relationKri = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowRelationRiskKri', {
                        //                             modal: true, idRisk: record.data['idRisk']
                        //                         });
                        //                         relationKri.show();
                        //                         /*DukeSource.getApplication().loadController('DukeSource.controller.risk.kri.report.ControllerPanelAreaQualificationKri');
                        //                          var winControl = Ext.create('DukeSource.view.risk.kri.report.ViewPanelAreaQualificationKri', {
                        //                          title: 'REPORTE POR ÁREA DE KRIS',
                        //                          closeAction: 'destroy',
                        //                          closable: true
                        //                          });
                        //                          DukeSource.getApplication().centerPanel.addPanel(winControl);*/
                        //                     }
                        //                 }
                        //             ]
                        //         }]
                        // },
                        {
                            header: 'Eventos', columns: [
                                {
                                    header: 'Cant',
                                    align: 'center',
                                    dataIndex: 'numberEventLost',
                                    width: 45
                                },
                                {
                                    header: 'Eventos',
                                    align: 'center',
                                    dataIndex: 'codesEventLost',
                                    width: 120
                                },
                                {
                                    header: 'Monto',
                                    align: 'center',
                                    dataIndex: 'amountEventLost',
                                    width: 100,
                                    renderer: function (value, metaData, record) {
                                        if (value !== '') {
                                            return '<span>' + Ext.util.Format.number(value, '0,0.00') + '</span>';
                                        }
                                    }
                                }
                                // ,
                                // {
                                //     xtype: 'actioncolumn',
                                //     align: 'center',
                                //     header: 'IR',
                                //     width: 45,
                                //     items: [
                                //         {
                                //             icon: 'images/ir.png',
                                //             handler: function (grid, rowIndex) {
                                //                 var rowRisk = grid.store.getAt(rowIndex);
                                //                 if (Ext.ComponentQuery.query('ViewPanelEventsRiskOperational')[0] == undefined) {
                                //                     DukeSource.global.DirtyView.verifyLoadController('DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelEventsRiskOperational');
                                //                     var k = Ext.create('DukeSource.view.risk.DatabaseEventsLost.ViewPanelEventsRiskOperational', {
                                //                         title: 'EVENTOS DE PÉRDIDA',
                                //                         closable: true,
                                //                         border: false
                                //                     });
                                //                     locateEventLost(k, rowRisk);
                                //                 } else {
                                //                     var k1 = Ext.ComponentQuery.query('ViewPanelEventsRiskOperational')[0];
                                //                     locateEventLost(k1, rowRisk);
                                //                 }
                                //             }
                                //         }
                                //     ]
                                // }
                            ]
                        }
                        // ,
                        // {
                        //     header: 'INCIDENTES', columns: [
                        //         {
                        //             header: 'Cant', align: 'center', dataIndex: 'numberIncidents', width: 45
                        //         },
                        //         {
                        //             header: 'INCIDENTES', align: 'center', dataIndex: 'codesIncidents', width: 120
                        //         }
                        //     ]
                        // }
                    ],
                    bbar: {
                        xtype: 'pagingtoolbar',
                        pageSize: 50,
                        store: StoreGridPanelRiskOperational,
                        displayInfo: true,
                        displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                        emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
                        doRefresh: function () {
                            var me = this,
                                current = me.store.currentPage;
                            var gralPanel = me.up('ViewPanelIdentifyRiskOperational');
                            if (me.fireEvent('beforechange', me, current) !== false) {
                                me.store.loadPage(current);
                                gralPanel.down('#buttonEvaluation').setVisible(false);
                                gralPanel.down('#buttonIr').setVisible(false);
                                gralPanel.down('ViewComboGridEvaluationRisk').setVisible(false);
                            }
                        }
                    }
                }

            ]
        });
        me.callParent(arguments);
    },
    openPanelControl: function (rowRisk) {
        DukeSource.global.DirtyView.verifyLoadController('DukeSource.controller.risk.parameter.controls.ControllerPanelRegisterControl');

        var panel = Ext.create('DukeSource.view.risk.parameter.controls.ViewPanelRegisterControl', {
            title: 'Controles',
            closable: true,
            border: false
        });

        DukeSource.getApplication().centerPanel.addPanel(panel);

        var gridMasterPlan = panel.down('grid');
        gridMasterPlan.store.getProxy().extraParams = {
            idRisk: rowRisk.get('idRisk'),
            idEvaluation: rowRisk.get('versionCorrelative')
        };
        gridMasterPlan.store.getProxy().url = 'http://localhost:9000/giro/findControlByEvaluationRisk.htm';
        gridMasterPlan.down('pagingtoolbar').moveFirst();
    }

});

function viewAvgActionPlan(v, m, r) {
    var tmpValue = v / 100;
    var tmpText = v + "%";
    var progressRenderer = (function (pValue, pText) {
        var b = new Ext.ProgressBar();
        if (tmpValue <= 0.3334) {
            b.baseCls = 'x-taskBar';
        } else if (tmpValue <= 0.6667) {
            b.baseCls = 'x-taskBar-medium';
        } else {
            b.baseCls = 'x-taskBar-high';
        }
        return function (pValue, pText) {
            b.updateProgress(pValue, pText, true);
            return Ext.DomHelper.markup(b.getRenderTree());
        };
    })(tmpValue, tmpText);
    return progressRenderer(tmpValue, tmpText);
}

function locateActionPlan(k, rowActionPlan) {
    DukeSource.getApplication().centerPanel.addPanel(k);
    k.down('combobox').setValue(rowActionPlan.get('nameStateDocument'));
    var fields = 'r.idRisk,ver.id.correlative,ue.category';
    var values = rowActionPlan.get('idRisk') + ',' + rowActionPlan.get('versionCorrelative') + ',' + category;
    var types = 'Long,Long,String';
    var operator = 'equal,equal,equal';
    var gridMasterPlan = k.down('grid');
    gridMasterPlan.store.getProxy().extraParams = {
        fields: fields,
        values: values,
        types: types,
        operator: operator,
        searchIn: 'ActionPlanRisk',
        stateDocument: DukeSource.global.GiroConstants.PROCESS
    };
    gridMasterPlan.store.getProxy().url = 'http://localhost:9000/giro/findDetailDocument.htm';
    gridMasterPlan.down('pagingtoolbar').moveFirst();
}

function locateEventLost(k, rowRisk) {
    DukeSource.getApplication().centerPanel.addPanel(k);
    var fields = 'ri.idRisk';
    var values = rowRisk.get('idRisk');
    var types = 'Long';
    var operator = 'equal';
    var gridMasterPlan = k.down('grid');
    gridMasterPlan.store.getProxy().extraParams = {
        fields: fields,
        values: values,
        types: types,
        operators: operator,
        searchIn: 'EventRisk'
    };
    gridMasterPlan.store.getProxy().url = 'http://localhost:9000/giro/advancedSearchEvent.htm';
    gridMasterPlan.down('pagingtoolbar').moveFirst();
}
