Ext.define("ModelGridProductRisk", {
  extend: "Ext.data.Model",
  fields: [
    "idProcess",
    "idActivity",
    "idSubProcess",
    "idProcessType",
    "processType",
    "descriptionProcessType",
    "descriptionSubProcess",
    "descriptionProcess",
    "descriptionLong",
    "abbreviation",
    "description",
    "idProduct",
    "descriptionProduct",
    "state",
    "path",
    "text",
    "parent",
    "evaluator",
    "transactional",
    "input",
    "output",
    "generateProduct",
    "importance"
  ]
});

var StoreGridProductRisk = Ext.create("Ext.data.TreeStore", {
  model: "ModelGridProductRisk",
  autoLoad: false,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListProductActives.htm",
    extraParams: {
      depth: "0"
    }
  },
  folderSort: true,
  sorters: [
    {
      property: "text",
      direction: "ASC"
    }
  ],
  root: {
    text: "",
    id: "root",
    node: "root",
    depth: "0"
  }
});

function showRisk() {
  var tree = Ext.ComponentQuery.query("PanelProductRiskEvaluation")[0];
  var node = tree.getSelectionModel().getSelection()[0];

  if (node === undefined) {
    DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
  } else {
    tree.disable();
    DukeSource.global.DirtyView.verifyLoadController(
      "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelIdentifyRiskOperational"
    );

    var k = Ext.create(
      "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelIdentifyRiskOperational",
      {
        closable: false,
        title: "Evaluación de riesgo &#8702; 1",
        callerPanel: "PanelProductRiskEvaluation",
        nodeProcess: node,
        border: false
      }
    );

    DukeSource.getApplication().centerPanel.addPanel(k);

    var gridRisk = k.down("grid");
    gridRisk.store.getProxy().extraParams = {
      idProduct: node.get("idProduct")
    };
    gridRisk.store.getProxy().url =
      "http://localhost:9000/giro/getRiskByProduct.htm";
    gridRisk.down("pagingtoolbar").doRefresh();
    gridRisk.setTitle(
      "" +
        gridRisk.title +
        " &#8702; " +
        '<span style="color: #de3838">' +
        node.get("path") +
        "</span>"
    );
  }
}

Ext.define(
  "DukeSource.view.risk.EvaluatorRiskOperational.PanelProductRiskEvaluation",
  {
    extend: "Ext.tree.Panel",
    alias: "widget.PanelProductRiskEvaluation",
    store: StoreGridProductRisk,
    useArrows: true,
    multiSelect: true,
    mixins: {
      treeFilter: "TreeFilter"
    },
    singleExpand: false,
    expanded: false,
    rootVisible: false,
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        columns: [
          {
            xtype: "treecolumn",
            text: "Productos",
            width: 800,
            sortable: true,
            dataIndex: "text"
          }
        ],
        tbar: [
          {
            xtype:"UpperCaseTextField",
            labelWidth: 60,
            padding: 2,
            fieldLabel: "Buscar",
            width: 260,
            emptyText: "Buscar",
            enableKeyEvents: true,
            listeners: {
              afterrender: function(e) {
                e.focus(false, 200);
              },
              keyup: function(e, t) {
                me.expandAll(this);
                me.filterByText(e.getValue());
                if (e.getValue() === "") {
                  me.collapseAll(this);
                }
              }
            }
          },
          {
            text: "Continuar",
            iconCls: "ir",
            cls: "my-btn",
            overCls: "my-over",
            handler: function() {
              showRisk();
            }
          },
          {
            xtype: "combobox",
            anchor: "100%",
            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
            name: "typeViewRisk",
            itemId: "typeViewRisk",
            msgTarget: "side",
            fieldLabel: "Ver riesgos por",
            editable: false,
            hidden: hidden("PPRE_CB_groups"),
            queryMode: "local",
            displayField: "description",
            valueField: "value",
            store: {
              fields: ["value", "description"],
              pageSize: 9999,
              autoLoad: true,
              proxy: {
                actionMethods: {
                  create: "POST",
                  read: "POST",
                  update: "POST"
                },
                type: "ajax",
                url:
                  "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                extraParams: {
                  propertyOrder: "description",
                  propertyFind: "identified",
                  valueFind: "RISK_TYPE_VIEW"
                },
                reader: {
                  type: "json",
                  root: "data",
                  successProperty: "success"
                }
              }
            },
            listeners: {
              select: function(cbo) {
                var me = Ext.ComponentQuery.query(
                  "PanelProductRiskEvaluation"
                )[0];

                var namePanel;

                if (cbo.getValue() === "1") {
                  me.destroy();
                  namePanel =
                    "DukeSource.view.risk.EvaluatorRiskOperational.PanelProcessRiskEvaluation";
                } else if (cbo.getValue() === "2") {
                  me.destroy();
                  namePanel =
                    "DukeSource.view.risk.EvaluatorRiskOperational.PanelProductRiskEvaluation";
                } else if (cbo.getValue() === "3") {
                  me.destroy();
                  namePanel =
                    "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelPreviousIdentifyRiskOperational";
                }

                var panel = Ext.create(namePanel, {
                  title: "EVRO",
                  closeAction: "destroy",
                  closable: true
                });
                DukeSource.getApplication().centerPanel.addPanel(panel);
              },
              afterrender: function(cbo) {
                cbo.setValue("2");
              }
            }
          }
        ],
        listeners: {
          beforerender: function(view) {
            view.headerCt.border = 1;
          },
          itemdblclick: function() {
            showRisk();
          },
          afterrender: function(tree) {
            tree.store.load({
              callback: function() {
                tree.expandPath("/root/1");
              }
            });
          }
        }
      });
      me.callParent(arguments);
    }
  }
);
