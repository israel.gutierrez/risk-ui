Ext.define("ModelGridBalanceAscertainment", {
  extend: "Ext.data.Model",
  fields: [
    "ledgerAccount",
    "actualValue",
    "description",
    "openingBalance",
    "debit",
    "credit"
  ]
});

var StoreGridBalanceAscertainment = Ext.create("Ext.data.Store", {
  model: "ModelGridBalanceAscertainment",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("ModelGridBalancedLoaded", {
  extend: "Ext.data.Model",
  fields: [
    "codeFormat",
    "idFormat",
    "institution",
    "length",
    "month",
    "descriptionMonth",
    "description",
    "year"
  ]
});
var StoreGridBalancedLoaded = Ext.create("Ext.data.Store", {
  model: "ModelGridBalancedLoaded",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.PatrimonyCash.ViewPanelLoadBalanceAscertainment",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelLoadBalanceAscertainment",
    //    height: 451,
    //    width: 759,
    layout: "border",

    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            region: "center",
            margins: "2 2 2 0",
            name: "riskIdentify",
            itemId: "gridBalanceAscertainment",
            title: "CARGAR BALANCE",
            store: StoreGridBalanceAscertainment,
            loadMask: true,
            columnLines: true,
            tbar: [
              {
                iconCls: "save",
                text: "CARGAR BALANCE",
                handler: function() {
                  //
                  Ext.Ajax.request({
                    method: "POST",
                    url: "http://localhost:9000/giro/findFormatGeneralLast.htm",
                    success: function(response) {
                      response = Ext.decode(response.responseText);
                      if (response.success) {
                        var win = Ext.create(
                          "DukeSource.view.risk.PatrimonyCash.windows.ViewWindowLoadBalanceAscertainment",
                          {
                            modal: true,
                            buttons: [
                              {
                                text: "GUARDAR",
                                action: "loadBalanceAscertainment",
                                iconCls: "save"
                              },
                              {
                                text: "SALIR",
                                handler: function() {
                                  win.close();
                                },
                                iconCls: "logout"
                              }
                            ]
                          }
                        ).show();
                        win
                          .down("TextFieldNumberFormatObligatory")
                          .setValue(response.data["year"]);
                        win
                          .down("ViewComboMonth")
                          .setValue(response.data["month"]);
                      } else {
                        DukeSource.global.DirtyView.messageAlert(
                          DukeSource.global.GiroMessages.TITLE_WARNING,
                          response.mensaje,
                          Ext.Msg.ERROR
                        );
                      }
                    },
                    failure: function() {}
                  });
                }
              },
              {
                iconCls: "search",
                text: "CONSULTAR BALANCE",
                handler: function() {
                  var win = Ext.create(
                    "DukeSource.view.risk.PatrimonyCash.windows.ViewWindowLoadBalanceAscertainment",
                    {
                      modal: true,
                      buttons: [
                        {
                          text: "Consultar",
                          action: "selectBalanceAscertainment",
                          iconCls: "save"
                        },
                        {
                          text: "Salir",
                          handler: function() {
                            win.close();
                          },
                          iconCls: "logout"
                        }
                      ]
                    }
                  );
                  win.down("fileuploadfield").setDisabled(true);
                  win.down("fileuploadfield").setVisible(false);
                  win.show();
                }
              }
            ],
            columns: [
              { xtype: "rownumberer", width: 40, sortable: false },
              { header: "Nro CUENTA", dataIndex: "ledgerAccount", width: 100 },
              { header: "DESCRIPCION", dataIndex: "description", flex: 1 },
              {
                header: "SALTO INICIAL",
                align: "right",
                dataIndex: "openingBalance",
                xtype: "numbercolumn",
                format: "0,0.00",
                width: 100
              },
              {
                header: "DEBE",
                align: "right",
                dataIndex: "debit",
                xtype: "numbercolumn",
                format: "0,0.00",
                width: 100
              },
              {
                header: "HABER",
                align: "right",
                dataIndex: "credit",
                xtype: "numbercolumn",
                format: "0,0.00",
                width: 100
              },
              {
                header: "SALTO ACTUAL",
                align: "right",
                dataIndex: "actualValue",
                xtype: "numbercolumn",
                format: "0,0.00",
                width: 100
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridBalanceAscertainment,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              items: [
                "-",
                {
                  xtype:"UpperCaseTrigger",
                  fieldLabel: "FILTRAR",
                  action: "searchTriggerGridFulFillment",
                  labelWidth: 60,
                  width: 300
                }
              ],
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            }
          },
          {
            xtype: "gridpanel",
            region: "west",
            width: 400,
            split: true,
            itemId: "gridBalanceLoad",
            margins: "2 0 2 2",
            name: "riskIdentify",
            title: "BALANCES CARGADOS",
            store: StoreGridBalancedLoaded,
            loadMask: true,
            columnLines: true,
            tbar: [
              {
                xtype: "TextFieldNumberFormatObligatory",
                width: 100,
                labelWidth: 40,
                itemId: "yearBalance",
                allowBlank: false,
                msgTarget: "side",
                fieldCls: "obligatoryTextField",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                fieldLabel: "A&Ntilde;O"
              },
              "-",
              {
                iconCls: "search",
                text: "BUSCAR",
                action: "searchBalanceAscertainment"
              },
              "-",
              {
                iconCls: "delete",
                text: "ELIMINAR",
                action: "deleteBalanceAscertainment"
              }
            ],
            columns: [
              { xtype: "rownumberer", width: 25, sortable: false },
              {
                header: "A&Ntilde;O",
                align: "center",
                dataIndex: "year",
                width: 50
              },
              { header: "MES", dataIndex: "descriptionMonth", width: 80 },
              { header: "ARCHIVO", dataIndex: "description", flex: 1 }
              //                        ,{header: 'RESPONSABLE', align:'right', dataIndex: 'debit',xtype: 'numbercolumn', format:'0,0.00', flex:1}
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridBalancedLoaded,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            },
            listeners: {
              render: function() {
                var me = this;
                me.store.getProxy().extraParams = {
                  propertyFind: "year",
                  valueFind: "",
                  propertyOrder: "year"
                };
                me.store.getProxy().url =
                  "http://localhost:9000/giro/findFormatGeneral.htm";
                me.down("pagingtoolbar").moveFirst();
              }
            }
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
