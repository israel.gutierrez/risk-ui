Ext.define(
  "DukeSource.view.risk.PatrimonyCash.ViewPanelGenerationReportRegulatory",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelGenerationReportRegulatory",
    border: false,
    layout: "border",

    initComponent: function() {
      var me = this;
      this.items = [
        {
          xtype: "form",
          anchor: "100%",
          region: "north",
          margin: "2 2 0 2",
          bodyPadding: "5",
          layout: {
            type: "hbox"
          },
          items: [
            {
              xtype: "ViewComboTitleReport",
              fieldLabel: "REPORTE",
              flex: 1,
              allowBlank: false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              name: "idTitleReport"
            },
            {
              xtype: "ViewComboYear",
              fieldLabel: "A&Ntilde;O",
              labelWidth: 60,
              padding: "0 20 0 40",
              flex: 1,
              allowBlank: false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              name: "yearUntil"
            },
            {
              xtype: "ViewComboMonth",
              fieldLabel: "MES",
              labelWidth: 60,
              padding: "0 20 0 40",
              flex: 1,
              allowBlank: false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              name: "monthUntil"
            },
            {
              xtype: "button",
              flex: 1,
              action: "generateReportRegulatory",
              text: "GENERAR"
            },
            {
              xtype: "button",
              flex: 1,
              iconCls: "excel",
              action: "generateExcelReportRegulatory",
              text: "EXCEL"
            },
            {
              xtype: "button",
              flex: 1,
              iconCls: "txt",
              action: "generateTxtReportRegulatory",
              text: "SUCAVE"
            }
          ]
        },
        {
          xtype: "container",
          name: "containerGenerateReport",
          region: "center",
          layout: "fit",
          items: []
        },
        {
          xtype: "form",
          padding: "0 2 2 2",
          region: "south",
          height: 75,
          layout: {
            align: "stretch",
            type: "hbox"
          },
          bodyPadding: 10,
          title: "",
          items: [
            {
              xtype: "container",
              width: 500,
              layout: {
                type: "anchor"
              },
              items: [
                {
                  xtype: "displayfield",
                  labelWidth: 500,
                  anchor: "100%",
                  fieldLabel:
                    "Requerimiento de patrimonio efectivo por riesgo operacional"
                },
                {
                  xtype: "displayfield",
                  labelWidth: 500,
                  anchor: "100%",
                  fieldLabel: "APR riesgo operacional"
                }
              ]
            },
            {
              xtype: "container",
              flex: 1
            },
            {
              xtype: "container",
              width: 150,
              layout: {
                type: "anchor"
              },
              items: [
                {
                  xtype: "UpperCaseTextFieldReadOnly",
                  anchor: "100%",
                  name: "equity"
                },
                {
                  xtype: "UpperCaseTextFieldReadOnly",
                  anchor: "100%",
                  name: "weightedAssets"
                }
              ]
            }
          ]
        }
      ];
      this.callParent();
    }
  }
);
