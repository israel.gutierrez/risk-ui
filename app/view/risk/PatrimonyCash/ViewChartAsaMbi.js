Ext.define("ModelChartAsaMbi", {
  extend: "Ext.data.Model",
  fields: ["asa", "mbi", "period"]
});

var store = Ext.create("Ext.data.Store", {
  model: "ModelChartAsaMbi",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.PatrimonyCash.ViewChartAsaMbi", {
  extend: "Ext.chart.Chart",
  alias: "widget.ViewChartAsaMbi",
  //    height: 250,
  //    width: 400,
  animate: true,
  insetPadding: 20,
  store: store,
  legend: {
    position: "right"
  },
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      axes: [
        {
          type: "Numeric",
          label: {
            renderer: Ext.util.Format.numberRenderer("0,0.00")
          },
          //                    format:'0,00',
          fields: ["asa", "mbi"],
          position: "left",
          title: "MONTO SOLES(S/.)"
        },
        {
          type: "Category",
          fields: ["period"],
          position: "bottom",
          label: {
            rotate: {
              degrees: 270
            }
          },
          title: "PERIODO"
        }
      ],
      series: [
        //
        {
          type: "line",
          axis: "left",
          title: "ASA",
          xField: "period",
          yField: ["asa"],
          tips: {
            trackMouse: true,
            width: 150,
            height: 38,
            renderer: function(storeItem, item) {
              this.setTitle(
                Ext.util.Format.number(storeItem.get("asa"), "0,0.00") +
                  "<br />" +
                  storeItem.get("period")
              );
            }
          },
          smooth: 3
        },
        {
          type: "line",
          axis: "left",
          title: "MIB",
          xField: "period",
          yField: ["mbi"],
          tips: {
            trackMouse: true,
            width: 150,
            height: 38,
            renderer: function(storeItem, item) {
              this.setTitle(
                Ext.util.Format.number(storeItem.get("mbi"), "0,0.00") +
                  "<br />" +
                  storeItem.get("period")
              );
            }
          },
          smooth: 3
        }
      ]
    });

    me.callParent(arguments);
  }
});
