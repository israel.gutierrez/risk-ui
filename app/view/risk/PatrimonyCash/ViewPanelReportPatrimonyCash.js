Ext.define("DukeSource.view.risk.PatrimonyCash.ViewPanelReportPatrimonyCash", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelReportPatrimonyCash",
  border: false,
  layout: "fit",
  initComponent: function() {
    var me = this;
    this.items = [
      {
        xtype: "container",
        name: "generalContainer",
        border: false,
        layout: {
          align: "stretch",
          type: "border"
        },
        items: [
          {
            xtype: "panel",
            padding: "2 2 2 0",
            flex: 3,
            name: "panelReport",
            region: "center",
            layout: "fit"
          },
          {
            xtype: "panel",
            padding: "2 0 2 2",
            region: "west",
            collapseDirection: "right",
            collapsed: false,
            collapsible: true,
            split: true,
            flex: 1.2,
            layout: "anchor",
            title: "REPORTE EVENTOS DE PÉRDIDA PARA IGROP",
            items: [
              {
                xtype: "container",
                anchor: "100%",
                layout: {
                  type: "anchor"
                },
                padding: "5",
                items: [
                  {
                    xtype: "datefield",
                    fieldLabel: "FECHA DESDE",
                    allowBlank: false,
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    labelWidth: 130,
                    format: "Y-m-d",
                    anchor: "100%",
                    itemId: "dateInit",
                    name: "dateInit"
                  },
                  {
                    xtype: "datefield",
                    fieldLabel: "FECHA HASTA",
                    allowBlank: false,
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    labelWidth: 130,
                    format: "Y-m-d",
                    anchor: "100%",
                    itemId: "dateEnd",
                    name: "dateEnd"
                  },
                  {
                    xtype: "container",
                    layout: {
                      align: "stretch",
                      pack: "end",
                      padding: "10 40 10 10",
                      type: "hbox"
                    },
                    items: [
                      {
                        xtype: "button",
                        scale: "medium",
                        text: "GENERAR",
                        iconCls: "pdf",
                        action: "generateReportPatrimonyCashPdf"
                      },
                      {
                        xtype: "button",
                        scale: "medium",
                        text: "GENERAR",
                        iconCls: "excel",
                        action: "generateReportPatrimonyCashXls"
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ];
    this.callParent();
  }
});
