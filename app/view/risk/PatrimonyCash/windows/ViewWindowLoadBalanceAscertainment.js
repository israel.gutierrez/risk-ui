Ext.define(
  "DukeSource.view.risk.PatrimonyCash.windows.ViewWindowLoadBalanceAscertainment",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowLoadBalanceAscertainment",
    width: 479,
    border: false,
    layout: {
      type: "fit"
    },
    title: "DETALLES BALANCE DE COMPROBACION",
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            padding: "2 2 2 2",
            layout: "anchor",
            bodyPadding: 10,
            items: [
              {
                xtype: "TextFieldNumberFormatObligatory",
                anchor: "100%",
                allowBlank: false,
                msgTarget: "side",
                fieldCls: "obligatoryTextField",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                fieldLabel: "A&Ntilde;O",
                name: "year"
              },
              {
                xtype: "ViewComboMonth",
                anchor: "100%",
                allowBlank: false,
                msgTarget: "side",
                fieldCls: "obligatoryTextField",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                fieldLabel: "MES",
                name: "month"
              },
              {
                xtype: "fileuploadfield",
                anchor: "100%",
                fieldLabel: "ARCHIVO"
              }
            ]
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
