Ext.define(
  "DukeSource.view.risk.PatrimonyCash.windows.ViewWindowAttachReport",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowAttachReport",
    height: 100,
    width: 479,
    border: false,
    layout: {
      type: "fit"
    },
    title: "ADJUNTAR REPORTE",
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            items: [
              {
                xtype: "textfield",
                itemId: "id",
                value: "id",
                name: "id",
                hidden: true
              },
              {
                xtype: "filefield",
                anchor: "100%",
                allowBlank: false,
                msgTarget: "side",
                fieldCls: "obligatoryTextField",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                itemId: "text",
                name: "text",
                fieldLabel: "DESCRIPCION"
              }
            ]
          }
        ],
        buttons: [
          {
            text: "GUARDAR",
            iconCls: "save",
            action: "attachReport"
          },
          {
            text: "SALIR",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
