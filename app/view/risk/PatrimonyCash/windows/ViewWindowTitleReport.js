Ext.define('DukeSource.view.risk.PatrimonyCash.windows.ViewWindowTitleReport', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowTitleReport',
    height: 100,
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'TITULO REPORTE',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            itemId: 'text',
                            name: 'text',
                            fieldLabel: 'DESCRIPCION'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    iconCls: 'save',
                    action: 'saveTitleBalanceAscertainment'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });

        me.callParent(arguments);
    }

});
