Ext.define('DukeSource.view.risk.PatrimonyCash.windows.ViewWindowFormCalculation', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowFormCalculation',
    height: 376,
    width: 642,
    layout: {
        type: 'fit'
    },
    title: 'My Window',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'gridpanel',
                            flex: 1,
                            title: '',
                            columns: [
                                {
                                    xtype: 'gridcolumn',
                                    dataIndex: 'string',
                                    text: 'String'
                                },
                                {
                                    xtype: 'numbercolumn',
                                    dataIndex: 'number',
                                    text: 'Number'
                                },
                                {
                                    xtype: 'datecolumn',
                                    dataIndex: 'date',
                                    text: 'Date'
                                },
                                {
                                    xtype: 'booleancolumn',
                                    dataIndex: 'bool',
                                    text: 'Boolean'
                                }
                            ],
                            viewConfig: {}
                        },
                        {
                            xtype: 'form',
                            flex: 1,
                            width: 294,
                            layout: {
                                align: 'stretch',
                                type: 'vbox'
                            },
                            bodyPadding: 10,
                            title: '',
                            items: [
                                {
                                    xtype: 'container',
                                    flex: 1
                                },
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    width: 113,
                                    layout: {
                                        align: 'middle',
                                        pack: 'center',
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'button',
                                            text: '+'
                                        },
                                        {
                                            xtype: 'button',
                                            text: '-'
                                        },
                                        {
                                            xtype: 'button',
                                            text: 'x'
                                        },
                                        {
                                            xtype: 'button',
                                            text: '/'
                                        },
                                        {
                                            xtype: 'button',
                                            text: '('
                                        },
                                        {
                                            xtype: 'button',
                                            text: ')',
                                            handler: function () {
                                                var val = me.down('textareafield').getValue() + 'VALOR'
                                                me.down('textareafield').setValue(val);
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: {
                                        align: 'middle',
                                        pack: 'center',
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'textareafield',
                                            maskRe: /[*+/()-]/,
                                            flex: 1,
                                            fieldLabel: ''
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    flex: 1
                                }
                            ],
                            bbar: [
                                {
                                    text: 'GUARDAR'
                                }
                            ]
//                            ,
//                            dockedItems: [
//                                {
//                                    xtype: 'toolbar',
//                                    flex: 1,
//                                    dock: 'bottom'
//                                }
//                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});