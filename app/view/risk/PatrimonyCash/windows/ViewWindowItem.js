Ext.define('DukeSource.view.risk.PatrimonyCash.windows.ViewWindowItem', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowItem',
    height: 205,
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'ITEM REPORTE',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            labelWidth: 120,
                            anchor: '100%',
                            itemId: 'fieldOne',
                            name: 'fieldOne',
                            fieldLabel: 'OPERADOR INICIAL'
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            labelWidth: 120,
                            anchor: '100%',
                            itemId: 'fieldFour',
                            name: 'fieldFour',
                            fieldLabel: 'TIPO'
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            labelWidth: 120,
                            anchor: '100%',
                            itemId: 'fieldTwo',
                            name: 'fieldTwo',
                            fieldLabel: 'VALOR'
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            labelWidth: 120,
                            anchor: '100%',
                            itemId: 'fieldThree',
                            name: 'fieldThree',
                            fieldLabel: 'OPERADOR FINAL'
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            labelWidth: 120,
                            anchor: '100%',
                            itemId: 'text',
                            name: 'text',
                            fieldLabel: 'FORMULA'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    action: 'saveTitleBalanceAscertainment',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });

        me.callParent(arguments);
    }

});
