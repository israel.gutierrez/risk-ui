Ext.define('DukeSource.view.risk.PatrimonyCash.windows.ViewWindowSubGroup', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowSubGroup',
    height: 190,
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'SUBGRUPO REPORTE',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            labelWidth: 120,
                            itemId: 'text',
                            name: 'text',
                            fieldLabel: 'DESCRIPCION'
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            labelWidth: 120,
                            itemId: 'formula',
                            name: 'formula',
                            fieldLabel: 'FORMULA GENERAL'
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            labelWidth: 120,
                            itemId: 'level',
                            name: 'level',
                            fieldLabel: 'NIVEL'
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            labelWidth: 120,
                            itemId: 'sign',
                            name: 'sign',
                            fieldLabel: 'SIGNO'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    action: 'saveTitleBalanceAscertainment',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });

        me.callParent(arguments);
    }

});
