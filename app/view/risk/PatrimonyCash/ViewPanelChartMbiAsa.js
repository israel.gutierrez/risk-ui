Ext.define("ModelGridPanelChartMbiAsa", {
  extend: "Ext.data.Model",
  fields: ["asa", "mbi", "difference", "period"]
});

var StoreGridPanelChartMbiAsa = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelChartMbiAsa",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.PatrimonyCash.ViewPanelChartMbiAsa", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelChartMbiAsa",
  border: false,
  layout: "border",

  initComponent: function() {
    var me = this;
    this.items = [
      {
        xtype: "form",
        anchor: "100%",
        region: "north",
        margin: "2 2 0 2",
        bodyPadding: "5",
        layout: {
          type: "hbox"
        },
        items: [
          {
            xtype: "combobox",
            fieldLabel: "REPORTE",
            labelWidth: 70,
            flex: 1,
            displayField: "name",
            editable: false,
            value: "1",
            queryMode: "local",
            valueField: "id",
            store: {
              fields: ["id", "name"],

              data: [
                { id: "1", name: "MENSUAL" },
                { id: "2", name: "ANUAL" }
              ]
            },
            listeners: {
              select: function(cbo) {
                var panel = cbo.up("ViewPanelChartMbiAsa");
                if (
                  panel.down("combobox[name=idTitleReport]").getValue() == "2"
                ) {
                  panel.down("ViewComboYear[name=yearStart]").setVisible(false);
                  panel.down("ViewComboYear[name=yearStart]").setDisabled(true);
                  panel
                    .down("ViewComboMonth[name=monthStart]")
                    .setVisible(false);
                  panel
                    .down("ViewComboMonth[name=monthStart]")
                    .setDisabled(true);
                } else {
                  panel.down("ViewComboYear[name=yearStart]").setVisible(true);
                  panel
                    .down("ViewComboYear[name=yearStart]")
                    .setDisabled(false);
                  panel
                    .down("ViewComboMonth[name=monthStart]")
                    .setVisible(true);
                  panel
                    .down("ViewComboMonth[name=monthStart]")
                    .setDisabled(false);
                }
              }
            },
            allowBlank: false,
            msgTarget: "side",
            fieldCls: "obligatoryTextField",
            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
            name: "idTitleReport"
          },
          {
            xtype: "ViewComboYear",
            fieldLabel: "A&Ntilde;O INICIO",
            padding: "0 20 0 20",
            flex: 1,
            labelWidth: 75,
            allowBlank: false,
            msgTarget: "side",
            fieldCls: "obligatoryTextField",
            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
            name: "yearStart"
          },
          {
            xtype: "ViewComboMonth",
            fieldLabel: "MES INICIO",
            padding: "0 20 0 0",
            flex: 1,
            labelWidth: 75,
            allowBlank: false,
            msgTarget: "side",
            fieldCls: "obligatoryTextField",
            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
            name: "monthStart"
          },
          {
            xtype: "ViewComboYear",
            fieldLabel: "A&Ntilde;O FIN",
            padding: "0 20 0 20",
            labelWidth: 75,
            flex: 1,
            allowBlank: false,
            msgTarget: "side",
            fieldCls: "obligatoryTextField",
            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
            name: "yearFinal"
          },
          {
            xtype: "ViewComboMonth",
            fieldLabel: "MES FIN",
            padding: "0 20 0 0",
            flex: 1,
            labelWidth: 75,
            allowBlank: false,
            msgTarget: "side",
            fieldCls: "obligatoryTextField",
            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
            name: "monthFinal"
          },
          {
            xtype: "button",
            flex: 1,
            margin: "0 10 0 0",
            action: "generateChartComparativeMbiAsa",
            text: "GENERAR"
          },
          {
            xtype: "button",
            flex: 1,
            iconCls: "excel",
            action: "generateExcelComparativeMbiAsa",
            text: "EXCEL"
          }
        ]
      },
      {
        //                xtype:'form',
        //                region:'west',
        //                padding:'2 0 2 2',
        //                width:400 ,
        xtype: "gridpanel",
        //                cls: 'custom-grid',
        store: StoreGridPanelChartMbiAsa,
        loadMask: true,
        columnLines: true,
        region: "west",
        width: 450,
        padding: "2 0 2 2",
        columns: [
          { xtype: "rownumberer", width: 25, sortable: false },
          { header: "MES", dataIndex: "period", flex: 1 },
          {
            header: "MIB",
            align: "right",
            xtype: "numbercolumn",
            format: "0,0.00",
            dataIndex: "mbi",
            flex: 1
          },
          {
            header: "ASA",
            align: "right",
            xtype: "numbercolumn",
            format: "0,0.00",
            dataIndex: "asa",
            flex: 1
          },
          {
            header: "DIFERENCIA",
            align: "right",
            xtype: "numbercolumn",
            format: "0,0.00",
            dataIndex: "difference",
            flex: 1
          }
        ]
      },
      {
        xtype: "container",
        name: "containerChart",
        region: "center",
        layout: "fit",
        items: []
      }
      //            ,
      //            {
      //                xtype: 'form',
      //                padding:'0 2 2 2',
      //                region:'south',
      //                height: 75,
      //                layout: {
      //                    align: 'stretch',
      //                    type: 'hbox'
      //                },
      //                bodyPadding: 10,
      //                title: '',
      //                items: [
      //                    {
      //                        xtype: 'container',
      //                        width: 500,
      //                        layout: {
      //                            type: 'anchor'
      //                        },
      //                        items: [
      //                            {
      //                                xtype: 'displayfield',
      //                                labelWidth:500,
      //                                anchor: '100%',
      //                                fieldLabel: 'Requerimiento de patrimonio efectivo por riesgo operacional'
      //                            },
      //                            {
      //                                xtype: 'displayfield',
      //                                labelWidth:500,
      //                                anchor: '100%',
      //                                fieldLabel: 'APR riesgo operacional'
      //                            }
      //                        ]
      //                    },
      //                    {
      //                        xtype: 'container',
      //                        flex: 1
      //                    },
      //                    {
      //                        xtype: 'container',
      //                        width: 150,
      //                        layout: {
      //                            type: 'anchor'
      //                        },
      //                        items: [
      //                            {
      //                                xtype: 'UpperCaseTextFieldReadOnly',
      //                                anchor: '100%',
      //                                name: 'equity'
      //                            },
      //                            {
      //                                xtype: 'UpperCaseTextFieldReadOnly',
      //                                anchor: '100%',
      //                                name: 'weightedAssets'
      //                            }
      //                        ]
      //                    }
      //                ]
      //            }
    ];
    this.callParent();
  }
});
