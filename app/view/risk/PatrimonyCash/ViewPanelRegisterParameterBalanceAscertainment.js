Ext.define('DukeSource.view.risk.PatrimonyCash.ViewPanelRegisterParameterBalanceAscertainment', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterParameterBalanceAscertainment',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newRegisterTitleParamBalance'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchRegisterEvent',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'registerEventAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewTreeGridPanelParameterBalanceAscertainment', padding: '2 2 2 2'}];
        this.callParent();
    }
});
