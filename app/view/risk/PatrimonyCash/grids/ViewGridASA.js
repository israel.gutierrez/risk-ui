Ext.define("ModelASA", {
  extend: "Ext.data.Model",
  fields: [
    "period",
    "column1",
    "column2",
    "column3",
    "column4",
    "column5",
    "column6",
    "column7",
    "column8",
    "column9",
    "column10",
    "column11",
    "column12",
    "column13",
    "column14",
    "column15"
  ]
});

var storeASA = Ext.create("Ext.data.Store", {
  model: "ModelASA",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("DukeSource.view.risk.PatrimonyCash.grids.ViewGridASA", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridASA",
  store: storeASA,
  loadMask: true,
  columnLines: true,
  initComponent: function() {
    this.columns = [
      { header: "PERIODOS DE 12 MESES", dataIndex: "period", width: 200 },
      {
        header: "FINANZAS CORPORATIVAS",
        columns: [
          {
            header: "INGRESOS",
            align: "center",
            dataIndex: "column1",
            width: 100,
            xtype: "numbercolumn",
            format: "0,0.00"
          },
          {
            header: "GASTOS",
            align: "center",
            dataIndex: "column2",
            width: 100,
            xtype: "numbercolumn",
            format: "0,0.00"
          },
          {
            header: "IEx(F.A.)",
            align: "center",
            dataIndex: "column3",
            width: 100,
            xtype: "numbercolumn",
            format: "0,0.00"
          }
        ]
      },
      {
        header: "NEGOCIACIÓN Y VENTAS",
        columns: [
          {
            header: "INGRESOS",
            align: "center",
            dataIndex: "column4",
            width: 100,
            xtype: "numbercolumn",
            format: "0,0.00"
          },
          {
            header: "GASTOS",
            align: "center",
            dataIndex: "column5",
            width: 100,
            xtype: "numbercolumn",
            format: "0,0.00"
          },
          {
            header: "IEx(F.A.)",
            align: "center",
            dataIndex: "column6",
            width: 100,
            xtype: "numbercolumn",
            format: "0,0.00"
          }
        ]
      },
      {
        header: "LIQUIDACIÓN Y PAGOS",
        columns: [
          {
            header: "INGRESOS",
            align: "center",
            dataIndex: "column7",
            width: 100,
            xtype: "numbercolumn",
            format: "0,0.00"
          },
          {
            header: "GASTOS",
            align: "center",
            dataIndex: "column8",
            width: 100,
            xtype: "numbercolumn",
            format: "0,0.00"
          },
          {
            header: "IEx(F.A.)",
            align: "center",
            dataIndex: "column9",
            width: 100,
            xtype: "numbercolumn",
            format: "0,0.00"
          }
        ]
      },
      {
        header: "OTROS SERVICIOS",
        columns: [
          {
            header: "INGRESOS",
            align: "center",
            dataIndex: "column10",
            width: 100,
            xtype: "numbercolumn",
            format: "0,0.00"
          },
          {
            header: "GASTOS",
            align: "center",
            dataIndex: "column11",
            width: 100,
            xtype: "numbercolumn",
            format: "0,0.00"
          },
          {
            header: "IEx(F.A.)",
            align: "center",
            dataIndex: "column12",
            width: 100,
            xtype: "numbercolumn",
            format: "0,0.00"
          }
        ]
      },
      {
        header: "BANCA COMERCIAL",
        columns: [
          {
            header: "IEx(F.A.)",
            align: "center",
            dataIndex: "column13",
            width: 100,
            xtype: "numbercolumn",
            format: "0,0.00"
          }
        ]
      },
      {
        header: "BANCA MINORISTA",
        columns: [
          {
            header: "IEx(F.A.)",
            align: "center",
            dataIndex: "column14",
            width: 100,
            xtype: "numbercolumn",
            format: "0,0.00"
          }
        ]
      },
      {
        header: "REQUERIMIENTO<br>ANUAL<br>",
        dataIndex: "column15",
        width: "120",
        xtype: "numbercolumn",
        format: "0,0.00"
      }
    ];
    this.callParent(arguments);
  }
});
