Ext.define("ModelMIB", {
  extend: "Ext.data.Model",
  fields: ["period", "column1", "column2", "column3", "column4", "column5"]
});

var storeMIB = Ext.create("Ext.data.Store", {
  model: "ModelMIB",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("DukeSource.view.risk.PatrimonyCash.grids.ViewGridMIB", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridMIB",
  store: storeMIB,
  loadMask: true,
  columnLines: true,
  initComponent: function() {
    this.columns = [
      { header: "PERIODOS DE 12 MESES", dataIndex: "period", flex: 1 },
      {
        header: "INGRESOS<br> FINANCIEROS<br>",
        align: "center",
        dataIndex: "column1",
        xtype: "numbercolumn",
        format: "0,0.00",
        flex: 1
      },
      {
        header: "INGRESOS <br>POR SERVICIOS<br>",
        align: "center",
        dataIndex: "column2",
        xtype: "numbercolumn",
        format: "0,0.00",
        flex: 1
      },
      {
        header: "GASTOS <br>FINANCIEROS<br>",
        align: "center",
        dataIndex: "column3",
        xtype: "numbercolumn",
        format: "0,0.00",
        flex: 1
      },
      {
        header: "GASTOS <br>POR SERVICIOS<br>",
        align: "center",
        dataIndex: "column4",
        xtype: "numbercolumn",
        format: "0,0.00",
        flex: 1
      },
      {
        header: "MARGEN BRUTO <br>OPERACIONAL<br>",
        align: "center",
        dataIndex: "column5",
        xtype: "numbercolumn",
        format: "0,0.00",
        flex: 1
      }
    ];
    this.callParent(arguments);
  }
});
