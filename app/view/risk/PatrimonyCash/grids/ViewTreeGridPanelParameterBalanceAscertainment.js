Ext.define('DukeSource.view.risk.PatrimonyCash.grids.ViewTreeGridPanelParameterBalanceAscertainment', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.ViewTreeGridPanelParameterBalanceAscertainment',
    store: 'risk.PatrimonyCash.grids.StoreTreeGridPanelParameterBalanceAscertainment',
    useArrows: true,
    multiSelect: true,
    singleExpand: true,
    rootVisible: false,
    columns: [
        {
            xtype: 'treecolumn',
            text: 'TITULO DE REPORTE -> GRUDO DE REPORTE -> SUBGRUPO DE REPORTE-> ITEMS DE REPORTE',
            flex: 2,
            sortable: true,
            dataIndex: 'text'
        }
    ]
});
