Ext.define('DukeSource.view.risk.parameter.grids.ViewTreeGridPanelRegisterBusinessLine', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.ViewTreeGridPanelRegisterBusinessLine',
    store: 'risk.parameter.grids.StoreTreeGridPanelRegisterBusinessLine',
    useArrows: true,
    multiSelect: true,
    singleExpand: true,
    rootVisible: false,
    columns: [
        {
            xtype: 'treecolumn',
            text: 'LINEA DE NEGOCIO N1 -> LINEA DE NEGOCIO N2 -> LINEA DE NEGOCIO N3',
            flex: 2,
            sortable: true,
            dataIndex: 'text'
        }
    ]
});
