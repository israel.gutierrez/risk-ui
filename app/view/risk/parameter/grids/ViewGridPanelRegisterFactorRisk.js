Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterFactorRisk",
  {
    extend: "Ext.tree.Panel",
    alias: "widget.ViewGridPanelRegisterFactorRisk",
    store: "risk.parameter.grids.StoreGridPanelRegisterFactorRisk",
    mixins: { treeFilter: "DukeSource.global.TreeFilter" },
    columnLines: true,
    useArrows: true,
    singleExpand: false,
    rootVisible: false,
    columns: [
      {
        xtype: "treecolumn",
        text: "FACTORES RIESGO OPERACIONAL",
        flex: 2,
        sortable: true,
        dataIndex: "text"
      }
    ],
    listeners: {
      select: function(me, record) {
        var panelFactorRisk = Ext.ComponentQuery.query(
          "ViewPanelRegisterFactorRisk"
        )[0];
        if (record.data["depth"] === 1) {
          panelFactorRisk.down("#modifyTreeFactorRisk").setDisabled(true);
          panelFactorRisk.down("#deleteTreeFactorRisk").setDisabled(true);
        } else {
          panelFactorRisk.down("#modifyTreeFactorRisk").setDisabled(false);
          panelFactorRisk.down("#deleteTreeFactorRisk").setDisabled(false);
        }
      }
    }
  }
);
