Ext.require(["Ext.ux.ColorField"]);
Ext.define("ModelGridPanelRegisterScaleBusinessLine", {
  extend: "Ext.data.Model",
  fields: [
    "idScaleRisk",
    "description",
    { name: "percentageExposition", type: "int" },
    "state",
    { name: "rangeInf", type: "int" },
    { name: "rangeSup", type: "int" },
    "levelColour",
    { name: "businessLineOne", type: "int" },
    { name: "operationalRiskExposition", type: "int" },
    { name: "valueOperationalRiskExposition", type: "int" }
  ]
});

var StoreGridPanelRegisterScaleBusinessLine = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelGridPanelRegisterScaleBusinessLine",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterScaleBusinessLine",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterScaleBusinessLine",
    store: StoreGridPanelRegisterScaleBusinessLine,
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridPanelRegisterScaleBusinessLine,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      //        items:['-',{text:'EXPORTAR A PDF',action:'exportScaleRiskPdf',iconCls:'pdf'},'-',{text:'EXPORTAR A EXCEL',action:'exportScaleRiskExcel',iconCls:'excel'},'-'],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.loadGridDefault(me);
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var panel = grid.up("panel");
          var valueComboBusiness = panel
            .down("ViewComboBusinessLineOne")
            .getValue();
          var valueComboOperation = panel
            .down("ViewComboOperationalRiskExposition")
            .getValue();
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;
            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveScaleRisk.htm?nameView=ViewPanelConfigurationGeneralMatrix",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  grid.store.getProxy().extraParams = {
                    maxExposition: valueComboOperation,
                    businessLine: valueComboBusiness
                  };
                  grid.store.getProxy().url =
                    "http://localhost:9000/giro/findScaleRiskByBusinessLineAndMaxExposition.htm";
                  grid.down("pagingtoolbar").moveFirst();
                  //                                DukeSource.global.DirtyView.searchPaginationGridNormal('',grid, grid.down('pagingtoolbar'),'http://localhost:9000/giro/findScaleRisk.htm','description','description')
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });

            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 25, sortable: false },
        {
          header: "CODIGO",
          align: "center",
          dataIndex: "idScaleRisk",
          flex: 1
        },
        {
          header: "DESCRIPCION",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "% MAXIMA<br> EXPOSICION<br>",
          dataIndex: "percentageExposition",
          align: "center",
          flex: 1,
          xtype: "numbercolumn",
          format: "0,0.00",
          editor: {
            xtype: "NumberDecimalNumberObligatory",
            maxValue: 100.0,
            allowBlank: false
          }
        },
        {
          header: "LIMITE PÉRDIDA<br> INFERIOR<br>",
          align: "right",
          dataIndex: "rangeInf",
          flex: 1,
          xtype: "numbercolumn",
          format: "0,0.00",
          editor: {
            xtype: "NumberDecimalNumberObligatory",
            allowBlank: false
          }
        },
        {
          header: "LIMITE PÉRDIDA<br> SUPERIOR<br>",
          align: "right",
          dataIndex: "rangeSup",
          flex: 1,
          xtype: "numbercolumn",
          format: "0,0.00"
          //                ,editor: {
          //                xtype:'NumberDecimalNumberObligatory',
          //                allowBlank: false
          //            }
        },
        {
          header: "COLOR",
          align: "center",
          dataIndex: "levelColour",
          flex: 1,
          editor: {
            xtype: "colorfield",
            allowBlank: false
          },
          renderer: function(value) {
            return (
              '<div  style="background-color:#' +
              value +
              "; color:#" +
              value +
              ';"> ' +
              value +
              "</div>"
            );
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);
