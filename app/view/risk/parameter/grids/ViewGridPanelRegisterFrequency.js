Ext.define("ModelGridPanelRegisterFrequency", {
  extend: "Ext.data.Model",
  fields: [
    "idFrequency",
    "description",
    "alias",
    { name: "percentage", type: "number" },
    { name: "valueMinimal", type: "number" },
    { name: "valueMaximo", type: "number" },
    "idTypeMatrix",
    "idOperationalRiskExposition",
    "equivalentValue",
    "type"
  ]
});

var StoreGridPanelRegisterFrequency = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelGridPanelRegisterFrequency",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterFrequency",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterFrequency",
    store: StoreGridPanelRegisterFrequency,
    loadMask: true,
    columnLines: true,
    tbar: [
      {
        text: "Nuevo",
        iconCls: "add",
        action: "newFrequency"
      },
      "-",
      {
        text: "Eliminar",
        iconCls: "delete",
        action: "deleteFrequency"
      },
      "->",
      {
        text: "Auditoria",
        action: "frequencyAuditory",
        iconCls: "auditory"
      }
    ],
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridPanelRegisterFrequency,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridFrequency",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var panelGeneral = Ext.ComponentQuery.query(
          "ViewWindowConfigurationMatrix"
        )[0];
        var me = this;
        me.store.getProxy().extraParams = {
          idTypeMatrix: panelGeneral.down("ViewComboTypeMatrix").getValue(),
          idOperationalRiskExposition: panelGeneral
            .down("ViewComboOperationalRiskExposition")
            .getValue()
        };
        me.store.getProxy().url =
          "http://localhost:9000/giro/findFrequency.htm";
        me.down("pagingtoolbar").moveFirst();
      }
    },
    setFactor: function(grid, a) {
      var minValue = grid
        .getPlugin("roweditor")
        .editor.down("#minValue")
        .getValue();
      var maxValue = grid
        .getPlugin("roweditor")
        .editor.down("#maxValue")
        .getValue();

      var values = [parseFloat(minValue), parseFloat(maxValue)];
      var result;
      switch (a.getValue()) {
        case "avg":
          result = mean(values);
          break;
        case "max":
          result = Math.max(values[0], values[1]);
          break;
        case "min":
          result = Math.min(values[0], values[1]);
          break;
        case "nan":
          result = 0;
          break;
      }
      grid
        .getPlugin("roweditor")
        .editor.down("#factor")
        .setValue(result);
    },
    initComponent: function() {
      var me = this;

      var typesStore = Ext.create("Ext.data.Store", {
        fields: ["id", "description"],
        data: [
          { id: "avg", description: "Promedio" },
          { id: "max", description: "Maximo" },
          { id: "min", description: "Minimo" },
          { id: "nan", description: "Editable" }
        ]
      });

      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "Guardar",
        pluginId: "roweditor",
        cancelBtnText: "Cancelar",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;
            var windowConfiguration = Ext.ComponentQuery.query(
              "ViewWindowConfigurationMatrix"
            )[0];
            var gridConfiguration = Ext.ComponentQuery.query(
              "ViewPanelConfigurationGeneralMatrix ViewGridPanelRegisterConfigurationMatrix"
            )[0];
            if (
              windowConfiguration.stateConfig === "P" ||
              windowConfiguration.stateConfig === "N"
            ) {
              Ext.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/saveFrequency.htm?nameView=ViewPanelConfigurationGeneralMatrix",
                params: {
                  jsonData: Ext.JSON.encode(record.data),
                  idTypeMatrix: windowConfiguration
                    .down("ViewComboTypeMatrix")
                    .getValue(),
                  idOperationalRiskExposition: windowConfiguration
                    .down("ViewComboOperationalRiskExposition")
                    .getValue()
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      response.mensaje,
                      Ext.Msg.INFO
                    );
                    grid.store.getProxy().extraParams = {
                      idTypeMatrix: windowConfiguration
                        .down("ViewComboTypeMatrix")
                        .getValue(),
                      idOperationalRiskExposition: windowConfiguration
                        .down("ViewComboOperationalRiskExposition")
                        .getValue()
                    };
                    grid.store.getProxy().url =
                      "http://localhost:9000/giro/findFrequency.htm";
                    grid.down("pagingtoolbar").moveFirst();
                    windowConfiguration
                      .down("#idConfigurationImpact")
                      .setDisabled(false);
                    if (response.data === 0) {
                      gridConfiguration.store.getProxy().extraParams = {
                        idOperationalRiskExposition: windowConfiguration
                          .down("ViewComboOperationalRiskExposition")
                          .getValue()
                      };
                      gridConfiguration.store.getProxy().url =
                        "http://localhost:9000/giro/findExpositionMatrix.htm";
                      gridConfiguration.down("pagingtoolbar").moveFirst();
                    }
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_ERROR,
                      response.mensaje,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function() {}
              });
              me.fireEvent("edit", me, me.context);
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                "Para modificar, debe ELIMINAR la Matriz generada",
                Ext.Msg.WARNING
              );
            }
          }
        }
      });
      this.columns = [
        {
          header: "Valor equivalente",
          dataIndex: "equivalentValue",
          align: "center",
          flex: 0.4
        },
        {
          header: "Descripción",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextFieldObligatory",
            allowBlank: false
          }
        },
        {
          header: "Valor inferior",
          dataIndex: "valueMinimal",
          xtype: "numbercolumn",
          minValue: 0,
          format: "0,0.00",
          align: "center",
          flex: 1,
          editor: {
            xtype: "NumberDecimalNumberObligatory",
            itemId: "minValue",
            decimalPrecision: DECIMAL_PRECISION,
            enableKeyEvents: true,
            allowBlank: false,
            listeners: {
              keyup: function(f, e) {
                var grid = f.up("panel").context.grid;
                var a = grid.getPlugin("roweditor").editor.down("#calculation");
                me.setFactor(grid, a);
              }
            }
          }
        },
        {
          header: "Valor superior",
          dataIndex: "valueMaximo",
          xtype: "numbercolumn",
          format: "0,0.00",
          minValue: 0,
          align: "center",
          flex: 1,
          editor: {
            xtype: "NumberDecimalNumberObligatory",
            itemId: "maxValue",
            decimalPrecision: DECIMAL_PRECISION,
            maxValue: 999,
            allowBlank: false,
            enableKeyEvents: true,
            listeners: {
              keyup: function(f, e) {
                var grid = f.up("panel").context.grid;
                var a = grid.getPlugin("roweditor").editor.down("#calculation");
                me.setFactor(grid, a);
              }
            }
          }
        },
        {
          header: "Calculo",
          dataIndex: "type",
          align: "center",
          flex: 1,
          editor: {
            xtype: "combobox",
            queryMode: "local",
            itemId: "calculation",
            displayField: "description",
            valueField: "id",
            editable: true,
            forceSelection: true,
            store: typesStore,
            allowBlank: false,
            listeners: {
              select: function(a, b, c, d) {
                var grid = a.up("panel").context.grid;
                me.setFactor(grid, a);
                if (a.getValue() === "nan") {
                  grid
                    .getPlugin("roweditor")
                    .editor.down("#factor")
                    .setReadOnly(false);
                  grid
                    .getPlugin("roweditor")
                    .editor.down("#factor")
                    .setFieldStyle("background: #d9ffdb");
                } else {
                  grid
                    .getPlugin("roweditor")
                    .editor.down("#factor")
                    .setReadOnly(true);
                  grid
                    .getPlugin("roweditor")
                    .editor.down("#factor")
                    .setFieldStyle("background: #e2e2e2");
                }
              }
            }
          },
          renderer: function(value) {
            var idx = typesStore.find("id", value);
            var rec = typesStore.getAt(idx);
            return rec.get("description");
          }
        },
        {
          header: "Factor",
          align: "center",
          dataIndex: "percentage",
          xtype: "numbercolumn",
          format: "0,0.00",
          flex: 1,
          editor: {
            xtype: "numberfield",
            fieldCls: "readOnlyTextCenter",
            itemId: "factor",
            readOnly: true,
            decimalPrecision: DECIMAL_PRECISION,
            allowBlank: false
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);

function numbers(vals) {
  var nums = [];
  if (vals == null) return nums;

  for (var i = 0; i < vals.length; i++) {
    if (isNumber(vals[i])) nums.push(+vals[i]);
  }
  return nums;
}

function sum(vals) {
  vals = numbers(vals);
  var total = 0;
  for (var i = 0; i < vals.length; i++) {
    total += vals[i];
  }
  return total;
}

function mean(vals) {
  vals = numbers(vals);
  if (vals.length === 0) return NaN;
  return parseFloat(sum(vals) / vals.length);
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
