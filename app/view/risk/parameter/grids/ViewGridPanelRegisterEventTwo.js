Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterEventTwo",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterEventTwo",
    store: "risk.parameter.grids.StoreGridPanelRegisterEventTwo",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterEventTwo",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridEventTwo",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findEventTwo.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        { header: "PRIMER EVENTO", dataIndex: "eventOne", flex: 1 },
        { header: "SEGUNDO EVENTO", dataIndex: "idEventTwo", flex: 1 },
        { header: "DESCRIPCION", dataIndex: "description", flex: 1 },
        { header: "ESTADO", dataIndex: "state", width: 60 }
      ];
      this.callParent(arguments);
    }
  }
);
