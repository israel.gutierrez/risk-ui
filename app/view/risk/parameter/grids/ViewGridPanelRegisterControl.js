Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterControl",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterControl",
    store: "risk.parameter.grids.StoreGridPanelRegisterControl",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterControl",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridControl",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "%",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findControl.htm",
          "c.description",
          "c.idControl"
        );
      }
    },
    initComponent: function() {
      this.columns = [
        {
          xtype: "rownumberer",
          sortable: false,
          width: 45
        },
        {
          header: "Calificación",
          align: "center",
          dataIndex: "descriptionEvaluation",
          hidden: hidden("WRCDescriptionEvaluation"),
          width: 140,
          renderer: function(value, metaData, record) {
            metaData.tdAttr =
              'style="background-color: #' +
              record.get("colorQualification") +
              ' !important;"';
            return (
              record.get("descriptionEvaluation") +
              " - " +
              record.get("valueScoreControl")
            );
          }
        },
        {
          header: "Código control",
          align: "center",
          dataIndex: "codeControl",
          width: 100
        },
        {
          header: "Estado",
          align: "center",
          dataIndex: "stateEvaluated",
          width: 140,
          renderer: function(value, meta) {
            if (value === "N") {
              return '<span style="color:red;">' + "SIN EVALUAR" + "</span>";
            } else if (value === "S") {
              return "EVALUADO";
            }
          }
        },
        {
          header: "Descripción",
          dataIndex: "descriptionControl",
          flex: 4
        },
        {
          header: "Responsable, quién?",
          dataIndex: "jobPlaceDescription",
          flex: 2
        },
        {
          header: getName("WRCInternalDescription"),
          hidden: hidden("WRCInternalDescription"),
          dataIndex: "internalDescription",
          flex: 2
        },
        {
          header: getName("WRCExternalDescription"),
          hidden: hidden("WRCExternalDescription"),
          dataIndex: "externalDescription",
          flex: 2
        }
      ];
      this.callParent(arguments);
    }
  }
);
