Ext.define('DukeSource.view.risk.parameter.grids.ViewTreeGridPanelRegisterControlType', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.ViewTreeGridPanelRegisterControlType',
    store: 'risk.parameter.grids.StoreTreeGridPanelRegisterControlType',
    useArrows: true,
    multiSelect: true,
    singleExpand: true,
    rootVisible: false,
    columns: [
        {
            xtype: 'treecolumn',
            text: 'TIPO DE CONTROL -> ESPECIFICACION DE TIPO DE CONTROL',
            width: 500,
            sortable: true,
            dataIndex: 'text'
        },
        {
            text: 'VALOR FUNCION',
            flex: 2,
            sortable: true,
            dataIndex: 'fieldOne',
            align: 'center'
        }

    ]
});
