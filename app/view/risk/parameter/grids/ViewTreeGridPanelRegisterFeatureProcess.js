Ext.define('DukeSource.view.risk.parameter.grids.ViewTreeGridPanelRegisterFeatureProcess', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.ViewTreeGridPanelRegisterFeatureProcess',
    store: 'risk.parameter.grids.StoreTreeGridPanelRegisterFeatureProcess',
    useArrows: true,
    multiSelect: true,
    singleExpand: true,
    rootVisible: false,
    columns: [
        {
            xtype: 'treecolumn',
            text: 'CARACTERÍSTICA DE PROCESO -> EVALUACIÓN CARACTERÍSTICA DE PROCESO',
            width: 500,
            sortable: true,
            dataIndex: 'text'
        },
        {
            text: 'CONCEPTO VALORACION',
            flex: 2,
            sortable: true,
            dataIndex: 'fieldOne',
            align: 'left'
        },
        {
            text: 'VALOR CARACTERISTICA',
            flex: 2,
            sortable: true,
            dataIndex: 'fieldTwo',
            align: 'left'
        }

    ]
});
