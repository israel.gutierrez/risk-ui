Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterEventOne",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterEventOne",
    store: "risk.parameter.grids.StoreGridPanelRegisterEventOne",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterEventOne",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridEventOne",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findEventOne.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/saveEventOne.htm",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findEventOne.htm",
                    "description",
                    "description"
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 30, sortable: false },
        { header: "CODIGO", dataIndex: "idEventOne", flex: 1 },
        {
          header: "DESCRIPCION",
          dataIndex: "description",
          flex: 4,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "ABREVIATURA",
          dataIndex: "abbreviation",
          flex: 4,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },

        {
          header: "ESTADO",
          dataIndex: "state",
          flex: 1,
          editor: new Ext.form.field.ComboBox({
            triggerAction: "all",
            editable: false,
            readOnly: true,
            selectOnTab: true,
            allowBlank: false,
            store: [
              ["S", "SI"],
              ["N", "NO"]
            ],
            lazyRender: true,
            listClass: "x-combo-list-small"
          })
        }
      ];
      this.callParent(arguments);
    }
  }
);
