var rowEdit = Ext.define("ModelGridPanelRegisterConfigurationMatrix", {
  extend: "Ext.data.Model",
  fields: [
    "idExpositionMatriz",
    "typeMatrix",
    "idOperationalRiskExposition",
    "stateConfig",
    "levelConfig",
    "nameTypeMatrix",
    { name: "valueRiskExposition", type: "number" },
    "year",
    "validity",
    "nameCreatorUser",
    "nameValidatorUser",
    "stateVerification"
  ]
});

var StoreGridPanelRegisterConfigurationMatrix = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelGridPanelRegisterConfigurationMatrix",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterConfigurationMatrix",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterConfigurationMatrix",
    store: StoreGridPanelRegisterConfigurationMatrix,
    loadMask: true,
    columnLines: true,
    tbar: [
      {
        text: "Nuevo",
        iconCls: "add",
        action: "newConfigurationMatrix"
      },
      "-",
      {
        text: "Eliminar",
        iconCls: "delete",
        action: "deleteConfigurationMatrix"
      },
      "-",
      {
        text: "Replicar",
        iconCls: "replicate",
        action: "replicateConfigurationMatrix"
      },
      "-",
      "-",
      {
        xtype: "ViewComboOperationalRiskExposition",
        labelWidth: 150,
        fieldLabel: "Patrimonio efectivo",
        listeners: {
          select: function(cbo, records) {
            var panel = Ext.ComponentQuery.query(
              "ViewPanelConfigurationGeneralMatrix"
            )[0];
            var gridConfiguration = panel.down(
              "ViewGridPanelRegisterConfigurationMatrix"
            );
            if (cbo !== undefined) {
              if (records[0].data.validity === "S") {
                panel
                  .down("button[action=newConfigurationMatrix]")
                  .setDisabled(false);
                panel
                  .down("button[action=deleteConfigurationMatrix]")
                  .setDisabled(false);
                panel.down("button[action=checkConfigurationMatrix]") !==
                undefined
                  ? panel
                      .down("button[action=checkConfigurationMatrix]")
                      .setDisabled(false)
                  : "";
              } else {
                panel
                  .down("button[action=newConfigurationMatrix]")
                  .setDisabled(true);
                panel
                  .down("button[action=deleteConfigurationMatrix]")
                  .setDisabled(true);
                panel.down("button[action=checkConfigurationMatrix]") !==
                undefined
                  ? panel
                      .down("button[action=checkConfigurationMatrix]")
                      .setDisabled(true)
                  : "";
              }
              gridConfiguration.store.getProxy().extraParams = {
                idOperationalRiskExposition: cbo.getValue()
              };
              gridConfiguration.store.getProxy().url =
                "http://localhost:9000/giro/findExpositionMatrix.htm";
              gridConfiguration.down("pagingtoolbar").moveFirst();
            }
          },
          render: function(me) {
            me.store.load({
              callback: function(cbo) {
                for (var i = 0; i < cbo.length; i++) {
                  if (cbo[i].data.validity === "S") {
                    me.setValue(cbo[i].data.idOperationalRiskExposition);
                    var gridConfiguration = Ext.ComponentQuery.query(
                      "ViewPanelConfigurationGeneralMatrix ViewGridPanelRegisterConfigurationMatrix"
                    )[0];
                    if (cbo !== undefined) {
                      gridConfiguration.store.getProxy().extraParams = {
                        idOperationalRiskExposition:
                          cbo[i].data.idOperationalRiskExposition
                      };
                      gridConfiguration.store.getProxy().url =
                        "http://localhost:9000/giro/findExpositionMatrix.htm";
                      gridConfiguration.down("pagingtoolbar").moveFirst();
                    }
                  }
                }
              }
            });
          }
        }
      },
      "-",
      {
        text: "Consultar",
        hidden: true,
        iconCls: "replicate",
        handler: function() {
          var view = Ext.create(
            "DukeSource.view.risk.parameter.windows.ViewWindowRiskEvaluationPending",
            {
              modal: true
            }
          );
          view.show();
        }
      },
      "->",
      {
        text: "Auditoria",
        action: "auditoryConfigurationMatrix",
        iconCls: "auditory"
      }
    ],
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridPanelRegisterConfigurationMatrix,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridFrequency",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      afterrender: function(me) {
        if (userRole === DukeSource.global.GiroConstants.SUPER_ANALYST) {
          me.down("toolbar").add(6, {
            text: "Aprobar",
            iconCls: "check",
            action: "checkConfigurationMatrix"
          });
        } else {
        }
      }
    },
    initComponent: function() {
      this.columns = [
        {
          xtype: "rownumberer",
          width: 25,
          sortable: false
        },
        {
          header: "Patrimonio efectivo",
          align: "right",
          xtype: "numbercolumn",
          format: "0,0.00",
          dataIndex: "valueRiskExposition",
          width: 100
        },
        {
          header: "Año",
          align: "center",
          dataIndex: "year",
          width: 50
        },
        {
          header: "Tipo de matriz",
          dataIndex: "nameTypeMatrix",
          align: "center",
          flex: 1
        },
        {
          header: "Configuración",
          dataIndex: "levelConfig",
          align: "center",
          width: 200,
          renderer: function(value, meta) {
            if (value.trim() == "1") {
              return (
                "Frecuencia," +
                '<span style="color:red;">' +
                "Frecuencia" +
                "</span>"
              );
            } else if (value.trim() == "2") {
              return (
                "Frecuencia, Impacto" +
                '<span style="color:red;">' +
                ", Matriz" +
                "</span>"
              );
            } else if (value.trim() == "3") {
              return "Frecuencia, Impacto, Matriz";
            }
          }
        },
        {
          header: "Usuario que registró",
          dataIndex: "nameCreatorUser",
          align: "center",
          flex: 0.7
        },
        {
          header: "Usuario que verificó",
          dataIndex: "nameValidatorUser",
          align: "center",
          flex: 0.7
        },
        {
          header: "Estado",
          dataIndex: "stateConfig",
          align: "center",
          width: 100,
          renderer: function(value, meta) {
            if (value === "P") {
              return '<span style="color:red;">' + "Pendiente" + "</span>";
            } else if (value === "F") {
              return "Finalizado";
            }
          }
        },
        {
          header: "Aprobación",
          dataIndex: "stateVerification",
          align: "center",
          width: 90,
          renderer: function(value, meta) {
            if (value === "P") {
              return '<span style="color:red;">' + "Pendiente" + "</span>";
            } else if (value === "A") {
              return "Aprobado";
            }
          }
        },
        {
          xtype: "actioncolumn",
          header: "Modificar",
          align: "center",
          width: 70,
          dataIndex: "stateConfig",
          items: [
            {
              icon: "images/application_form_edit.png",
              handler: function(grid, rowIndex) {
                if (grid.store.getAt(rowIndex).get("validity") === "S") {
                  if (
                    userRole !==
                      DukeSource.global.GiroConstants.SUPER_ANALYST &&
                    grid.store.getAt(rowIndex).get("stateVerification") === "A"
                  ) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      "No Cuenta con permisos suficientes para MODIFICAR",
                      Ext.Msg.INFO
                    );
                  } else {
                    Ext.Ajax.request({
                      method: "POST",
                      url: "http://localhost:9000/giro/findLevelConfig.htm",
                      params: {
                        idOperationalRiskExposition: grid.store
                          .getAt(rowIndex)
                          .get("idOperationalRiskExposition"),
                        idTypeMatrix: grid.store
                          .getAt(rowIndex)
                          .get("typeMatrix")
                      },
                      success: function(response) {
                        response = Ext.decode(response.responseText);
                        if (response.success) {
                          var windowConfiguration = Ext.create(
                            "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowConfigurationMatrix",
                            {
                              title:
                                "Configuración de matriz : " +
                                grid.store
                                  .getAt(rowIndex)
                                  .get("nameTypeMatrix"),
                              stateVerification: grid.store
                                .getAt(rowIndex)
                                .get("stateVerification"),
                              stateConfig: grid.store
                                .getAt(rowIndex)
                                .get("stateConfig")
                            }
                          );
                          var cboRiskExposition = windowConfiguration.down(
                            "ViewComboOperationalRiskExposition"
                          );
                          cboRiskExposition.store.load({
                            callback: function(cbo) {
                              for (var i = 0; i < cbo.length; i++) {
                                if (cbo[i].data.validity == "S") {
                                  cboRiskExposition.setValue(
                                    cbo[i].data.idOperationalRiskExposition
                                  );

                                  windowConfiguration
                                    .down("ViewComboTypeMatrix")
                                    .getStore()
                                    .load({
                                      callback: function() {
                                        windowConfiguration
                                          .down("ViewComboTypeMatrix")
                                          .setValue(
                                            grid.store
                                              .getAt(rowIndex)
                                              .get("typeMatrix")
                                          );
                                        windowConfiguration
                                          .down("ViewComboTypeMatrix")
                                          .setReadOnly(true);

                                        if (response.data == "1") {
                                          windowConfiguration
                                            .down("#idConfigurationFrequency")
                                            .setDisabled(false);
                                          windowConfiguration
                                            .down("#idConfigurationImpact")
                                            .setDisabled(false);
                                          windowConfiguration
                                            .down("panel")
                                            .add({
                                              xtype: "tabpanel",
                                              padding: "2 2 2 2",
                                              items: [
                                                {
                                                  title: "Impacto",
                                                  xtype:
                                                    "ViewGridPanelRegisterImpact"
                                                },
                                                {
                                                  title: "Criterio de impacto",
                                                  xtype:
                                                    "ViewGridPanelRegisterImpacfeature"
                                                },
                                                {
                                                  title:
                                                    "Detalle criterio de impacto",
                                                  xtype:
                                                    "ViewGridPanelRegisterDetailImpactFeature"
                                                }
                                              ]
                                            });
                                          windowConfiguration.show();
                                        } else if (
                                          response.data == "2" ||
                                          response.data == "3"
                                        ) {
                                          windowConfiguration
                                            .down("#idConfigurationFrequency")
                                            .setDisabled(false);
                                          windowConfiguration
                                            .down("#idConfigurationImpact")
                                            .setDisabled(false);
                                          windowConfiguration
                                            .down("#idConfigurationMatrix")
                                            .setDisabled(false);
                                          windowConfiguration
                                            .down("panel")
                                            .add({
                                              xtype:
                                                "ViewPanelGenerateAllMatrix",
                                              padding: "2 2 2 2",
                                              idOperationalRiskExposition: grid.store
                                                .getAt(rowIndex)
                                                .get(
                                                  "idOperationalRiskExposition"
                                                ),
                                              idTypeMatrix: grid.store
                                                .getAt(rowIndex)
                                                .get("typeMatrix"),
                                              originModify: "modify"
                                            });
                                          windowConfiguration.show();
                                        }
                                      }
                                    });
                                }
                              }
                            }
                          });
                        } else {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_ERROR,
                            response.mensaje,
                            Ext.Msg.ERROR
                          );
                        }
                      },
                      failure: function() {}
                    });
                  }
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    "El PATRIMONIO EFECTIVO no esta vigente, No puede modificar",
                    Ext.Msg.INFO
                  );
                }
              }
            }
          ]
        },
        {
          xtype: "actioncolumn",
          header: "Detalle",
          align: "center",
          width: 70,
          items: [
            {
              icon: "images/detail.png",
              handler: function(grid, rowIndex) {
                if (grid.store.getAt(rowIndex).get("stateConfig") === "F") {
                  var window = Ext.create(
                    "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowConfigurationMatrixReport",
                    {
                      title:
                        "Detalle de matriz : " +
                        grid.store.getAt(rowIndex).get("nameTypeMatrix"),
                      modal: true
                    }
                  );
                  var windowGrid = window.down("#itemIdResumeMatrix");
                  Ext.Ajax.request({
                    method: "POST",
                    url:
                      "http://localhost:9000/giro/summaryConfigurationMatrices.htm",
                    params: {
                      idOperationalRiskExposition: grid.store
                        .getAt(rowIndex)
                        .get("idOperationalRiskExposition"),
                      idTypeMatrix: grid.store
                        .getAt(rowIndex)
                        .get("typeMatrix"),
                      equivalentValue: "equivalentValue"
                    },
                    success: function(response) {
                      response = Ext.decode(response.responseText);
                      if (response.success) {
                        window
                          .down("#itemIdResumeScaleMatrix")
                          .getStore()
                          .loadData(JSON.parse(response.listScaleRiskBean));
                        window
                          .down("#itemIdResumeImpact")
                          .getStore()
                          .loadData(JSON.parse(response.listImpactBean));
                        window
                          .down("#itemIdResumeFrequency")
                          .getStore()
                          .loadData(JSON.parse(response.listFrequencyBean));
                        var columns = JSON.parse(response.columnMatrix);
                        var fields = JSON.parse(response.fieldsMatrix);
                        DukeSource.global.DirtyView.createMatrix(
                          columns,
                          fields,
                          windowGrid
                        );
                        window
                          .down("#itemIdResumeMatrix")
                          .getStore()
                          .loadData(JSON.parse(response.listMatrixBean));
                        window.show();
                      } else {
                        DukeSource.global.DirtyView.messageAlert(
                          DukeSource.global.GiroMessages.TITLE_ERROR,
                          response.mensaje,
                          Ext.Msg.ERROR
                        );
                      }
                    },
                    failure: function() {}
                  });
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_WARNING,
                    "Debe finalizar la configuracion",
                    Ext.Msg.WARNING
                  );
                }
              }
            }
          ]
        }
      ];
      this.callParent(arguments);
    }
  }
);
