Ext.define('DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterProduct', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.ViewGridPanelRegisterProduct',
    store: 'risk.parameter.grids.StoreGridPanelRegisterProduct',
    useArrows: true,
    multiSelect: true,
    mixins: {
        treeFilter: "TreeFilter"
    },
    singleExpand: false,
    expanded: true,
    rootVisible: false,
    columns: [
        {
            xtype: 'treecolumn',
            text: 'Productos',
            flex: 2,
            sortable: true,
            dataIndex: 'text'
        }
    ]
});
