Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterEventThree",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterEventThree",
    store: "risk.parameter.grids.StoreGridPanelRegisterEventThree",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterEventThree",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridEventThree",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findEventThree.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        { header: "PRIMER EVENTO", dataIndex: "eventOne", flex: 1 },
        { header: "SEGUNDO EVENTO", dataIndex: "eventTwo", flex: 1 },
        { header: "ARBOL EVENTO", dataIndex: "idEventThree", flex: 1 },
        { header: "DESCRIPCION", dataIndex: "description", flex: 1 },
        { header: "ESTADO", dataIndex: "state", width: 60 }
      ];
      this.callParent(arguments);
    }
  }
);
