Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewTreeGridPanelRegisterCompany",
  {
    extend: "Ext.tree.Panel",
    alias: "widget.ViewTreeGridPanelRegisterCompany",
    store: "risk.parameter.grids.StoreTreeGridPanelRegisterCompany",
    padding: 5,
    useArrows: true,
    multiSelect: true,
    singleExpand: true,
    rootVisible: false,
    columns: [
      {
        xtype: "treecolumn",
        text: "Empresa",
        flex: 2,
        sortable: true,
        dataIndex: "text"
      }
    ]
  }
);
