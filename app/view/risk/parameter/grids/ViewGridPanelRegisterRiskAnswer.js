Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterRiskAnswer",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterRiskAnswer",
    store: "risk.parameter.grids.StoreGridPanelRegisterRiskAnswer",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterRiskAnswer",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridRiskAnswer",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findRiskAnswer.htm",
          "description",
          "idRiskAnswer"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveRiskAnswer.htm?nameView=ViewPanelRegisterRiskAnswer",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findRiskAnswer.htm",
                    "description",
                    "idRiskAnswer"
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        {
          xtype: "rownumberer",
          width: 25,
          sortable: false
        },
        {
          header: "CODIGO",
          align: "center",
          dataIndex: "idRiskAnswer",
          flex: 1,
          hidden: true
        },
        {
          header: "DESCRIPCION",
          dataIndex: "description",
          flex: 3,
          editor: {
            xtype: "UpperCaseTextFieldObligatory"
          }
        },
        {
          header: "MÍNIMO DE PLANES",
          align: "center",
          dataIndex: "minActionPlan",
          allowBlank: false,
          flex: 1,
          editor: {
            xtype: "numberfield",
            fieldCls: "obligatoryTextField",
            value: 0,
            maxValue: 100,
            minValue: 0
          }
        },
        {
          header: "MÁXIMO DE PLANES",
          align: "center",
          allowBlank: false,
          dataIndex: "maxActionPlan",
          flex: 1,
          editor: {
            xtype: "numberfield",
            fieldCls: "obligatoryTextField",
            value: 100,
            maxValue: 100,
            minValue: 0
          }
        },
        {
          header: "MÍNIMO DE CONTROLES",
          align: "center",
          allowBlank: false,
          dataIndex: "minControl",
          flex: 1,
          editor: {
            xtype: "numberfield",
            fieldCls: "obligatoryTextField",
            value: 0,
            maxValue: 100,
            minValue: 0
          }
        },
        {
          header: "MÁXIMO DE CONTROLES",
          align: "center",
          allowBlank: false,
          dataIndex: "maxControl",
          flex: 1,
          editor: {
            xtype: "numberfield",
            fieldCls: "obligatoryTextField",
            value: 100,
            maxValue: 100,
            minValue: 0
          }
        },
        {
          header: "ESTADO",
          align: "center",
          dataIndex: "state",
          flex: 1
        }
      ];
      this.callParent(arguments);
    }
  }
);
