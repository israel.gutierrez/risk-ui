Ext.define("ModelComboBusinessLineOneTypeMatrix", {
  extend: "Ext.data.Model",
  fields: ["idBusinessLineOne", "description"]
});

var StoreComboBusinessLineOneTypeMatrix = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboBusinessLineOneTypeMatrix",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListBusinessLineOneActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
var rowEdit = Ext.define("ModelGridPanelRegisterTypeMatrixBusinessLine", {
  extend: "Ext.data.Model",
  fields: [
    "idTypeMatrixBusinessLine",
    "typeMatrix",
    "businessLineOne",
    "nameTypeMatrix",
    "nameBusinessLineOne",
    "state"
  ]
});

var StoreGridPanelRegisterTypeMatrixBusinessLine = Ext.create(
  "Ext.data.Store",
  {
    extend: "Ext.data.Store",
    model: "ModelGridPanelRegisterTypeMatrixBusinessLine",
    proxy: {
      actionMethods: {
        create: "POST",
        read: "POST",
        update: "POST"
      },
      type: "ajax",
      url: "http://localhost:9000/giro/loadGridDefault.htm",
      reader: {
        type: "json",
        totalProperty: "totalCount",
        root: "data",
        successProperty: "success"
      }
    }
  }
);

Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterTypeMatrixBusinessLine",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterTypeMatrixBusinessLine",
    store: StoreGridPanelRegisterTypeMatrixBusinessLine,
    loadMask: true,
    columnLines: true,
    tbar: [
      {
        xtype: "UpperCaseTextField",
        action: "searchTypeMatrixBusinessLine",
        fieldLabel: "BUSCAR",
        labelWidth: 60,
        width: 300
      },
      "-",
      "->",
      {
        text: "AUDITORIA",
        action: "typeMatrixBusinessLineAuditory",
        iconCls: "auditory"
      }
    ],
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridPanelRegisterTypeMatrixBusinessLine,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        //            {text:'EXPORTAR A PDF',action:'exportFrequencyPdf',iconCls:'pdf'},'-',
        //            {text:'EXPORTAR A EXCEL',action:'exportFrequencyExcel',iconCls:'excel'},'-',
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridTypeMatrixBusinessLine",
          labelWidth: 60,
          width: 200
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findFrequency.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveTypeMatrixBusinessLine.htm?nameView=ViewPanelRegisterTypeMatrix",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findFrequency.htm",
                    "description",
                    "description"
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 25, sortable: false },
        {
          header: "CODIGO",
          align: "center",
          dataIndex: "idFrequency",
          width: 60
        },
        { header: "TIPO DE MATRIZ", dataIndex: "description", flex: 1 },
        {
          header: "LINEA DE NEGOCIO",
          dataIndex: "equivalentValue",
          align: "center",
          flex: 1,
          editor: {
            xtype: "ViewComboBusinessLineOne",
            store: StoreComboBusinessLineOneTypeMatrix
          },
          renderer: function(val) {
            var Impact = Ext.getStore(StoreComboBusinessLineOneTypeMatrix);
            var index = Impact.findExact("idBusinessLineOne", val);
            if (index != -1) {
              var rs = Impact.getAt(index).data;
              return rs.description;
            }
          }
        },
        {
          header: "ESTADO",
          align: "center",
          dataIndex: "percentage",
          xtype: "numbercolumn",
          format: "0,0.00",
          width: 60
        }
      ];
      this.callParent(arguments);
    }
  }
);
