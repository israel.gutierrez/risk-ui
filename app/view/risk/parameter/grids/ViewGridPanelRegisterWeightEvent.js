Ext.define('DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterWeightEvent', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ViewGridPanelRegisterWeightEvent',
    store: 'risk.parameter.grids.StoreGridPanelRegisterWeightEvent',
    loadMask: true,
    columnLines: true,
    bbar: {
        xtype: 'pagingtoolbar',
        pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
        store: 'risk.parameter.grids.StoreGridPanelRegisterWeightEvent',
        displayInfo: false,
        displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
//        items:['-',{text:'EXPORTAR A PDF',action:'exportWeightEventPdf',iconCls:'pdf'},'-',{text:'EXPORTAR A EXCEL',action:'exportWeightEventExcel',iconCls:'excel'},'-',{ xtype:"UpperCaseTrigger",fieldLabel:'FILTRAR',action:'searchTriggerGridWeightEvent',labelWidth:60,width:300}],
        emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    features: [
        {
            ftype: 'summary'
        }
    ],
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        })
    ],

    initComponent: function () {
        this.columns = [
            {xtype: 'rownumberer', width: 50, sortable: false}
            , {header: 'idEventOne', hidden: true, dataIndex: 'idEventOne', flex: 1}
            , {header: 'IdScorecardMaster', hidden: true, dataIndex: 'idScorecardMaster', flex: 1}
//            ,{header: 'NOMBRE DE CUADRO DEL MANDO', dataIndex: 'nameScorecardMaster',  flex: 3}
            , {header: 'EVENTO ', dataIndex: 'nameEventOne', flex: 3}
            , {
                text: "PESO",
                dataIndex: 'weight',
                flex: 1,
                tdCls: 'custom-column',
                editor: {
                    xtype: 'NumberDecimalNumberObligatory',
                    allowBlank: false
                },
                summaryType: "sum"
            }
//            ,{header: 'PESO', dataIndex: 'weight',  flex: 1}
            , {header: 'ESTADO', dataIndex: 'state', width: 60}
        ];
        this.callParent(arguments);
    }
});
