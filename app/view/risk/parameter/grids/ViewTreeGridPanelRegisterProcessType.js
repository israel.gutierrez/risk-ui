Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewTreeGridPanelRegisterProcessType",
  {
    extend: "Ext.tree.Panel",
    alias: "widget.ViewTreeGridPanelRegisterProcessType",
    store: "risk.parameter.grids.StoreTreeGridPanelRegisterProcessType",
    useArrows: true,
    multiSelect: true,
    mixins: {
      treeFilter: "DukeSource.global.TreeFilter"
    },
    singleExpand: false,
    title: "Procesos",
    id: "treeProcess",
    rootVisible: false,
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        dockedItems: [
          {
            xtype: "toolbar",
            dock: "top",
            items: [
              {
                text: "Nuevo",
                iconCls: "add",
                action: "newProcess"
              },
              {
                text: "Modificar",
                iconCls: "modify",
                action: "modifyProcess"
              },
              {
                text: "Eliminar",
                iconCls: "delete",
                action: "deleteProcess"
              },
              "-",
              {
                text: "Evaluar",
                iconCls: "matrix",
                action: "evaluateProcess"
              },
              "-",
              {
                text: "Adjunto",
                iconCls: "gestionfile",
                action: "manageAttachment"
              }
            ]
          },
          {
            xtype: "toolbar",
            dock: "top",
            items: [
              {
                xtype: "textfield",
                itemId: "searchOption",
                name: "searchOption",
                labelWidth: 60,
                padding: 2,
                fieldLabel: "Buscar",
                width: 250,
                emptyText: "Buscar",
                enableKeyEvents: true,
                listeners: {
                  afterrender: function(e) {
                    e.focus(false, 1000);
                  },
                  keyup: function(e, t) {
                    me.expandAll(me);
                    me.filterByText(e.getValue());
                    if (e.getValue() === "") {
                      me.collapseAll(me);
                    }
                  }
                }
              }
            ]
          }
        ],
        columns: [
          {
            xtype: "treecolumn",
            text: "Procesos",
            width: 800,
            sortable: true,
            dataIndex: "text"
          }
        ],
        listeners: {
          beforerender: function(view) {
            view.headerCt.border = 1;
          }
        }
      });
      me.callParent(arguments);
    }
  }
);
