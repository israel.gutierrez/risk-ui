Ext.define("ModelGridPanelRegisterOperationalRiskExposition", {
  extend: "Ext.data.Model",
  fields: [
    "idOperationalRiskExposition",
    { name: "maxAmount", type: "number" },
    "year",
    "validity"
  ]
});

var StoreGridPanelRegisterOperationalRiskExposition = Ext.create(
  "Ext.data.Store",
  {
    extend: "Ext.data.Store",
    model: "ModelGridPanelRegisterOperationalRiskExposition",
    proxy: {
      actionMethods: {
        create: "POST",
        read: "POST",
        update: "POST"
      },
      type: "ajax",
      url: "http://localhost:9000/giro/loadGridDefault.htm",
      reader: {
        type: "json",
        totalProperty: "totalCount",
        root: "data",
        successProperty: "success"
      }
    }
  }
);

Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterOperationalRiskExposition",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterOperationalRiskExposition",
    store: StoreGridPanelRegisterOperationalRiskExposition,
    loadMask: true,
    columnLines: true,
    tbar: [
      {
        text: "Nuevo",
        iconCls: "add",
        action: "newOperationalRiskExposition"
      },
      "-",
      {
        text: "Eliminar",
        iconCls: "delete",
        action: "deleteOperationalRiskExposition"
      },
      "-",
      {
        text: "Consultar",
        iconCls: "replicate",
        handler: function() {
          var view = Ext.create(
            "DukeSource.view.risk.parameter.windows.ViewWindowRiskEvaluationPending",
            {
              modal: true
            }
          );
          view.show();
        }
      },
      "-",
      "->",
      {
        text: "Auditoria",
        action: "operationalRiskExpositionAuditory",
        iconCls: "auditory"
      }
    ],
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridPanelRegisterOperationalRiskExposition,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridOperationalRiskExposition",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findOperationalRiskExposition.htm",
          "state",
          "validity"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;
            if (
              selModel.getSelection()[0].get("idOperationalRiskExposition") ==
              "id"
            ) {
              Ext.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/saveOperationalRiskExposition.htm?nameView=ViewPanelConfigurationGeneralMatrix",
                params: {
                  jsonData: Ext.JSON.encode(record.data)
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      response.mensaje,
                      Ext.Msg.INFO
                    );
                    DukeSource.global.DirtyView.searchPaginationGridNormal(
                      "",
                      grid,
                      grid.down("pagingtoolbar"),
                      "http://localhost:9000/giro/findOperationalRiskExposition.htm",
                      "state",
                      "validity"
                    );
                    Ext.ComponentQuery.query(
                      "ViewGridPanelRegisterConfigurationMatrix"
                    )[0]
                      .down("ViewComboOperationalRiskExposition")
                      .getStore()
                      .load({
                        callback: function() {
                          Ext.ComponentQuery.query(
                            "ViewGridPanelRegisterConfigurationMatrix"
                          )[0]
                            .getStore()
                            .load();
                        }
                      });
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_ERROR,
                      response.mensaje,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function() {}
              });
            } else if (selModel.getSelection()[0].get("validity") == "S") {
              Ext.MessageBox.show({
                title: DukeSource.global.GiroMessages.TITLE_WARNING,
                msg:
                  "Se modificaran el IMPACTO y el NIVEL DE RIESGO, esta seguro de continuar?",
                icon: Ext.Msg.QUESTION,
                buttonText: {
                  yes: "Si"
                },
                buttons: Ext.MessageBox.YESNO,
                fn: function(btn) {
                  if (btn == "yes") {
                    Ext.Ajax.request({
                      method: "POST",
                      url:
                        "http://localhost:9000/giro/saveOperationalRiskExposition.htm?nameView=ViewPanelConfigurationGeneralMatrix",
                      params: {
                        jsonData: Ext.JSON.encode(record.data)
                      },
                      success: function(response) {
                        response = Ext.decode(response.responseText);
                        if (response.success) {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_MESSAGE,
                            response.mensaje,
                            Ext.Msg.INFO
                          );
                          DukeSource.global.DirtyView.searchPaginationGridNormal(
                            "",
                            grid,
                            grid.down("pagingtoolbar"),
                            "http://localhost:9000/giro/findOperationalRiskExposition.htm",
                            "state",
                            "validity"
                          );
                          Ext.ComponentQuery.query(
                            "ViewGridPanelRegisterConfigurationMatrix"
                          )[0]
                            .down("ViewComboOperationalRiskExposition")
                            .getStore()
                            .load({
                              callback: function() {
                                Ext.ComponentQuery.query(
                                  "ViewGridPanelRegisterConfigurationMatrix"
                                )[0]
                                  .getStore()
                                  .load();
                              }
                            });
                        } else {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_ERROR,
                            response.mensaje,
                            Ext.Msg.ERROR
                          );
                        }
                      },
                      failure: function() {}
                    });
                  }
                }
              });
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                "EL REGISTRO NO PUEDE SER MODIFICADO",
                Ext.Msg.ERROR
              );
              grid.getStore().load();
            }
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 25, sortable: false },
        {
          header: "Código",
          align: "center",
          dataIndex: "idOperationalRiskExposition",
          flex: 0.5
        },
        {
          header: "Patrimonio efectivo",
          align: "right",
          xtype: "numbercolumn",
          format: "0,0.00",
          dataIndex: "maxAmount",
          flex: 1,
          editor: {
            xtype: "NumberDecimalNumberObligatory",
            allowBlank: false
          }
        },
        {
          header: "A&ntilde;o",
          align: "center",
          dataIndex: "year",
          flex: 1,
          editor: {
            xtype: "TextFieldNumberFormatObligatory",
            maxLength: 4,
            enforceMaxLength: 4,
            allowBlank: false
          }
        },
        {
          header: "Estado",
          align: "center",
          dataIndex: "validity",
          flex: 0.5,
          renderer: function(value, meta) {
            if (value === "S") {
              return "ACTIVO";
            } else if (value === "N") {
              return "INACTIVO";
            }
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);
