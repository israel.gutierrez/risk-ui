Ext.define("ModelTreeGridPaf", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "idPaf",
    "description",
    "text",
    "codeIntern",
    "codePlace",
    "region",
    "parent",
    "parentId",
    "depth",
    "state"
  ]
});

var storePaf = Ext.create("Ext.data.TreeStore", {
  model: "ModelTreeGridPaf",
  autoLoad: true,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showPafActives.htm",
    extraParams: {
      node: "root",
      depth: 0
    }
  },
  root: {
    id: "root",
    description: "",
    expanded: true
  },
  folderSort: true,
  sorters: [
    {
      property: "description",
      direction: "ASC"
    }
  ]
});

Ext.define("DukeSource.view.risk.parameter.grids.TreeGridPaf", {
  extend: "Ext.tree.Panel",
  alias: "widget.TreeGridPaf",
  store: storePaf,
  mixins: { treeFilter: "TreeFilter" },
  columnLines: true,
  useArrows: true,
  multiSelect: false,
  cls: "customTree-grid",
  singleExpand: false,
  rootVisible: false,
  columns: [
    {
      xtype: "treecolumn",
      text: "Punto de atención financiera",
      flex: 2,
      sortable: true,
      dataIndex: "text"
    }
  ]
});
