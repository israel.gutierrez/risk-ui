var storeActionPlan = Ext.create("Ext.data.Store", {
  fields: ["id", "description"],
  data: [
    { id: "R", description: "RIESGO" },
    { id: "I", description: "INCIDENTE" },
    { id: "E", description: "EVENTO" }
  ]
});
Ext.define("ModelComboIndicatorType", {
  extend: "Ext.data.Model",
  fields: ["idIndicatorType", "description"]
});
var StoreComboIndicatorType = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboIndicatorType",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListIndicatorTypeActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
StoreComboIndicatorType.load();
Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterActionPlan",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterActionPlan",
    store: "risk.parameter.grids.StoreGridPanelRegisterActionPlan",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterActionPlan",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        //            {text: 'EXPORTAR A PDF', action: 'exportActionPlanPdf', iconCls: 'pdf'}, '-',
        //            {text: 'EXPORTAR A EXCEL', action: 'exportActionPlanExcel', iconCls: 'excel'}, '-',
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridActionPlan",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findActionPlan.htm",
          "d.description",
          "d.idDocument"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveActionPlan.htm?nameView=ViewPanelRegisterActionPlan",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findActionPlan.htm",
                    "d.description",
                    "d.idDocument"
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 30, sortable: false },
        {
          header: "CODIGO",
          dataIndex: "idActionPlan",
          flex: 1,
          align: "center"
        },
        {
          header: "CODIGO PLAN  ",
          dataIndex: "codePlan",
          flex: 1.5,
          align: "center"
        },
        {
          header: "TIPO INDICADOR",
          dataIndex: "indicatorType",
          flex: 2,
          align: "center",
          editor: {
            xtype: "ViewComboIndicatorType",
            store: StoreComboIndicatorType,
            allowBlank: false
          },
          renderer: function(val) {
            var indicatorType = Ext.getStore(StoreComboIndicatorType);
            var index = indicatorType.findExact("idIndicatorType", val);
            if (index != -1) {
              var rs = indicatorType.getAt(index).data;
              return rs.description;
            }
          }
        },
        {
          header: "ORIGEN PLAN ACCION",
          dataIndex: "originPlanAction",
          align: "center",
          flex: 2.5,
          editor: {
            xtype: "ViewComboOriginActionPlan",
            store: storeActionPlan,
            allowBlank: false
          },
          renderer: function(val) {
            var indicatorAction = Ext.getStore(storeActionPlan);
            var index = indicatorAction.findExact("id", val);
            if (index != -1) {
              var rs = indicatorAction.getAt(index).data;
              return rs.description;
            }
          }
        },
        {
          header: "DESCRIPCION",
          dataIndex: "description",
          flex: 15,
          editor: {
            xtype: "UpperCaseTextFieldObligatory",
            allowBlank: false
          }
        },
        { header: "ESTADO", dataIndex: "state", flex: 1, align: "center" }
      ];
      this.callParent(arguments);
    }
  }
);
