Ext.define("ModelTreeGridProcessRecursive", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "idProcessRecursive",
    "description",
    "detailDescription",
    "isCritic",
    "text",
    "code",
    "parent",
    "parentId",
    "depth",
    "state"
  ]
});

var storeProcessRecursive = Ext.create("Ext.data.TreeStore", {
  model: "ModelTreeGridProcessRecursive",
  autoLoad: true,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showProcessRecursiveActives.htm",
    extraParams: {
      node: "root",
      depth: 0
    }
  },
  root: {
    id: "root",
    description: "",
    expanded: true
  },
  folderSort: true,
  sorters: [
    {
      property: "description",
      direction: "ASC"
    }
  ]
});

Ext.define("DukeSource.view.risk.parameter.grids.TreeGridProcessRecursive", {
  extend: "Ext.tree.Panel",
  alias: "widget.TreeGridProcessRecursive",
  store: storeProcessRecursive,
  mixins: {
    treeFilter: "TreeFilter"
  },
  columnLines: true,
  useArrows: true,
  cls: "customTree-grid",
  multiSelect: false,
  singleExpand: false,
  rootVisible: false,
  columns: [
    {
      xtype: "treecolumn",
      text: "Procesos",
      flex: 2,
      sortable: true,
      dataIndex: "text"
    }
  ]
});
