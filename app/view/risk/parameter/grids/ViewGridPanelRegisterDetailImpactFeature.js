Ext.define("ModelComboImpact", {
  extend: "Ext.data.Model",
  fields: ["idImpact", "description", "valueMaximo", "percentage"]
});
Ext.define("ModelComboImpactFeature", {
  extend: "Ext.data.Model",
  fields: ["idImpactFeature", "description"]
});

var StoreComboImpactFeature = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboImpactFeature",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListImpactFeatureActivesComboBox.htm",
    extraParams: {
      propertyOrder: "if.description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("ModelGridPanelRegisterDetailImpactFeature", {
  extend: "Ext.data.Model",
  fields: ["idImpact", "idImpactFeature", "description", "alias", "state"]
});

var StoreGridPanelRegisterDetailImpactFeature = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelGridPanelRegisterDetailImpactFeature",
  groupField: "idImpactFeature",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterDetailImpactFeature",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterDetailImpactFeature",
    store: StoreGridPanelRegisterDetailImpactFeature,
    loadMask: true,
    columnLines: true,
    tbar: [
      {
        text: "Nuevo",
        iconCls: "add",
        action: "newDetailImpacfeature"
      },
      "-",
      {
        text: "Eliminar",
        iconCls: "delete",
        action: "deleteDetailImpacfeature"
      },
      "-",
      {
        xtype: "UpperCaseTextField",
        action: "searchDetailImpacfeature",
        fieldLabel: "BUSCAR",
        labelWidth: 60,
        width: 300
      },
      "-",
      "->",
      {
        text: "Auditoria",
        action: "detailImpacfeatureAuditory",
        iconCls: "auditory"
      }
    ],
    features: [
      {
        ftype: "grouping",
        startCollapsed: true
      }
    ],
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridPanelRegisterDetailImpactFeature,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridDetailImpacfeature",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        StoreComboImpact.load({
          callback: function() {
            StoreComboImpactFeature.load({
              callback: function() {
                var panelGeneral = Ext.ComponentQuery.query(
                  "ViewWindowConfigurationMatrix"
                )[0];
                me.store.getProxy().extraParams = {
                  idTypeMatrix: panelGeneral
                    .down("ViewComboTypeMatrix")
                    .getValue(),
                  idOperationalRiskExposition: panelGeneral
                    .down("ViewComboOperationalRiskExposition")
                    .getValue(),
                  propertyFind: "description",
                  valueFind: "",
                  propertyOrder: "description"
                };
                me.store.getProxy().url =
                  "http://localhost:9000/giro/findDetailImpactFeature.htm";
                me.down("pagingtoolbar").moveFirst();
              }
            });
          }
        });
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveDetailImpactFeature.htm?nameView=ViewPanelConfigurationGeneralMatrix",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageNormal(response.message);

                  var panelGeneral = Ext.ComponentQuery.query(
                    "ViewWindowConfigurationMatrix"
                  )[0];
                  grid.store.getProxy().extraParams = {
                    idTypeMatrix: panelGeneral
                      .down("ViewComboTypeMatrix")
                      .getValue(),
                    idOperationalRiskExposition: panelGeneral
                      .down("ViewComboOperationalRiskExposition")
                      .getValue(),
                    propertyFind: "description",
                    valueFind: "",
                    propertyOrder: "description"
                  };
                  grid.store.getProxy().url =
                    "http://localhost:9000/giro/findDetailImpactFeature.htm";
                  grid.down("pagingtoolbar").moveFirst();
                } else {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        {
          xtype: "rownumberer",
          width: 25,
          sortable: false
        },
        {
          header: "Impacto",
          dataIndex: "idImpact",
          flex: 0.5,
          editor: {
            xtype: "ViewComboImpact",
            allowBlank: false,
            msgTarget: "side",
            fieldCls: "obligatoryTextField",
            store: (StoreComboImpact = Ext.create("Ext.data.Store", {
              extend: "Ext.data.Store",
              model: "ModelComboImpact",
              pageSize: 9999,
              autoLoad: false,
              proxy: {
                actionMethods: {
                  create: "POST",
                  read: "POST",
                  update: "POST"
                },
                type: "ajax",
                url: "http://localhost:9000/giro/showListImpactActives.htm",
                extraParams: {
                  idTypeMatrix: Ext.ComponentQuery.query(
                    "ViewWindowConfigurationMatrix"
                  )[0]
                    .down("ViewComboTypeMatrix")
                    .getValue(),
                  idOperationalRiskExposition: Ext.ComponentQuery.query(
                    "ViewWindowConfigurationMatrix"
                  )[0]
                    .down("ViewComboOperationalRiskExposition")
                    .getValue()
                },
                reader: {
                  type: "json",
                  root: "data",
                  successProperty: "success"
                }
              }
            }))
          },
          renderer: function(val) {
            var Impact = Ext.getStore(StoreComboImpact);
            var index = Impact.findExact("idImpact", val);
            if (index !== -1) {
              var rs = Impact.getAt(index).data;
              return rs.description;
            }
          }
        },
        {
          header: "Criterio",
          dataIndex: "idImpactFeature",
          flex: 1,
          editor: {
            xtype: "ViewComboImpactFeature",
            allowBlank: false,
            msgTarget: "side",
            fieldCls: "obligatoryTextField",
            store: (StoreComboImpactFeature = Ext.create("Ext.data.Store", {
              extend: "Ext.data.Store",
              model: "ModelComboImpactFeature",
              pageSize: 9999,
              autoLoad: false,
              proxy: {
                actionMethods: {
                  create: "POST",
                  read: "POST",
                  update: "POST"
                },
                type: "ajax",
                url:
                  "http://localhost:9000/giro/showListImpactFeatureActivesComboBox.htm",
                extraParams: {
                  idTypeMatrix: Ext.ComponentQuery.query(
                    "ViewWindowConfigurationMatrix"
                  )[0]
                    .down("ViewComboTypeMatrix")
                    .getValue(),
                  idOperationalRiskExposition: Ext.ComponentQuery.query(
                    "ViewWindowConfigurationMatrix"
                  )[0]
                    .down("ViewComboOperationalRiskExposition")
                    .getValue(),
                  propertyOrder: "if.description"
                },
                reader: {
                  type: "json",
                  root: "data",
                  successProperty: "success"
                }
              }
            }))
          },
          renderer: function(val) {
            var ImpactFeature = Ext.getStore(StoreComboImpactFeature);
            var index = ImpactFeature.findExact("idImpactFeature", val);
            if (index !== -1) {
              var rs = ImpactFeature.getAt(index).data;
              return rs.description;
            }
          }
        },
        {
          header: "Detalle",
          dataIndex: "description",
          flex: 2,
          editor: {
            xtype: "UpperCaseTextFieldObligatory",
            allowBlank: false
          }
        },
        {
          header: "Alias",
          dataIndex: "alias",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextFieldObligatory",
            allowBlank: false,
            maxLength: 300
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);
