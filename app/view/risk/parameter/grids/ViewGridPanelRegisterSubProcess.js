Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterSubProcess",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterSubProcess",
    store: "risk.parameter.grids.StoreGridPanelRegisterSubProcess",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterSubProcess",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridSubProcess",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/showListSubProcessActivesComboBox.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        { header: "CODIGO", dataIndex: "idSubProcess", flex: 1 },
        { header: "PROCESO", dataIndex: "nameProcess", flex: 2 },
        { header: "SUBPROCESO", dataIndex: "description", flex: 3 },
        { header: "ABREVIATURA", dataIndex: "abbreviation", flex: 1 },
        { header: "ESTADO", dataIndex: "state", width: 60 }
      ];
      this.callParent(arguments);
    }
  }
);
