Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterProcess",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterProcess",
    store: "risk.parameter.grids.StoreGridPanelRegisterProcess",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterProcess",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridProcess",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/showListProcessActives.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        { header: "CODIGO", dataIndex: "idProcess", flex: 1 },
        { header: "TIPO PROCESO", dataIndex: "nameProcessType", flex: 1 },
        { header: "PROCESO", dataIndex: "description", flex: 4 },
        { header: "ABREVIATURA", dataIndex: "abbreviation", flex: 1 },
        { header: "ESTADO", dataIndex: "state", width: 60 }
      ];
      this.callParent(arguments);
    }
  }
);
