Ext.define("DukeSource.view.risk.parameter.grids.ViewGridPanelTitleEvents", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridPanelTitleEvents",
  store: "risk.parameter.grids.StoreGridPanelRegisterTitleEvents",
  loadMask: true,
  columnLines: true,
  bbar: {
    xtype: "pagingtoolbar",
    pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
    store: "risk.parameter.grids.StoreGridPanelRegisterTitleEvents",
    displayInfo: true,
    displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
    emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
  },
  listeners: {
    render: function() {
      var me = this;
      DukeSource.global.DirtyView.searchPaginationGridNormal(
        "",
        me,
        me.down("pagingtoolbar"),
        "http://localhost:9000/giro/showListEventTittleActives.htm",
        "description",
        "description"
      );
    }
  },
  initComponent: function() {
    this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
      clicksToMoveEditor: 1,
      saveBtnText: "GUARDAR",
      cancelBtnText: "CANCELAR",
      autoCancel: false,
      completeEdit: function() {
        var me = this;
        var grid = me.grid;
        var selModel = grid.getSelectionModel();
        var record = selModel.getLastSelected();
        if (me.editing && me.validateEdit()) {
          me.editing = false;
          Ext.Ajax.request({
            method: "POST",
            url:
              "http://localhost:9000/giro/saveEventTittle.htm?nameView=ViewPanelRegisterTitleEvents",
            params: {
              jsonData: Ext.JSON.encode(record.data)
            },
            success: function(response) {
              response = Ext.decode(response.responseText);
              if (response.success) {
                DukeSource.global.DirtyView.messageNormal(response.message);
                grid.down("pagingtoolbar").doRefresh();
              } else {
                DukeSource.global.DirtyView.messageWarning(response.message);
              }
            },
            failure: function() {}
          });
          me.fireEvent("edit", me, me.context);
        }
      }
    });
    this.columns = [
      {
        xtype: "rownumberer",
        width: 30,
        sortable: false
      },
      {
        header: "Descripci&oacute;n",
        dataIndex: "description",
        flex: 1,
        editor: {
          xtype: "UpperCaseTextFieldObligatory",
          allowBlank: false
        }
      },
      {
        header: "Abreviatura",
        align: "center",
        dataIndex: "abbreviation",
        width: 150,
        editor: {
          xtype: "UpperCaseTextFieldObligatory",
          allowBlank: false
        }
      }
    ];
    this.callParent(arguments);
  }
});
