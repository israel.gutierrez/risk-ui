Ext.define('DukeSource.view.risk.parameter.grids.ViewGridPanelRankingWeightEvent', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ViewGridPanelRankingWeightEvent',
    store: 'risk.parameter.grids.StoreGridPanelRegisterWeightEvent',
    loadMask: true,
    columnLines: true,
    features: [
        {
            ftype: 'summary'
        }
    ],

    initComponent: function () {
        this.columns = [
            {header: 'idEventOne', dataIndex: 'idEventOne', width: 25}
            , {header: 'EVENTO DE RIESGO OPERACIONAL', dataIndex: 'nameEventOne', flex: 4}
            , {header: "PESO", dataIndex: 'weight', flex: 1, summaryType: "sum"}
            , {text: "Evento", dataIndex: 'score', flex: 1}
            , {header: 'PODERADO', dataIndex: 'weighting', flex: 1}
            , {header: 'yearMonth', hidden: true, dataIndex: 'yearMonth', flex: 1}
            , {header: 'year', hidden: true, dataIndex: 'year', flex: 1}
            , {header: 'month', hidden: true, dataIndex: 'month', flex: 1}


        ];

        this.callParent(arguments);
    }
});
