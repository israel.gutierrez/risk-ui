Ext.define("ModelGridPanelRegisterGroupIncidents", {
  extend: "Ext.data.Model",
  fields: ["idIncidentGroup", "description", "state"]
});

var storeGridPanelRegisterGroupIncidents = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelGridPanelRegisterGroupIncidents",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListIncidentGroupActives.htm",
    params: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterGroupIncidents",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterGroupIncidents",
    store: storeGridPanelRegisterGroupIncidents,
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: storeGridPanelRegisterGroupIncidents,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridGroupIncidents",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/showListIncidentGroupActives.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;
            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveIncidentGroup.htm?nameView=ViewPanelRegisterGroupIncidents",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/showListIncidentGroupActives.htm",
                    "description",
                    "description"
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        {
          header: "CODIGO",
          dataIndex: "idIncidentGroup",
          flex: 1,
          style: "background: #D8D8D8"
        },
        {
          header: "DESCRIPTION",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "ESTADO",
          dataIndex: "state",
          flex: 1,
          style: "background: #D8D8D8"
        }
      ];
      this.callParent(arguments);
    }
  }
);
