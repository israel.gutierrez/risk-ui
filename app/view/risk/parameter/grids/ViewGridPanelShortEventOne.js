Ext.define("DukeSource.view.risk.parameter.grids.ViewGridPanelShortEventOne", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridPanelShortEventOne",
  store: "risk.parameter.grids.StoreGridPanelRegisterEventOne",
  loadMask: true,
  columnLines: true,
  title: "Evento Riesgo Operacional",
  bbar: {
    xtype: "pagingtoolbar",
    pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
    store: "risk.parameter.grids.StoreGridPanelRegisterEventOne",
    displayInfo: false
  },
  listeners: {
    render: function() {
      var me = this;
      DukeSource.global.DirtyView.searchPaginationGridNormal(
        "",
        me,
        me.down("pagingtoolbar"),
        "http://localhost:9000/giro/findAllEventOne.htm",
        "description",
        "description"
      );
    }
  },
  initComponent: function() {
    this.columns = [
      {
        header: "Cod",
        dataIndex: "idEventOne",
        width: 25,
        align: "center",
        sortable: false
      },
      { header: "DESCRIPCION", dataIndex: "description", flex: 4 },
      {
        header: "Niv.",
        align: "center",
        dataIndex: "legend",
        sortable: false,
        width: 35
      }
    ];
    this.callParent(arguments);
  }
});
