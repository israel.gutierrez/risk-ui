Ext.define('DukeSource.view.risk.parameter.grids.ViewTreeGridPanelRegisterEvent', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.ViewTreeGridPanelRegisterEvent',
    store: 'risk.parameter.grids.StoreTreeGridPanelRegisterEvent',
    useArrows: true,
    multiSelect: true,
    singleExpand: true,
    rootVisible: false,
    columns: [
        {
            xtype: 'treecolumn',
            text: 'Tipo de evento Nivel 1 -> Nivel 2 -> Nivel 3',
            flex: 2,
            sortable: true,
            dataIndex: 'text'
        }

    ]
});
