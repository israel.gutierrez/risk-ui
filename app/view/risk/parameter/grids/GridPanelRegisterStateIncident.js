Ext.require(["Ext.ux.ColorField"]);
Ext.define("ModelStateIncident", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "typeIncident",
    "descriptionTypeIncident",
    "colorIncident",
    "sequence",
    "abbreviation",
    "description",
    "state"
  ]
});

var storeStateIncident = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelStateIncident",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.grids.GridPanelRegisterStateIncident",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.GridPanelRegisterStateIncident",
    store: storeStateIncident,
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: storeStateIncident,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findStateIncident.htm",
          "si.description",
          "si.typeIncident"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveStateIncident.htm?nameView=PanelRegisterStateIncident",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  grid.down("pagingtoolbar").moveFirst();
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        { header: "CODIGO", dataIndex: "id", width: 80 },
        {
          header: "TIPO",
          dataIndex: "typeIncident",
          width: 250,
          editor: {
            xtype: "combobox",
            editable: false,
            allowBlank: false,
            fieldCls: "obligatoryTextField",
            queryMode: "local",
            displayField: "description",
            valueField: "value",
            store: {
              fields: ["value", "description"],
              pageSize: 9999,
              autoLoad: true,
              proxy: {
                actionMethods: {
                  create: "POST",
                  read: "POST",
                  update: "POST"
                },
                type: "ajax",
                url:
                  "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                extraParams: {
                  propertyOrder: "value",
                  propertyFind: "identified",
                  valueFind: "TYPEINCIDENT"
                },
                reader: {
                  type: "json",
                  root: "data",
                  successProperty: "success"
                }
              }
            }
          }
        },
        {
          header: "DESCRIPCION",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextFieldObligatory",
            maskRe: /[a-zA-Z ]/,
            allowBlank: false
          }
        },
        {
          header: "COLOR",
          align: "center",
          dataIndex: "colorIncident",
          flex: 1,
          editor: {
            xtype: "colorfield",
            allowBlank: false
          },
          renderer: function(value) {
            return (
              '<div  style="background-color:#' +
              value +
              "; color:#" +
              value +
              ';"> ' +
              value +
              "</div>"
            );
          }
        },
        {
          header: "SECUENCIA",
          dataIndex: "sequence",
          flex: 1,
          editor: {
            xtype: "numberfield",
            allowBlank: false
          }
        },
        { header: "ESTADO", dataIndex: "state", width: 80 }
      ];
      this.callParent(arguments);
    }
  }
);
