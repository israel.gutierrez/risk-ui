Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterAccountPlan",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterAccountPlan",
    store: "risk.parameter.grids.StoreGridPanelRegisterAccountPlan",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterAccountPlan",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridAccountPlan",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findAccountPlan.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveAccountPlan.htm?nameView=ViewPanelRegisterAccountPlan",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageNormal(response.message);
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findAccountPlan.htm",
                    "description",
                    "description"
                  );
                } else {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        {
          header: "CODIGO",
          dataIndex: "idAccountPlan",
          flex: 1,
          hidden: true,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "Codigo cuenta",
          dataIndex: "codeAccount",
          width: 150,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "Cuenta de orden",
          dataIndex: "orderAccount",
          width: 150,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: true
          }
        },
        {
          header: "Descripcion",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "Moneda",
          dataIndex: "currency",
          width: 90,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "ESTADO",
          dataIndex: "state",
          flex: 1,
          hidden: true,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);
