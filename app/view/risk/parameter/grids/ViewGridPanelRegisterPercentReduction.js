Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterPercentReduction",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterPercentReduction",
    store: "risk.parameter.grids.StoreGridPanelRegisterPercentReduction",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterPercentReduction",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridPercentReduction",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "F",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findPercentReduction.htm",
          "typePercent",
          "idPercentReduction"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/savePercentReduction.htm",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findPercentReduction.htm",
                    "idPercentReduction",
                    "idPercentReduction"
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        {
          header: "CODIGO",
          dataIndex: "idPercentReduction",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "VALOR PORCENTUAL",
          dataIndex: "valuePercent",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "ESTADO",
          dataIndex: "state",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "TIPO PORCENTAJE",
          dataIndex: "typePercent",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);
