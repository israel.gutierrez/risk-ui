Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewTreeGridPanelRegisterWorkArea",
  {
    extend: "Ext.tree.Panel",
    alias: "widget.ViewTreeGridPanelRegisterWorkArea",
    store: "risk.parameter.grids.StoreTreeGridPanelRegisterWorkArea",
    mixins: {
      treeFilter: "DukeSource.global.TreeFilter"
    },
    useArrows: true,
    singleExpand: false,
    rootVisible: false,
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        viewConfig: {
          plugins: {
            ptype: "treeviewdragdrop"
          },
          listeners: {
            beforedrop: function(
              node,
              data,
              overModel,
              dropPosition,
              dropHandlers
            ) {
              dropHandlers.wait = true;
              data.records[0].data["parent"] = overModel.internalId;
              data.records[0].data["parentId"] = overModel.internalId;
              Ext.MessageBox.confirm({
                title: DukeSource.global.GiroMessages.TITLE_WARNING,
                msg: "Estas seguro que desea continuar?",
                icon: Ext.Msg.WARNING,
                buttonText: {
                  yes: "Si",
                  no: "No"
                },
                buttons: Ext.MessageBox.YESNO,
                fn: function(btn) {
                  if (btn === "yes") {
                    DukeSource.lib.Ajax.request({
                      method: "POST",
                      url: "http://localhost:9000/giro/saveWorkArea.htm",
                      params: {
                        jsonData: Ext.JSON.encode(data.records[0].data)
                      },
                      success: function(response) {
                        response = Ext.decode(response.responseText);
                        if (response.success) {
                          dropHandlers.processDrop();
                        } else {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_WARNING,
                            response.mensaje,
                            Ext.Msg.ERROR
                          );
                        }
                      },
                      failure: function() {
                        DukeSource.global.DirtyView.messageAlert(
                          DukeSource.global.GiroMessages.TITLE_WARNING,
                          response.mensaje,
                          Ext.Msg.ERROR
                        );
                      }
                    });
                  } else {
                    dropHandlers.cancelDrop();
                  }
                }
              });
            }
          }
        },
        dockedItems: [
          {
            xtype: "toolbar",
            dock: "top",
            items: [
              {
                text: "Nuevo",
                iconCls: "add",
                type: "O",
                action: "newWorkArea"
              },
              "-",
              {
                text: "Modificar",
                itemId: "modifyTreeWorkArea",
                type: "O",
                disabled: true,
                action: "modifyWorkArea",
                iconCls: "modify"
              },
              "-",
              {
                text: "Eliminar",
                itemId: "deleteTreeWorkArea",
                type: "O",
                disabled: true,
                action: "deleteWorkArea",
                iconCls: "delete"
              },
              "-",
              {
                text: "Aprobadores",
                itemId: "authorizedWorkArea",
                disabled: true,
                type: "O",
                hidden: hidden("PARAM_Tree_AuthorizedWorkArea"),
                action: "authorizedWorkArea",
                iconCls: "users"
              }
            ]
          },
          {
            xtype: "toolbar",
            dock: "top",
            items: [
              {
                xtype: "textfield",
                itemId: "searchOption",
                name: "searchOption",
                fieldLabel: "Buscar",
                labelWidth: 60,
                width: 250,
                emptyText: "Buscar",
                enableKeyEvents: true,
                listeners: {
                  afterrender: function(e) {
                    e.focus(false, 1000);
                  },
                  keyup: function(e, t) {
                    me.expandAll(me);
                    me.filterByText(e.getValue());
                    if (e.getValue() === "") {
                      me.collapseAll(me);
                    }
                  }
                }
              }
            ]
          }
        ],
        columns: [
          {
            xtype: "treecolumn",
            text: "Areas de la empresa",
            flex: 2,
            sortable: true,
            dataIndex: "text"
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
