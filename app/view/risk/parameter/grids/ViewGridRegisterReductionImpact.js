Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridRegisterReductionImpact",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridRegisterReductionImpact",
    store: "risk.parameter.grids.StoreGridPanelRegisterPercentReduction",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterPercentReduction",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        //            {text:'EXPORTAR A PDF',action:'exportPercentReductionImpactPdf',iconCls:'pdf'},'-',
        //            {text:'EXPORTAR A EXCEL',action:'exportPercentReductionImpactExcel',iconCls:'excel'},'-',
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridPercentReductionImpact",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "I",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findPercentReduction.htm",
          "typePercent",
          "idPercentReduction"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/savePercentReduction.htm",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "I",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findPercentReduction.htm",
                    "typePercent",
                    "idPercentReduction"
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 25, sortable: false },
        {
          header: "CODIGO",
          align: "center",
          dataIndex: "idPercentReduction",
          flex: 1
        },
        {
          header: "VALOR PORCENTUAL",
          align: "center",
          dataIndex: "valuePercent",
          flex: 3,
          editor: {
            xtype: "UpperCaseTextFieldObligatory"
          }
        },
        { header: "ESTADO", align: "center", dataIndex: "state", flex: 1 },
        {
          header: "TIPO PORCENTAJE",
          align: "center",
          dataIndex: "typePercent",
          flex: 1
        }
      ];
      this.callParent(arguments);
    }
  }
);
