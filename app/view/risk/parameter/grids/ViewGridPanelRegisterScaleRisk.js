Ext.require(["Ext.ux.ColorField"]);
Ext.define("ModelGridPanelRegisterScaleRisk", {
  extend: "Ext.data.Model",
  fields: [
    "idScaleRisk",
    "description",
    "operationalRiskExposition",
    "idTypeMatrix",
    "equivalentValue",
    { name: "percentageExposition", type: "number" },
    "state",
    { name: "rangeInf", type: "number" },
    { name: "rangeSup", type: "number" },
    "levelColour",
    { name: "businessLineOne", type: "number" },
    { name: "operationalRiskExposition", type: "number" },
    { name: "valueOperationalRiskExposition", type: "number" }
  ]
});

var StoreGridPanelRegisterScaleRisk = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelGridPanelRegisterScaleRisk",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterScaleRisk",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterScaleRisk",
    store: StoreGridPanelRegisterScaleRisk,
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridPanelRegisterScaleRisk,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var windowConfiguration = Ext.ComponentQuery.query(
          "ViewWindowConfigurationMatrix"
        )[0];
        var me = this;
        me.store.getProxy().extraParams = {
          idTypeMatrix: windowConfiguration
            .down("ViewComboTypeMatrix")
            .getValue(),
          idOperationalRiskExposition: windowConfiguration
            .down("ViewComboOperationalRiskExposition")
            .getValue()
        };
        me.store.getProxy().url =
          "http://localhost:9000/giro/findInitialScaleRisk.htm";
        me.down("pagingtoolbar").moveFirst();
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;
            var windowConfiguration = Ext.ComponentQuery.query(
              "ViewWindowConfigurationMatrix"
            )[0];

            if (windowConfiguration.stateConfig == "P") {
              Ext.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/saveScaleRisk.htm?nameView=ViewPanelConfigurationGeneralMatrix",
                params: {
                  jsonData: Ext.JSON.encode(record.data),
                  idTypeMatrix: windowConfiguration
                    .down("ViewComboTypeMatrix")
                    .getValue(),
                  idOperationalRiskExposition: windowConfiguration
                    .down("ViewComboOperationalRiskExposition")
                    .getValue()
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      response.mensaje,
                      Ext.Msg.INFO
                    );
                    grid.store.getProxy().extraParams = {
                      idTypeMatrix: windowConfiguration
                        .down("ViewComboTypeMatrix")
                        .getValue(),
                      idOperationalRiskExposition: windowConfiguration
                        .down("ViewComboOperationalRiskExposition")
                        .getValue()
                    };
                    grid.store.getProxy().url =
                      "http://localhost:9000/giro/findInitialScaleRisk.htm";
                    grid.down("pagingtoolbar").moveFirst();
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_ERROR,
                      response.mensaje,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function() {}
              });
              me.fireEvent("edit", me, me.context);
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                "Para modificar, debe ELIMINAR la Matriz generada",
                Ext.Msg.WARNING
              );
            }
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 25, sortable: false },
        {
          header: "C&oacute;digo",
          align: "center",
          dataIndex: "equivalentValue",
          width: 70
        },
        {
          header: "Nombre",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextFieldObligatory",
            allowBlank: false
          }
        },
        {
          header: "% Maxima exposici&oacute;n",
          dataIndex: "percentageExposition",
          align: "center",
          flex: 1,
          xtype: "numbercolumn",
          format: "0,0.0000",
          editor: {
            xtype: "NumberDecimalNumberObligatory",
            maxValue: 100,
            value: 0,
            allowBlank: true
          }
        },
        {
          header: "P&eacute;rdida inferior",
          align: "right",
          dataIndex: "rangeInf",
          flex: 1,
          xtype: "numbercolumn",
          format: "0,0.00"
        },
        {
          header: "P&eacute;rdida superior",
          align: "right",
          dataIndex: "rangeSup",
          flex: 1,
          xtype: "numbercolumn",
          format: "0,0.00",
          editor: {
            xtype: "NumberDecimalNumberObligatory",
            allowBlank: false
          }
        },
        {
          header: "Color",
          align: "center",
          dataIndex: "levelColour",
          flex: 1,
          editor: {
            xtype: "colorfield",
            allowBlank: false
          },
          renderer: function(value) {
            return (
              '<div  style="background-color:#' +
              value +
              "; color:#" +
              value +
              ';"> ' +
              value +
              "</div>"
            );
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);
