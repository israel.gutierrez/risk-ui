Ext.define("ModelTreeGridOperation", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "idOperation",
    "description",
    "text",
    "codeEntitySupervisor",
    "parent",
    "parentId",
    "depth",
    "state"
  ]
});

var storeOperation = Ext.create("Ext.data.TreeStore", {
  model: "ModelTreeGridOperation",
  autoLoad: true,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showOperationActives.htm",
    extraParams: {
      node: "root",
      depth: 0
    }
  },
  root: {
    id: "root",
    description: "",
    expanded: true
  },
  folderSort: true,
  sorters: [
    {
      property: "description",
      direction: "ASC"
    }
  ]
});

Ext.define("DukeSource.view.risk.parameter.grids.TreeGridOperations", {
  extend: "Ext.tree.Panel",
  alias: "widget.TreeGridOperations",
  store: storeOperation,
  mixins: { treeFilter: "TreeFilter" },
  columnLines: true,
  useArrows: true,
  multiSelect: false,
  cls: "customTree-grid",
  singleExpand: false,
  rootVisible: false,
  columns: [
    {
      xtype: "treecolumn",
      text: "OPERACION",
      flex: 2,
      sortable: true,
      dataIndex: "text"
    }
  ]
});
