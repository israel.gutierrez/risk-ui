//Ext.define('ModelComboBusinessLineOneTypeMatrix',{
//    extend: 'Ext.data.Model',
//    fields: [
//        'idBusinessLineOne',
//        'description'
//    ]
//});
//
//var StoreComboBusinessLineOneTypeMatrix =Ext.create('Ext.data.Store',{
//    extend: 'Ext.data.Store',
//    model: 'ModelComboBusinessLineOneTypeMatrix',
//    pageSize: 9999,
//    proxy: {
//        actionMethods: {
//            create: 'POST',
//            read: 'POST',
//            update: 'POST'
//        },
//        type: 'ajax',
//        url: 'http://localhost:9000/giro/showListBusinessLineOneActives.htm',
//        extraParams: {
//            propertyOrder: 'description'
//        },
//        reader: {
//            type: 'json',
//            root: 'data',
//            successProperty: 'success'
//        }
//    }
//});
//StoreComboBusinessLineOneTypeMatrix.load();
var rowEdit = Ext.define("ModelGridPanelRegisterTypeMatrix", {
  extend: "Ext.data.Model",
  fields: [
    "idTypeMatrix",
    "idBusinessLineOne",
    "descriptionBusinessLine",
    "description",
    "state"
  ]
});

var StoreGridPanelRegisterTypeMatrix = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelGridPanelRegisterTypeMatrix",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterTypeMatrix",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterTypeMatrix",
    store: StoreGridPanelRegisterTypeMatrix,
    loadMask: true,
    columnLines: true,
    tbar: [
      {
        text: "Nuevo",
        iconCls: "add",
        action: "newTypeMatrix"
      },
      "-",
      {
        text: "Eliminar",
        iconCls: "delete",
        action: "deleteTypeMatrix"
      },
      "-",
      {
        xtype: "UpperCaseTextField",
        action: "searchTypeMatrix",
        fieldLabel: "Buscar",
        labelWidth: 60,
        width: 300
      },
      "-",
      "->",
      {
        text: "Auditoria",
        action: "typeMatrixAuditory",
        iconCls: "auditory"
      }
    ],
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridPanelRegisterTypeMatrix,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "Filtrar",
          action: "searchTriggerGridTypeMatrix",
          labelWidth: 60,
          width: 200
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findTypeMatrix.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.columns = [
        {
          xtype: "rownumberer",
          width: 25,
          sortable: false
        },
        {
          header: "Código",
          align: "center",
          dataIndex: "idTypeMatrix",
          width: 70
        },
        {
          header: "Nombre",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextFieldObligatory",
            allowBlank: false
          }
        },
        {
          header: "Línea de negocio",
          dataIndex: "descriptionBusinessLine",
          align: "center",
          flex: 1
        },
        {
          header: "Estado",
          dataIndex: "state",
          align: "center",
          width: 70
        }
      ];
      this.callParent(arguments);
    }
  }
);
