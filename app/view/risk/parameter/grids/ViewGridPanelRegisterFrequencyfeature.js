Ext.define("ModelGridPanelRegisterFrequencyfeature", {
  extend: "Ext.data.Model",
  fields: ["idFrequencyFeature", "description", "state", "type"]
});

var StoreGridPanelRegisterFrequencyfeature = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelGridPanelRegisterFrequencyfeature",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterFrequencyfeature",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterFrequencyfeature",
    store: StoreGridPanelRegisterFrequencyfeature,
    loadMask: true,
    columnLines: true,
    tbar: [
      {
        text: "Nuevo",
        iconCls: "add",
        action: "newFrequencyfeature"
      },
      "-",
      {
        text: "Eliminar",
        iconCls: "delete",
        action: "deleteFrequencyfeature"
      },
      "-",
      {
        xtype: "UpperCaseTextField",
        action: "searchFrequencyfeature",
        fieldLabel: "Buscar",
        labelWidth: 60,
        width: 300
      },
      "-",
      "->",
      {
        text: "Auditoria",
        action: "frequencyfeatureAuditory",
        iconCls: "auditory"
      }
    ],
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridPanelRegisterFrequencyfeature,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridFrequencyfeature",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var panelGeneral = Ext.ComponentQuery.query(
          "ViewWindowConfigurationMatrix"
        )[0];
        var me = this;
        me.store.getProxy().extraParams = {
          idTypeMatrix: panelGeneral.down("ViewComboTypeMatrix").getValue(),
          idOperationalRiskExposition: panelGeneral
            .down("ViewComboOperationalRiskExposition")
            .getValue(),
          propertyFind: "description",
          valueFind: "",
          propertyOrder: "description"
        };
        me.store.getProxy().url =
          "http://localhost:9000/giro/findFrequencyFeature.htm";
        me.down("pagingtoolbar").moveFirst();

        //     		var me = this;
        //             DukeSource.global.DirtyView.searchPaginationGridNormal('',me, me.down('pagingtoolbar'),'http://localhost:9000/giro/findFrequencyFeature.htm','description','description')
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;
            var windowConfiguration = Ext.ComponentQuery.query(
              "ViewWindowConfigurationMatrix"
            )[0];
            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveFrequencyFeature.htm?nameView=ViewPanelConfigurationGeneralMatrix",
              params: {
                jsonData: Ext.JSON.encode(record.data),
                idTypeMatrix: windowConfiguration
                  .down("ViewComboTypeMatrix")
                  .getValue(),
                idOperationalRiskExposition: windowConfiguration
                  .down("ViewComboOperationalRiskExposition")
                  .getValue()
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  var panelGeneral = Ext.ComponentQuery.query(
                    "ViewWindowConfigurationMatrix"
                  )[0];
                  grid.store.getProxy().extraParams = {
                    idTypeMatrix: panelGeneral
                      .down("ViewComboTypeMatrix")
                      .getValue(),
                    idOperationalRiskExposition: panelGeneral
                      .down("ViewComboOperationalRiskExposition")
                      .getValue(),
                    propertyFind: "description",
                    valueFind: "",
                    propertyOrder: "description"
                  };
                  grid.store.getProxy().url =
                    "http://localhost:9000/giro/findFrequencyFeature.htm";
                  grid.down("pagingtoolbar").moveFirst();
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 25, sortable: false },
        { header: "Código", dataIndex: "idFrequencyFeature", width: 100 },
        {
          header: "Descripion",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextFieldObligatory"
          }
        },
        {
          header: "Tipo",
          dataIndex: "type",
          width: 200,
          editor: {
            xtype: "combobox",
            selectOnTab: true,
            allowBlank: false,
            store: ["Cualitativo", "Cuantitativo"],
            lazyRender: true,
            listClass: "x-combo-list-small"
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);
