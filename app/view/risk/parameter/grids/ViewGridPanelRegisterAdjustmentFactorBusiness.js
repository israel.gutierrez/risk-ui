Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterAdjustmentFactorBusiness",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterAdjustmentFactorBusiness",
    store:
      "risk.parameter.grids.StoreGridPanelRegisterAdjustmentFactorBusiness",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store:
        "risk.parameter.grids.StoreGridPanelRegisterAdjustmentFactorBusiness",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridAdjustmentFactorBusiness",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findAdjustmentFactorBusiness.htm",
          "state",
          "state"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveAdjustmentFactorBusiness.htm?nameView=ViewPanelRegisterAdjustmentFactorBusiness",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findAdjustmentFactorBusiness.htm",
                    "state",
                    "state"
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 25, sortable: false },
        { header: "CODIGO", dataIndex: "idAdjustmentFactorBusiness", flex: 1 },
        {
          header: "TITULO REPORTE",
          dataIndex: "titleReport",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "GRUPO REPORTE",
          dataIndex: "groupReport",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "FACTOR FIJO",
          dataIndex: "fixedFactor",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "FACTOR AJUSTE",
          dataIndex: "adjustedFactor",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "FIJO * AJUSTE",
          dataIndex: "fixedAdjusted",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "FECHA INICIO VIGENCIA",
          dataIndex: "dateStartValidity",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
            allowBlank: false
          }
        },
        {
          header: "FECHA LIMITE VIGENCIA",
          dataIndex: "dateEndValidity",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
            allowBlank: false
          }
        },

        { header: "STATE", dataIndex: "state", flex: 1 }
      ];
      this.callParent(arguments);
    }
  }
);
