Ext.define("ModelGridPanelRegisterImpacfeature", {
  extend: "Ext.data.Model",
  fields: ["idImpactFeature", "description", "state", "type"]
});

var StoreGridPanelRegisterImpacfeature = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelGridPanelRegisterImpacfeature",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterImpacfeature",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterImpacfeature",
    store: StoreGridPanelRegisterImpacfeature,
    loadMask: true,
    columnLines: true,
    tbar: [
      {
        text: "Nuevo",
        iconCls: "add",
        action: "newImpacfeature"
      },
      "-",
      {
        text: "Eliminar",
        iconCls: "delete",
        action: "deleteImpacfeature"
      },
      "-",
      {
        xtype: "UpperCaseTextField",
        action: "searchImpacfeature",
        fieldLabel: "Buscar",
        labelWidth: 60,
        width: 300
      },
      "-",
      "->",
      {
        text: "Auditoria",
        action: "impacfeatureAuditory",
        iconCls: "auditory"
      }
    ],
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridPanelRegisterImpacfeature,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridImpacfeature",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var panelGeneral = Ext.ComponentQuery.query(
          "ViewWindowConfigurationMatrix"
        )[0];
        var me = this;
        me.store.getProxy().extraParams = {
          idTypeMatrix: panelGeneral.down("ViewComboTypeMatrix").getValue(),
          idOperationalRiskExposition: panelGeneral
            .down("ViewComboOperationalRiskExposition")
            .getValue(),
          propertyFind: "if.description",
          valueFind: "",
          propertyOrder: "if.description"
        };
        me.store.getProxy().url =
          "http://localhost:9000/giro/findImpactFeature.htm";
        me.down("pagingtoolbar").moveFirst();
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;
            var windowConfiguration = Ext.ComponentQuery.query(
              "ViewWindowConfigurationMatrix"
            )[0];
            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveImpactFeature.htm?nameView=ViewPanelConfigurationGeneralMatrix",
              params: {
                jsonData: Ext.JSON.encode(record.data),
                idTypeMatrix: windowConfiguration
                  .down("ViewComboTypeMatrix")
                  .getValue(),
                idOperationalRiskExposition: windowConfiguration
                  .down("ViewComboOperationalRiskExposition")
                  .getValue()
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  var panelGeneral = Ext.ComponentQuery.query(
                    "ViewWindowConfigurationMatrix"
                  )[0];
                  grid.store.getProxy().extraParams = {
                    idTypeMatrix: panelGeneral
                      .down("ViewComboTypeMatrix")
                      .getValue(),
                    idOperationalRiskExposition: panelGeneral
                      .down("ViewComboOperationalRiskExposition")
                      .getValue(),
                    propertyFind: "if.description",
                    valueFind: "",
                    propertyOrder: "if.description"
                  };
                  grid.store.getProxy().url =
                    "http://localhost:9000/giro/findImpactFeature.htm";
                  grid.down("pagingtoolbar").moveFirst();
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 25, sortable: false },
        { header: "Código", dataIndex: "idImpactFeature", width: 100 },
        {
          header: "Descripción",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextFieldObligatory"
          }
        },
        {
          header: "Tipo",
          dataIndex: "type",
          width: 200,
          editor: {
            xtype: "combobox",
            selectOnTab: true,
            allowBlank: false,
            store: ["Cualitativo", "Cuantitativo"],
            lazyRender: true,
            listClass: "x-combo-list-small"
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);
