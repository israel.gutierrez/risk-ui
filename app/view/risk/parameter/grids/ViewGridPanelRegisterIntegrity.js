Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterIntegrity",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterIntegrity",
    store: "risk.parameter.grids.StoreGridPanelRegisterIntegrity",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterIntegrity",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridIntegrity",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findIntegrity.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveIntegrity.htm?nameView=ViewPanelRegisterIntegrity",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findIntegrity.htm",
                    "description",
                    "description"
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        { header: "CODIGO", dataIndex: "idIntegrity", width: 80 },
        {
          header: "DESCRIPCION",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextFieldObligatory",
            maskRe: /[a-zA-Z ]/,
            allowBlank: false
          }
        },
        { header: "ESTADO", dataIndex: "state", width: 80 }
      ];
      this.callParent(arguments);
    }
  }
);
