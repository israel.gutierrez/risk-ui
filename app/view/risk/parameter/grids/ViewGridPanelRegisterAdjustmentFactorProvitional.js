Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterAdjustmentFactorProvitional",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterAdjustmentFactorProvitional",
    store:
      "risk.parameter.grids.StoreGridPanelRegisterAdjustmentFactorProvitional",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store:
        "risk.parameter.grids.StoreGridPanelRegisterAdjustmentFactorProvitional",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridAdjustmentFactorProvitional",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findAdjustmentFactorProvitional.htm",
          "state",
          "state"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveAdjustmentFactorProvitional.htm?nameView=ViewPanelRegisterAdjustmentFactorProvitional",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findAdjustmentFactorProvitional.htm",
                    "state",
                    "state"
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 25, sortable: false },
        {
          header: "CODIGO",
          align: "center",
          dataIndex: "idAdjustmentFactor",
          flex: 1
        },
        {
          header: "FACTOR DE AJUSTE",
          align: "center",
          dataIndex: "valueAdjustment",
          flex: 1,
          editor: {
            xtype: "NumberDecimalNumberObligatory",
            allowBlank: false
          }
        }, //
        {
          header: "LIMITE GLOBAL",
          align: "center",
          dataIndex: "reverseOverallLimit",
          flex: 1,
          editor: {
            xtype: "NumberDecimalNumberObligatory",
            allowBlank: false
          }
        },
        {
          header: "FACTOR FIJO",
          align: "center",
          dataIndex: "fixedFactor",
          flex: 1,
          editor: {
            xtype: "NumberDecimalNumberObligatory",
            allowBlank: false
          }
        },
        {
          header: "FECHA INICIO VIGENCIA",
          dataIndex: "dateStartValidity",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },

        {
          header: "FECHA LIMITE VIGENCIA",
          dataIndex: "dateEndValidity",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        { header: "STATE", align: "center", dataIndex: "state", flex: 1 }
      ];
      this.callParent(arguments);
    }
  }
);
