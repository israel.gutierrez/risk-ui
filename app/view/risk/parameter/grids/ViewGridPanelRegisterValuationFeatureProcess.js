Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterValuationFeatureProcess",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterValuationFeatureProcess",
    store: "risk.parameter.grids.StoreGridPanelRegisterValuationFeatureProcess",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store:
        "risk.parameter.grids.StoreGridPanelRegisterValuationFeatureProcess",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridValuationFeatureProcess",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/showListValuationFeatureProcessActives.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        { header: "CÓDIGO", dataIndex: "idValuationFeatureProcess", flex: 1 },
        { header: "PROCESO FUNCIÓN", dataIndex: "nameFeatureProcess", flex: 1 },
        { header: "VALOR FUNCIÓN", dataIndex: "valueFeature", flex: 1 },
        { header: "DESCRIPCIÓN", dataIndex: "description", flex: 1 },
        {
          header: "EVALUACIÓN CONCEPTO",
          dataIndex: "valuationConcept",
          flex: 1
        },
        { header: "ESTADO", dataIndex: "state", width: 60 }
      ];
      this.callParent(arguments);
    }
  }
);
