Ext.define("DukeSource.model.risk.parameter.grids.ModelJobPlace", {
  extend: "Ext.data.Model",
  fields: [
    "idHierarchyJobPlace",
    "id",
    "idJobPlace",
    "description",
    "detailDescription",
    "parent",
    "parentId",
    "text",
    "codeMigration",
    "workArea",
    "category",
    "descriptionCategory",
    "children",
    "nameWorkArea",
    "analystUsername",
    "analystFullName",
    "analystEmail",
    "state"
  ]
});

var storeJobPlace = Ext.create("Ext.data.TreeStore", {
  model: "DukeSource.model.risk.parameter.grids.ModelJobPlace",
  autoLoad: false,
  autoSync: true,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    extraParams: {
      propertyOrder: "jp.description",
      start: "0",
      limit: "99999"
    },
    reader: {
      type: "json",
      idProperty: "id"
    },
    writer: {
      type: "json",
      nameProperty: "text"
    }
  },
  root: {
    id: "root",
    name: "Tree",
    data: [],
    expanded: true
  },
  folderSort: true,
  sorters: [
    {
      property: "description",
      direction: "ASC"
    }
  ]
});

Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterJobPlace",
  {
    extend: "Ext.tree.Panel",
    alias: "widget.ViewGridPanelRegisterJobPlace",
    store: storeJobPlace,
    useArrows: true,
    multiSelect: true,
    singleExpand: false,
    mixins: {
      treeFilter: "DukeSource.global.TreeFilter"
    },
    rootVisible: false,
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        viewConfig: {
          plugins: {
            ptype: "treeviewdragdrop"
          },
          listeners: {
            beforedrop: function(
              node,
              data,
              overModel,
              dropPosition,
              dropHandlers
            ) {
              var tree = Ext.ComponentQuery.query(
                "ViewGridPanelRegisterJobPlace"
              )[0];
              dropHandlers.wait = true;
              Ext.MessageBox.confirm({
                title: DukeSource.global.GiroMessages.TITLE_WARNING,
                msg: "Estas seguro que desea continuar?",
                icon: Ext.Msg.WARNING,
                buttonText: {
                  yes: "Si",
                  no: "No"
                },
                buttons: Ext.MessageBox.YESNO,
                fn: function(btn) {
                  if (btn === "yes") {
                    DukeSource.lib.Ajax.request({
                      method: "POST",
                      url: "http://localhost:9000/giro/saveJobPlace.htm",
                      params: {
                        jsonData: Ext.JSON.encode(data.records[0].data),
                        node: overModel.internalId
                      },
                      success: function(response) {
                        response = Ext.decode(response.responseText);
                        if (response.success) {
                          dropHandlers.processDrop();
                        } else {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_WARNING,
                            response.mensaje,
                            Ext.Msg.ERROR
                          );
                        }
                      },
                      failure: function() {
                        DukeSource.global.DirtyView.messageAlert(
                          DukeSource.global.GiroMessages.TITLE_WARNING,
                          response.mensaje,
                          Ext.Msg.ERROR
                        );
                      }
                    });
                  } else {
                    dropHandlers.cancelDrop();
                  }
                }
              });
            }
          }
        },
        dockedItems: [
          {
            xtype: "toolbar",
            dock: "top",
            items: [
              {
                text: "Nuevo",
                iconCls: "add",
                type: "O",
                disabled: true,
                itemId: "newTreeJobPlace",
                action: "newJobPlace"
              },
              "-",
              {
                text: "Modificar",
                itemId: "modifyTreeJobPlace",
                type: "O",
                disabled: true,
                action: "modifyJobPlace",
                iconCls: "modify"
              },
              "-",
              {
                text: "Eliminar",
                itemId: "deleteTreeJobPlace",
                type: "O",
                disabled: true,
                action: "deleteJobPlace",
                iconCls: "delete"
              },
              "->",
              {
                text: "Procesar cargos",
                iconCls: "operations",
                handler: function(btn) {
                  DukeSource.lib.Ajax.request({
                    method: "POST",
                    url: "http://localhost:9000/giro/processHierarchyUsers.htm",
                    success: function(response) {
                      response = Ext.decode(response.responseText);
                      if (response.success) {
                        DukeSource.global.DirtyView.messageAlert(
                          DukeSource.global.GiroMessages.TITLE_CONFIRM,
                          response.message,
                          Ext.Msg.INFO
                        );
                      } else {
                        DukeSource.global.DirtyView.messageAlert(
                          DukeSource.global.GiroMessages.TITLE_WARNING,
                          response.message,
                          Ext.Msg.WARNING
                        );
                      }
                    },
                    failure: function() {
                      DukeSource.global.DirtyView.messageAlert(
                        DukeSource.global.GiroMessages.TITLE_WARNING,
                        response.message,
                        Ext.Msg.WARNING
                      );
                    }
                  });
                }
              }
            ]
          },
          {
            xtype: "toolbar",
            dock: "top",
            items: [
              {
                xtype: "textfield",
                itemId: "searchOption",
                name: "searchOption",
                fieldLabel: "Buscar",
                labelWidth: 60,
                width: 250,
                emptyText: "Buscar",
                enableKeyEvents: true,
                listeners: {
                  afterrender: function(e) {
                    e.focus(false, 1000);
                  },
                  keyup: function(e, t) {
                    me.expandAll(me);
                    me.filterByText(e.getValue());
                    if (e.getValue() === "") {
                      me.collapseAll(me);
                    }
                  }
                }
              }
            ]
          }
        ],
        columns: [
          {
            xtype: "treecolumn",
            text: "Cargos de la empresa",
            flex: 2,
            sortable: true,
            dataIndex: "text"
          },
          {
            text: "Tipo",
            flex: 1,
            sortable: true,
            dataIndex: "descriptionCategory"
          }
        ],
        listeners: {
          beforerender: function(view) {
            view.headerCt.border = 1;
          }
        }
      });
      me.callParent(arguments);
    }
  }
);
