Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterFixedAssets",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterFixedAssets",
    store: "risk.parameter.grids.StoreGridPanelRegisterFixedAssets",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterFixedAssets",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        /*{text:'EXPORTAR A PDF',action:'exportFixedAssetsPdf',iconCls:'pdf'},'-',{text:'EXPORTAR A EXCEL',action:'exportFixedAssetsExcel',iconCls:'excel'},*/ "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridFixedAssets",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findFixedAssets.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        {
          header: "CODIGO",
          dataIndex: "idFixedAssets",
          width: 80,
          hidden: true
        },
        { header: "CODIGO", dataIndex: "codeAssets", width: 80 },
        {
          header: "ACTIVO DE INFORMACION",
          dataIndex: "description",
          flex: 2,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "DISPONIBILIDAD",
          dataIndex: "nameAvailability",
          width: 120,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "CONFIDENCIALIDAD",
          dataIndex: "nameConfidentiality",
          width: 120,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "INTEGRIDAD",
          dataIndex: "nameIntegrity",
          width: 120,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "CUSTODIO",
          dataIndex: "nameTechnicalCustodio",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "PROPIETARIO",
          dataIndex: "nameOwner",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);
