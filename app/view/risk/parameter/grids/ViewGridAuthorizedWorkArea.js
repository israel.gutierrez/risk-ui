Ext.define('DukeSource.view.risk.parameter.grids.ViewGridAuthorizedWorkArea', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ViewGridAuthorizedWorkArea',
    store: 'risk.parameter.grids.StoreGridAuthorizedWorkArea',
    loadMask: true,
    columnLines: true,
    bbar: {
        xtype: 'pagingtoolbar',
        pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
        store: 'risk.parameter.grids.StoreGridAuthorizedWorkArea',
        displayInfo: true,
        displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
        emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    initComponent: function () {
        this.columns = [
            {
                header: 'Estado',
                dataIndex: 'abbreviationStateEvent',
                width: 120,
                align: 'center'
            },
            {
                header: 'Usuario',
                dataIndex: 'fullName',
                width: 200
            },
            {
                xtype: 'numbercolumn',
                header: 'Monto m&iacute;nimo',
                dataIndex: 'minAmount',
                width: 200,
                align: 'center',
                format: '0,0.00',
                renderer: function (value, metaData, record) {
                    if (record.get('sequence') != 3) {
                        return '<span>' + '--' + '</span>';
                    } else {
                        return '<span>' + value + '.00</span>';
                    }
                }
            },
            {
                header: 'Monto m&aacute;ximo',
                dataIndex: 'maxAmount',
                width: 200,
                align: 'center',
                xtype: 'numbercolumn',
                format: '0,0.00',
                renderer: function (value, metaData, record) {
                    if (record.get('sequence') != 3) {
                        return '<span>' + '--' + '</span>';
                    } else {
                        return '<span>' + value + '.00</span>';
                    }
                }
            }

        ];
        this.callParent(arguments);
    }
});
