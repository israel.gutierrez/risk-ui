Ext.define("ModelComboFrequency", {
  extend: "Ext.data.Model",
  fields: ["idFrequency", "description", "percentage"]
});
Ext.define("ModelComboFrequencyFeature", {
  extend: "Ext.data.Model",
  fields: ["idFrequencyFeature", "description"]
});
var StoreComboFrequencyFeature = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboFrequencyFeature",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListFrequencyFeatureActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("ModelGridPanelRegisterDetailFrequencyFeature", {
  extend: "Ext.data.Model",
  fields: ["idFrequencyFeature", "idFrequency", "description", "alias", "state"]
});

var StoreGridPanelRegisterDetailFrequencyFeature = Ext.create(
  "Ext.data.Store",
  {
    extend: "Ext.data.Store",
    model: "ModelGridPanelRegisterDetailFrequencyFeature",
    groupField: "idFrequencyFeature",
    proxy: {
      actionMethods: {
        create: "POST",
        read: "POST",
        update: "POST"
      },
      type: "ajax",
      url: "http://localhost:9000/giro/loadGridDefault.htm",
      reader: {
        type: "json",
        totalProperty: "totalCount",
        root: "data",
        successProperty: "success"
      }
    }
  }
);

Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterDetailFrequencyFeature",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterDetailFrequencyFeature",
    store: StoreGridPanelRegisterDetailFrequencyFeature,
    loadMask: true,
    columnLines: true,
    tbar: [
      {
        text: "Nuevo",
        iconCls: "add",
        action: "newDetailFrequencyfeature"
      },
      "-",
      {
        text: "Eliminar",
        iconCls: "delete",
        action: "deleteDetailFrequencyfeature"
      },
      "-",
      {
        xtype: "UpperCaseTextField",
        action: "searchDetailFrequencyfeature",
        fieldLabel: "Buscar",
        labelWidth: 60,
        width: 300
      },
      "-",
      "->",
      {
        text: "Auditoria",
        action: "detailFrequencyfeatureAuditory",
        iconCls: "auditory"
      }
    ],
    features: [
      {
        ftype: "grouping",
        startCollapsed: true
      }
    ],
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridPanelRegisterDetailFrequencyFeature,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridDetailFrequencyfeature",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        StoreComboFrequency.load({
          callback: function() {
            StoreComboFrequencyFeature.load({
              callback: function() {
                var panelGeneral = Ext.ComponentQuery.query(
                  "ViewWindowConfigurationMatrix"
                )[0];
                me.store.getProxy().extraParams = {
                  idTypeMatrix: panelGeneral
                    .down("ViewComboTypeMatrix")
                    .getValue(),
                  idOperationalRiskExposition: panelGeneral
                    .down("ViewComboOperationalRiskExposition")
                    .getValue(),
                  propertyFind: "description",
                  valueFind: "",
                  propertyOrder: "description"
                };
                me.store.getProxy().url =
                  "http://localhost:9000/giro/findDetailFrequencyFeature.htm";
                me.down("pagingtoolbar").moveFirst();
              }
            });
          }
        });
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveDetailFrequencyFeature.htm?nameView=ViewPanelConfigurationGeneralMatrix",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  var panelGeneral = Ext.ComponentQuery.query(
                    "ViewWindowConfigurationMatrix"
                  )[0];
                  grid.store.getProxy().extraParams = {
                    idTypeMatrix: panelGeneral
                      .down("ViewComboTypeMatrix")
                      .getValue(),
                    idOperationalRiskExposition: panelGeneral
                      .down("ViewComboOperationalRiskExposition")
                      .getValue(),
                    propertyFind: "description",
                    valueFind: "",
                    propertyOrder: "description"
                  };
                  grid.store.getProxy().url =
                    "http://localhost:9000/giro/findDetailFrequencyFeature.htm";
                  grid.down("pagingtoolbar").moveFirst();
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        {
          xtype: "rownumberer",
          width: 25,
          sortable: false
        },
        {
          header: "Frecuencia",
          dataIndex: "idFrequency",
          flex: 0.5,
          editor: {
            xtype: "ViewComboFrequency",
            allowBlank: false,
            msgTarget: "side",
            fieldCls: "obligatoryTextField",
            store: (StoreComboFrequency = Ext.create("Ext.data.Store", {
              extend: "Ext.data.Store",
              model: "ModelComboFrequency",
              pageSize: 9999,
              autoLoad: false,
              proxy: {
                actionMethods: {
                  create: "POST",
                  read: "POST",
                  update: "POST"
                },
                type: "ajax",
                url: "http://localhost:9000/giro/showListFrequencyActives.htm",
                extraParams: {
                  idTypeMatrix: Ext.ComponentQuery.query(
                    "ViewWindowConfigurationMatrix"
                  )[0]
                    .down("ViewComboTypeMatrix")
                    .getValue(),
                  idOperationalRiskExposition: Ext.ComponentQuery.query(
                    "ViewWindowConfigurationMatrix"
                  )[0]
                    .down("ViewComboOperationalRiskExposition")
                    .getValue()
                },
                reader: {
                  type: "json",
                  root: "data",
                  successProperty: "success"
                }
              }
            }))
          },
          renderer: function(val) {
            var Frequency = Ext.getStore(StoreComboFrequency);
            var index = Frequency.findExact("idFrequency", val);
            if (index !== -1) {
              var rs = Frequency.getAt(index).data;
              return rs.description;
            }
          }
        },
        {
          header: "Criterio",
          dataIndex: "idFrequencyFeature",
          flex: 0.8,
          editor: {
            xtype: "ViewComboFrequencyFeature",
            allowBlank: false,
            msgTarget: "side",
            fieldCls: "obligatoryTextField",
            store: (StoreComboFrequencyFeature = Ext.create("Ext.data.Store", {
              extend: "Ext.data.Store",
              model: "ModelComboFrequencyFeature",
              pageSize: 9999,
              autoLoad: false,
              proxy: {
                actionMethods: {
                  create: "POST",
                  read: "POST",
                  update: "POST"
                },
                type: "ajax",
                url:
                  "http://localhost:9000/giro/showListFrequencyFeatureActivesComboBox.htm",
                extraParams: {
                  idTypeMatrix: Ext.ComponentQuery.query(
                    "ViewWindowConfigurationMatrix"
                  )[0]
                    .down("ViewComboTypeMatrix")
                    .getValue(),
                  idOperationalRiskExposition: Ext.ComponentQuery.query(
                    "ViewWindowConfigurationMatrix"
                  )[0]
                    .down("ViewComboOperationalRiskExposition")
                    .getValue(),
                  propertyOrder: "description"
                },
                reader: {
                  type: "json",
                  root: "data",
                  successProperty: "success"
                }
              }
            }))
          },
          renderer: function(val) {
            var FrequencyFeature = Ext.getStore(StoreComboFrequencyFeature);
            var index = FrequencyFeature.findExact("idFrequencyFeature", val);
            if (index !== -1) {
              var rs = FrequencyFeature.getAt(index).data;
              return rs.description;
            }
          }
        },
        {
          header: "Detalle",
          dataIndex: "description",
          flex: 2,
          editor: {
            xtype: "UpperCaseTextFieldObligatory",
            allowBlank: false
          }
        },
        {
          header: "Alias",
          dataIndex: "alias",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextFieldObligatory",
            allowBlank: false,
            maxLength: 300
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);
