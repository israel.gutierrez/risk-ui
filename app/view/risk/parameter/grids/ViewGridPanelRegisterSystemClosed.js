Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterSystemClosed",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterSystemClosed",
    store: "risk.parameter.grids.StoreGridPanelRegisterSystemClosed",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterSystemClosed",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridSystemClosed",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findSystemClosed.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      //        this.plugins = Ext.create('Ext.grid.plugin.RowEditing', {
      //        clicksToMoveEditor: 1,
      //        saveBtnText  : 'GUARDAR',
      //        cancelBtnText: 'CANCELAR',
      //        autoCancel: false,
      //        completeEdit:function(){
      //            var me = this;
      //            var grid= me.grid;
      //            var selModel = grid.getSelectionModel();
      //            var record = selModel.getLastSelected();
      //            if (me.editing && me.validateEdit()){
      //                me.editing = false;
      //
      //                Ext.Ajax.request({
      //                    method:'POST',
      //                    url:'http://localhost:9000/giro/saveSystemClosed.htm?nameView=ViewPanelRegisterSystemClosed',
      //                    params:{
      //                        jsonData:Ext.JSON.encode(record.data)
      //                    },
      //                    success:function(response){
      //                        response = Ext.decode(response.responseText);
      //                        if (response.success) {
      //                            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE,response.mensaje,Ext.Msg.INFO);
      //                            DukeSource.global.DirtyView.searchPaginationGridNormal('',grid, grid.down('pagingtoolbar'),'http://localhost:9000/giro/findSystemClosed.htm','description','description')
      //                        } else {
      //                            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_ERROR,response.mensaje,Ext.Msg.ERROR);
      //                        }
      //                    },
      //                    failure:function(){}
      //                });
      //                me.fireEvent('edit', me, me.context);
      //            }
      //        }
      //    });
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        { header: "CODIGO", dataIndex: "idSystemClosed", width: 70 },
        {
          header: "FECHA DE CIERRE",
          dataIndex: "dateClose",
          flex: 1,
          editor: {
            xtype: "datefield",
            fieldCls: "obligatoryTextField",
            format: "d/m/Y",
            enforceMaxLength: true,
            maxLength: 10,
            //                submitFormat: 'd/m/Y H:i:s',
            listeners: {
              expand: function(field, value) {
                field.setMaxValue(new Date());
              }
            },
            allowBlank: false
          }
        },
        {
          header: "DESCRIPCION",
          dataIndex: "description",
          flex: 2,
          editor: {
            xtype: "UpperCaseTextFieldObligatory",
            allowBlank: false
          }
        },
        {
          header: "ESTADO",
          dataIndex: "state",
          align: "center",
          width: 70,
          editor: {
            xtype: "UpperCaseTextFieldReadOnly",
            allowBlank: false
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);
