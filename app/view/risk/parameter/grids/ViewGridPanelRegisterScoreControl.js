Ext.require(["Ext.ux.ColorField"]);
Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterScoreControl",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterScoreControl",
    store: "risk.parameter.grids.StoreGridPanelRegisterScoreControl",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterScoreControl",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          DukeSource.global.GiroConstants.MITIGATION_CONTROL,
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findScoreControl.htm",
          "typeControl",
          "scoreControl"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "Guardar",
        cancelBtnText: "Cancelar",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;
            var panel = Ext.ComponentQuery.query(
              "ViewPanelRegisterScoreControl"
            )[0];

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveScoreControl.htm?nameView=ViewPanelRegisterScoreControl",
              params: {
                jsonData: Ext.JSON.encode(record.data),
                typeControl: panel.down("#typeControl").getValue()
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageNormal(response.message);

                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    panel.down("#typeControl").getValue(),
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findScoreControl.htm",
                    "typeControl",
                    "scoreControl"
                  );
                } else {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        {
          header: "Descripción",
          dataIndex: "description",
          width: 180,
          editor: {
            xtype: "UpperCaseTextFieldObligatory",
            allowBlank: false
          }
        },
        {
          header: "Efectividad de control(ec)",
          columns: [
            {
              header: "% Mínimo",
              align: "center",
              dataIndex: "minEffectiveness",
              width: 180,
              xtype: "numbercolumn",
              format: "0,0.00",
              editor: {
                xtype: "NumberDecimalNumberObligatory",
                allowBlank: false
              }
            },
            {
              header: "% Maximo",
              align: "center",
              dataIndex: "maxEffectiveness",
              width: 180,
              xtype: "numbercolumn",
              format: "0,0.00",
              editor: {
                xtype: "NumberDecimalNumberObligatory",
                allowBlank: false
              }
            }
          ]
        },
        {
          header: "Reducción",
          align: "center",
          dataIndex: "scoreControl",
          width: 180,
          editor: {
            xtype: "NumberDecimalNumberObligatory",
            allowBlank: false
          }
        },
        {
          header: "Codigo color",
          align: "center",
          dataIndex: "codeColour",
          flex: 1,
          editor: {
            xtype: "colorfield",
            allowBlank: false
          },
          renderer: function(value) {
            return (
              '<div  style="background-color:#' +
              value +
              "; color:#" +
              value +
              ';"> ' +
              value +
              "</div>"
            );
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);
