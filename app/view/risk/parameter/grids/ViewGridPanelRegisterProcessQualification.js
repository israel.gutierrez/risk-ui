Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterProcessQualification",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterProcessQualification",
    store: "risk.parameter.grids.StoreGridPanelRegisterProcessQualification",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterProcessQualification",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        //            {text:'EXPORTAR A PDF',action:'exportProcessQualificationPdf',iconCls:'pdf'},'-',
        //            {text:'EXPORTAR A EXCEL',action:'exportProcessQualificationExcel',iconCls:'excel'},'-',
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridProcessQualification",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findProcessQualification.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveProcessQualification.htm?nameView=ViewPanelRegisterProcessQualification",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findProcessQualification.htm",
                    "description",
                    "description"
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 25, sortable: false },
        {
          header: "CODIGO",
          align: "center",
          dataIndex: "idProcessQualification",
          flex: 1
        },
        {
          header: "CALIFICACION",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextFieldObligatory"
          }
        },
        {
          header: "VALORACION",
          align: "center",
          dataIndex: "valueQualification",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextFieldObligatory"
          }
        },
        {
          header: "ESTADO",
          align: "center",
          dataIndex: "state",
          flex: 1,
          value: "S"
        }
      ];
      this.callParent(arguments);
    }
  }
);
