Ext.define("ModelGridPanelRegisterForeignKeys", {
  extend: "Ext.data.Model",
  fields: [
    "idFore",
    "identified",
    "nameTable",
    "nameColumn",
    "description",
    "value",
    "state"
  ]
});

var storeGridPanelRegisterForeignKeys = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelGridPanelRegisterForeignKeys",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListForeignActives.htm",
    params: {
      valueFind: "S",
      propertyFind: "state",
      propertyOrder: "identified"
    },
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterForeignKeys",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterForeignKeys",
    store: storeGridPanelRegisterForeignKeys,
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: storeGridPanelRegisterForeignKeys,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridForeignKeys",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "S",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/showListForeignActives.htm",
          "state",
          "identified"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/saveForeignKeys.htm",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  grid.down("pagingtoolbar").moveFirst();
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        {
          header: "CODIGO",
          dataIndex: "idFore",
          style: "background: #D8D8D8",
          width: 80
        },
        {
          header: "IDENTIFICADOR",
          dataIndex: "identified",
          width: 150,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "TABLA",
          dataIndex: "nameTable",
          width: 90,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "COLUMNA",
          dataIndex: "nameColumn",
          width: 90,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "DESCRIPCION",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "VALOR",
          dataIndex: "value",
          width: 120,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "ESTADO",
          dataIndex: "state",
          style: "background: #D8D8D8",
          width: 50
        }
      ];
      this.callParent(arguments);
    }
  }
);
