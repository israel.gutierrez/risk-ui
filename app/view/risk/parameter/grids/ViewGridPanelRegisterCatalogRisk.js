Ext.define(
  "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterCatalogRisk",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterCatalogRisk",
    store: "risk.parameter.grids.StoreGridPanelRegisterCatalogRisk",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.parameter.grids.StoreGridPanelRegisterCatalogRisk",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        //            {text:'EXPORTAR A PDF',action:'exportCatalogRiskPdf',iconCls:'pdf'},'-',
        //            {text:'EXPORTAR A EXCEL',action:'exportCatalogRiskExcel',iconCls:'excel'},'-',
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridCatalogRisk",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/showListCatalogRiskActives.htm",
          "description",
          "idRisk"
        );
      }
    },
    initComponent: function() {
      this.columns = [
        { xtype: "rownumberer", width: 25, sortable: false },
        {
          header: "CODIGO",
          align: "center",
          dataIndex: "codeRisk",
          width: 120
        },
        { header: "DESCRIPCIÓN", dataIndex: "description", flex: 1 },
        { header: "ESTADO", align: "center", dataIndex: "state", width: 60 }
      ];
      this.callParent(arguments);
    }
  }
);
