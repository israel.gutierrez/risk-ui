Ext.define("ModelComboBusinessLineOne", {
  extend: "Ext.data.Model",
  fields: ["idBusinessLineOne", "description"]
});

var StoreComboBusinessLineOne = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboBusinessLineOne",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListBusinessLineOneActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboBusinessLineOne", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboBusinessLineOne",
  queryMode: "remote",
  displayField: "description",
  valueField: "idBusinessLineOne",

  fieldLabel: "LINEA NEG.1",
  msgTarget: "side",
  editable: true,
  forceSelection: true,
  store: StoreComboBusinessLineOne
});
