Ext.define("ModelComboEventOne", {
  extend: "Ext.data.Model",
  fields: ["idEventOne", "description"]
});

var StoreComboEventOne = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboEventOne",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListEventOneActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboEventOne", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboEventOne",
  queryMode: "remote",
  displayField: "description",
  valueField: "idEventOne",

  editable: true,
  forceSelection: true,
  fieldLabel: "EVENTOS N1",
  msgTarget: "side",
  store: StoreComboEventOne
});
