Ext.define("ModelComboTitleReport", {
  extend: "Ext.data.Model",
  fields: ["idTitleReport", "description"]
});
var StoreComboTitleReport = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboTitleReport",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListTitleReportActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboTitleReport", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboTitleReport",
  queryMode: "local",
  displayField: "description",
  valueField: "idTitleReport",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: StoreComboTitleReport
});
