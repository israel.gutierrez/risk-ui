Ext.define("ModelComboEventThree", {
  extend: "Ext.data.Model",
  fields: ["idEventThree", "description"]
});

var StoreComboEventThree = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboEventThree",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboEventThree", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboEventThree",
  queryMode: "local",
  displayField: "description",
  valueField: "idEventThree",

  fieldLabel: "EVENTOS N3",
  msgTarget: "side",
  editable: true,
  forceSelection: true,
  store: StoreComboEventThree
});
