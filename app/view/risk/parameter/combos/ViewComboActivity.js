Ext.define("ModelComboActivity", {
  extend: "Ext.data.Model",
  fields: ["idActivity", "description"]
});

var StoreComboActivity = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboActivity",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListActivityActives.htm",
    extraParams: {
      valueFind: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboActivity", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboActivity",
  queryMode: "local",
  displayField: "description",
  valueField: "idActivity",

  editable: true,
  forceSelection: true,
  store: StoreComboActivity
});
