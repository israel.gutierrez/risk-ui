Ext.define('DukeSource.view.risk.parameter.combos.ViewComboCompany', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboCompanyBean',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idCompanyBean',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.parameter.combos.StoreComboCompany'
});
