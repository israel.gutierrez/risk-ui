Ext.define("ModelComboTypeMatrix", {
  extend: "Ext.data.Model",
  fields: [
    "idTypeMatrix",
    "description",
    "idBusinessLineOne",
    "isBusinessLine",
    "stateVerification",
    "stateApproved"
  ]
});

var StoreComboTypeMatrix = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboTypeMatrix",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListTypeMatrixActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      rootProperty: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboTypeMatrix", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboTypeMatrix",
  queryMode: "local",
  displayField: "description",
  valueField: "idTypeMatrix",

  editable: true,
  forceSelection: true,
  store: StoreComboTypeMatrix
});
