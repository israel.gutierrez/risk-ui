Ext.define("ModelComboForeignKey", {
  extend: "Ext.data.Model",
  fields: ["value", "description"]
});

var storeComboForeignKey = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboForeignKey",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboForeignKey", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboForeignKey",
  queryMode: "local",
  displayField: "description",
  valueField: "value",
  editable: true,
  forceSelection: true,
  msgTarget: "side",
  store: storeComboForeignKey
});
