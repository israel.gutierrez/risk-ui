var yesno = Ext.create('Ext.data.Store', {
    fields: ['id', 'description'],
    data: [
        {"id": "A", "description": "ABIERTO"},
        {"id": "C", "description": "CERRADO"}
    ]
});

Ext.define('DukeSource.view.risk.parameter.combos.ViewComboStateEvent', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboStateEvent',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'id',
    value: 'A',
    editable: false,
    forceSelection: true,
    listeners: {
        render: function () {
            var me = this;
            me.store.load();
        }
    },
    store: yesno
});