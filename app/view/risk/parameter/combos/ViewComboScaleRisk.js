Ext.define("ModelComboScaleRisk", {
  extend: "Ext.data.Model",
  fields: [
    "idScaleRisk",
    "description",
    "rangeInf",
    "rangeSup",
    "levelColour",
    "businessLineOne"
  ]
});

var StoreComboScaleRisk = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboScaleRisk",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListScaleRiskActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboScaleRisk", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboScaleRisk",
  queryMode: "local",
  displayField: "description",
  valueField: "idScaleRisk",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: StoreComboScaleRisk
});
