var storeYear = Ext.create("Ext.data.Store", {
  fields: ["id", "description"],
  data: [
    { id: "2019", description: "2019" },
    { id: "2018", description: "2018" },
    { id: "2017", description: "2017" },
    { id: "2016", description: "2016" },
    { id: "2015", description: "2015" },
    { id: "2014", description: "2014" },
    { id: "2013", description: "2013" },
    { id: "2012", description: "2012" },
    { id: "2011", description: "2011" }
  ]
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboYear", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboYear",
  queryMode: "local",
  displayField: "description",
  valueField: "id",
  editable: false,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: storeYear
});
