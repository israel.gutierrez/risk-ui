Ext.define("ModelComboImpactFeature", {
  extend: "Ext.data.Model",
  fields: ["idImpactFeature", "description", "type"]
});

var StoreComboImpactFeature = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboImpactFeature",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListImpactFeatureActivesComboBox.htm",
    extraParams: {
      propertyOrder: "if.description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboImpactFeature", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboImpactFeature",
  queryMode: "local",
  displayField: "description",
  valueField: "idImpactFeature",

  editable: true,
  forceSelection: true,
  store: StoreComboImpactFeature,
  tpl: Ext.create(
    "Ext.XTemplate",
    '<tpl for=".">',
    '<div class="x-boundlist-item"><span style="display: inline-block; width: 50%">{description}</span><span style="display: inline-block; width: 50%;text-align: right">{type}</span></div>',
    "</tpl>"
  ),

  displayTpl: Ext.create(
    "Ext.XTemplate",
    '<tpl for=".">',
    "{description} ({type}) ",
    "</tpl>"
  )
});
