var category = Ext.create("Ext.data.Store", {
  fields: ["id", "description"],
  data: [
    { id: "COLABORADOR", description: "COLABORADOR" },
    { id: "GESTOR", description: "GESTOR" },
    { id: "ANALISTA", description: "ANALISTA RO" },
    { id: "ANALISTASI", description: "ANALISTA SI" },
    { id: "ANALISTACN", description: "ANALISTA CN" }
  ]
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboCategory", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboCategory",
  queryMode: "local",
  displayField: "description",
  valueField: "id",
  editable: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: category
});
