Ext.define("ModelComboTypeEvent", {
  extend: "Ext.data.Model",
  fields: ["idTypeEvent", "description"]
});
var StoreComboTypeEvent = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboTypeEvent",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListTypeEventActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboTypeEvent", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboTypeEvent",
  queryMode: "remote",
  displayField: "description",
  valueField: "idTypeEvent",

  editable: true,
  forceSelection: true,
  typeAhead: true,
  store: StoreComboTypeEvent
});
