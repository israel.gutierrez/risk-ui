Ext.define("ModelComboDetailImpactFeature", {
  extend: "Ext.data.Model",
  fields: ["idImpactFeature", "description"]
});

var StoreComboDetailImpactFeature = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboDetailImpactFeature",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListDetailImpactFeatureActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.combos.ViewComboDetailImpactFeature",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboDetailImpactFeature",
    queryMode: "local",
    displayField: "description",
    valueField: "idImpactFeature",

    editable: true,
    forceSelection: true,
    //    initComponent: function() {
    //        this.on('render',this.handleFieldRender,this);
    //        this.callParent(arguments);
    //    },
    //    handleFieldRender: function() {
    //        var me = this;
    //        me.store.load();
    //    },
    store: StoreComboDetailImpactFeature
  }
);
