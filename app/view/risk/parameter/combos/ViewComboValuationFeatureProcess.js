Ext.define('DukeSource.view.risk.parameter.combos.ViewComboValuationFeatureProcess', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboValuationFeatureProcess',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idValuationFeatureProcess',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.parameter.combos.StoreComboValuationFeatureProcess'
});
