Ext.define("ModelComboAgency", {
  extend: "Ext.data.Model",
  fields: ["idAgency", "description"]
});
var StoreComboAgency = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboAgency",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListAgencyActivesComboBox.htm",
    extraParams: {
      propertyOrder: "a.description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboAgency", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboAgencyBean",
  queryMode: "local",
  displayField: "description",
  valueField: "idAgencyBean",

  editable: true,
  typeAhead: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: StoreComboAgency
});
