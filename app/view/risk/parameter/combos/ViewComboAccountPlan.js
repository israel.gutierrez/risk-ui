Ext.define('DukeSource.view.risk.parameter.combos.ViewComboAccountPlan', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboAccountPlanBean',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idAccountPlanBean',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.parameter.combos.StoreComboAccountPlan'
});
