var storeActionPlan = Ext.create('Ext.data.Store', {
    fields: ['id', 'description'],
    data: [
        {"id": "R", "description": "RIESGO"},
        {"id": "I", "description": "INCIDENTE"},
        {"id": "E", "description": "EVENTO"},
        {"id": "A", "description": "ACUERDO"}
    ]
});

Ext.define('DukeSource.view.risk.parameter.combos.ViewComboOriginActionPlan', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboOriginActionPlan',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'id',
    editable: false,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: storeActionPlan
});