Ext.define("ModelComboProcess", {
  extend: "Ext.data.Model",
  fields: ["idProcess", "description"]
});

var StoreComboProcess = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboProcess",
  pageSize: 9999,
  autoLoad: false,
  sorters: [
    {
      property: "description",
      direction: "ASC"
    }
  ],
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListProcessActivesComboBox.htm",
    extraParams: {
      propertyOrder: "idProcess",
      start: 0,
      limit: 9999
    },
    reader: {
      type: "json",
      rootProperty: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboProcess", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboProcess",
  queryMode: "local",
  displayField: "description",
  valueField: "idProcess",

  editable: true,
  forceSelection: true,
  store: StoreComboProcess
});
