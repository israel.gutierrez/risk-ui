Ext.define("ModelComboFactorRisk", {
  extend: "Ext.data.Model",
  fields: ["idFactorRisk", "description"]
});

var StoreComboFactorRisk = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboFactorRisk",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListFactorRiskActivesComboBox.htm", //'showListFactorRiskActives.htm',
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  },
  sorters: [
    {
      property: "description",
      direction: "ASC"
    }
  ]
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboFactorRisk", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboFactorRisk",
  queryMode: "remote",
  displayField: "description",
  valueField: "idFactorRisk",

  msgTarget: "side",
  fieldLabel: "FACTOR DE RIESGO",
  editable: true,
  typeAhead: true,
  forceSelection: true,
  store: StoreComboFactorRisk
});
