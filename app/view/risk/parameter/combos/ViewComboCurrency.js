Ext.define("ModelComboCurrency", {
  extend: "Ext.data.Model",
  fields: ["idCurrency", "description"]
});
var StoreComboCurrency = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboCurrency",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListCurrencyActivesComboBox.htm", //'showListCurrencyActives.htm',
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboCurrency", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboCurrency",
  queryMode: "remote",
  displayField: "description",
  valueField: "idCurrency",

  msgTarget: "side",
  fieldLabel: "MONEDA",
  editable: true,
  forceSelection: true,
  store: StoreComboCurrency
});
