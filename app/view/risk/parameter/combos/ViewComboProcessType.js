Ext.define("ModelComboProcessType", {
  extend: "Ext.data.Model",
  fields: ["idProcessType", "description"]
});

var StoreComboProcessType = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboProcessType",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListProcessTypeActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboProcessType", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboProcessType",
  queryMode: "remote",
  displayField: "description",
  valueField: "idProcessType",

  editable: true,
  forceSelection: true,
  typeAhead: true,
  store: StoreComboProcessType
});
