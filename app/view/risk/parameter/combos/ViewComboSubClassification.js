Ext.define("ModelComboSubClassification", {
  extend: "Ext.data.Model",
  fields: ["idSubClassification", "description"]
});

var storeComboSubClassification = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  autoLoad: false,
  model: "ModelComboSubClassification",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListSubClassificationActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboSubClassification", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboSubClassification",
  queryMode: "local",
  displayField: "description",
  valueField: "idSubClassification",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: storeComboSubClassification
});
