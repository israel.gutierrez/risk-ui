Ext.define("ModelComboIndicatorType", {
  extend: "Ext.data.Model",
  fields: ["idIndicatorType", "description"]
});
var StoreComboIndicatorType = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboIndicatorType",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListIndicatorTypeActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboIndicatorType", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboIndicatorType",
  queryMode: "local",
  displayField: "description",
  valueField: "idIndicatorType",

  editable: true,
  forceSelection: true,
  store: StoreComboIndicatorType
});
