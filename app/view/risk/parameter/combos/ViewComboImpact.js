Ext.define("ModelComboImpact", {
  extend: "Ext.data.Model",
  fields: ["idImpact", "description", "valueMaximo", "percentage"]
});

var StoreComboImpact = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboImpact",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListImpactActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboImpact", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboImpact",
  queryMode: "local",
  displayField: "description",
  valueField: "idImpact",

  editable: true,
  forceSelection: true,
  store: StoreComboImpact
});
