Ext.define('DukeSource.view.risk.parameter.combos.ViewComboTechnicalCustodio', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboTechnicalCustodio',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idTechnicalCustodio',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.parameter.combos.StoreComboTechnicalCustodio'
});
