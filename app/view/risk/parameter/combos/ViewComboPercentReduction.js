Ext.define("ModelComboPercentReduction", {
  extend: "Ext.data.Model",
  fields: ["idPercentReduction", "valuePercent"]
});

var StoreComboPercentReduction = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboPercentReduction",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListPercentReductionActives.htm",
    extraParams: {
      propertyOrder: "valuePercent"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboPercentReduction", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboPercentReduction",
  queryMode: "local",
  displayField: "valuePercent",
  valueField: "idPercentReduction",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: StoreComboPercentReduction
});
