Ext.define("ModelComboYearRatingPeriodEvent", {
  extend: "Ext.data.Model",
  fields: [{ name: "year", type: "int" }, "descriptionYear"]
});

var StoreComboYearRatingPeriodEvent = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboYearRatingPeriodEvent",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/listYearRatingPeriodEvent.htm",
    extraParams: {
      propertyOrder: "descriptionYear"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.combos.ViewComboYearRatingPeriodEvent",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboYearRatingPeriodEvent",
    queryMode: "local",
    displayField: "descriptionYear",
    valueField: "year",

    editable: true,
    forceSelection: true,
    initComponent: function() {
      this.on("render", this.handleFieldRender, this);
      this.callParent(arguments);
    },
    handleFieldRender: function() {
      var me = this;
      me.store.load();
    },
    store: StoreComboYearRatingPeriodEvent
  }
);
