Ext.define("ModelComboDetailFrequencyFeature", {
  extend: "Ext.data.Model",
  fields: ["idFrequencyFeature", "description"]
});
var StoreComboDetailFrequencyFeature = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboDetailFrequencyFeature",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListDetailFrequencyFeatureActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.combos.ViewComboDetailFrequencyFeature",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboDetailFrequencyFeature",
    queryMode: "local",
    displayField: "description",
    valueField: "idFrequencyFeature",

    editable: true,
    forceSelection: true,
    //    initComponent: function() {
    //        this.on('render',this.handleFieldRender,this);
    //        this.callParent(arguments);
    //    },
    //    handleFieldRender: function() {
    //        var me = this;
    //        me.store.load();
    //    },
    store: StoreComboDetailFrequencyFeature
  }
);
