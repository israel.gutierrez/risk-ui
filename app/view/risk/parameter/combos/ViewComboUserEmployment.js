Ext.define("ModelComboUserEmployment", {
  extend: "Ext.data.Model",
  fields: ["idUserEmployment", "description", "category", "employment"]
});

var StoreComboUserEmployment = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboUserEmployment",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboUserEmployment", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboUserEmployment",
  queryMode: "local",
  displayField: "description",
  valueField: "idUserEmployment",

  editable: true,
  forceSelection: true,
  store: StoreComboUserEmployment
});
