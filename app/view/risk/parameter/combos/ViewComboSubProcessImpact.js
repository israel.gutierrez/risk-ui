Ext.define("ModelComboSubProcessImpact", {
  extend: "Ext.data.Model",
  fields: ["idSubProcess", "description"]
});

var StoreComboSubProcessImpact = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboSubProcessImpact",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboSubProcessImpact", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboSubProcessImpact",
  queryMode: "local",
  displayField: "description",
  valueField: "idSubProcess",

  editable: true,
  forceSelection: true,
  store: StoreComboSubProcessImpact
});
