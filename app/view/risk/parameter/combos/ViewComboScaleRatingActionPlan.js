Ext.define('DukeSource.view.risk.parameter.combos.ViewComboScaleRatingActionPlan', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboScaleRatingActionPlanBean',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idScaleRatingActionPlanBean',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.parameter.combos.StoreComboScaleRatingActionPlan'
});
