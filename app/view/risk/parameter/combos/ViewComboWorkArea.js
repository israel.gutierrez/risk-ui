Ext.define("ModelComboWorkArea", {
  extend: "Ext.data.Model",
  fields: ["idWorkArea", "description"]
});

var StoreComboWorkArea = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboWorkArea",
  pageSize: 9999,
  autoLoad: true,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListWorkAreaActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboWorkArea", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboWorkArea",
  plugins: ["ComboSelectCount"],
  queryMode: "remote",
  displayField: "description",
  valueField: "idWorkArea",

  editable: true,
  typeAhead: true,
  forceSelection: true,
  store: StoreComboWorkArea
});
