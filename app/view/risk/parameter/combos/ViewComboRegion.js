Ext.define('DukeSource.view.risk.parameter.combos.ViewComboRegion', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboRegion',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idProductBean',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.parameter.combos.StoreComboRegion'
});
