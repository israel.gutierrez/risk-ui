Ext.define("ModelComboProduct", {
  extend: "Ext.data.Model",
  fields: ["idProduct", "description"]
});
var StoreComboProduct = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboProduct",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboProduct", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboProduct",
  queryMode: "remote",
  displayField: "description",
  valueField: "idProduct",

  editable: true,
  forceSelection: true,
  store: StoreComboProduct
});
