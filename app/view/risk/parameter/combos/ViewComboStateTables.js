Ext.define('DukeSource.view.risk.parameter.combos.ViewComboStateTables', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboStateTablesBean',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idStateTablesBean',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.parameter.combos.StoreComboStateTables'
});
