Ext.define("ModelComboSubprocessImportance", {
  extend: "Ext.data.Model",
  fields: ["value", "description"]
});
var StoreComboSubprocessImportance = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboSubprocessImportance",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
    extraParams: {
      propertyFind: "identified",
      valueFind: "IMPORTANTPROCESS",
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.combos.ViewComboSubprocessImportance",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboSubprocessImportance",
    queryMode: "local",
    displayField: "description",
    valueField: "value",
    editable: false,
    //    forceSelection: true,
    initComponent: function() {
      this.on("render", this.handleFieldRender, this);
      this.callParent(arguments);
    },
    handleFieldRender: function() {
      var me = this;
      me.store.load();
    },
    store: StoreComboSubprocessImportance
  }
);
