Ext.define("ModelComboRiskType", {
  extend: "Ext.data.Model",
  fields: ["idRiskType", "description"]
});

var StoreComboRiskType = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboRiskType",
  autoLoad: true,
  pageSize: 25,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListRiskTypeActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      rootProperty: "data",
      successProperty: "success"
    }
  }
});
Ext.define("DukeSource.view.risk.parameter.combos.ViewComboRiskType", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboRiskType",
  queryMode: "remote",
  displayField: "description",
  valueField: "idRiskType",

  editable: true,
  forceSelection: true,
  store: StoreComboRiskType
});
