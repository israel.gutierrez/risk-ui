Ext.define("ModelComboYearKri", {
  extend: "Ext.data.Model",
  fields: [{ name: "keyInt", type: "int" }, "description", "keyString"]
});

var StoreComboYearKri = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboYearKri",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboYearKri", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboYearKri",
  queryMode: "local",
  displayField: "description",
  valueField: "keyInt",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.callParent(arguments);
  },
  store: StoreComboYearKri
});
