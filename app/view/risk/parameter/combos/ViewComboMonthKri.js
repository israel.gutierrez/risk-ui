Ext.define("ModelComboMonthKri", {
  extend: "Ext.data.Model",
  fields: [{ name: "keyInt", type: "int" }, "description", "keyString"]
});

var StoreComboMonthKri = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboMonthKri",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboMonthKri", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboMonthKri",
  queryMode: "local",
  displayField: "description",
  valueField: "keyInt",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: StoreComboMonthKri
});
