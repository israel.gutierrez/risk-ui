Ext.define("ModelComboInsuranceCompany", {
  extend: "Ext.data.Model",
  fields: ["idInsuranceCompany", "description"]
});
var StoreComboInsuranceCompany = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboInsuranceCompany",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListInsuranceCompanyActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboInsuranceCompany", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboInsuranceCompany",
  queryMode: "remote",
  displayField: "description",
  valueField: "idInsuranceCompany",

  editable: true,
  forceSelection: true,
  store: StoreComboInsuranceCompany
});
