Ext.define("ModelAvailability", {
  extend: "Ext.data.Model",
  fields: ["idAvailability", "description"]
});

var storeAvailability = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  autoLoad: true,
  model: "ModelAvailability",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListAvailabilityActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboAvailability", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboAvailability",
  queryMode: "local",
  displayField: "description",
  valueField: "idAvailability",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: storeAvailability
});
