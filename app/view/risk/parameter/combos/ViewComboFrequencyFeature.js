Ext.define("ModelComboFrequencyFeature", {
  extend: "Ext.data.Model",
  fields: ["idFrequencyFeature", "description", "type"]
});

var StoreComboFrequencyFeature = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboFrequencyFeature",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListFrequencyFeatureActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboFrequencyFeature", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboFrequencyFeature",
  queryMode: "local",
  displayField: "description",
  valueField: "idFrequencyFeature",
  editable: true,
  forceSelection: true,
  store: StoreComboFrequencyFeature,
  tpl: Ext.create(
    "Ext.XTemplate",
    '<tpl for=".">',
    '<div class="x-boundlist-item"><span style="display: inline-block; width: 50%">{description}</span><span style="display: inline-block; width: 50%;text-align: right">{type}</span></div>',
    "</tpl>"
  ),

  displayTpl: Ext.create(
    "Ext.XTemplate",
    '<tpl for=".">',
    "{description} ({type}) ",
    "</tpl>"
  )
});
