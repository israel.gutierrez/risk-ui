Ext.define("ModelComboDetailControlType", {
  extend: "Ext.data.Model",
  fields: ["idDetailControlType", "idControlType", "typificationControl"]
});

var StoreComboDetailControlType = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboDetailControlType",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/findDetailControlType.htm",
    extraParams: {
      propertyFind: "1",
      propertyOrder: "valuetypificationControl"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboDetailControlType", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboDetailControlType",
  queryMode: "local",
  displayField: "typificationControl",
  valueField: "idDetailControlType",
  editable: true,
  forceSelection: true,
  store: StoreComboDetailControlType
});
