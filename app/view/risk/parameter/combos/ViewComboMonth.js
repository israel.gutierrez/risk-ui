var storeMonth = Ext.create("Ext.data.Store", {
  fields: ["id", "description"],
  data: [
    { id: "01", description: "ENERO" },
    { id: "02", description: "FEBRERO" },
    { id: "03", description: "MARZO" },
    { id: "04", description: "ABRIL" },

    { id: "05", description: "MAYO" },
    { id: "06", description: "JUNIO" },
    { id: "07", description: "JULIO" },
    { id: "08", description: "AGOSTO" },
    { id: "09", description: "SEPTIEMBRE" },
    { id: "10", description: "OCTUBRE" },
    { id: "11", description: "NOVIEMBRE" },
    { id: "12", description: "DICIEMBRE" }
  ]
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboMonth", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboMonth",
  queryMode: "local",
  displayField: "description",
  valueField: "id",
  editable: false,
  //    forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: storeMonth
});
