Ext.define("ModelComboMonthRatingPeriodEvent", {
  extend: "Ext.data.Model",
  fields: ["month", "descriptionMonth"]
});

var StoreComboMonthRatingPeriodEvent = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboMonthRatingPeriodEvent",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.combos.ViewComboMonthRatingPeriodEvent",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboMonthRatingPeriodEvent",
    queryMode: "local",
    displayField: "descriptionMonth",
    valueField: "month",

    editable: true,
    forceSelection: true,
    initComponent: function() {
      this.on("render", this.handleFieldRender, this);
      this.callParent(arguments);
    },
    handleFieldRender: function() {
      var me = this;
      me.store.load();
    },
    store: StoreComboMonthRatingPeriodEvent
  }
);
