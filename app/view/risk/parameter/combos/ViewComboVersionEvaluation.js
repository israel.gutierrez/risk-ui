Ext.define("ModelComboVersionEvaluation", {
  extend: "Ext.data.Model",
  fields: ["idVersionEvaluation", "description"]
});

var StoreComboVersionEvaluation = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboVersionEvaluation",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showVersionEvaluationActivesComboBox.htm",
    extraParams: {
      //            propertyOrder: 'description'
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboVersionEvaluation", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboVersionEvaluation",
  queryMode: "local",
  displayField: "description",
  valueField: "idVersionEvaluation",

  editable: true,
  forceSelection: true,
  //    initComponent: function(){
  //        this.on('render',this.handleFieldRender,this);
  //        this.callParent(arguments);
  //    },
  //    handleFieldRender: function(){
  //        var me = this;
  //        me.store.load();
  //    },
  store: StoreComboVersionEvaluation
});
