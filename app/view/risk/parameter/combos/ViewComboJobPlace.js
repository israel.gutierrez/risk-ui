Ext.define("ModelComboJobPlace", {
  extend: "Ext.data.Model",
  fields: ["idJobPlace", "description", "category"]
});

var storeComboJobPlace = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboJobPlace",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListJobPlaceActives.htm",
    extraParams: {
      propertyOrder: "jp.description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboJobPlace", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboJobPlace",
  plugins: ["ComboSelectCount"],
  queryMode: "local",
  displayField: "description",
  valueField: "idJobPlace",

  editable: true,
  forceSelection: true,
  typeAhead: true,
  store: storeComboJobPlace
});
