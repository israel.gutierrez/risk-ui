Ext.define('DukeSource.view.risk.parameter.combos.ViewComboControlType', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboControlTypeBean',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idControlTypeBean',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.parameter.combos.StoreComboControlType'
});
