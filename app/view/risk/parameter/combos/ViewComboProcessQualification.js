Ext.define("ModelComboProcessQualification", {
  extend: "Ext.data.Model",
  fields: ["description", "valueQualification", "idProcessQualification"]
});

var StoreComboProcessQualification = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboProcessQualification",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListProcessQualificationActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.combos.ViewComboProcessQualification",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboProcessQualification",
    queryMode: "local",
    displayField: "description",
    valueField: "idProcessQualification",

    editable: true,
    forceSelection: true,
    initComponent: function() {
      this.on("render", this.handleFieldRender, this);
      this.callParent(arguments);
    },
    handleFieldRender: function() {
      var me = this;
      me.store.load();
    },
    store: StoreComboProcessQualification
  }
);
