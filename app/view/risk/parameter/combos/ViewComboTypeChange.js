Ext.define('DukeSource.view.risk.parameter.combos.ViewComboTypeChange', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboTypeChangeBean',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idTypeChangeBean',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.parameter.combos.StoreComboTypeChange'
});
