Ext.define("ModelComboEventTwo", {
  extend: "Ext.data.Model",
  fields: ["idEventTwo", "description"]
});

var StoreComboEventTwo = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboEventTwo",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboEventTwo", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboEventTwo",
  queryMode: "local",
  displayField: "description",
  valueField: "idEventTwo",

  editable: true,
  forceSelection: true,
  fieldLabel: "EVENTOS N2",
  msgTarget: "side",
  store: StoreComboEventTwo
});
