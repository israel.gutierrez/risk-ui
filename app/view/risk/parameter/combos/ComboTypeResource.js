Ext.define("ModelComboTypeResource", {
  extend: "Ext.data.Model",
  fields: ["id", "description"]
});
var StoreComboTypeResource = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboTypeResource",
  pageSize: 9999,
  autoLoad: true,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListTypeResourceActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ComboTypeResource", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ComboTypeResource",
  queryMode: "local",
  displayField: "description",
  valueField: "id",

  editable: true,
  forceSelection: true,
  typeAhead: true,
  store: StoreComboTypeResource
});
