Ext.define("ModelComboRiskAnswer", {
  extend: "Ext.data.Model",
  fields: ["idRiskAnswer", "description"]
});

var StoreComboRiskAnswer = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboRiskAnswer",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListRiskAnswerActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboRiskAnswer", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboRiskAnswer",
  queryMode: "local",
  displayField: "description",
  valueField: "idRiskAnswer",

  editable: true,
  forceSelection: true,
  //    initComponent: function() {
  //        this.on('render',this.handleFieldRender,this);
  //        this.callParent(arguments);
  //    },
  //    handleFieldRender: function() {
  //        var me = this;
  //        me.store.load();
  //    },
  store: StoreComboRiskAnswer
});
