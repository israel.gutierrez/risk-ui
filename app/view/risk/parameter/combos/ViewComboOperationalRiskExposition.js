Ext.define("ModelComboOperationalRiskExposition", {
  extend: "Ext.data.Model",
  fields: [
    { name: "maxAmount", type: "number" },
    "idOperationalRiskExposition",
    "validity",
    "descriptionValidity"
  ]
});

var StoreComboOperationalRiskExposition = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboOperationalRiskExposition",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListOperationalRiskExpositionActivesComboBox.htm",
    extraParams: {
      propertyOrder: "state"
    },
    reader: {
      type: "json",
      rootProperty: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.combos.ViewComboOperationalRiskExposition",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboOperationalRiskExposition",
    queryMode: "local",
    displayField: "maxAmount",
    valueField: "idOperationalRiskExposition",

    editable: false,
    forceSelection: true,
    store: StoreComboOperationalRiskExposition,
    tpl: Ext.create(
      "Ext.XTemplate",
      '<tpl for=".">',
      '<div class="x-boundlist-item">{maxAmount} | {descriptionValidity}</div>',
      "</tpl>"
    ),

    displayTpl: Ext.create(
      "Ext.XTemplate",
      '<tpl for=".">',
      "{maxAmount} | {descriptionValidity}",
      "</tpl>"
    )
  }
);
