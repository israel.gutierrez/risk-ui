Ext.define("ModelComboSanctioningCompany", {
  extend: "Ext.data.Model",
  fields: ["idSanctioningCompany", "description"]
});
var StoreComboSanctioningCompany = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboSanctioningCompany",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListSanctioningCompanyActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.parameter.combos.ViewComboSanctioningCompany",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboSanctioningCompany",
    queryMode: "remote",
    displayField: "description",
    valueField: "idSanctioningCompany",

    editable: true,
    forceSelection: true,
    //    initComponent: function() {
    //        this.on('render',this.handleFieldRender,this);
    //        this.callParent(arguments);
    //    },
    //    handleFieldRender: function() {
    //        var me = this;
    //        me.store.load();
    //    },
    store: StoreComboSanctioningCompany
  }
);
