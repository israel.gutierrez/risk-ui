Ext.define("ModelComboFrequency", {
  extend: "Ext.data.Model",
  fields: ["idFrequency", "description", "percentage"]
});

var StoreComboFrequency = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboFrequency",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListFrequencyActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboFrequency", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboFrequency",
  queryMode: "local",
  displayField: "description",
  valueField: "idFrequency",
  editable: true,
  forceSelection: true,
  store: StoreComboFrequency
});
