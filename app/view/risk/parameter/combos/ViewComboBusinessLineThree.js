Ext.define("ModelComboBusinessLineThree", {
  extend: "Ext.data.Model",
  fields: ["idBusinessLineThree", "description"]
});

var StoreComboBusinessLineThree = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboBusinessLineThree",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboBusinessLineThree", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboBusinessLineThree",
  queryMode: "local",
  displayField: "description",
  valueField: "idBusinessLineThree",

  editable: true,
  forceSelection: true,
  fieldLabel: "LINEA NEG.3",
  msgTarget: "side",
  store: StoreComboBusinessLineThree
});
