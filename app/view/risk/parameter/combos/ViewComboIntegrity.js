Ext.define("ModelComboIntegrity", {
  extend: "Ext.data.Model",
  fields: ["idIntegrity", "description"]
});

var storeIntegrity = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboIntegrity",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListIntegrityActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboIntegrity", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboIntegrity",
  queryMode: "local",
  displayField: "description",
  valueField: "idIntegrity",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: storeIntegrity
});
