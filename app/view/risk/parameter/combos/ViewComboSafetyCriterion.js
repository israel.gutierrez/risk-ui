Ext.define("ModelComboSafetyCriterion", {
  extend: "Ext.data.Model",
  fields: ["idSafetyCriterion", "description"]
});

var storeComboSafetyCriterion = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboSafetyCriterion",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListSafetyCriterionActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboSafetyCriterion", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboSafetyCriterion",
  queryMode: "local",
  displayField: "description",
  valueField: "idSafetyCriterion",

  editable: true,
  forceSelection: true,
  multiSelect: true,
  delimiter: ",",
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: storeComboSafetyCriterion
});
