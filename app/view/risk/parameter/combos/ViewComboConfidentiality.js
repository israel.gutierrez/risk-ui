Ext.define("ModelConfidentiality", {
  extend: "Ext.data.Model",
  fields: ["idConfidentiality", "description"]
});

var storeConfidentiality = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelConfidentiality",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListConfidentialityActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboConfidentiality", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboConfidentiality",
  queryMode: "local",
  displayField: "description",
  valueField: "idConfidentiality",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: storeConfidentiality
});
