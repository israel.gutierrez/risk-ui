Ext.define("ModelComboBusinessLineTwo", {
  extend: "Ext.data.Model",
  fields: ["idBusinessLineTwo", "description"]
});

var StoreComboBusinessLineTwo = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboBusinessLineTwo",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboBusinessLineTwo", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboBusinessLineTwo",
  queryMode: "local",
  displayField: "description",
  valueField: "idBusinessLineTwo",

  editable: true,
  forceSelection: true,
  fieldLabel: "LINEA NEG.2",
  msgTarget: "side",
  store: StoreComboBusinessLineTwo
});
