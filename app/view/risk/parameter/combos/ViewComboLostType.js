Ext.define("ModelComboLostType", {
  extend: "Ext.data.Model",
  fields: ["idLostType", "description"]
});
var StoreComboLostType = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboLostType",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListLostTypeActivesComboBox.htm", //'showListLostTypeActives.htm',
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboLostType", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboLostType",
  queryMode: "remote",
  displayField: "description",
  valueField: "idLostType",
  msgTarget: "side",
  fieldLabel: "TIPOS DE P&Eacute;RDIDA",

  editable: true,
  forceSelection: true,
  store: StoreComboLostType
});
