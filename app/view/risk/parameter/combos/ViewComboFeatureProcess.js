Ext.define("ModelComboFeatureProcess", {
  extend: "Ext.data.Model",
  fields: ["idValuationFeatureProcess", "description", "valueFeature"]
});

var StoreComboFeatureProcess = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboFeatureProcess",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/findValuationFeatureProcess.htm",
    extraParams: {
      valueFind: "1"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.parameter.combos.ViewComboFeatureProcess", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboFeatureProcess",
  queryMode: "local",
  displayField: "description",
  valueField: "idValuationFeatureProcess",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: StoreComboFeatureProcess
});
