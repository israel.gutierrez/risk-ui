Ext.define("ModelComboScoreControl", {
  extend: "Ext.data.Model",
  fields: [
    "idScoreControl",
    "description",
    "codeColour",
    "scoreControl",
    "scoreMin",
    "scoreMax",
    "maxEffectiveness",
    "minEffectiveness"
  ]
});

var StoreComboScoreControl = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboScoreControl",
  pageSize: 9999,
  // autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListScoreControlActivesComboBox.htm",
    extraParams: {
      propertyFind: "typeControl",
      valueFind: "C",
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("DukeSource.view.risk.parameter.combos.ViewComboScoreControl", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboScoreControl",
  queryMode: "local",
  displayField: "description",
  valueField: "idScoreControl",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.callParent(arguments);
  },
  store: StoreComboScoreControl
});
