Ext.define("ModelComboTypeRiskEvaluation", {
  extend: "Ext.data.Model",
  fields: ["idTypeRiskEvaluation", "description"]
});
var StoreComboTypeRiskEvaluation = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboTypeRiskEvaluation",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListTypeRiskEvaluationActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.parameter.combos.ViewComboTypeRiskEvaluation",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboTypeRiskEvaluation",
    queryMode: "local",
    displayField: "description",
    valueField: "idTypeRiskEvaluation",

    editable: true,
    forceSelection: true,
    initComponent: function() {
      this.on("render", this.handleFieldRender, this);
      this.callParent(arguments);
    },
    handleFieldRender: function() {
      var me = this;
      me.store.load();
    },
    store: StoreComboTypeRiskEvaluation
  }
);
