Ext.define(
  "DukeSource.view.risk.parameter.evaluationrisk.ViewPanelRegisterImpact",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelRegisterImpact",
    border: false,
    layout: "fit",
    tbar: [
      {
        text: "NUEVO",
        iconCls: "add",
        action: "newImpact"
      },
      "-",
      {
        text: "ELIMINAR",
        iconCls: "delete",
        action: "deleteImpact"
      },
      "-",
      {
        xtype: "ViewComboOperationalRiskExposition",
        fieldLabel: "MAX.EXPOSICION.",
        listeners: {
          select: function(cbo, record) {
            var panel = cbo.up("ViewPanelRegisterImpact");
            var grid = panel.down("grid");
            if (record[0].data["validity"] == "N") {
              panel.down("button[action=newImpact]").setDisabled(true);
            } else {
              panel.down("button[action=newImpact]").setDisabled(false);
            }
            grid.store.getProxy().extraParams = { valueFind: cbo.getValue() };
            grid.store.getProxy().url =
              "http://localhost:9000/giro/findAuditImpact.htm";
            grid.down("pagingtoolbar").moveFirst();
          }
        }
      },
      "-",
      "->",
      {
        text: "AUDITORIA",
        action: "impactAuditory",
        iconCls: "auditory"
      }
    ],
    initComponent: function() {
      this.items = [
        { xtype: "ViewGridPanelRegisterImpact", padding: "2 2 2 2" }
      ];
      this.callParent();
    }
  }
);
