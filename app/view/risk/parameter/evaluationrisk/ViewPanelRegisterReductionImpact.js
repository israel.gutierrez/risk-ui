Ext.define('DukeSource.view.risk.parameter.evaluationrisk.ViewPanelRegisterReductionImpact', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterReductionImpact',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newPercentReductionImpact'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deletePercentReductionImpact'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchPercentReductionImpact',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'percentReductionImpactAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridRegisterReductionImpact', padding: '2 2 2 2'}];
        this.callParent();
    }
});
