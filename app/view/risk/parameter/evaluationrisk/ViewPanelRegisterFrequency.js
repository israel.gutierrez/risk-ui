Ext.define('DukeSource.view.risk.parameter.evaluationrisk.ViewPanelRegisterFrequency', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterFrequency',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newFrequency'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteFrequency'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchFrequency',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'frequencyAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterFrequency', padding: '2 2 2 2'}];
        this.callParent();
    }
});
