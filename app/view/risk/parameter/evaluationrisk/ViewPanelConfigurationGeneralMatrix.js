Ext.define("ModelGridMatrixScaleRisk", {
  extend: "Ext.data.Model",
  fields: []
});

var StoreGridMatrixScaleRisk = Ext.create("Ext.data.Store", {
  model: "ModelGridMatrixScaleRisk",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("ModelGridMatrixScaleBusinessLine", {
  extend: "Ext.data.Model",
  fields: ["cell1", "cell2", "cell3", "cell4", "cell5"]
});

var StoreGridMatrixScaleBusinessLine = Ext.create("Ext.data.Store", {
  model: "ModelGridMatrixScaleBusinessLine",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.parameter.evaluationrisk.ViewPanelConfigurationGeneralMatrix",
  {
    extend: "Ext.tab.Panel",
    alias: "widget.ViewPanelConfigurationGeneralMatrix",
    border: false,
    padding: "2 0 0 0",
    plain: true,
    layout: "fit",
    initComponent: function() {
      this.items = [
        {
          title: "Patrimonio efectivo",
          xtype: "ViewGridPanelRegisterOperationalRiskExposition",
          padding: 2
        },
        {
          title: "Configuración de matriz",
          xtype: "ViewGridPanelRegisterConfigurationMatrix",
          padding: 2
        }
      ];
      this.callParent();
    }
  }
);
