Ext.define('DukeSource.view.risk.parameter.evaluationrisk.ViewPanelRegisterFrequencyFeature', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterFrequencyFeature',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newFrequencyfeature'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteFrequencyfeature'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchFrequencyfeature',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'frequencyfeatureAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {
                xtype: 'ViewGridPanelRegisterFrequencyfeature',
                padding:
                    '2 2 2 2'
            }
        ];
        this.callParent();
    }
});
