Ext.define('DukeSource.view.risk.parameter.evaluationrisk.ViewPanelRegisterDetailImpactFeature', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterDetailImpactFeature',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newDetailImpacfeature'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteDetailImpacfeature'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDetailImpacfeature',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'detailImpacfeatureAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterDetailImpactFeature', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
