Ext.define('DukeSource.view.risk.parameter.evaluationrisk.ViewPanelRegisterRiskType', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterRiskType',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newRiskType'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteRiskType'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchRiskType',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'riskTypeAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterRiskType', padding: '2 2 2 2'}];
        this.callParent();
    }
});
