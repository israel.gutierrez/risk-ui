Ext.define('DukeSource.view.risk.parameter.evaluationrisk.ViewPanelRegisterRiskAnswer', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterRiskAnswer',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newRiskAnswer'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteRiskAnswer'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchRiskAnswer',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'riskAnswerAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterRiskAnswer', padding: '2 2 2 2'}];
        this.callParent();
    }
});
