Ext.define("ModelGridMatrixScaleBusinessLine", {
  extend: "Ext.data.Model",
  fields: ["cell1", "cell2", "cell3", "cell4", "cell5"]
});

var StoreGridMatrixScaleBusinessLine = Ext.create("Ext.data.Store", {
  model: "ModelGridMatrixScaleBusinessLine",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.parameter.evaluationrisk.ViewPanelRegisterScaleBusinessLine",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelRegisterScaleBusinessLine",
    border: false,
    layout: "border",
    tbar: [
      {
        text: "NUEVO",
        iconCls: "add",
        action: "newScaleBusinessLine"
      },
      "-",
      {
        text: "ELIMINAR",
        iconCls: "delete",
        action: "deleteScaleBusinessLine"
      },
      "-",
      {
        xtype: "ViewComboBusinessLineOne",
        fieldLabel: "LINEA DE NEGOCIO",
        labelWidth: 120,
        width: 300,
        listeners: {
          select: function(cbo, record) {
            var panel = cbo.up("ViewPanelRegisterScaleBusinessLine");
            var grid = panel.down("ViewGridPanelRegisterScaleBusinessLine");
            var grid2 = panel.down("grid[name=matrixScaleBusinessLine]");
            Ext.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/buildHeaderAxisX.htm",
              params: {
                valueFind: Ext.ComponentQuery.query(
                  "ViewPanelRegisterScaleBusinessLine ViewComboOperationalRiskExposition"
                )[0].getValue(),
                propertyOrder: "equivalentValue"
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  var columns = JSON.parse(response.data);
                  var fields = JSON.parse(response.fields);
                  DukeSource.global.DirtyView.createMatrix(
                    columns,
                    fields,
                    grid2
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            grid.store.getProxy().extraParams = {
              maxExposition: panel
                .down("ViewComboOperationalRiskExposition")
                .getValue(),
              businessLine: cbo.getValue()
            };
            grid.store.getProxy().url =
              "http://localhost:9000/giro/findScaleRiskByBusinessLineAndMaxExposition.htm";
            grid.down("pagingtoolbar").moveFirst();
            grid2.store.getProxy().extraParams = {
              maxExposition: panel
                .down("ViewComboOperationalRiskExposition")
                .getValue(),
              businessLine: cbo.getValue()
            };
            grid2.store.getProxy().url =
              "http://localhost:9000/giro/findMatrixAndLoad.htm";
            grid2.down("pagingtoolbar").moveFirst();
          }
        }
      },
      "-",
      {
        xtype: "ViewComboOperationalRiskExposition",
        fieldLabel: "MAX.EXPOSICION.",
        listeners: {
          select: function(cbo, record) {
            var panel = cbo.up("ViewPanelRegisterScaleBusinessLine");
            var grid = panel.down("ViewGridPanelRegisterScaleBusinessLine");
            var grid2 = panel.down("grid[name=matrixScaleBusinessLine]");
            if (record[0].data["validity"] == "N") {
              panel
                .down("button[action=newScaleBusinessLine]")
                .setDisabled(true);
              panel
                .down("button[action=deleteScaleBusinessLine]")
                .setDisabled(true);
              panel
                .down("button[action=generateMatrixScaleBusinessLine]")
                .setDisabled(true);
            } else {
              panel
                .down("button[action=newScaleBusinessLine]")
                .setDisabled(false);
              panel
                .down("button[action=deleteScaleBusinessLine]")
                .setDisabled(false);
              panel
                .down("button[action=generateMatrixScaleBusinessLine]")
                .setDisabled(false);
            }
            Ext.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/buildHeaderAxisX.htm",
              params: {
                valueFind: cbo.getValue(),
                propertyOrder: "equivalentValue"
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  var columns = JSON.parse(response.data);
                  var fields = JSON.parse(response.fields);
                  DukeSource.global.DirtyView.createMatrix(
                    columns,
                    fields,
                    grid2
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            grid.store.getProxy().extraParams = {
              maxExposition: cbo.getValue(),
              businessLine: panel.down("ViewComboBusinessLineOne").getValue()
            };
            grid.store.getProxy().url =
              "http://localhost:9000/giro/findScaleRiskByBusinessLineAndMaxExposition.htm";
            grid.down("pagingtoolbar").moveFirst();
            grid2.store.getProxy().extraParams = {
              maxExposition: cbo.getValue(),
              businessLine: panel.down("ViewComboBusinessLineOne").getValue()
            };
            grid2.store.getProxy().url =
              "http://localhost:9000/giro/findMatrixAndLoad.htm";
            grid2.down("pagingtoolbar").moveFirst();
          }
        }
      },
      "-",
      {
        text: "GENERAR MATRIZ",
        action: "generateMatrixScaleBusinessLine"
      },
      "-",
      "->",
      {
        text: "AUDITORIA",
        action: "scaleRiskAuditory",
        iconCls: "auditory"
      }
    ],
    initComponent: function() {
      this.items = [
        {
          xtype: "ViewGridPanelRegisterScaleBusinessLine",
          padding: "2 0 2 2",
          region: "center",
          flex: 1
        },
        {
          xtype: "gridpanel",
          padding: "2 2 2 2",
          region: "east",
          width: 700,
          name: "matrixScaleBusinessLine",
          store: StoreGridMatrixScaleBusinessLine,
          title: "MATRIZ EQUIVALENTE",
          loadMask: true,
          columnLines: true,
          columns: [],
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: StoreGridMatrixScaleBusinessLine,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          },
          listeners: {
            render: function() {
              var grid = this;
              DukeSource.global.DirtyView.loadGridDefault(grid);
            }
          }
        }
      ];
      this.callParent();
    }
  }
);
