Ext.define('DukeSource.view.risk.parameter.evaluationrisk.ViewPanelRegisterImpactFeature', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterImpactFeature',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newImpacfeature'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteImpacfeature'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchImpacfeature',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'impacfeatureAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterImpacfeature', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
