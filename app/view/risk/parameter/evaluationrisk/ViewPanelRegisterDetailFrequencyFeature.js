Ext.define('DukeSource.view.risk.parameter.evaluationrisk.ViewPanelRegisterDetailFrequencyFeature', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterDetailFrequencyFeature',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newDetailFrequencyfeature'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteDetailFrequencyfeature'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDetailFrequencyfeature',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'detailFrequencyfeatureAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterDetailFrequencyFeature', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
