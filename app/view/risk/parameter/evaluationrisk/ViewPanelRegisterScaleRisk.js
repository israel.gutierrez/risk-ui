Ext.define("ModelGridMatrixScaleRisk", {
  extend: "Ext.data.Model",
  fields: []
});

var StoreGridMatrixScaleRisk = Ext.create("Ext.data.Store", {
  model: "ModelGridMatrixScaleRisk",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.parameter.evaluationrisk.ViewPanelRegisterScaleRisk",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelRegisterScaleRisk",
    border: false,
    layout: "border",
    tbar: [
      {
        text: "NUEVO",
        iconCls: "add",
        action: "newScaleRisk"
      },
      "-",
      {
        text: "ELIMINAR",
        iconCls: "delete",
        action: "deleteScaleRisk"
      },
      "-",
      {
        xtype: "ViewComboOperationalRiskExposition",
        fieldLabel: "MAX.EXPOSICION.",
        listeners: {
          select: function(cbo, record) {
            var panel = cbo.up("ViewPanelRegisterScaleRisk");
            var grid = panel.down("grid");
            var grid2 = panel.down("grid[name=matrixScaleRisk]");
            if (record[0].data["validity"] == "N") {
              panel.down("button[action=newScaleRisk]").setDisabled(true);
              panel.down("button[action=deleteScaleRisk]").setDisabled(true);
              panel
                .down("button[action=generateMatrixScaleRisk]")
                .setDisabled(true);
            } else {
              panel.down("button[action=newScaleRisk]").setDisabled(false);
              panel.down("button[action=deleteScaleRisk]").setDisabled(false);
              panel
                .down("button[action=generateMatrixScaleRisk]")
                .setDisabled(false);
            }
            Ext.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/buildHeaderAxisX.htm",
              params: {
                valueFind: cbo.getValue(),
                propertyOrder: "equivalentValue"
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  var columns = JSON.parse(response.data);
                  var fields = JSON.parse(response.fields);
                  DukeSource.global.DirtyView.createMatrix(
                    columns,
                    fields,
                    grid2
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });

            grid.store.getProxy().extraParams = {
              maxExposition: cbo.getValue(),
              businessLine: "999"
            };
            grid.store.getProxy().url =
              "http://localhost:9000/giro/findScaleRiskByBusinessLineAndMaxExposition.htm";
            grid.down("pagingtoolbar").moveFirst();
            grid2.store.getProxy().extraParams = {
              maxExposition: cbo.getValue(),
              businessLine: "999"
            };
            grid2.store.getProxy().url =
              "http://localhost:9000/giro/findMatrixAndLoad.htm";
            grid2.down("pagingtoolbar").moveFirst();
          },
          render: function(me) {
            me.store.load({
              callback: function(cbo) {
                for (var i = 0; i < cbo.length; i++) {
                  if (cbo[i].data.validity == "S") {
                    me.setValue(cbo[i].data.idOperationalRiskExposition);
                    var grid2 = Ext.ComponentQuery.query(
                      "ViewPanelRegisterScaleRisk grid[name=matrixScaleRisk]"
                    )[0];
                    if (cbo != undefined) {
                      Ext.Ajax.request({
                        method: "POST",
                        url: "http://localhost:9000/giro/buildHeaderAxisX.htm",
                        params: {
                          valueFind: me.getValue(),
                          propertyOrder: "equivalentValue"
                        },
                        success: function(response) {
                          response = Ext.decode(response.responseText);
                          if (response.success) {
                            var columns = JSON.parse(response.data);
                            var fields = JSON.parse(response.fields);
                            DukeSource.global.DirtyView.createMatrix(
                              columns,
                              fields,
                              grid2
                            );
                            grid2.store.getProxy().extraParams = {
                              maxExposition: me.getValue(),
                              businessLine: "999"
                            };
                            grid2.store.getProxy().url =
                              "http://localhost:9000/giro/findMatrixAndLoad.htm";
                            grid2.down("pagingtoolbar").moveFirst();
                          } else {
                            DukeSource.global.DirtyView.messageAlert(
                              DukeSource.global.GiroMessages.TITLE_ERROR,
                              response.mensaje,
                              Ext.Msg.ERROR
                            );
                          }
                        },
                        failure: function() {}
                      });
                    }
                  }
                }
              }
            });
          }
        }
      },
      "-",
      {
        text: "GENERAR MATRIZ",
        action: "generateMatrixScaleRisk"
      },
      "-",
      "->",
      {
        text: "AUDITORIA",
        action: "scaleRiskAuditory",
        iconCls: "auditory"
      }
    ],
    initComponent: function() {
      this.items = [
        {
          xtype: "ViewGridPanelRegisterScaleRisk",
          padding: "2 0 2 2",
          region: "center",
          flex: 1
        },
        {
          xtype: "gridpanel",
          padding: "2 2 2 2",
          region: "east",
          width: 700,
          name: "matrixScaleRisk",
          store: StoreGridMatrixScaleRisk,
          title: "MATRIZ EQUIVALENTE",
          loadMask: true,
          columnLines: true,
          columns: [],

          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: StoreGridMatrixScaleRisk,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          }
        }
      ];
      this.callParent();
    }
  }
);
