Ext.define("ModelGridMatrixScaleRisk", {
  extend: "Ext.data.Model",
  fields: []
});

var StoreGridMatrixScaleRisk = Ext.create("Ext.data.Store", {
  model: "ModelGridMatrixScaleRisk",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.parameter.evaluationrisk.ViewPanelGenerateAllMatrix",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelGenerateAllMatrix",
    border: false,
    layout: "border",
    initComponent: function() {
      var idOperationalRiskExposition = this.idOperationalRiskExposition;
      var idTypeMatrix = this.idTypeMatrix;
      var originModify = this.originModify;
      this.items = [
        {
          xtype: "ViewGridPanelRegisterScaleRisk",
          region: "center",
          flex: 1,
          title: "Umbral anual de pérdida",
          listeners: {
            render: function(grid) {
              var windowConfiguration = Ext.ComponentQuery.query(
                "ViewWindowConfigurationMatrix"
              )[0];
              if (originModify == "modify") {
                grid.store.getProxy().extraParams = {
                  idTypeMatrix: idTypeMatrix,
                  idOperationalRiskExposition: idOperationalRiskExposition
                };
              } else {
                grid.store.getProxy().extraParams = {
                  idTypeMatrix: windowConfiguration
                    .down("ViewComboTypeMatrix")
                    .getValue(),
                  idOperationalRiskExposition: windowConfiguration
                    .down("ViewComboOperationalRiskExposition")
                    .getValue()
                };
              }
              grid.store.getProxy().url =
                "http://localhost:9000/giro/findInitialScaleRisk.htm";
              grid.down("pagingtoolbar").moveFirst();
            }
          },
          tbar: [
            {
              text: "Nuevo",
              iconCls: "add",
              action: "newScaleRisk"
            },
            "-",
            {
              text: "Eliminar",
              iconCls: "delete",
              action: "deleteScaleRisk"
            },
            "-",
            {
              text: "Generar matriz",
              iconCls: "operations",
              action: "generateMatrixScaleRisk"
            },
            "-",
            "->",
            {
              text: "Auditoria",
              action: "scaleRiskAuditory",
              iconCls: "auditory"
            }
          ]
        },
        {
          xtype: "gridpanel",
          region: "east",
          width: 700,
          name: "matrixScaleRisk",
          store: StoreGridMatrixScaleRisk,
          title: "MATRIZ RESULTANTE",
          loadMask: true,
          split: true,
          columnLines: true,
          columns: [],
          tbar: [
            {
              text: "Eliminar matriz",
              iconCls: "delete",
              action: "deleteMatrixScaleRisk"
            }
          ],
          viewConfig: {
            listeners: {
              celldblclick: function(
                view,
                cell,
                cellIndex,
                record,
                row,
                rowIndex,
                e
              ) {
                var clickedDataIndex = view.panel.headerCt.getHeaderAtIndex(
                  cellIndex
                ).dataIndex;
                var clickedCellValue = record.get(clickedDataIndex);
                var win = Ext.create(
                  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowModifyCellMatrix",
                  {
                    idMatrix: clickedCellValue["idMatrix"]
                  }
                ).show();
                win.down("toolbar").add(
                  {
                    xtype: "label",
                    text: "FREC : " + clickedCellValue["descriptionFrequency"]
                  },
                  "-",
                  {
                    xtype: "label",
                    text: "IMP : " + clickedCellValue["descriptionImpact"]
                  },
                  "-",
                  {
                    xtype: "textfield",
                    readOnly: true,
                    fieldStyle:
                      "background-color: #" +
                      clickedCellValue["colour"] +
                      "; background-image: none;",
                    value: clickedCellValue["valueCell"]
                  }
                );
              }
            }
          },
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: StoreGridMatrixScaleRisk,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          },
          listeners: {
            render: function() {
              var me = this;
              var windowConfiguration = Ext.ComponentQuery.query(
                "ViewWindowConfigurationMatrix"
              )[0];
              Ext.Ajax.request({
                method: "POST",
                url: "http://localhost:9000/giro/buildHeaderAxisX.htm",
                params: {
                  valueFind:
                    originModify == "modify"
                      ? idOperationalRiskExposition
                      : windowConfiguration
                          .down("ViewComboOperationalRiskExposition")
                          .getValue(),
                  idTypeMatrix:
                    originModify == "modify"
                      ? idTypeMatrix
                      : windowConfiguration
                          .down("ViewComboTypeMatrix")
                          .getValue(),
                  propertyOrder: "equivalentValue"
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    var columns = JSON.parse(response.data);
                    var fields = JSON.parse(response.fields);
                    DukeSource.global.DirtyView.createMatrix(
                      columns,
                      fields,
                      me
                    );
                    if (originModify == "modify") {
                      me.store.getProxy().extraParams = {
                        idTypeMatrix: idTypeMatrix,
                        idOperationalRiskExposition: idOperationalRiskExposition
                      };
                    } else {
                      me.store.getProxy().extraParams = {
                        idTypeMatrix: windowConfiguration
                          .down("ViewComboTypeMatrix")
                          .getValue(),
                        idOperationalRiskExposition: windowConfiguration
                          .down("ViewComboOperationalRiskExposition")
                          .getValue()
                      };
                    }
                    me.store.getProxy().url =
                      "http://localhost:9000/giro/findMatrixAndLoad.htm";
                    me.down("pagingtoolbar").moveFirst();
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_ERROR,
                      response.mensaje,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function() {}
              });
            }
          }
        }
      ];
      this.callParent();
    }
  }
);
