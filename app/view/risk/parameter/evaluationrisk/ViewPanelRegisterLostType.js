Ext.define('DukeSource.view.risk.parameter.evaluationrisk.ViewPanelRegisterLostType', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterLostType',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newLostType'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteLostType'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchLostType',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'lostTypeAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterLostType', padding: '2 2 2 2'}];
        this.callParent();
    }
});
