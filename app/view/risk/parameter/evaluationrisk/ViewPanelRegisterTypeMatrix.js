Ext.define('DukeSource.view.risk.parameter.evaluationrisk.ViewPanelRegisterTypeMatrix', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterTypeMatrix',
    border: false,
    layout: 'fit',

    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterTypeMatrix', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
