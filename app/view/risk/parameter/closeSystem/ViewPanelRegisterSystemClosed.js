Ext.define('DukeSource.view.risk.parameter.closeSystem.ViewPanelRegisterSystemClosed', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterSystemClosed',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newSystemClosed'
        }, '-',
        {
            text: 'MODIFICAR',
            iconCls: 'modify',
            action: 'modifySystemClosed'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteSystemClosed'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchSystemClosed',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'systemClosedAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterSystemClosed', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
