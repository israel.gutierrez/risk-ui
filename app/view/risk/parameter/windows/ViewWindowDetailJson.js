Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowDetailJson', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowDetailJson',
    height: 200,
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'DETALLES DE DIRECTORIO',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            itemId: 'description',
                            name: 'text',
                            fieldLabel: 'URL'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            itemId: 'entityModel',
                            name: 'entityModel',
                            fieldLabel: 'USUARIO'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            inputType: 'password',
                            itemId: 'nameFile',
                            name: 'nameFile',
                            fieldLabel: 'PASSWORD'
                        }

                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    action: 'saveFileJson',
                    scale: 'medium',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});

