Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowEventOne', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowEventOne',
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'EVENTO NIVEL 1',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            itemId: 'text',
                            name: 'text',
                            fieldLabel: 'DESCRIPCION'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    scale: 'medium',
                    action: 'saveEventTwo',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});
