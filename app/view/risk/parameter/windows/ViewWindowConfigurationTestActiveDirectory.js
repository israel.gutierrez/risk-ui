Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowConfigurationTestActiveDirectory', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowConfigurationTestActiveDirectory',
    height: 230,
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'CONFIGURACION DE DIRECTORIO ACTIVO',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            fieldCls: 'obligatoryTextField',
                            allowBlank: false,
                            itemId: 'domain',
                            name: 'domain',
                            labelWidth: 120,
                            anchor: '100%',
                            emptyText: 'teslat.com',
                            fieldLabel: 'SERVIDOR DOMINIO'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            fieldCls: 'obligatoryTextField',
                            allowBlank: false,
                            itemId: 'url',
                            name: 'url',
                            labelWidth: 120,
                            emptyText: 'ldap://teslat.com:389/',
                            fieldLabel: 'DIRECCION URL'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            fieldCls: 'obligatoryTextField',
                            allowBlank: false,
                            labelWidth: 120,
                            itemId: 'username',
                            name: 'username',
                            emptyText: 'supervisor',
                            fieldLabel: 'USUARIO'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            itemId: 'password',
                            labelWidth: 120,
                            fieldCls: 'obligatoryTextField',
                            allowBlank: false,
                            inputType: 'password',
                            name: 'password',
                            emptyText: 'password',
                            fieldLabel: 'PASSWORD'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'TEST',
                    action: 'testActiveDirectory',
                    scale: 'medium',
                    iconCls: 'save'
                }, {
                    text: 'LIMPIAR',
                    scale: 'medium',
                    iconCls: 'clear',
                    handler: function () {
                        me.down('form').getForm().reset();
                    }
                },
                {
                    text: 'SALIR',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });

        me.callParent(arguments);
    }

});

