Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowFeaturesProcess', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowFeaturesProcess',
    height: 150,
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'CARACTERISTICA DE PROCESO',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            itemId: 'text',
                            name: 'text',
                            fieldLabel: 'DESCRIPCION'
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            itemId: 'valuationConcept'
                        },
                        {
                            xtype: 'numberfield',
                            anchor: '100%',
                            minValue: 0,
                            maxValue: 100,
                            itemId: 'valueFeature',
                            name: 'fieldTwo',
                            fieldLabel: 'PESO'
                        }

                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    action: 'saveFeatureProcess',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });

        me.callParent(arguments);
    }

});
