Ext.define(
  "DukeSource.view.risk.parameter.windows.ViewWindowBusinessLineThree",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowBusinessLineThree",
    width: 500,
    border: false,
    layout: {
      type: "fit"
    },
    title: "LINEA DE NEGOCIO N3",
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            items: [
              {
                xtype: "textfield",
                itemId: "id",
                name: "id",
                hidden: true
              },
              {
                xtype: "UpperCaseTextFieldObligatory",
                anchor: "100%",
                allowBlank: false,
                itemId: "text",
                name: "text",
                fieldLabel: "DESCRIPCION"
              },
              {
                xtype: "combobox",
                anchor: "100%",
                fieldLabel: "MACROPRODUCTO",
                editable: false,
                hidden: hidden("VWBLTLineUp"),
                name: "lineUp",
                itemId: "lineUp",
                queryMode: "local",
                displayField: "description",
                valueField: "value",
                store: {
                  fields: ["value", "description"],
                  pageSize: 9999,
                  autoLoad: true,
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url:
                      "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                    extraParams: {
                      propertyOrder: "description",
                      propertyFind: "identified",
                      valueFind: "MACRO_PRODUCT"
                    },
                    reader: {
                      type: "json",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                }
              },
              {
                xtype: "ViewComboYesNo",
                anchor: "100%",
                fieldLabel: "IMPLEMENTADO",
                itemId: "active",
                name: "active",
                hidden: hidden("VWBLTActive")
              },
              {
                xtype: "textfield",
                itemId: "codeEntitySupervisor",
                name: "codeEntitySupervisor",
                fieldLabel: "CODIGO",
                hidden: hidden("VWBLTCodeEntitySupervisor")
              }
            ]
          }
        ],
        buttons: [
          {
            text: "GUARDAR",
            scale: "medium",
            action: "saveBusinessLine",
            iconCls: "save"
          },
          {
            text: "SALIR",
            scale: "medium",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);
