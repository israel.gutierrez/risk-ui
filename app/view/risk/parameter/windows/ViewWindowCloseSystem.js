Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowCloseSystem', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowCloseSystem',
//    height: 100,
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'REALIZAR CIERRE DEL SISTEMA',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'UpperCaseTextFieldReadOnly',
                            itemId: 'id',
                            name: 'idSystemClosed',
                            value: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'datefield',
                            format: 'd/m/Y',
                            allowBlank: false,
                            msgTarget: 'side',
                            anchor: '100%',
                            fieldCls: 'obligatoryTextField',
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            flex: 1,
                            fieldLabel: 'FECHA',
                            enforceMaxLength: true,
                            maxLength: 10,
                            name: 'dateClose',
                            listeners: {
                                expand: function (field, value) {
                                    field.setMaxValue(new Date());
                                }
                            }

                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            allowBlank: false,
                            width: 50,
                            anchor: '100%',
                            itemId: 'description',
                            name: 'description',
                            fieldLabel: 'DESCRIPCION'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    action: 'saveCloseSystem',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });

        me.callParent(arguments);
    }

});
