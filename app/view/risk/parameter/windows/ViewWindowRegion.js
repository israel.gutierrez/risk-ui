Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowRegion', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowRegion',
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'Region',
    titleAlign: 'center',
    buttonAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            itemId: 'text',
                            name: 'text',
                            fieldLabel: 'Nombre'
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'location',
                            fieldLabel: 'Ubigeo',
                            name: 'location'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    scale: 'medium',
                    action: 'saveCompany',
                    iconCls: 'save'
                },
                {
                    text: 'Salir',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});
