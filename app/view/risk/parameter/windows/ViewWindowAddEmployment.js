Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowAddEmployment', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowAddEmployment',
    height: 259,
    width: 445,
    border: false,
    layout: 'anchor',
    title: 'PUESTO',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            anchor: '100%',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldReadOnly',
                            name: 'correlative',
                            anchor: '100%',
                            fieldLabel: 'CODIGO'
                        }, {
                            xtype: 'ViewComboAgency',
                            fieldLabel: 'AGENCIA',
                            anchor: '100%',
                            name: 'agency',
                            fieldCls: 'obligatoryTextField',
                            allowBlank: false,
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            msgTarget: 'side'
                        },
                        {
                            xtype: 'ViewComboCategory',
                            anchor: '100%',
                            fieldLabel: 'CATEGORIA',
                            fieldCls: 'obligatoryTextField',
                            allowBlank: false,
                            msgTarget: 'side',
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: 'category',
                            listeners: {
                                specialkey: function (f, e) {
                                    DukeSource.global.DirtyView.focusEventEnterObligatory(f, e, Ext.ComponentQuery.query('ViewPanelUsers ViewComboAgency')[0])
                                }
                            }
                        }, {
                            xtype: 'datefield',
                            format: 'd/m/Y',
                            fieldLabel: 'FECHA DE INICIO',
                            name: 'dateStart',
                            anchor: '100%',
                            fieldCls: 'obligatoryTextField',
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            allowBlank: false,
                            maxLength: 10,
                            listeners: {
                                expand: function (field, value) {
                                    me.down('datefield[name=dateStart]').setMaxValue(new Date());
                                },
                                specialkey: function (f, e) {
                                    DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('datefield[name=dateDiscovery]'))
                                }
                            }
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            itemId: 'text',
                            anchor: '100%',
                            name: 'description',
                            fieldLabel: 'DESCRIPCION',
                            fieldCls: 'obligatoryTextField',
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            allowBlank: false
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    action: 'saveEmployment',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });
        me.callParent(arguments);
    }
});

