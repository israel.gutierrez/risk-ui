//Ext.define('ModelWindowWeightEvent', {
//    extend: 'Ext.data.Model',
//    fields: [
//        {name: 'idEventOne', type: 'string'},
//        {name: 'idScorecardMaster', type: 'string'},
//        {name: 'weight', type: 'BigDecimal'},
//        {name: 'state', type: 'string'}
//    ]
//});
Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowWeightEvent', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowWeightEvent',
    height: 208,
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'WeightEvent',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
//                    model:'ModelWindowWeightEvent',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'UpperCaseTextField',
                            anchor: '60%',
                            name: 'idEventOne',
                            value: 'id',
                            fieldLabel: 'idEventOne',
                            fieldCls: 'readOnlyText',
                            readOnly: true
                        },
                        {
                            xtype: 'UpperCaseTextField',
                            anchor: '60%',
                            name: 'idScorecardMaster',
                            value: 'id',
                            fieldLabel: 'IdScorecardMaster',
                            fieldCls: 'readOnlyText',
                            readOnly: true
                        },
                        {
                            xtype: 'UpperCaseTextField',
                            anchor: '100%',
                            name: 'weight',
                            fieldCls: 'UpperCaseTextFieldObligatory',
                            fieldLabel: 'weight'
                        },
                        {
                            xtype: 'UpperCaseTextField',
                            anchor: '100%',
                            name: 'state',
                            fieldCls: 'UpperCaseTextFieldObligatory',
                            fieldLabel: 'ESTADO'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    action: 'saveWeightEvent',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });

        me.callParent(arguments);
    }

});
