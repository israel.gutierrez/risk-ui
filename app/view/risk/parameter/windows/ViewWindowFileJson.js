Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowFileJson', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowFileJson',
    height: 150,
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'DOMINIO',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            itemId: 'description',
                            name: 'text',
                            fieldLabel: 'DOMINIO'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            itemId: 'entityModel',
                            name: 'entityModel',
                            fieldLabel: 'ENTIDAD DEL MODELO',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            itemId: 'nameFile',
                            name: 'nameFile',
                            fieldLabel: 'NOMBRE DEL ARCHIVO',
                            hidden: true
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    action: 'saveFileJson',
                    scale: 'medium',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});
