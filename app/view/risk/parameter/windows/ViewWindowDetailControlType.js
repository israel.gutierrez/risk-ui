Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowDetailControlType', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowDetailControlType',
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'DETALLE TIPO DE CONTROL',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '90%',
                            itemId: 'text',
                            name: 'text',
                            fieldLabel: 'Descripción'
                        },
                        {
                            xtype: 'numberfield',
                            allowBlank: false,
                            fieldCls: 'obligatoryTextField',
                            itemId: 'weightedControl',
                            name: 'fieldOne',
                            fieldLabel: 'Ponderación'
                        }

                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    scale: 'medium',
                    action: 'saveControlType',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});
