Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowValuationFeatureProcess', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowValuationFeatureProcess',
    height: 250,
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'CARACTERISTICA DE PROCESO',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
//                    model:'ModelWindowValuationFeatureProcess',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'UpperCaseTextField',
                            anchor: '60%',
                            name: 'idValuationFeatureProcess',
                            value: 'id',
                            fieldLabel: 'CODIGO'
                        },
                        {
                            xtype: 'ViewComboFeatureProcess',
                            anchor: '100%',
                            allowBlank: false,
                            msgTarget: 'side',
                            fieldCls: 'obligatoryTextField',
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: 'featureProcess',
                            fieldLabel: 'PROCESO FUNCION'
                        },
                        {
                            xtype: 'UpperCaseTextField',
                            anchor: '100%',
                            name: 'valueFeature',
                            fieldLabel: 'VALOR FUNCION'
                        },
                        {
                            xtype: 'UpperCaseTextField',
                            anchor: '100%',
                            name: 'description',
                            fieldLabel: 'DESCRIPCION'
                        },
                        {
                            xtype: 'UpperCaseTextField',
                            anchor: '100%',
                            name: 'valuationConcept',
                            fieldLabel: 'EVALUACIÓN CONCEPTO'
                        },
                        {
                            xtype: 'ViewComboYesNo',
                            anchor: '60%',
                            name: 'state',
                            allowBlank: false,
                            msgTarget: 'side',
                            fieldCls: 'UpperCaseTextFieldReadOnly',
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            fieldLabel: 'ESTADO'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    action: 'saveValuationFeatureProcess',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });

        me.callParent(arguments);
    }

});
