Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowAgency', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowAgency',
    height: 200,
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'Agencia',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            itemId: 'text',
                            name: 'text',
                            fieldLabel: 'Nombre'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '60%',
                            hidden: true,
                            itemId: 'codePAF',
                            name: 'codePAF',
                            fieldLabel: 'CODIGO PAF'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '60%',
                            itemId: 'zipCode',
                            name: 'zipCode',
                            fieldLabel: 'Ubigeo'
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'location',
                            name: 'location',
                            hidden: true
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    scale: 'medium',
                    action: 'saveCompany',
                    iconCls: 'save'
                },
                {
                    text: 'Salir',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});
