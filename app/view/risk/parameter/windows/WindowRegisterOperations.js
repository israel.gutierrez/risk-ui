Ext.define('DukeSource.view.risk.parameter.windows.WindowRegisterOperations', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowRegisterOperations',
    height: 200,
    width: 600,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'OPERACION',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    fieldDefaults: {
                        labelCls: 'changeSizeFontToEightPt',
                        fieldCls: 'changeSizeFontToEightPt'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'parent',
                            name: 'parent',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'depth',
                            name: 'depth',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'path',
                            name: 'path',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'state',
                            name: 'state',
                            value: 'S',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'idOperation',
                            name: 'idOperation',
                            value: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'codeEntitySupervisor',
                            itemId: 'codeEntitySupervisor',
                            fieldLabel: 'CODIGO'
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            height: 70,
                            name: 'description',
                            itemId: 'description',
                            fieldLabel: 'PROCESO'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    scale: 'medium',
                    action: 'saveOperation',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });
        me.callParent(arguments);
    }
});