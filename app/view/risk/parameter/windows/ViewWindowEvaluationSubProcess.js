Ext.define("ModelUserActives", {
  extend: "Ext.data.Model",
  fields: [
    "idFeaturesProcess",
    "descriptionFeaturesProcess",
    "descriptionValueFeatureProcess",
    "idValueFeaturesProcess",
    "value",
    "weight",
    "typeItem"
  ]
});

var storeUserActives = Ext.create("Ext.data.Store", {
  model: "ModelUserActives",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("ModelComboFeatureProcess", {
  extend: "Ext.data.Model",
  fields: ["idValuationFeatureProcess", "description", "valueFeature"]
});
var StoreComboFeatureProcess = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboFeatureProcess",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/findValuationFeatureProcess.htm",
    extraParams: {
      valueFind: "1"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.windows.ViewWindowEvaluationSubProcess",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowEvaluationSubProcess",
    layout: {
      align: "stretch",
      type: "vbox"
    },
    anchorSize: 100,
    title: "CALIFICAR PROCESO",
    titleAlign: "center",
    width: 700,
    height: 350,
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "container",
            flex: 3,
            padding: "2 2 2 2",
            layout: {
              align: "stretch",
              type: "hbox"
            },
            items: [
              {
                xtype: "gridpanel",
                padding: "0 1 0 0",
                store: storeUserActives,
                flex: 1,
                titleAlign: "center",
                plugins: [
                  Ext.create("Ext.grid.plugin.CellEditing", {
                    clicksToEdit: 1,
                    listeners: {
                      beforeedit: function(e) {
                        var Frequency = Ext.getStore(StoreComboFeatureProcess);
                        Frequency.loadData([], false);
                        var store = Frequency.load({
                          url:
                            "http://localhost:9000/giro/findValuationFeatureProcess.htm",
                          params: {
                            valueFind: me
                              .down("grid")
                              .getSelectionModel()
                              .getSelection()[0]
                              .get("idFeaturesProcess")
                          }
                        });
                      }
                    }
                  })
                ],
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    dataIndex: "descriptionFeaturesProcess",
                    flex: 1,
                    text: "CARACTERISTICA"
                  },
                  {
                    dataIndex: "weight",
                    width: 40,
                    align: "center",
                    text: "PESO"
                  },
                  {
                    header: "VALOR CARACTERISTICA",
                    tdCls: "custom-column",
                    dataIndex: "idValueFeaturesProcess",
                    flex: 1,
                    editor: {
                      xtype: "ViewComboFeatureProcess",
                      store: StoreComboFeatureProcess,
                      flex: 1,
                      msgTarget: "side",
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      allowBlank: false,
                      listeners: {
                        select: function(cbo, record) {
                          me.down("grid")
                            .getSelectionModel()
                            .getSelection()[0]
                            .set("value", record[0].data["valueFeature"]);
                          me.down("grid")
                            .getSelectionModel()
                            .getSelection()[0]
                            .set(
                              "descriptionValueFeatureProcess",
                              record[0].data["description"]
                            );
                          qualification(me, me.down("grid"));
                        }
                      }
                    },
                    renderer: function(val) {
                      var Frequency = Ext.getStore(StoreComboFeatureProcess);
                      var index = Frequency.findExact(
                        "idValuationFeatureProcess",
                        val
                      );
                      if (index != -1) {
                        var rs = Frequency.getAt(index).data;
                        return rs.description;
                      }
                    }
                  },
                  {
                    header: "DES. CARACTERISTICA",
                    dataIndex: "descriptionValueFeatureProcess",
                    flex: 1
                  },
                  {
                    header: "CALIFICACION",
                    align: "center",
                    dataIndex: "value"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: storeUserActives,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                }
              }
            ]
          },
          {
            xtype: "form",
            height: 45,
            padding: "2 2 2 2",
            bodyPadding: 10,
            items: [
              {
                xtype: "textfield",
                itemId: "id",
                name: "id",
                hidden: true
              },
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "ViewComboProcessQualification",
                    fieldCls: "obligatoryTextField",
                    readOnly: true,
                    name: "idProcessQualification",
                    padding: "0 10 0 0",
                    flex: 1.5,
                    fieldLabel: "CALIFICACION"
                  },
                  {
                    xtype: "UpperCaseTextFieldReadOnly",
                    name: "valueQualification",
                    flex: 1
                  }
                ]
              }
            ]
          }
        ],
        buttons: [
          {
            text: "CALIFICAR PROCESO",
            action: "saveEvaluationProcess",
            scale: "medium",
            iconCls: "save"
          },
          {
            text: "SALIR",
            scale: "medium",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);

function qualification(window, grid) {
  var comboQualification = window.down("ViewComboProcessQualification");
  var records = grid.getStore().getRange();
  var calf = 0;
  var total = grid.getStore().getCount();
  for (var int = 0; int < total; int++) {
    calf = calf + records[int].get("value") * 1;
  }

  var valueCombo = Math.round(calf / total);
  for (var i = 0; i < comboQualification.store.data.items.length; i++) {
    if (
      valueCombo ==
      comboQualification.store.data.items[i].data.valueQualification
    ) {
      comboQualification.setValue(
        comboQualification.store.data.items[i].data.idProcessQualification
      );
      window
        .down("UpperCaseTextFieldReadOnly[name=valueQualification]")
        .setValue(valueCombo + "");
    }
  }
}
