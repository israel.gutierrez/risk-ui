Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowCompany', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowCompany',
    height: 100,
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'Empresa',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'location',
                            name: 'location',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            itemId: 'text',
                            name: 'text',
                            fieldLabel: 'Nombre'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    action: 'saveCompany',
                    iconCls: 'save'
                },
                {
                    text: 'Salir',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });
        me.callParent(arguments);
    }
});