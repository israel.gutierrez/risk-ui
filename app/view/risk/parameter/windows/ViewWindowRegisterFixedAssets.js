Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowRegisterFixedAssets', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowRegisterFixedAssets',
    height: 240,
    width: 750,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'ACTIVO DE INFORMACIÓN',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            hidden: true,
                            value: 'id',
                            name: 'idFixedAssets'
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            name: 'description',
                            itemId: 'description',
                            allowBlank: false,
                            fieldLabel: 'ACTIVO DE INFORMACIÓN',
                            labelWidth: 120
                        },
                        {
                            xtype: 'ViewComboWorkArea',
                            anchor: '100%',
                            name: 'owner',
                            fieldCls: 'obligatoryTextField',
                            fieldLabel: 'PROPIETARIO',
                            allowBlank: false,
                            queryMode: 'remote',
                            labelWidth: 120
                        },
                        {
                            xtype: 'ViewComboJobPlace',
                            anchor: '100%',
                            fieldCls: 'obligatoryTextField',
                            name: 'technicalCustodio',
                            allowBlank: false,
                            fieldLabel: 'CUSTODIO TÉCNICO',
                            labelWidth: 120
                        },
                        {
                            xtype: 'container',
                            height: 26,
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'ViewComboConfidentiality',
                                    flex: 1,
                                    fieldCls: 'obligatoryTextField',
                                    allowBlank: false,
                                    name: 'confidentiality',
                                    fieldLabel: 'CONFIDENCIALIDAD',
                                    labelWidth: 120
                                },
                                {
                                    xtype: 'ViewComboIntegrity',
                                    flex: 1,
                                    fieldCls: 'obligatoryTextField',
                                    allowBlank: false,
                                    name: 'integrity',
                                    padding: '0 5 0 5',
                                    fieldLabel: 'INTEGRIDAD',
                                    labelWidth: 80
                                },
                                {
                                    xtype: 'ViewComboAvailability',
                                    flex: 1,
                                    fieldCls: 'obligatoryTextField',
                                    allowBlank: false,
                                    name: 'availability',
                                    fieldLabel: 'DISPONIBILIDAD'
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    scale: 'medium',
                    action: 'saveFixedAssets',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});
