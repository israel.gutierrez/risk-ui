Ext.define('DukeSource.view.risk.parameter.windows.WindowAuthorizedWorkArea', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowAuthorizedWorkArea',
    height: 470,
    width: 750,
    layout: {
        type: 'fit'
    },
    title: 'Aprobadores por Area',
    titleAlign: 'center',
    border: true,
    tbar: [
        {
            text: 'Agregar',
            iconCls: 'add',
            action: 'newAuthorizedWorkArea'
        }, '-',
        {
            text: 'Modificar',
            iconCls: 'modify',
            action: 'modifyAuthorizedWorkArea'
        }, '-',
        {
            text: 'Eliminar',
            iconCls: 'delete',
            action: 'deleteAuthorizedWorkArea'
        }
    ],
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'ViewGridAuthorizedWorkArea',
                    border: false
                }
            ],
            buttons: [
                {
                    text: 'Salir',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });
        me.callParent(arguments);
    }
});

