Ext.define("ModelComboBusinessLineOneTypeMatrix", {
  extend: "Ext.data.Model",
  fields: ["idBusinessLineOne", "description", "stateAssigned"]
});

var StoreComboBusinessLineOneTypeMatrix = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboBusinessLineOneTypeMatrix",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/findBusinessLineOneStateAssigned.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
StoreComboBusinessLineOneTypeMatrix.load();
Ext.define(
  "DukeSource.view.risk.parameter.windows.ViewWindowRegisterTypeMatrix",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowRegisterTypeMatrix",
    width: 500,
    border: false,
    modal: true,
    layout: {
      type: "fit"
    },
    title: "Matrices",
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            items: [
              {
                xtype: "textfield",
                itemId: "idTypeMatrix",
                value: "id",
                name: "idTypeMatrix",
                hidden: true
              },
              {
                xtype: "UpperCaseTextFieldObligatory",
                labelWidth: 140,
                anchor: "100%",
                itemId: "description",
                name: "description",
                fieldLabel: "Nombre"
              },
              {
                xtype: "ViewComboBusinessLineOne",
                queryMode: "local",
                editable: false,
                labelWidth: 140,
                hidden: true,
                anchor: "100%",
                itemId: "idBusinessLineOne",
                name: "idBusinessLineOne",
                fieldLabel: "Linea de negocio",
                store: StoreComboBusinessLineOneTypeMatrix,
                tpl: Ext.create(
                  "Ext.XTemplate",
                  '<tpl for=".">',
                  '<div class="x-boundlist-item">{description} | {stateAssigned}</div>',
                  "</tpl>"
                ),
                displayTpl: Ext.create(
                  "Ext.XTemplate",
                  '<tpl for=".">',
                  "{description} | {stateAssigned}",
                  "</tpl>"
                ),
                listeners: {
                  select: function(cbo, record) {
                    if (record[0].data["stateAssigned"] == "ASIGNADO") {
                      me.down(
                        "button[action=saveRelationMatrixToBusinessLine]"
                      ).setDisabled(true);
                    } else {
                      me.down(
                        "button[action=saveRelationMatrixToBusinessLine]"
                      ).setDisabled(false);
                    }
                  }
                }
              }
            ]
          }
        ],
        buttons: [
          {
            text: "Salir",
            scale: "medium",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          },
          {
            text: "Guardar",
            scale: "medium",
            action: "saveRelationMatrixToBusinessLine",
            iconCls: "save"
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);
