Ext.define("ModelUserActives", {
  extend: "Ext.data.Model",
  fields: [
    "userName",
    "fullName",
    "documentNumber",
    "email",
    "descriptionWorKArea"
  ]
});

var storeUserActives = Ext.create("Ext.data.Store", {
  model: "ModelUserActives",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
//storeUserActives.load();

Ext.define(
  "DukeSource.view.risk.parameter.windows.ViewWindowSearchTypeChange",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowSearchTypeChange",
    layout: {
      align: "stretch",
      type: "vbox"
    },
    anchorSize: 100,
    title: "BUSCAR USUARIO",
    titleAlign: "center",
    width: 500,
    height: 350,
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            height: 45,
            padding: "2 2 2 2",
            bodyPadding: 10,
            items: [
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "combobox",
                    value: "2",
                    labelWidth: 40,
                    width: 190,
                    //                                    flex:1,
                    fieldLabel: "TIPO",
                    store: [
                      ["1", "USUARIO"],
                      ["2", "NOMBRE COMPLETO"]
                    ]
                  },
                  {
                    xtype: "UpperCaseTextField",
                    //    action:'searchUserActives',
                    flex: 2,

                    //                                    fieldLabel: 'BUSCAR PER',
                    listeners: {
                      afterrender: function(field) {
                        field.focus(false, 200);
                      },
                      specialkey: function(field, e) {
                        var property = "";
                        if (
                          Ext.ComponentQuery.query(
                            "ViewWindowSearchTypeChange combobox"
                          )[0].getValue() == "1"
                        ) {
                          property = "username";
                        } else {
                          property = "fullName";
                        }
                        if (e.getKey() === e.ENTER) {
                          var grid = me.down("grid");
                          var toolbar = grid.down("pagingtoolbar");
                          DukeSource.global.DirtyView.searchPaginationGridToEnter(
                            field,
                            grid,
                            toolbar,
                            "http://localhost:9000/giro/findMatchUser.htm",
                            property,
                            "username"
                          );
                        }
                      }
                    }
                  }
                ]
              }
            ]
          },
          {
            xtype: "container",
            flex: 3,
            padding: "2 2 2 2",
            layout: {
              align: "stretch",
              type: "hbox"
            },
            items: [
              {
                xtype: "gridpanel",
                padding: "0 1 0 0",
                store: storeUserActives,
                flex: 1,
                titleAlign: "center",
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    dataIndex: "userName",
                    width: 120,
                    text: "CODIGO"
                  },
                  {
                    dataIndex: "fullName",
                    flex: 1,
                    text: "NOMBRE"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: storeUserActives,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  render: function() {
                    var me = this;
                    DukeSource.global.DirtyView.searchPaginationGridNormal(
                      "",
                      me,
                      me.down("pagingtoolbar"),
                      "http://localhost:9000/giro/findMatchUser.htm",
                      "fullName",
                      "fullName"
                    );
                  }
                }
              }
            ]
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
