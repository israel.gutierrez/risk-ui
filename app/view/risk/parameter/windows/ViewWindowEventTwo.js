Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowEventTwo', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowEventTwo',
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'Tipo de evento nivel 2',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            itemId: 'text',
                            name: 'text',
                            fieldLabel: 'Nombre'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            itemId: 'codeEntitySupervisor',
                            name: 'codeEntitySupervisor',
                            fieldLabel: getName('WETCodeEntitySupervisor')
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    scale: 'medium',
                    action: 'saveEventTwo',
                    iconCls: 'save'
                },
                {
                    text: 'Salir',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});
