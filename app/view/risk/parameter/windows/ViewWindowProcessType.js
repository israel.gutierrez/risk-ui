Ext.define("DukeSource.view.risk.parameter.windows.ViewWindowProcessType", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowProcessType",
  width: 500,
  border: false,
  layout: {
    type: "fit"
  },
  title: "Macroproceso",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          title: "",
          items: [
            {
              xtype: "textfield",
              itemId: "id",
              name: "id",
              hidden: true
            },
            {
              xtype: "textfield",
              itemId: "idProcessType",
              name: "idProcessType",
              value: "id",
              hidden: true
            },
            {
              xtype: "textfield",
              itemId: "path",
              name: "path",
              hidden: true
            },
            {
              xtype: "container",
              layout: {
                type: "hbox"
              },
              items: [
                {
                  xtype: "UpperCaseTextFieldObligatory",
                  itemId: "description",
                  name: "description",
                  enforceMaxLength: true,
                  maxLength: 600,
                  fieldLabel: "Nombre",
                  labelAlign: "top",
                  flex: 3,
                  listeners: {
                    afterrender: function(field) {
                      field.focus(false, 200);
                    }
                  }
                },
                {
                  xtype: "textfield",
                  itemId: "alias",
                  name: "alias",
                  allowBlank: false,
                  padding: "0 0 0 5",
                  flex: 1,
                  fieldCls: "obligatoryTextField",
                  labelAlign: "top",
                  maxLength: 100,
                  fieldLabel: "Alias"
                }
              ]
            },
            {
              xtype: "textfield",
              labelAlign: "top",
              padding: "5 0 0 0",
              itemId: "abbreviation",
              name: "abbreviation",
              maxLength: 100,
              fieldLabel: "Código"
            },
            {
              xtype: "combobox",
              fieldLabel: "Tipo",
              padding: "5 0 0 0",
              labelAlign: "top",
              queryMode: "local",
              name: "type",
              itemId: "type",
              displayField: "description",
              valueField: "value",
              store: {
                fields: ["value", "description"],
                pageSize: 9999,
                autoLoad: true,
                proxy: {
                  actionMethods: {
                    create: "POST",
                    read: "POST",
                    update: "POST"
                  },
                  type: "ajax",
                  url:
                    "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                  extraParams: {
                    propertyOrder: "description",
                    propertyFind: "identified",
                    valueFind: "TYPEMACROPROCESS"
                  },
                  reader: {
                    type: "json",
                    root: "data",
                    successProperty: "success"
                  }
                }
              }
            }
          ]
        }
      ],
      buttons: [
        {
          text: "Guardar",
          scale: "medium",
          action: "saveProcess",
          iconCls: "save"
        },
        {
          text: "Salir",
          scale: "medium",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});
