//Ext.define('ModelWindowScorecardMaster', {
//    extend: 'Ext.data.Model',
//    fields: [
//        {name: 'idScorecardMaster', type: 'string'},
//        {name: 'description', type: 'string'},
//        {name: 'state', type: 'string'}
//    ]
//});
Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowScorecardMaster', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowScorecardMaster',
    height: 208,
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'ScorecardMaster',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
//                    model:'ModelWindowScorecardMaster',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '60%',
                            name: 'idScorecardMaster',
                            value: 'id',
                            fieldLabel: 'CODIGO',
                            fieldCls: 'readOnlyText',
                            readOnly: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            name: 'description',
                            fieldCls: 'obligatoryTextField',
                            fieldLabel: 'DESCRIPCION'
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            name: 'state',
                            fieldCls: 'obligatoryTextField',
                            fieldLabel: 'ESTADO'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    action: 'saveScorecardMaster',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });

        me.callParent(arguments);
    }

});
