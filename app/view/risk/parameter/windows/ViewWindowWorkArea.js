Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowWorkArea', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowWorkArea',
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'AREA DE TRABAJO',
    titleAlign: 'center',
    buttonAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'parent',
                            name: 'parent',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'state',
                            name: 'state',
                            value: 'S',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'idWorkArea',
                            name: 'idWorkArea',
                            value: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'levelArea',
                            name: 'levelArea',
                            value: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'path',
                            name: 'path',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'codeMigration',
                            name: 'codeMigration',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            name: 'description',
                            itemId: 'description',
                            labelWidth: 125,
                            fieldLabel: 'Area'
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            readOnly: true,
                            labelWidth: 125,
                            itemId: 'descriptionParent',
                            name: 'descriptionParent',
                            fieldLabel: 'Area superior'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            itemId: 'costCenter',
                            labelWidth: 125,
                            name: 'costCenter',
                            fieldLabel: 'Centro de costo'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    scale: 'medium',
                    action: 'saveWorkArea',
                    iconCls: 'save'
                },
                {
                    text: 'Salir',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });

        me.callParent(arguments);
    }

});
