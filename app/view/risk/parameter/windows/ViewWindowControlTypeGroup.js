Ext.define(
  "DukeSource.view.risk.parameter.windows.ViewWindowControlTypeGroup",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowControlTypeGroup",
    width: 479,
    border: false,
    layout: {
      type: "fit"
    },
    title: "CLASIFICACIÓN DE CONTROL",
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            items: [
              {
                xtype: "textfield",
                itemId: "id",
                name: "id",
                hidden: true
              },
              {
                xtype: "combobox",
                anchor: "90%",
                itemId: "typeControl",
                name: "typeControl",
                editable: false,
                fieldLabel: getName("WRCTypeControl"),
                hidden: hidden("WRCTypeControl"),
                labelWidth: 100,
                allowBlank: true,
                queryMode: "local",
                displayField: "description",
                valueField: "value",
                value: "C",
                store: {
                  fields: ["value", "description"],
                  pageSize: 9999,
                  autoLoad: true,
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url:
                      "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                    extraParams: {
                      propertyOrder: "description",
                      propertyFind: "identified",
                      valueFind: "TYPECONTROL"
                    },
                    reader: {
                      type: "json",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                },
                listeners: {
                  select: function(cbo) {
                    if (cbo.getValue() == "S") {
                    }
                  }
                }
              },
              {
                xtype: "UpperCaseTextFieldObligatory",
                anchor: "90%",
                itemId: "text",
                name: "text",
                fieldLabel: "DESCRIPCION"
              },
              {
                xtype: "numberfield",
                allowBlank: false,
                fieldCls: "obligatoryTextField",
                itemId: "percentage",
                name: "percentage",
                value: 0,
                minValue: 0,
                maxValue: 100,
                fieldLabel: "PESO (%)"
              },
              {
                xtype: "textfield",
                itemId: "abbreviation",
                hidden: false,
                name: "abbreviation",
                fieldLabel: "ABREVIATURA"
              }
            ]
          }
        ],
        buttons: [
          {
            text: "GUARDAR",
            scale: "medium",
            action: "saveControlTypeGroup",
            iconCls: "save"
          },
          {
            text: "SALIR",
            scale: "medium",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);
