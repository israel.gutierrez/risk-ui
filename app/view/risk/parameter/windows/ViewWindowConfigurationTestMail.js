Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowConfigurationTestMail', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowConfigurationTestMail',
    width: 600,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'Configuración de correo',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    fieldDefaults: {
                        labelCls: 'changeSizeFontToEightPt',
                        fieldCls: 'changeSizeFontToEightPt'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'nameClass',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'id',
                            value: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'fieldset',
                            title: 'Servidor SMTP',
                            layout: {
                                type: 'anchor'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    height: 26,
                                    layout: {
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            itemId: 'host',
                                            fieldCls: 'obligatoryTextField',
                                            allowBlank: false,
                                            name: 'host',
                                            labelWidth: 80,
                                            flex: 3,
                                            emptyText: 'mail.tesla.com.pe',
                                            fieldLabel: 'SMTP host'
                                        },
                                        {
                                            xtype: 'textfield',
                                            padding: '0 0 0 5',
                                            flex: 1,
                                            fieldCls: 'obligatoryTextField',
                                            allowBlank: false,
                                            itemId: 'port',
                                            name: 'port',
                                            labelWidth: 50,
                                            emptyText: '995',
                                            fieldLabel: 'Puerto'
                                        }

                                    ]
                                },
                                {
                                    xtype: 'checkbox',
                                    name: 'permitTTLS',
                                    flex: 1,
                                    itemId: 'permitTTLS',
                                    inputValue: "true",
                                    uncheckedValue: "false",
                                    checked: false,
                                    labelAlign: 'right',
                                    margin: '0 0 0 85',
                                    boxLabel: 'TLS'
                                },
                                {
                                    xtype: 'checkbox',
                                    name: 'authorization',
                                    flex: 1,
                                    itemId: 'authorization',
                                    inputValue: "true",
                                    uncheckedValue: "false",
                                    checked: false,
                                    margin: '0 0 10 85',
                                    labelAlign: 'right',
                                    boxLabel: 'Requiere login',
                                    changeBox: function (cbx) {
                                        if (cbx.getValue()) {
                                            me.down('#userName').setReadOnly(false);
                                            me.down('#password').setReadOnly(false);
                                            me.down('#password').setFieldStyle('background: #d9ffdb');
                                            me.down('#userName').setFieldStyle('background: #d9ffdb');
                                            me.down('#password').allowBlank = false;
                                            me.down('#userName').allowBlank = false;

                                        } else {
                                            me.down('#userName').setReadOnly(true);
                                            me.down('#password').setReadOnly(true);
                                            me.down('#password').setFieldStyle('background: #d8d8d8');
                                            me.down('#userName').setFieldStyle('background: #d8d8d8');
                                            me.down('#password').allowBlank = true;
                                            me.down('#userName').allowBlank = true;
                                        }
                                    },
                                    listeners: {
                                        beforerender: function (cbx) {
                                            this.changeBox(cbx);
                                        },
                                        change: function (cbx) {
                                            this.changeBox(cbx);
                                        }
                                    }
                                },
                                {
                                    xtype: 'container',
                                    height: 26,
                                    layout: {
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            itemId: 'userName',
                                            allowBlank: false,
                                            margin: '0 0 0 110',
                                            flex: 3,
                                            fieldCls: 'obligatoryTextField',
                                            name: 'userName',
                                            labelWidth: 60,
                                            emptyText: 'usuario',
                                            fieldLabel: 'Usuario'
                                        },
                                        {
                                            xtype: 'textfield',
                                            itemId: 'password',
                                            padding: '0 0 0 10',
                                            flex: 2,
                                            fieldCls: 'obligatoryTextField',
                                            allowBlank: false,
                                            labelWidth: 60,
                                            inputType: 'password',
                                            name: 'password',
                                            emptyText: 'password',
                                            fieldLabel: 'Password'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            title: 'Test',
                            layout: {
                                type: 'anchor'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    vtype: 'email',
                                    labelWidth: 80,
                                    anchor: '75%',
                                    fieldCls: 'obligatoryTextField',
                                    allowBlank: false,
                                    itemId: 'emailAccount',
                                    name: 'emailAccount',
                                    emptyText: 'tesla@teslatec.pe',
                                    fieldLabel: 'De'
                                },
                                {
                                    xtype: 'textfield',
                                    vtype: 'email',
                                    labelWidth: 80,
                                    anchor: '75%',
                                    itemId: 'to',
                                    name: 'to',
                                    fieldCls: 'obligatoryTextField',
                                    allowBlank: false,
                                    emptyText: 'destino@empresa.com',
                                    fieldLabel: 'Para'
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    action: 'saveEmail',
                    scale: 'medium',
                    iconCls: 'save'
                },
                {
                    text: 'Salir',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});