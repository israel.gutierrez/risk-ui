Ext.define('DukeSource.view.risk.parameter.windows.WindowFrequencyProfileRisk', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowFrequencyProfileRisk',
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'FRECUENCIA - PERFIL DE RIESGO',
    titleAlign: 'center',
    buttonAlign: 'center',
    initComponent: function () {
        var me = this;
        var rowBack = this.rowBack;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'idFrequency',
                            name: 'idFrequency',
                            value: rowBack.get('idFrequency'),
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'state',
                            name: 'state',
                            value: 'S',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            name: 'descriptionProfileRisk',
                            itemId: 'descriptionProfileRisk',
                            fieldLabel: 'CONCEPTO'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    scale: 'medium',
                    action: 'updateFrequencyProfileRisk',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });
        me.callParent(arguments);
    }
});