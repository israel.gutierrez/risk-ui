Ext.define(
  "DukeSource.view.risk.parameter.windows.ViewWindowAssignJobPlaceProcess",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowAssignJobPlaceProcess",
    width: 479,
    border: false,
    layout: {
      type: "fit"
    },
    title: "ASIGNAR RESPONSABLE",
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            title: "",
            items: [
              {
                xtype: "textfield",
                itemId: "id",
                name: "id",
                hidden: true
              },
              {
                xtype: "ViewComboWorkArea",
                anchor: "100%",
                allowBlank: false,
                msgTarget: "side",
                queryMode: "remote",
                fieldCls: "obligatoryTextField",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                fieldLabel: "AREA",
                itemId: "idWorkArea",
                name: "idWorkArea",
                listeners: {
                  select: function(cbo) {
                    me.down("ViewComboJobPlace")
                      .getStore()
                      .load({
                        url:
                          "http://localhost:9000/giro/showListJobPlaceByWorkArea.htm",
                        params: {
                          workArea: cbo.getValue()
                        },
                        callback: function() {
                          me.down("ViewComboJobPlace").reset();
                          me.down("ViewComboJobPlace").setDisabled(false);
                        }
                      });
                  }
                }
              },
              {
                xtype: "ViewComboJobPlace",
                anchor: "100%",
                fieldLabel: "CARGO",
                allowBlank: false,
                msgTarget: "side",
                disabled: true,
                fieldCls: "obligatoryTextField",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                itemId: "idJobPlace",
                name: "idJobPlace"
              }
            ]
          }
        ],
        buttons: [
          {
            text: "GUARDAR",
            action: "saveAssignJobPlaceToProcess",
            iconCls: "save"
          },
          {
            text: "SALIR",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
