Ext.define("DukeSource.view.risk.parameter.windows.WindowAccountingEvent", {
  extend: "Ext.window.Window",
  alias: "widget.WindowAccountingEvent",
  width: 750,
  layout: {
    type: "fit"
  },
  title: "Contabilizaci&oacute;n del evento",
  titleAlign: "center",
  border: true,
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          layout: "anchor",
          border: false,
          bodyPadding: 5,
          fieldDefaults: {
            labelCls: "changeSizeFontToEightPt",
            fieldCls: "changeSizeFontToEightPt"
          },
          items: [
            {
              xtype: "textfield",
              hidden: true,
              itemId: "idEventApproved",
              name: "idEventApproved",
              value: "id"
            },
            {
              xtype: "textfield",
              hidden: true,
              itemId: "idEvent",
              name: "idEvent"
            },
            {
              xtype: "textfield",
              hidden: true,
              value: new Date(),
              itemId: "dateRegister",
              name: "dateRegister"
            },
            {
              xtype: "textfield",
              hidden: true,
              value: userName,
              itemId: "user",
              name: "user"
            },
            {
              xtype: "textfield",
              hidden: true,
              itemId: "eventState",
              name: "eventState"
            },
            {
              xtype: "textfield",
              hidden: true,
              itemId: "sequenceEventState",
              name: "sequenceEventState"
            },
            {
              xtype: "textfield",
              hidden: true,
              itemId: "approve",
              name: "approve",
              value: "A"
            },
            {
              xtype: "textfield",
              hidden: true,
              itemId: "order",
              name: "order"
            },
            {
              xtype: "textfield",
              hidden: true,
              itemId: "orderApproved",
              name: "orderApproved",
              value: 1
            },
            {
              xtype: "fieldset",
              title: "Informaci&oacute;n",
              items: [
                {
                  xtype: "container",
                  height: 27,
                  layout: "hbox",
                  items: [
                    {
                      xtype: "UpperCaseTextFieldReadOnly",
                      flex: 1,
                      fieldLabel: "T&iacute;tulo del evento",
                      itemId: "descriptionTittleEvent",
                      name: "descriptionTittleEvent",
                      fieldCls: "readOnlyText",
                      labelWidth: 150,
                      padding: "0 20 0 0"
                    },
                    {
                      xtype: "textfield",
                      fieldCls: "codeIncident",
                      itemId: "codeEvent",
                      name: "codeEvent",
                      width: 100
                    },
                    {
                      xtype: "button",
                      iconCls: "attach",
                      text: "Ver adjuntos",
                      width: 110,
                      handler: function() {
                        var idEvent = me.down("#idEvent").getValue();
                        var codeEvent = me.down("#codeEvent").getValue();
                        var window = Ext.create(
                          "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowEventsFileAttach",
                          {
                            idEvent: idEvent,
                            buttonsDisabled: true,
                            modal: true
                          }
                        );
                        window.title = window.title + " - " + codeEvent;
                        window.show();
                      }
                    }
                  ]
                },
                {
                  xtype: "container",
                  height: 28,
                  layout: {
                    type: "hbox",
                    align: "top"
                  },
                  items: [
                    {
                      xtype: "UpperCaseTextFieldReadOnly",
                      width: 360,
                      fieldLabel: "Monto",
                      labelWidth: 150,
                      name: "amountOrigin",
                      itemId: "amountOrigin",
                      fieldCls: "readOnlyText"
                    },
                    {
                      xtype: "UpperCaseTextFieldReadOnly",
                      width: 60,
                      padding: "0 0 0 4",
                      fieldCls: "readOnlyText",
                      name: "nameCurrency",
                      itemId: "nameCurrency"
                    }
                  ]
                },
                {
                  xtype: "displayfield",
                  anchor: "100%",
                  labelWidth: 150,
                  fieldCls: "style-for-url",
                  fieldLabel: "Organizaci&oacute;n interna",
                  itemId: "descriptionWorkArea",
                  name: "descriptionWorkArea"
                },
                {
                  xtype: "UpperCaseTextArea",
                  anchor: "100%",
                  allowBlank: false,
                  fieldLabel: "Descripci&oacute;n detallada",
                  fieldCls: "readOnlyText",
                  readOnly: true,
                  itemId: "descriptionLarge",
                  name: "descriptionLarge",
                  height: 70,
                  labelWidth: 150
                }
              ]
            },
            {
              xtype: "fieldset",
              title: "Detalle",
              items: [
                {
                  xtype: "UpperCaseTextFieldReadOnly",
                  width: 400,
                  fieldLabel: "Centro de costo",
                  itemId: "costCenter",
                  name: "costCenter",
                  fieldCls: "readOnlyText",
                  labelWidth: 150
                },
                {
                  xtype: "container",
                  height: 28,
                  layout: "hbox",
                  items: [
                    {
                      xtype: "textfield",
                      name: "idPlanAccountLoss",
                      itemId: "idPlanAccountLoss",
                      hidden: true
                    },
                    {
                      xtype: "TextFieldNumberFormatReadOnly",
                      width: 400,
                      fieldLabel: "Cuenta contable",
                      name: "codeAccountLoss",
                      itemId: "codeAccountLoss",
                      msgTarget: "side",
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      allowBlank: true,
                      labelWidth: 150
                    },
                    {
                      xtype: "button",
                      iconCls: "search",
                      handler: function() {
                        var win = Ext.create(
                          "DukeSource.view.risk.parameter.windows.ViewWindowSearchPlanAccountCountable",
                          { modal: true }
                        ).show();
                        win
                          .down("grid")
                          .on("itemdblclick", function(view, row) {
                            me.down("#codeAccountLoss").setValue(
                              row.get("codeAccount")
                            );
                            me.down("#idPlanAccountLoss").setValue(
                              row.get("idAccountPlan")
                            );
                            win.close();
                          });
                      }
                    }
                  ]
                },
                {
                  xtype: "UpperCaseTextArea",
                  anchor: "100%",
                  fieldLabel: "Glosa",
                  name: "comments",
                  itemId: "comments",
                  msgTarget: "side",
                  fieldCls: "obligatoryTextField",
                  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  allowBlank: true,
                  height: 70,
                  labelWidth: 150
                }
              ]
            }
          ]
        }
      ],
      buttons: [
        {
          text: "Guardar",
          scale: "medium",
          iconCls: "save",
          handler: function(btn) {
            var form = me.down("form").getForm();
            if (form.isValid()) {
              DukeSource.lib.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/saveAccountingEvent.htm?nameView=ViewPanelEventsRiskOperational",
                params: {
                  jsonData: Ext.JSON.encode(form.getValues())
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    DukeSource.global.DirtyView.messageNormal(response.message);
                    me.close();
                    Ext.ComponentQuery.query(
                      "ViewPanelEventsRiskOperational grid"
                    )[0]
                      .down("pagingtoolbar")
                      .doRefresh();
                  } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                  }
                },
                failure: function() {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              });
            } else {
              DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
            }
          }
        },
        {
          text: "Salir",
          scope: this,
          scale: "medium",
          handler: this.close,
          iconCls: "logout"
        }
      ],
      buttonAlign: "center"
    });
    me.callParent(arguments);
  }
});
