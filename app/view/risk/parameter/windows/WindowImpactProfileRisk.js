Ext.define('DukeSource.view.risk.parameter.windows.WindowImpactProfileRisk', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowImpactProfileRisk',
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'IMPACTO - PERFIL DE RIESGO',
    titleAlign: 'center',
    buttonAlign: 'center',
    initComponent: function () {
        var me = this;
        var rowBack = this.rowBack;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'idImpact',
                            name: 'idImpact',
                            value: rowBack.get('idImpact'),
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'state',
                            name: 'state',
                            value: 'S',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            allowBlank: false,
                            height: 50,
                            fieldCls: 'obligatoryTextField',
                            name: 'descriptionProfileRisk',
                            itemId: 'descriptionProfileRisk',
                            fieldLabel: 'DESCRIPCION<br>CUALITATIVA'
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                defaultPadding: {left: 5, right: 5, top: 5, bottom: 5}
                            },
                            items: [
                                {
                                    xtype: 'numberfield',
                                    flex: 1,
                                    name: 'minValueProfileRisk',
                                    itemId: 'minValueProfileRisk',
                                    value: 0
                                },
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    padding: '0 5 0 5',
                                    readOnly: true,
                                    fieldCls: 'readOnlyTextCenter',
                                    value: '< ' + rowBack.get('description') + '< '
                                },
                                {
                                    xtype: 'numberfield',
                                    flex: 1,
                                    name: 'maxValueProfileRisk',
                                    itemId: 'maxValueProfileRisk',
                                    value: 0
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    scale: 'medium',
                    action: 'updateImpactProfileRisk',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });
        me.callParent(arguments);
    }
});