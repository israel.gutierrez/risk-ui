Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowValuationFeaturesProcess', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowValuationFeaturesProcess',
    height: 153,
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'EVALUADOR DE CARACTERISTICA DE PROCESO',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            labelWidth: 160,
                            anchor: '100%',
                            itemId: 'text',
                            name: 'text',
                            fieldLabel: 'DESCRIPCION'
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            labelWidth: 160,
                            anchor: '100%',
                            itemId: 'valuationConcept',
                            name: 'fieldOne',
                            fieldLabel: 'CONCEPTO VALORACION'
                        },
                        {
                            xtype: 'NumberDecimalNumberObligatory',
                            labelWidth: 160,
                            anchor: '100%',
                            itemId: 'valueFeature',
                            name: 'fieldTwo',
                            fieldLabel: 'VALOR CARACTERISTICA'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    action: 'saveFeatureProcess',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });

        me.callParent(arguments);
    }

});
