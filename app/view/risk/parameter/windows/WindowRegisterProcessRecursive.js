Ext.define('DukeSource.view.risk.parameter.windows.WindowRegisterProcessRecursive', {
    extend: 'Ext.window.Window',
    requires: ['DukeSource.view.risk.util.ViewComboYesNo'],
    alias: 'widget.WindowRegisterProcessRecursive',
    height: 250,
    width: 600,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'PROCESO',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    fieldDefaults: {
                        labelCls: 'changeSizeFontToEightPt',
                        fieldCls: 'changeSizeFontToEightPt'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'parent',
                            name: 'parent',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'depth',
                            name: 'depth',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'path',
                            name: 'path',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'state',
                            name: 'state',
                            value: 'S',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'idProcessRecursive',
                            name: 'idProcessRecursive',
                            value: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            name: 'code',
                            itemId: 'code',
                            fieldLabel: 'CODIGO'
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            name: 'description',
                            itemId: 'description',
                            fieldLabel: 'PROCESO'
                        },
                        {
                            xtype: 'textarea',
                            anchor: '100%',
                            height: 80,
                            name: 'detailDescription',
                            itemId: 'detailDescription',
                            fieldLabel: 'DETALLE'
                        },
                        {
                            xtype: 'ViewComboYesNo',
                            name: 'isCritic',
                            itemId: 'isCritic',
                            msgTarget: 'side',
                            fieldCls: 'obligatoryTextField',
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            fieldLabel: 'ES CRITICO?'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    scale: 'medium',
                    action: 'saveProcessRecursive',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });
        me.callParent(arguments);
    }
});