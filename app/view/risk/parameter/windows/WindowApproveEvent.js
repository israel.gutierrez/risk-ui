Ext.define("DukeSource.view.risk.parameter.windows.WindowApproveEvent", {
  extend: "Ext.window.Window",
  alias: "widget.WindowApproveEvent",
  width: 750,
  layout: {
    type: "fit"
  },
  title: "Aprobar evento",
  titleAlign: "center",
  border: true,
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          layout: "anchor",
          border: false,
          bodyPadding: 5,
          fieldDefaults: {
            labelCls: "changeSizeFontToEightPt",
            fieldCls: "changeSizeFontToEightPt"
          },
          items: [
            {
              xtype: "textfield",
              hidden: true,
              itemId: "idEventApproved",
              name: "idEventApproved",
              value: "id"
            },
            {
              xtype: "textfield",
              hidden: true,
              itemId: "idEvent",
              name: "idEvent"
            },
            {
              xtype: "textfield",
              hidden: true,
              value: new Date(),
              itemId: "dateRegister",
              name: "dateRegister"
            },
            {
              xtype: "textfield",
              hidden: true,
              value: userName,
              itemId: "user",
              name: "user"
            },
            {
              xtype: "textfield",
              hidden: true,
              itemId: "eventState",
              name: "eventState"
            },
            {
              xtype: "textfield",
              hidden: true,
              itemId: "sequenceEventState",
              name: "sequenceEventState"
            },
            {
              xtype: "textfield",
              hidden: true,
              itemId: "order",
              name: "order"
            },
            {
              xtype: "textfield",
              hidden: true,
              itemId: "orderApproved",
              name: "orderApproved"
            },
            {
              xtype: "textfield",
              hidden: true,
              itemId: "stateApprove",
              name: "stateApprove"
            },
            {
              xtype: "textfield",
              hidden: true,
              itemId: "sequenceApprove",
              name: "sequenceApprove"
            },
            {
              xtype: "textfield",
              hidden: true,
              itemId: "descriptionMotiveRefused",
              name: "descriptionMotiveRefused"
            },
            {
              xtype: "fieldset",
              title: "Informaci&oacute;n",
              items: [
                {
                  xtype: "container",
                  height: 27,
                  layout: "hbox",
                  items: [
                    {
                      xtype: "UpperCaseTextFieldReadOnly",
                      flex: 1,
                      fieldLabel: "T&iacute;tulo del evento",
                      itemId: "descriptionTittleEvent",
                      name: "descriptionTittleEvent",
                      fieldCls: "readOnlyText",
                      labelWidth: 150,
                      padding: "0 20 0 0"
                    },
                    {
                      xtype: "textfield",
                      fieldCls: "codeIncident",
                      itemId: "codeEvent",
                      name: "codeEvent",
                      width: 100
                    },
                    {
                      xtype: "button",
                      iconCls: "attach",
                      text: "Ver adjuntos",
                      width: 110,
                      handler: function() {
                        var idEvent = me.down("#idEvent").getValue();
                        var codeEvent = me.down("#codeEvent").getValue();
                        var window = Ext.create(
                          "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowEventsFileAttach",
                          {
                            idEvent: idEvent,
                            buttonsDisabled: true,
                            modal: true
                          }
                        );
                        window.title = window.title + " - " + codeEvent;
                        window.show();
                      }
                    }
                  ]
                },
                {
                  xtype: "container",
                  height: 28,
                  layout: {
                    type: "hbox",
                    align: "top"
                  },
                  items: [
                    {
                      xtype: "UpperCaseTextFieldReadOnly",
                      width: 360,
                      fieldLabel: "Monto",
                      labelWidth: 150,
                      name: "amountOrigin",
                      itemId: "amountOrigin",
                      fieldCls: "readOnlyText"
                    },
                    {
                      xtype: "UpperCaseTextFieldReadOnly",
                      width: 60,
                      padding: "0 0 0 4",
                      fieldCls: "readOnlyText",
                      name: "nameCurrency",
                      itemId: "nameCurrency"
                    }
                  ]
                },
                {
                  xtype: "displayfield",
                  anchor: "100%",
                  labelWidth: 150,
                  fieldCls: "style-for-url",
                  fieldLabel: "Organizaci&oacute;n interna",
                  itemId: "descriptionUnity",
                  name: "descriptionUnity"
                },
                {
                  xtype: "UpperCaseTextArea",
                  anchor: "100%",
                  allowBlank: false,
                  fieldLabel: "Descripci&oacute;n detallada",
                  fieldCls: "readOnlyText",
                  readOnly: true,
                  itemId: "descriptionLarge",
                  name: "descriptionLarge",
                  height: 70,
                  labelWidth: 150
                }
              ]
            },
            {
              xtype: "fieldset",
              title: "Detalle",
              items: [
                {
                  xtype: "container",
                  height: 28,
                  layout: {
                    type: "hbox",
                    pack: "center"
                  },
                  items: [
                    {
                      xtype: "radiogroup",
                      padding: "0 0 0 40",
                      width: 342,
                      items: [
                        {
                          style: "font-weight: bold;",
                          name: "approve",
                          inputValue: "A",
                          checked: true,
                          boxLabel: "Aceptar"
                        },
                        {
                          style: "font-weight: bold;",
                          name: "approve",
                          inputValue: "R",
                          boxLabel: "Rechazar"
                        }
                      ],
                      listeners: {
                        change: function(radioGroup, newvalue) {
                          if (newvalue["approve"] === "A") {
                            me.down("#motiveReject").setVisible(false);
                          } else {
                            me.down("#motiveReject").setVisible(true);
                          }
                        }
                      }
                    }
                  ]
                },
                {
                  xtype: "combobox",
                  width: 500,
                  itemId: "motiveReject",
                  name: "motiveReject",
                  fieldLabel: "Motivo",
                  msgTarget: "side",
                  fieldCls: "obligatoryTextField",
                  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  hidden: true,
                  labelWidth: 150,
                  queryMode: "local",
                  displayField: "description",
                  valueField: "value",
                  store: {
                    fields: ["value", "description"],
                    pageSize: 9999,
                    autoLoad: true,
                    proxy: {
                      actionMethods: {
                        create: "POST",
                        read: "POST",
                        update: "POST"
                      },
                      type: "ajax",
                      url:
                        "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                      extraParams: {
                        propertyOrder: "description",
                        propertyFind: "identified",
                        valueFind: "REFUSEDEVENT"
                      },
                      reader: {
                        type: "json",
                        root: "data",
                        successProperty: "success"
                      }
                    }
                  },
                  listeners: {
                    select: function(cbo) {
                      me.down("#descriptionMotiveRefused").setValue(
                        cbo.getRawValue()
                      );
                    }
                  }
                },
                {
                  xtype: "textareafield",
                  anchor: "100%",
                  itemId: "comments",
                  name: "comments",
                  fieldLabel: "Comentario",
                  fieldCls: "obligatoryTextField",
                  msgTarget: "side",
                  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  allowBlank: false,
                  height: 50,
                  labelWidth: 150
                }
              ]
            }
          ]
        }
      ],
      buttons: [
        {
          text: "Guardar",
          scale: "medium",
          iconCls: "save",
          handler: function(btn) {
            var form = me.down("form").getForm();
            if (form.isValid()) {
              DukeSource.lib.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/saveEventApproved.htm?nameView=ViewPanelEventsRiskOperational",
                params: {
                  jsonData: Ext.JSON.encode(form.getValues())
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    DukeSource.global.DirtyView.messageNormal(response.message);
                    me.close();
                    Ext.ComponentQuery.query("ViewPanelEventsManager grid")[0]
                      .down("pagingtoolbar")
                      .doRefresh();
                  } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                  }
                },
                failure: function() {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              });
            } else {
              DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
            }
          }
        },
        {
          text: "Salir",
          scope: this,
          scale: "medium",
          handler: this.close,
          iconCls: "logout"
        }
      ],
      buttonAlign: "center"
    });
    me.callParent(arguments);
  }
});
