Ext.define("ModelUserActives", {
  extend: "Ext.data.Model",
  fields: [
    "idAccountPlan",
    "codeAccount",
    "currency",
    "description",
    "orderAccount"
  ]
});

var storeUserActives = Ext.create("Ext.data.Store", {
  model: "ModelUserActives",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.windows.ViewWindowSearchPlanAccountCountable",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowSearchPlanAccountCountable",
    layout: {
      align: "stretch",
      type: "vbox"
    },
    anchorSize: 100,
    title: "Buscar cuenta contable",
    titleAlign: "center",
    width: 500,
    height: 350,
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            height: 45,
            padding: "2 2 2 2",
            bodyPadding: 10,
            items: [
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "combobox",
                    value: 2,
                    labelWidth: 40,
                    width: 190,
                    fieldLabel: "TIPO",
                    store: [
                      [1, "Nro de Cuenta"],
                      [2, "Descripción"],
                      [3, "Cuenta de orden"]
                    ]
                  },
                  {
                    xtype: "UpperCaseTextField",
                    flex: 2,
                    listeners: {
                      specialkey: function(field, e) {
                        var property = "";
                        if (me.down("combo").getValue() === 1) {
                          property = "codeAccount";
                        } else if (me.down("combo").getValue() === 2) {
                          property = "description";
                        } else {
                          property = "orderAccount";
                        }
                        if (e.getKey() === e.ENTER) {
                          var grid = me.down("grid");
                          var toolbar = grid.down("pagingtoolbar");
                          DukeSource.global.DirtyView.searchPaginationGridToEnter(
                            field,
                            grid,
                            toolbar,
                            "http://localhost:9000/giro/findAccountPlan.htm",
                            property,
                            "description"
                          );
                        }
                      }
                    }
                  }
                ]
              }
            ]
          },
          {
            xtype: "container",
            flex: 3,
            padding: "2 2 2 2",
            layout: {
              align: "stretch",
              type: "hbox"
            },
            items: [
              {
                xtype: "gridpanel",
                padding: "0 1 0 0",
                store: storeUserActives,
                flex: 1,
                titleAlign: "center",
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    dataIndex: "codeAccount",
                    width: 120,
                    text: "Cuenta"
                  },
                  {
                    dataIndex: "orderAccount",
                    width: 120,
                    text: "Cuenta de orden"
                  },
                  {
                    dataIndex: "description",
                    flex: 1,
                    text: "Descripción"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: storeUserActives,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  render: function() {
                    var me = this;
                    DukeSource.global.DirtyView.searchPaginationGridNormal(
                      "",
                      me,
                      me.down("pagingtoolbar"),
                      "http://localhost:9000/giro/findAccountPlan.htm",
                      "description",
                      "description"
                    );
                  }
                }
              }
            ]
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
