Ext.define("DukeSource.view.risk.parameter.windows.ViewWindowCatalogRisk", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowCatalogRisk",
  width: 479,
  border: false,
  layout: {
    type: "fit"
  },
  title: "Catálogo de Riesgo",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          title: "",
          items: [
            {
              xtype: "UpperCaseTextFieldReadOnly",
              anchor: "60%",
              name: "idRisk",
              value: "id",
              fieldLabel: "ID",
              readOnly: true,
              hidden: true
            },
            {
              xtype: "UpperCaseTextFieldReadOnly",
              anchor: "100%",
              name: "codeRisk",
              maxLength: 20,
              fieldLabel: "CODIGO RIESGO"
            },
            {
              xtype: "UpperCaseTextArea",
              anchor: "100%",
              allowBlank: false,
              itemId: "description",
              name: "description",
              fieldLabel: "DESCRIPCION"
            }
          ]
        }
      ],
      buttonAlign: "center",
      buttons: [
        {
          text: "GUARDAR",
          iconCls: "save",
          scale: "medium",
          handler: function() {
            if (
              me
                .down("form")
                .getForm()
                .isValid()
            ) {
              Ext.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/saveCatalogRisk.htm?nameView=ViewPanelRegisterCatalogRisk",
                params: {
                  jsonData: Ext.JSON.encode(me.down("form").getValues())
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    me.close();
                    DukeSource.global.DirtyView.messageNormal(response.message);
                    if (
                      Ext.ComponentQuery.query("ViewWindowSearchRisk")[0] ===
                      undefined
                    ) {
                      Ext.ComponentQuery.query(
                        "ViewPanelRegisterCatalogRisk grid"
                      )[0]
                        .getStore()
                        .load();
                    } else {
                      Ext.ComponentQuery.query("ViewWindowSearchRisk grid")[0]
                        .getStore()
                        .load();
                    }
                  } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                  }
                },
                failure: function() {}
              });
            } else {
              DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
            }
          }
        },
        {
          text: "SALIR",
          scope: this,
          scale: "medium",
          handler: this.close,
          iconCls: "logout"
        }
      ]
    });

    me.callParent(arguments);
  }
});
