Ext.define('DukeSource.view.risk.parameter.windows.WindowRegisterFactorRisk', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowRegisterFactorRisk',
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'FACTOR-CAUSA',
    titleAlign: 'center',
    buttonAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'depth',
                            name: 'depth',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'path',
                            name: 'path',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'parent',
                            name: 'parent',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'state',
                            name: 'state',
                            value: 'S',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'idFactorRisk',
                            name: 'idFactorRisk',
                            value: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            name: 'description',
                            itemId: 'description',
                            fieldLabel: 'FACTOR ROP'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    scale: 'medium',
                    action: 'saveFactorRisk',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });
        me.callParent(arguments);
    }
});