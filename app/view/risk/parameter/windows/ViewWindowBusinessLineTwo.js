Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowBusinessLineTwo', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowBusinessLineTwo',
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'LINEA DE NEGOCIO N2',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            itemId: 'text',
                            name: 'text',
                            fieldLabel: 'DESCRIPCION'
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'codeEntitySupervisor',
                            name: 'codeEntitySupervisor',
                            fieldLabel: 'CODIGO',
                            hidden: hidden('VWBLTCodeEntitySupervisor')
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    scale: 'medium',
                    action: 'saveBusinessLine',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});
