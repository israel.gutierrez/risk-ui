Ext.define('DukeSource.view.risk.parameter.windows.WindowRegisterResourceContinuity', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowRegisterResourceContinuity',
    height: 280,
    width: 600,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'RECURSOS DE CONTINUIDAD',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            hidden: true,
                            value: 'id',
                            name: 'id'
                        },
                        {
                            xtype: 'UpperCaseTextField',
                            anchor: '100%',
                            name: 'name',
                            itemId: 'name',
                            allowBlank: false,
                            fieldLabel: 'NOMBRE',
                            labelWidth: 120
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            name: 'description',
                            itemId: 'description',
                            allowBlank: false,
                            fieldLabel: 'DESCRIPCION',
                            labelWidth: 120
                        },
                        {
                            xtype: 'ViewComboWorkArea',
                            anchor: '100%',
                            name: 'idWorkArea',
                            itemId: 'idWorkArea',
                            fieldCls: 'obligatoryTextField',
                            fieldLabel: 'AREA RESPONSABLE',
                            allowBlank: false,
                            queryMode: 'remote',
                            labelWidth: 120
                        },
                        {
                            xtype: 'ViewComboJobPlace',
                            anchor: '100%',
                            fieldCls: 'obligatoryTextField',
                            name: 'idJobPlace',
                            itemId: 'idJobPlace',
                            allowBlank: false,
                            fieldLabel: 'RESPONSABLE DE CUIDADO',
                            labelWidth: 120
                        },
                        {
                            xtype: 'container',
                            height: 26,
                            layout: 'hbox',
                            items: [
                                {
                                    xtype: 'ComboTypeResource',
                                    flex: 1,
                                    fieldCls: 'obligatoryTextField',
                                    allowBlank: false,
                                    labelWidth: 120,
                                    name: 'idTypeResource',
                                    fieldLabel: 'TIPO RECURSO'
                                },
                                {
                                    xtype: 'ViewComboAvailability',
                                    flex: 1,
                                    padding: '0 0 0 5',
                                    fieldCls: 'obligatoryTextField',
                                    allowBlank: false,
                                    name: 'idAvailability',
                                    fieldLabel: 'DISPONIBILIDAD'
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    action: 'saveResourceContinuity',
                    scale: 'medium',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});
