Ext.define("ModelVersionRiskPending", {
  extend: "Ext.data.Model",
  fields: ["process", "idRisk", "codeRisk", "analyst", "versionCorrelative"]
});

var storeVersionRiskPending = Ext.create("Ext.data.Store", {
  model: "ModelVersionRiskPending",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/checkRiskEvaluationPending.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
storeVersionRiskPending.load();
Ext.define(
  "DukeSource.view.risk.parameter.windows.ViewWindowRiskEvaluationPending",
  {
    extend: "Ext.window.Window",
    alias: "widget.viewWindowRiskEvaluationPending",
    height: 400,
    width: 580,
    border: false,
    layout: {
      type: "fit"
    },
    title: "RIESGOS NO CULMINADOS DE EVALUAR",
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            store: storeVersionRiskPending,
            flex: 1,
            itemId: "gridEvalPending",
            titleAlign: "center",
            columns: [
              {
                xtype: "rownumberer",
                width: 50,
                sortable: false
              },
              {
                dataIndex: "process",
                width: 180,
                text: "PROCESO",
                align: "left"
              },
              {
                dataIndex: "idRisk",
                width: 60,
                text: "ID RIESGO",
                align: "center"
              },
              {
                dataIndex: "codeRisk",
                width: 80,
                text: "CODIGO",
                align: "left"
              },
              {
                dataIndex: "versionCorrelative",
                width: 60,
                text: "VERSION",
                align: "center"
              },
              {
                dataIndex: "analyst",
                width: 120,
                text: "ANALISTA",
                align: "center"
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: storeVersionRiskPending,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            },
            listeners: {
              render: function(grid) {
                grid.down("pagingtoolbar").moveFirst();
              }
            }
          }
        ],
        buttons: [
          {
            text: "ACEPTAR",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
