Ext.define("DukeSource.view.risk.parameter.windows.ViewWindowProcess", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowProcess",
  width: 650,
  border: false,
  layout: {
    type: "fit"
  },
  title: "Proceso",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          items: [
            {
              xtype: "textfield",
              itemId: "idProcessType",
              name: "idProcessType",
              hidden: true
            },
            {
              xtype: "textfield",
              itemId: "idProcess",
              name: "idProcess",
              value: "id",
              hidden: true
            },
            {
              xtype: "textfield",
              itemId: "path",
              name: "path",
              hidden: true
            },
            {
              xtype: "container",
              layout: {
                type: "hbox"
              },
              items: [
                {
                  xtype: "textfield",
                  flex: 4,
                  itemId: "description",
                  labelAlign: "top",
                  name: "description",
                  fieldCls: "obligatoryTextField",
                  enforceMaxLength: true,
                  maxLength: 600,
                  fieldLabel: "Nombre",
                  listeners: {
                    afterrender: function(field) {
                      field.focus(false, 200);
                    }
                  }
                },
                {
                  xtype: "textfield",
                  itemId: "alias",
                  name: "alias",
                  allowBlank: false,
                  padding: "0 0 0 5",
                  flex: 1,
                  fieldCls: "obligatoryTextField",
                  labelAlign: "top",
                  maxLength: 100,
                  fieldLabel: "Alias"
                },
                {
                  xtype: "UpperCaseTextField",
                  padding: "0 0 0 5",
                  itemId: "abbreviation",
                  name: "abbreviation",
                  enforceMaxLength: true,
                  flex: 1,
                  maxLength: 30,
                  labelAlign: "top",
                  fieldLabel: "Código"
                }
              ]
            },
            {
              xtype: "combobox",
              padding: "5 0 0 0",
              labelAlign: "top",
              fieldLabel: getName("VWPTransactional"),
              hidden: hidden("VWPTransactional"),
              editable: false,
              enforceMaxLength: true,
              name: "transactional",
              itemId: "transactional",
              queryMode: "local",
              displayField: "description",
              valueField: "value",
              store: {
                fields: ["value", "description"],
                sorters: [
                  {
                    property: "description",
                    direction: "ASC"
                  }
                ],
                pageSize: 9999,
                autoLoad: true,
                proxy: {
                  actionMethods: {
                    create: "POST",
                    read: "POST",
                    update: "POST"
                  },
                  type: "ajax",
                  url:
                    "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                  extraParams: {
                    propertyOrder: "description",
                    propertyFind: "identified",
                    valueFind: "BUSINESS_LINE_ONE"
                  },
                  reader: {
                    type: "json",
                    root: "data",
                    successProperty: "success"
                  }
                }
              }
            },
            {
              xtype: "container",
              layout: {
                type: "hbox"
              },
              items: [
                {
                  xtype: "UpperCaseTextArea",
                  flex: 4,
                  labelAlign: "top",
                  itemId: "descriptionLong",
                  name: "descriptionLong",
                  enforceMaxLength: true,
                  maxLength: 2000,
                  fieldLabel: "Descripci&oacute;n"
                }
              ]
            },
            {
              xtype: "container",
              padding: "5 0 0 0",
              layout: {
                type: "hbox"
              },
              items: [
                {
                  xtype: "textarea",
                  itemId: "input",
                  enforceMaxLength: true,
                  flex: 2,
                  labelAlign: "top",
                  name: "input",
                  maxLength: 2000,
                  fieldLabel: "Entrada(input)"
                },
                {
                  xtype: "textarea",
                  itemId: "output",
                  padding: "0 0 0 5",
                  flex: 2,
                  labelAlign: "top",
                  name: "output",
                  maxLength: 2000,
                  fieldLabel: "Salida(output)"
                },
                {
                  xtype: "textarea",
                  itemId: "generateProduct",
                  name: "generateProduct",
                  padding: "0 0 0 5",
                  flex: 2,
                  labelAlign: "top",
                  maxLength: 2000,
                  fieldLabel: "Genera producto o servicio"
                }
              ]
            }
          ]
        }
      ],
      buttons: [
        {
          text: "Guardar",
          scale: "medium",
          action: "saveProcess",
          iconCls: "save"
        },
        {
          text: "Salir",
          scale: "medium",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});
