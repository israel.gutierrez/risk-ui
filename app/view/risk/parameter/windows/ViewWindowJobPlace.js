Ext.define("DukeSource.view.risk.parameter.windows.ViewWindowJobPlace", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowJobPlace",
  width: 479,
  border: false,
  layout: {
    type: "fit"
  },
  title: "Cargo laboral",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          items: [
            {
              xtype: "textfield",
              itemId: "idJobPlace",
              name: "idJobPlace",
              value: "id",
              hidden: true
            },
            {
              xtype: "textfield",
              itemId: "idHierarchyJobPlace",
              name: "idHierarchyJobPlace",
              hidden: true
            },
            {
              xtype: "textfield",
              itemId: "codeMigration",
              name: "codeMigration",
              hidden: true
            },
            {
              xtype: "combobox",
              fieldLabel: "&Aacute;rea",
              plugins: ["ComboSelectCount"],
              name: "workArea",
              itemId: "workArea",
              editable: false,
              readOnly: true,
              forceSelection: true,
              displayField: "description",
              valueField: "idWorkArea",
              store: {
                fields: ["idWorkArea", "description"],
                pageSize: 999,
                proxy: {
                  actionMethods: {
                    create: "POST",
                    read: "POST",
                    update: "POST"
                  },
                  type: "ajax",
                  url:
                    "http://localhost:9000/giro/showListWorkAreaActivesComboBox.htm",
                  extraParams: {
                    propertyOrder: "description"
                  },
                  reader: {
                    type: "json",
                    root: "data",
                    successProperty: "success"
                  }
                }
              },
              listeners: {
                select: function(cbo) {
                  me.down("#descriptionParent").setValue(cbo.getRawValue());
                  me.down("#parent").setValue(cbo.getValue());
                }
              }
            },
            {
              xtype: "combobox",
              fieldLabel: "Cargo superior",
              plugins: ["ComboSelectCount"],
              name: "parent",
              itemId: "parent",
              editable: false,
              readOnly: true,
              forceSelection: true,
              displayField: "description",
              valueField: "idJobPlace",
              store: {
                fields: ["idJobPlace", "description"],
                pageSize: 999,
                proxy: {
                  actionMethods: {
                    create: "POST",
                    read: "POST",
                    update: "POST"
                  },
                  type: "ajax",
                  url:
                    "http://localhost:9000/giro/showAllJobPlaceActivesComboBox.htm",
                  extraParams: {
                    propertyOrder: "1"
                  },
                  reader: {
                    type: "json",
                    root: "data",
                    successProperty: "success"
                  }
                }
              }
            },
            {
              xtype: "UpperCaseTextFieldObligatory",
              anchor: "100%",
              itemId: "description",
              name: "description",
              fieldLabel: "Cargo"
            },
            {
              xtype: "textarea",
              height: 50,
              anchor: "100%",
              itemId: "detailDescription",
              name: "detailDescription",
              fieldLabel: "Detalle"
            },
            {
              xtype: "combobox",
              fieldLabel: "Tipo",
              name: "category",
              itemId: "category",
              editable: false,
              forceSelection: true,
              displayField: "description",
              valueField: "value",
              store: {
                fields: ["value", "description"],
                pageSize: 9999,
                autoLoad: true,
                proxy: {
                  actionMethods: {
                    create: "POST",
                    read: "POST",
                    update: "POST"
                  },
                  type: "ajax",
                  url:
                    "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                  extraParams: {
                    propertyOrder: "description",
                    propertyFind: "identified",
                    valueFind: "CONFIG_TYPE_JOB_PLACE"
                  },
                  reader: {
                    type: "json",
                    root: "data",
                    successProperty: "success"
                  }
                },
                sorters: [
                  {
                    property: "value",
                    direction: "ASC"
                  }
                ]
              }
            }
          ]
        }
      ],
      buttons: [
        {
          text: "Guardar",
          scale: "medium",
          action: "saveJobPlace",
          iconCls: "save"
        },
        {
          text: "Salir",
          scale: "medium",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ]
    });

    me.callParent(arguments);
  }
});
