Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowControlType', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowControlType',
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'TIPO DE CONTROL',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '90%',
                            itemId: 'text',
                            name: 'text',
                            fieldLabel: 'Descripión'
                        },
                        {
                            xtype: 'numberfield',
                            allowBlank: false,
                            fieldCls: 'obligatoryTextField',
                            itemId: 'weightedControl',
                            name: 'fieldOne',
                            fieldLabel: 'Ponderación'
                        }

                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    scale: 'medium',
                    action: 'saveControlType',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});
