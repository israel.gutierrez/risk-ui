Ext.define(
  "DukeSource.view.risk.parameter.windows.ViewWindowAttachReportToSubprocess",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowAttachReportToSubprocess",
    height: 100,
    width: 479,
    border: false,
    layout: {
      type: "fit"
    },
    title: "ADJUNTAR ARCHIVO",
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            items: [
              {
                xtype: "textfield",
                itemId: "id",
                value: "id",
                name: "id",
                hidden: true
              },
              {
                xtype: "filefield",
                anchor: "100%",
                allowBlank: false,
                msgTarget: "side",
                fieldCls: "obligatoryTextField",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                itemId: "text",
                name: "text",
                fieldLabel: "DESCRIPCION"
              }
            ]
          }
        ],
        buttons: [
          {
            text: "GUARDAR",
            iconCls: "save",
            handler: function() {
              var form = me.down("form");
              if (form.getForm().isValid()) {
                form.getForm().submit({
                  url:
                    "http://localhost:9000/giro/saveFileAttachmentsSubProcess.htm?nameView=ViewPanelRegisterProcessType",
                  method: "POST",
                  success: function(form, action) {
                    var valor = Ext.decode(action.response.responseText);
                    if (valor.success) {
                      DukeSource.global.DirtyView.messageAlert(
                        DukeSource.global.GiroMessages.TITLE_MESSAGE,
                        valor.mensaje,
                        Ext.Msg.INFO
                      );
                      me.close();
                    } else {
                      DukeSource.global.DirtyView.messageAlert(
                        DukeSource.global.GiroMessages.TITLE_ERROR,
                        valor.mensaje,
                        Ext.Msg.ERROR
                      );
                    }
                  },
                  failure: function(form, action) {
                    var valor = Ext.decode(action.response.responseText);
                    if (!valor.success) {
                      DukeSource.global.DirtyView.messageAlert(
                        DukeSource.global.GiroMessages.TITLE_ERROR,
                        valor.mensaje,
                        Ext.Msg.ERROR
                      );
                    }
                  }
                });
              } else {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_WARNING,
                  DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
                  Ext.Msg.ERROR
                );
              }
            }
          },
          {
            text: "SALIR",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
