Ext.define('DukeSource.view.risk.parameter.windows.WindowRegisterPaf', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowRegisterPaf',
    width: 600,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'PAF',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    fieldDefaults: {
                        labelCls: 'changeSizeFontToEightPt',
                        fieldCls: 'changeSizeFontToEightPt'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'id',
                            name: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'parent',
                            name: 'parent',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'depth',
                            name: 'depth',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'path',
                            name: 'path',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'state',
                            name: 'state',
                            value: 'S',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'idPaf',
                            name: 'idPaf',
                            value: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'codeIntern',
                            itemId: 'codeIntern',
                            fieldLabel: 'Código'
                        },
                        {
                            xtype: 'textarea',
                            anchor: '100%',
                            height: 70,
                            name: 'description',
                            itemId: 'description',
                            fieldLabel: 'PAF'
                        },
                        {
                            xtype: 'textfield',
                            name: 'codePlace',
                            itemId: 'codePlace',
                            fieldLabel: 'Código del lugar'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    scale: 'medium',
                    action: 'savePaf',
                    iconCls: 'save'
                },
                {
                    text: 'Salir',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'Log-Out-icon'
                }
            ],
            buttonAlign: 'center'
        });
        me.callParent(arguments);
    }
});