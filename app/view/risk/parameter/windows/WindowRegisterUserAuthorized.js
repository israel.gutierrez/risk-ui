Ext.define(
  "DukeSource.view.risk.parameter.windows.WindowRegisterUserAuthorized",
  {
    extend: "Ext.window.Window",
    alias: "widget.WindowRegisterUserAuthorized",
    width: 479,
    border: false,
    layout: {
      type: "fit"
    },
    title: "Registro de aprobadores",
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      var categorySearch;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            fieldDefaults: {
              labelCls: "changeSizeFontToEightPt",
              fieldCls: "changeSizeFontToEightPt"
            },
            items: [
              {
                xtype: "combobox",
                fieldLabel: "Estado",
                anchor: "100%",
                editable: false,
                name: "eventState",
                itemId: "eventState",
                fieldCls: "obligatoryTextField",
                displayField: "abbreviation",
                valueField: "id",
                store: {
                  autoLoad: true,
                  fields: ["id", "abbreviation", "sequence"],
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url: "http://localhost:9000/giro/findStateIncident.htm",
                    extraParams: {
                      propertyFind: "si.typeIncident",
                      valueFind: DukeSource.global.GiroConstants.EVENT,
                      propertyOrder: "si.sequence"
                    },
                    reader: {
                      type: "json",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                },
                listeners: {
                  select: function(cbo) {
                    var record = cbo.findRecord(
                      cbo.valueField || cbo.displayField,
                      cbo.getValue()
                    );
                    var index = cbo.store.indexOf(record);
                    me.down("#sequence").setValue(
                      cbo.store.data.items[index].data.sequence
                    );
                    if (cbo.store.data.items[index].data.sequence === 1) {
                      me.down("#allUsers").setVisible(true);
                    } else {
                      me.down("#allUsers").setVisible(false);
                      me.down("#fullName").setValue("");
                    }
                    if (cbo.store.data.items[index].data.sequence === 2) {
                      categorySearch = DukeSource.global.GiroConstants.ANALYST;
                    } else {
                      categorySearch = DukeSource.global.GiroConstants.GESTOR;
                    }

                    if (cbo.store.data.items[index].data.sequence === 3) {
                      me.down("#descMaxAmount").setVisible(true);
                      me.down("#descMinAmount").setVisible(true);

                      me.down("#maxAmount").setVisible(true);
                      me.down("#minAmount").setVisible(true);
                      me.down("#maxAmount").allowBlank = false;
                      me.down("#minAmount").allowBlank = false;
                    } else {
                      me.down("#descMaxAmount").setVisible(false);
                      me.down("#descMinAmount").setVisible(false);
                      me.down("#maxAmount").setVisible(false);
                      me.down("#minAmount").setVisible(false);
                      me.down("#maxAmount").allowBlank = true;
                      me.down("#minAmount").allowBlank = true;
                    }
                  }
                }
              },
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "textfield",
                    flex: 1,
                    allowBlank: false,
                    readOnly: true,
                    fieldCls: "readOnlyText",
                    fieldLabel: "Usuario",
                    name: "fullName",
                    itemId: "fullName"
                  },
                  {
                    xtype: "button",
                    width: 26,
                    iconCls: "search",
                    itemId: "searchAnalystPrevious",
                    handler: function(button) {
                      var panel = button.up("window");
                      var windows = Ext.create(
                        "DukeSource.view.risk.util.search.SearchUser",
                        {
                          modal: true,
                          category:
                            categorySearch === undefined
                              ? "GESTOR"
                              : categorySearch
                        }
                      ).show();
                      var grid = windows.down("grid");
                      grid.store.getProxy().extraParams = {
                        fields: "wa.id, usu.category",
                        values:
                          me.winParent.node.raw.idWorkArea +
                          "," +
                          windows.category,
                        types: "Integer" + "," + "String",
                        operators: "equal" + "," + "equal",
                        searchIn: "User"
                      };
                      grid.store.getProxy().url =
                        "http://localhost:9000/giro/advancedSearchUser.htm";
                      grid.down("pagingtoolbar").moveFirst();

                      windows
                        .down("grid")
                        .on("itemdblclick", function(view, record) {
                          panel.down("#user").setValue(record.get("userName"));
                          panel
                            .down("#fullName")
                            .setValue(record.get("fullName"));
                          windows.close();
                        });
                    }
                  },
                  {
                    xtype: "checkboxfield",
                    itemId: "allUsers",
                    fieldLabel: "TODOS",
                    labelWidth: 60,
                    checked: false,
                    hidden: true,
                    inputValue: "T",
                    uncheckedValue: "N",
                    listeners: {
                      change: function(chk) {
                        if (chk.getValue()) {
                          me.down("#searchAnalystPrevious").setDisabled(true);
                          me.down("#fullName").setValue("TODOS");
                        } else {
                          me.down("#searchAnalystPrevious").setDisabled(false);
                          me.down("#fullName").setValue("");
                        }
                      }
                    }
                  }
                ]
              },
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "textfield",
                    flex: 1.2,
                    itemId: "descMinAmount",
                    readOnly: true,
                    fieldCls: "readOnlyText",
                    fieldLabel: "Monto m&iacute;nimo",
                    value: currency + " (" + symbol + ")"
                  },
                  {
                    xtype: "NumberDecimalNumberObligatory",
                    flex: 1,
                    padding: "0 5 0 5",
                    value: 0,
                    name: "minAmount",
                    itemId: "minAmount",
                    maxLength: 16,
                    allowBlank: false,
                    fieldCls: "numberPositive"
                  }
                ]
              },
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "textfield",
                    flex: 1.2,
                    readOnly: true,
                    itemId: "descMaxAmount",
                    fieldCls: "readOnlyText",
                    fieldLabel: "Monto m&aacute;ximo",
                    value: currency + " (" + symbol + ")"
                  },
                  {
                    xtype: "NumberDecimalNumberObligatory",
                    flex: 1,
                    padding: "0 5 0 5",
                    name: "maxAmount",
                    value: 0,
                    itemId: "maxAmount",
                    maxLength: 18,
                    allowBlank: false,
                    fieldCls: "numberPositive"
                  }
                ]
              },
              {
                xtype: "textfield",
                name: "id",
                itemId: "id",
                value: "id",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "user",
                itemId: "user",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "workArea",
                itemId: "workArea",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "sequence",
                itemId: "sequence",
                hidden: true
              },
              {
                xtype: "textfield",
                name: "type",
                itemId: "type",
                value: "U",
                hidden: true
              }
            ]
          }
        ],
        buttons: [
          {
            text: "Guardar",
            action: "saveUserAuthorized",
            scale: "medium",
            iconCls: "save"
          },
          {
            text: "Salir",
            scale: "medium",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);
