Ext.define('DukeSource.view.risk.parameter.windows.ViewWindowSubProcess', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowSubProcess',
    width: 580,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'Subproceso',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'idProcessType',
                            name: 'idProcessType',
                            hidden: true
                        }, {
                            xtype: 'textfield',
                            itemId: 'idProcess',
                            name: 'idProcess',
                            hidden: true
                        }, {
                            xtype: 'textfield',
                            itemId: 'idSubProcess',
                            name: 'idSubProcess',
                            value: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'path',
                            name: 'path',
                            hidden: true
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'UpperCaseTextFieldObligatory',
                                    anchor: '100%',
                                    itemId: 'description',
                                    name: 'description',
                                    enforceMaxLength: true,
                                    maxLength: 600,
                                    flex: 4,
                                    labelAlign: 'top',
                                    fieldCls: "obligatoryTextField",
                                    fieldLabel: 'Nombre',
                                    listeners: {
                                        afterrender: function (field) {
                                            field.focus(false, 200);
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    itemId: 'alias',
                                    name: 'alias',
                                    allowBlank: false,
                                    padding: '0 0 0 5',
                                    flex: 1,
                                    fieldCls: 'obligatoryTextField',
                                    labelAlign: 'top',
                                    maxLength: 100,
                                    fieldLabel: 'Alias'
                                },
                                {
                                    xtype: 'UpperCaseTextField',
                                    padding: '0 0 0 5',
                                    flex: 1,
                                    itemId: 'abbreviation',
                                    name: 'abbreviation',
                                    enforceMaxLength: true,
                                    maxLength: 100,
                                    labelAlign: 'top',
                                    fieldLabel: 'Código'
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    scale: 'medium',
                    action: 'saveProcess',
                    iconCls: 'save'
                },
                {
                    text: 'Salir',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});