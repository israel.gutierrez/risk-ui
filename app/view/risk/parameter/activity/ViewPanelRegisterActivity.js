Ext.define('DukeSource.view.risk.parameter.activity.ViewPanelRegisterActivity', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterActivity',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newActivity'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteActivity'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchActivity',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'activityAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterActivity', padding: '2 2 2 2'}];
        this.callParent();
    }
});
