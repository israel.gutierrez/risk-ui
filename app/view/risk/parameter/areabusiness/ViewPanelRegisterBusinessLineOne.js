Ext.define('DukeSource.view.risk.parameter.areabusiness.ViewPanelRegisterBusinessLineOne', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterBusinessLineOne',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newBusinessLineOne'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteBusinessLineOne'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchBusinessLineOne',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'businessLineOneAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterBusinessLineOne', padding: '2 2 2 2'}];
        this.callParent();
    }
});
