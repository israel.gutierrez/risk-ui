Ext.define(
  "DukeSource.view.risk.parameter.areabusiness.ViewPanelRegisterWorkArea",
  {
    extend: "Ext.panel.Panel",
    requires: [
      "DukeSource.view.risk.parameter.grids.ViewGridPanelRegisterJobPlace"
    ],
    alias: "widget.ViewPanelRegisterWorkArea",
    border: false,
    layout: {
      type: "hbox",
      align: "stretch"
    },
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "panel",
            flex: 1,
            border: false,
            style: {
              borderRight: "1px solid #99bce8"
            },
            layout: "fit",
            items: [
              {
                xtype: "ViewTreeGridPanelRegisterWorkArea",
                border: false,
                bbar: [
                  {
                    xtype: "button",
                    text: "Actualizar",
                    iconCls: "operations",
                    handler: function() {
                      var tree = me.down("treepanel");
                      var node = tree.getSelectionModel().getSelection()[0];
                      tree.store.load({
                        node: node
                      });
                    }
                  }
                ],
                listeners: {
                  itemclick: function(view, record, item, index, e) {
                    var treeWorkArea = me.down("#jobPlaceView");

                    treeWorkArea.store.getProxy().url =
                      "http://localhost:9000/giro/showJobPlaceByHierarchyByWorkArea.htm";
                    treeWorkArea.store.getProxy().extraParams = {
                      idWorkArea: record.get("idWorkArea"),
                      depth: 0,
                      start: "0",
                      limit: "9999",
                      node: ""
                    };
                    treeWorkArea.store.load();
                    treeWorkArea.getSelectionModel().deselectAll();
                    me.down("#searchOption").focus(false, 100);
                  },
                  beforerender: function(view) {
                    view.headerCt.border = 1;
                  },
                  select: function(a, record) {
                    var view = me.down("ViewTreeGridPanelRegisterWorkArea");
                    var viewJob = me.down("ViewGridPanelRegisterJobPlace");
                    if (record.data["depth"] === 1) {
                      view.down("#modifyTreeWorkArea").setDisabled(true);
                      view.down("#deleteTreeWorkArea").setDisabled(true);
                      view.down("#authorizedWorkArea").setDisabled(true);
                    } else {
                      view.down("#modifyTreeWorkArea").setDisabled(false);
                      view.down("#deleteTreeWorkArea").setDisabled(false);
                      view.down("#authorizedWorkArea").setDisabled(false);
                    }

                    viewJob.down("#newTreeJobPlace").setDisabled(true);
                    viewJob.down("#modifyTreeJobPlace").setDisabled(true);
                    viewJob.down("#deleteTreeJobPlace").setDisabled(true);
                  }
                }
              }
            ]
          },
          {
            xtype: "panel",
            flex: 1.3,
            margin: "0 0 0 2",
            border: false,
            style: {
              borderLeft: "1px solid #99bce8"
            },
            layout: "fit",
            items: [
              {
                xtype: "ViewGridPanelRegisterJobPlace",
                itemId: "jobPlaceView",
                border: false,
                listeners: {
                  select: function(a, record) {
                    var view = me.down("ViewGridPanelRegisterJobPlace");
                    if (record.data["depth"] === 1) {
                      view.down("#newTreeJobPlace").setDisabled(false);
                      view.down("#modifyTreeJobPlace").setDisabled(true);
                      view.down("#deleteTreeJobPlace").setDisabled(true);
                    } else {
                      view.down("#newTreeJobPlace").setDisabled(false);
                      view.down("#modifyTreeJobPlace").setDisabled(false);
                      view.down("#deleteTreeJobPlace").setDisabled(false);
                    }
                  }
                }
              }
            ]
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
