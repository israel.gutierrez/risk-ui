Ext.define('DukeSource.view.risk.parameter.areabusiness.AddMenuWorkArea', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.AddMenuWorkArea',
    width: 120,
    initComponent: function () {
        var me = this;
        var record = this.record;
        var view = this.view;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'menuitem',
                    text: 'Asignar Cargo',
                    record: record,
                    view: view,
                    iconCls: 'add'
                },
                {
                    xtype: 'menuitem',
                    text: 'Agregar Cargo',
                    iconCls: 'add'
                },
                {
                    xtype: 'menuitem',
                    text: 'Editar',
                    iconCls: 'modify'
                },
                {
                    xtype: 'menuitem',
                    text: 'Eliminar',
                    iconCls: 'delete'
                }
            ]
        });

        me.callParent(arguments);
    }
});