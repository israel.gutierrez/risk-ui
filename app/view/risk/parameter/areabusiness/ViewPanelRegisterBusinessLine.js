Ext.define('DukeSource.view.risk.parameter.areabusiness.ViewPanelRegisterBusinessLine', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterBusinessLine',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newRegisterBusinessLine'
        }, '-'
//        ,
//        {
//            xtype: 'UpperCaseTextField',
//            action:'searchRegisterBusinessLine',
//            fieldLabel:'BUSCAR',
//            labelWidth:60,
//            width:300
//        },'-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'registerEventBusinessLine',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewTreeGridPanelRegisterBusinessLine', padding: '2 2 2 2'}];
        this.callParent();
    }
});
