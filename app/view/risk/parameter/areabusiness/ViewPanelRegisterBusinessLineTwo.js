Ext.define('DukeSource.view.risk.parameter.areabusiness.ViewPanelRegisterBusinessLineTwo', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterBusinessLineTwo',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newBusinessLineTwo'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteBusinessLineTwo'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchBusinessLineTwo',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'businessLineTwoAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterBusinessLineTwo', padding: '2 2 2 2'}];
        this.callParent();
    }
});
