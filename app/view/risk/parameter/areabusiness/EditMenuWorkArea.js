Ext.define('DukeSource.view.risk.parameter.areabusiness.EditMenuWorkArea', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.EditMenuWorkArea',
    width: 120,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'menuitem',
                    text: 'Agregar Puesto',
                    iconCls: 'add'
                }, {
                    xtype: 'menuitem',
                    text: 'Editar',
                    iconCls: 'modify'
                },
                {
                    xtype: 'menuitem',
                    text: 'Eliminar',
                    iconCls: 'delete'
                }
            ]
        });

        me.callParent(arguments);
    }
});
