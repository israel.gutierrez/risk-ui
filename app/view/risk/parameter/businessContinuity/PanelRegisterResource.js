Ext.define('DukeSource.view.risk.parameter.businessContinuity.PanelRegisterResource', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.PanelRegisterResource',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newResourceContinuity'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteResourceContinuity'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchResourceContinuity',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'resourceContinuityAudit',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {
                xtype: 'GridPanelRegisterResource',
                padding: '2 2 2 2'
            }
        ];
        this.callParent();
    }
});
