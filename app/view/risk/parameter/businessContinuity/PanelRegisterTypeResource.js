Ext.define('DukeSource.view.risk.parameter.businessContinuity.PanelRegisterTypeResource', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.PanelRegisterTypeResource',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newTypeResource'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteTypeResource'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchTypeResource',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'typeResourceAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {
                xtype: 'GridPanelRegisterTypeResource',
                padding: '2 2 2 2'
            }
        ];
        this.callParent();
    }
});
