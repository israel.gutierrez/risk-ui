Ext.define("ResourceContinuity", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "abbreviation",
    "idAvailability",
    "idWorkArea",
    "idTypeResource",
    "idJobPlace",
    "availability",
    "workArea",
    "typeResource",
    "jobPlace",
    "name",
    "description",
    "locationStore",
    "state"
  ]
});

var resourceContinuity = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ResourceContinuity",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.businessContinuity.grid.GridPanelRegisterResource",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.GridPanelRegisterResource",
    store: resourceContinuity,
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: resourceContinuity,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/showListResourceActives.htm",
          "r.description",
          "r.description"
        );
      }
    },
    initComponent: function() {
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        { header: "CODIGO", dataIndex: "id", width: 80 },
        {
          header: "DESCRIPCION",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextFieldObligatory",
            maskRe: /[a-zA-Z ]/,
            allowBlank: false
          }
        },
        {
          header: "DISPONIBILIDAD",
          dataIndex: "availability",
          width: 120,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "AREA",
          dataIndex: "workArea",
          width: 180,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "RESP. CUIDADO",
          dataIndex: "jobPlace",
          width: 250,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "TIPO",
          dataIndex: "typeResource",
          width: 200,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);
