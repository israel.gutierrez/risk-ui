Ext.define('DukeSource.view.risk.parameter.businessContinuity.PanelRegisterTypeSolution', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.PanelRegisterTypeSolution',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newTypeSolution'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteTypeSolution'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchTypeSolution',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'typeSolutionAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {
                xtype: 'GridPanelRegisterTypeSolution',
                padding: '2 2 2 2'
            }
        ];
        this.callParent();
    }
});
