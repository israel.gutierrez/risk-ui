Ext.define('DukeSource.view.risk.parameter.businessContinuity.PanelRegisterOwnerSolution', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.PanelRegisterOwnerSolution',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newOwnerSolution'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteOwnerSolution'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchOwnerSolution',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'ownerSolutionAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {
                xtype: 'GridPanelRegisterOwnerSolution',
                padding: '2 2 2 2'
            }
        ];
        this.callParent();
    }
});
