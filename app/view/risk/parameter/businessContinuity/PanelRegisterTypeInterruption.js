Ext.define('DukeSource.view.risk.parameter.businessContinuity.PanelRegisterTypeInterruption', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.PanelRegisterTypeInterruption',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newTypeInterruption'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteTypeInterruption'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchTypeInterruption',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'typeInterruptionAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {
                xtype: 'GridPanelRegisterTypeInterruption',
                padding: '2 2 2 2'
            }
        ];
        this.callParent();
    }
});
