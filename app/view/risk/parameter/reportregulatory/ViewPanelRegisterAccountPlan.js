Ext.define('DukeSource.view.risk.parameter.reportregulatory.ViewPanelRegisterAccountPlan', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterAccountPlan',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newAccountPlan'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteAccountPlan'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchAccountPlan',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'accountPlanAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterAccountPlan', padding: '2 2 2 2'}];
        this.callParent();
    }
});
