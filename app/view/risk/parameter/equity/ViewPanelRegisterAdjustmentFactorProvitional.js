Ext.define('DukeSource.view.risk.parameter.equity.ViewPanelRegisterAdjustmentFactorProvitional', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterAdjustmentFactorProvitional',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newAdjustmentFactorProvitional'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteAdjustmentFactorProvitional'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchAdjustmentFactorProvitional',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'adjustmentFactorProvitionalAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterAdjustmentFactorProvitional', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
