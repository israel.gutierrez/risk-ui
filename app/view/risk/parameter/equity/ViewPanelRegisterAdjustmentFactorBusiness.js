Ext.define('DukeSource.view.risk.parameter.equity.ViewPanelRegisterAdjustmentFactorBusiness', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterAdjustmentFactorBusiness',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newAdjustmentFactorBusiness'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteAdjustmentFactorBusiness'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchAdjustmentFactorBusiness',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'adjustmentFactorBusinessAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterAdjustmentFactorBusiness', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
