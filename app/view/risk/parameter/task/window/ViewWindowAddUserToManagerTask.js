Ext.define("ModelGridUserAvailableManageTask", {
  extend: "Ext.data.Model",
  fields: [
    "idRisk",
    "idRiskEvaluationMatrix",
    "descriptionRisk",
    "descriptionProcess",
    "description",
    "idCatalogRisk",
    "codeRisk",
    "descriptionFrequency",
    "descriptionImpact",
    "descriptionScaleRisk"
  ]
});

var StoreGridUserAvailableManageTask = Ext.create("Ext.data.Store", {
  model: "StoreGridUserAvailableManageTask",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("ModelGridUserAssignedManageTask", {
  extend: "Ext.data.Model",
  fields: [
    "idRisk",
    "idRiskEvaluationMatrix",
    "descriptionRisk",
    "descriptionProcess",
    "description",
    "idCatalogRisk",
    "codeRisk",
    "descriptionFrequency",
    "descriptionImpact",
    "descriptionScaleRisk"
  ]
});

var StoreGridUserAssignedManageTask = Ext.create("Ext.data.Store", {
  model: "ModelGridUserAssignedManageTask",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.parameter.task.window.ViewWindowAddUserToManagerTask",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowAddUserToManagerTask",
    border: false,
    height: 500,
    width: 1000,
    title: "ASIGNAR DESTINATARIO",
    titleAlign: "center",
    layout: "border",
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            flex: 1,
            bodyPadding: 10,
            region: "west",
            collapseDirection: "left",
            collapsed: true,
            collapsible: true,
            split: true,
            title: "PARAMETROS DE BUSQUEDA",
            items: [
              {
                xtype: "combobox",
                anchor: "100%",
                fieldLabel: "AGENCIA"
              },
              {
                xtype: "combobox",
                anchor: "100%",
                fieldLabel: "AREA"
              },
              {
                xtype: "combobox",
                anchor: "100%",
                fieldLabel: "CARGO"
              },
              {
                xtype: "combobox",
                anchor: "100%",
                fieldLabel: "CATEGORIA"
              },
              {
                xtype: "container",
                height: 30,
                layout: {
                  type: "hbox",
                  align: "stretch",
                  pack: "center"
                },
                items: [
                  {
                    xtype: "button",
                    iconCls: "search",
                    text: "BUSCAR"
                  }
                ]
              }
            ]
          },
          {
            xtype: "gridpanel",
            flex: 1,
            padding: "0 2 0 0",
            region: "center",
            title: "USUARIOS DISPONIBLES",
            columns: [
              { xtype: "rownumberer", width: 25, sortable: false },
              {
                header: "USUARIO",
                dataIndex: "nameEvaluation",
                flex: 1
              },
              {
                header: "EMAIL",
                dataIndex: "nameEvaluation",
                flex: 1
              }
            ],
            store: StoreGridUserAvailableManageTask,
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridUserAvailableManageTask,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE
            }
          },
          {
            xtype: "gridpanel",
            region: "east",
            flex: 1,
            title: "USUARIOS ASIGNADOS",
            columns: [
              { xtype: "rownumberer", width: 25, sortable: false },
              {
                header: "USUARIO",
                dataIndex: "nameEvaluation",
                flex: 1
              },
              {
                header: "EMAIL",
                dataIndex: "nameEvaluation",
                flex: 1
              }
            ],
            store: StoreGridUserAssignedManageTask,
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridUserAssignedManageTask,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE
            }
          }
        ],
        buttons: [
          {
            text: "GUARDAR",
            iconCls: "save"
          },
          {
            text: "SALIR",
            iconCls: "logout",
            handler: function() {
              me.close();
            }
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
