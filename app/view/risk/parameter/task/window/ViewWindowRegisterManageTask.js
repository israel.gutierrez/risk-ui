Ext.define('DukeSource.view.risk.parameter.task.window.ViewWindowRegisterManageTask', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowRegisterManageTask',
    border: false,
    width: 659,
    layout: 'fit',
    title: 'INGRESO TAREA PROGRAMADA',
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            fieldLabel: 'NOMBRE'
                        },
                        {
                            xtype: 'textareafield',
                            anchor: '100%',
                            fieldLabel: 'DESCRIPCION'
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'datefield',
                                    flex: 1,
                                    padding: '0 10 0 0',
                                    fieldLabel: 'INICIO VIGENCIA'
                                },
                                {
                                    xtype: 'datefield',
                                    flex: 1,
                                    padding: '0 0 0 10',
                                    fieldLabel: 'FIN VIGENCIA'
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    iconCls: 'logout',
                    handler: function () {
                        me.close();
                    }
                }
            ]
        });

        me.callParent(arguments);
    }

});
