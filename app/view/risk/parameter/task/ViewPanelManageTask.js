Ext.define("ModelGridPanelManageTask", {
  extend: "Ext.data.Model",
  fields: [
    "idJob",
    "code",
    "name",
    "classJob",
    "cron",
    "state",
    "timeDown",
    {
      name: "lastExecution",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "nextExecution",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    }
  ]
});

var StoreGridPanelManageTask = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelManageTask",
  autoLoad: true,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListJobsActives.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("DukeSource.view.risk.parameter.task.ViewPanelManageTask", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelManageTask",
  border: false,
  layout: {
    type: "fit"
  },
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "gridpanel",
          region: "center",
          padding: 2,
          columns: [
            {
              xtype: "rownumberer",
              width: 25,
              sortable: false
            },
            {
              header: "Nombre del Job",
              dataIndex: "name",
              flex: 3
            },
            {
              header: "Identificador",
              dataIndex: "code",
              flex: 1
            },
            {
              header: "Estado",
              align: "center",
              flex: 1
            },
            {
              xtype: "datecolumn",
              header: "Última ejecución",
              dataIndex: "lastExecution",
              flex: 1,
              format: "d/m/Y H:i:s",
              align: "center"
            },
            {
              xtype: "datecolumn",
              header: "Próxima ejecución",
              dataIndex: "nextExecution",
              flex: 1,
              format: "d/m/Y H:i:s",
              align: "center"
            },
            {
              header: "Tiempo restante",
              dataIndex: "timeDown",
              flex: 1,
              align: "center",
              renderer: function(v, m, r) {
                // timeCountDown(r);
                return v;
              }
            },
            {
              xtype: "actioncolumn",
              align: "center",
              flex: 1,
              header: "Lanzar",
              icon: "images/play_green.png",
              handler: function(grid, rowIndex) {
                var row = grid.store.getAt(rowIndex);

                DukeSource.lib.Ajax.request({
                  method: "POST",
                  url: "http://localhost:9000/giro/startSelectJob.htm",
                  params: {
                    dataClass: row.get("classJob"),
                    nameJob: row.get("code"),
                    cron: row.get("cron"),
                    idJob: row.get("idJob")
                  },
                  success: function(response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                      DukeSource.global.DirtyView.messageNormal(
                        response.message
                      );
                      grid.getStore().load();
                    } else {
                      DukeSource.global.DirtyView.messageWarning(
                        response.message
                      );
                    }
                  },
                  failure: function() {}
                });
              }
            }
          ],
          store: StoreGridPanelManageTask,
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: StoreGridPanelManageTask,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE
          },
          listeners: {
            render: function() {}
          },
          dockedItems: [
            {
              xtype: "toolbar",
              dock: "top",
              items: [
                {
                  xtype: "button",
                  iconCls: "play",
                  action: "addManageTask",
                  text: "Iniciar"
                },
                "-",
                {
                  xtype: "button",
                  iconCls: "stop",
                  action: "deleteManageTask",
                  text: "Detener"
                }
              ]
            }
          ]
        }
      ]
    });

    me.callParent(arguments);
  }
});

function timeCountDown(record) {
  var countDownDate = new Date(record.get("nextExecution")).getTime();

  var x = setInterval(function() {
    var now = new Date().getTime();

    var distance = countDownDate - now;

    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    if (distance < 0) {
      clearInterval(x);
    }

    record.set(
      "timeDown",
      days + "d " + hours + "h " + minutes + "m " + seconds + "s "
    );
  }, 1000);
}
