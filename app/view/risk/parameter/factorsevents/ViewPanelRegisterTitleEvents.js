Ext.define('DukeSource.view.risk.parameter.factorsevents.ViewPanelRegisterTitleEvents', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterTitleEvents',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'Nuevo',
            iconCls: 'add',
            scale: 'medium',
            type: 'O',
            cls: 'my-btn',
            overCls: 'my-over',
            action: 'newTitleEvents'
        }, '-',
        {
            text: 'Eliminar',
            scale: 'medium',
            type: 'O',
            cls: 'my-btn',
            overCls: 'my-over',
            iconCls: 'delete',
            action: 'deleteTitleEvents'
        }, '-'
        ,
        {
            xtype: 'UpperCaseTextField',
            action: 'searchTitleEvents',
            fieldLabel: 'Buscar',
            labelWidth: 60,
            width: 300
        }


    ],
    initComponent: function () {
        this.items = [
            {
                xtype: 'ViewGridPanelTitleEvents', padding: 2
            }
        ];
        this.callParent();
    }
});
