Ext.define('DukeSource.view.risk.parameter.factorsevents.ViewPanelRegisterEvent', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterEvent',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'Nuevo',
            iconCls: 'add',
            action: 'newRegisterEvent'
        }, '-'
        , '->',
        {
            text: 'Auditor&iacute;a',
            action: 'registerEventAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {
                xtype: 'ViewTreeGridPanelRegisterEvent',
                padding: 2
            }
        ];
        this.callParent();
    }
});
