Ext.define(
  "DukeSource.view.risk.parameter.factorsevents.ViewPanelRegisterFactorRisk",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelRegisterFactorRisk",
    border: false,
    layout: "fit",
    tbar: [
      {
        text: "NUEVO",
        itemId: "newTreeFactorRisk",
        iconCls: "add",
        scale: "medium",
        type: "O",
        cls: "my-btn",
        overCls: "my-over",
        action: "newFactorRisk"
      },
      "-",
      {
        text: "MODIFICAR",
        itemId: "modifyTreeFactorRisk",
        iconCls: "modify",
        scale: "medium",
        type: "O",
        cls: "my-btn",
        overCls: "my-over",
        action: "modifyFactorRisk"
      },
      "-",
      {
        text: "ELIMINAR",
        itemId: "deleteTreeFactorRisk",
        iconCls: "delete",
        scale: "medium",
        type: "O",
        cls: "my-btn",
        overCls: "my-over",
        action: "deleteFactorRisk"
      },
      "-",
      {
        xtype: "UpperCaseTextField",
        action: "searchFactorRisk",
        fieldLabel: "BUSCAR",
        labelWidth: 60,
        width: 300,
        enableKeyEvents: true,
        listeners: {
          afterrender: function(e) {
            e.focus(false, 100);
          },
          buffer: 3000
        }
      }
    ],
    initComponent: function() {
      this.items = [
        {
          xtype: "ViewGridPanelRegisterFactorRisk",
          border: false
        }
      ];
      this.callParent();
    }
  }
);
