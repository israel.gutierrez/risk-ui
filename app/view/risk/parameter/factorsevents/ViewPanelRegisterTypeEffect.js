Ext.define('DukeSource.view.risk.parameter.factorsevents.ViewPanelRegisterTypeEffect', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterTypeEffect',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'Nuevo',
            iconCls: 'add',
            scale: 'medium',
            type: 'O',
            cls: 'my-btn',
            overCls: 'my-over',
            action: 'newTypeEffect'
        }, '-',
        {
            text: 'Eliminar',
            scale: 'medium',
            type: 'O',
            cls: 'my-btn',
            overCls: 'my-over',
            iconCls: 'delete',
            action: 'deleteTypeEffect'
        }, '-'
        ,
        {
            xtype: 'UpperCaseTextField',
            action: 'searchTypeEffect',
            fieldLabel: 'Buscar',
            labelWidth: 60,
            width: 300
        }

    ],
    initComponent: function () {
        this.items = [
            {
                xtype: 'ViewGridPanelTypeEffect', padding: 2
            }
        ];
        this.callParent();
    }
});
