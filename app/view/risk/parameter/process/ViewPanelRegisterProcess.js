Ext.define('DukeSource.view.risk.parameter.process.ViewPanelRegisterProcess', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterProcess',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newProcess'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteProcess'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchProcess',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'processAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterProcess', padding: '2 2 2 2'}];
        this.callParent();
    }
});
