Ext.define('DukeSource.view.risk.parameter.process.ViewPanelRegisterValuationFeatureProcess', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterValuationFeatureProcess',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newValuationFeatureProcess'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteValuationFeatureProcess'
        }, '-'
//                ,
//                {
//                    xtype: 'UpperCaseTextField',
//                    action:'searchValuationFeatureProcess',
//                    fieldLabel:'BUSCAR',
//                    labelWidth:60,
//                    width:300
//                },'-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'valuationFeatureProcessAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterValuationFeatureProcess', padding: '2 2 2 2'}];
        this.callParent();
    }
});
