Ext.define('DukeSource.view.risk.parameter.process.ViewPanelRegisterProcessQualification', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterProcessQualification',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newProcessQualification'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteProcessQualification'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchProcessQualification',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'processQualificationAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterProcessQualification', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
