Ext.define("ModelGridPanelUsers", {
  extend: "Ext.data.Model",
  fields: [
    "userName",
    "id",
    "username",
    "fullName",
    "email",
    "category",
    "workArea",
    "jobPlace",
    "agency",
    "region",
    "descriptionAgency",
    "descriptionRegion",
    "descriptionJobPlace",
    "descriptionWorkArea",
    "descriptionProcessType"
  ]
});
var sm = Ext.create("Ext.selection.CheckboxModel");

var StoreGridPanelUsersAvailable = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelUsers",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

var storeTypeOwner = Ext.create("Ext.data.Store", {
  fields: ["value", "description"],
  pageSize: 9999,
  autoLoad: true,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
    extraParams: {
      propertyOrder: "description",
      propertyFind: "identified",
      valueFind: "TYPE_PROCESS_OWNER"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.process.ViewWindowAddOwnerToProcess",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowAddOwnerToProcess",
    height: 500,
    width: 980,
    title: "Asignar responsable",
    titleAlign: "center",
    layout: "border",
    border: false,
    initComponent: function() {
      var me = this;
      var actionButton = this.actionButton;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "container",
            region: "center",
            layout: {
              type: "vbox",
              align: "stretch"
            },
            items: [
              {
                xtype: "gridpanel",
                name: "griOwnerProcessAvailable",
                itemId: "griOwnerProcessAvailable",
                flex: 1,
                selModel: sm,
                store: StoreGridPanelUsersAvailable,
                loadMask: true,
                title: "Disponibles",
                columnLines: true,
                plugins: [
                  Ext.create("Ext.grid.plugin.CellEditing", {
                    clicksToEdit: 1
                  })
                ],
                tbar: [
                  {
                    xtype: "UpperCaseTextField",
                    fieldLabel: "Usuario",
                    labelWidth: 120,
                    padding: 2,
                    enableKeyEvents: true,
                    width: 400,
                    listeners: {
                      afterrender: function(f, e) {
                        f.focus(false, 200);
                      },
                      keyup: function(e, f) {
                        var grid = e.up("gridpanel");
                        if (f.getKey() === f.ENTER) {
                          grid.store.getProxy().extraParams = {
                            fields: "usu.fullName, usu.state",
                            values:
                              "%" +
                              e.getValue().toUpperCase() +
                              "%," +
                              DukeSource.global.GiroConstants.YES,
                            types: "String,String",
                            operators: "like,equal",
                            searchIn: "User"
                          };
                          grid.store.getProxy().url =
                            "http://localhost:9000/giro/advancedSearchUser.htm";
                          grid.down("pagingtoolbar").moveFirst();
                        }
                      }
                    }
                  }
                ],
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    header: "CODIGO",
                    dataIndex: "userName",
                    width: 100,
                    renderer: function(value, meta) {
                      meta.tdCls = "user-cell";
                      return value;
                    }
                  },
                  {
                    header: "Tipo",
                    dataIndex: "type",
                    flex: 0.6,
                    tdCls: "column-obligatory",
                    editor: {
                      xtype: "combobox",
                      name: "type",
                      allowBlank: false,
                      itemId: "type",
                      queryMode: "local",
                      displayField: "description",
                      valueField: "value",
                      store: storeTypeOwner
                    },
                    renderer: function(value) {
                      if (value !== undefined) {
                        var idx = storeTypeOwner.find("value", value);
                        var rec = storeTypeOwner.getAt(idx);
                        return rec.get("description");
                      }
                    }
                  },
                  {
                    header: "Nombre",
                    dataIndex: "fullName",
                    flex: 1.5,
                    tdCls: "wrap"
                  },
                  {
                    header: "Cargo",
                    dataIndex: "descriptionJobPlace",
                    flex: 1.5
                  },
                  {
                    header: "Categoría",
                    dataIndex: "category",
                    width: 100
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: StoreGridPanelUsersAvailable,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  render: function() {
                    var grid = this;
                    DukeSource.global.DirtyView.loadGridDefault(grid);
                  }
                }
              }
            ]
          },
          {
            xtype: "AdvancedSearchUser",
            region: "west",
            collapseDirection: "left",
            collapsed: true,
            background: "#F5D705 !important;",
            collapsible: true,
            split: true,
            selModel: sm,
            flex: 0.4,
            title: "Búsqueda avanzada",
            tbar: [
              {
                text: "Buscar",
                scale: "medium",
                iconCls: "search",
                margin: "0 5 0 0",
                handler: function(btn) {
                  var form = btn.up("form");
                  var grid = me.down("#griOwnerProcessAvailable");
                  DukeSource.global.DirtyView.findAdvancedUserGeneric(
                    form,
                    grid
                  );
                }
              },
              {
                text: "Limpiar",
                scale: "medium",
                iconCls: "clear",
                margin: "0 5 0 0",
                handler: function() {
                  me.down("form")
                    .getForm()
                    .reset();
                }
              }
            ]
          }
        ],
        buttons: [
          {
            text: "Guardar",
            iconCls: "save",
            scale: "medium",
            cls: "my-btn",
            overCls: "my-over",
            action: actionButton
          },
          {
            text: "Salir",
            scale: "medium",
            iconCls: "logout",
            handler: function() {
              me.close();
            }
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);
