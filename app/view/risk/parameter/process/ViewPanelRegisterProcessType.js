Ext.define("ModelGridPanelOwners", {
  extend: "Ext.data.Model",
  fields: [
    "userName",
    "id",
    "type",
    "descriptionType",
    "username",
    "fullName",
    "email",
    "category",
    "descriptionJobPlace",
    "descriptionWorkArea",
    "descriptionProcessType",
    "descriptionProcess",
    "descriptionSubProcess",
    "descriptionActivity",
    "idProcessType",
    "idProcess",
    "idSubProcess",
    "idActivity"
  ]
});

var StoreGridPanelProcessOwner = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelOwners",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.parameter.process.ViewPanelRegisterProcessType",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelRegisterProcessType",
    border: false,
    layout: {
      type: "hbox",
      align: "stretch"
    },
    initComponent: function() {
      var me = this;
      var sm = Ext.create("Ext.selection.CheckboxModel");
      Ext.applyIf(me, {
        items: [
          {
            xtype: "ViewTreeGridPanelRegisterProcessType",
            flex: 1.05,
            style: "border-right:1px solid #99bce8",
            border: false,
            listeners: {
              itemclick: function(view, record, item, index, e) {
                var panel = Ext.ComponentQuery.query(
                  "ViewPanelRegisterProcessType"
                )[0];
                var tree = panel.down("ViewTreeGridPanelRegisterProcessType");
                var node = tree.getSelectionModel().getSelection()[0];

                if (node === undefined) {
                  DukeSource.global.DirtyView.messageWarning(
                    DukeSource.global.GiroMessages.MESSAGE_ITEM
                  );
                } else {
                  var depth = node.data["depth"];

                  var process = [];
                  var recordsProcess = tree.getSelectionModel().getSelection();

                  var ids = "";

                  ids = DukeSource.app
                    .getController(
                      "DukeSource.controller.risk.parameter.process.ControllerPanelRegisterProcessType"
                    )
                    .settingByDepth(recordsProcess, process, depth, ids);

                  var grid = panel.down("#griOwnerProcess");
                  grid.store.getProxy().url =
                    "http://localhost:9000/giro/showListOwnerToProcess.htm?nameView=ViewPanelRegisterProcessType";
                  grid.store.getProxy().extraParams = {
                    depth: depth,
                    ids: ids
                  };
                  grid.down("pagingtoolbar").doRefresh();
                }
              }
            }
          },
          {
            xtype: "gridpanel",
            name: "griOwnerProcess",
            itemId: "griOwnerProcess",
            title: "Responsables",
            selModel: sm,
            flex: 1.2,
            margin: "0 0 0 2",
            style: "border-left:1px solid #99bce8",
            store: StoreGridPanelProcessOwner,
            loadMask: true,
            border: false,
            columnLines: true,
            plugins: [
              Ext.create("Ext.grid.plugin.RowEditing", {
                clicksToMoveEditor: 1,
                saveBtnText: "Guardar",
                cancelBtnText: "Cancelar",
                autoCancel: false,
                completeEdit: function() {
                  var me = this;
                  var grid = me.grid;
                  var selModel = grid.getSelectionModel();
                  var record = selModel.getLastSelected();
                  if (me.editing && me.validateEdit()) {
                    me.editing = false;
                    DukeSource.lib.Ajax.request({
                      method: "POST",
                      url: "http://localhost:9000/giro/updateOwnerProcess.htm",
                      params: {
                        jsonData: Ext.JSON.encode(record.data)
                      },
                      success: function(response) {
                        response = Ext.decode(response.responseText);
                        if (response.success) {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_MESSAGE,
                            response.mensaje,
                            Ext.Msg.INFO
                          );
                          grid.down("pagingtoolbar").moveFirst();
                        } else {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_ERROR,
                            response.mensaje,
                            Ext.Msg.ERROR
                          );
                        }
                      },
                      failure: function() {}
                    });
                    me.fireEvent("edit", me, me.context);
                  }
                }
              })
            ],
            tbar: [
              {
                text: "Agregar",
                iconCls: "users",
                type: "O",
                action: "assignOwnerProcess"
              },
              {
                text: "Eliminar",
                iconCls: "delete",
                action: "deleteOwnerProcess"
              },
              "->",
              {
                text: "Inventario de procesos",
                iconCls: "excel",
                hidden: true,
                action: "reportInventoryProcess"
              }
            ],
            columns: [
              { xtype: "rownumberer", width: 25, sortable: false },
              {
                header: "Nombre",
                dataIndex: "fullName",
                width: 260,
                renderer: function(value, meta) {
                  meta.tdCls = "user-cell";
                  return value;
                }
              },
              {
                header: "Tipo",
                dataIndex: "type",
                align: "center",
                width: 100,
                tdCls: "column-obligatory",
                editor: {
                  xtype: "combobox",
                  name: "type",
                  allowBlank: false,
                  itemId: "type",
                  queryMode: "local",
                  displayField: "description",
                  valueField: "value",
                  store: {
                    fields: ["value", "description"],
                    pageSize: 9999,
                    autoLoad: true,
                    proxy: {
                      actionMethods: {
                        create: "POST",
                        read: "POST",
                        update: "POST"
                      },
                      type: "ajax",
                      url:
                        "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                      extraParams: {
                        propertyOrder: "description",
                        propertyFind: "identified",
                        valueFind: "TYPE_PROCESS_OWNER"
                      },
                      reader: {
                        type: "json",
                        root: "data",
                        successProperty: "success"
                      }
                    }
                  }
                },
                renderer: function(value, metaData, record) {
                  return record.get("descriptionType");
                }
              },
              {
                header: "Cargo",
                dataIndex: "descriptionJobPlace",
                width: 300
              },
              {
                header: "Categoría",
                dataIndex: "category",
                width: 100
              },
              {
                header: "Proceso",
                dataIndex: "descriptionProcessType",
                width: 350,
                renderer: function(value, meta) {
                  meta.tdCls = "folder-cell";
                  return value;
                }
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridPanelProcessOwner,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            },
            listeners: {
              render: function() {
                var grid = this;
                DukeSource.global.DirtyView.loadGridDefault(grid);
              },
              beforerender: function(view) {
                view.headerCt.border = 1;
              }
            }
          }
        ]
      });
      this.callParent();
    }
  }
);
