Ext.define('DukeSource.view.risk.parameter.process.ViewPanelRegisterSubProcess', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterSubProcess',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newSubProcess'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteSubProcess'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchSubProcess',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'subProcessAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterSubProcess', padding: '2 2 2 2'}];
        this.callParent();
    }
});
