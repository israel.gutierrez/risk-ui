Ext.define('DukeSource.view.risk.parameter.process.AddMenuSubProcess', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.AddMenuSubProcess',
    width: 140,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'menuitem',
                    text: 'Agregar',
                    iconCls: 'add'
                },
                {
                    xtype: 'menuitem',
                    text: 'Responsable',
                    iconCls: 'users'
                },
                {
                    xtype: 'menuitem',
                    text: 'Evaluar Proceso',
                    iconCls: 'matrix'
                },
                {
                    xtype: 'menuitem',
                    text: 'Gestionar Adjuntos',
                    iconCls: 'gestionfile'
                },
                {
                    xtype: 'menuitem',
                    text: 'Editar',
                    iconCls: 'modify'
                },
                {
                    xtype: 'menuitem',
                    text: 'Eliminar',
                    iconCls: 'delete'
                }
            ]
        });

        me.callParent(arguments);
    }
});