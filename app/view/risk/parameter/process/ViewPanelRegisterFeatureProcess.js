Ext.define('DukeSource.view.risk.parameter.process.ViewPanelRegisterFeatureProcess', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterFeatureProcess',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newFeatureProcess'
        }, '-'
//                ,
//                {
//                    text: 'ELIMINAR',
//                    iconCls:'delete',
//                    action:'deleteFeatureProcess'
//                },'-',
//                {
//                    xtype: 'UpperCaseTextField',
//                    action:'searchFeatureProcess',
//                    fieldLabel:'BUSCAR',
//                    labelWidth:60,
//                    width:300
//                },'-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'featureProcessAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewTreeGridPanelRegisterFeatureProcess', padding: '2 2 2 2'}];
        this.callParent();
    }
});
