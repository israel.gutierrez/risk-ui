Ext.define('DukeSource.view.risk.parameter.process.AddMenuActivity', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.AddMenuActivity',
    width: 130,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'menuitem',
                    text: 'Agregar',
                    iconCls: 'add'
                },
//                {
//                    xtype: 'menuitem',
//                    text: 'Evaluar',
//                    iconCls: 'matrix'
//                },
//                {
//                    xtype: 'menuitem',
//                    text: 'Gestionar Adjuntos',
//                    iconCls: 'gestionfile'
//                },
                {
                    xtype: 'menuitem',
                    text: 'Editar',
                    iconCls: 'modify'
                },
                {
                    xtype: 'menuitem',
                    text: 'Eliminar',
                    iconCls: 'delete'
                }
            ]
        });

        me.callParent(arguments);
    }
});