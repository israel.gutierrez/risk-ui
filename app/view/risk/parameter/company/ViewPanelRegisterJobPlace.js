Ext.define('DukeSource.view.risk.parameter.company.ViewPanelRegisterJobPlace', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterJobPlace',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newJobPlace'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteJobPlace'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchJobPlace',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'jobPlaceAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterJobPlace', padding: '2 2 2 2'}];
        this.callParent();
    }
});
