Ext.define('DukeSource.view.risk.parameter.company.EditMenuAgency', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.EditMenuAgency',
    width: 120,
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [

                {
                    xtype: 'menuitem',
                    text: 'Editar',
                    iconCls: 'modify'
                },
                {
                    xtype: 'menuitem',
                    text: 'Eliminar',
                    iconCls: 'delete'
                }
            ]
        });

        me.callParent(arguments);
    }
});