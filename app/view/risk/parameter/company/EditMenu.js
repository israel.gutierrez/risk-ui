Ext.define('DukeSource.view.risk.parameter.company.EditMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.EditMenu',
    width: 120,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'menuitem',
                    text: 'Edit',
                    iconCls: 'icon-edit'
                },
                {
                    xtype: 'menuitem',
                    text: 'Delete',
                    iconCls: 'icon-delete'
                }
            ]
        });

        me.callParent(arguments);
    }
});