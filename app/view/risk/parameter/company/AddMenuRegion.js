Ext.define('DukeSource.view.risk.parameter.company.AddMenuRegion', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.AddMenuRegion',
    width: 120,
    initComponent: function () {
        var me = this;
        var record = this.record;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'menuitem',
                    text: 'Agregar',
                    iconCls: 'add',
                    record: record
                }
                /*,
                {
                    xtype: 'menuitem',
                    text: 'Editar',
                    iconCls: 'modify',
                    record: record
                }*/
            ]
        });

        me.callParent(arguments);
    }
});