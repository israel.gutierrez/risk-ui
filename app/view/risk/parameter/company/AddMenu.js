Ext.define('DukeSource.view.risk.parameter.company.AddMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.AddMenu',
    width: 120,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'menuitem',
                    text: 'Add',
                    iconCls: 'icon-add'
                }
            ]
        });

        me.callParent(arguments);
    }
});