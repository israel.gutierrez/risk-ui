Ext.define('DukeSource.view.risk.parameter.company.ViewPanelRegisterAgency', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterAgency',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newAgency'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteAgency'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchAgency',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'agencyAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterAgency', padding: '2 2 2 2'}];
        this.callParent();
    }
});
