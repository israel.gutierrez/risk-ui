Ext.define("DukeSource.view.risk.parameter.company.ViewPanelRegisterCompany", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelRegisterCompany",
  border: false,
  layout: "fit",
  tbar: [
    {
      xtype: "UpperCaseTextField",
      action: "searchCompany",
      fieldLabel: "BUSCAR",
      labelWidth: 60,
      width: 300
    },
    "-",
    "->",
    {
      text: "AUDITORIA",
      action: "companyAuditory",
      iconCls: "auditory"
    }
  ],
  initComponent: function() {
    this.items = [
      {
        xtype: "ViewTreeGridPanelRegisterCompany",
        padding: 2
      }
    ];
    this.callParent();
  }
});
