Ext.define('DukeSource.view.risk.parameter.kri.ViewPanelRegisterScorecardMaster', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterScorecardMaster',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newScorecardMaster'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteScorecardMaster'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchScorecardMaster',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'scorecardMasterAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterScorecardMaster', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
