Ext.define('DukeSource.view.risk.parameter.kri.ViewPanelRegisterWeightEvent', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterWeightEvent',
    border: false,
    layout: 'fit',
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterWeightEvent', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
