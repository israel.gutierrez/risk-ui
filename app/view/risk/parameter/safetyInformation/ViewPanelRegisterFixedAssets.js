Ext.define('DukeSource.view.risk.parameter.safetyInformation.ViewPanelRegisterFixedAssets', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterFixedAssets',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newFixedAssets'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteFixedAssets'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchFixedAssets',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'fixedAssetsAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterFixedAssets', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
