Ext.define('DukeSource.view.risk.parameter.safetyInformation.ViewPanelRegisterTechnicalCustodio', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterTechnicalCustodio',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newTechnicalCustodio'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteTechnicalCustodio'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchTechnicalCustodio',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'technicalCustodioAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterTechnicalCustodio', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
