Ext.define('DukeSource.view.risk.parameter.safetyInformation.ViewPanelRegisterAvailability', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterAvailability',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newAvailability'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteAvailability'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchAvailability',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'availabilityAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterAvailability', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
