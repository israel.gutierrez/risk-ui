Ext.define('DukeSource.view.risk.parameter.safetyInformation.ViewPanelRegisterConfidentiality', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterConfidentiality',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newConfidentiality'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteConfidentiality'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchConfidentiality',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'confidentialityAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterConfidentiality', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
