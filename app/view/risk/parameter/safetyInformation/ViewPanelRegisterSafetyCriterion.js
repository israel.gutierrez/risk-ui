Ext.define('DukeSource.view.risk.parameter.safetyInformation.ViewPanelRegisterSafetyCriterion', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterSafetyCriterion',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newSafetyCriterion'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteSafetyCriterion'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchSafetyCriterion',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'safetyCriterionAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterSafetyCriterion', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
