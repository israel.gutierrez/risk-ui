Ext.define('DukeSource.view.risk.parameter.safetyInformation.ViewPanelRegisterIntegrity', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterIntegrity',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newIntegrity'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteIntegrity'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchIntegrity',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'integrityAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterIntegrity', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
