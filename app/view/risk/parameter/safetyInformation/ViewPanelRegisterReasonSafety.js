Ext.define('DukeSource.view.risk.parameter.safetyInformation.ViewPanelRegisterReasonSafety', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterReasonSafety',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newReasonSafety'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteReasonSafety'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchReasonSafety',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'reasonSafetyAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterReasonSafety', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
