Ext.define('DukeSource.view.risk.parameter.safetyInformation.ViewPanelRegisterSubClassification', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterSubClassification',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newSubClassification'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteSubClassification'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchSubClassification',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'subClassificationAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterSubClassification', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
