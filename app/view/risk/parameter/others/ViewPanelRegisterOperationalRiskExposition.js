Ext.define(
  "DukeSource.view.risk.parameter.others.ViewPanelRegisterOperationalRiskExposition",
  {
    extend: "Ext.tab.Panel",
    alias: "widget.ViewPanelRegisterOperationalRiskExposition",
    border: false,
    plain: true,
    layout: "fit",

    //                items: [
    //                    {
    //                        title:'INCIDENTES PENDIENTES DE REVISION',
    //                        xtype: 'ViewGridIncidentsPendingRiskOperational'
    //                    },
    //                    {
    //                        title:'INCIDENTES REVISADOS',
    //                        xtype: 'ViewGridIncidentsRevisedRiskOperational'
    //                    }
    //
    //                ],
    initComponent: function() {
      this.items = [
        {
          title: "Máxima Exposición",
          xtype: "ViewGridPanelRegisterOperationalRiskExposition",
          padding: "2 2 2 2"
        },
        {
          xtype: "tabpanel",
          flex: 1,
          plain: true,
          //                    padding: '0 2 2 2',

          items: [
            {
              title: "INCIDENTES PENDIENTES DE REVISION",
              xtype: "ViewGridIncidentsPendingRiskOperational"
            },
            {
              title: "INCIDENTES REVISADOS",
              xtype: "ViewGridIncidentsRevisedRiskOperational"
            }
          ],
          listeners: {
            render: function() {
              if (category == DukeSource.global.GiroConstants.GESTOR) {
                me.down("tabpanel").add({
                  title: "INCIDENTES REVISADOS POR EL ANALISTA",
                  xtype: "ViewGridIncidentsGestorRiskOperational"
                });
              }
            }
          }
        },
        {}
      ];
      this.callParent();
    }
  }
);
