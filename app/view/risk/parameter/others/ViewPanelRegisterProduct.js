Ext.define("DukeSource.view.risk.parameter.others.ViewPanelRegisterProduct", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelRegisterProduct",
  border: false,
  layout: "fit",
  requires: [
    "DukeSource.view.risk.DatabaseEventsLost.treepanel.TreePanelProduct"
  ],
  tbar: [
    {
      text: "Nuevo",
      iconCls: "add",
      action: "newProduct",
      itemId: "newProduct"
    },
    "-",
    {
      text: "Modificar",
      iconCls: "modify",
      action: "modifyProduct",
      itemId: "modifyProduct"
    },
    "-",
    {
      text: "Eliminar",
      iconCls: "delete",
      action: "deleteProduct",
      itemId: "deleteProduct"
    },
    "->",
    {
      text: "Auditoría",
      action: "productAuditory",
      iconCls: "auditory"
    }
  ],
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "container",
          layout: {
            type: "hbox",
            align: "stretch"
          },
          items: [
            {
              xtype: "TreePanelProduct",
              style: "border-right:1px solid #99bce8",
              border: false,
              flex: 1,
              listeners: {
                select: function(a, record) {
                  if (record.data["depth"] === 1) {
                    me.down("#modifyProduct").setDisabled(true);
                    me.down("#deleteProduct").setDisabled(true);
                  } else {
                    me.down("#modifyProduct").setDisabled(false);
                    me.down("#deleteProduct").setDisabled(false);

                    var grid2 = me.down("grid");
                    grid2.down("#factor").focus(false, 200);
                    grid2.down("#factor").setValue(record.get("factor"));

                    grid2.idProduct = record.get("idProduct");

                    grid2.setTitle("Matriz");
                    grid2.setTitle(
                      grid2.title +
                        " &#8702; " +
                        '<span style="color: red">' +
                        record.data["path"] +
                        "</span>"
                    );
                    loadMatrixByProduct(grid2, 1, 1, "equivalentValue");
                  }
                }
              }
            },
            {
              xtype: "gridpanel",
              hidden: hidden("VPRP_PNEL_MatrixProduct"),
              border: false,
              margin: "0 0 0 5",
              title: "Matriz por producto",
              style: "border-left:1px solid #99bce8",
              flex: 1.6,
              name: "matrixByProduct",
              itemId: "matrixByProduct",
              store: StoreGridMatrixByProduct,
              loadMask: true,
              columnLines: true,
              columns: [],
              tbar: [
                {
                  xtype: "numberfield",
                  minValue: 0,
                  maxValue: 100,
                  decimalPrecision: 10,
                  labelWidth: 130,
                  width: 280,
                  fieldLabel: "Factor de escalamiento",
                  itemId: "factor",
                  name: "factor",
                  listeners: {
                    specialkey: function(f, e) {
                      if (e.getKey() === e.ENTER || e.getKey() === e.TAB) {
                        rebuildMatrixByFactor(me.down("grid"), f);
                      }
                    }
                  }
                },
                {
                  text: "Actualizar",
                  iconCls: "update",
                  handler: function() {
                    var grid2 = me.down("grid");
                    loadMatrixByProduct(grid2, 1, 1, "equivalentValue");
                  }
                },
                {
                  text: "Guardar",
                  iconCls: "save",
                  handler: function() {
                    Ext.Ajax.request({
                      method: "POST",
                      url:
                        "http://localhost:9000/giro/saveFactorAffectToProduct.htm",
                      params: {
                        idProduct: me.down("grid").idProduct,
                        factor: me.down("#factor").getValue()
                      },
                      success: function(response) {
                        response = Ext.decode(response.responseText);
                        if (response.success) {
                          DukeSource.global.DirtyView.messageNormal(
                            response.message
                          );
                          var tree = me.down("treepanel");

                          var node = tree.getSelectionModel().getSelection()[0];
                          var path = node.parentNode.getPath();

                          tree.store.load({
                            callback: function() {
                              tree.expandPath(path);
                            }
                          });
                        } else {
                          DukeSource.global.DirtyView.messageWarning(
                            response.message
                          );
                        }
                      },
                      failure: function() {}
                    });
                  }
                }
              ],
              listeners: {
                beforerender: function(view) {
                  view.headerCt.border = 1;
                }
              }
            }
          ]
        }
      ]
    });
    me.callParent(arguments);
  }
});

Ext.define("ModelGridMatrixByProduct", {
  extend: "Ext.data.Model",
  fields: []
});
var StoreGridMatrixByProduct = Ext.create("Ext.data.Store", {
  model: "ModelGridMatrixByProduct",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

function loadMatrixByProduct(
  grid,
  idOperationalRiskExposition,
  idTypeMatrix,
  equivalentValue
) {
  Ext.Ajax.request({
    method: "POST",
    url: "http://localhost:9000/giro/buildHeaderAxisX.htm",
    params: {
      valueFind: idOperationalRiskExposition,
      idTypeMatrix: idTypeMatrix,
      propertyOrder: equivalentValue
    },
    success: function(response) {
      response = Ext.decode(response.responseText);
      if (response.success) {
        var factor = Ext.ComponentQuery.query(
          "ViewPanelRegisterProduct"
        )[0].down("#factor");
        var columns = Ext.JSON.decode(response.data);
        var fields = Ext.JSON.decode(response.fields);
        DukeSource.global.DirtyView.createMatrix(columns, fields, grid);
        grid.store.getProxy().extraParams = {
          idOperationalRiskExposition: idOperationalRiskExposition,
          idTypeMatrix: idTypeMatrix
        };
        grid.store.getProxy().url =
          "http://localhost:9000/giro/findMatrixAndLoad.htm";
        grid.getStore().load({
          callback: function() {
            rebuildMatrixByFactor(grid, factor);
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_ERROR,
          response.message,
          Ext.Msg.ERROR
        );
        grid.getView().refresh();
      }
    },
    failure: function() {}
  });
}

function rebuildMatrixByFactor(grid, f) {
  var matrix = grid.store.data;

  for (var i = 0; i < matrix.length; i++) {
    var row = Object.values(matrix.items[i].data);

    for (var j = 0; j < row.length; j++) {
      row[j]["valueCell"] =
        (parseFloat(row[j]["valueCell"]) * parseFloat(f.getValue())) / 100;
    }
  }
  grid.getView().refresh();
}
