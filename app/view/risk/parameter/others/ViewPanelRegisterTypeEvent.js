Ext.define('DukeSource.view.risk.parameter.others.ViewPanelRegisterTypeEvent', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterTypeEvent',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newTypeEvent'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteTypeEvent'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDataGridTypeEvent',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'typeEventAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterTypeEvent', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
