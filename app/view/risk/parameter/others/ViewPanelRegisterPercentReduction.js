Ext.define('DukeSource.view.risk.parameter.others.ViewPanelRegisterPercentReduction', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterPercentReduction',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newPercentReduction'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deletePercentReduction'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchPercentReduction',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'percentReductionAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterPercentReduction', padding: '2 2 2 2'}];
        this.callParent();
    }
});
