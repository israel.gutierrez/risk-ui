Ext.define('DukeSource.view.risk.parameter.others.ViewPanelRegisterForeignKeys', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterForeignKeys',
    layout: 'fit',
    border: false,
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newForeignKeys'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteForeignKeys'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchForeignKeys',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-', '->',
        {
            text: 'AUDITORIA',
            //action: 'currencyForeignKeys',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterForeignKeys', padding: '2 2 2 2'}];
        this.callParent();
    }
});