Ext.define('DukeSource.view.risk.parameter.others.ViewPanelRegisterSanctioningCompany', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterSanctioningCompany',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newSanctioningCompany'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteSanctioningCompany'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDataGridSanctioningCompany',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'sanctioningCompanyAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterSanctioningCompany', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
