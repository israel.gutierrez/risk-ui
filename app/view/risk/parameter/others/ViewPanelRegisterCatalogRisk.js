Ext.define('DukeSource.view.risk.parameter.others.ViewPanelRegisterCatalogRisk', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterCatalogRisk',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newCatalogRisk'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteCatalogRisk'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchCatalogRisk',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'catalogRiskAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterCatalogRisk', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
