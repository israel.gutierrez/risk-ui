Ext.define('DukeSource.view.risk.parameter.others.ViewPanelRegisterScaleRatingActionPlan', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterScaleRatingActionPlan',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newScaleRatingActionPlan'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteScaleRatingActionPlan'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchScaleRatingActionPlan',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'scaleRatingActionPlanAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterScaleRatingActionPlan', padding: '2 2 2 2'}];
        this.callParent();
    }
});
