Ext.define('DukeSource.view.risk.parameter.others.ViewPanelRegisterInsuranceCompany', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterInsuranceCompany',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newInsuranceCompany'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteInsuranceCompany'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchInsuranceCompany',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'insuranceCompanyAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterInsuranceCompany', padding: '2 2 2 2'}];
        this.callParent();
    }
});
