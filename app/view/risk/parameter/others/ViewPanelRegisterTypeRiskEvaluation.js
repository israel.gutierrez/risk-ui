Ext.define('DukeSource.view.risk.parameter.others.ViewPanelRegisterTypeRiskEvaluation', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterTypeRiskEvaluation',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newTypeRiskEvaluation'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteTypeRiskEvaluation'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDataGridTypeRiskEvaluation',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'typeRiskEvaluationAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterTypeRiskEvaluation', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
