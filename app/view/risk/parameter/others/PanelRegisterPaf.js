Ext.define('DukeSource.view.risk.parameter.others.PanelRegisterPaf', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.PanelRegisterPaf',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'Nuevo',
            iconCls: 'add',
            action: 'newPaf',
            itemId: 'newPaf'
        }, '-',
        {
            text: 'Modificar',
            iconCls: 'modify',
            action: 'modifyPaf',
            itemId: 'modifyPaf'
        }, '-',
        {
            text: 'Eliminar',
            iconCls: 'delete',
            action: 'deletePaf',
            itemId: 'deletePaf'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchPaf',
            fieldLabel: 'BUSCAR',
            hidden: true,
            labelWidth: 60,
            width: 300
        }
        , '->',
        {
            text: 'Auditoria',
            action: 'auditPaf',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        var me = this;
        this.items = [
            {
                xtype: 'TreeGridPaf',
                border: false,
                listeners: {
                    select: function (a, record) {

                        if (record.data['depth'] === 1) {
                            me.down('#modifyPaf').setDisabled(true);
                            me.down('#deletePaf').setDisabled(true);
                        } else {
                            me.down('#modifyPaf').setDisabled(false);
                            me.down('#deletePaf').setDisabled(false);
                        }
                    }
                }
            }];
        this.callParent();
    }
});
