Ext.define('DukeSource.view.risk.parameter.others.ViewPanelRegisterDetailJson', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterDetailJson',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newDetailJson'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteDetailJson'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDetailJson',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'detailJsonAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
//            {xtype:'ViewGridPanelRegisterDetailJson', padding:'2 2 2 2'}
        ];
        this.callParent();
    }
});

