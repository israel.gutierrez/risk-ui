Ext.define('DukeSource.view.risk.parameter.others.PanelRegisterProcessRecursive', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.PanelRegisterProcessRecursive',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'Nuevo',
            iconCls: 'add',
            action: 'newProcessRecursive',
            itemId: 'newProcessRecursive'
        }, '-',
        {
            text: 'Modificar',
            iconCls: 'modify',
            action: 'modifyProcessRecursive',
            itemId: 'modifyProcessRecursive'
        }, '-',
        {
            text: 'Eliminar',
            iconCls: 'delete',
            action: 'deleteProcessRecursive',
            itemId: 'deleteProcessRecursive'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchProcessRecursive',
            fieldLabel: 'Buscar',
            labelWidth: 60,
            width: 300
        }
        , '->',
        {
            text: 'Auditoria',
            action: 'productProcessRecursive',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        var me = this;
        this.items = [
            {
                xtype: 'TreeGridProcessRecursive',
                border: false,
                listeners: {
                    select: function (a, record) {

                        if (record.data['depth'] === 1) {
                            me.down('#modifyProcessRecursive').setDisabled(true);
                            me.down('#deleteProcessRecursive').setDisabled(true);
                        } else {
                            me.down('#modifyProcessRecursive').setDisabled(false);
                            me.down('#deleteProcessRecursive').setDisabled(false);
                        }
                    }
                }
            }];
        this.callParent();
    }
});
