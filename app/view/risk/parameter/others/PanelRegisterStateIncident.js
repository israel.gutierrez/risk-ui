Ext.define("DukeSource.view.risk.parameter.others.PanelRegisterStateIncident", {
  extend: "Ext.panel.Panel",
  alias: "widget.PanelRegisterStateIncident",
  border: false,
  layout: "fit",
  tbar: [
    {
      text: "NUEVO",
      iconCls: "add",
      action: "newStateIncident"
    },
    "-",
    {
      text: "ELIMINAR",
      iconCls: "delete",
      action: "deleteStateIncident"
    },
    "-",
    {
      xtype: "combobox",
      editable: false,
      width: 300,
      fieldLabel: "TIPO",
      fieldCls: "obligatoryTextField",
      queryMode: "local",
      displayField: "description",
      valueField: "value",
      store: {
        fields: ["value", "description"],
        pageSize: 9999,
        autoLoad: true,
        proxy: {
          actionMethods: {
            create: "POST",
            read: "POST",
            update: "POST"
          },
          type: "ajax",
          url: "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
          extraParams: {
            propertyOrder: "value",
            propertyFind: "identified",
            valueFind: "TYPEINCIDENT"
          },
          reader: {
            type: "json",
            root: "data",
            successProperty: "success"
          }
        }
      },
      listeners: {
        select: function(cbo) {
          var panel = cbo.up("panel");
          var grid = panel.down("grid");
          grid.store.getProxy().url =
            "http://localhost:9000/giro/findStateIncident.htm";
          grid.store.getProxy().extraParams = {
            propertyFind: "si.typeIncident",
            valueFind: cbo.getValue(),
            propertyOrder: "si.sequence"
          };
          grid.down("pagingtoolbar").moveFirst();
        }
      }
    },
    {
      xtype: "UpperCaseTextField",
      action: "searchStateIncident",
      fieldLabel: "BUSCAR",
      labelWidth: 60,
      width: 300
    },
    "-",
    "->",
    {
      text: "AUDITORIA",
      action: "stateIncidentAuditory",
      iconCls: "auditory"
    }
  ],
  initComponent: function() {
    this.items = [
      {
        xtype: "GridPanelRegisterStateIncident",
        padding: "2 2 2 2"
      }
    ];
    this.callParent();
  }
});
