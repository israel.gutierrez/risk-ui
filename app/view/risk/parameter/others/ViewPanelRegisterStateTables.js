Ext.define('DukeSource.view.risk.parameter.others.ViewPanelRegisterStateTables', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterStateTables',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newStateTables'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteStateTables'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchStateTables',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'stateTablesAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterStateTables', padding: '2 2 2 2'}];
        this.callParent();
    }
});
