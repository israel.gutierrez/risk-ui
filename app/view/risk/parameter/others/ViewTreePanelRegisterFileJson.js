Ext.define('DukeSource.view.risk.parameter.others.ViewTreePanelRegisterFileJson', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.ViewTreePanelRegisterFileJson',
    store: 'risk.parameter.grids.StoreGridPanelRegisterFileJson',
    useArrows: true,
    multiSelect: true,
    singleExpand: false,
    rootVisible: false,
    columns: [
        {
            xtype: 'treecolumn',
            text: 'DOMINIOS',
            flex: 4,
            sortable: true,
            dataIndex: 'text'
        },
        {
            text: 'USUARIOS',
            flex: 2,
            sortable: true,
            dataIndex: 'fieldOne'
        }

    ]
});
