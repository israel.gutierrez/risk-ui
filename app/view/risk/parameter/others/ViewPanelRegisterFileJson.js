Ext.define("DukeSource.view.risk.parameter.others.ViewPanelRegisterFileJson", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelRegisterFileJson",
  border: false,
  layout: "fit",
  tbar: [
    {
      text: "Nuevo",
      iconCls: "add",
      scale: "medium",
      action: "newRegisterFileJson"
    },
    "-",
    {
      text: "Test directorio activo",
      iconCls: "users",
      scale: "medium",
      action: "newSynchronizeJson"
    },
    "-",
    {
      xtype: "combobox",
      queryMode: "local",
      displayField: "description",
      valueField: "idConstantTest",
      editable: true,
      iconCls: "rout",
      typeAhead: true,
      forceSelection: true,
      labelWidth: 160,
      width: 400,
      fieldLabel: "Test de otras conexiones",
      listeners: {
        render: function() {
          var me = this;
          me.store.load();
        },
        select: function(cbo) {
          if (cbo.getValue() === "1") {
            var windows = Ext.create(
              "DukeSource.view.risk.parameter.windows.ViewWindowConfigurationTestActiveDirectory",
              {
                modal: true
              }
            );
            windows.show();
          }
          if (cbo.getValue() === "2") {
            windows = Ext.create(
              "DukeSource.view.fulfillment.parameter.ViewPanelRegisterPropertyEmail",
              {
                width: 900,
                height: 400,
                modal: true
              }
            );
            windows.show();
          }
        }
      },
      store: {
        fields: ["idConstantTest", "description"],
        data: [
          { idConstantTest: "1", description: "Directorio Activo" },
          { idConstantTest: "2", description: "Servidor de Correo" },
          { idConstantTest: "3", description: "Directorio de Documentos" },
          { idConstantTest: "4", description: "Procedimientos Almacenados" },
          { idConstantTest: "5", description: "Servicios SOA" }
        ]
      }
    },
    "->",
    {
      text: "AUDITORIA",
      action: "registerEventFileJson",
      iconCls: "auditory"
    }
  ],
  initComponent: function() {
    this.items = [
      {
        xtype: "ViewTreePanelRegisterFileJson",
        border: false
      }
    ];
    this.callParent();
  }
});
