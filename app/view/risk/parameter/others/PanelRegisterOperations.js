Ext.define('DukeSource.view.risk.parameter.others.PanelRegisterOperations', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.PanelRegisterOperations',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'Nuevo',
            iconCls: 'add',
            action: 'newOperation',
            itemId: 'newOperation'
        }, '-',
        {
            text: 'Modificar',
            iconCls: 'modify',
            action: 'modifyOperation',
            itemId: 'modifyOperation'
        }, '-',
        {
            text: 'Eliminar',
            iconCls: 'delete',
            action: 'deleteOperation',
            itemId: 'deleteOperation'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchOperation',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }
        , '->',
        {
            text: 'Auditoria',
            action: 'productOperation',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        var me = this;
        this.items = [
            {
                xtype: 'TreeGridOperations',
                border: false,
                listeners: {
                    select: function (a, record) {

                        if (record.data['depth'] === 1) {
                            me.down('#modifyOperation').setDisabled(true);
                            me.down('#deleteOperation').setDisabled(true);
                        } else {
                            me.down('#modifyOperation').setDisabled(false);
                            me.down('#deleteOperation').setDisabled(false);
                        }
                    }
                }
            }];
        this.callParent();
    }
});
