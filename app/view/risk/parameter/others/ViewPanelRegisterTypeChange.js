Ext.define('DukeSource.view.risk.parameter.others.ViewPanelRegisterTypeChange', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterTypeChange',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newTypeChange'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteTypeChange'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchTypeChange',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'typeChangeAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterTypeChange', padding: '2 2 2 2'}];
        this.callParent();
    }
});
