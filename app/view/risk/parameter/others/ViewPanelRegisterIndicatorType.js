Ext.define('DukeSource.view.risk.parameter.others.ViewPanelRegisterIndicatorType', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterIndicatorType',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newIndicatorType'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteIndicatorType'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchIndicatorType',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'indicatorTypeAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterIndicatorType', padding: '2 2 2 2'}];
        this.callParent();
    }
});
