Ext.define('DukeSource.view.risk.parameter.others.ViewPanelRegisterCurrency', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterCurrency',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newCurrency'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteCurrency'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchCurrency',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'currencyAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterCurrency', padding: '2 2 2 2'}];
        this.callParent();
    }
});
