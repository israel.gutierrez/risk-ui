Ext.define('DukeSource.view.risk.parameter.others.ViewPanelRegisterActionPlan', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterActionPlan',
    border: false,
    layout: 'fit',
    tbar: [
        {
            xtype: 'UpperCaseTextField',
            action: 'searchActionPlan',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'actionPlanAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterActionPlan', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
