Ext.define('DukeSource.view.risk.parameter.others.ViewPanelRegisterGroupIncidents', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterGroupIncidents',
    layout: 'fit',
    border: false,
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newGroupIncidents'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteGroupIncidents'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchGroupIncidents',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-', '->',
        {
            text: 'AUDITORIA',
            //action: 'currencyGroupIncidents',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterGroupIncidents', padding: '2 2 2 2'}];
        this.callParent();
    }
});