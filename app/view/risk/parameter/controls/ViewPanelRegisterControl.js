Ext.define("DukeSource.view.risk.parameter.controls.ViewPanelRegisterControl", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelRegisterControl",
  border: false,
  layout: "border",
  tbar: [
    {
      text: "Nuevo",
      iconCls: "add",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      action: "newControl"
    },
    "-",
    {
      text: "Evaluación",
      iconCls: "evaluate",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      action: "evaluateControl"
    },
    "-",
    {
      text: "Modificar",
      iconCls: "modify",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      action: "modifyControl"
    },
    "-",
    {
      text: "Invalidar",
      iconCls: "delete",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      action: "deleteControl"
    },
    "-",
    {
      xtype: "UpperCaseTextField",
      action: "searchControl",
      fieldLabel: "Buscar",
      labelWidth: 60,
      width: 300
    },
    "-",
    "->",
    {
      xtype: "checkbox",
      fieldLabel: "Activos",
      labelWidth: 60,
      name: "state",
      itemId: "state",
      inputValue: "S",
      uncheckedValue: "N",
      checked: true,
      listeners: {
        change: function(c) {
          var view = Ext.ComponentQuery.query(
            "ViewGridPanelRegisterControl"
          )[0];
          var toolbar = c.up("toolbar");
          if (c.checked == true) {
            c.setFieldLabel("ACTIVOS");
            toolbar.query(".button, .UpperCaseTextField").forEach(function(c) {
              c.setDisabled(false);
            });
            DukeSource.global.DirtyView.searchPaginationGridNormal(
              "%",
              view,
              view.down("pagingtoolbar"),
              "http://localhost:9000/giro/findControl.htm",
              "c.description",
              "c.idControl"
            );
          } else {
            c.setFieldLabel("INACTIVOS");
            toolbar.query(".button, .UpperCaseTextField").forEach(function(c) {
              c.setDisabled(true);
            });
            DukeSource.global.DirtyView.searchPaginationGridNormal(
              "N",
              view,
              view.down("pagingtoolbar"),
              "http://localhost:9000/giro/showListControlActives.htm",
              "c.state",
              "c.idControl"
            );
          }
        }
      }
    },
    "-",
    {
      text: "Auditoría",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      action: "controlAuditory",
      iconCls: "auditory"
    }
  ],
  initComponent: function() {
    var me = this;
    this.items = [
      /*{
                xtype: 'panel',
                itemId: 'advancedSearch',
                name: 'advancedSearch',
                title: 'Búsqueda avanzada',
                bodyPadding: '5',
                region: 'west',
                layout: 'anchor',
                collapsible: true,
                collapsed: true,
                padding: '2',
                split: true,
                width: 300,
                items: [
                    {
                        xtype: 'form',
                        border: false,
                        items: [
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Código',
                                itemId: 'codeControl',
                                name: 'codeControl',
                                allowBlank: true,
                                anchor: '100%'
                            },
                            {
                                xtype: 'ViewComboProcessType',
                                anchor: '100%',
                                name: 'processType',
                                itemId: 'processType',
                                fieldLabel: 'Macroproceso',
                                allowBlank: true,
                                msgTarget: 'side',
                                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                listeners: {
                                    render: function (cbo) {
                                        cbo.getStore().load();
                                    },
                                    select: function (cbo) {
                                        me.down('ViewComboProcess').reset();
                                        me.down('ViewComboProcess').getStore().load({
                                            url: 'http://localhost:9000/giro/showListProcessActives.htm',
                                            params: {
                                                valueFind: cbo.getValue()
                                            }
                                        });
                                        me.down('ViewComboProcess').setDisabled(false);
                                    },
                                    specialkey: function (f, e) {
                                        DukeSource.global.DirtyView.focusEventEnterObligatory(f, e, me.down('ViewComboProcess'))
                                    }
                                }
                            },
                            {
                                xtype: 'ViewComboProcess',
                                anchor: '100%',
                                name: 'process',
                                itemId: 'process',
                                fieldLabel: 'Proceso',
                                listeners: {
                                    select: function (cbo) {
                                        me.down('ViewComboSubProcess').reset();
                                        me.down('ViewComboSubProcess').getStore().load({
                                            url: 'http://localhost:9000/giro/showListSubProcessActives.htm',
                                            params: {
                                                valueFind: cbo.getValue()
                                            }
                                        });
                                        me.down('ViewComboSubProcess').setDisabled(false);
                                    },
                                    specialkey: function (f, e) {
                                        DukeSource.global.DirtyView.focusEventEnterObligatory(f, e, me.down('ViewComboSubProcess'))

                                    }
                                }

                            },
                            {
                                xtype: 'ViewComboSubProcess',
                                name: 'subProcess',
                                itemId: 'subProcess',
                                anchor: '100%',
                                fieldLabel: 'Subproceso',
                                listeners: {
                                    specialkey: function (f, e) {
                                        DukeSource.global.DirtyView.focusEventEnterObligatory(f, e, me.down('button[itemId=searchUserControl]'))

                                    }
                                }
                            }
                        ],
                        buttons: [
                            {
                                xtype: 'button',
                                iconCls: 'search',
                                scale: 'medium',
                                text: 'Buscar',
                                action: 'searchControlAdvanced',
                                anchor: '100%'
                            },
                            {
                                xtype: 'button',
                                iconCls: 'clear',
                                scale: 'medium',
                                text: 'Limpiar',
                                anchor: '100%',
                                handler: function (btn) {
                                    btn.up('form').getForm().reset();
                                }
                            }
                        ]
                    }
                ]
            },*/
      {
        xtype: "ViewGridPanelRegisterControl",
        region: "center",
        padding: "2 0 2 2"
      }
    ];
    this.callParent();
  }
});
