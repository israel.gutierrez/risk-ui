Ext.define('DukeSource.view.risk.parameter.controls.AddMenuControlTypeGroup', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.AddMenuControlTypeGroup',
    width: 120,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'menuitem',
                    text: 'Agregar',
                    iconCls: 'add'
                },
                {
                    xtype: 'menuitem',
                    text: 'Editar',
                    iconCls: 'modify'
                },
                {
                    xtype: 'menuitem',
                    text: 'Eliminar',
                    iconCls: 'delete'
                }
            ]
        });

        me.callParent(arguments);
    }
});