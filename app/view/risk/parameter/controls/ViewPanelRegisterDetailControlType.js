Ext.define('DukeSource.view.risk.parameter.controls.ViewPanelRegisterDetailControlType', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterDetailControlType',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newDetailControlType'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteDetailControlType'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDetailControlType',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'detailControlTypeAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterDetailControlType', padding: '2 2 2 2'}];
        this.callParent();
    }
});
