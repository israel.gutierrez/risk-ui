Ext.define('DukeSource.view.risk.parameter.controls.ViewPanelRegisterWeakness', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterWeakness',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            scale: 'medium',
            cls: 'my-btn',
            overCls: 'my-over',
            action: 'newWeakness'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            scale: 'medium',
            cls: 'my-btn',
            overCls: 'my-over',
            action: 'deleteWeakness'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchWeakness',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            scale: 'medium',
            cls: 'my-btn',
            overCls: 'my-over',
            action: 'weaknessAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterWeakness', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
