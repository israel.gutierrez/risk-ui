Ext.define(
  "DukeSource.view.risk.parameter.controls.ViewWindowRegisterControl",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowRegisterControl",
    requires: [
      "DukeSource.view.risk.parameter.combos.ViewComboForeignKey",
      "DukeSource.view.risk.parameter.combos.ViewComboProcessType",
      "DukeSource.view.risk.parameter.combos.ViewComboProcess",
      "DukeSource.view.risk.parameter.combos.ViewComboSubProcess",
      "DukeSource.view.risk.parameter.combos.ViewComboJobPlace"
    ],
    width: 550,
    layout: {
      type: "fit"
    },
    title: getName("TitleRegisterControl"),
    border: false,
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            title: "",
            items: [
              {
                xtype: "textfield",
                name: "idControl",
                hidden: true,
                value: "id"
              },
              {
                xtype: "textfield",
                name: "stateEvaluated",
                hidden: true,
                value: "S"
              },
              {
                xtype: "UpperCaseTextFieldReadOnly",
                anchor: "100%",
                labelWidth: 130,
                name: "codeControl",
                itemId: "codeControl",
                fieldLabel: "Código"
              },
              {
                xtype: "combobox",
                anchor: "100%",
                editable: false,
                fieldLabel: getName("WRCTypeControl"),
                hidden: hidden("WRCTypeControl"),
                labelWidth: 130,
                allowBlank: true,
                name: "typeControl",
                itemId: "typeControl",
                queryMode: "local",
                displayField: "description",
                valueField: "value",
                value: "C",
                store: {
                  fields: ["value", "description"],
                  pageSize: 9999,
                  autoLoad: true,
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url:
                      "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                    extraParams: {
                      propertyOrder: "description",
                      propertyFind: "identified",
                      valueFind: "TYPECONTROL"
                    },
                    reader: {
                      type: "json",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                },
                listeners: {
                  select: function(cbo) {
                    if (cbo.getValue() === "S") {
                      me.down("#jobPlace").allowBlank = true;
                      me.down("#jobPlace").setVisible(false);
                    } else {
                      me.down("#jobPlace").allowBlank = false;
                      me.down("#jobPlace").setVisible(true);
                    }
                  }
                }
              },
              {
                xtype: "ViewComboProcessType",
                anchor: "100%",
                labelWidth: 130,
                name: "typeProcess",
                itemId: "typeProcess",
                msgTarget: "side",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                fieldLabel: "Macroproceso",
                hidden: hidden("WRCProcessType"),
                listeners: {
                  render: function(cbo) {
                    cbo.getStore().load();
                  },
                  select: function(cbo) {
                    me.down("ViewComboProcess").reset();
                    me.down("ViewComboProcess")
                      .getStore()
                      .load({
                        url:
                          "http://localhost:9000/giro/showListProcessActives.htm",
                        params: {
                          valueFind: cbo.getValue()
                        }
                      });
                    me.down("ViewComboProcess").setDisabled(false);
                    me.down("ViewComboSubProcess").setDisabled(true);
                    me.down("ViewComboSubProcess").reset();
                  },
                  specialkey: function(f, e) {
                    DukeSource.global.DirtyView.focusEventEnterObligatory(
                      f,
                      e,
                      me.down("ViewComboProcess")
                    );
                  }
                }
              },
              {
                xtype: "ViewComboProcess",
                anchor: "100%",
                labelWidth: 130,
                name: "idProcess",
                disabled: true,
                hidden: hidden("WRCProcess"),
                msgTarget: "side",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                fieldLabel: "Proceso",
                listeners: {
                  select: function(cbo) {
                    me.down("ViewComboSubProcess").reset();
                    me.down("ViewComboSubProcess")
                      .getStore()
                      .load({
                        url:
                          "http://localhost:9000/giro/showListSubProcessActives.htm",
                        params: {
                          valueFind: cbo.getValue()
                        }
                      });
                    me.down("ViewComboSubProcess").setDisabled(false);
                  },
                  specialkey: function(f, e) {
                    DukeSource.global.DirtyView.focusEventEnterObligatory(
                      f,
                      e,
                      me.down("ViewComboSubProcess")
                    );
                  }
                }
              },
              {
                xtype: "ViewComboSubProcess",
                anchor: "100%",
                labelWidth: 130,
                disabled: true,
                allowBlank: true,
                msgTarget: "side",
                hidden: hidden("WRCSubProcess"),
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                name: "idSubProcess",
                fieldLabel: "Sub-Proceso",
                listeners: {
                  specialkey: function(f, e) {
                    DukeSource.global.DirtyView.focusEventEnterObligatory(
                      f,
                      e,
                      me.down("ViewComboJobPlace")
                    );
                  }
                }
              },
              {
                xtype: "ViewComboJobPlace",
                name: "jobPlace",
                itemId: "jobPlace",
                anchor: "100%",
                labelWidth: 130,
                fieldLabel: getName("WRCJobPlace"),
                fieldCls: "obligatoryTextField",
                allowBlank: false,
                hidden: false,
                queryMode: "remote",
                plugins: ["ComboSelectCount"],
                msgTarget: "side",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                listeners: {
                  specialkey: function(f, e) {
                    DukeSource.global.DirtyView.focusEventEnterObligatory(
                      f,
                      e,
                      me.down("UpperCaseTextArea[name=description]")
                    );
                  }
                }
              },
              {
                xtype: "UpperCaseTextArea",
                anchor: "100%",
                labelWidth: 130,
                height: 70,
                itemId: "description",
                name: "description",
                fieldLabel: getName("WRCDescription"),
                fieldCls: "obligatoryTextField",
                allowBlank: false,
                msgTarget: "side",
                maxLength: 2000,
                listeners: {
                  specialkey: function(f, e) {
                    DukeSource.global.DirtyView.focusEventEnterObligatory(
                      f,
                      e,
                      me.down("button[itemId=saveWindowControl]")
                    );
                  }
                }
              },
              {
                xtype: "UpperCaseTextArea",
                anchor: "100%",
                labelWidth: 130,
                height: 45,
                maxLength: 2000,
                hidden: hidden("WRCInternalDescription"),
                fieldLabel: getName("WRCInternalDescription"),
                name: "internalDescription",
                enforceMaxLength: true
              },
              {
                xtype: "UpperCaseTextArea",
                anchor: "100%",
                labelWidth: 130,
                height: 45,
                maxLength: 2000,
                hidden: hidden("WRCExternalDescription"),
                fieldLabel: getName("WRCExternalDescription"),
                name: "externalDescription",
                enforceMaxLength: true
              }
            ]
          }
        ],
        buttonAlign: "center",
        buttons: [
          {
            text: "Guardar",
            scale: "medium",
            itemId: "saveWindowControl",
            handler: function() {
              if (
                me
                  .down("form")
                  .getForm()
                  .isValid()
              ) {
                Ext.Ajax.request({
                  method: "POST",
                  url:
                    "http://localhost:9000/giro/saveControl.htm?nameView=ViewPanelRegisterControl",
                  params: {
                    jsonData: Ext.JSON.encode(
                      me
                        .down("form")
                        .getForm()
                        .getValues()
                    )
                  },
                  success: function(response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                      me.close();
                      var grid = Ext.ComponentQuery.query(
                        "ViewPanelRegisterControl grid"
                      )[0];
                      var grid1 = Ext.ComponentQuery.query(
                        "ViewWindowSearchControl grid"
                      )[0];
                      if (grid1 !== undefined) {
                        DukeSource.global.DirtyView.searchPaginationGridNormal(
                          "%",
                          grid1,
                          grid1.down("pagingtoolbar"),
                          "http://localhost:9000/giro/findControl.htm",
                          "c.description",
                          "c.idControl"
                        );
                      } else {
                        DukeSource.global.DirtyView.searchPaginationGridNormal(
                          "%",
                          grid,
                          grid.down("pagingtoolbar"),
                          "http://localhost:9000/giro/findControl.htm",
                          "c.description",
                          "c.idControl"
                        );
                      }
                      DukeSource.global.DirtyView.messageNormal(
                        response.message
                      );
                    } else {
                      DukeSource.global.DirtyView.messageWarning(
                        response.message
                      );
                    }
                  },
                  failure: function() {}
                });
              } else {
                DukeSource.global.DirtyView.messageWarning(
                  DukeSource.global.GiroMessages.MESSAGE_COMPLETE
                );
              }
            },
            iconCls: "save"
          },
          {
            text: "Salir",
            scale: "medium",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
