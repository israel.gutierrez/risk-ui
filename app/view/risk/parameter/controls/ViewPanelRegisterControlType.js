Ext.define('DukeSource.view.risk.parameter.controls.ViewPanelRegisterControlType', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterControlType',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newControlType'
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'controlTypeAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewTreeGridPanelRegisterControlType', padding: '2 2 2 2'}];
        this.callParent();
    }
});
