Ext.define(
  "DukeSource.view.risk.parameter.controls.ViewPanelRegisterScoreControl",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelRegisterScoreControl",
    border: false,
    layout: "fit",
    tbar: [
      {
        text: "Nuevo",
        iconCls: "add",
        action: "newScoreControl"
      },
      "-",
      {
        text: "Eliminar",
        iconCls: "delete",
        action: "deleteScoreControl"
      },
      "-",
      {
        xtype: "combobox",
        anchor: "100%",
        editable: false,
        fieldLabel: getName("WRCTypeControlMitigation"),
        hidden: hidden("WRCTypeControlMitigation"),
        labelWidth: 130,
        allowBlank: true,
        name: "typeControl",
        itemId: "typeControl",
        queryMode: "local",
        displayField: "description",
        valueField: "value",
        value: "C",
        store: {
          fields: ["value", "description"],
          pageSize: 9999,
          autoLoad: true,
          proxy: {
            actionMethods: {
              create: "POST",
              read: "POST",
              update: "POST"
            },
            type: "ajax",
            url:
              "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
            extraParams: {
              propertyOrder: "description",
              propertyFind: "identified",
              valueFind: "TYPECONTROL"
            },
            reader: {
              type: "json",
              root: "data",
              successProperty: "success"
            }
          }
        },
        listeners: {
          select: function(cbo) {
            var win = Ext.ComponentQuery.query(
              "ViewGridPanelRegisterScoreControl"
            )[0];
            DukeSource.global.DirtyView.searchPaginationGridNormal(
              cbo.getValue(),
              win,
              win.down("pagingtoolbar"),
              "http://localhost:9000/giro/findScoreControl.htm",
              "typeControl",
              "scoreControl"
            );
          }
        }
      },
      "-",
      "->",
      {
        text: "AUDITORIA",
        action: "scoreControlAuditory",
        iconCls: "auditory"
      }
    ],
    initComponent: function() {
      this.items = [
        {
          xtype: "ViewGridPanelRegisterScoreControl",
          padding: 2
        }
      ];
      this.callParent();
    }
  }
);
