Ext.define("DukeSource.view.risk.util.search.BasicSearchEvent", {
  extend: "Ext.form.Panel",
  alias: "widget.BasicSearchEvent",
  title: "Búsqueda de eventos",
  titleAlign: "center",
  padding: "0 0 0 2",
  region: "east",
  collapseDirection: "left",
  collapsed: true,
  collapsible: true,
  split: true,
  requires: ["Ext.ux.DateTimeField"],
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "panel",
          flex: 10,
          name: "panelReport",
          region: "center",
          border: false,
          padding: "0 0 0 0",
          titleAlign: "center"
        },
        {
          xtype: "form",
          border: false,
          bodyPadding: 10,
          fieldDefaults: {
            labelCls: "changeSizeFontToEightPt",
            fieldCls: "changeSizeFontToEightPt"
          },
          items: [
            {
              xtype: "textfield",
              fieldLabel: "Código del evento",
              name: "codeEvent",
              itemId: "codeEvent",
              emptyText: "E-1",
              fieldStyle: "text-transform:uppercase",
              width: 205,
              allowBlank: true
            },
            {
              xtype: "numberfield",
              width: 205,
              name: "amount",
              itemId: "amount",
              decimalPrecision: 2,
              fieldLabel: "Monto igual a"
            },
            {
              xtype: "datetimefield",
              fieldLabel: "Fecha reporte de",
              name: "dateRegisterInit",
              allowBlank: true,
              format: "d/m/Y H:i",
              flex: 1,
              itemId: "dateRegisterInit"
            },
            {
              xtype: "datetimefield",
              labelAlign: "right",
              fieldLabel: "Fecha reporte al",
              name: "dateRegisterEnd",
              format: "d/m/Y H:i",
              allowBlank: true,
              itemId: "dateRegisterEnd"
            }
          ]
        }
      ]
    });
    me.callParent(arguments);
  }
});
