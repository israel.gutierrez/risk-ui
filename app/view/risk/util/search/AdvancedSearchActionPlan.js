var statesActionPlan = Ext.create('Ext.data.Store', {
    fields: ['id', 'name'],
    data: [
        {
            "id": "PR",
            "name": "En proceso"
        },
        {
            "id": "CU",
            "name": "Culminados"
        },
        {
            "id": "IN",
            "name": "Invalidos"
        },
        {
            "id": "%",
            "name": "Todos"
        }
    ]
});

Ext.define('DukeSource.view.risk.util.search.AdvancedSearchActionPlan', {
    extend: 'Ext.form.Panel',
    alias: 'widget.AdvancedSearchActionPlan',
    requires: [
        'DukeSource.view.fulfillment.combos.ViewComboTypeDocument'
    ],
    title: 'Búsqueda avanzada',
    titleAlign: 'center',
    padding: 2,
    region: 'east',
    itemId: 'AdvancedSearchActionPlan',
    collapseDirection: 'left',
    collapsed: true,
    collapsible: true,
    split: true,
    listeners: {
        expand: function (view) {
            view.down('#codePlan').focus(false, 100);
        }
    },
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    border: false,
                    items: [
                        {
                            xtype:"UpperCaseTextField",
                            fieldLabel: 'Código',
                            name: 'codePlan',
                            itemId: 'codePlan',
                            flex: 1
                        },
                        {
                            xtype: 'combobox',
                            itemId: 'statePlan',
                            fieldLabel: 'Estado',
                            value: '%',
                            flex: 1,
                            store: statesActionPlan,
                            queryMode: 'local',
                            displayField: 'name',
                            valueField: 'id'
                        },
                        {
                            xtype: 'ViewComboTypeDocument',
                            itemId: 'validityPlan',
                            fieldLabel: 'Vigencia'
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            height: 26,
                            items: [
                                {
                                    xtype: 'textfield',
                                    itemId: 'idUserEmitted',
                                    name: 'idUserEmitted',
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    itemId: 'fullNameEmitted',
                                    fieldLabel: 'Remitente',
                                    labelWidth: 100
                                },
                                {
                                    xtype: 'button',
                                    width: 30,
                                    itemId: 'searchEmitted',
                                    iconCls: 'search',
                                    handler: function () {
                                        var windows = Ext.create('DukeSource.view.risk.util.search.SearchUser', {}).show();
                                        windows.down('grid').on('itemdblclick', function (view, record) {
                                            me.down('#idUserEmitted').setValue(record.get('userName'));
                                            me.down('#fullNameEmitted').setValue(record.get('fullName'));
                                            windows.close();
                                        });
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            height: 26,
                            items: [
                                {
                                    xtype: 'textfield',
                                    itemId: 'idUserReceptor',
                                    name: 'idUserReceptor',
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    flex: 1,
                                    itemId: 'fullNameReceptor',
                                    fieldLabel: 'Responsable',
                                    labelWidth: 100
                                },
                                {
                                    xtype: 'button',
                                    width: 30,
                                    itemId: 'searchRisk',
                                    iconCls: 'search',
                                    handler: function () {
                                        var windows = Ext.create('DukeSource.view.risk.util.search.SearchUser', {}).show();
                                        windows.down('grid').on('itemdblclick', function (view, record) {
                                            me.down('#idUserReceptor').setValue(record.get('userName'));
                                            me.down('#fullNameReceptor').setValue(record.get('fullName'));
                                            windows.close();
                                        });
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            height: 26,
                            items: [
                                {
                                    xtype: 'datefield',
                                    flex: 1.7,
                                    itemId: 'dateInitFirst',
                                    format: 'Y-m-d',
                                    fieldLabel: 'Fecha inicio de'
                                },
                                {
                                    xtype: 'datefield',
                                    flex: 1,
                                    padding: '0 0 0 3',
                                    labelSeparator: '',
                                    itemId: 'dateInitSecond',
                                    format: 'Y-m-d',
                                    fieldLabel: 'a',
                                    labelAlign: 'right',
                                    labelWidth: 5
                                }
                            ]
                        }
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
});
