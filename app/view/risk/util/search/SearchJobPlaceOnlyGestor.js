Ext.define("ModelJobPlaceGestorAll", {
  extend: "Ext.data.Model",
  fields: ["idJobPlace", "workArea", "description", "category"]
});

var storeJobPlaceGestorAll = Ext.create("Ext.data.Store", {
  model: "ModelJobPlaceGestorAll",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListJobPlaceByCategory.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.util.search.SearchJobPlaceOnlyGestor", {
  extend: "Ext.window.Window",
  alias: "widget.SearchJobPlaceOnlyGestor",
  layout: {
    align: "stretch",
    type: "vbox"
  },
  anchorSize: 100,
  title: "ASIGNAR CARGO",
  titleAlign: "center",
  width: 500,
  height: 400,
  initComponent: function() {
    var me = this;
    var category = this.category;
    var workArea = this.workArea;
    var url = this.url;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          height: 45,
          padding: "2 2 2 2",
          bodyPadding: 10,
          items: [
            {
              xtype: "textfield",
              itemId: "id",
              name: "id",
              hidden: true
            },
            {
              xtype:"UpperCaseTrigger",
              action: "searchTriggerJobPlace",
              anchor: "100%",
              fieldLabel: "BUSCAR CARGO"
            },
            {
              xtype: "textfield",
              name: "ocultar",
              hidden: true,
              fieldLabel: "ROLE",
              hideLabel: true
            }
          ]
        },
        {
          xtype: "gridpanel",
          itemId: "availableJobPlace",
          padding: "0 1 1 1",
          store: storeJobPlaceGestorAll,
          loadMask: true,
          columnLines: true,
          flex: 1,
          title: "DISPONIBLES",
          titleAlign: "center",
          multiSelect: true,
          columns: [
            {
              xtype: "rownumberer",
              width: 40,
              sortable: false
            },
            {
              dataIndex: "description",
              flex: 1,
              text: "ROL"
            }
          ],
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: storeJobPlaceGestorAll,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          },
          listeners: {
            render: function() {
              var me = this;
              me.store.getProxy().extraParams = {
                category: category,
                workArea: workArea
              };
              me.store.getProxy().url = url;
              me.down("pagingtoolbar").moveFirst();
            }
          }
        }
      ],
      buttons: [
        {
          text: "GUARDAR",
          iconCls: "save",
          action: "assignJobPlace"
        },
        {
          text: "SALIR",
          iconCls: "logout",
          scope: this,
          handler: this.close
        }
      ]
    });

    me.callParent(arguments);
  }
});
