Ext.define(
  "DukeSource.view.risk.util.search.AdvancedSearchIdentificationRisk",
  {
    extend: "Ext.form.Panel",
    alias: "widget.AdvancedSearchIdentificationRisk",
    title: "BUSQUEDA AVANZADA",
    titleAlign: "center",
    region: "east",
    collapseDirection: "left",
    collapsed: true,
    collapsible: true,
    split: true,
    height: 200,
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            border: false,
            bodyPadding: 5,
            fieldDefaults: {
              labelCls: "changeSizeFontToEightPt",
              fieldCls: "changeSizeFontToEightPt"
            },
            items: [
              {
                xtype: "UpperCaseTextFieldObligatory",
                fieldLabel: "CODIGO USUARIO",
                itemId: "idUser",
                name: "idUser",
                anchor: "100%",
                allowBlank: true
              },
              {
                xtype: "UpperCaseTextFieldObligatory",
                fieldLabel: "NOMBRE: ",
                itemId: "nameUser",
                name: "nameUser",
                anchor: "100%",
                allowBlank: true
              },
              {
                xtype: "datefield",
                fieldLabel: "FECHA",
                itemId: "dateEvaluationInit",
                name: "dateEvaluationInit",
                format: "d/m/Y",
                anchor: "100%",
                allowBlank: true
              },
              {
                xtype: "combobox",
                fieldLabel: "ESTADO",
                editable: false,
                name: "stateIdentification",
                itemId: "stateIdentification",
                anchor: "100%",
                queryMode: "local",
                displayField: "description",
                valueField: "id",
                value: "P",
                store: {
                  fields: ["id", "description"],
                  data: [
                    { id: "P", description: "PENDIENTE" },
                    { id: "R", description: "REVISADO" },
                    { id: "%", description: "TODOS" }
                  ]
                }
              }
            ]
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
