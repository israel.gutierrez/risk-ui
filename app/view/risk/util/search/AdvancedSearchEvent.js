Ext.define("DukeSource.view.risk.util.search.AdvancedSearchEvent", {
  extend: "Ext.form.Panel",
  alias: "widget.AdvancedSearchEvent",
  title: "Búsqueda avanzada",
  titleAlign: "center",
  padding: "0 0 0 2",
  region: "east",
  collapseDirection: "left",
  collapsed: true,
  collapsible: true,
  split: true,
  requires: ["Ext.ux.DateTimeField"],
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "panel",
          flex: 10,
          name: "panelReport",
          itemId: "panelReport",
          region: "center",
          border: false,
          padding: 0,
          titleAlign: "center"
        },
        {
          xtype: "form",
          border: false,
          bodyPadding: 4,
          fieldDefaults: {
            labelCls: "changeSizeFontToEightPt",
            fieldCls: "changeSizeFontToEightPt"
          },
          items: [
            {
              xtype: "textfield",
              fieldLabel: "Código del evento",
              name: "codeEvent",
              itemId: "codeEvent",
              emptyText: "E-1",
              fieldStyle: "text-transform:uppercase",
              width: 205,
              allowBlank: true
            },
            {
              xtype: "textfield",
              fieldLabel: "Código migración",
              itemId: "codeMigration",
              name: "codeMigration",
              allowBlank: true,
              width: 205
            },
            {
              xtype: "numberfield",
              width: 205,
              name: "amount",
              itemId: "amount",
              fieldLabel: "Monto igual a"
            },
            {
              xtype: "combobox",
              width: 205,
              fieldLabel: "Estado del evento",
              editable: false,
              name: "eventState",
              itemId: "eventState",
              displayField: "description",
              valueField: "id",
              store: {
                fields: ["id", "description", "sequence"],
                proxy: {
                  actionMethods: {
                    create: "POST",
                    read: "POST",
                    update: "POST"
                  },
                  type: "ajax",
                  url: "http://localhost:9000/giro/findStateIncident.htm",
                  extraParams: {
                    propertyFind: "si.typeIncident",
                    valueFind: DukeSource.global.GiroConstants.EVENT,
                    propertyOrder: "si.sequence"
                  },
                  reader: {
                    type: "json",
                    root: "data",
                    successProperty: "success"
                  }
                }
              },
              listeners: {
                specialkey: function(f, e) {
                  DukeSource.global.DirtyView.focusEventEnter(f, e, me.down("#riskType"));
                }
              }
            },
            {
              xtype: "datetimefield",
              fieldLabel: "Fecha reporte de",
              name: "dateRegisterInit",
              allowBlank: true,
              format: "d/m/Y H:i",
              flex: 1,
              itemId: "dateRegisterInit"
            },
            {
              xtype: "datetimefield",
              labelAlign: "right",
              fieldLabel: "Fecha reporte al",
              name: "dateRegisterEnd",
              format: "d/m/Y H:i",
              allowBlank: true,
              itemId: "dateRegisterEnd"
            },
            {
              xtype: "container",
              layout: {
                type: "hbox"
              },
              height: 26,
              items: [
                {
                  xtype: "textfield",
                  name: "idUser",
                  itemId: "idUser",
                  hidden: true
                },
                {
                  xtype:"UpperCaseTextFieldReadOnly",
                  fieldLabel: "Usuario registró",
                  name: "fullNameUser",
                  allowBlank: true,
                  flex: 1,
                  itemId: "fullNameUser"
                },
                {
                  xtype: "button",
                  width: 26,
                  iconCls: "search",
                  itemId: "searchUser",
                  handler: function(button) {
                    var panel = button.up("form");
                    var windows = Ext.create(
                      "DukeSource.view.risk.util.search.SearchUser",
                      {
                        modal: true
                      }
                    ).show();
                    windows.down("grid").on("itemdblclick", function() {
                      panel.down("#idUser").setValue(
                        windows
                          .down("grid")
                          .getSelectionModel()
                          .getSelection()[0]
                          .get("userName")
                      );
                      panel.down("#fullNameUser").setValue(
                        windows
                          .down("grid")
                          .getSelectionModel()
                          .getSelection()[0]
                          .get("fullName")
                      );
                      windows.close();
                    });
                  }
                }
              ]
            },
            {
              xtype: "combobox",
              fieldLabel: "Área",
              labelAlign: "left",
              queryMode: "remote",
              emptyText: "Seleccionar",
              editable: false,
              anchor: "100%",
              name: "workArea",
              itemId: "workArea",
              displayField: "description",
              valueField: "idWorkArea",
              store: {
                fields: ["idWorkArea", "description"],
                proxy: {
                  actionMethods: {
                    create: "POST",
                    read: "POST",
                    update: "POST"
                  },
                  type: "ajax",
                  url:
                    "http://localhost:9000/giro/showListWorkAreaActivesComboBox.htm",
                  extraParams: {
                    propertyOrder: "description",
                    start: 0,
                    limit: 9999
                  },
                  reader: {
                    type: "json",
                    root: "data",
                    successProperty: "success"
                  }
                }
              }
            },
            {
              xtype: "combobox",
              fieldLabel: "Agencia",
              plugins: ["ComboSelectCount"],
              labelAlign: "left",
              emptyText: "Seleccionar",
              queryMode: "remote",
              forceSelection: false,
              editable: true,
              anchor: "100%",
              name: "agency",
              itemId: "agency",
              displayField: "description",
              valueField: "idAgency",
              store: {
                fields: ["idAgency", "description"],
                proxy: {
                  actionMethods: {
                    create: "POST",
                    read: "POST",
                    update: "POST"
                  },
                  type: "ajax",
                  url:
                    "http://localhost:9000/giro/showListAgencyActivesComboBox.htm",
                  extraParams: {
                    propertyOrder: "description"
                  },
                  reader: {
                    type: "json",
                    root: "data",
                    successProperty: "success"
                  }
                }
              }
            }
          ]
        }
      ],
      listeners: {
        expand: function() {
          me.down("#codeEvent").focus(false, 200);
        }
      }
    });
    me.callParent(arguments);
  }
});
