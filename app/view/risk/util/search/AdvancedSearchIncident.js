Ext.define("DukeSource.view.risk.util.search.AdvancedSearchIncident", {
  extend: "Ext.form.Panel",
  alias: "widget.AdvancedSearchIncident",
  title: "Busqueda avanzada",
  titleAlign: "center",
  padding: 2,
  collapseDirection: "left",
  collapsed: false,
  border: true,
  collapsible: true,
  split: true,
  flex: 0.45,
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          border: false,
          layout: "anchor",
          padding: 5,
          items: [
            {
              xtype: "numberfield",
              fieldLabel: "Codigo",
              itemId: "codeIncident",
              name: "codeIncident",
              allowBlank: true,
              anchor: "100%"
            },
            {
              xtype: "container",
              layout: {
                type: "hbox",
                align: "stretch"
              },
              margin: "5 0 5 0",
              items: [
                {
                  xtype: "datefield",
                  fieldLabel: "F.reporte",
                  name: "dateRegisterInit",
                  format: "Y-m-d",
                  flex: 1,
                  itemId: "dateRegisterInit"
                },
                {
                  xtype: "datefield",
                  fieldLabel: "al",
                  labelAlign: "right",
                  format: "Y-m-d",
                  name: "dateRegisterEnd",
                  labelWidth: 20,
                  flex: 0.6,
                  itemId: "dateRegisterEnd"
                }
              ]
            },
            {
              xtype: "container",
              layout: {
                type: "hbox",
                align: "stretch"
              },
              margin: "5 0 5 0",
              items: [
                {
                  xtype: "datefield",
                  fieldLabel: "F.ocurrencia",
                  format: "Y-m-d",
                  name: "dateOccurrenceInit",
                  flex: 1,
                  itemId: "dateOccurrenceInit"
                },
                {
                  xtype: "datefield",
                  fieldLabel: "al",
                  format: "Y-m-d",
                  labelAlign: "right",
                  name: "dateOccurrenceEnd",
                  labelWidth: 20,
                  flex: 0.6,
                  itemId: "dateOccurrenceEnd"
                }
              ]
            },
            {
              xtype: "combobox",
              anchor: "100%",
              name: "processOrigin",
              itemId: "processOrigin",
              fieldLabel: "Proc. origen",
              displayField: "description",
              valueField: "idProcess",
              editable: false,
              plugins: ["ComboSelectCount"],
              store: {
                fields: ["idProcess", "description"],
                proxy: {
                  actionMethods: {
                    create: "POST",
                    read: "POST",
                    update: "POST"
                  },
                  type: "ajax",
                  url:
                    "http://localhost:9000/giro/showListProcessActivesComboBox.htm",
                  extraParams: {
                    propertyOrder: "idProcess"
                  },
                  reader: {
                    type: "json",
                    root: "data",
                    successProperty: "success"
                  }
                }
              }
            },
            {
              xtype: "combobox",
              anchor: "100%",
              name: "processImpact",
              itemId: "processImpact",
              displayField: "description",
              valueField: "idProcess",
              editable: false,
              plugins: ["ComboSelectCount"],
              fieldLabel: "Proc. impacto",
              store: {
                fields: ["idProcess", "description"],
                proxy: {
                  actionMethods: {
                    create: "POST",
                    read: "POST",
                    update: "POST"
                  },
                  type: "ajax",
                  url:
                    "http://localhost:9000/giro/showListProcessActivesComboBox.htm",
                  extraParams: {
                    propertyOrder: "idProcess"
                  },
                  reader: {
                    type: "json",
                    root: "data",
                    successProperty: "success"
                  }
                }
              }
            },
            {
              xtype: "container",
              layout: {
                type: "hbox",
                align: "stretch"
              },
              margin: "5 0 5 0",
              items: [
                {
                  xtype: "textfield",
                  name: "idUser",
                  itemId: "idUser",
                  hidden: true
                },
                {
                  xtype:"UpperCaseTextFieldReadOnly",
                  fieldLabel: "Usuario",
                  name: "fullNameUser",
                  flex: 1,
                  itemId: "fullNameUser"
                },
                {
                  xtype: "button",
                  width: 26,
                  iconCls: "search",
                  itemId: "searchUser",
                  handler: function(button) {
                    var panel = button.up("form");
                    var windows = Ext.create(
                      "DukeSource.view.risk.util.search.SearchUser",
                      {
                        modal: true
                      }
                    ).show();
                    windows.down("grid").on("itemdblclick", function() {
                      panel.down("#idUser").setValue(
                        windows
                          .down("grid")
                          .getSelectionModel()
                          .getSelection()[0]
                          .get("userName")
                      );
                      panel.down("#fullNameUser").setValue(
                        windows
                          .down("grid")
                          .getSelectionModel()
                          .getSelection()[0]
                          .get("fullName")
                      );
                      windows.close();
                    });
                  }
                }
              ]
            },
            {
              xtype: "combobox",
              labelAlign: "left",
              fieldLabel: "Estado",
              anchor: "100%",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              name: "si.id",
              displayField: "description",
              valueField: "id",
              itemId: "stateIncident",
              listeners: {
                expand: function(cbo) {
                  cbo.getStore().load({
                    callback: function() {
                      cbo.store.add({
                        value: "T",
                        description: "TODOS"
                      });
                    }
                  });
                }
              },
              store: {
                fields: ["id", "description", "sequence"],
                proxy: {
                  actionMethods: {
                    create: "POST",
                    read: "POST",
                    update: "POST"
                  },
                  type: "ajax",
                  url: "http://localhost:9000/giro/findStateIncident.htm",
                  extraParams: {
                    propertyFind: "si.typeIncident",
                    valueFind: DukeSource.global.GiroConstants.OPERATIONAL,
                    propertyOrder: "si.sequence"
                  },
                  reader: {
                    type: "json",
                    root: "data",
                    successProperty: "success"
                  }
                }
              }
            },
            {
              xtype: "combobox",
              fieldLabel: "Area",
              labelAlign: "left",
              queryMode: "remote",
              emptyText: "Seleccionar",
              forceSelection: false,
              editable: false,
              anchor: "100%",
              name: "workArea",
              itemId: "workArea",
              displayField: "description",
              valueField: "idWorkArea",
              store: {
                fields: ["idWorkArea", "description"],
                proxy: {
                  actionMethods: {
                    create: "POST",
                    read: "POST",
                    update: "POST"
                  },
                  type: "ajax",
                  url:
                    "http://localhost:9000/giro/showListWorkAreaActivesComboBox.htm",
                  extraParams: {
                    propertyOrder: "description",
                    start: 0,
                    limit: 9999
                  },
                  reader: {
                    type: "json",
                    root: "data",
                    successProperty: "success"
                  }
                }
              }
            },
            {
              xtype: "combobox",
              fieldLabel: "Agencia",
              plugins: ["ComboSelectCount"],
              labelAlign: "left",
              emptyText: "Seleccionar",
              queryMode: "remote",
              forceSelection: false,
              editable: true,
              anchor: "100%",
              name: "agency",
              itemId: "agency",
              displayField: "description",
              valueField: "idAgency",
              store: {
                fields: ["idAgency", "description"],
                proxy: {
                  actionMethods: {
                    create: "POST",
                    read: "POST",
                    update: "POST"
                  },
                  type: "ajax",
                  url:
                    "http://localhost:9000/giro/showListAgencyActivesComboBox.htm",
                  extraParams: {
                    propertyOrder: "a.description"
                  },
                  reader: {
                    type: "json",
                    root: "data",
                    successProperty: "success"
                  }
                }
              }
            }
          ]
        }
      ]
    });
    me.callParent(arguments);
  }
});
