Ext.define("DukeSource.view.risk.util.search.AdvancedSearchUser", {
  extend: "Ext.form.Panel",
  alias: "widget.AdvancedSearchUser",
  title: "Búsqueda avanzada",
  titleAlign: "center",
  region: "east",
  collapseDirection: "left",
  collapsed: true,
  collapsible: true,
  split: true,
  height: 200,
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          border: false,
          bodyPadding: 5,
          fieldDefaults: {
            labelCls: "changeSizeFontToEightPt",
            fieldCls: "changeSizeFontToEightPt"
          },
          items: [
            {
              xtype:"UpperCaseTextField",
              fieldLabel: "Codigo",
              itemId: "idUser",
              name: "idUser",
              anchor: "100%",
              allowBlank: true
            },
            {
              xtype:"UpperCaseTextField",
              fieldLabel: "Documento",
              itemId: "doiUser",
              anchor: "100%",
              name: "doiUser",
              allowBlank: true
            },
            {
              xtype:"UpperCaseTextField",
              fieldLabel: "Nombre",
              itemId: "nameUser",
              name: "nameUser",
              anchor: "100%",
              allowBlank: true
            },
            {
              xtype: "ViewComboAgency",
              fieldLabel: "Agencia",
              /* editable: false, */
              emptyText: "Seleccionar",
              forceSelection: false,
              anchor: "100%",
              name: "comboAgency",
              itemId: "comboAgency"
            },
            {
              xtype: "ViewComboWorkArea",
              fieldLabel: "Area",
              editable: true,
              emptyText: "Seleccionar",
              name: "comboWorkArea",
              itemId: "comboWorkArea",
              forceSelection: false,
              anchor: "100%"
            },
            {
              xtype: "ViewComboJobPlace",
              fieldLabel: "Cargo",
              editable: true,
              queryMode: "remote",
              anchor: "100%",
              name: "comboJobPlace",
              itemId: "comboJobPlace",
              emptyText: "Seleccionar",
              forceSelection: false
            },
            {
              xtype: "combobox",
              fieldLabel: "Categoría",
              editable: true,
              name: "comboCategory",
              anchor: "100%",
              itemId: "comboCategory",
              displayField: "description",
              valueField: "id",
              value: "%",
              typeAhead: true,
              store: {
                fields: ["id", "description"],
                data: [
                  { id: "ANALISTA", description: "ANALISTA" },
                  { id: "GESTOR", description: "GESTOR" },
                  { id: "COLABORADOR", description: "COLABORADOR" },
                  { id: "%", description: "TODOS" }
                ]
              }
            },
            {
              xtype: "combobox",
              fieldLabel: "Estado",
              editable: true,
              name: "comboStateUser",
              anchor: "100%",
              itemId: "comboStateUser",
              queryMode: "local",
              displayField: "description",
              valueField: "id",
              value: "S",
              typeAhead: true,
              store: {
                fields: ["id", "description"],
                data: [
                  { id: "S", description: "Activos" },
                  { id: "N", description: "Inactivos" }
                ]
              }
            }
          ]
        }
      ]
    });
    me.callParent(arguments);
  }
});
