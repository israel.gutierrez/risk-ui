Ext.define("ModelUserActives", {
  extend: "Ext.data.Model",
  fields: [
    "userName",
    "fullName",
    "documentNumber",
    "email",
    "agency",
    "idJobPlace",
    "workArea",
    "category",
    "descriptionJobPlace",
    "descriptionAgency",
    "descriptionCategory",
    "userDetailDescription",
    "descriptionWorkArea"
  ]
});

var storeUserActives = Ext.create("Ext.data.Store", {
  model: "ModelUserActives",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.util.search.SearchUser", {
  extend: "Ext.window.Window",
  alias: "widget.SearchUser",
  layout: {
    align: "stretch",
    type: "vbox"
  },
  anchorSize: 100,
  title: "BUSCAR USUARIO",
  titleAlign: "center",
  width: 650,
  height: 350,
  border: false,
  initComponent: function() {
    var me = this;
    var category = this.category;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          height: 45,
          bodyPadding: 10,
          items: [
            {
              xtype: "container",
              anchor: "100%",
              height: 26,
              layout: {
                type: "hbox"
              },
              items: [
                {
                  xtype: "combobox",
                  value: "2",
                  labelWidth: 40,
                  width: 190,
                  fieldLabel: "TIPO",
                  store: [
                    ["1", "USUARIO"],
                    ["2", "NOMBRE COMPLETO"]
                  ]
                },
                {
                  xtype:"UpperCaseTextField",
                  flex: 2,
                  listeners: {
                    specialkey: function(field, e) {
                      var property = "";
                      if (
                        Ext.ComponentQuery.query(
                          "SearchUser combobox"
                        )[0].getValue() == "1"
                      ) {
                        property = "u.username";
                      } else {
                        property = "u.fullName";
                      }
                      if (e.getKey() === e.ENTER) {
                        var grid = me.down("grid");
                        grid.store.getProxy().extraParams = {
                          propertyFind: property,
                          valueFind: field.getValue(),
                          propertyOrder: "u.fullName",
                          category: category != undefined ? category : ""
                        };
                        grid.store.getProxy().url =
                          "http://localhost:9000/giro/findMatchUser.htm";
                        grid.down("pagingtoolbar").moveFirst();
                      }
                    },
                    afterrender: function(f) {
                      f.focus(false, 200);
                    }
                  }
                }
              ]
            }
          ]
        },
        {
          xtype: "container",
          flex: 3,
          layout: {
            align: "stretch",
            type: "hbox"
          },
          items: [
            {
              xtype: "gridpanel",
              padding: "2 0 0 0",
              store: storeUserActives,
              flex: 1,
              titleAlign: "center",
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false
                },
                {
                  dataIndex: "userName",
                  width: 120,
                  align: "center",
                  text: "CÓDIGO"
                },
                {
                  dataIndex: "fullName",
                  flex: 1,
                  text: "NOMBRE"
                },
                {
                  dataIndex: "descriptionCategory",
                  width: 100,
                  text: "CATEGORÍA"
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: storeUserActives,
                displayInfo: true,
                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              },
              listeners: {
                render: function() {
                  var me = this;
                  me.store.getProxy().extraParams = {
                    propertyFind: "u.fullName",
                    valueFind: "",
                    propertyOrder: "u.fullName",
                    category: category != undefined ? category : ""
                  };
                  me.store.getProxy().url =
                    "http://localhost:9000/giro/findMatchUser.htm";
                  me.down("pagingtoolbar").moveFirst();
                }
              }
            }
          ]
        }
      ],
      buttons: [
        {
          text: "SALIR",
          scale: "medium",
          iconCls: "logout",
          handler: function(btn) {
            me.close();
          }
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});
