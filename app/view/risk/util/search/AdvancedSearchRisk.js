Ext.define("DukeSource.view.risk.util.search.AdvancedSearchRisk", {
  extend: "Ext.form.Panel",
  alias: "widget.AdvancedSearchRisk",
  requires: [
    "DukeSource.view.risk.parameter.combos.ViewComboProcessType",
    "DukeSource.view.risk.parameter.combos.ViewComboProcess"
  ],
  title: "Búsqueda avanzada",
  titleAlign: "center",
  padding: "0 0 0 2",
  region: "east",
  itemId: "AdvancedSearchRisk",
  collapsed: true,
  collapsible: true,
  split: true,
  border: true,
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "container",
          layout: {
            type: "anchor"
          },
          padding: "5",
          items: [
            {
              xtype: "textfield",
              anchor: "85%",
              itemId: "idRisk",
              fieldLabel: "Id"
            },
            {
              xtype: "textfield",
              anchor: "85%",
              itemId: "codeRisk",
              fieldLabel: "C&oacute;digo"
            },
            {
              xtype: "ViewComboProcessType",
              anchor: "85%",
              fieldLabel: "Macroproceso",
              queryMode: "remote",
              msgTarget: "side",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              name: "processType",
              itemId: "processType",
              listeners: {
                render: function(cbo) {
                  cbo.getStore().load();
                },
                select: function(cbo, record) {
                  me.down("#process")
                    .getStore()
                    .load({
                      url:
                        "http://localhost:9000/giro/showListProcessActives.htm",
                      params: {
                        valueFind: cbo.getValue()
                      },
                      callback: function(cbo) {
                        me.down("#process").reset();
                      }
                    });
                  me.down("#process").setDisabled(false);
                }
              }
            },
            {
              xtype: "ViewComboProcess",
              anchor: "85%",
              fieldLabel: "Proceso",
              msgTarget: "side",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              multiSelect: true,
              name: "process",
              itemId: "process"
            },
            {
              xtype: "combobox",
              value: "det.valueResidualRisk",
              itemId: "conditionCombo",
              anchor: "85%",
              fieldLabel: "Ordenar por",
              store: [
                ["ma.valueMatrix", "Riesgo inherente"],
                ["det.valueResidualRisk", "Riesgo residual"]
              ]
            }
          ]
        }
      ]
    });
    me.callParent(arguments);
  }
});
