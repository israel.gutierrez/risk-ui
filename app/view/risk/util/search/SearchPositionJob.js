Ext.define("ModelPositionJob", {
  extend: "Ext.data.Model",
  fields: [
    "userName",
    "fullName",
    "documentNumber",
    "email",
    "agency",
    "idJobPlace",
    "workArea",
    "descriptionJobPlace",
    "descriptionAgency",
    "descriptionWorkArea"
  ]
});

var StorePositionJob = Ext.create("Ext.data.Store", {
  model: "ModelPositionJob",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
//Busca cargo
Ext.define("DukeSource.view.risk.util.search.SearchPositionJob", {
  extend: "Ext.window.Window",
  alias: "widget.SearchPositionJob",
  layout: {
    align: "stretch",
    type: "vbox"
  },
  anchorSize: 100,
  title: "BUSCAR USUARIO",
  titleAlign: "center",
  width: 500,
  height: 350,
  initComponent: function() {
    var me = this;
    var category = this.category;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          height: 45,
          padding: "2 2 2 2",
          bodyPadding: 10,
          items: [
            {
              xtype: "container",
              anchor: "100%",
              height: 26,
              layout: {
                type: "hbox"
              },
              items: [
                {
                  xtype: "combobox",
                  value: "2",
                  labelWidth: 40,
                  width: 190,
                  fieldLabel: "TIPO",
                  store: [
                    ["1", "USUARIO"],
                    ["2", "NOMBRE COMPLETO"]
                  ]
                },
                {
                  xtype:"UpperCaseTextField",
                  flex: 2,
                  listeners: {
                    specialkey: function(field, e) {
                      var property = "";
                      if (
                        Ext.ComponentQuery.query(
                          "SearchUser combobox"
                        )[0].getValue() == "1"
                      ) {
                        property = "username";
                      } else {
                        property = "fullName";
                      }
                      if (e.getKey() === e.ENTER) {
                        var grid = me.down("grid");
                        grid.store.getProxy().extraParams = {
                          propertyFind: property,
                          valueFind: field.getValue(),
                          propertyOrder: "username",
                          category: category != undefined ? category : ""
                        };
                        grid.store.getProxy().url =
                          "http://localhost:9000/giro/findMatchUser.htm";
                        grid.down("pagingtoolbar").moveFirst();
                      }
                    }
                  }
                }
              ]
            }
          ]
        },
        {
          xtype: "container",
          flex: 3,
          padding: "2 2 2 2",
          layout: {
            align: "stretch",
            type: "hbox"
          },
          items: [
            {
              xtype: "gridpanel",
              padding: "0 1 0 0",
              store: StorePositionJob,
              flex: 1,
              titleAlign: "center",
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false
                },
                {
                  dataIndex: "userName",
                  width: 120,
                  text: "CODIGO"
                },
                {
                  dataIndex: "fullName",
                  flex: 1,
                  text: "NOMBRE"
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: StorePositionJob,
                displayInfo: true,
                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              },
              listeners: {
                render: function() {
                  var me = this;
                  me.store.getProxy().extraParams = {
                    propertyFind: "fullName",
                    valueFind: "",
                    propertyOrder: "fullName",
                    category: category != undefined ? category : ""
                  };
                  me.store.getProxy().url =
                    "http://localhost:9000/giro/findMatchUser.htm";
                  me.down("pagingtoolbar").moveFirst();
                }
              }
            }
          ]
        }
      ]
    });

    me.callParent(arguments);
  }
});
