Ext.define("TextFieldNumberFormat", {
  extend: "Ext.form.field.Text",
  alias: "widget.TextFieldNumberFormat",
  maskRe: /[0-9]/,
  initComponent: function() {
    this.callParent(arguments);
  }
});
