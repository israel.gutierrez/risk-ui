Ext.define("UpperCaseTextArea", {
  extend: "Ext.form.field.TextArea",
  alias: "widget.UpperCaseTextArea",
  initComponent: function() {
    this.on("blur", this.handleFieldBlur, this);
    this.callParent(arguments);
  },
  handleFieldBlur: function(e) {
    var t = e.getValue();
    e.setRawValue(t.trim());
    e.setRawValue(
      e
        .getValue()
        .toString()
        .replace(/"/g, "'")
    );
  }
});
