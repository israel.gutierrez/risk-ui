Ext.define('DukeSource.view.risk.util.Constants', {
    singleton: true,
    REVISED: "R",
    YES: "S",
    NO: "N",
    PENDING_REVISED: "P",
    COLLABORATOR: "COLABORADOR",
    ANALYST: "ANALISTA",
    GESTOR: "GESTOR",
    SECURITY: 'SI',
    CONTINUITY: 'CN',
    OPERATIONAL: 'RO',
    EVENT: 'EV',
    RISK: 'RI',
    GHOST: "FANTASMA",
    CONTROL_AVERAGE: 'AVERAGE',
    CONTROL_POINTS: 'POINTS',
    CONTROL_CALCULATE: 'CALCULATE',
    PENDING: "PE",
    PROCESS: "PR",
    INVALID: "IN",
    FINISHED: "CU",
    SECURITY_ANALYST: "ANALISTASI",
    BUSINESS_CONTINUITY: 'ANALISTACN',
    SUPER_ANALYST: "SUPER_ANALISTA",
    METHODOLOGY_STANDARD: "METHODOLOGY_STANDARD",
    METHODOLOGY_INITIAL: "METHODOLOGY_INITIAL",
    ITEMS_PAGE: 25
});
