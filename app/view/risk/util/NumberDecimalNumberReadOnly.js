Ext.define("NumberDecimalNumberReadOnly", {
  extend: "Ext.form.field.Number",
  alias: "widget.NumberDecimalNumberReadOnly",
  readOnly: true,
  hideTrigger: true,
  keyNavEnabled: false,
  decimalPrecision: DECIMAL_PRECISION,
  mouseWheelEnabled: false,
  fieldCls: "readOnlyText",
  initComponent: function() {
    this.on("change", this.handleFieldChanged, this);
    this.callParent(arguments);
  },
  handleFieldChanged: function(e) {
    e.setRawValue(Ext.util.Format.number(e.getValue(), "0,0.00"));
  },
  handleFieldColor: function(e) {
    if (e.getValue() < 0) {
      e.setFieldStyle("background-color: #FF0000; background-image: none;");
    } else {
      e.setFieldStyle("background-color: #3300FF; background-image: none;");
    }
  },
  handleFieldLoad: function(e) {
    e.setRawValue(Ext.util.Format.number(e.getValue(), "0,0.00"));
    if (e.getValue() < 0) {
      e.setFieldStyle("background-image: none;color: #BB001C;");
    } else {
      e.setFieldStyle("background-image: none;color: blue;");
    }
  }
});
