var yesNo = Ext.create('Ext.data.Store', {
    fields: ['id', 'description'],
    data: [
        {"id": "S", "description": "Si"},
        {"id": "N", "description": "No"}
    ]
});

Ext.define('DukeSource.view.risk.util.ViewComboYesNo', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboYesNo',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'id',
    editable: true,
    forceSelection: true,
    store: yesNo
});