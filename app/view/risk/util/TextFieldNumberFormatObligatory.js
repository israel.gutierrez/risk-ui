Ext.define("TextFieldNumberFormatObligatory", {
  extend: "Ext.form.field.Text",
  alias: "widget.TextFieldNumberFormatObligatory",
  maskRe: /[0-9]/,
  allowBlank: false,
  msgTarget: "side",
  fieldCls: "obligatoryTextField",
  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
  initComponent: function() {
    this.callParent(arguments);
  }
});
