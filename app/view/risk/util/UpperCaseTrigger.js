Ext.define("UpperCaseTrigger", {
  extend: "Ext.form.field.Trigger",
  alias: "widget.UpperCaseTrigger",
  triggerCls: "x-form-clear-trigger",
  size: 30,
  minChars: 1,
  enableKeyEvents: true,
  initComponent: function() {
    this.callParent(arguments);
  },
  onTriggerClick: function() {
    this.setRawValue("");
    DukeSource.global.DirtyView.focusEvent(this);
  }
});
