Ext.define("DukeSource.view.risk.util.ComboSelectCount", {
  alias: "plugin.ComboSelectCount",
  init: function(combo) {
    combo.enableKeyEvents = true;
    combo.typeAhead = true;
    combo.minChars = 0;

    combo.on({
      expand: {
        fn: function() {
          combo.doQuery = function(queryString, forceAll) {
            combo.expand();
            combo.store.clearFilter(!forceAll);
            if (!forceAll) {
              combo.store.filter(
                combo.displayField,
                new RegExp(Ext.String.escapeRegex(queryString), "i")
              );
            }
          };
        },
        single: true
      }
    });
  }
});
