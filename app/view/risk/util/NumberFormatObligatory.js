Ext.define("NumberFormatObligatory", {
  extend: "Ext.form.field.Number",
  alias: "widget.NumberFormatObligatory",
  hideTrigger: true,
  keyNavEnabled: false,
  enableKeyEvents: true,
  mouseWheelEnabled: false,
  allowBlank: false,
  msgTarget: "side",
  fieldCls: "obligatoryTextField",
  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE
});
