Ext.define("NumberDecimalNumber", {
  extend: "Ext.form.field.Number",
  alias: "widget.NumberDecimalNumber",
  hideTrigger: true,
  keyNavEnabled: false,
  enableKeyEvents: true,
  mouseWheelEnabled: false,
  useThousandSeparator: true,
  thousandSeparator: ",",
  fieldStyle: "text-align: right;",
  minValue: 0,
  initComponent: function() {
    this.callParent(arguments);
  },
  setValue: function(e) {
    e =
      typeof e == "number" ? e : String(e).replace(this.decimalSeparator, ".");
    e = isNaN(e) ? "" : String(e).replace(".", this.decimalSeparator);
    e = isNaN(e)
      ? ""
      : this.fixPrecision(String(e).replace(".", this.decimalSeparator));
    return Ext.form.field.Number.superclass.setRawValue.call(this, e);
  },
  onSpinUp: function() {
    var e = this;
    if (!e.readOnly) {
      var t = parseFloat(e.step);
      if (e.getValue() !== "") {
        t = parseFloat(e.getValue());
        e.setValue(t + parseFloat(e.step));
      }
    }
  },
  fixPrecision: function(e) {
    var t = isNaN(e);
    if (!this.allowDecimals || this.decimalPrecision == -1 || t || !e) {
      return t ? "" : e;
    }
    return parseFloat(e).toFixed(this.decimalPrecision);
  }
});
