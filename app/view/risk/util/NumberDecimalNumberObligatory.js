Ext.define("NumberDecimalNumberObligatory", {
  extend: "Ext.form.field.Number",
  alias: "widget.NumberDecimalNumberObligatory",
  hideTrigger: true,
  keyNavEnabled: false,
  enableKeyEvents: true,
  mouseWheelEnabled: true,
  allowBlank: false,
  decimalPrecision: DECIMAL_PRECISION,
  useThousandSeparator: true,
  thousandSeparator: ",",
  fieldStyle: "text-align: right;",
  minValue: 0,
  msgTarget: "side",
  fieldCls: "obligatoryTextField",
  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
  initComponent: function() {
    this.callParent(arguments);
  },
  setValue: function(e) {
    e =
      typeof e === "number" ? e : String(e).replace(this.decimalSeparator, ".");
    e = isNaN(e) ? "" : String(e).replace(".", this.decimalSeparator);
    e = isNaN(e)
      ? ""
      : this.fixPrecision(String(e).replace(".", this.decimalSeparator));
    return Ext.form.field.Number.superclass.setRawValue.call(this, e);
  },
  onSpinUp: function() {
    var e = this;
    if (!e.readOnly) {
      var t = parseFloat(e.step);
      if (e.getValue() !== "") {
        t = parseFloat(e.getValue());
        e.setValue(t + parseFloat(e.step));
      }
    }
  },
  fixPrecision: function(e) {
    var t = isNaN(e);
    if (!this.allowDecimals || this.decimalPrecision === -1 || t || !e) {
      return t ? "" : e;
    }
    return parseFloat(e).toFixed(this.decimalPrecision);
  },
  valueToRaw: function(e) {
    var t = this,
      n = t.decimalSeparator;
    e = t.parseValue(e);
    e = t.fixPrecision(e);
    e = Ext.isNumber(e) ? e : parseFloat(String(e).replace(n, "."));
    if (isNaN(e)) {
      e = "";
    } else {
      e = t.forcePrecision ? e.toFixed(t.decimalPrecision) : parseFloat(e);
      e = String(e).replace(".", n);
    }
    return e;
  }
});
