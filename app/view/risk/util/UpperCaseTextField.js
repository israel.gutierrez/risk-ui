Ext.define("UpperCaseTextField", {
  extend: "Ext.form.field.Text",
  alias: "widget.UpperCaseTextField",
  initComponent: function() {
    this.on("blur", this.handleFieldBlur, this);
    this.callParent(arguments);
  },
  handleFieldBlur: function(e) {
    var t = e.getValue();
    e.setRawValue(t.trim());
    e.setRawValue(
      e
        .getValue()
        .toString()
        .replace(/"/g, "'")
    );
  }
});
