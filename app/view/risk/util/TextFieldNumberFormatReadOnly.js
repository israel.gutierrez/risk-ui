Ext.define("TextFieldNumberFormatReadOnly", {
  extend: "Ext.form.field.Text",
  alias: "widget.TextFieldNumberFormatReadOnly",
  maskRe: /[0-9]/,
  readOnly: true,
  fieldCls: "readOnlyText",
  initComponent: function() {
    this.callParent(arguments);
  }
});
