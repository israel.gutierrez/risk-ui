Ext.define("UpperCaseTextFieldReadOnly", {
  extend: "Ext.form.field.Text",
  alias: "widget.UpperCaseTextFieldReadOnly",
  readOnly: true,
  fieldCls: "readOnlyText",
  initComponent: function() {
    this.on("change", this.handleFieldChanged, this);
    this.on("blur", this.handleFieldBlur, this);
    this.callParent(arguments);
  },
  handleFieldChanged: function(e, t) {
    e.setRawValue(t);
  },
  handleFieldBlur: function(e) {
    var t = e.getValue();
    e.setRawValue(t.trim());
    e.setRawValue(
      e
        .getValue()
        .toString()
        .replace(/"/g, "'")
    );
  }
});
