Ext.define("DukeSource.view.risk.util.WindowAuditory", {
  extend: "Ext.window.Window",
  alias: "widget.WindowAuditory",
  height: 290,
  width: 367,
  layout: {
    type: "fit"
  },
  title: DukeSource.global.GiroMessages.TITLE_AUDIT,
  initComponent: function() {
    this.formAuditory = Ext.create("Ext.form.Panel", {
      border: false,
      bodyPadding: 10,
      title: "",
      items: [
        {
          xtype:"UpperCaseTextFieldReadOnly",
          anchor: "100%",
          readOnly: true,
          fieldLabel: "Fecha inserción",
          labelWidth: 150,
          name: "dateInsertion"
        },
        {
          xtype:"UpperCaseTextFieldReadOnly",
          anchor: "100%",
          readOnly: true,
          fieldLabel: "Usuario que insertó",
          labelWidth: 150,
          name: "userInsertion"
        },
        {
          xtype:"UpperCaseTextFieldReadOnly",
          anchor: "100%",
          readOnly: true,
          fieldLabel: "Ip inserción",
          labelWidth: 150,
          name: "ipInsertion"
        },
        {
          xtype:"UpperCaseTextFieldReadOnly",
          anchor: "100%",
          readOnly: true,
          fieldLabel: "Terminal de inserción",
          labelWidth: 150,
          name: "terminalInsertion"
        },
        {
          xtype:"UpperCaseTextFieldReadOnly",
          anchor: "100%",
          readOnly: true,
          fieldLabel: "Fecha de modificación",
          labelWidth: 150,
          name: "dateModification"
        },
        {
          xtype:"UpperCaseTextFieldReadOnly",
          anchor: "100%",
          readOnly: true,
          fieldLabel: "Usuario que modificó",
          labelWidth: 150,
          name: "userModification"
        },
        {
          xtype:"UpperCaseTextFieldReadOnly",
          anchor: "100%",
          readOnly: true,
          fieldLabel: "Ip modificación",
          labelWidth: 150,
          name: "ipModification"
        },
        {
          xtype:"UpperCaseTextFieldReadOnly",
          anchor: "100%",
          readOnly: true,
          fieldLabel: "Terminal modificación",
          labelWidth: 150,
          name: "terminalModification"
        }
      ]
    });
    this.items = [this.formAuditory];
    this.buttons = [
      {
        text: "SALIR",
        scope: this,
        handler: this.close,
        iconCls: "logout"
      }
    ];

    this.callParent();
  }
});
