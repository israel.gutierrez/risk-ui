Ext.define('DukeSource.view.risk.util.Messages', {
    singleton: true,

    TITLE_MENSAJE: 'MENSAJE',
    TITLE_CONFIRMAR: 'CONFIRMAR',
    TITLE_ADVERTENCIA: 'ADVERTENCIA',
    TITLE_AUDITORIA: 'AUDITORIA',
    TITLE_ERROR: 'ERROR',
    MESSAGE_SALIR: 'Esta seguro que desea CERRAR el espacio de trabajo?',
    MESSAGE_ITEM: 'Seleccione por favor un REGISTRO',
    MESSAGE_ELIMINAR: 'Esta seguro que desea ELIMINAR',
    MESSAGE_COMPLETAR: 'Verifique por favor la la CONSISTENCIA de DATOS a ingresar en cada campo',
    MESSAGE_OBLIGATORIO: 'Este campo es OBLIGATORIO',
    MESSAGE_VACIO: 'El campo no contiene INFORMACION, completelo',
    MESSAGE_GRIDNUMERO: 'No existe informaci&oacute;n a mostrar',
    MESSAGE_GRIDPAGINA: 'Mostrando {0} - {1} de {2}',
    MESSAGE_MAXCHARACTER: 'El Numero de Caracteres de debe ser menor a ',
    MESSAGE_SAVING: 'GIRO - TOPRISK Saving...',
    MESSAGE_LOADING: 'GIRO - TOPRISK Loading...',
    MESSAGE_ADVANCE_VERIFIED: 'El avance no puede ser modificado, ya fue revisado',
    MESSAGE_ERROR_LOAD: 'Error al cargar o no existen los datos',
    MESSAGE_FIELDS_ERROR: 'Complete por favor los campos obligatorios o corriga su valor',
    MESSAGE_REPROGRAM_PLAN: 'El Plan de accion esta siendo reprogramado'
});