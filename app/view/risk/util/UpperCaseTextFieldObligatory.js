Ext.define("UpperCaseTextFieldObligatory", {
  extend: "Ext.form.field.Text",
  alias: "widget.UpperCaseTextFieldObligatory",
  allowBlank: false,
  msgTarget: "side",
  fieldCls: "obligatoryTextField",
  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
  initComponent: function() {
    this.on("blur", this.handleFieldBlur, this);
    this.callParent(arguments);
  },
  handleFieldBlur: function(e) {
    e.setRawValue(
      e
        .getValue()
        .toString()
        .replace(/"/g, "'")
    );
  }
});
