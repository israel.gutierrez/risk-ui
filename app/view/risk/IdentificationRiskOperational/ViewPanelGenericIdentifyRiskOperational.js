Ext.define("ModelGridGenericIdentifyRiskOperational", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "usernameRegister",
    "usernameAccept",
    "fullName",
    "consequence",
    "fail",
    "cause",
    "description",
    "state",
    "comment",
    "stateIdentification",
    "descriptionState",
    {
      name: "dateEvaluation",
      type: "date",
      format: "d/m/Y",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y");
      }
    },
    "idRisk",
    "risk",
    "userRegister",
    "userAccept",
    "frequency",
    "descriptionFrequency",
    "impact",
    "descriptionImpact",
    "idMatrix",
    "matrix",
    "scaleRisk",
    "typeMatrix",
    "typeMatrixScaleRisk",
    "subProcess",
    "descriptionSubProcess",
    "idActivity",
    "descriptionProcess",
    "fullNameRegister",
    "fullNameAccept",
    "descriptionScaleRisk",
    "descriptionMatrix",
    "color",
    "valueMatrix",
    "descriptionTypeMatrix",
    "descriptionRisk",
    "codeRisk",
    "idOperationalRiskExposition",
    "maxAmount",
    "year",
    "userApprove",
    "commentApprove",
    "dateApprove",
    "checkApprove",
    "eventOne",
    "descriptionEventOne",
    "factorRisk",
    "descriptionFactorRisk"
  ]
});

var StoreGridGenericIdentifyRiskOperational = Ext.create("Ext.data.Store", {
  model: "ModelGridGenericIdentifyRiskOperational",
  groupField: "nameTypeRiskEvaluation",
  autoLoad: false,
  pageSize: 50,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
var sm = Ext.create("Ext.selection.CheckboxModel");

Ext.define(
  "DukeSource.view.risk.IdentificationRiskOperational.ViewPanelGenericIdentifyRiskOperational",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelGenericIdentifyRiskOperational",
    itemId: "viewPanelGenericIdentifyRiskOperational",
    requires: [
      "DukeSource.view.risk.util.search.AdvancedSearchIdentificationRisk",
      "DukeSource.view.risk.util.search.AdvancedSearchRisk"
    ],
    layout: "fit",
    border: false,
    initComponent: function() {
      var me = this;

      function modifyIdentification(grid) {
        var record = grid.getSelectionModel().getSelection()[0];

        if (record === undefined) {
          DukeSource.global.DirtyView.messageAlert(
            DukeSource.global.GiroMessages.TITLE_WARNING,
            DukeSource.global.GiroMessages.MESSAGE_ITEM,
            Ext.Msg.WARNING
          );
        } else {
          var win = Ext.create(
            "DukeSource.view.risk.IdentificationRiskOperational.windows.ViewWindowIdentifyRiskOperational",
            {
              modal: true,
              grid: me.down("grid")
            }
          );
          win.down("#eventOne").store.load();

          if (record.get("impact") !== "") {
            var idTypeMatrix = record.get("typeMatrix");
            win.down("#impact").store.load({
              params: {
                idTypeMatrix: idTypeMatrix,
                idOperationalRiskExposition: idMaxExpositionActive
              },
              callback: function() {
                win.down("#frequency").store.load({
                  params: {
                    idTypeMatrix: idTypeMatrix,
                    idOperationalRiskExposition: idMaxExpositionActive
                  },
                  callback: function() {
                    win
                      .down("form")
                      .getForm()
                      .setValues(record.raw);
                    win.show();
                  }
                });
              }
            });
          } else {
            win
              .down("form")
              .getForm()
              .setValues(record.raw);
            win.show();
          }
          win
            .down("#descriptionScaleRisk")
            .setFieldStyle(
              "background-color: #" +
                record.get("color") +
                "; background-image: none;color:#000000;"
            );
          win.down("#description").focus(false, 200);

          if (
            category === DukeSource.global.GiroConstants.COLLABORATOR &&
            record.get("stateIdentification") !== "P"
          ) {
            win.query(".textfield,.textarea").forEach(function(c) {
              c.setDisabled(true);
            });
            win.down("button").setDisabled(true);
          }
        }
      }

      Ext.applyIf(me, {
        items: [
          {
            xtype: "container",
            layout: "border",
            items: [
              {
                xtype: "AdvancedSearchIdentificationRisk",
                itemId: "AdvancedSearchIdentificationRisk",
                region: "west",
                padding: 2,
                split: false,
                flex: 0.5,
                tbar: [
                  {
                    text: "BUSCAR",
                    scale: "medium",
                    action: "searchAdvancedIdentification",
                    iconCls: "search"
                  },
                  {
                    text: "LIMPIAR",
                    scale: "medium",
                    iconCls: "clear",
                    handler: function(btn) {
                      btn
                        .up("form")
                        .getForm()
                        .reset();
                    }
                  }
                ]
              },
              {
                xtype: "gridpanel",
                itemId: "previousRiskGrid",
                flex: 2,
                region: "center",
                title: "Identificación de riesgos operacionales",
                titleAlign: "center",
                store: StoreGridGenericIdentifyRiskOperational,
                loadMask: true,
                columnLines: true,
                padding: 2,
                tbar: [
                  {
                    text: "Nuevo",
                    iconCls: "add",
                    scale: "medium",
                    cls: "my-btn",
                    // hidden: category === DukeSource.global.GiroConstants.GESTOR,
                    overCls: "my-over",
                    handler: function(btn) {
                      var win = Ext.create(
                        "DukeSource.view.risk.IdentificationRiskOperational.windows.ViewWindowIdentifyRiskOperational",
                        {
                          modal: true,
                          grid: me.down("grid")
                        }
                      );
                      win.show();
                      win.down("#description").focus(false, 200);
                    }
                  },
                  "-",
                  {
                    text: "Modificar",
                    iconCls: "modify",
                    scale: "medium",
                    cls: "my-btn",
                    overCls: "my-over",
                    handler: function(btn) {
                      var grid = btn.up("grid");
                      modifyIdentification(grid);
                    }
                  },
                  "-",
                  {
                    text: "Vincular riesgo",
                    iconCls: "relation",
                    scale: "medium",
                    hidden:
                      category !== DukeSource.global.GiroConstants.ANALYST,
                    cls: "my-btn",
                    overCls: "my-over",
                    handler: function() {
                      var grid = Ext.ComponentQuery.query(
                        "ViewPanelGenericIdentifyRiskOperational grid"
                      )[0];
                      var row = grid.getSelectionModel().getSelection()[0];

                      if (row !== undefined) {
                        var win = Ext.create(
                          "DukeSource.view.risk.IdentificationRiskOperational.windows.WindowRelationRisk",
                          {
                            modal: true,
                            idIdentification: row.get("id")
                          }
                        );
                        win.show(undefined, function() {
                          var gridSelect = win.down("#gridRiskSelected");
                          gridSelect.store.getProxy().extraParams = {
                            idRisk: row.get("risk")
                          };
                          gridSelect.store.getProxy().url =
                            "http://localhost:9000/giro/showListIdentificationActivesByRisk.htm?nameView=ViewPanelGenericIdentifyRiskOperational";
                          gridSelect.down("pagingtoolbar").doRefresh();
                        });
                      } else {
                        DukeSource.global.DirtyView.messageWarning(
                          DukeSource.global.GiroMessages.MESSAGE_ITEM
                        );
                      }
                    }
                  },
                  "-",
                  {
                    text: "Eliminar",
                    iconCls: "delete",
                    scale: "medium",
                    hidden: hidden("WPGI_BTN_DeleteIdentificationRisk"),
                    cls: "my-btn",
                    overCls: "my-over",
                    action: "deleteIdentificationRisk"
                  },
                  "-",
                  {
                    text: "Evaluar",
                    iconCls: "auditory",
                    hidden: true,
                    scale: "medium",
                    cls: "my-btn",
                    overCls: "my-over",
                    action: "evaluateIdentificationRisk"
                  }
                ],
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 30,
                    resizable: true
                  },
                  {
                    header: "Estado",
                    dataIndex: "descriptionState",
                    width: 80,
                    align: "center"
                  },
                  // {
                  //     header: 'PROCESO',
                  //     hidden: hidden('PGCProcess'),
                  //     dataIndex: 'descriptionProcess',
                  //     width: 180
                  // },
                  {
                    header: "Descripción",
                    dataIndex: "description",
                    width: 500
                  },
                  {
                    header: "Fecha",
                    xtype: "datecolumn",
                    dataIndex: "dateEvaluation",
                    format: "d/m/Y",
                    width: 80,
                    align: "center",
                    renderer: Ext.util.Format.dateRenderer("d/m/Y")
                  },
                  // {
                  //     header: 'Falla',
                  //     dataIndex: 'fail',
                  //     hidden:hidden('WIRFail'),
                  //     width: 300
                  // },
                  {
                    header: "Causas ¿porque?",
                    dataIndex: "cause",
                    width: 300
                  },
                  {
                    header: "Consecuencia",
                    dataIndex: "consequence",
                    width: 300
                  },
                  {
                    header: "Riesgo vinculado",
                    align: "center",
                    dataIndex: "codeRisk",
                    width: 70,
                    renderer: function(value, metaData, record) {
                      return '<span style="color: red">' + value + "</span>";
                    }
                  },
                  // {
                  //     header: 'INHERENTE',
                  //     hidden: hidden('WIRTypeMatrix'),
                  //     columns: [
                  //         {
                  //             header: 'FRECUENCIA',
                  //             align: 'center',
                  //             hidden: hidden('WIRFrequency'),
                  //             dataIndex: 'descriptionFrequency',
                  //             width: 90
                  //         }
                  //         ,
                  //         {header: 'IMPACTO', align: 'center',hidden: hidden('WIRImpact'), dataIndex: 'descriptionImpact', width: 90}
                  //         ,
                  //         {
                  //             header: 'RIESGO',
                  //             align: 'center',
                  //             hidden: hidden('WIRTypeMatrix'),
                  //             dataIndex: 'descriptionMatrix',
                  //             width: 100,
                  //             renderer: function (value, metaData, record) {
                  //                 metaData.tdAttr = 'style="background-color: #' + record.get('color') + ' !important;"';
                  //                 return '<span>' + value + '</span>';
                  //             }
                  //         }
                  //     ]
                  // },
                  {
                    header: "Usuario que registró",
                    dataIndex: "fullNameRegister",
                    width: 200
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: 20,
                  store: StoreGridGenericIdentifyRiskOperational,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  render: function(grid) {
                    grid.store.proxy.url =
                      "http://localhost:9000/giro/showListIdentificationActives.htm?nameView=ViewPanelGenericIdentifyRiskOperational";
                    grid.store.proxy.params = {
                      userName: ""
                    };
                    grid.down("pagingtoolbar").moveFirst();
                  },
                  itemdblclick: function(grid, record, item, index, e) {
                    modifyIdentification(grid);
                  }
                }
              }
            ]
          }
        ],
        listeners: {
          afterrender: function(win) {
            if (
              category === DukeSource.global.GiroConstants.COLLABORATOR ||
              category === DukeSource.global.GiroConstants.GESTOR
            ) {
              me.down("#AdvancedSearchIdentificationRisk").setVisible(false);
            } else {
              me.down("#AdvancedSearchIdentificationRisk").setVisible(false);
            }
          }
        }
      });

      me.callParent(arguments);
    }
  }
);
