Ext.define(
  "DukeSource.view.risk.IdentificationRiskOperational.windows.ViewWindowIdentifyRiskOperational",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowIdentifyRiskOperational",
    width: 747,
    layout: "fit",
    title: "Identificación del riesgo",
    titleAlign: "center",
    requires: [
      "DukeSource.view.risk.parameter.combos.ViewComboTypeMatrix",
      "DukeSource.view.risk.parameter.combos.ViewComboFrequency",
      "DukeSource.view.risk.parameter.combos.ViewComboImpact"
    ],
    border: false,
    initComponent: function() {
      var me = this;
      var innerGrid = me.grid;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            fieldDefaults: {
              labelCls: "changeSizeFontToEightPt",
              fieldCls: "changeSizeFontToEightPt"
            },
            items: [
              {
                xtype: "container",
                layout: {
                  type: "hbox",
                  align: "stretch"
                },
                height: 35,
                items: [
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "userApprove",
                    name: "userApprove"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "commentApprove",
                    name: "commentApprove"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "dateApprove",
                    name: "dateApprove"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    value: "N",
                    itemId: "checkApprove",
                    name: "checkApprove"
                  },
                  {
                    xtype: "datefield",
                    labelWidth: 115,
                    name: "dateEvaluation",
                    format: "d/m/Y",
                    value: new Date(),
                    maxValue: new Date(),
                    fieldCls: "readOnlyText",
                    readOnly: true,
                    itemId: "dateEvaluation",
                    flex: 1,
                    fieldLabel: "F. identificación"
                  },
                  {
                    xtype: "textfield",
                    flex: 1,
                    padding: "0 0 0 5",
                    name: "userRegister",
                    readOnly: true,
                    itemId: "userRegister",
                    fieldCls: "readOnlyText",
                    fieldLabel: "Usuario registró",
                    value: userName,
                    labelWidth: 130,
                    labelAlign: "left"
                  },
                  {
                    xtype: "button",
                    text: "APROBAR",
                    flex: 0.35,
                    hidden: true,
                    scale: "medium",
                    margin: "0 0 0 2",
                    iconCls: "evaluate",
                    name: "btnApproveIdentification",
                    itemId: "btnApproveIdentification",
                    cls: "my-btn",
                    overCls: "my-over",
                    handler: function() {
                      var win = Ext.create(
                        "DukeSource.view.risk.DatabaseEventsLost.windows.WindowApproveCorrelative",
                        {
                          modal: true,
                          id: me.down("#id").getValue(),
                          actionExecute: "approveIdentification"
                        }
                      );
                      if (
                        me.down("#checkApprove").getValue() ===
                        DukeSource.global.GiroConstants.YES
                      ) {
                        win
                          .down("#dateApprove")
                          .setValue(me.down("#dateApprove").getValue());
                        win
                          .down("#userApprove")
                          .setValue(me.down("#userApprove").getValue());
                        win
                          .down("#commentApprove")
                          .setValue(me.down("#commentApprove").getValue());
                        win.down("#saveApprove").setDisabled(true);
                        win.down("#saveApprove").setVisible(false);
                      }
                      win.show();
                    }
                  }
                ]
              },
              {
                xtype: "fieldset",
                checkboxToggle: true,
                title:
                  '<span style="font-weight: bold;">CONCEPTUALIZACIÓN DEL RIESGO</span>',
                items: [
                  {
                    xtype: "UpperCaseTextArea",
                    anchor: "100%",
                    height: 80,
                    maxLength: 1000,
                    name: "description",
                    itemId: "description",
                    hidden: hidden("WIRDescription"),
                    allowBlank: blank("WIRDescription"),
                    fieldCls: styleField("WIRDescription"),
                    fieldLabel: getName("WIRDescription"),
                    labelAlign: "left"
                  },
                  {
                    xtype: "UpperCaseTextArea",
                    anchor: "100%",
                    height: 60,
                    name: "fail",
                    itemId: "fail",
                    maxLength: 800,
                    fieldCls: styleField("WIRFail"),
                    fieldLabel: getName("WIRFail"),
                    allowBlank: blank("WIRFail"),
                    hidden: hidden("WIRFail"),
                    labelAlign: "left"
                  },
                  {
                    xtype: "UpperCaseTextArea",
                    anchor: "100%",
                    fieldLabel: getName("WIRCause"),
                    allowBlank: blank("WIRCause"),
                    fieldCls: styleField("WIRCause"),
                    hidden: hidden("WIRCause"),
                    name: "cause",
                    itemId: "cause",
                    height: 60,
                    maxLength: 800,
                    labelAlign: "left"
                  },
                  {
                    xtype: "UpperCaseTextArea",
                    anchor: "100%",
                    fieldLabel: getName("WIRConsequence"),
                    allowBlank: blank("WIRConsequence"),
                    fieldCls: styleField("WIRConsequence"),
                    hidden: hidden("WIRConsequence"),
                    height: 60,
                    name: "consequence",
                    maxLength: 800,
                    itemId: "consequence",
                    labelAlign: "left"
                  }
                ]
              },
              {
                xtype: "fieldset",
                checkboxToggle: true,
                title:
                  '<span style="font-weight: bold;">' +
                  getName("WIRContainerAdditional") +
                  "</span>",
                layout: {
                  type: "hbox",
                  align: "stretch"
                },
                items: [
                  {
                    xtype: "container",
                    padding: "0 10 0 0",
                    flex: 1,
                    layout: "anchor",
                    items: [
                      {
                        xtype: "ViewComboTypeMatrix",
                        anchor: "100%",
                        msgTarget: "side",
                        fieldLabel: getName("WIRTypeMatrix"),
                        allowBlank: blank("WIRTypeMatrix"),
                        fieldCls: styleField("WIRTypeMatrix"),
                        hidden: hidden("WIRTypeMatrix"),
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        name: "typeMatrix",
                        itemId: "typeMatrix",
                        tpl: Ext.create(
                          "Ext.XTemplate",
                          '<tpl for=".">',
                          '<div class="x-boundlist-item">{description} | {stateApproved}</div>',
                          "</tpl>"
                        ),
                        displayTpl: Ext.create(
                          "Ext.XTemplate",
                          '<tpl for=".">',
                          "{description} | {stateAssigned}",
                          "</tpl>"
                        ),
                        listeners: {
                          render: function(cbo) {
                            cbo.getStore().load();
                          }
                        }
                      },
                      {
                        xtype: "ViewComboFrequency",
                        fieldLabel: getName("WIRFrequency"),
                        allowBlank: blank("WIRFrequency"),
                        fieldCls: styleField("WIRFrequency"),
                        hidden: hidden("WIRFrequency"),
                        anchor: "100%",
                        msgTarget: "side",
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        name: "frequency",
                        itemId: "frequency",
                        listeners: {
                          expand: function(cbo) {
                            var idTypeMatrix = me
                              .down("#typeMatrix")
                              .getValue();
                            cbo.getStore().load({
                              params: {
                                idTypeMatrix: idTypeMatrix,
                                idOperationalRiskExposition: idMaxExpositionActive
                              }
                            });
                          },
                          select: function(cbo, record) {
                            var idTypeMatrix = me
                              .down("#typeMatrix")
                              .getValue();
                            riskInherent(me, idTypeMatrix);
                          }
                        }
                      },
                      {
                        xtype: "ViewComboImpact",
                        fieldLabel: getName("WIRImpact"),
                        allowBlank: blank("WIRImpact"),
                        fieldCls: styleField("WIRImpact"),
                        hidden: hidden("WIRImpact"),
                        anchor: "100%",
                        msgTarget: "side",
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        name: "impact",
                        itemId: "impact",
                        listeners: {
                          expand: function(cbo) {
                            var idTypeMatrix = me
                              .down("#typeMatrix")
                              .getValue();
                            cbo.getStore().load({
                              params: {
                                idTypeMatrix: idTypeMatrix,
                                idOperationalRiskExposition: idMaxExpositionActive
                              }
                            });
                          },
                          select: function(cbo, record) {
                            var idTypeMatrix = me
                              .down("#typeMatrix")
                              .getValue();
                            riskInherent(me, idTypeMatrix);
                          }
                        }
                      },
                      {
                        xtype: "combobox",
                        anchor: "100%",
                        fieldLabel: getName("WIREventOne"),
                        allowBlank: blank("WIREventOne"),
                        fieldCls: styleField("WIREventOne"),
                        hidden: hidden("WIREventOne"),
                        msgTarget: "side",
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        editable: false,
                        name: "eventOne",
                        itemId: "eventOne",
                        displayField: "description",
                        valueField: "idEventOne",

                        forceSelection: true,
                        store: {
                          fields: ["idEventOne", "description"],
                          proxy: {
                            actionMethods: {
                              create: "POST",
                              read: "POST",
                              update: "POST"
                            },
                            type: "ajax",
                            url:
                              "http://localhost:9000/giro/showListEventOneActives.htm",
                            extraParams: {
                              propertyOrder: "description"
                            },
                            reader: {
                              type: "json",
                              root: "data",
                              successProperty: "success"
                            }
                          }
                        }
                      },
                      {
                        xtype: "combobox",
                        editable: false,
                        anchor: "100%",
                        fieldLabel: getName("WIRTypeEventLost"),
                        hidden: hidden("WIRTypeEventLost"),
                        allowBlank: blank("WIRTypeEventLost"),
                        fieldCls: styleField("WIRTypeEventLost"),
                        name: "typeEventLost",
                        itemId: "typeEventLost",
                        queryMode: "local",
                        displayField: "description",
                        valueField: "value",
                        store: {
                          fields: ["value", "description"],
                          pageSize: 9999,
                          autoLoad: true,
                          proxy: {
                            actionMethods: {
                              create: "POST",
                              read: "POST",
                              update: "POST"
                            },
                            type: "ajax",
                            url:
                              "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                            extraParams: {
                              propertyOrder: "description",
                              propertyFind: "identified",
                              valueFind: "EVENT_LOSS_TYPE"
                            },
                            reader: {
                              type: "json",
                              root: "data",
                              successProperty: "success"
                            }
                          }
                        }
                      },
                      {
                        xtype: "combobox",
                        anchor: "100%",
                        editable: false,
                        hidden:
                          category !== DukeSource.global.GiroConstants.ANALYST,
                        fieldLabel: "Estado",
                        itemId: "stateIdentification",
                        name: "stateIdentification",
                        queryMode: "local",
                        value: "P",
                        displayField: "description",
                        valueField: "value",
                        store: {
                          fields: ["value", "description"],
                          pageSize: 9999,
                          autoLoad: true,
                          proxy: {
                            actionMethods: {
                              create: "POST",
                              read: "POST",
                              update: "POST"
                            },
                            type: "ajax",
                            url:
                              "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                            extraParams: {
                              propertyOrder: "description",
                              propertyFind: "identified",
                              valueFind: "IDENTIFICATION_STATE"
                            },
                            reader: {
                              type: "json",
                              root: "data",
                              successProperty: "success"
                            }
                          }
                        }
                      }
                    ]
                  },
                  {
                    xtype: "container",
                    flex: 1,
                    layout: "anchor",
                    items: [
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "factorRisk",
                        name: "factorRisk"
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        padding: "4 0 4 0",
                        hidden: hidden("WIRDescriptionFactorRisk"),
                        fieldLabel: "Factor de riesgo",
                        itemId: "descriptionFactorRisk",
                        name: "descriptionFactorRisk",
                        labelWidth: 125,
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeFactorRisk",
                                {
                                  winParent: me
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "textfield",
                        labelWidth: 110,
                        anchor: "100%",
                        fieldLabel: getName("WIRDescriptionScaleRisk"),
                        allowBlank: blank("WIRDescriptionScaleRisk"),
                        hidden: hidden("WIRDescriptionScaleRisk"),
                        name: "descriptionScaleRisk",
                        itemId: "descriptionScaleRisk",
                        listeners: {
                          afterrender: function(f) {
                            f.inputEl.setStyle("text-align", "center");
                          }
                        }
                      }
                    ]
                  }
                ]
              },
              {
                xtype: "textfield",
                itemId: "scaleRisk",
                name: "scaleRisk",
                hidden: true
              },
              {
                xtype: "textfield",
                itemId: "id",
                name: "id",
                value: "id",
                hidden: true
              },
              {
                xtype: "textfield",
                itemId: "matrix",
                name: "matrix",
                hidden: true
              },
              {
                xtype: "textfield",
                itemId: "risk",
                name: "risk",
                hidden: true
              },
              {
                xtype: "textfield",
                itemId: "typeMatrixScaleRisk",
                name: "typeMatrixScaleRisk",
                hidden: true
              }
            ]
          }
        ],
        buttons: [
          {
            text: "Guardar",
            scale: "medium",
            iconCls: "save",
            handler: function(btn) {
              var win = btn.up("window");
              var form = win.down("form");
              var factor = me.down("#factorRisk").getValue();

              if (form.getForm().isValid() && factor !== "") {
                DukeSource.lib.Ajax.request({
                  method: "POST",
                  url:
                    "http://localhost:9000/giro/saveIdentificationRisk.htm?nameView=ViewPanelGenericIdentifyRiskOperational",
                  params: {
                    jsonData: Ext.JSON.encode(form.getForm().getValues())
                  },
                  success: function(response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                      win.close();
                      innerGrid.down("pagingtoolbar").moveFirst();

                      DukeSource.global.DirtyView.messageNormal(
                        response.message
                      );
                    } else {
                      DukeSource.global.DirtyView.messageWarning(
                        response.message
                      );
                    }
                  },
                  failure: function(response) {
                    DukeSource.global.DirtyView.messageWarning(
                      response.message
                    );
                  }
                });
              } else {
                me.down("#descriptionFactorRisk").setFieldStyle("color:red");
                DukeSource.global.DirtyView.messageWarning(
                  DukeSource.global.GiroMessages.MESSAGE_COMPLETE
                );
              }
            }
          },
          {
            text: "Salir",
            scope: this,
            handler: this.close,
            scale: "medium",
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);

function riskInherent(me, typeMatrix) {
  var frequency = me.down("ViewComboFrequency").getValue();
  var impact = me.down("ViewComboImpact").getValue();
  if (frequency != null && impact != null) {
    Ext.Ajax.request({
      method: "POST",
      url: "http://localhost:9000/giro/findEvaluationRiskInMatrix.htm",
      params: {
        frequency: frequency,
        impact: impact,
        idTypeMatrix: typeMatrix
      },
      scope: this,
      success: function(response) {
        response = Ext.decode(response.responseText);
        if (response.success) {
          if (response.data["ghostIndicator"] === "S") {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              "EL RIESGO ES: " +
                response.data["descriptionScaleRisk"] +
                " - " +
                response.data["valueMatrix"] +
                " Seleccione otro",
              Ext.Msg.WARNING
            );
            me.down("#descriptionScaleRisk").reset();
            me.down("#descriptionScaleRisk").setFieldStyle(
              "background-color:  #d9ffdb; background-image: none;color:#000000;"
            );
          } else {
            me.down("#scaleRisk").setValue(response.data["idScaleRisk"]);
            me.down("#matrix").setValue(response.data["idMatrix"]);
            me.down("#typeMatrixScaleRisk").setValue(
              response.data["idBusinessLineOne"]
            );
            me.down("#descriptionScaleRisk").setValue(
              response.data["descriptionScaleRisk"]
            );
            me.down("#descriptionScaleRisk").setFieldStyle(
              "background-color: #" +
                response.data["color"] +
                "; background-image: none;color:#000000;"
            );
          }
        } else {
          DukeSource.global.DirtyView.messageAlert(
            DukeSource.global.GiroMessages.TITLE_WARNING,
            response.mensaje,
            Ext.Msg.ERROR
          );
        }
      },
      failure: function() {}
    });
  }
}
