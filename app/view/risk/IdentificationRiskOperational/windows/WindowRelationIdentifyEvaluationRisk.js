Ext.define("ModelRelationIdentifyRiskOperational", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "usernameRegister",
    "usernameAccept",
    "fullName",
    "consequence",
    "fail",
    "cause",
    "description",
    "state",
    "comment",
    "stateIdentification",
    {
      name: "dateEvaluation",
      type: "date",
      format: "d/m/Y",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y");
      }
    },
    "idRisk",
    "risk",
    "userRegister",
    "userAccept",
    "frequency",
    "descriptionFrequency",
    "impact",
    "descriptionImpact",
    "idMatrix",
    "matrix",
    "scaleRisk",
    "typeMatrix",
    "typeMatrixScaleRisk",
    "subProcess",
    "descriptionSubProcess",
    "idActivity",
    "descriptionProcess",
    "fullNameRegister",
    "fullNameAccept",
    "descriptionScaleRisk",
    "descriptionMatrix",
    "color",
    "valueMatrix",
    "descriptionTypeMatrix",
    "descriptionRisk"
  ]
});

var StoreRelationIdentifyRiskOperational = Ext.create("Ext.data.Store", {
  model: "ModelRelationIdentifyRiskOperational",
  groupField: "nameTypeRiskEvaluation",
  autoLoad: false,
  pageSize: 50,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.IdentificationRiskOperational.windows.WindowRelationIdentifyEvaluationRisk",
  {
    extend: "Ext.window.Window",
    alias: "widget.WindowRelationIdentifyEvaluationRisk",
    itemId: "WindowRelationIdentifyEvaluationRisk",
    requires: [
      "DukeSource.view.risk.util.search.AdvancedSearchIdentificationRisk"
    ],
    layout: "fit",
    border: false,
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "container",
            layout: "border",
            items: [
              {
                xtype: "AdvancedSearchIdentificationRisk",
                itemId: "AdvancedSearchIdentificationRisk",
                region: "west",
                flex: 0.6,
                tbar: [
                  {
                    text: "BUSCAR",
                    scale: "medium",
                    action: "searchAdvancedIdentificationEvaluation",
                    iconCls: "search"
                  },
                  {
                    text: "LIMPIAR",
                    scale: "medium",
                    iconCls: "clear",
                    handler: function(btn) {
                      btn
                        .up("form")
                        .getForm()
                        .reset();
                    }
                  }
                ]
              },
              {
                xtype: "gridpanel",
                itemId: "relationIdentifyEvaluationRisk",
                flex: 2,
                region: "center",
                title: "IDENTIFICACIÓN DE RIESGOS OPERACIONALES",
                titleAlign: "center",
                store: StoreRelationIdentifyRiskOperational,
                loadMask: true,
                columnLines: true,
                columns: [
                  { xtype: "rownumberer", width: 25, sortable: false },
                  {
                    header: "FECHA",
                    xtype: "datecolumn",
                    dataIndex: "dateEvaluation",
                    format: "d/m/Y",
                    width: 90,
                    renderer: Ext.util.Format.dateRenderer("d/m/Y")
                  },
                  {
                    header: "ESTADO",
                    dataIndex: "stateIdentification",
                    width: 80,
                    align: "center",
                    renderer: function(value, metaData, record) {
                      var stateIdentification =
                        value == "P"
                          ? "PENDIENTE"
                          : value == "R"
                          ? "REVISADO"
                          : "NO ACEPTADO";
                      var color =
                        value == "P"
                          ? "#f34d4d"
                          : value == "R"
                          ? "#39c739"
                          : "gray";
                      metaData.tdAttr =
                        'style="background-color: ' + color + ' !important;"';
                      return "<span>" + stateIdentification + "</span>";
                    }
                  },
                  {
                    header: "DESCRIPCIÓN DEL RIESGO ESPECÍFICO",
                    dataIndex: "description",
                    width: 300
                  },
                  { header: "FALLA", dataIndex: "fail", width: 200 },
                  { header: "CAUSAS ¿PORQUE?", dataIndex: "cause", width: 200 },
                  {
                    header: "CONSECUENCIA",
                    dataIndex: "consequence",
                    width: 200
                  },
                  {
                    header: "INHERENTE",
                    columns: [
                      {
                        header: "FRECUENCIA",
                        align: "center",
                        dataIndex: "descriptionFrequency",
                        width: 90
                      },
                      {
                        header: "IMPACTO",
                        align: "center",
                        dataIndex: "descriptionImpact",
                        width: 90
                      },
                      {
                        header: "RIESGO",
                        align: "center",
                        dataIndex: "descriptionMatrix",
                        width: 100,
                        renderer: function(value, metaData, record) {
                          metaData.tdAttr =
                            'style="background-color: #' +
                            record.get("color") +
                            ' !important;"';
                          return "<span>" + value + "</span>";
                        }
                      }
                    ]
                  },
                  {
                    header: "USUARIO REGISTRO",
                    dataIndex: "fullNameRegister",
                    width: 200
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: 20,
                  store: StoreRelationIdentifyRiskOperational,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  render: function() {
                    var grid = me.down("grid");
                    grid.store.proxy.url =
                      "http://localhost:9000/giro/showListIdentificationActives.htm?nameView=ViewPanelGenericIdentifyRiskOperational";
                    grid.store.proxy.params = {
                      userName: ""
                    };
                    grid.down("pagingtoolbar").moveFirst();
                  }
                }
              }
            ]
          }
        ],
        buttons: [
          {
            text: "AGREGAR",
            scale: "medium",
            iconCls: "add",
            handler: function(btn) {
              var win = btn.up("window");
              var grid = win.down("grid");
              var row = grid.getSelectionModel().getSelection()[0];
              if (row == undefined) {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_ADVERTENCIA,
                  DukeSource.global.GiroMessages.MESSAGE_ITEM,
                  Ext.Msg.WARNING
                );
              } else {
                row.raw.risk = me.idRisk;
                DukeSource.lib.Ajax.request({
                  method: "POST",
                  url:
                    "http://localhost:9000/giro/saveIdentificationRisk.htm?nameView=ViewPanelGenericIdentifyRiskOperational",
                  params: {
                    jsonData: Ext.JSON.encode(row.raw)
                  },
                  success: function(response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                      win.close();
                      me.grid.down("pagingtoolbar").moveFirst();

                      DukeSource.global.DirtyView.messageAlert(
                        DukeSource.global.GiroMessages.TITLE_MESSAGE,
                        response.message,
                        Ext.Msg.INFO
                      );
                    } else {
                      DukeSource.global.DirtyView.messageAlert(
                        DukeSource.global.GiroMessages.TITLE_ERROR,
                        response.message,
                        Ext.Msg.ERROR
                      );
                    }
                  },
                  failure: function(response) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_ERROR,
                      response.message,
                      Ext.Msg.ERROR
                    );
                  }
                });
              }
            }
          },
          {
            text: "SALIR",
            iconCls: "logout",
            scale: "medium",
            handler: function(btn) {
              me.close();
            }
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);
