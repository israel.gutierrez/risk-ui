Ext.define("ModelRiskAll", {
  extend: "Ext.data.Model",
  fields: [
    "codeRisk",
    "catalogRisk",
    "keyRiskIndicator",
    "descriptionRisk",
    "description",
    "descriptionProcess",
    "idRisk"
  ]
});

Ext.define("ModelRiskSelected", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "usernameRegister",
    "usernameAccept",
    "fullName",
    "consequence",
    "fail",
    "cause",
    "description",
    "state",
    "comment",
    "stateIdentification",
    {
      name: "dateEvaluation",
      type: "date",
      format: "d/m/Y",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y");
      }
    },
    "idRisk",
    "risk",
    "codeRisk",
    "userRegister",
    "userAccept",
    "frequency",
    "descriptionFrequency",
    "impact",
    "descriptionImpact",
    "idMatrix",
    "matrix",
    "scaleRisk",
    "typeMatrix",
    "typeMatrixScaleRisk",
    "subProcess",
    "descriptionSubProcess",
    "idActivity",
    "descriptionProcess",
    "fullNameRegister",
    "fullNameAccept",
    "descriptionScaleRisk",
    "descriptionMatrix",
    "color",
    "valueMatrix",
    "descriptionTypeMatrix",
    "descriptionRisk"
  ]
});
var storeRiskAll = Ext.create("Ext.data.Store", {
  model: "ModelRiskAll",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

var storeRiskSelected = Ext.create("Ext.data.Store", {
  model: "ModelRiskSelected",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    extraParams: {
      propertyFind: "description",
      valueFind: "",
      propertyOrder: "description"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.IdentificationRiskOperational.windows.WindowRelationRisk",
  {
    extend: "Ext.window.Window",
    alias: "widget.WindowRelationRisk",
    layout: {
      align: "stretch",
      type: "vbox"
    },
    height: 600,
    width: 1200,
    anchorSize: 100,
    titleAlign: "center",

    initComponent: function() {
      var idIdentification = this.idIdentification;
      var process = this.process;
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "container",
            padding: "2 2 2 2",
            layout: {
              align: "stretch",
              type: "hbox"
            },
            items: [
              {
                xtype: "AdvancedSearchRisk",
                title: "",
                flex: 0.5,
                padding: "2 0 2 2",
                collapsed: false,
                collapsible: false,
                buttonAlign: "center",
                buttons: [
                  {
                    text: "Buscar",
                    scale: "medium",
                    iconCls: "search",
                    handler: function() {
                      var process =
                        me.down("#process").getValue() == ""
                          ? ""
                          : "(" + me.down("#process").getValue() + ")";
                      var processType =
                        me.down("#processType").getValue() == undefined
                          ? ""
                          : me.down("#processType").getValue();
                      var fields =
                        "r.idRisk,r.codeRisk,pt.idProcessType,p.idProcess";
                      var values =
                        me.down("#idRisk").getValue() +
                        ";" +
                        me.down("#codeRisk").getValue() +
                        ";" +
                        processType +
                        ";" +
                        process;
                      var types = "Long,String,Long,Long";
                      var operator = "equal,equal,equal,multiple";
                      me.down("grid").store.getProxy().extraParams = {
                        fields: fields,
                        values: values,
                        types: types,
                        operators: operator,
                        condition: me.down("#conditionCombo").getValue(),
                        search: "full",
                        isLast: "true"
                      };
                      me.down("grid").store.getProxy().url =
                        "http://localhost:9000/giro/findMatchRisk.htm";
                      me.down("grid")
                        .down("pagingtoolbar")
                        .moveFirst();
                    }
                  },
                  {
                    text: "Limpiar",
                    scale: "medium",
                    iconCls: "clear",
                    handler: function() {
                      me.down("#processType").reset();
                      me.down("#process").reset();
                      me.down("#codeRisk").reset();
                      me.down("grid").store.getProxy().url =
                        "http://localhost:9000/giro/loadGridDefault.htm";
                      me.down("grid")
                        .down("pagingtoolbar")
                        .moveFirst();
                    }
                  }
                ]
              },
              {
                xtype: "container",
                flex: 1
              }
            ]
          },
          {
            xtype: "container",
            flex: 3,
            padding: 2,
            layout: {
              align: "stretch",
              type: "hbox"
            },
            items: [
              {
                xtype: "gridpanel",
                padding: "0 1 0 0",
                itemId: "gridRiskAll",
                store: storeRiskAll,
                loadMask: true,
                columnLines: true,
                flex: 1,
                title: "Riesgos disponibles",
                titleAlign: "center",
                multiSelect: true,
                columns: [
                  {
                    dataIndex: "idRisk",
                    width: 25,
                    align: "center",
                    text: "ID"
                  },
                  {
                    dataIndex: "codeRisk",
                    width: 60,
                    align: "center",
                    text: "Código"
                  },
                  {
                    dataIndex: "descriptionProcess",
                    flex: 3,
                    tdCls: "process-columnFree",
                    text: "Proceso",
                    renderer: function(value, metaData, record) {
                      var s = value.split(" &#8702; ");
                      var path = "";
                      for (var i = 0; i < s.length; i++) {
                        var text = s[i];
                        text = s[i] + "<br>";
                        path = path + " &#8702; " + text;
                      }
                      return path;
                    }
                  },
                  {
                    dataIndex: "description",
                    flex: 5,
                    text: "Nombre"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: 50,
                  store: storeRiskAll,
                  items: [
                    {
                      xtype:"UpperCaseTrigger",
                      width: 120,
                      action: "searchGridAllRole"
                    }
                  ]
                },
                listeners: {
                  itemdblclick: function(grid, record, item) {
                    var win = Ext.ComponentQuery.query("WindowRelationRisk")[0];
                    Ext.MessageBox.show({
                      title: DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      msg:
                        "Desea agregar el Riesgo " +
                        record.get("descriptionRisk") +
                        "?",
                      icon: Ext.Msg.QUESTION,
                      buttonText: {
                        yes: "Si"
                      },
                      buttons: Ext.MessageBox.YESNO,
                      fn: function(btn) {
                        if (btn === "yes") {
                          DukeSource.lib.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/saveRelationIdentificationRisk.htm?nameView=ViewPanelGenericIdentifyRiskOperational",
                            params: {
                              idRisk: record.data["idRisk"],
                              idIdentification: idIdentification
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              if (response.success) {
                                DukeSource.global.DirtyView.messageNormal(
                                  response.message
                                );
                                var grid2 = win.down("#gridRiskSelected");
                                grid2.store.getProxy().extraParams = {
                                  idRisk: record.data["idRisk"]
                                };
                                grid2.store.getProxy().url =
                                  "http://localhost:9000/giro/showListIdentificationActivesByRisk.htm?nameView=ViewPanelGenericIdentifyRiskOperational";
                                grid2.down("pagingtoolbar").moveFirst();
                              } else {
                                DukeSource.global.DirtyView.messageWarning(
                                  response.message
                                );
                              }
                            },
                            failure: function() {}
                          });
                        }
                      }
                    });
                  }
                }
              },
              {
                xtype: "gridpanel",
                padding: "0 0 0 1",
                itemId: "gridRiskSelected",
                store: storeRiskSelected,
                loadMask: true,
                columnLines: true,
                multiSelect: true,
                flex: 1,
                title: "Riesgo asignado",
                titleAlign: "center",
                columns: [
                  {
                    dataIndex: "codeRisk",
                    width: 60,
                    text: "Código"
                  },
                  {
                    dataIndex: "descriptionRisk",
                    flex: 5,
                    text: "Descripción"
                  },
                  {
                    dataIndex: "descriptionProcess",
                    flex: 3,
                    tdCls: "process-columnBusy",
                    text: "Proceso",
                    renderer: function(value, metaData, record) {
                      var s = value.split(" &#8702; ");
                      var path = "";
                      for (var i = 0; i < s.length; i++) {
                        var text = s[i];
                        text = s[i] + "<br>";
                        path = path + " &#8702; " + text;
                      }
                      return path;
                    }
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: 50,
                  store: storeRiskSelected
                },
                listeners: {
                  itemdblclick: function(grid, record, item) {
                    var win = Ext.ComponentQuery.query("WindowRelationRisk")[0];
                    Ext.MessageBox.show({
                      title: DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      msg:
                        "Desea eliminar el riesgo " +
                        record.get("nameCatalogRisk") +
                        "?",
                      icon: Ext.Msg.QUESTION,
                      buttonText: {
                        yes: "Si"
                      },
                      buttons: Ext.MessageBox.YESNO,
                      fn: function(btn) {
                        if (btn === "yes") {
                          Ext.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/deleteRelationIdentificationRisk.htm?nameView=ViewPanelGenericIdentifyRiskOperational",
                            params: {
                              idRisk: record.data["risk"],
                              idKri: idIdentification
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              if (response.success) {
                                DukeSource.global.DirtyView.messageNormal(
                                  response.message
                                );
                                var grid2 = win.down("#gridRiskSelected");
                                grid2.store.getProxy().extraParams = {
                                  idRisk: record.data["risk"]
                                };
                                grid2.store.getProxy().url =
                                  "http://localhost:9000/giro/showListIdentificationActivesByRisk.htm?nameView=ViewPanelGenericIdentifyRiskOperational";
                                grid2.down("pagingtoolbar").moveFirst();
                              } else {
                                DukeSource.global.DirtyView.messageWarning(
                                  response.message
                                );
                              }
                            },
                            failure: function() {}
                          });
                        }
                      }
                    });
                  }
                }
              }
            ]
          }
        ],
        buttons: [
          {
            text: "Salir",
            scale: "medium",
            iconCls: "logout",
            scope: this,
            handler: this.close
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
