Ext.define(
  "DukeSource.view.risk.IdentificationRiskOperational.windows.WindowEvaluationIdentificationRisk",
  {
    extend: "Ext.window.Window",
    alias: "widget.WindowEvaluationIdentificationRisk",
    requires: [
      "DukeSource.view.risk.parameter.combos.ViewComboTypeMatrix",
      "DukeSource.view.risk.parameter.combos.ViewComboProcessType",
      "DukeSource.view.risk.parameter.combos.ViewComboProcess",
      "DukeSource.view.risk.parameter.combos.ViewComboSubProcess",
      "DukeSource.view.risk.parameter.combos.ViewComboActivity",
      "DukeSource.view.risk.parameter.combos.ViewComboBusinessLineOne",
      "DukeSource.view.risk.parameter.combos.ViewComboBusinessLineTwo",
      "DukeSource.view.risk.parameter.combos.ViewComboBusinessLineThree",
      "DukeSource.view.risk.parameter.combos.ViewComboRiskType"
    ],
    width: 750,
    border: false,
    layout: {
      type: "fit"
    },
    title: "EVALUACIÓN DE RIESGOS",
    modal: true,
    initComponent: function() {
      var me = this;
      var link = this.link;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            items: [
              {
                xtype: "container",
                anchor: "100%",
                height: 27,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "ViewComboProcessType",
                    flex: 1,
                    fieldLabel: "MACROPROCESO",
                    allowBlank: false,
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    action: "selectProcessTypeRisk",
                    name: "processType",
                    listeners: {
                      render: function(cbo) {
                        cbo.getStore().load();
                      },
                      specialkey: function(f, e) {
                        DukeSource.global.DirtyView.focusEventEnter(
                          f,
                          e,
                          me.down("ViewComboProcess")
                        );
                      },
                      select: function(cbo) {
                        var win = cbo.up("window");
                        var comboProcess = win.down("ViewComboProcess");
                        var comboSubProcess = win.down("ViewComboSubProcess");
                        var comboActivity = win.down("ViewComboActivity");
                        comboProcess.getStore().load({
                          url:
                            "http://localhost:9000/giro/showListProcessActives.htm",
                          params: {
                            valueFind: cbo.getValue()
                          },
                          callback: function() {
                            comboProcess.reset();
                            comboSubProcess.reset();
                            comboActivity.reset();
                            comboSubProcess.setDisabled(true);
                            comboActivity.setDisabled(true);
                          }
                        });
                        comboProcess.setDisabled(false);
                      }
                    }
                  },
                  {
                    xtype: "ViewComboBusinessLineOne",
                    flex: 1,
                    labelWidth: 120,
                    padding: "0 0 0 10",
                    fieldLabel: "L&Iacute;NEA NEG. 1",
                    allowBlank: false,
                    readOnly: true,
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    action: "selectBusinessLineOneRisk",
                    name: "businessLineOne",
                    listeners: {
                      specialkey: function(f, e) {
                        DukeSource.global.DirtyView.focusEventEnter(
                          f,
                          e,
                          me.down("ViewComboBusinessLineTwo")
                        );
                      },
                      select: function(cbo) {
                        var win = cbo.up("window");
                        var comboBusinessLineTwo = win.down(
                          "ViewComboBusinessLineTwo"
                        );
                        var comboBusinessLineThree = win.down(
                          "ViewComboBusinessLineThree"
                        );
                        comboBusinessLineTwo.getStore().load({
                          url:
                            "http://localhost:9000/giro/showListBusinessLineTwoActivesComboBox.htm",
                          params: {
                            valueFind: cbo.getValue()
                          },
                          callback: function() {
                            comboBusinessLineTwo.reset();
                            comboBusinessLineThree.reset();
                          }
                        });
                        comboBusinessLineTwo.setDisabled(false);
                      }
                    }
                  }
                ]
              },
              {
                xtype: "container",
                anchor: "100%",
                height: 27,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "ViewComboProcess",
                    flex: 1,
                    fieldLabel: "PROCESO",
                    allowBlank: false,
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    disabled: true,
                    action: "selectProcessRisk",
                    name: "process",
                    listeners: {
                      specialkey: function(f, e) {
                        DukeSource.global.DirtyView.focusEventEnter(
                          f,
                          e,
                          me.down("ViewComboSubProcess")
                        );
                      },
                      select: function(cbo) {
                        var win = cbo.up("window");
                        var comboSubProcess = win.down("ViewComboSubProcess");
                        var comboActivity = win.down("ViewComboActivity");
                        comboSubProcess.getStore().load({
                          url:
                            "http://localhost:9000/giro/showListSubProcessActives.htm",
                          params: {
                            valueFind: cbo.getValue()
                          },
                          callback: function() {
                            comboSubProcess.reset();
                            comboActivity.reset();
                            comboActivity.setDisabled(true);
                          }
                        });
                        comboSubProcess.setDisabled(false);
                      }
                    }
                  },
                  {
                    xtype: "ViewComboBusinessLineTwo",
                    padding: "0 0 0 10",
                    flex: 1,
                    labelWidth: 120,
                    disabled: true,
                    allowBlank: false,
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    action: "selectBusinessLineTwoRisk",
                    fieldLabel: "L&Iacute;NEA NEG. 2",
                    name: "businessLineTwo",
                    listeners: {
                      specialkey: function(f, e) {
                        DukeSource.global.DirtyView.focusEventEnter(
                          f,
                          e,
                          me.down("ViewComboBusinessLineThree")
                        );
                      },
                      select: function(cbo) {
                        var win = cbo.up("window");
                        var comboBusinessLineThree = win.down(
                          "ViewComboBusinessLineThree"
                        );
                        comboBusinessLineThree.getStore().load({
                          url:
                            "http://localhost:9000/giro/showListBusinessLineThreeActivesComboBox.htm",
                          params: {
                            idBusinessLineOne: win
                              .down("ViewComboBusinessLineOne")
                              .getValue(),
                            idBusinessLineTwo: cbo.getValue()
                          },
                          callback: function(cbo) {
                            comboBusinessLineThree.reset();
                          }
                        });
                        comboBusinessLineThree.setDisabled(false);
                      }
                    }
                  }
                ]
              },
              {
                xtype: "container",
                anchor: "100%",
                height: 27,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "ViewComboSubProcess",
                    flex: 1,
                    fieldLabel: "SUB-PROCESO",
                    allowBlank: false,
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    disabled: true,
                    action: "selectSubProcessRisk",
                    name: "subProcess",
                    listeners: {
                      specialkey: function(f, e) {
                        DukeSource.global.DirtyView.focusEventEnter(
                          f,
                          e,
                          me.down("ViewComboActivity")
                        );
                      },
                      select: function(cbo) {
                        var win = cbo.up("window");
                        var comboProcess = win.down("ViewComboProcess");
                        var comboActivity = win.down("ViewComboActivity");
                        comboActivity.getStore().load({
                          url:
                            "http://localhost:9000/giro/showListActivityActives.htm",
                          params: {
                            valueFind: comboProcess.getValue(),
                            valueFind2: cbo.getValue()
                          },
                          callback: function() {
                            comboActivity.reset();
                          }
                        });
                        comboActivity.setDisabled(false);
                      }
                    }
                  },
                  {
                    xtype: "ViewComboBusinessLineThree",
                    flex: 1,
                    labelWidth: 120,
                    padding: "0 0 0 10",
                    fieldLabel: "LÍNEA NEG. 3",
                    disabled: true,
                    allowBlank: false,
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    name: "businessLineThree",
                    listeners: {
                      specialkey: function(f, e) {
                        DukeSource.global.DirtyView.focusEventEnter(
                          f,
                          e,
                          me.down("ViewComboRiskType")
                        );
                      }
                    }
                  }
                ]
              },
              {
                xtype: "container",
                height: 27,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "ViewComboActivity",
                    disabled: true,
                    flex: 1,
                    fieldLabel: "ACTIVIDAD",
                    name: "idActivity",
                    listeners: {
                      specialkey: function(f, e) {
                        DukeSource.global.DirtyView.focusEventEnter(
                          f,
                          e,
                          me.down("ViewComboBusinessLineOne")
                        );
                      }
                    }
                  },
                  {
                    xtype: "ViewComboRiskType",
                    flex: 1,
                    labelWidth: 120,
                    fieldLabel: "RIESGO RELACION",
                    padding: "0 0 0 10",
                    allowBlank: false,
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    name: "riskType",
                    listeners: {
                      specialkey: function(f, e) {
                        DukeSource.global.DirtyView.focusEventEnter(
                          f,
                          e,
                          me.down("button[action=saveRiskOperationalDetail]")
                        );
                      }
                    }
                  }
                ]
              },
              {
                xtype: "container",
                height: 27,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "ViewComboTypeMatrix",
                    flex: 1,
                    fieldLabel: "TIPO DE MATRIZ",
                    padding: "0 10 0 0",
                    allowBlank: false,
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    name: "typeMatrix",
                    tpl: Ext.create(
                      "Ext.XTemplate",
                      '<tpl for=".">',
                      '<div class="x-boundlist-item">{description} | {stateApproved}</div>',
                      "</tpl>"
                    ),
                    displayTpl: Ext.create(
                      "Ext.XTemplate",
                      '<tpl for=".">',
                      "{description} | {stateAssigned}",
                      "</tpl>"
                    ),
                    listeners: {
                      render: function(cbo) {
                        cbo.getStore().load();
                      },
                      select: function(cbo, records) {
                        if (records[0].get("isBusinessLine") == "S") {
                          me.down("ViewComboBusinessLineOne")
                            .getStore()
                            .load({
                              callback: function() {
                                me.down("ViewComboBusinessLineOne").setValue(
                                  records[0].get("idBusinessLineOne")
                                );
                                me.down("ViewComboBusinessLineOne").setReadOnly(
                                  true
                                );
                                me.down("ViewComboBusinessLineTwo")
                                  .getStore()
                                  .load({
                                    url:
                                      "http://localhost:9000/giro/showListBusinessLineTwoActivesComboBox.htm",
                                    params: {
                                      valueFind: records[0].get(
                                        "idBusinessLineOne"
                                      )
                                    },
                                    callback: function() {
                                      me.down(
                                        "ViewComboBusinessLineTwo"
                                      ).reset();
                                      me.down(
                                        "ViewComboBusinessLineThree"
                                      ).setDisabled(true);
                                      me.down(
                                        "ViewComboBusinessLineThree"
                                      ).reset();
                                    }
                                  });
                                me.down("ViewComboBusinessLineTwo").setDisabled(
                                  false
                                );
                              }
                            });
                        } else {
                          me.down("ViewComboBusinessLineOne").setReadOnly(
                            false
                          );
                        }
                      },
                      specialkey: function(f, e) {
                        DukeSource.global.DirtyView.focusEventEnter(
                          f,
                          e,
                          me.down("ViewComboProcessType")
                        );
                      }
                    }
                  },
                  {
                    xtype: "combobox",
                    flex: 1,
                    fieldLabel: "ÁMBITO EVALUACIÓN",
                    name: "idRiskEvaluationMatrix",
                    plugins: ["ComboSelectCount"],
                    itemId: "idRiskEvaluationMatrix",
                    fieldCls: "obligatoryTextField",
                    editable: false,
                    allowBlank: false,
                    labelWidth: 120,
                    displayField: "nameEvaluation",
                    valueField: "idRiskEvaluationMatrix",
                    fieldStyle: "text-transform:uppercase",
                    store: {
                      fields: ["idRiskEvaluationMatrix", "nameEvaluation"],
                      proxy: {
                        actionMethods: {
                          create: "POST",
                          read: "POST",
                          update: "POST"
                        },
                        type: "ajax",
                        url:
                          "http://localhost:9000/giro/showListRiskEvaluationMatrixActives.htm",
                        extraParams: {
                          propertyOrder: "nameEvaluation",
                          start: 0,
                          limit: 100
                        },
                        reader: {
                          type: "json",
                          root: "data",
                          successProperty: "success"
                        }
                      }
                    }
                  }
                ]
              },
              {
                xtype: "container",
                height: 65,
                anchor: "100%",
                layout: {
                  type: "hbox",
                  align: "middle"
                },
                items: [
                  {
                    xtype: "UpperCaseTextArea",
                    anchor: "100%",
                    flex: 1,
                    height: 55,
                    maxLengthText:
                      DukeSource.global.GiroMessages.MESSAGE_MAX_CHARACTER +
                      600,
                    fieldLabel: "RIESGO",
                    allowBlank: false,
                    fieldCls: "readOnlyText",
                    itemId: "descriptionRisk",
                    name: "descriptionRisk",
                    enforceMaxLength: true,
                    readOnly: true,
                    maxLength: 600
                  },
                  {
                    xtype: "button",
                    itemId: "buttonSearchRisk",
                    width: 67,
                    height: 55,
                    textAlign: "center",
                    text: "BUSCAR",
                    iconCls: "search",
                    handler: function() {
                      var windowRisk = Ext.create(
                        "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowSearchRisk",
                        {
                          modal: true
                        }
                      ).show();
                      windowRisk
                        .down("grid")
                        .on("itemdblclick", function(grid, record) {
                          me.down("#descriptionRisk").setValue(
                            record.get("description")
                          );
                          me.down("#idCatalogRisk").setValue(
                            record.get("idRisk")
                          );
                          me.down("#codeRisk").setValue(record.get("codeRisk"));
                          windowRisk.close();
                        });
                    }
                  }
                ]
              },
              {
                xtype: "UpperCaseTextArea",
                anchor: "100%",
                height: 55,
                maxLengthText:
                  DukeSource.global.GiroMessages.MESSAGE_MAX_CHARACTER + 300,
                fieldLabel: "COMENTARIO",
                name: "description",
                itemId: "description",
                enforceMaxLength: true,
                maxLength: 300
              },
              {
                xtype: "textfield",
                hidden: true,
                name: "idRisk",
                itemId: "idRisk"
              },
              {
                xtype: "textfield",
                hidden: true,
                name: "codeRisk",
                itemId: "codeRisk"
              },
              {
                xtype: "textfield",
                hidden: true,
                value: "S",
                name: "state",
                itemId: "state"
              },
              {
                xtype: "textfield",
                hidden: true,
                name: "idCatalogRisk",
                itemId: "idCatalogRisk"
              },
              {
                xtype: "textfield",
                hidden: true,
                itemId: "versionCorrelative",
                name: "versionCorrelative"
              },
              {
                xtype: "textfield",
                hidden: true,
                itemId: "idOperationalRiskExposition",
                name: "idOperationalRiskExposition",
                value: idMaxExpositionActive
              }
            ]
          }
        ],
        buttons: [
          {
            text: "GUARDAR",
            action: "saveEvaluationIdentificationRisk",
            link: link,
            scale: "medium",
            iconCls: "save"
          },
          {
            text: "SALIR",
            scale: "medium",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
