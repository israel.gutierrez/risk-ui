Ext.define("ModelGridDetailIdentifyRiskOperational", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "idIdentification",
    "usernameRegister",
    "usernameAccept",
    "fullName",
    "consequence",
    "fail",
    "cause",
    "description",
    "state",
    "comment",
    "stateIdentification",
    "dateEvaluation",
    "idRisk",
    "risk",
    "userRegister",
    "userAccept",
    "frequency",
    "descriptionFrequency",
    "impact",
    "descriptionImpact",
    "idMatrix",
    "matrix",
    "scaleRisk",
    "typeMatrix",
    "typeMatrixScaleRisk",
    "subProcess",
    "descriptionSubProcess",
    "idActivity",
    "descriptionProcess",
    "fullNameRegister",
    "fullNameAccept",
    "descriptionScaleRisk",
    "descriptionMatrix",
    "color",
    "valueMatrix",
    "descriptionTypeMatrix",
    "descriptionRisk"
  ]
});

var StoreGridDetailIdentifyRiskOperational = Ext.create("Ext.data.Store", {
  model: "ModelGridDetailIdentifyRiskOperational",
  groupField: "nameTypeRiskEvaluation",
  autoLoad: false,
  pageSize: 50,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
var sm = Ext.create("Ext.selection.CheckboxModel");

Ext.define(
  "DukeSource.view.risk.IdentificationRiskOperational.windows.ViewDetailIdentifyRiskOperational",
  {
    extend: "Ext.window.Window",
    title: "IDENTIFICACIÓN DE RIESGOS OPERACIONALES",
    titleAlign: "center",
    alias: "widget.ViewDetailIdentifyRiskOperational",
    layout: "fit",
    border: false,
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            itemId: "gridDetailIdentification",
            flex: 2,
            selModel: sm,
            region: "center",
            titleAlign: "center",
            store: StoreGridDetailIdentifyRiskOperational,
            loadMask: true,
            columnLines: true,
            tbar: [
              {
                text: "AGREGAR",
                iconCls: "add",
                scale: "medium",
                cls: "my-btn",
                overCls: "my-over",
                handler: function(button) {
                  var win = Ext.create(
                    "DukeSource.view.risk.IdentificationRiskOperational.windows.WindowRelationIdentifyEvaluationRisk",
                    {
                      modal: true,
                      height: 600,
                      width: 1200,
                      grid: me.down("grid"),
                      idRisk: me.idRisk
                    }
                  );
                  win.show();
                }
              },
              "-",
              {
                text: "MODIFICAR",
                iconCls: "modify",
                scale: "medium",
                cls: "my-btn",
                overCls: "my-over",
                handler: function(btn) {
                  var grid = btn.up("grid");
                  var record = grid.getSelectionModel().getSelection()[0];
                  if (record == undefined) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      DukeSource.global.GiroMessages.MESSAGE_ITEM,
                      Ext.Msg.WARNING
                    );
                  } else {
                    var win = Ext.create(
                      "DukeSource.view.risk.IdentificationRiskOperational.windows.ViewWindowIdentifyRiskOperational",
                      {
                        modal: true,
                        grid: me.down("grid")
                      }
                    ).show();
                    win
                      .down("form")
                      .getForm()
                      .loadRecord(record);
                    win
                      .down("#descriptionScaleRisk")
                      .setFieldStyle(
                        "background-color: #" +
                          record.get("color") +
                          "; background-image: none;color:#000000;"
                      );
                    win.down("#fail").focus(false, 100);
                  }
                }
              },
              "-",
              {
                text: "ELIMINAR",
                iconCls: "delete",
                scale: "medium",
                cls: "my-btn",
                overCls: "my-over",
                handler: function(btn) {
                  var grid = me.down("grid");
                  var row = grid.getSelectionModel().getSelection()[0];
                  if (row != undefined) {
                    Ext.MessageBox.show({
                      title: DukeSource.global.GiroMessages.TITLE_WARNING,
                      msg:
                        "Estas seguro que desea eliminar esta identificación?",
                      icon: Ext.Msg.WARNING,
                      buttonText: {
                        yes: "Si",
                        no: "No"
                      },
                      buttons: Ext.MessageBox.YESNO,
                      fn: function(btn) {
                        if (btn == "yes") {
                          DukeSource.lib.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/deleteEvaluationIdentificationRisk.htm?nameView=ViewPanelGenericIdentifyRiskOperational",
                            params: {
                              jsonData: Ext.JSON.encode(row.raw)
                            },
                            success: function(response) {
                              var response = Ext.decode(response.responseText);
                              if (response.success) {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_MESSAGE,
                                  response.message,
                                  Ext.Msg.INFO
                                );
                                grid.down("pagingtoolbar").moveFirst();
                              } else {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_WARNING,
                                  response.message,
                                  Ext.Msg.WARNING
                                );
                              }
                            },
                            failure: function() {}
                          });
                        } else {
                        }
                      }
                    });
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_ADVERTENCIA,
                      DukeSource.global.GiroMessages.MESSAGE_ITEM,
                      Ext.Msg.WARNING
                    );
                  }
                }
              },
              "-",
              {
                text: "NO ACEPTAR",
                iconCls: "clear",
                scale: "medium",
                cls: "my-btn",
                overCls: "my-over",
                handler: function(btn) {
                  var grid = me.down("grid");
                  var row = grid.getSelectionModel().getSelection()[0];
                  if (row != undefined) {
                    Ext.MessageBox.show({
                      title: DukeSource.global.GiroMessages.TITLE_WARNING,
                      msg:
                        "Estas seguro que NO desea ACEPTAR esta identificación?",
                      icon: Ext.Msg.WARNING,
                      buttonText: {
                        yes: "Si",
                        no: "No"
                      },
                      buttons: Ext.MessageBox.YESNO,
                      fn: function(btn) {
                        if (btn == "yes") {
                          row.raw.stateIdentification = "A";

                          DukeSource.lib.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/saveIdentificationRisk.htm?nameView=ViewPanelGenericIdentifyRiskOperational",
                            params: {
                              jsonData: Ext.JSON.encode(row.raw)
                            },
                            success: function(response) {
                              var response = Ext.decode(response.responseText);
                              if (response.success) {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_MESSAGE,
                                  response.message,
                                  Ext.Msg.INFO
                                );
                                grid.down("pagingtoolbar").moveFirst();
                              } else {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_WARNING,
                                  response.message,
                                  Ext.Msg.WARNING
                                );
                              }
                            },
                            failure: function() {}
                          });
                        } else {
                        }
                      }
                    });
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_ADVERTENCIA,
                      DukeSource.global.GiroMessages.MESSAGE_ITEM,
                      Ext.Msg.WARNING
                    );
                  }
                }
              }
            ],
            columns: [
              { xtype: "rownumberer", width: 25, sortable: false },
              { header: "FECHA", dataIndex: "dateEvaluation", width: 90 },
              {
                header: "ESTADO",
                dataIndex: "stateIdentification",
                width: 80,
                align: "center",
                renderer: function(value, metaData, record) {
                  var stateIdentification =
                    value == "P"
                      ? "PENDIENTE"
                      : value == "R"
                      ? "REVISADO"
                      : "NO ACEPTADO";
                  var color =
                    value == "P"
                      ? "#f34d4d"
                      : value == "R"
                      ? "#39c739"
                      : "gray";
                  metaData.tdAttr =
                    'style="background-color: ' + color + ' !important;"';
                  return "<span>" + stateIdentification + "</span>";
                }
              },
              {
                header: "Descripción del riesgo",
                dataIndex: "description",
                width: 400
              },
              { header: "Falla", dataIndex: "fail", width: 300 },
              { header: "Causas ¿porque?", dataIndex: "cause", width: 300 },
              { header: "Consecuencia", dataIndex: "consequence", width: 300 },
              {
                header: "Inherente",
                columns: [
                  {
                    header: "Frecuencia",
                    align: "center",
                    dataIndex: "descriptionFrequency",
                    width: 90
                  },
                  {
                    header: "Impacto",
                    align: "center",
                    dataIndex: "descriptionImpact",
                    width: 90
                  },
                  {
                    header: "Riesgo",
                    align: "center",
                    dataIndex: "descriptionScaleRisk",
                    width: 100,
                    renderer: function(value, metaData, record) {
                      metaData.tdAttr =
                        'style="background-color: #' +
                        record.get("color") +
                        ' !important;"';
                      return "<span>" + value + "</span>";
                    }
                  }
                ]
              },
              {
                header: "Usuario que registró",
                dataIndex: "fullNameRegister",
                width: 200
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: 20,
              store: StoreGridDetailIdentifyRiskOperational,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            }
          }
        ],
        buttons: [
          {
            text: "SALIR",
            scope: this,
            handler: this.close,
            scale: "medium",
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);
