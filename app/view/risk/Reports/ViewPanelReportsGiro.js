function settingValuesButtonReport(me, record) {
  me.down("#MainReport").setTitle(record.data["text"].toUpperCase());
  me.down("#excelButton").nameReport = record.raw.nameReport;
}

Ext.define("DukeSource.view.risk.Reports.ViewPanelReportsGiro", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelReportsGiro",
  border: false,
  layout: "border",

  initComponent: function() {
    var me = this;
    this.items = [
      {
        xtype: "panel",
        title: "Reportes GIRO",
        region: "west",
        width: 250,
        height: 300,
        layout: "accordion",
        margins: "2",
        items: [
          {
            xtype: "treepanel",
            title: "Incidentes/Eventos",
            rootVisible: false,
            hidden: true,
            root: {
              expanded: true,
              children: [
                {
                  text: "Resumen de Cumplimiento",
                  leaf: true,
                  nameReport: "ROXXX0007XLS"
                },
                {
                  text: "Eventos de Pérdida",
                  leaf: true,
                  nameReport: "ROXXX0014XLS"
                }
              ]
            },
            listeners: {
              itemclick: function(view, record, item, index, e) {
                addMainPanel(me);
                settingValuesButtonReport(me, record);
                addDateFields(me.down("#formParameters"));
                if (record.raw["nameReport"] == "ROXXX0014XLS") {
                  me.down("#formParameters")
                    .down("checkbox")
                    .setVisible(true);
                  me.down("#formParameters")
                    .down("checkbox")
                    .setValue(true);
                }
              }
            }
          },
          {
            xtype: "treepanel",
            title: "Evaluación de Riesgos",
            rootVisible: false,
            root: {
              expanded: true,
              children: [
                {
                  text: "Dashboard de la gestión de RO",
                  leaf: true,
                  nameReport: "ROXXX0001XLS"
                },
                {
                  text: "Resumen de resultados de RO",
                  leaf: true,
                  nameReport: "ROXXX0002XLS"
                },
                {
                  text: "Matriz de Riesgo Operacional",
                  leaf: true,
                  nameReport: "ROXXX0018XLS"
                },
                {
                  text: "Listado de Riesgos Para la Matriz",
                  leaf: true,
                  nameReport: "ROXXX0019XLS"
                },
                {
                  text: "Resumen de la Autoevaluación de RO",
                  leaf: true,
                  nameReport: "ROXXX0003XLS"
                },
                {
                  text: "Riesgos Inherentes",
                  leaf: true,
                  nameReport: "ROXXX0004XLS"
                },
                {
                  text: "Riesgos Residuales",
                  leaf: true,
                  nameReport: "ROXXX0005XLS"
                }
              ]
            },
            listeners: {
              itemclick: function(grid, record, item, index, e) {
                if (index == 2) {
                  addMainPanel(me);
                  settingValuesButtonReport(me, record);
                  //addProcessAndEvaluation(me.down('#formParameters'));
                  me.down("#formParameters").add(
                    {
                      xtype: "ViewComboOperationalRiskExposition",
                      fieldLabel: "MAXIMA EXPOSICION",
                      anchor: "100%",
                      allowBlank: false,
                      queryMode: "remote",
                      labelWidth: 130
                    },
                    {
                      xtype: "ViewComboTypeMatrix",
                      fieldLabel: "TIPO DE MATRIZ",
                      queryMode: "remote",
                      allowBlank: false,
                      anchor: "100%",
                      msgTarget: "side",
                      fieldCls: "obligatoryTextField",
                      name: "typeMatrix",
                      labelWidth: 130
                    }
                  );
                  Ext.ComponentQuery.query("ViewPanelReportsGiro")[0]
                    .down("#optionButtonReport")
                    .menu.add({
                      text: "MATRIZ",
                      action: "reportGiroXls",
                      itemId: "matrixButton",
                      nameReport: "ROXXX0020XLS",
                      typeReport: "xls",
                      iconCls: "excel"
                    });
                  Ext.ComponentQuery.query("ViewPanelReportsGiro")[0].down(
                    "#matrixButton"
                  ).nameReport = "ROXXX0020XLS";
                  Ext.ComponentQuery.query("ViewPanelReportsGiro")[0].down(
                    "#pdfButton"
                  ).nameReport = record.raw.nameReport;
                } else {
                  addMainPanel(me);
                  settingValuesButtonReport(me, record);
                  if (record.raw["nameReport"] == "ROXXX0001XLS") {
                    addDateFields(me.down("#formParameters"));
                  } else {
                    addProcessAndEvaluation(me.down("#formParameters"));
                  }
                }
              }
            }
          },
          {
            xtype: "treepanel",
            title: "Planes de Acción",
            rootVisible: false,
            root: {
              expanded: true,
              children: [
                {
                  text: "Resumen de Planes de Acción",
                  leaf: true,
                  nameReport: "ROXXX0010XLS"
                },
                {
                  text: "Grado de Avance de Planes de acción",
                  leaf: true,
                  nameReport: "ROXXX0011XLS"
                },
                {
                  text: "Seguimiento de recomendaciones",
                  leaf: true,
                  nameReport: "ROXXX0012XLS"
                },
                {
                  text: "Cuadro de seguimiento de acuerdos de RO",
                  leaf: true,
                  nameReport: "ROXXX0013XLS"
                },
                {
                  text: "Seguimiento de controles",
                  leaf: true,
                  nameReport: "ROCTRL0002XLS"
                }
              ]
            },
            listeners: {
              itemclick: function(grid, record, item, index, e) {
                addMainPanel(me);
                settingValuesButtonReport(me, record);
                addProcessAndEvaluation(me.down("#formParameters"));
              }
            }
          },
          {
            xtype: "treepanel",
            title: "Indicadores Claves de Riesgo",
            rootVisible: false,
            root: {
              expanded: true,
              children: [
                {
                  text: "Formulario de diseño de KRI",
                  leaf: true,
                  nameReport: "ROXXX0006XLS"
                },
                {
                  text: "Tablero de Riesgos - KRI",
                  leaf: true,
                  nameReport: "ROXXX0016XLS"
                },
                {
                  text: "Tablero de KRI - Riesgos",
                  leaf: true,
                  nameReport: "ROXXX0015XLS"
                }
              ]
            },
            listeners: {
              itemclick: function(grid, record, item, index, e) {
                addMainPanel(me);
                settingValuesButtonReport(me, record);
                addFieldsKri(me.down("#formParameters"), record);
                if (record.raw["nameReport"] == "ROXXX0006XLS") {
                  Ext.ComponentQuery.query("ViewPanelReportsGiro")[0]
                    .down("#optionButtonReport")
                    .menu.add(
                      {
                        text: "WORD",
                        action: "reportGiroXls",
                        itemId: "wordButton",
                        nameReport: "",
                        typeReport: "word",
                        iconCls: "word"
                      },
                      {
                        text: "PDF",
                        action: "reportGiroXls",
                        itemId: "pdfButton",
                        nameReport: "",
                        typeReport: "pdf",
                        iconCls: "pdf"
                      }
                    );
                  Ext.ComponentQuery.query("ViewPanelReportsGiro")[0].down(
                    "#wordButton"
                  ).nameReport = record.raw.nameReport;
                  Ext.ComponentQuery.query("ViewPanelReportsGiro")[0].down(
                    "#pdfButton"
                  ).nameReport = record.raw.nameReport;
                }
              }
            }
          }
        ]
      }
    ];
    this.callParent();
  }
});

function addDateFields(view) {
  view.removeAll();
  view.add(
    {
      xtype: "checkboxfield",
      fieldLabel: "Tipo de fecha",
      name: "typeDate",
      boxLabel: "Ocurrencia",
      checked: false,
      hidden: true,
      inputValue: "S",
      uncheckedValue: "N",
      listeners: {
        change: function(me, value) {
          if (value) {
            me.getEl()
              .down("label.x-form-cb-label")
              .update("Ocurrencia");
          } else {
            me.getEl()
              .down("label.x-form-cb-label")
              .update("Reporte");
          }
        }
      }
    },
    {
      xtype: "datefield",
      itemId: "dateInit",
      name: "dateInit",
      format: "Y-m-d",
      anchor: "100%",
      fieldLabel: "Fecha de Inicio"
    },
    {
      xtype: "datefield",
      itemId: "dateEnd",
      name: "dateEnd",
      format: "Y-m-d",
      anchor: "100%",
      fieldLabel: "Fecha Fin"
    }
  );
}

function addProcessAndEvaluation(view) {
  view.removeAll();
  view.add(
    {
      xtype: "combobox",
      queryMode: "local",
      editable: false,
      msgTarget: "side",
      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
      anchor: "100%",
      fieldLabel: "EVALUACIÓN",
      name: "idRiskEvaluationMatrix",
      itemId: "idRiskEvaluationMatrix",
      flex: 1,
      displayField: "nameEvaluation",
      valueField: "idRiskEvaluationMatrix",
      fieldStyle: "text-transform:uppercase",
      store: {
        fields: ["idRiskEvaluationMatrix", "nameEvaluation"],
        proxy: {
          actionMethods: {
            create: "POST",
            read: "POST",
            update: "POST"
          },
          type: "ajax",
          url:
            "http://localhost:9000/giro/showListRiskEvaluationMatrixActives.htm",
          extraParams: {
            propertyOrder: "description"
          },
          reader: {
            type: "json",
            root: "data",
            successProperty: "success"
          }
        }
      },
      listeners: {
        render: function(cbo) {
          cbo.getStore().load({
            callback: function() {
              cbo.store.add({
                idRiskEvaluationMatrix: "T",
                nameEvaluation: "TODOS"
              });
              cbo.setValue("T");
            }
          });
        }
      }
    },
    {
      xtype: "ViewComboProcessType",
      anchor: "100%",
      fieldLabel: "MACRO PROCESO",
      name: "processType",
      itemId: "processType",
      forceSelection: false,
      listeners: {
        render: function(cbo) {
          cbo.getStore().load();
        }
      }
    }
  );
}

function addFieldsKri(view, record) {
  view.removeAll();

  view.add(
    {
      xtype: "ViewComboProcessType",
      anchor: "100%",
      queryMode: "local",
      fieldLabel: "MacroProceso",
      emptyText: "Seleccionar",
      name: "processType",
      itemId: "processType",
      editable: false,
      forceSelection: false,
      listeners: {
        render: function(cbo) {
          cbo.getStore().load({
            callback: function() {
              cbo.store.add({
                idProcessType: "T",
                description: "TODOS"
              });
            }
          });
        },
        select: function(cbo) {
          view
            .down("#kri")
            .getStore()
            .load({
              url:
                "http://localhost:9000/giro/showListKeyRiskIndicatorActives.htm",
              params: {
                propertyOrder: "key.description",
                typeProcess: cbo.getValue()
              },
              callback: function() {
                view.down("#kri").reset();
              }
            });
        }
      }
    },
    {
      xtype: "ViewComboKeyRiskIndicator",
      anchor: "100%",
      fieldLabel: "KRI",
      name: "kri",
      itemId: "kri",
      emptyText: "Seleccionar",
      forceSelection: false
    },
    {
      xtype: "ViewComboYear",
      hidden: record.raw["nameReport"] == "ROXXX0006XLS",
      fieldLabel: "A&ntilde;o",
      anchor: "100%"
    }
  );
}

function addMainPanel(view) {
  if (view.down("#MainReport") != undefined) view.down("#MainReport").destroy();
  view.add({
    xtype: "panel",
    region: "center",
    title: "Reportes GIRO",
    itemId: "MainReport",
    bodyPadding: 5,
    layout: {
      type: "vbox",
      align: "stretch"
    },
    margins: "2",
    items: [
      {
        xtype: "container",
        layout: "hbox",
        items: [
          {
            xtype: "container",
            flex: 1,
            layout: {
              type: "vbox",
              align: "stretch"
            },
            items: [
              {
                xtype: "form",
                padding: 5,
                itemId: "formParameters",
                flex: 1,
                border: false,
                items: []
              },
              {
                xtype: "container",
                height: 60,
                layout: {
                  align: "stretch",
                  pack: "end",
                  padding: 10,
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "splitbutton",
                    scale: "medium",
                    itemId: "optionButtonReport",
                    text: "GENERAR REPORTE",
                    iconCls: "attachFile",
                    menu: {
                      width: 150,
                      items: [
                        {
                          text: "EXCEL",
                          action: "reportGiroXls",
                          itemId: "excelButton",
                          nameReport: "",
                          typeReport: "xls",
                          iconCls: "excel"
                        }
                        //,
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            xtype: "container",
            flex: 1
          }
        ]
      },
      {
        xtype: "panel",
        padding: "2 2 2 0",
        flex: 4,
        title: "REPORTE",
        name: "panelReport",
        layout: "fit",
        itemId: "panelReport"
      }
    ]
  });
}
