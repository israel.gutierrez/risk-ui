Ext.define("ModelGridMatrixReport", {
  extend: "Ext.data.Model",
  fields: ["cell1", "cell2", "cell3", "cell4", "cell5"]
});

var StoreGridMatrix = Ext.create("Ext.data.Store", {
  model: "ModelGridMatrixReport",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.Reports.ViewPanelMatrixReport", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelMatrixReport",
  height: 734,
  width: 1167,
  title: "",

  layout: {
    type: "vbox",
    align: "stretch",
    padding: ""
  },
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          flex: 0.28,
          margins: "0",
          height: 154,
          border: false,
          margin: 0,
          layout: {
            type: "hbox",
            align: "stretch"
          },
          items: [
            {
              xtype: "container",
              flex: 0
            },
            {
              xtype: "fieldset",
              flex: 1,
              border: false,
              padding: "10 10 0 10",
              title: "CONSTRUCCION MATRIZ",
              items: [
                {
                  xtype: "ViewComboOperationalRiskExposition",
                  fieldLabel: "MAXIMA EXPOSICION",
                  anchor: "100%",
                  allowBlank: false,
                  queryMode: "remote",
                  labelWidth: 130
                },
                {
                  xtype: "ViewComboTypeMatrix",
                  fieldLabel: "TIPO DE MATRIZ",
                  queryMode: "remote",
                  allowBlank: false,
                  anchor: "100%",
                  msgTarget: "side",
                  fieldCls: "obligatoryTextField",
                  name: "typeMatrix",
                  labelWidth: 130,
                  listeners: {
                    select: function(cbo, record) {
                      var panel = cbo.up("ViewPanelMatrixReport");
                      var grid2 = panel.down("grid[name=matrixGeneral]");
                      Ext.Ajax.request({
                        method: "POST",
                        url: "http://localhost:9000/giro/buildHeaderAxisX.htm",
                        params: {
                          valueFind: panel
                            .down("ViewComboOperationalRiskExposition")
                            .getValue(),
                          propertyOrder: "equivalentValue",
                          idTypeMatrix: cbo.getValue()
                        },
                        success: function(response) {
                          response = Ext.decode(response.responseText);
                          if (response.success) {
                            var columns = Ext.JSON.decode(response.data);
                            var fields = Ext.JSON.decode(response.fields);
                            DukeSource.global.DirtyView.createMatrix(
                              columns,
                              fields,
                              grid2
                            );
                          } else {
                            DukeSource.global.DirtyView.messageAlert(
                              DukeSource.global.GiroMessages.TITLE_ERROR,
                              response.mensaje,
                              Ext.Msg.ERROR
                            );
                            grid2.getView().refresh();
                          }
                        },
                        failure: function() {}
                      });
                      grid2.store.getProxy().extraParams = {
                        idOperationalRiskExposition: panel
                          .down("ViewComboOperationalRiskExposition")
                          .getValue(),
                        idTypeMatrix: cbo.getValue()
                      };
                      grid2.store.getProxy().url =
                        "http://localhost:9000/giro/findMatrixAndLoad.htm";
                      grid2.down("pagingtoolbar").moveFirst();
                    }
                  }
                }
              ]
            },
            {
              xtype: "fieldset",
              flex: 1,
              border: false,
              height: 150,
              padding: "10 10 0 10",
              width: 485,
              title: "BUSQUEDA DE RIESGOS",
              items: [
                {
                  xtype: "combobox",
                  queryMode: "local",
                  editable: false,
                  msgTarget: "side",
                  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  anchor: "100%",
                  fieldLabel: "Evaluacion",
                  name: "idRiskEvaluationMatrix",
                  itemId: "idRiskEvaluationMatrix",
                  labelWidth: 130,
                  displayField: "nameEvaluation",
                  valueField: "idRiskEvaluationMatrix",
                  fieldStyle: "text-transform:uppercase",
                  store: {
                    fields: ["idRiskEvaluationMatrix", "nameEvaluation"],
                    proxy: {
                      actionMethods: {
                        create: "POST",
                        read: "POST",
                        update: "POST"
                      },
                      type: "ajax",
                      url:
                        "http://localhost:9000/giro/showListRiskEvaluationMatrixActives.htm",
                      extraParams: {
                        propertyOrder: "description"
                      },
                      reader: {
                        type: "json",
                        root: "data",
                        successProperty: "success"
                      }
                    }
                  },
                  listeners: {
                    render: function(cbo) {
                      cbo.getStore().load();
                    }
                  }
                },
                {
                  xtype: "ViewComboProcessType",
                  queryMode: "remote",
                  anchor: "100%",
                  fieldLabel: "MacroProceso",
                  labelWidth: 130
                },
                {
                  xtype: "container",
                  layout: {
                    type: "hbox",
                    align: "middle"
                  },
                  items: [
                    {
                      xtype: "button",
                      scale: "medium",
                      iconCls: "search",
                      action: "identifiedFullRiskInMatrix",
                      text: "BUSCAR RIESGOS",
                      margin: "0 5 0 0"
                    },
                    {
                      xtype: "button",
                      scale: "medium",
                      iconCls: "clear",
                      text: "LIMPIAR",
                      handler: function(btn) {
                        me.down("ViewComboProcessType").reset();
                        me.down("#idRiskEvaluationMatrix").reset();
                        me.down("grid")
                          .getView()
                          .refresh();
                      }
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          xtype: "container",
          flex: 1,
          title: "MATRIZ DE RIESGOS",
          layout: {
            type: "hbox",
            align: "stretch"
          },
          items: [
            {
              xtype: "gridpanel",
              padding: "2 2 2 2",
              name: "matrixGeneral",
              store: StoreGridMatrix,
              flex: 1,
              loadMask: true,
              columnLines: true,
              columns: [],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: StoreGridMatrix,
                displayInfo: true,
                items: [
                  {
                    iconCls: "print",
                    handler: function() {
                      DukeSource.global.DirtyView.printElementTogrid(
                        Ext.ComponentQuery.query(
                          "ViewPanelMatrixReport"
                        )[0].down("grid")
                      );
                    }
                  },
                  {
                    text: "RIESGO INHERENTE"
                  },
                  {
                    xtype: "button",
                    cls: "buttonRiskInherent"
                  },
                  "--",
                  {
                    text: "RIESGO RESIDUAL"
                  },
                  {
                    xtype: "button",
                    cls: "buttonRiskResidual",
                    text: ' <a style="color: #0c76cb;font-size:14px">X</a>'
                  }
                ],
                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              },
              listeners: {
                render: function() {
                  var grid = this;
                  DukeSource.global.DirtyView.loadGridDefault(grid);
                }
              }
            }
          ]
        }
      ]
    });

    me.callParent(arguments);
  }
});
