Ext.define('DukeSource.view.risk.Reports.PanelReportsASFI', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.PanelReportsASFI',
    border: false,
    layout: 'border',
    requires: [
        'DukeSource.view.risk.util.ViewComboYesNo'
    ],

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'panel',
                    title: 'GENERAR REPORTE',
                    region: 'center',
                    margins: '2',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'panel',
                            flex: 1,
                            margins: '2',
                            items: [
                                {
                                    xtype: 'form',
                                    border: false,
                                    bodyPadding: 5,
                                    fieldDefaults: {
                                        labelCls: 'changeSizeFontToEightPt',
                                        fieldCls: 'changeSizeFontToEightPt'
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            name: 'nameReport',
                                            itemId: 'nameReport',
                                            hidden: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            name: 'year',
                                            itemId: 'year',
                                            hidden: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            name: 'month',
                                            itemId: 'month',
                                            hidden: true
                                        }, {
                                            xtype: 'textfield',
                                            name: 'lastYear',
                                            itemId: 'lastYear',
                                            hidden: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            name: 'lastMonth',
                                            itemId: 'lastMonth',
                                            hidden: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'Nombre archivo',
                                            allowBlank: false,
                                            fieldCls: 'readOnlyText',
                                            readOnly: true,
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            anchor: '100%',
                                            name: 'nameFile',
                                            itemId: 'nameFile'
                                        },
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: 'Periodo',
                                            allowBlank: false,
                                            fieldCls: 'fieldBlue',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            format: 'Ymd',
                                            editable: false,
                                            anchor: '100%',
                                            name: 'period',
                                            itemId: 'period',
                                            buildNameFile: function (field) {
                                                var month = parseFloat(Ext.Date.format(field.getValue(), 'm'));
                                                var year = parseFloat(Ext.Date.format(field.getValue(), 'Y'));
                                                var quarter = parseInt(month / 3);

                                                var lastYear = (quarter < 2) ? year - 1 : year;
                                                var lastMonth = (quarter < 2) ? 12 : (quarter - 1) * 3;

                                                var name = me.down('#nameFile').getValue();

                                                me.down('#nameFile').setValue(name.replace(name.substring(2, 10), field.getRawValue()));
                                                me.down('#dateReport').setValue(field.getValue());
                                                me.down('#year').setValue(year);
                                                me.down('#month').setValue(month);
                                                me.down('#lastYear').setValue(lastYear);
                                                me.down('#lastMonth').setValue(lastMonth);
                                            },
                                            isLastDay: function (field) {
                                                var daySelect = parseFloat(Ext.Date.format(field.getValue(), 'j'));
                                                var lastDay = Ext.Date.format(Ext.Date.getLastDateOfMonth(field.getValue()), 'j');

                                                return daySelect === parseFloat(lastDay);
                                            },
                                            setValuesToReport: function (field) {
                                                var month = parseFloat(Ext.Date.format(field.getValue(), 'm'));
                                                if (((month % 3) === 0) && field.isLastDay(field)) {
                                                    field.buildNameFile(field);
                                                }
                                                else {
                                                    DukeSource.global.DirtyView.YesNoWarning('No es el fin de un trimestre, desea continuar?');
                                                    field.buildNameFile(field);
                                                }
                                            }, listeners: {
                                                select: function (field) {
                                                    field.setValuesToReport(field);
                                                },
                                                blur: function (field) {
                                                    field.setValuesToReport(field);
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'datefield',
                                            hidden: true,
                                            format: 'd/m/Y',
                                            anchor: '100%',
                                            name: 'dateReport',
                                            itemId: 'dateReport'
                                        },
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: 'F. de reporte desde',
                                            format: 'd/m/Y',
                                            anchor: '100%',
                                            itemId: 'dateInit',
                                            name: 'dateInit'
                                        },
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: 'F. de reporte hasta',
                                            format: 'd/m/Y',
                                            anchor: '100%',
                                            name: 'dateEnd',
                                            itemId: 'dateEnd'
                                        },
                                        {
                                            xtype: 'combobox',
                                            fieldLabel: 'Estado',
                                            editable: false,
                                            name: 'eventState',
                                            anchor: '100%',
                                            itemId: 'eventState',
                                            displayField: 'description',
                                            hidden: true,
                                            valueField: 'id',
                                            multiSelect: true,
                                            allowBlank: true,
                                            store: {
                                                fields: ['id', 'description', 'sequence'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/findStateIncident.htm',
                                                    extraParams: {
                                                        propertyFind: 'si.typeIncident',
                                                        valueFind: DukeSource.global.GiroConstants.EVENT,
                                                        propertyOrder: 'si.sequence'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'container',
                                            height: 26,
                                            layout: {
                                                type: 'hbox'
                                            },
                                            itemId: 'containerBuilder',
                                            hidden: true,
                                            items: [
                                                {
                                                    xtype: 'textfield',
                                                    name: 'userBuilder',
                                                    itemId: 'userBuilder',
                                                    hidden: true
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    name: 'userDetailDescriptionBuilder',
                                                    itemId: 'userDetailDescriptionBuilder',
                                                    hidden: true
                                                },
                                                {
                                                    xtype: 'UpperCaseTextFieldReadOnly',
                                                    flex: 1.3,
                                                    fieldCls: 'obligatoryTextField',
                                                    name: 'fullNameUserBuilder',
                                                    itemId: 'fullNameUserBuilder',
                                                    fieldLabel: 'Elaborador'
                                                },
                                                {
                                                    xtype: 'button',
                                                    iconCls: 'search',
                                                    handler: function () {
                                                        var win = Ext.create('DukeSource.view.risk.util.search.SearchUser', {
                                                            modal: true
                                                        }).show();
                                                        win.down('grid').on('itemdblclick', function (grid, record, item, index, e) {
                                                            me.down('#userBuilder').setValue(record.get('userName'));
                                                            me.down('#fullNameUserBuilder').setValue(record.get('fullName'));
                                                            me.down('#userDetailDescriptionBuilder').setValue(record.get('userDetailDescription'));
                                                            win.close();
                                                        });
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            height: 26,
                                            layout: {
                                                type: 'hbox'
                                            },
                                            itemId: 'containerReviser',
                                            hidden: true,
                                            items: [
                                                {
                                                    xtype: 'textfield',
                                                    name: 'userReviser',
                                                    itemId: 'userReviser',
                                                    hidden: true
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    name: 'userDetailDescriptionReviser',
                                                    itemId: 'userDetailDescriptionReviser',
                                                    hidden: true
                                                },
                                                {
                                                    xtype: 'UpperCaseTextFieldReadOnly',
                                                    flex: 1.3,
                                                    fieldCls: 'obligatoryTextField',
                                                    name: 'fullNameUserReviser',
                                                    itemId: 'fullNameUserReviser',
                                                    fieldLabel: 'Revisor'
                                                },
                                                {
                                                    xtype: 'button',
                                                    iconCls: 'search',
                                                    handler: function () {
                                                        var win = Ext.create('DukeSource.view.risk.util.search.SearchUser', {
                                                            modal: true
                                                        }).show();
                                                        win.down('grid').on('itemdblclick', function (grid, record, item, index, e) {
                                                            me.down('#userReviser').setValue(record.get('userName'));
                                                            me.down('#fullNameUserReviser').setValue(record.get('fullName'));
                                                            me.down('#userDetailDescriptionReviser').setValue(record.get('userDetailDescription'));
                                                            win.close();
                                                        });
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            itemId: 'containerApprove',
                                            hidden: true,
                                            height: 26,
                                            layout: {
                                                type: 'hbox'
                                            },
                                            items: [
                                                {
                                                    xtype: 'textfield',
                                                    name: 'userApprove',
                                                    itemId: 'userApprove',
                                                    hidden: true
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    name: 'userDetailDescriptionApprove',
                                                    itemId: 'userDetailDescriptionApprove',
                                                    hidden: true
                                                },
                                                {
                                                    xtype: 'UpperCaseTextFieldReadOnly',
                                                    flex: 1.3,
                                                    fieldCls: 'obligatoryTextField',
                                                    name: 'fullNameUserApprove',
                                                    itemId: 'fullNameUserApprove',
                                                    fieldLabel: 'Aprobador'
                                                },
                                                {
                                                    xtype: 'button',
                                                    iconCls: 'search',
                                                    handler: function () {
                                                        var win = Ext.create('DukeSource.view.risk.util.search.SearchUser', {
                                                            modal: true
                                                        }).show();
                                                        win.down('grid').on('itemdblclick', function (grid, record, item, index, e) {
                                                            me.down('#userApprove').setValue(record.get('userName'));
                                                            me.down('#fullNameUserApprove').setValue(record.get('fullName'));
                                                            me.down('#userDetailDescriptionApprove').setValue(record.get('userDetailDescription'));
                                                            win.close();
                                                        });
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'ViewComboYesNo',
                                            anchor: '100%',
                                            fieldLabel: 'Críticos',
                                            itemId: 'isCritic',
                                            name: 'isCritic',
                                            listeners: {
                                                render: function (cbo) {
                                                    cbo.getStore().load({
                                                            callback: function () {
                                                                cbo.store.add({
                                                                    id: 'T',
                                                                    description: 'Todos'
                                                                });
                                                            }
                                                        }
                                                    );
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'ViewComboYesNo',
                                            allowBlank: false,
                                            fieldCls: 'obligatoryTextField',
                                            anchor: '100%',
                                            fieldLabel: 'En línea',
                                            itemId: 'online',
                                            name: 'online'
                                        }
                                    ],
                                    buttons: [
                                        {
                                            text: 'Limpiar',
                                            scale: 'medium',
                                            iconCls: 'clear',
                                            handler: function () {
                                                me.down('form').getForm().reset();
                                            }
                                        },
                                        {
                                            text: 'Generar reporte',
                                            scale: 'medium',
                                            iconCls: 'txt',
                                            action: 'generateReportASFI'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            itemId: 'containerReport',
                            flex: 3,
                            margins: '2',
                            style: {
                                background: '#dde8f4',
                                border: '#99bce8 solid 1px'
                            },
                            layout: 'fit',
                            items: []
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    title: 'Reportes ASFI',
                    region: 'west',
                    width: 250,
                    height: 300,
                    collapsible: true,
                    layout: 'accordion',
                    margins: '2',
                    items: [
                        {
                            xtype: 'treepanel',
                            itemId: 'treepanel',
                            title: 'Evaluación de Riesgos',
                            rootVisible: false,
                            root: {
                                expanded: true,
                                children: [
                                    {
                                        text: "Evento de Riesgo Operativo",
                                        leaf: true,
                                        nameDownload: 'ROAAAAMMDDA.' + CODE_SEND_ENTITY,
                                        nameReport: 'ASFI_CIRO_001'
                                    },
                                    {
                                        text: "Cuentas Contables",
                                        leaf: true,
                                        nameDownload: 'ROAAAAMMDDB.' + CODE_SEND_ENTITY,
                                        nameReport: 'ASFI_CIRO_002'
                                    },
                                    {
                                        text: "Tipo de evento",
                                        leaf: true,
                                        nameDownload: 'ROAAAAMMDDC.' + CODE_SEND_ENTITY,
                                        nameReport: 'ASFI_CIRO_003'
                                    },
                                    {
                                        text: "Punto de Atención Financiera",
                                        leaf: true,
                                        nameDownload: 'ROAAAAMMDDD.' + CODE_SEND_ENTITY,
                                        nameReport: 'ASFI_CIRO_004'
                                    },
                                    {
                                        text: "Canal",
                                        leaf: true,
                                        nameDownload: 'ROAAAAMMDDE.' + CODE_SEND_ENTITY,
                                        nameReport: 'ASFI_CIRO_005'
                                    },
                                    {
                                        text: "Proceso",
                                        leaf: true,
                                        nameDownload: 'ROAAAAMMDDF.' + CODE_SEND_ENTITY,
                                        nameReport: 'ASFI_CIRO_006'
                                    },
                                    {
                                        text: "Operación",
                                        leaf: true,
                                        nameDownload: 'ROAAAAMMDDG.' + CODE_SEND_ENTITY,
                                        nameReport: 'ASFI_CIRO_007'
                                    },
                                    {
                                        text: "Lugar",
                                        leaf: true,
                                        nameDownload: 'ROAAAAMMDDH.' + CODE_SEND_ENTITY,
                                        nameReport: 'ASFI_CIRO_008'
                                    },
                                    {
                                        text: "Línea de negocio",
                                        leaf: true,
                                        nameDownload: 'ROAAAAMMDDI.' + CODE_SEND_ENTITY,
                                        nameReport: 'ASFI_CIRO_009'
                                    }
                                ]
                            },
                            listeners: {
                                itemclick: function (grid, record, item, index, e) {
                                    me.down('form').getForm().reset();
                                    me.down('#nameFile').setValue(record.raw.nameDownload);
                                    me.down('#nameReport').setValue(record.raw.nameReport);
                                    if (index === 0) {
                                        me.down('#containerBuilder').setVisible(true);
                                        me.down('#containerReviser').setVisible(true);
                                        me.down('#containerApprove').setVisible(true);
                                        me.down('#fullNameUserBuilder').allowBlank = false;
                                        me.down('#fullNameUserReviser').allowBlank = false;
                                        me.down('#fullNameUserApprove').allowBlank = false;
                                    }
                                    else {
                                        me.down('#containerBuilder').setVisible(false);
                                        me.down('#containerReviser').setVisible(false);
                                        me.down('#containerApprove').setVisible(false);
                                        me.down('#fullNameUserBuilder').allowBlank = true;
                                        me.down('#fullNameUserReviser').allowBlank = true;
                                        me.down('#fullNameUserApprove').allowBlank = true;
                                    }
                                }
                            }
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }
});