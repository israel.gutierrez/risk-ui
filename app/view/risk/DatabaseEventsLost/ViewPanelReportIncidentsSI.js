Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.ViewPanelReportIncidentsSI",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelReportIncidentsSI",
    border: false,
    layout: "fit",
    initComponent: function() {
      var me = this;
      this.items = [
        {
          xtype: "container",
          name: "generalContainer",
          border: false,
          layout: {
            align: "stretch",
            type: "border"
          },
          items: [
            {
              xtype: "panel",
              padding: "2 2 2 0",
              flex: 3,
              name: "panelReport",
              title: "REPORTE DE INCIDENTES",
              titleAlign: "center",
              region: "center",
              layout: "fit"
            },
            {
              xtype: "form",
              padding: "2 0 2 2",
              region: "west",
              collapseDirection: "right",
              collapsed: false,
              collapsible: true,
              split: true,
              flex: 1.2,
              title: "PARAMETROS DE BUSQUEDA",
              items: [
                {
                  xtype: "container",
                  layout: {
                    type: "anchor"
                  },
                  padding: "5",
                  items: [
                    {
                      xtype: "fieldset",
                      layout: {
                        align: "stretch",
                        type: "vbox"
                      },
                      checkboxToggle: true,
                      title: "FECHA DE OCURRENCIA",
                      items: [
                        {
                          xtype: "datefield",
                          fieldLabel: "DESDE",
                          labelAlign: "right",
                          allowBlank: false,
                          fieldCls: "obligatoryTextField",
                          blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                          labelWidth: 130,
                          format: "d/m/Y",
                          anchor: "100%",
                          itemId: "dateInitOcu",
                          name: "dateInitOcu"
                        },
                        {
                          xtype: "datefield",
                          fieldLabel: "HASTA",
                          labelAlign: "right",
                          allowBlank: false,
                          fieldCls: "obligatoryTextField",
                          blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                          labelWidth: 130,
                          format: "d/m/Y",
                          anchor: "100%",
                          itemId: "dateEndOcu",
                          name: "dateEndOcu"
                        }
                      ]
                    },
                    {
                      xtype: "fieldset",
                      layout: {
                        align: "stretch",
                        type: "vbox"
                      },
                      checkboxToggle: true,
                      title: "FECHA DE REGISTRO",
                      items: [
                        {
                          xtype: "datefield",
                          fieldLabel: "DESDE",
                          labelAlign: "right",
                          allowBlank: true,
                          blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                          labelWidth: 130,
                          format: "d/m/Y",
                          anchor: "100%",
                          itemId: "dateInit",
                          name: "dateInit"
                        },
                        {
                          xtype: "datefield",
                          fieldLabel: "HASTA",
                          labelAlign: "right",
                          allowBlank: true,
                          blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                          labelWidth: 130,
                          format: "d/m/Y",
                          anchor: "100%",
                          itemId: "dateEnd",
                          name: "dateEnd"
                        }
                      ]
                    },
                    {
                      xtype: "fieldset",
                      layout: {
                        align: "stretch",
                        padding: "10 0 0 0",
                        type: "vbox"
                      },
                      items: [
                        {
                          xtype: "combobox",
                          labelAlign: "right",
                          fieldLabel: "ESTADO INCIDENTE",
                          allowBlank: false,
                          fieldCls: "obligatoryTextField",
                          blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                          labelWidth: 130,
                          anchor: "100%",
                          store: {
                            fields: ["id", "description", "sequence"],
                            proxy: {
                              actionMethods: {
                                create: "POST",
                                read: "POST",
                                update: "POST"
                              },
                              type: "ajax",
                              url:
                                "http://localhost:9000/giro/findStateIncident.htm",
                              extraParams: {
                                propertyFind: "si.typeIncident",
                                valueFind: DukeSource.global.GiroConstants.OPERATIONAL,
                                propertyOrder: "si.sequence"
                              },
                              reader: {
                                type: "json",
                                root: "data",
                                successProperty: "success"
                              }
                            }
                          },
                          displayField: "description",
                          valueField: "id",
                          name: "stateIncident",
                          queryMode: "local",
                          listeners: {
                            render: function(cbo) {
                              cbo.getStore().load({
                                callback: function() {
                                  cbo.store.add({
                                    value: "T",
                                    description: "TODOS"
                                  });
                                }
                              });
                            }
                          }
                        },
                        {
                          xtype: "ViewComboWorkArea",
                          fieldLabel: "AREA",
                          labelAlign: "right",
                          labelWidth: 130,
                          queryMode: "remote",
                          emptyText: "Seleccionar",
                          forceSelection: false,
                          anchor: "100%",
                          name: "workArea"
                        },
                        {
                          xtype: "ViewComboAgency",
                          fieldLabel: "AGENCIA",
                          labelAlign: "right",
                          labelWidth: 130,
                          emptyText: "Seleccionar",
                          forceSelection: false,
                          anchor: "100%",
                          name: "agency"
                        }
                      ]
                    },
                    {
                      xtype: "container",
                      layout: {
                        align: "stretch",
                        pack: "end",
                        padding: "10 40 10 10",
                        type: "hbox"
                      },
                      items: [
                        {
                          xtype: "button",
                          scale: "medium",
                          text: "GENERAR",
                          iconCls: "pdf",
                          margin: "0 5 0 0",
                          action: "generateReportIncidentsSIPdf"
                        },
                        {
                          xtype: "button",
                          scale: "medium",
                          text: "GENERAR",
                          iconCls: "excel",
                          action: "generateReportIncidentsSIXls"
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ];
      this.callParent();
    }
  }
);
