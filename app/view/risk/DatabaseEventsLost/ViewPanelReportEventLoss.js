Ext.define("DukeSource.view.risk.DatabaseEventsLost.ViewPanelReportEventLoss", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelReportEventLoss",
  border: false,
  layout: "fit",
  requires: ["Ext.ux.DateTimeField"],
  values: function(item) {
    this.down("form")
      .getForm()
      .reset();
    this.down("form").setVisible(true);
    this.down("#nameFile").setValue(item.nameDownload);
    this.down("#nameReport").setValue(item.nameReport);
    this.down("#eventState").setVisible(item.eventState);
    this.down("#amount").setVisible(item.amount);
    this.down("#descriptionWorkArea").setVisible(item.descriptionWorkArea);
    this.down("#agency").setVisible(item.agency);
    this.down("#dateInit").setVisible(item.dateInit);
    this.down("#dateEnd").setVisible(item.dateEnd);
    this.down("#dateInit").allowBlank = !item.dateInit;
    this.down("#dateEnd").allowBlank = !item.dateEnd;
    this.down("#panelFilter").setTitle("Filtros - " + item.text);
  },
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "container",
          name: "generalContainer",
          border: false,
          layout: {
            align: "stretch",
            type: "border"
          },
          items: [
            {
              xtype: "panel",
              title: "Reportes",
              region: "west",
              width: 250,
              height: 300,
              collapsible: true,
              layout: "accordion",
              padding: 2,
              items: [
                {
                  xtype: "menu",
                  floating: false,
                  bodyStyle: "background-color:#f3f7fb !important;",
                  items: [
                    {
                      text: "Consolidado de eventos",
                      leaf: true,
                      iconCls: "application",
                      nameDownload: "consolidado_eventos",
                      nameReport: nameReport("ReportEvent_option_01"),
                      eventState: false,
                      agency: true,
                      descriptionWorkArea: true,
                      dateInit: true,
                      dateEnd: true,
                      amount: true
                    },
                    {
                      text: getName("ReportEvent_option_02"),
                      leaf: true,
                      iconCls: "application",
                      nameDownload: getName("ReportEvent_option_02"),
                      hidden: hidden("ReportEvent_option_02"),
                      nameReport: nameReport("ReportEvent_option_02"),
                      eventState: false,
                      agency: true,
                      descriptionWorkArea: true,
                      dateInit: true,
                      dateEnd: true,
                      amount: true
                    },
                    {
                      text: getName("ReportEvent_option_03"),
                      leaf: true,
                      iconCls: "application",
                      nameDownload: getName("ReportEvent_option_03"),
                      hidden: hidden("ReportEvent_option_03"),
                      nameReport: nameReport("ReportEvent_option_03"),
                      eventState: false,
                      agency: true,
                      descriptionWorkArea: true,
                      dateInit: true,
                      dateEnd: true,
                      amount: true
                    },
                    {
                      text: "Prepagos",
                      leaf: true,
                      nameDownload: "prepagos",
                      iconCls: "application",
                      hidden: hidden("ReportEvent_option_04"),
                      nameReport: nameReport("ReportEvent_option_04"),
                      eventState: false,
                      agency: true,
                      descriptionWorkArea: true,
                      dateInit: true,
                      dateEnd: true,
                      amount: true
                    },
                    {
                      text: "Hist&oacute;rico de eventos",
                      leaf: true,
                      iconCls: "application",
                      nameDownload: "Historico_eventos",
                      hidden: hidden("ReportEvent_option_05"),
                      nameReport: nameReport("ReportEvent_option_05"),
                      eventState: false,
                      agency: false,
                      descriptionWorkArea: false,
                      dateInit: false,
                      dateEnd: false,
                      amount: false
                    }
                  ],
                  listeners: {
                    click: function(menu, item, e) {
                      if (userRole === DukeSource.global.GiroConstants.ACCOUNTANT) {
                        if (item.nameDownload === "Contabilidad") {
                          me.values(item);
                        } else {
                          DukeSource.global.DirtyView.messageWarning("No permitido");
                        }
                      } else {
                        me.values(item);
                      }
                    }
                  }
                }
              ]
            },
            {
              xtype: "panel",
              flex: 3,
              border: false,
              itemId: "panelReport",
              titleAlign: "center",
              region: "center",
              margins: 2,
              layout: {
                type: "hbox",
                align: "stretch"
              },
              items: [
                {
                  xtype: "panel",
                  itemId: "panelFilter",
                  flex: 1.5,
                  title: "Filtros",
                  items: [
                    {
                      xtype: "container",
                      hidden: true,
                      items: [
                        {
                          xtype: "textfield",
                          name: "nameReport",
                          itemId: "nameReport",
                          hidden: true
                        },
                        {
                          xtype: "textfield",
                          name: "nameFile",
                          itemId: "nameFile",
                          hidden: true
                        }
                      ]
                    },
                    {
                      xtype: "form",
                      border: false,
                      hidden: true,
                      bodyPadding: 5,
                      fieldDefaults: {
                        labelCls: "changeSizeFontToEightPt",
                        fieldCls: "changeSizeFontToEightPt"
                      },
                      items: [
                        {
                          xtype: "textfield",
                          hidden: true,
                          itemId: "unity",
                          name: "unity"
                        },
                        {
                          xtype: "textfield",
                          hidden: true,
                          itemId: "costCenter",
                          name: "costCenter"
                        },
                        {
                          xtype: "datetimefield",
                          fieldLabel: "Fecha de reporte de",
                          allowBlank: false,
                          labelWidth: 120,
                          fieldCls: "obligatoryTextField",
                          blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                          format: "d/m/Y H:i",
                          name: "dateInit",
                          itemId: "dateInit"
                        },
                        {
                          xtype: "datetimefield",
                          labelWidth: 120,
                          fieldLabel: "Fecha de reporte a",
                          allowBlank: false,
                          fieldCls: "obligatoryTextField",
                          blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                          format: "d/m/Y H:i",
                          name: "dateEnd",
                          itemId: "dateEnd"
                        },
                        {
                          xtype: "combobox",
                          fieldLabel: "Estado",
                          editable: false,
                          name: "eventState",
                          labelWidth: 120,
                          itemId: "eventState",
                          queryMode: "local",
                          displayField: "description",
                          valueField: "id",
                          store: {
                            fields: ["id", "description", "sequence"],
                            proxy: {
                              actionMethods: {
                                create: "POST",
                                read: "POST",
                                update: "POST"
                              },
                              type: "ajax",
                              url:
                                "http://localhost:9000/giro/findStateIncident.htm",
                              extraParams: {
                                propertyFind: "si.typeIncident",
                                valueFind: DukeSource.global.GiroConstants.EVENT,
                                propertyOrder: "si.sequence"
                              },
                              reader: {
                                type: "json",
                                root: "data",
                                successProperty: "success"
                              }
                            }
                          },
                          listeners: {
                            render: function(cbo) {
                              cbo.getStore().load({
                                callback: function() {
                                  cbo.store.add({
                                    id: "T",
                                    description: "Todos",
                                    sequence: "999"
                                  });
                                  cbo.setValue("T");
                                }
                              });
                            }
                          }
                        },
                        {
                          xtype: "ViewComboAgency",
                          fieldLabel: "Agencia",
                          labelWidth: 120,
                          emptyText: "Seleccionar",
                          queryMode: "local",
                          forceSelection: false,
                          name: "agency",
                          itemId: "agency",
                          listeners: {
                            expand: {
                              fn: function(cbo) {
                                cbo.store.add({
                                  idAgency: "T",
                                  description: "Todos"
                                });
                              },
                              single: true
                            }
                          }
                        },
                        {
                          xtype: "displayfield",
                          anchor: "100%",
                          labelWidth: 120,
                          fieldLabel: getName("ReportEvents_VPE_WorkArea"),
                          itemId: "descriptionWorkArea",
                          name: "descriptionWorkArea",
                          padding: "2 0 2 0",
                          value: "(Seleccionar)",
                          fieldCls: "style-for-url",
                          listeners: {
                            afterrender: function(view) {
                              view.getEl().on("click", function() {
                                Ext.create(
                                  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeOrganization",
                                  {
                                    backWindow: me,
                                    descriptionUnity: "descriptionWorkArea",
                                    unity: "unity",
                                    costCenter: "costCenter"
                                  }
                                ).show();
                              });
                            }
                          }
                        },
                        {
                          xtype: "NumberDecimalNumber",
                          labelWidth: 120,
                          fieldLabel: "Pérdida mayor a",
                          emptyText: "0.00",
                          minValue: -9999999999,
                          name: "amount",
                          itemId: "amount"
                        }
                      ],
                      buttons: [
                        {
                          text: "Limpiar",
                          scale: "medium",
                          iconCls: "clear",
                          handler: function() {
                            me.down("form")
                              .getForm()
                              .reset();
                          }
                        },
                        {
                          text: "Generar",
                          scale: "medium",
                          iconCls: "excel",
                          action: "generateReportsEventLoss"
                        }
                      ]
                    }
                  ]
                },
                {
                  xtype: "container",
                  itemId: "containerReport",
                  flex: 3,
                  margins: "0 0 0 2",
                  style: {
                    background: "#dde8f4",
                    border: "#99bce8 solid 1px !important"
                  },
                  layout: "fit",
                  items: []
                }
              ]
            }
          ]
        }
      ]
    });

    me.callParent();
  }
});
