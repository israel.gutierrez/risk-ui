Ext.define("DukeSource.view.risk.DatabaseEventsLost.ViewPanelReportIncidents", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelReportIncidents",
  border: false,
  layout: "fit",
  requires: ["Ext.ux.DateTimeField"],
  initComponent: function() {
    var me = this;
    this.items = [
      {
        xtype: "container",
        name: "generalContainer",
        border: false,
        layout: {
          align: "stretch",
          type: "border"
        },
        items: [
          {
            xtype: "panel",
            title: "Reportes",
            region: "west",
            width: 250,
            height: 300,
            collapsible: true,
            layout: "accordion",
            padding: 2,
            items: [
              {
                xtype: "menu",
                floating: false,
                bodyStyle: "background-color:#f3f7fb !important;",
                items: [
                  {
                    text: "Consolidado de Incidentes",
                    leaf: true,
                    iconCls: "application",
                    nameDownload: "consolidado_incidentes",
                    nameReport: nameReport("ReportIncident_option_01"),
                    stateIncident: true,
                    descriptionWorkArea: true,
                    dateInit: true,
                    dateEnd: true
                  },
                  {
                    text: "Incidentes pendientes",
                    leaf: true,
                    iconCls: "application",
                    nameDownload: "incidentes_pendientes",
                    nameReport: nameReport("ReportIncident_option_02"),
                    stateIncident: true,
                    descriptionWorkArea: true,
                    dateInit: true,
                    dateEnd: true
                  },
                  {
                    text: "Incidentes de Ciberseguridad",
                    leaf: true,
                    iconCls: "application",
                    nameDownload: "incidentes_ciberseguridad",
                    nameReport: nameReport("ReportIncident_option_03"),
                    hidden: hidden("ReportIncident_option_03"),
                    stateIncident: true,
                    descriptionWorkArea: false,
                    dateInit: true,
                    dateEnd: true
                  }
                ],
                listeners: {
                  click: function(menu, item, e) {
                    me.down("form")
                      .getForm()
                      .reset();
                    me.down("form").setVisible(true);
                    me.down("#nameFile").setValue(item.nameDownload);
                    me.down("#nameReport").setValue(item.nameReport);
                    me.down("#stateIncident").setVisible(item.stateIncident);
                    me.down("#descriptionWorkArea").setVisible(
                      item.descriptionWorkArea
                    );
                    me.down("#dateInit").setVisible(item.dateInit);
                    me.down("#dateEnd").setVisible(item.dateEnd);
                    me.down("#panelFilter").setTitle("Filtros - " + item.text);
                  }
                }
              }
            ]
          },
          {
            xtype: "panel",
            flex: 3,
            border: false,
            itemId: "panelReport",
            titleAlign: "center",
            region: "center",
            margins: 2,
            layout: {
              type: "hbox",
              align: "stretch"
            },
            items: [
              {
                xtype: "panel",
                itemId: "panelFilter",
                flex: 1.5,
                title: "Filtros",
                items: [
                  {
                    xtype: "container",
                    hidden: true,
                    items: [
                      {
                        xtype: "textfield",
                        name: "nameReport",
                        itemId: "nameReport",
                        hidden: true
                      },
                      {
                        xtype: "textfield",
                        name: "nameFile",
                        itemId: "nameFile",
                        hidden: true
                      }
                    ]
                  },
                  {
                    xtype: "form",
                    border: false,
                    hidden: true,
                    bodyPadding: 5,
                    fieldDefaults: {
                      labelCls: "changeSizeFontToEightPt",
                      fieldCls: "changeSizeFontToEightPt"
                    },
                    items: [
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "unity",
                        name: "unity"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "costCenter",
                        name: "costCenter"
                      },
                      {
                        xtype: "datetimefield",
                        fieldLabel: "Fecha registro de",
                        allowBlank: false,
                        fieldCls: "obligatoryTextField",
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        labelWidth: 120,
                        format: "d/m/Y H:i",
                        itemId: "dateInit",
                        name: "dateInit",
                        listeners: {
                          specialkey: function(f, e) {
                            DukeSource.global.DirtyView.focusEventEnter(
                              f,
                              e,
                              me.down("datefield[name=dateEnd]")
                            );
                          }
                        }
                      },
                      {
                        xtype: "datetimefield",
                        fieldLabel: "Fecha registro a",
                        allowBlank: false,
                        fieldCls: "obligatoryTextField",
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        labelWidth: 120,
                        format: "d/m/Y H:i",
                        itemId: "dateEnd",
                        name: "dateEnd",
                        listeners: {
                          specialkey: function(f, e) {
                            DukeSource.global.DirtyView.focusEventEnter(
                              f,
                              e,
                              me.down("combobox[name=stateIncident]")
                            );
                          }
                        }
                      },
                      {
                        xtype: "combobox",
                        flex: 1,
                        labelWidth: 120,
                        fieldLabel: "Estado",
                        editable: false,
                        name: "stateIncident",
                        itemId: "stateIncident",
                        anchor: "100%",
                        queryMode: "local",
                        emptyText: "Seleccionar",
                        displayField: "description",
                        valueField: "id",
                        store: {
                          fields: ["id", "description", "sequence"],
                          autoLoad: true,
                          proxy: {
                            actionMethods: {
                              create: "POST",
                              read: "POST",
                              update: "POST"
                            },
                            type: "ajax",
                            url:
                              "http://localhost:9000/giro/findStateIncident.htm",
                            extraParams: {
                              propertyFind: "si.typeIncident",
                              valueFind: DukeSource.global.GiroConstants.OPERATIONAL,
                              propertyOrder: "si.sequence"
                            },
                            reader: {
                              type: "json",
                              root: "data",
                              successProperty: "success"
                            }
                          },
                          listeners: {
                            load: function(store) {
                              store.add({
                                id: "T",
                                description: "Todos",
                                sequence: "999"
                              });
                            }
                          }
                        }
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        labelWidth: 120,
                        fieldLabel: getName("ReportEvents_VPE_WorkArea"),
                        itemId: "descriptionWorkArea",
                        name: "descriptionWorkArea",
                        padding: "2 0 2 0",
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeOrganization",
                                {
                                  backWindow: me,
                                  descriptionUnity: "descriptionWorkArea",
                                  unity: "unity",
                                  costCenter: "costCenter"
                                }
                              ).show();
                            });
                          }
                        }
                      }
                    ],
                    buttons: [
                      {
                        text: "Limpiar",
                        scale: "medium",
                        iconCls: "clear",
                        handler: function() {
                          me.down("form")
                            .getForm()
                            .reset();
                        }
                      },
                      {
                        text: "Generar",
                        scale: "medium",
                        iconCls: "excel",
                        action: "generateReportIncidents"
                      }
                    ]
                  }
                ]
              },
              {
                xtype: "container",
                itemId: "containerReport",
                flex: 3,
                margins: "0 0 0 2",
                style: {
                  background: "#dde8f4",
                  border: "#99bce8 solid 1px !important"
                },
                layout: "fit",
                items: []
              }
            ]
          }
        ]
      }
    ];
    this.callParent();
  }
});
