Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowHistoricalIncident', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowHistoricalIncident',
    layout: {
        type: 'fit'
    },
    title: 'HISTORICO DE INCIDENTES',
    border: false,
    width: 800,
    height: 600,

    titleAlign: 'center',
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'ViewGridHistoricalIncident'
                }
            ]
        });

        me.callParent(arguments);
    }

});
