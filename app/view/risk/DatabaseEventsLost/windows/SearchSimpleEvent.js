Ext.define("ModelSearchEvent", {
  extend: "Ext.data.Model",
  fields: [
    "idEvent",
    "codeEvent",
    "descriptionShort",
    "descriptionLarge",
    "eventType",
    "eventMain"
  ]
});

var storeSearchEvent = Ext.create("Ext.data.Store", {
  model: "ModelSearchEvent",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.SearchSimpleEvent",
  {
    extend: "Ext.window.Window",
    alias: "widget.SearchSimpleEvent",
    layout: {
      align: "stretch",
      type: "vbox"
    },
    anchorSize: 100,
    title: "BUSCAR EVENTO",
    titleAlign: "center",
    width: 900,
    height: 600,
    initComponent: function() {
      var me = this;
      var recordMainEvent = this.recordMainEvent;

      function validateEvent(record, recordMainEvent) {
        var go = true;
        if (record.get("codeEvent") === recordMainEvent["codeEvent"]) {
          DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_EVENT_REFERENCE_CIRCLE);
          go = false;
        } else if (record.get("eventType") === "2") {
          DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_EVENT_SIMPLE_ADD);
          go = false;
        } else if (record.get("eventMain") === "N") {
          DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_EVENT_IS_SUB_EVENT);
          go = false;
        }
        return go;
      }

      function continueSelectSubEvent() {
        var record = me
          .down("grid")
          .getSelectionModel()
          .getSelection()[0];

        if (validateEvent(record, recordMainEvent)) {
          DukeSource.lib.Ajax.request({
            method: "POST",
            url: "http://localhost:9000/giro/getEventMasterByIdEvent.htm",
            params: {
              idEvent: record.get("idEvent")
            },
            success: function(response) {
              response = Ext.decode(response.responseText);
              if (response.success) {
                var view = Ext.create(
                  "DukeSource.view.risk.DatabaseEventsLost.windows.WindowEventAndSubEvent",
                  {
                    modal: true,
                    width: 900,
                    recordSubEvent: response.data,
                    recordMainEvent: recordMainEvent
                  }
                );
                var recordSubEvent = response.data;

                view
                  .down("#agency")
                  .getStore()
                  .load();
                view
                  .down("#workArea")
                  .getStore()
                  .load();
                view
                  .down("#eventType")
                  .getStore()
                  .load();
                view
                  .down("#factorRisk")
                  .getStore()
                  .load();
                view
                  .down("#eventState")
                  .getStore()
                  .load();
                view
                  .down("#currency")
                  .getStore()
                  .load();
                addCombos(view, recordSubEvent);
                view
                  .down("form")
                  .getForm()
                  .setValues(recordSubEvent);
                view
                  .query(
                    ".UpperCaseTextArea,.UpperCaseTextFieldObligatory, .datetimefield, .combobox"
                  )
                  .forEach(function(c) {
                    c.setReadOnly(true);
                    c.setFieldStyle("background: #f1f1f1");
                  });
                view.down("#workArea").setValue(recordSubEvent["parentArea"]);
                view.down("#eventMain").setValue("N");

                view
                  .down("#descriptionShortMainEvent")
                  .setValue(recordMainEvent["descriptionShort"]);
                view
                  .down("#codeMainEvent")
                  .setValue(recordMainEvent["codeEvent"]);
                view
                  .down("#numberRelation")
                  .setValue(recordMainEvent["idEvent"]);
                view.show();
                me.close();
              } else {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_WARNING,
                  response.message,
                  Ext.Msg.ERROR
                );
              }
            },
            failure: function() {}
          });
        }
      }

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            height: 45,
            padding: "2 2 2 2",
            bodyPadding: 10,
            items: [
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "combobox",
                    value: "2",
                    labelWidth: 40,
                    width: 190,
                    fieldLabel: "TIPO",
                    store: [
                      ["1", "CODIGO"],
                      ["2", "DESCRIPCION"]
                    ]
                  },
                  {
                    xtype:"UpperCaseTextField",
                    flex: 2,
                    listeners: {
                      afterrender: function(field) {
                        field.focus(false, 200);
                      },
                      specialkey: function(field, e) {
                        var property = "";
                        if (me.down("combobox").getValue() === "1") {
                          property = "codeEvent";
                        } else {
                          property = "descriptionLargeEvent";
                        }
                        if (e.getKey() === e.ENTER) {
                          var grid = me.down("grid");
                          DukeSource.global.DirtyView.searchPaginationGridToEnter(
                            field,
                            grid,
                            grid.down("pagingtoolbar"),
                            "http://localhost:9000/giro/findEventMaster.htm",
                            property,
                            "idEvent"
                          );
                        }
                      }
                    }
                  }
                ]
              }
            ]
          },
          {
            xtype: "container",
            flex: 3,
            padding: "2 2 2 2",
            layout: {
              align: "stretch",
              type: "hbox"
            },
            items: [
              {
                xtype: "gridpanel",
                padding: "0 1 0 0",
                store: storeSearchEvent,
                flex: 1,
                titleAlign: "center",
                columns: [
                  {
                    dataIndex: "codeEvent",
                    align: "center",
                    width: 120,
                    text: "CODIGO"
                  },
                  {
                    dataIndex: "descriptionLarge",
                    flex: 1,
                    text: "DESCRIPCION LARGA"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: storeSearchEvent,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  render: function() {
                    var me = this;
                    DukeSource.global.DirtyView.searchPaginationGridNormal(
                      "",
                      me,
                      me.down("pagingtoolbar"),
                      "http://localhost:9000/giro/findEventMaster.htm",
                      "descriptionLargeEvent",
                      "idEvent"
                    );
                  },
                  itemdblclick: function(grid, record, item) {
                    continueSelectSubEvent();
                  }
                }
              }
            ]
          }
        ],
        buttons: [
          {
            text: "CONTINUAR",
            scale: "medium",
            iconCls: "arrow_right_16",
            handler: function(btn) {
              continueSelectSubEvent();
            }
          },
          {
            text: "SALIR",
            scale: "medium",
            iconCls: "logout",
            handler: function() {
              me.close();
            }
          }
        ],
        buttonAlign: "center"
      });
      me.callParent(arguments);
    }
  }
);

function addCombos(win, recordSubEvent) {
  var comboSubProcess = win.down("#idSubProcess");
  comboSubProcess.setDisabled(false);
  comboSubProcess.getStore().load({
    url: "http://localhost:9000/giro/showListSubProcessActives.htm",
    params: {
      valueFind: recordSubEvent["process"]
    },
    callback: function(cbo) {
      win
        .down("#process")
        .getStore()
        .load();
      comboSubProcess.setValue(recordSubEvent["idSubProcess"]);
    }
  });
}
