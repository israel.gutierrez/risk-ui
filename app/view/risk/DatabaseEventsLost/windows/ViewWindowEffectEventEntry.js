Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowEffectEventEntry",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowEffectEventEntry",
    width: 500,
    layout: {
      type: "fit"
    },
    title: "Ingresar el tipo de efecto",
    titleAlign: "center",
    border: false,
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            title: "",
            items: [
              {
                xtype: "container",
                height: 84,
                anchor: "100%",
                layout: {
                  type: "hbox",
                  align: "middle"
                },
                items: [
                  {
                    xtype: "textfield",
                    value: "id",
                    name: "id",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    name: "abbreviation",
                    value: "ABREV",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    value: "S",
                    name: "state",
                    hidden: true
                  },
                  {
                    xtype: 'UpperCaseTextArea',
                    height: 75,
                    flex: "1",
                    maxLengthText: DukeSource.global.GiroMessages.MESSAGE_MAX_CHARACTER + 800,
                    maxLength: 800,
                    allowBlank: false,
                    fieldLabel: "Descripci&oacute;n",
                    name: "description",
                    listeners: {
                      afterrender: function(field) {
                        field.focus(false, 200);
                      },
                      specialkey: function(f, e) {
                        DukeSource.global.DirtyView.focusEventEnter(
                          f,
                          e,
                          me.down("button[action=saveNewDebility]")
                        );
                      }
                    }
                  }
                ]
              }
            ]
          }
        ],
        buttonAlign: "center",
        buttons: [
          {
            text: "Guardar",
            scale: "medium",
            iconCls: "save",
            handler: function() {
              var grid = Ext.ComponentQuery.query(
                "ViewWindowEffectEvent"
              )[0].down("grid");
              Ext.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/saveEffectType.htm?nameView=ViewPanelRegisterTypeEffect",
                params: {
                  jsonData: Ext.JSON.encode(me.down("form").getValues())
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    grid.down("pagingtoolbar").moveFirst();
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      response.mensaje,
                      Ext.Msg.INFO
                    );
                    me.close();
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_ERROR,
                      response.mensaje,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function() {}
              });
            }
          },
          {
            text: "Salir",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
