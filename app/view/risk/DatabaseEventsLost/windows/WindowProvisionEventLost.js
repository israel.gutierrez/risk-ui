Ext.define('ModelProvisionEventLost', {
    extend: 'Ext.data.Model',
    fields: [
        'idEvent'
        , 'amountRecovery'
        , 'amountTotal'
        , 'dateRecovery'
        , 'descriptionLarge'
        , 'descriptionTypeRecovery'
        , 'nameCurrency'
        , 'typeChange'
        , 'typeRegister'
        , 'typeRecovery'
        , 'code'
    ]
});

var storeSpendEventLost = Ext.create('Ext.data.Store', {
    model: 'ModelProvisionEventLost',
    autoLoad: false,
    proxy: {
        actionMethods: {
            create: 'POST',
            read: 'POST',
            update: 'POST'
        },
        type: 'ajax',
        url: 'http://localhost:9000/giro/loadGridDefault.htm',
        reader: {
            totalProperty: 'totalCount',
            root: 'data',
            successProperty: 'success'
        }
    }
});

Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.WindowProvisionEventLost', {
    extend: 'Ext.window.Window',
    requires: [
        'DukeSource.view.risk.parameter.combos.ViewComboCurrency'
    ],
    alias: 'widget.WindowProvisionEventLost',
    height: 520,
    width: 600,
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    border: false,
    title: 'Provisiones del evento de pérdida',
    initComponent: function () {
        var me = this;
        var recordMainEvent = this.recordMainEvent;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 5,
                    flex: 1,
                    fieldDefaults: {
                        labelCls: 'changeSizeFontToEightPt',
                        fieldCls: 'changeSizeFontToEightPt'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            hidden: true,
                            value: 'id',
                            name: 'idRecoveryEvent'
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            name: 'idEvent',
                            itemId: 'idEvent'
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            name: 'numberRelation',
                            itemId: 'numberRelation'
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            name: 'typeRegister',
                            value: DukeSource.global.GiroConstants.TYPE_PROVISION,
                            itemId: 'typeRegister'
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            name: 'code',
                            itemId: 'code'
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            height: 60,
                            allowBlank: false,
                            fieldCls: 'obligatoryTextField',
                            fieldLabel: 'Descripción',
                            name: 'descriptionLarge',
                            itemId: 'descriptionLarge',
                            maxLength: 600
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: {
                                        type: 'anchor'
                                    },
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    xtype: 'ViewComboCurrency',
                                                    fieldLabel: 'Moneda',
                                                    name: 'currency',
                                                    itemId: 'currency',
                                                    anchor: '100%',
                                                    fieldCls: 'obligatoryTextField',
                                                    allowBlank: false,
                                                    flex: 1,
                                                    listeners: {
                                                        specialkey: function (f, e) {
                                                            DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#typeChange'))
                                                        },
                                                        select: function (cbo) {
                                                            var typeChange = me.down('#typeChange');
                                                            if (cbo.getValue() == '1') {
                                                                typeChange.setValue('1.00');
                                                                typeChange.setFieldStyle('background: #D8D8D8');
                                                                typeChange.setReadOnly(true);
                                                            } else {
                                                                typeChange.setValue('');
                                                                typeChange.setFieldStyle('background: #d9ffdb');
                                                                typeChange.setReadOnly(false);
                                                            }
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype: 'NumberDecimalNumberObligatory',
                                                    anchor: '100%',
                                                    fieldLabel: 'T.C',
                                                    name: 'typeChange',
                                                    itemId: 'typeChange',
                                                    labelWidth: 20,
                                                    padding: '0 0 0 5',
                                                    allowBlank: false,
                                                    maxValue: 9999,
                                                    forcePrecision: true,
                                                    flex: 0.6,
                                                    decimalPrecision: 4,
                                                    listeners: {
                                                        specialkey: function (f, e) {
                                                            if (e.getKey() === e.ENTER || e.getKey() === e.TAB) {
                                                                calculateTotalSpend(me);
                                                                DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#amountRecovery'))
                                                            }
                                                        },
                                                        blur: function (f, e) {
                                                            calculateTotalSpend(me);
                                                        }
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'NumberDecimalNumberObligatory',
                                            name: 'amountRecovery',
                                            allowBlank: false,
                                            itemId: 'amountRecovery',
                                            padding: '5 0 0 0',
                                            fieldLabel: 'Monto de provisión',
                                            maxLength: 13,
                                            anchor: '100%',
                                            listeners: {
                                                specialkey: function (f, e) {
                                                    if (e.getKey() === e.ENTER || e.getKey() === e.TAB) {
                                                        calculateTotalSpend(me);
                                                        DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#dateRecovery'))
                                                    }
                                                },
                                                blur: function (f, e) {
                                                    calculateTotalSpend(me);
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'NumberDecimalNumberObligatory',
                                            anchor: '100%',
                                            name: 'totalSpend',
                                            itemId: 'totalSpend',
                                            fieldLabel: 'Total de provisión',
                                            fieldCls: 'readOnlyText'
                                        },
                                        {
                                            xtype: 'combobox',
                                            anchor: '100%',
                                            fieldLabel: 'Tipo provisión',
                                            msgTarget: 'side',
                                            fieldCls: 'obligatoryTextField',
                                            displayField: 'description',
                                            valueField: 'value',

                                            forceSelection: true,
                                            editable: false,
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            allowBlank: false,
                                            name: 'typeRecovery',
                                            itemId: 'typeRecovery',
                                            store: {
                                                fields: ['value', 'description'],
                                                pageSize: 9999,
                                                autoLoad: true,
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                    extraParams: {
                                                        propertyOrder: 'description',
                                                        propertyFind: 'identified',
                                                        valueFind: 'TYPE_PROVISION'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        }

                                    ]
                                },
                                {
                                    xtype: 'container',
                                    anchor: '100%',
                                    padding: '0 0 0 5',
                                    flex: 1,
                                    layout: {
                                        type: 'anchor'
                                    },
                                    items: [
                                        {
                                            xtype: 'datefield',
                                            allowBlank: false,
                                            msgTarget: 'side',
                                            name: 'dateRecovery',
                                            itemId: 'dateRecovery',
                                            format: 'd/m/Y',
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            anchor: '100%',
                                            fieldLabel: 'Fecha de provisión',
                                            maxValue: new Date(),
                                            listeners: {
                                                expand: function (field, value) {
                                                    field.setMinValue(recordMainEvent.get('dateAcceptLossEvent'));
                                                    field.setMaxValue(new Date());
                                                },
                                                blur: function (field, value) {
                                                    field.setMinValue(recordMainEvent.get('dateAcceptLossEvent'));
                                                    field.setMaxValue(new Date());
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'container',
                                            anchor: '100%',
                                            height: 27,
                                            layout: {
                                                type: 'hbox'
                                            },
                                            items: [
                                                {
                                                    xtype: 'textfield',
                                                    name: 'accountPlan',
                                                    hidden: true
                                                },
                                                {
                                                    xtype: 'TextFieldNumberFormatReadOnly',
                                                    flex: 1,
                                                    allowBlank: false,
                                                    fieldLabel: 'Cuenta contable',
                                                    name: 'codeAccount',
                                                    itemId: 'codeAccount'
                                                },
                                                {
                                                    xtype: 'button',
                                                    name: 'searchAccountCta',
                                                    itemId: 'searchAccountCta',
                                                    width: 21,
                                                    iconCls: 'search',
                                                    handler: function () {
                                                        var window = Ext.create('DukeSource.view.risk.parameter.windows.ViewWindowSearchPlanAccountCountable', {modal: true}).show();
                                                        window.down('UpperCaseTextField').focus(false, 100);
                                                        window.down('UpperCaseTextField').setValue(me.down('#codeAccount').getValue());
                                                        window.down('grid').on('itemdblclick', function () {
                                                            me.down('#codeAccount').setValue(window.down('grid').getSelectionModel().getSelection()[0].get('codeAccount'));
                                                            me.down('textfield[name=accountPlan]').setValue(window.down('grid').getSelectionModel().getSelection()[0].get('idAccountPlan'));
                                                            me.down('UpperCaseTextField[name=bookkeepingRecovery]').focus(false, 100);
                                                            window.close();
                                                        });
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype:"UpperCaseTextField",
                                            anchor: '100%',
                                            fieldLabel: 'Asiento contable',
                                            name: 'bookkeepingRecovery',
                                            maxLength: 50,
                                            listeners: {
                                                specialkey: function (f, e) {
                                                    DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('datefield[name=dateRecovery]'))
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    tbar: [
                        {
                            text: 'Nuevo',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            iconCls: 'add',
                            handler: function () {
                                var form = me.down('form');
                                form.getForm().reset();
                                me.down('#idEvent').setValue(recordMainEvent.get('idEvent'));
                            }
                        },
                        {
                            text: 'Guardar',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            iconCls: 'save',
                            handler: function () {
                                var form = me.down('form');
                                if (form.getForm().isValid()) {
                                    Ext.Ajax.request({
                                        method: 'POST',
                                        url: 'http://localhost:9000/giro/saveSpendEvent.htm?nameView=ViewPanelEventsRiskOperational',
                                        params: {
                                            jsonData: Ext.JSON.encode(form.getValues())
                                        },
                                        success: function (response) {
                                            var idEvent = me.down('#idEvent').getValue();
                                            var numberRelation = me.down('#numberRelation').getValue();
                                            response = Ext.decode(response.responseText);
                                            if (response.success) {
                                                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                                                var grid = Ext.ComponentQuery.query('ViewPanelEventsRiskOperational grid')[0];
                                                var gridSubEvent = Ext.ComponentQuery.query('ViewWindowRegisterSubEvents grid')[0];
                                                if (gridSubEvent != undefined) {
                                                    gridSubEvent.store.getProxy().extraParams = {idEvent: numberRelation};
                                                    gridSubEvent.store.getProxy().url = 'http://localhost:9000/giro/getSubEventsByEvent.htm';
                                                    gridSubEvent.down('pagingtoolbar').doRefresh();
                                                }
                                                grid.down('pagingtoolbar').doRefresh();
                                                me.down('grid').down('pagingtoolbar').doRefresh();
                                                form.getForm().reset();
                                                me.down('#idEvent').setValue(recordMainEvent.get('idEvent'));
                                            } else {
                                                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, response.mensaje, Ext.Msg.ERROR);
                                            }
                                        },
                                        failure: function () {
                                        }
                                    });
                                } else {
                                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_COMPLETE, Ext.Msg.ERROR);
                                }
                            }
                        }
                    ]
                },
                {
                    xtype: 'gridpanel',
                    store: storeSpendEventLost,
                    flex: 1,
                    padding: '2 0 0 0',
                    tbar: [
                        {
                            text: 'Eliminar',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            iconCls: 'delete',
                            handler: function () {
                                var grid = me.down('grid');
                                var form = me.down('form');
                                var record = grid.getSelectionModel().getSelection()[0];
                                if (grid.getSelectionModel().getCount() != 0) {
                                    Ext.MessageBox.show({
                                        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                                        msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
                                        icon: Ext.Msg.QUESTION,
                                        buttonText: {
                                            yes: "Si"
                                        },
                                        buttons: Ext.MessageBox.YESNO,
                                        fn: function (btn) {
                                            if (btn == 'yes') {
                                                DukeSource.lib.Ajax.request({
                                                    method: 'POST',
                                                    url: 'http://localhost:9000/giro/deleteRecoveryEvent.htm?nameView=ViewPanelEventsRiskOperational',
                                                    params: {
                                                        jsonData: Ext.JSON.encode(record.raw)
                                                    },
                                                    success: function (response) {
                                                        response = Ext.decode(response.responseText);
                                                        var numberRelation = me.down('#numberRelation').getValue();
                                                        if (response.success) {
                                                            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                                                            var gridSubEvent = Ext.ComponentQuery.query('ViewWindowRegisterSubEvents grid')[0];
                                                            if (gridSubEvent != undefined) {
                                                                gridSubEvent.store.getProxy().extraParams = {idEvent: numberRelation};
                                                                gridSubEvent.store.getProxy().url = 'http://localhost:9000/giro/getSubEventsByEvent.htm';
                                                                gridSubEvent.down('pagingtoolbar').doRefresh();
                                                            }
                                                            var gridEvent = Ext.ComponentQuery.query('ViewPanelEventsRiskOperational grid')[0];
                                                            gridEvent.down('pagingtoolbar').doRefresh();
                                                            me.down('grid').down('pagingtoolbar').doRefresh();
                                                            form.getForm().reset();
                                                            me.down('#idEvent').setValue(recordMainEvent.get('idEvent'));
                                                        } else {
                                                            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, response.mensaje, Ext.Msg.ERROR);
                                                        }
                                                    },
                                                    failure: function () {
                                                    }
                                                });
                                            }
                                        }
                                    });
                                } else {
                                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_ITEM, Ext.Msg.WARNING);
                                }
                            }
                        }
                    ],
                    columns: [
                        {
                            xtype: 'rownumberer',
                            width: 25,
                            sortable: false
                        },
                        {
                            dataIndex: 'descriptionTypeRecovery',
                            align: 'center',
                            text: 'Tipo',
                            width: 95,
                            renderer: function (value, metaData, record, row, col, store, gridView) {
                                metaData.tdAttr = 'style="background-color: #60ff97 !important;border:1px solid #60ff97 !important;"';
                                return '<div style="background-color:#60ff97;">' + value + '</div>';
                            }
                        },
                        {
                            dataIndex: 'dateRecovery',
                            text: 'Fecha',
                            align: 'center',
                            width: 120
                        },
                        {
                            dataIndex: 'typeChange',
                            width: 100,
                            align: 'center',
                            text: 'Tipo cambio'
                        },
                        {
                            dataIndex: 'amountRecovery',
                            width: 130,
                            align: 'center',
                            format: '0,0.00',
                            xtype: 'numbercolumn',
                            text: 'Monto original'
                        },
                        {
                            dataIndex: 'amountTotal',
                            width: 130,
                            format: '0,0.00',
                            xtype: 'numbercolumn',
                            text: 'Monto total'
                        },
                        {
                            header: 'Descripción corta',
                            width: 400,
                            align: 'left',
                            dataIndex: 'descriptionLarge'
                        }
                    ],
                    bbar: {
                        xtype: 'pagingtoolbar',
                        pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                        store: storeSpendEventLost,
                        displayInfo: true,
                        displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                        emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                    },
                    listeners: {
                        itemdblclick: function (grid, record) {
                            me.down('#currency').getStore().load();
                            me.down('form').getForm().setValues(record.raw);
                            me.down('#totalSpend').setValue(record.raw['amountTotal']);
                        }
                    }
                }
            ],
            buttons: [
                {
                    text: 'Salir',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });
        me.callParent(arguments);
    }
});

function calculateTotalSpend(me) {
    var typeChange = me.down('#typeChange').getValue();
    var amountOrigin = me.down('#amountRecovery').getValue();
    var result = amountOrigin * typeChange;
    me.down('#totalSpend').setValue(result);
}