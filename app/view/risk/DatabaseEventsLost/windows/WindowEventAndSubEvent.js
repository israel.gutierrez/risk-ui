Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.WindowEventAndSubEvent', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowEventAndSubEvent',
    requires: [
        'Ext.form.Panel',
        'Ext.ux.DateTimeField',
        'Ext.form.field.TextArea',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Date',
        'Ext.button.Button',
        'Ext.form.field.File'
    ],
    layout: {
        type: 'fit'
    },
    title: 'EVENTOS Y SUB-EVENTOS',
    border: false,
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        var mainEvent = this.recordMainEvent;
        var subEvent = this.recordSubEvent;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    layout: {
                        type: 'fit'
                    },
                    fieldDefaults: {
                        labelCls: 'changeSizeFontToEightPt',
                        fieldCls: 'changeSizeFontToEightPt'
                    },
                    bodyPadding: '0 10 0 10',
                    title: '',
                    items: [
                        {
                            xtype: 'container',
                            layout: {
                                type: 'anchor'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    padding: 5,
                                    title: '<b>INFORMACION DEL EVENTO PRINCIPAL</b>',
                                    layout: 'hbox',
                                    border: 2,
                                    style: {
                                        borderColor: '#78abf5',
                                        background: '#edf6ff'
                                    },
                                    height: 60,
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            flex: 3,
                                            labelWidth: 125,
                                            fieldLabel: 'DESCRIPCI&Oacute;N',
                                            name: 'descriptionShortMainEvent',
                                            itemId: 'descriptionShortMainEvent',
                                            fieldCls: "readOnlyText",
                                            maxLength: 300
                                        },
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'CODIGO',
                                            padding: '0 0 0 5',
                                            readOnly: true,
                                            labelWidth: 80,
                                            flex: 1,
                                            fieldCls: 'codeIncident',
                                            name: 'codeMainEvent',
                                            itemId: 'codeMainEvent'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    padding: 5,
                                    title: '<b>INFORMACION DEL SUB EVENTO</b>',
                                    layout: 'anchor',
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: 'hbox',
                                            height: 26,
                                            items: [
                                                {
                                                    xtype: 'UpperCaseTextFieldObligatory',
                                                    flex: 6,
                                                    labelWidth: 125,
                                                    fieldLabel: 'DESCRIPCI&Oacute;N CORTA',
                                                    name: 'descriptionShort',
                                                    itemId: 'descriptionShort',
                                                    fieldCls: "obligatoryTextField",
                                                    maxLength: 300,
                                                    listeners: {
                                                        specialkey: function (f, e) {
                                                            DukeSource.global.DirtyView.focusEventEnterObligatory(f, e, me.down('#descriptionLarge'))
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    fieldLabel: 'CODIGO',
                                                    padding: '0 0 0 5',
                                                    readOnly: true,
                                                    labelWidth: 80,
                                                    flex: 2,
                                                    fieldCls: 'codeIncident',
                                                    name: 'codeEvent',
                                                    itemId: 'codeEvent',
                                                    value: ''
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'UpperCaseTextArea',
                                            anchor: '100%',
                                            labelWidth: 125,
                                            height: 100,
                                            fieldLabel: 'DESCRIPCI&Oacute;N LARGA',
                                            name: 'descriptionLarge',
                                            itemId: 'descriptionLarge',
                                            fieldCls: 'obligatoryTextField',
                                            allowBlank: false,
                                            maxLength: 4000,
                                            listeners: {
                                                specialkey: function (f, e) {
                                                    if (e.getKey() === e.ENTER || e.getKey() === e.TAB) {
                                                        DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#currency'))
                                                    }

                                                }
                                            }
                                        },
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'hbox'
                                            },
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    flex: 1,
                                                    layout: {
                                                        type: 'anchor'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'textfield',
                                                            name: 'fileAttachment',
                                                            itemId: 'fileAttachment',
                                                            value: 'N',
                                                            hidden: true
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            name: 'indicatorAtRisk',
                                                            itemId: 'indicatorAtRisk',
                                                            hidden: true
                                                        },
                                                        {
                                                            xtype:"UpperCaseTextFieldReadOnly",
                                                            name: 'codesRiskAssociated',
                                                            itemId: 'codesRiskAssociated',
                                                            hidden: true
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            name: 'numberRiskAssociated',
                                                            itemId: 'numberRiskAssociated',
                                                            value: '0',
                                                            hidden: true
                                                        },
                                                        {
                                                            xtype: 'datefield',
                                                            format: 'd/m/Y',
                                                            width: 240,
                                                            labelWidth: 125,
                                                            disabled: true,
                                                            hidden: true,
                                                            name: 'dateProcess',
                                                            itemId: 'dateProcess'

                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            hidden: true,
                                                            name: 'idIncident',
                                                            itemId: 'idIncident'
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            name: 'numberActionPlan',
                                                            itemId: 'numberActionPlan',
                                                            value: '0',
                                                            hidden: true
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            name: 'codesActionPlan',
                                                            itemId: 'codesActionPlan',
                                                            hidden: true
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            hidden: true,
                                                            name: 'idDetailIncidents'
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            hidden: true,
                                                            value: 'id',
                                                            itemId: 'idEvent',
                                                            name: 'idEvent'
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            hidden: true,
                                                            name: 'sequenceEventState',
                                                            itemId: 'sequence'
                                                        },
                                                        {
                                                            xtype: 'container',
                                                            anchor: '100%',
                                                            height: 27,
                                                            layout: {
                                                                type: 'hbox'
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'combobox',
                                                                    flex: 1,
                                                                    labelWidth: 125,
                                                                    fieldLabel: getName('EVN_WER_CBX_EventType'),
                                                                    msgTarget: 'side',
                                                                    fieldCls: 'obligatoryTextField',
                                                                    displayField: 'description',
                                                                    valueField: 'idTypeEvent',

                                                                    forceSelection: true,
                                                                    editable: false,
                                                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                                    allowBlank: false,
                                                                    name: 'eventType',
                                                                    itemId: 'eventType',
                                                                    store: {
                                                                        fields: ["idTypeEvent", "description"],
                                                                        proxy: {
                                                                            actionMethods: {
                                                                                create: "POST",
                                                                                read: "POST",
                                                                                update: "POST"
                                                                            },
                                                                            type: "ajax",
                                                                            url: "http://localhost:9000/giro/showListTypeEventActivesComboBox.htm",
                                                                            extraParams: {
                                                                                propertyOrder: "description"
                                                                            },
                                                                            reader: {
                                                                                type: "json",
                                                                                root: "data",
                                                                                successProperty: "success"
                                                                            }
                                                                        }
                                                                    },
                                                                    listeners: {
                                                                        specialkey: function (f, e) {
                                                                            DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#lossType'))
                                                                        },
                                                                        select: function (cbo) {
                                                                            if (cbo.getValue() == '3') {
                                                                                me.down('#contSearchEvent').setVisible(true);
                                                                            }
                                                                            else {
                                                                                me.down('#contSearchEvent').setVisible(false);
                                                                            }
                                                                        }
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'combobox',
                                                                    flex: 0.6,
                                                                    labelWidth: 50,
                                                                    padding: '0 0 0 5',
                                                                    fieldLabel: 'ESTADO',
                                                                    editable: false,
                                                                    name: 'eventState',
                                                                    itemId: 'eventState',
                                                                    fieldCls: 'obligatoryTextField',
                                                                    displayField: 'description',
                                                                    valueField: 'id',
                                                                    store: {
                                                                        fields: ['id', 'description', 'sequence'],
                                                                        proxy: {
                                                                            actionMethods: {
                                                                                create: 'POST',
                                                                                read: 'POST',
                                                                                update: 'POST'
                                                                            },
                                                                            type: 'ajax',
                                                                            url: 'http://localhost:9000/giro/findStateIncident.htm',
                                                                            extraParams: {
                                                                                propertyFind: 'si.typeIncident',
                                                                                valueFind: DukeSource.global.GiroConstants.EVENT,
                                                                                propertyOrder: 'si.sequence'
                                                                            },
                                                                            reader: {
                                                                                type: 'json',
                                                                                root: 'data',
                                                                                successProperty: 'success'
                                                                            }
                                                                        }
                                                                    },
                                                                    listeners: {
                                                                        select: function (cbo) {
                                                                            var record = cbo.findRecord(cbo.valueField || cbo.displayField, cbo.getValue());
                                                                            var index = cbo.store.indexOf(record);
                                                                            me.down('#sequence').setValue(cbo.store.data.items[index].data.sequence);

                                                                        }
                                                                    }

                                                                }
                                                            ]
                                                        },
                                                        {
                                                            xtype: "combobox",
                                                            allowBlank: false,
                                                            editable: false,
                                                            typeAhead: true,
                                                            forceSelection: true,
                                                            msgTarget: "side",
                                                            fieldCls: "obligatoryTextField",
                                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                            anchor: "100%",
                                                            fieldLabel: "AGENCIA ORIGEN",
                                                            plugins: ['ComboSelectCount'],
                                                            name: "agency",
                                                            itemId: "agency",
                                                            flex: 1,
                                                            labelWidth: 125,
                                                            displayField: "description",
                                                            valueField: "idAgency",
                                                            fieldStyle: "text-transform:uppercase;font-size:8pt;",
                                                            store: {
                                                                fields: ["idAgency", "description"],
                                                                proxy: {
                                                                    actionMethods: {
                                                                        create: "POST",
                                                                        read: "POST",
                                                                        update: "POST"
                                                                    },
                                                                    type: "ajax",
                                                                    url: "http://localhost:9000/giro/showListAgencyActivesComboBox.htm",
                                                                    extraParams: {
                                                                        propertyOrder: "a.description"
                                                                    },
                                                                    reader: {
                                                                        type: "json",
                                                                        root: "data",
                                                                        successProperty: "success"
                                                                    }
                                                                }
                                                            },
                                                            listeners: {
                                                                specialkey: function (c, d) {
                                                                    DukeSource.global.DirtyView.focusEventEnterObligatory(c, d, me.down("#workArea"))
                                                                }
                                                            }
                                                        },
                                                        {
                                                            xtype: 'combobox',
                                                            queryMode: 'remote',
                                                            anchor: '100%',
                                                            labelWidth: 125,
                                                            fieldLabel: 'PROCESO',
                                                            plugins: ['ComboSelectCount'],
                                                            name: 'process',
                                                            itemId: 'process',
                                                            msgTarget: 'side',
                                                            fieldStyle: "text-transform:uppercase;font-size:8pt;",
                                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                            allowBlank: blank('EWProcess'),
                                                            fieldCls: styleField('EWProcess'),
                                                            forceSelection: true,
                                                            displayField: 'description',
                                                            valueField: 'idProcess',
                                                            store: {
                                                                fields: ['idProcess', 'description'],
                                                                proxy: {
                                                                    actionMethods: {
                                                                        create: "POST",
                                                                        read: "POST",
                                                                        update: "POST"
                                                                    },
                                                                    type: "ajax",
                                                                    url: "http://localhost:9000/giro/showListProcessActivesComboBox.htm",
                                                                    extraParams: {
                                                                        propertyOrder: "description",
                                                                        start: 0,
                                                                        limit: 9999
                                                                    },
                                                                    reader: {
                                                                        type: "json",
                                                                        root: "data",
                                                                        successProperty: "success"
                                                                    }
                                                                }
                                                            },
                                                            listeners: {
                                                                specialkey: function (f, e) {
                                                                    DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#idSubProcess'))
                                                                },
                                                                select: function (cbo) {
                                                                    var panel = cbo.up('window');
                                                                    var comboSubProcess = panel.down('#idSubProcess');
                                                                    comboSubProcess.getStore().load({
                                                                        url: 'http://localhost:9000/giro/showListSubProcessActives.htm',
                                                                        params: {
                                                                            valueFind: cbo.getValue()
                                                                        },
                                                                        callback: function (cbo) {
                                                                            comboSubProcess.reset();
                                                                        }
                                                                    });
                                                                    comboSubProcess.setDisabled(false);
                                                                }
                                                            }
                                                        },
                                                        {
                                                            xtype: 'combobox',
                                                            disabled: true,
                                                            editable: false,
                                                            anchor: '100%',
                                                            labelWidth: 125,
                                                            fieldLabel: 'SUBPROCESO',
                                                            msgTarget: 'side',
                                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                            allowBlank: blank('EWSubProcess'),
                                                            fieldCls: styleField('EWSubProcess'),
                                                            forceSelection: true,
                                                            queryMode: 'local',
                                                            displayField: 'description',
                                                            valueField: 'idSubProcess',
                                                            name: 'idSubProcess',
                                                            itemId: 'idSubProcess',
                                                            store: {
                                                                fields: ["idSubProcess", "description"],
                                                                autoLoad: false,
                                                                proxy: {
                                                                    actionMethods: {
                                                                        create: "POST",
                                                                        read: "POST",
                                                                        update: "POST"
                                                                    },
                                                                    type: "ajax",
                                                                    url: "http://localhost:9000/giro/data/loadGridDefault.htm",
                                                                    extraParams: {
                                                                        propertyOrder: "description"
                                                                    },
                                                                    reader: {
                                                                        type: "json",
                                                                        root: "data",
                                                                        successProperty: "success"
                                                                    }
                                                                }
                                                            },
                                                            listeners: {
                                                                specialkey: function (f, e) {
                                                                    DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#idBusinessLineOne'))
                                                                }
                                                            }

                                                        },
                                                        {
                                                            xtype: "combobox",
                                                            anchor: '100%',
                                                            labelWidth: 125,
                                                            fieldLabel: "&Aacute;REA ORIGEN ",
                                                            plugins: ['ComboSelectCount'],
                                                            name: "workArea",
                                                            itemId: "workArea",
                                                            msgTarget: 'side',
                                                            editable: false,
                                                            forceSelection: true,
                                                            allowBlank: false,
                                                            fieldCls: 'obligatoryTextField',
                                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                            displayField: "description",
                                                            valueField: "idWorkArea",
                                                            fieldStyle: "text-transform:uppercase",
                                                            store: {
                                                                fields: ["idWorkArea", "description"],
                                                                proxy: {
                                                                    actionMethods: {
                                                                        create: "POST",
                                                                        read: "POST",
                                                                        update: "POST"
                                                                    },
                                                                    type: "ajax",
                                                                    url: "http://localhost:9000/giro/showListWorkAreaActivesComboBox.htm",
                                                                    extraParams: {
                                                                        propertyOrder: "description"
                                                                    },
                                                                    reader: {
                                                                        type: "json",
                                                                        root: "data",
                                                                        successProperty: "success"
                                                                    }
                                                                }
                                                            },
                                                            listeners: {
                                                                specialkey: function (c, d) {
                                                                    DukeSource.global.DirtyView.focusEventEnterObligatory(c, d, me.down("#process"))
                                                                },
                                                                select: function (cbo) {
                                                                    var panel = cbo.up('window');
                                                                    var unity = panel.down('#unity');
                                                                    unity.getStore().load({
                                                                        url: 'http://localhost:9000/giro/findWorkArea.htm',
                                                                        params: {
                                                                            propertyFind: 'levelArea',
                                                                            valueFind: cbo.getValue(),
                                                                            propertyOrder: 'description'
                                                                        },
                                                                        callback: function (cbo) {
                                                                            unity.reset();
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        },
                                                        {
                                                            xtype: 'combobox',
                                                            anchor: '100%',
                                                            labelWidth: 125,
                                                            fieldLabel: 'FACTOR ROp',
                                                            msgTarget: 'side',
                                                            fieldCls: 'obligatoryTextField',
                                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                            allowBlank: false,
                                                            editable: false,
                                                            forceSelection: true,
                                                            name: 'factorRisk',
                                                            itemId: 'factorRisk',
                                                            displayField: "description",
                                                            valueField: "idFactorRisk",
                                                            fieldStyle: "text-transform:uppercase",
                                                            store: {
                                                                fields: ["idFactorRisk", "description", "state"],
                                                                proxy: {
                                                                    actionMethods: {
                                                                        create: "POST",
                                                                        read: "POST",
                                                                        update: "POST"
                                                                    },
                                                                    type: "ajax",
                                                                    url: "http://localhost:9000/giro/showListFactorRiskActivesComboBox.htm",
                                                                    extraParams: {
                                                                        propertyOrder: "description"
                                                                    },
                                                                    reader: {
                                                                        type: "json",
                                                                        root: "data",
                                                                        successProperty: "success"
                                                                    }
                                                                }
                                                            },
                                                            listeners: {
                                                                specialkey: function (f, e) {
                                                                    DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#eventOne'))
                                                                }
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'container',
                                                    flex: 1.2,
                                                    anchor: '100%',
                                                    padding: '0 0 0 10',
                                                    layout: {
                                                        type: 'anchor'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'container',
                                                            itemId: 'containerUserReport',
                                                            height: 26,
                                                            layout: 'hbox',
                                                            items: [
                                                                {
                                                                    xtype: 'textfield',
                                                                    name: 'userReport',
                                                                    itemId: 'userReport',
                                                                    hidden: true
                                                                },
                                                                {
                                                                    xtype:"UpperCaseTextFieldReadOnly",
                                                                    flex: 1,
                                                                    labelWidth: 120,
                                                                    allowBlank: false,
                                                                    name: 'fullNameUserReport',
                                                                    itemId: 'fullNameUserReport',
                                                                    fieldLabel: 'QUIEN REPORTÓ'
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    iconCls: 'search',
                                                                    handler: function () {
                                                                        var win = Ext.create('DukeSource.view.risk.util.search.SearchUser', {
                                                                            modal: true
                                                                        }).show();
                                                                        win.down('grid').on('itemdblclick', function () {
                                                                            me.down('textfield[name=userReport]').setValue(win.down('grid').getSelectionModel().getSelection()[0].get('userName'));
                                                                            me.down('UpperCaseTextFieldReadOnly[name=fullNameUserReport]').setValue(win.down('grid').getSelectionModel().getSelection()[0].get('fullName'));
                                                                            win.close();
                                                                        });
                                                                    }
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            xtype: 'container',
                                                            layout: {
                                                                type: 'hbox',
                                                                align: 'stretch'
                                                            },
                                                            items: [
                                                                {
                                                                    xtype: 'container',
                                                                    flex: 1,
                                                                    layout: 'anchor',
                                                                    items: [
                                                                        {
                                                                            xtype: 'combobox',
                                                                            padding: '0 5 0 0',
                                                                            labelWidth: 120,
                                                                            anchor: '100%',
                                                                            fieldLabel: 'MONEDA',
                                                                            msgTarget: 'side',
                                                                            fieldCls: 'obligatoryTextField',
                                                                            displayField: 'description',
                                                                            valueField: 'idCurrency',

                                                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                                            allowBlank: false,
                                                                            editable: false,
                                                                            forceSelection: true,
                                                                            name: 'currency',
                                                                            itemId: 'currency',
                                                                            store: {
                                                                                fields: ['idCurrency', 'description'],
                                                                                proxy: {
                                                                                    actionMethods: {
                                                                                        create: "POST",
                                                                                        read: "POST",
                                                                                        update: "POST"
                                                                                    },
                                                                                    type: "ajax",
                                                                                    url: "http://localhost:9000/giro/showListCurrencyActivesComboBox.htm",
                                                                                    extraParams: {
                                                                                        propertyOrder: "description"
                                                                                    },
                                                                                    reader: {
                                                                                        type: "json",
                                                                                        root: "data",
                                                                                        successProperty: "success"
                                                                                    }
                                                                                }
                                                                            }
                                                                        },
                                                                        {
                                                                            xtype: 'NumberDecimalNumberObligatory',
                                                                            labelWidth: 120,
                                                                            padding: '0 5 0 0',
                                                                            anchor: '100%',
                                                                            decimalPrecision: 4,
                                                                            fieldLabel: 'TIPO DE CAMBIO',
                                                                            name: 'typeChange',
                                                                            itemId: 'typeChange'
                                                                        },
                                                                        {
                                                                            xtype: 'NumberDecimalNumberObligatory',
                                                                            labelWidth: 120,
                                                                            padding: '0 5 0 0',
                                                                            anchor: '100%',
                                                                            fieldLabel: 'MONTO',
                                                                            name: 'amountOrigin',
                                                                            itemId: 'amountOrigin',
                                                                            fieldCls: 'readOnlyText',
                                                                            listeners: {
                                                                                specialkey: function (f, e) {
                                                                                    if (e.getKey() === e.ENTER || e.getKey() === e.TAB) {
                                                                                        calculateNetLoss(me);
                                                                                        DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#buttonSearchCta1'));
                                                                                    }
                                                                                },
                                                                                blur: function (field) {
                                                                                    calculateNetLoss(me);
                                                                                }
                                                                            }
                                                                        },
                                                                        {
                                                                            xtype: 'NumberDecimalNumberObligatory',
                                                                            anchor: '100%',
                                                                            labelWidth: 120,
                                                                            padding: '0 5 0 0',
                                                                            fieldLabel: 'P&Eacute;RDIDA BRUTA',
                                                                            name: 'grossLoss',
                                                                            itemId: 'grossLoss',
                                                                            enforceMaxLength: true,
                                                                            fieldCls: 'readOnlyText',
                                                                            readOnly: true,
                                                                            maxLength: 10
                                                                        },
                                                                        {
                                                                            xtype: 'NumberDecimalNumber',
                                                                            anchor: '100%',
                                                                            labelWidth: 120,
                                                                            padding: '0 5 0 0',
                                                                            fieldLabel: 'TOTAL RECUPERO',
                                                                            name: 'totalLossRecovery',
                                                                            itemId: 'totalLossRecovery',
                                                                            enforceMaxLength: true,
                                                                            readOnly: true,
                                                                            fieldCls: 'readOnlyText',
                                                                            maxLength: 10,
                                                                            value: 0,
                                                                            listeners: {
                                                                                specialkey: function (f, e) {
                                                                                    DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('button[name=searchAccountCta2]'));
                                                                                }
                                                                            }
                                                                        },
                                                                        {
                                                                            xtype: 'NumberDecimalNumber',
                                                                            anchor: '100%',
                                                                            labelWidth: 120,
                                                                            padding: '0 5 0 0',
                                                                            allowBlank: true,
                                                                            readOnly: true,
                                                                            fieldCls: 'readOnlyText',
                                                                            name: 'totalAmountSpend',
                                                                            itemId: 'totalAmountSpend',
                                                                            value: 0,
                                                                            fieldLabel: 'GASTOS ASOCIADOS'
                                                                        },
                                                                        {
                                                                            xtype: 'NumberDecimalNumber',
                                                                            anchor: '100%',
                                                                            labelWidth: 120,
                                                                            padding: '0 5 0 0',
                                                                            allowBlank: true,
                                                                            readOnly: true,
                                                                            fieldCls: 'readOnlyText',
                                                                            name: 'amountSubEvent',
                                                                            itemId: 'amountSubEvent',
                                                                            value: 0,
                                                                            fieldLabel: 'SUB-EVENTOS'
                                                                        },
                                                                        {
                                                                            xtype: 'NumberDecimalNumberObligatory',
                                                                            anchor: '100%',
                                                                            labelWidth: 120,
                                                                            padding: '0 5 0 0',
                                                                            fieldLabel: 'PÉRDIDA NETA',
                                                                            name: 'netLoss',
                                                                            itemId: 'netLoss',
                                                                            minValue: -1000000,
                                                                            enforceMaxLength: true,
                                                                            fieldCls: 'readOnlyText',
                                                                            readOnly: true,
                                                                            maxLength: 10
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    xtype: 'container',
                                                                    flex: 1,
                                                                    layout: 'anchor',
                                                                    items: [
                                                                        {
                                                                            xtype: 'datetimefield',
                                                                            format: 'd/m/Y H:i',
                                                                            anchor: '100%',
                                                                            padding: '0 0 0 5',
                                                                            labelWidth: 100,
                                                                            fieldLabel: 'FECHA DE INICIO',
                                                                            name: 'dateOccurrence',
                                                                            enforceMaxLength: true,
                                                                            fieldCls: 'obligatoryTextField',
                                                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                                            allowBlank: false,
                                                                            listeners: {
                                                                                expand: function (field, value) {
                                                                                    if (me.down('datefield[name=dateDiscovery]').getValue() != undefined) {
                                                                                        field.setMaxValue(me.down('datefield[name=dateDiscovery]').getValue());
                                                                                    }
                                                                                    else {
                                                                                        field.setMaxValue(new Date());
                                                                                    }
                                                                                },
                                                                                specialkey: function (f, e) {
                                                                                    DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#dateAccountingEvent'))
                                                                                }
                                                                            }
                                                                        },
                                                                        {
                                                                            xtype: 'datetimefield',
                                                                            format: 'd/m/Y H:i',
                                                                            anchor: '100%',
                                                                            padding: '0 0 0 5',
                                                                            labelWidth: 100,
                                                                            fieldLabel: 'FECHA FIN',
                                                                            name: 'dateEnd',
                                                                            itemId: 'dateEnd',
                                                                            enforceMaxLength: true,
                                                                            fieldCls: 'obligatoryTextField',
                                                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                                            allowBlank: false,
                                                                            listeners: {
                                                                                expand: function (field, value) {
                                                                                    if (me.down('datefield[name=dateDiscovery]').getValue() != undefined) {
                                                                                        field.setMaxValue(me.down('datefield[name=dateDiscovery]').getValue());
                                                                                    }
                                                                                    else {
                                                                                        field.setMaxValue(new Date());
                                                                                    }
                                                                                },
                                                                                specialkey: function (f, e) {
                                                                                    DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#dateAccountingEvent'))
                                                                                }
                                                                            }
                                                                        },
                                                                        {
                                                                            xtype: 'datetimefield',
                                                                            format: 'd/m/Y H:i',
                                                                            padding: '0 0 0 5',
                                                                            labelWidth: 100,
                                                                            anchor: '100%',
                                                                            fieldLabel: 'FECHA DESCUBRIM.',
                                                                            name: 'dateDiscovery',
                                                                            itemId: 'dateDiscovery',
                                                                            enforceMaxLength: true,
                                                                            listeners: {
                                                                                expand: function (field, value) {
                                                                                    if (me.down('datefield[name=dateAcceptLossEvent]').getValue() != undefined) {
                                                                                        field.setMaxValue(me.down('datefield[name=dateAcceptLossEvent]').getValue());
                                                                                    }
                                                                                    else {
                                                                                        field.setMaxValue(new Date());
                                                                                    }
                                                                                    field.setMinValue(me.down('datefield[name=dateOccurrence]').getValue());
                                                                                },
                                                                                specialkey: function (f, e) {
                                                                                    DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#dateOccurrence'))
                                                                                }
                                                                            }
                                                                        },
                                                                        {
                                                                            xtype: 'datetimefield',
                                                                            format: 'd/m/Y H:i',
                                                                            anchor: '100%',
                                                                            padding: '0 0 0 5',
                                                                            labelWidth: 100,
                                                                            fieldLabel: 'FECHA REPORTE',
                                                                            name: 'dateAcceptLossEvent',
                                                                            itemId: 'dateAcceptLossEvent',
                                                                            enforceMaxLength: true,
                                                                            value: new Date(),
                                                                            readOnly: false,
                                                                            editable: true,
                                                                            listeners: {
                                                                                expand: function (field, value) {
                                                                                    if (me.down('datefield[name=dateAccountingEvent]').getValue() != undefined) {
                                                                                        field.setMaxValue(me.down('datefield[name=dateAccountingEvent]').getValue());
                                                                                    }
                                                                                    else {
                                                                                        field.setMaxValue(new Date());
                                                                                    }
                                                                                    field.setMinValue(me.down('datefield[name=dateDiscovery]').getValue());
                                                                                },
                                                                                specialkey: function (f, e) {
                                                                                    DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#dateDiscovery'))
                                                                                }
                                                                            }
                                                                        },
                                                                        {
                                                                            xtype: 'datetimefield',
                                                                            format: 'd/m/Y H:i',
                                                                            anchor: '100%',
                                                                            labelWidth: 100,
                                                                            padding: '0 0 0 5',
                                                                            fieldLabel: 'FECHA CONTABLE',
                                                                            name: 'dateAccountingEvent',
                                                                            itemId: 'dateAccountingEvent',
                                                                            enforceMaxLength: true,
                                                                            listeners: {
                                                                                expand: function (field, value) {
                                                                                    field.setMaxValue(new Date());
                                                                                },
                                                                                specialkey: function (f, e) {
                                                                                    DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#insuranceCompany'))
                                                                                }
                                                                            }
                                                                        },
                                                                        {
                                                                            xtype: 'datetimefield',
                                                                            format: 'd/m/Y H:i',
                                                                            anchor: '100%',
                                                                            labelWidth: 100,
                                                                            padding: '0 0 0 5',
                                                                            fieldLabel: 'FECHA CIERRE',
                                                                            name: 'dateCloseEvent',
                                                                            enforceMaxLength: true,
                                                                            listeners: {
                                                                                expand: function (field, value) {
                                                                                    field.setMinValue(me.down('datefield[name=dateAccountingEvent]').getValue());
                                                                                }
                                                                            }
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        }

                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'UpperCaseTextArea',
                                            anchor: '100%',
                                            labelWidth: 125,
                                            height: 40,
                                            fieldLabel: 'COMENTARIOS',
                                            name: 'comments',
                                            itemId: 'comments',
                                            fieldCls: 'obligatoryTextField',
                                            allowBlank: false,
                                            maxLength: 2000,
                                            listeners: {
                                                specialkey: function (f, e) {
                                                    if (e.getKey() === e.ENTER || e.getKey() === e.TAB) {
                                                        DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#document'))
                                                    }

                                                }
                                            }
                                        },
                                        {
                                            xtype: 'textfield',
                                            itemId: 'eventMain',
                                            name: 'eventMain',
                                            value: 'N',
                                            hidden: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            itemId: 'numberRelation',
                                            name: 'numberRelation',
                                            hidden: true
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'CONVERTIR',
                    iconCls: 'import',
                    scale: 'medium',
                    handler: function (button) {
                        DukeSource.lib.Ajax.request({
                            method: 'POST',
                            url: 'http://localhost:9000/giro/convertEventInSubEvent.htm',
                            params: {
                                idMainEvent: mainEvent['idEvent'],
                                idSubEvent: subEvent['idEvent']
                            },
                            success: function (response) {
                                response = Ext.decode(response.responseText);
                                if (response.success) {
                                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.message, Ext.Msg.INFO);

                                    var grid = Ext.ComponentQuery.query('ViewWindowRegisterSubEvents grid')[0];
                                    grid.down('pagingtoolbar').moveFirst();

                                    me.close();
                                } else {
                                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, response.message, Ext.Msg.WARNING);
                                }
                            },
                            failure: function () {
                            }
                        });
                    }
                },
                {
                    text: 'SALIR',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });
        me.callParent(arguments);
    }
});