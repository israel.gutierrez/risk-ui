Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterEvents",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowRegisterEvents",
    requires: [
      "DukeSource.view.risk.parameter.combos.ViewComboRiskType",
      "Ext.form.Panel",
      "Ext.ux.DateTimeField",
      "Ext.form.field.TextArea",
      "Ext.form.field.ComboBox",
      "Ext.form.field.Checkbox",
      "Ext.form.field.Date",
      "Ext.button.Button",
      "Ext.form.field.File"
    ],
    layout: {
      type: "fit"
    },
    title: "Ingreso de eventos",
    border: false,
    autoScroll: true,
    overflow: "auto",

    coloredButtonActive: function(btn, index) {
      this.setHeight(this.heightMain);
      this.down("form")
        .getLayout()
        .setActiveItem(index);

      this.buttonActive.setText(this.buttonTextActive);
      this.buttonTextActive = btn.getText();

      btn.setText(btn.getText() + '<span style="color:red"> * </span>');
      this.buttonActive = btn;
    },

    calculateNetLoss: function() {
      var typeChange = this.down("#typeChange").getValue();
      var amountOrigin = this.down("#amountOrigin").getValue();
      var grossLoss = this.down("#grossLoss");
      var provision = this.down("#amountProvision").getValue();
      var spend = this.down("#totalAmountSpend").getValue();
      var subEvent = this.down("#amountSubEvent").getValue();
      grossLoss.setValue(
        parseFloat(amountOrigin) * parseFloat(typeChange) + parseFloat(subEvent)
      );
      var lossRecovery = this.down("#lossRecovery").getValue();
      var thirdRecovery = this.down("#amountThirdRecovery").getValue();
      var sureAmountRecovery = this.down("#sureAmountRecovery").getValue();
      var totalRecovery =
        parseFloat(lossRecovery) +
        parseFloat(sureAmountRecovery) +
        parseFloat(thirdRecovery);
      var result =
        parseFloat(grossLoss.getValue()) -
        totalRecovery +
        parseFloat(spend) +
        parseFloat(provision);

      this.down("#amountProvision").setValue(provision);
      this.down("#totalAmountSpend").setValue(spend);
      this.down("#amountSubEvent").setValue(subEvent);
      this.down("#lossRecovery").setValue(lossRecovery);
      this.down("#amountThirdRecovery").setValue(thirdRecovery);
      this.down("#sureAmountRecovery").setValue(sureAmountRecovery);
      this.down("#totalRecovery").setValue(totalRecovery);

      this.down("#netLoss").setValue(result);
    },

    saveEvent: function() {
      this.calculateNetLoss();

      var form = this.down("form");
      var win = this;

      if (form.getForm().isValid()) {
        form.getForm().submit({
          url:
            "http://localhost:9000/giro/saveEventMaster.htm?nameView=ViewPanelEventsRiskOperational",
          waitMsg: "Saving...",
          method: "POST",
          success: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (valor.success) {
              DukeSource.global.DirtyView.messageNormal(valor.message);

              win.doClose();
              win.callback(win.gridParent, win.indexRecordParent);
            } else {
              DukeSource.global.DirtyView.messageWarning(valor.message);
            }
          },
          failure: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (!valor.success) {
              DukeSource.global.DirtyView.messageWarning(valor.message);
            }
          }
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE
        );
      }
    },

    callback: function(grid, index) {
      index = index === undefined || index === -1 ? 0 : index;

      grid.store.load({
        callback: function() {
          grid.getSelectionModel().select(index);
        }
      });
    },

    initComponent: function() {
      var me = this;
      var actionType = this.actionType;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            padding: "0 0 0 2",
            layout: "card",
            autoScroll: true,
            fieldDefaults: {
              labelCls: "changeSizeFontToEightPt",
              fieldCls: "changeSizeFontToEightPt"
            },
            bodyPadding: 10,
            items: [
              {
                xtype: "container",
                itemId: "card-0",
                layout: "fit",
                autoScroll: true,
                items: [
                  {
                    xtype: "fieldset",
                    title: "Datos del Evento",
                    autoScroll: true,
                    items: [
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "codeCorrelative",
                        name: "codeCorrelative"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        value: "N",
                        itemId: "checkApprove",
                        name: "checkApprove"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "userApprove",
                        name: "userApprove"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "commentApprove",
                        name: "commentApprove"
                      },
                      {
                        xtype: "datetimefield",
                        format: "d/m/Y H:i:s",
                        hidden: true,
                        itemId: "dateApprove",
                        name: "dateApprove"
                      },
                      {
                        xtype: "textfield",
                        itemId: "eventMain",
                        name: "eventMain",
                        value: "S",
                        hidden: true
                      },
                      {
                        xtype: "textfield",
                        itemId: "actionType",
                        name: "actionType",
                        value: actionType,
                        hidden: true
                      },
                      {
                        xtype: "textfield",
                        name: "fileAttachment",
                        itemId: "fileAttachment",
                        value: "N",
                        hidden: true
                      },
                      {
                        xtype: "textfield",
                        itemId: "numberRelation",
                        name: "numberRelation",
                        hidden: true
                      },
                      {
                        xtype: "textfield",
                        name: "indicatorAtRisk",
                        itemId: "indicatorAtRisk",
                        hidden: true
                      },
                      {
                        xtype: "UpperCaseTextFieldReadOnly",
                        name: "codesRiskAssociated",
                        itemId: "codesRiskAssociated",
                        hidden: true
                      },
                      {
                        xtype: "textfield",
                        name: "numberRiskAssociated",
                        itemId: "numberRiskAssociated",
                        value: "0",
                        hidden: true
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        name: "idIncident",
                        itemId: "idIncident"
                      },
                      {
                        xtype: "textfield",
                        name: "numberActionPlan",
                        itemId: "numberActionPlan",
                        value: "0",
                        hidden: true
                      },
                      {
                        xtype: "textfield",
                        name: "codesActionPlan",
                        itemId: "codesActionPlan",
                        hidden: true
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        name: "idDetailIncidents"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        value: "id",
                        itemId: "idEvent",
                        name: "idEvent"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "codeEvent",
                        value: DukeSource.global.GiroConstants.DRAFT,
                        name: "codeEvent"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        value: "0",
                        itemId: "year",
                        name: "year"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        name: "sequenceEventState",
                        itemId: "sequenceEventState"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "product",
                        name: "product"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "unity",
                        name: "unity"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "costCenter",
                        name: "costCenter"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "unity2",
                        name: "unity2"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "costCenter2",
                        name: "costCenter2"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "factorRisk",
                        name: "factorRisk"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "codeProcess",
                        name: "codeProcess"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "idProcessType",
                        name: "idProcessType"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "idProcess",
                        name: "idProcess"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "idSubProcess",
                        name: "idSubProcess"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "idActivity",
                        name: "idActivity"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "businessLineOne",
                        name: "businessLineOne"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "businessLineTwo",
                        name: "businessLineTwo"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "businessLineThree",
                        name: "businessLineThree"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "eventOne",
                        name: "eventOne"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "eventTwo",
                        name: "eventTwo"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "eventThree",
                        name: "eventThree"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        value: "A",
                        itemId: "stateTemp",
                        name: "stateTemp"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "order",
                        value: 1,
                        name: "order"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "state",
                        value: "S",
                        name: "state"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "descriptionMotiveRefused",
                        name: "descriptionMotiveRefused"
                      },
                      {
                        xtype: "textfield",
                        name: "userRegister",
                        itemId: "userRegister",
                        value: userName,
                        hidden: true
                      },
                      {
                        xtype: "textfield",
                        name: "analystRegister",
                        itemId: "analystRegister",
                        value: userName,
                        hidden: true
                      },
                      {
                        xtype: "datetimefield",
                        format: "d/m/Y H:i:s",
                        hidden: true,
                        value: new Date(),
                        name: "dateRegister",
                        itemId: "dateRegister"
                      },
                      {
                        xtype: "datefield",
                        format: "d/m/Y H:i:s",
                        hidden: true,
                        value: new Date(),
                        name: "dateAnalystRegister",
                        itemId: "dateAnalystRegister"
                      },
                      {
                        xtype: "NumberDecimalNumber",
                        hidden: true,
                        name: "amountPayment",
                        itemId: "amountPayment",
                        value: 0
                      },
                      {
                        xtype: "UpperCaseTextFieldObligatory",
                        flex: 4,
                        anchor: "100%",
                        fieldLabel: "Descripción corta",
                        name: "descriptionShort",
                        itemId: "descriptionShort",
                        fieldCls: styleField("EVN_WER_TXF_DescriptionShort"),
                        hidden: hidden("EVN_WER_TXF_DescriptionShort"),
                        allowBlank: blank("EVN_WER_TXF_DescriptionShort"),
                        maxLength: 800
                      },
                      {
                        xtype: "UpperCaseTextArea",
                        anchor: "100%",
                        height: 30,
                        minLength: 10,
                        fieldLabel: getName("EVN_WER_TXF_DescriptionLarge"),
                        name: "descriptionLarge",
                        itemId: "descriptionLarge",
                        fieldCls: "obligatoryTextField",
                        allowBlank: false,
                        maxLength: blank(
                          "Exception_size_EVN_WER_TXF_DescriptionLarge"
                        )
                      },
                      {
                        xtype: "container",
                        layout: "hbox",
                        items: [
                          {
                            xtype: "container",
                            items: [
                              {
                                xtype: "combobox",
                                fieldLabel: getName("EVN_WER_CBX_EventType"),
                                msgTarget: "side",
                                fieldCls: "obligatoryTextField",
                                displayField: "description",
                                valueField: "idTypeEvent",
                                forceSelection: true,
                                editable: false,
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                allowBlank: false,
                                name: "eventType",
                                itemId: "eventType",
                                store: {
                                  fields: ["idTypeEvent", "description"],
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "http://localhost:9000/giro/showListTypeEventActivesComboBox.htm",
                                    extraParams: {
                                      propertyOrder: "description"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                }
                              },
                              {
                                xtype: "combobox",
                                fieldLabel: "Estado",
                                editable: false,
                                name: "eventState",
                                itemId: "eventState",
                                fieldCls: "obligatoryTextField",
                                displayField: "description",
                                valueField: "id",
                                allowBlank: false,
                                store: {
                                  fields: ["id", "description", "sequence"],
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "http://localhost:9000/giro/findStateIncident.htm",
                                    extraParams: {
                                      propertyFind: "si.typeIncident",
                                      valueFind:
                                        DukeSource.global.GiroConstants.EVENT,
                                      propertyOrder: "si.sequence"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                },
                                listeners: {
                                  select: function(cbo) {
                                    var record = cbo.findRecord(
                                      cbo.valueField || cbo.displayField,
                                      cbo.getValue()
                                    );
                                    var index = cbo.store.indexOf(record);
                                    me.down("#sequenceEventState").setValue(
                                      cbo.store.data.items[index].data.sequence
                                    );
                                  }
                                }
                              },
                              {
                                xtype: "combobox",
                                anchor: "100%",
                                fieldLabel: getName("EVN_WER_CBX_EventClass"),
                                editable: false,
                                hidden: hidden("EVN_WER_CBX_EventClass"),
                                name: "eventClass",
                                itemId: "eventClass",
                                queryMode: "local",
                                displayField: "description",
                                valueField: "value",
                                store: {
                                  fields: ["value", "description"],
                                  pageSize: 9999,
                                  autoLoad: true,
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                                    extraParams: {
                                      propertyOrder: "description",
                                      propertyFind: "identified",
                                      valueFind: "CLASSEVENT"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                }
                              },
                              {
                                xtype: "combobox",
                                msgTarget: "side",
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                fieldLabel: "Agencia",
                                plugins: ["ComboSelectCount"],
                                name: "agency",
                                itemId: "agency",
                                flex: 1,
                                displayField: "description",
                                valueField: "idAgency",
                                store: {
                                  fields: ["idAgency", "description"],
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "http://localhost:9000/giro/showListAgencyActivesComboBox.htm",
                                    extraParams: {
                                      propertyOrder: "a.description"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                }
                              }
                            ]
                          },
                          {
                            xtype: "container",
                            padding: "0 0 0 10",
                            items: [
                              {
                                xtype: "datetimefield",
                                format: "d/m/Y H:i",
                                labelWidth: 130,
                                fieldLabel: getName("EWDateOccurrence"),
                                name: "dateOccurrence",
                                itemId: "dateOccurrence",
                                enforceMaxLength: true,
                                fieldCls: "obligatoryTextField",
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                allowBlank: false,
                                listeners: {
                                  expand: function(field, value) {
                                    field.setMaxValue(new Date());
                                  },
                                  blur: function(field, value) {
                                    field.setMaxValue(new Date());
                                  }
                                }
                              },
                              {
                                xtype: "datetimefield",
                                format: "d/m/Y H:i",
                                labelWidth: 130,
                                fieldLabel: "Fecha de descubrimiento",
                                name: "dateDiscovery",
                                itemId: "dateDiscovery",
                                enforceMaxLength: true,
                                listeners: {
                                  expand: function(field, value) {
                                    field.setMinValue(
                                      me.down("#dateOccurrence").getValue()
                                    );
                                    field.setMaxValue(new Date());
                                  },
                                  blur: function(field, value) {
                                    field.setMinValue(
                                      me.down("#dateOccurrence").getValue()
                                    );
                                    field.setMaxValue(new Date());
                                  }
                                }
                              },
                              {
                                xtype: "datetimefield",
                                format: "d/m/Y H:i",
                                labelWidth: 130,
                                fieldLabel: "Fecha fin",
                                name: "dateEnd",
                                itemId: "dateEnd",
                                hidden: true,
                                allowBlank: blank("EWDateEnd"),
                                fieldCls: styleField("EWDateEnd"),
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                listeners: {
                                  expand: function(field, value) {
                                    field.setMinValue(
                                      me.down("#dateOccurrence").getValue()
                                    );
                                    field.setMaxValue(new Date());
                                  },
                                  blur: function(field, value) {
                                    field.setMinValue(
                                      me.down("#dateOccurrence").getValue()
                                    );
                                    field.setMaxValue(new Date());
                                  },
                                  specialkey: function(f, e) {
                                    DukeSource.global.DirtyView.focusEventEnter(
                                      f,
                                      e,
                                      me.down("#dateAccountingEvent")
                                    );
                                  }
                                }
                              },
                              {
                                xtype: "datetimefield",
                                format: "d/m/Y H:i",
                                labelWidth: 130,
                                hidden: true,
                                fieldLabel: "Fecha de cierre",
                                name: "dateCloseEvent",
                                enforceMaxLength: true,
                                listeners: {
                                  expand: function(field, value) {
                                    field.setMinValue(
                                      me.down("#dateOccurrence").getValue()
                                    );
                                    field.setMaxValue(new Date());
                                  },
                                  blur: function(field, value) {
                                    field.setMinValue(
                                      me.down("#dateOccurrence").getValue()
                                    );
                                    field.setMaxValue(new Date());
                                  }
                                }
                              },
                              {
                                xtype: "datetimefield",
                                format: "d/m/Y H:i",
                                labelWidth: 130,
                                fieldLabel: "Fecha de reporte",
                                name: "dateAcceptLossEvent",
                                itemId: "dateAcceptLossEvent",
                                enforceMaxLength: true,
                                value: new Date(),
                                fieldCls: "obligatoryTextField",
                                allowBlank: false,
                                editable: true,
                                listeners: {
                                  expand: function(field, value) {
                                    field.setMinValue(
                                      me.down("#dateOcurrence").getValue()
                                    );
                                    field.setMaxValue(new Date());
                                  },
                                  blur: function(field, value) {
                                    field.setMinValue(
                                      me.down("#dateOccurrence").getValue()
                                    );
                                    field.setMaxValue(new Date());
                                  },
                                  select: function(field, value) {
                                    me.down("#year").setValue(
                                      value.getFullYear()
                                    );
                                  }
                                }
                              }
                            ]
                          }
                        ]
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        height: 18,
                        fieldLabel: "Proceso afectado",
                        itemId: "descriptionProcess",
                        name: "descriptionProcess",
                        allowBlank: false,
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeProcess",
                                {
                                  backWindow: me
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        fieldLabel: getName("EWCBUnity"),
                        itemId: "descriptionUnity",
                        name: "descriptionUnity",
                        height: 18,
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeOrganization",
                                {
                                  backWindow: me,
                                  descriptionUnity: "descriptionUnity",
                                  unity: "unity",
                                  costCenter: "costCenter"
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        fieldLabel: getName("EVN_WRE_DSF_DescriptionUnity2"),
                        hidden: hidden("EVN_WRE_DSF_DescriptionUnity2"),
                        itemId: "descriptionUnity2",
                        name: "descriptionUnity2",
                        height: 18,
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeOrganization",
                                {
                                  backWindow: me,
                                  descriptionUnity: "descriptionUnity2",
                                  unity: "unity2",
                                  costCenter: "costCenter2"
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        height: 18,
                        fieldLabel: "Factor de riesgo",
                        itemId: "descriptionFactorRisk",
                        name: "descriptionFactorRisk",
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeFactorRisk",
                                {
                                  winParent: me
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        height: 18,
                        hidden: hidden("EVN_WER_DSF_DescriptionBusinessLine"),
                        itemId: "descriptionBusinessLineOne",
                        name: "descriptionBusinessLineOne",
                        fieldLabel: "L&iacute;nea de negocio",
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        allowBlank: false,
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeBusinessLine",
                                {
                                  winParent: me
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        itemId: "descriptionEventOne",
                        name: "descriptionEventOne",
                        fieldLabel: "Evento de riesgo",
                        height: 18,
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeTypeEvent",
                                {
                                  winParent: me
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        fieldLabel: "Producto afectado",
                        hidden: hidden("EVN_WER_DSF_DescriptionProduct"),
                        itemId: "descriptionProduct",
                        name: "descriptionProduct",
                        height: 18,
                        allowBlank: false,
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeProduct",
                                {
                                  winParent: me
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "combobox",
                        fieldLabel: getName("EWCBChannelAttention"),
                        hidden: hidden("EWCBChannelAttention"),
                        editable: false,
                        name: "channelAttention",
                        itemId: "channelAttention",
                        queryMode: "local",
                        displayField: "description",
                        valueField: "value",
                        store: {
                          fields: ["value", "description"],
                          pageSize: 9999,
                          autoLoad: true,
                          proxy: {
                            actionMethods: {
                              create: "POST",
                              read: "POST",
                              update: "POST"
                            },
                            type: "ajax",
                            url:
                              "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                            extraParams: {
                              propertyOrder: "description",
                              propertyFind: "identified",
                              valueFind: "CHANNELATTENTION"
                            },
                            reader: {
                              type: "json",
                              root: "data",
                              successProperty: "success"
                            }
                          }
                        }
                      },
                      {
                        xtype: "checkboxfield",
                        hidden: hidden("EWCBCriminal"),
                        name: "isSteal",
                        itemId: "isSteal",
                        fieldLabel: getName("EWCBCriminal"),
                        inputValue: "S",
                        uncheckedValue: "N",
                        value: "N"
                      },
                      {
                        xtype: "checkboxfield",
                        hidden: hidden("EWIsCritic"),
                        fieldLabel: getName("EWIsCritic"),
                        name: "isCritic",
                        itemId: "isCritic",
                        inputValue: "S",
                        uncheckedValue: "N",
                        value: "N",
                        listeners: {
                          change: function(cbo) {
                            if (cbo.getValue() === true) {
                              me.down("#descriptionCriticEvent").setVisible(
                                true
                              );
                            } else {
                              me.down("#descriptionCriticEvent").setVisible(
                                false
                              );
                            }
                          }
                        }
                      },
                      {
                        xtype: "combobox",
                        anchor: "100%",
                        fieldLabel: getName("EWCBEffectOtherRisk"),
                        hidden: hidden("EWCBEffectOtherRisk"),
                        allowBlank: blank("EWEffectOtherRisk"),
                        fieldCls: styleField("EWEffectOtherRisk"),
                        msgTarget: "side",
                        displayField: "description",
                        valueField: "value",

                        editable: false,
                        name: "effectOtherRisk",
                        itemId: "effectOtherRisk",
                        store: {
                          fields: ["value", "description"],
                          pageSize: 9999,
                          autoLoad: true,
                          proxy: {
                            actionMethods: {
                              create: "POST",
                              read: "POST",
                              update: "POST"
                            },
                            type: "ajax",
                            url:
                              "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                            extraParams: {
                              propertyOrder: "description",
                              propertyFind: "identified",
                              valueFind: "EFFECT_OTHER_RISK"
                            },
                            reader: {
                              type: "json",
                              root: "data",
                              successProperty: "success"
                            }
                          }
                        }
                      },
                      {
                        xtype: "container",
                        hidden: hidden("EWCPersonAuthorized"),
                        height: 26,
                        layout: "hbox",
                        items: [
                          {
                            xtype: "textfield",
                            name: "userAuthorized",
                            itemId: "userAuthorized",
                            hidden: true
                          },
                          {
                            xtype: "UpperCaseTextFieldReadOnly",
                            flex: 1,
                            name: "fullNameAuthorized",
                            itemId: "fullNameAuthorized",
                            fieldLabel: "Autorizado por"
                          },
                          {
                            xtype: "button",
                            iconCls: "search",
                            handler: function() {
                              var win = Ext.create(
                                "DukeSource.view.risk.util.search.SearchUser",
                                {
                                  modal: true
                                }
                              ).show();
                              win
                                .down("grid")
                                .on("itemdblclick", function(
                                  grid,
                                  record,
                                  item,
                                  index,
                                  e
                                ) {
                                  me.down("#userAuthorized").setValue(
                                    record.get("userName")
                                  );
                                  me.down("#fullNameAuthorized").setValue(
                                    record.get("fullName")
                                  );
                                  win.close();
                                });
                            }
                          }
                        ]
                      },
                      {
                        xtype: "ViewComboRiskType",
                        fieldLabel: "Riesgo relacionado",
                        labelStyle: "font-size:8pt;",
                        hidden: true,
                        msgTarget: "side",
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        name: "riskType",
                        itemId: "riskType",
                        listeners: {
                          specialkey: function(f, e) {
                            DukeSource.global.DirtyView.focusEventEnter(
                              f,
                              e,
                              me.down("#descriptionShort")
                            );
                          }
                        }
                      },
                      {
                        xtype: "combobox",
                        fieldLabel: "Tipo de pérdida",
                        editable: false,
                        forceSelection: true,
                        hidden: hidden("EWTypeEventLost"),
                        allowBlank: blank("EWTypeEventLost"),
                        fieldCls: styleField("EWTypeEventLost"),
                        name: "typeEventLost",
                        itemId: "typeEventLost",
                        queryMode: "local",
                        displayField: "description",
                        valueField: "value",
                        store: {
                          fields: ["value", "description"],
                          pageSize: 9999,
                          autoLoad: true,
                          proxy: {
                            actionMethods: {
                              create: "POST",
                              read: "POST",
                              update: "POST"
                            },
                            type: "ajax",
                            url:
                              "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                            extraParams: {
                              propertyOrder: "description",
                              propertyFind: "identified",
                              valueFind: "EVENT_LOSS_TYPE"
                            },
                            reader: {
                              type: "json",
                              root: "data",
                              successProperty: "success"
                            }
                          }
                        }
                      },
                      {
                        xtype: "UpperCaseTextArea",
                        anchor: "100%",
                        height: 30,
                        fieldLabel: "Comentarios",
                        name: "comments",
                        itemId: "comments",
                        allowBlank: blank("EWComments"),
                        fieldCls: styleField("EWComments"),
                        maxLength: 2000,
                        listeners: {
                          specialkey: function(f, e) {
                            if (
                              e.getKey() === e.ENTER ||
                              e.getKey() === e.TAB
                            ) {
                              DukeSource.global.DirtyView.focusEventEnter(
                                f,
                                e,
                                me.down("#document")
                              );
                            }
                          }
                        }
                      },
                      {
                        xtype: "filefield",
                        anchor: "100%",
                        fieldLabel: "Documento adjunto",
                        name: "document",
                        itemId: "document",
                        buttonConfig: {
                          iconCls: "tesla even-attachment"
                        },
                        buttonText: ""
                      }
                    ]
                  }
                ]
              },
              {
                xtype: "container",
                itemId: "card-1",
                layout: "fit",
                autoScroll: true,
                items: [
                  {
                    xtype: "fieldset",
                    title: "Impacto",
                    autoScroll: true,
                    items: [
                      {
                        xtype: "container",
                        height: 26,
                        layout: "hbox",
                        items: [
                          {
                            xtype: "combobox",
                            fieldLabel: "Moneda",
                            msgTarget: "side",
                            displayField: "description",
                            plugins: ["ComboSelectCount"],
                            valueField: "idCurrency",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            allowBlank: false,
                            editable: false,
                            queryMode: "local",
                            forceSelection: true,
                            name: "currency",
                            itemId: "currency",
                            store: {
                              autoLoad: true,
                              fields: ["idCurrency", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url:
                                  "http://localhost:9000/giro/showListCurrencyActivesComboBox.htm",
                                extraParams: {
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              select: function(cbo) {
                                var typeChange = me.down("#typeChange");
                                if (cbo.getValue() === "1") {
                                  typeChange.setValue(1);
                                  typeChange.setReadOnly(true);
                                } else {
                                  typeChange.setValue("");
                                  typeChange.setReadOnly(false);
                                }
                              },
                              render: function(cbo) {
                                cbo.getStore().load({
                                  callback: function() {
                                    cbo.setValue("1");
                                  }
                                });
                              }
                            }
                          },
                          {
                            xtype: "NumberDecimalNumberObligatory",
                            maxValue: 9999,
                            padding: "0 0 0 10",
                            value: 1,
                            decimalPrecision: 4,
                            fieldLabel: "Tipo de cambio",
                            name: "typeChange",
                            itemId: "typeChange",
                            listeners: {
                              specialkey: function(f, e) {
                                if (
                                  e.getKey() === e.ENTER ||
                                  e.getKey() === e.TAB
                                ) {
                                  me.calculateNetLoss();
                                }
                              },
                              blur: function() {
                                me.calculateNetLoss();
                              }
                            }
                          }
                        ]
                      },
                      {
                        xtype: "NumberDecimalNumberObligatory",
                        fieldLabel: "Monto de pérdida",
                        name: "amountOrigin",
                        itemId: "amountOrigin",
                        maxLength: 13,
                        value: 0,
                        listeners: {
                          specialkey: function(f, e) {
                            if (
                              e.getKey() === e.ENTER ||
                              e.getKey() === e.TAB
                            ) {
                              me.calculateNetLoss();
                              DukeSource.global.DirtyView.focusEventEnter(
                                f,
                                e,
                                me.down("#buttonSearchCta1")
                              );
                            }
                          },
                          blur: function() {
                            me.calculateNetLoss();
                          }
                        }
                      },
                      {
                        xtype: "fieldset",
                        layout: "anchor",
                        collapsible: true,
                        collapsed: true,
                        title: "Informe sobre el impacto",
                        items: [
                          {
                            xtype: "textfield",
                            fieldLabel:
                              "¿Se requirió Horas extras por parte de los colaboradores para reprocesar datos asociados a este evento?",
                            labelAlign: "top",
                            anchor: "100%",
                            maxLength: 300,
                            name: "descriptionCriticEvent",
                            itemId: "descriptionCriticEvent"
                          },
                          {
                            xtype: "textfield",
                            fieldLabel:
                              "¿Se requirió procesamientos por parte de algún proveedor?",
                            labelAlign: "top",
                            anchor: "100%",
                            maxLength: 300,
                            name: "descriptionStateEvent",
                            itemId: "descriptionStateEvent"
                          },
                          {
                            xtype: "textfield",
                            fieldLabel: "¿Se requirió de asesorías externas?",
                            labelAlign: "top",
                            anchor: "100%",
                            maxLength: 300,
                            name: "descriptionTypeEvent",
                            itemId: "descriptionTypeEvent"
                          }
                        ]
                      },
                      {
                        xtype: "NumberDecimalNumberObligatory",
                        fieldLabel: "Pérdida bruta",
                        name: "grossLoss",
                        itemId: "grossLoss",
                        value: 0,
                        enforceMaxLength: true,
                        fieldCls: "numberNegative",
                        readOnly: true
                      },
                      {
                        xtype: "NumberDecimalNumber",
                        allowBlank: true,
                        readOnly: true,
                        fieldCls: "numberNegative",
                        name: "totalAmountSpend",
                        itemId: "totalAmountSpend",
                        value: 0,
                        fieldLabel: getName("EVN_CPE_RCLK_Spends")
                      },
                      {
                        xtype: "NumberDecimalNumber",
                        hidden: hidden("EVN_WER_NFD_AmountProvision"),
                        readOnly: true,
                        fieldCls: "numberNegative",
                        name: "amountProvision",
                        itemId: "amountProvision",
                        value: 0,
                        maxLength: 20,
                        fieldLabel: "Provisiones"
                      },
                      {
                        xtype: "NumberDecimalNumber",
                        hidden: true,
                        readOnly: true,
                        fieldCls: "numberNegative",
                        name: "amountSubEvent",
                        itemId: "amountSubEvent",
                        value: 0,
                        maxLength: 20,
                        fieldLabel: "Monto Subeventos"
                      },
                      {
                        xtype: "NumberDecimalNumber",
                        fieldLabel: "Recupero por seguro",
                        name: "sureAmountRecovery",
                        itemId: "sureAmountRecovery",
                        hidden: true,
                        readOnly: true,
                        value: 0,
                        fieldCls: "fieldBlue"
                      },
                      {
                        xtype: "NumberDecimalNumber",
                        fieldLabel: "Recupero funcionario",
                        name: "lossRecovery",
                        itemId: "lossRecovery",
                        hidden: true,
                        readOnly: true,
                        fieldCls: "fieldBlue",
                        value: 0
                      },
                      {
                        xtype: "NumberDecimalNumber",
                        fieldLabel: "Repupero por terceros",
                        name: "amountThirdRecovery",
                        itemId: "amountThirdRecovery",
                        hidden: true,
                        readOnly: true,
                        fieldCls: "fieldBlue",
                        value: 0
                      },
                      {
                        xtype: "NumberDecimalNumber",
                        fieldLabel: "Total recuperado",
                        name: "totalRecovery",
                        itemId: "totalRecovery",
                        enforceMaxLength: true,
                        readOnly: true,
                        fieldCls: "fieldBlue",
                        value: 0
                      },
                      {
                        xtype: "NumberDecimalNumberObligatory",
                        fieldLabel: "Pérdida neta",
                        name: "netLoss",
                        itemId: "netLoss",
                        minValue: -1000000,
                        enforceMaxLength: true,
                        fieldCls: "readOnlyText",
                        readOnly: true
                      },
                      {
                        xtype: "fieldset",
                        title: "Afecta a clientes?",
                        collapsible: true,
                        collapsed: true,
                        items: [
                          {
                            xtype: "UpperCaseTextField",
                            hidden: hidden("EWTFPersonAffected"),
                            fieldLabel: getName("EWPersonAffected"),
                            maxLength: 300,
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "personAffected"
                          },
                          {
                            xtype: "UpperCaseTextField",
                            hidden: hidden("EWCPersonImplicated"),
                            name: "executiveInvolved",
                            fieldLabel: getName("EWCPersonImplicated"),
                            maxLength: 300
                          }
                        ]
                      }
                    ]
                  }
                ]
              },
              {
                xtype: "container",
                itemId: "card-2",
                layout: "fit",
                items: [
                  {
                    xtype: "fieldset",
                    title: "Contabilidad",
                    items: [
                      {
                        xtype: "datetimefield",
                        format: "d/m/Y H:i",
                        fieldLabel: "Fecha contable",
                        name: "dateAccountingEvent",
                        itemId: "dateAccountingEvent",
                        listeners: {
                          expand: function(field, value) {
                            field.setMinValue(
                              me.down("#dateOccurrence").getValue()
                            );
                            field.setMaxValue(new Date());
                          },
                          blur: function(field, value) {
                            field.setMinValue(
                              me.down("#dateOccurrence").getValue()
                            );
                            field.setMaxValue(new Date());
                          }
                        }
                      },
                      {
                        xtype: "container",
                        height: 26,
                        layout: {
                          type: "hbox"
                        },
                        items: [
                          {
                            xtype: "textfield",
                            name: "idPlanAccountLoss",
                            itemId: "idPlanAccountLoss",
                            hidden: true
                          },
                          {
                            xtype: "TextFieldNumberFormatReadOnly",
                            fieldLabel: "Cuenta contable",
                            name: "planAccountLoss",
                            itemId: "planAccountLoss",
                            enforceMaxLength: true
                          },
                          {
                            xtype: "button",
                            name: "buttonSearchCta1",
                            itemId: "buttonSearchCta1",
                            iconCls: "search",
                            handler: function() {
                              var window = Ext.create(
                                "DukeSource.view.risk.parameter.windows.ViewWindowSearchPlanAccountCountable",
                                { modal: true }
                              ).show();
                              window
                                .down("grid")
                                .on("itemdblclick", function() {
                                  me.down("#planAccountLoss").setValue(
                                    window
                                      .down("grid")
                                      .getSelectionModel()
                                      .getSelection()[0]
                                      .get("codeAccount")
                                  );
                                  me.down("#idPlanAccountLoss").setValue(
                                    window
                                      .down("grid")
                                      .getSelectionModel()
                                      .getSelection()[0]
                                      .get("idAccountPlan")
                                  );
                                  me.down("#bookkeepingEntryLoss").focus(
                                    false,
                                    100
                                  );
                                  window.close();
                                });
                            }
                          }
                        ]
                      },
                      {
                        xtype: "UpperCaseTextField",
                        fieldLabel: "Asiento contable",
                        name: "bookkeepingEntryLoss",
                        itemId: "bookkeepingEntryLoss",
                        enforceMaxLength: true,
                        maxLength: 50,
                        listeners: {
                          specialkey: function(f, e) {
                            DukeSource.global.DirtyView.focusEventEnter(
                              f,
                              e,
                              me.down("#dateAcceptLossEvent")
                            );
                          }
                        }
                      },
                      {
                        xtype: "combobox",
                        fieldLabel: "Empresa que sanciona",
                        msgTarget: "side",
                        displayField: "description",
                        valueField: "idSanctioningCompany",
                        forceSelection: true,
                        hidden: true,
                        editable: false,
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        name: "entityPenalizesLoss",
                        itemId: "entityPenalizesLoss",
                        store: {
                          fields: ["idSanctioningCompany", "description"],
                          proxy: {
                            actionMethods: {
                              create: "POST",
                              read: "POST",
                              update: "POST"
                            },
                            type: "ajax",
                            url:
                              "http://localhost:9000/giro/showListSanctioningCompanyActivesComboBox.htm",
                            extraParams: {
                              propertyOrder: "description"
                            },
                            reader: {
                              type: "json",
                              root: "data",
                              successProperty: "success"
                            }
                          }
                        }
                      },
                      {
                        xtype: "combobox",
                        fieldLabel: getName("EWInsuranceCompany"),
                        hidden: true,
                        msgTarget: "side",
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        editable: false,
                        name: "insuranceCompany",
                        itemId: "insuranceCompany",
                        enforceMaxLength: true,
                        displayField: "description",
                        valueField: "idInsuranceCompany",
                        forceSelection: true,
                        maxLength: 50,
                        store: {
                          fields: ["idInsuranceCompany", "description"],
                          proxy: {
                            actionMethods: {
                              create: "POST",
                              read: "POST",
                              update: "POST"
                            },
                            type: "ajax",
                            url:
                              "http://localhost:9000/giro/showListInsuranceCompanyActives.htm",
                            extraParams: {
                              propertyOrder: "description"
                            },
                            reader: {
                              type: "json",
                              root: "data",
                              successProperty: "success"
                            }
                          }
                        }
                      }
                    ]
                  }
                ]
              },
              {
                xtype: "container",
                itemId: "card-3",
                layout: "fit",
                items: [
                  {
                    xtype: "fieldset",
                    title: "ASFI",
                    items: [
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "operation",
                        name: "operation"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "codePlace",
                        name: "codePlace"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "codePaf",
                        name: "codePaf"
                      },
                      {
                        xtype: "textfield",
                        hidden: true,
                        itemId: "processRecursive",
                        name: "processRecursive"
                      },

                      {
                        xtype: "UpperCaseTextArea",
                        anchor: "100%",
                        height: 30,
                        hidden: hidden("EWDescriptionStateEvent"),
                        allowBlank: blank("EWDescriptionStateEvent"),
                        fieldCls: styleField("EWDescriptionStateEvent"),
                        fieldLabel: "Detalle del estado",
                        name: "descriptionStateEvent",
                        itemId: "descriptionStateEvent",
                        maxLength: 2000
                      },
                      {
                        xtype: "UpperCaseTextArea",
                        anchor: "100%",
                        height: 30,
                        fieldLabel: "Detalle ev.critico",
                        name: "descriptionCriticEvent",
                        itemId: "descriptionCriticEvent",
                        maxLength: 2000
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        height: 22,
                        fieldLabel: "PAF",
                        itemId: "descriptionPaf",
                        name: "descriptionPaf",
                        hidden: hidden("EWDescriptionPaf"),
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.WindowTreePaf",
                                {
                                  winParent: me
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        height: 18,
                        fieldLabel: "Proceso ASFI",
                        itemId: "descriptionProcessRecursive",
                        name: "descriptionProcessRecursive",
                        hidden: hidden("EWDescriptionProcessRecursive"),
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.WindowTreeProcessRecursive",
                                {
                                  winParent: me
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        fieldLabel: "Operacion",
                        hidden: hidden("EWDescriptionOperation"),
                        itemId: "descriptionOperation",
                        name: "descriptionOperation",
                        height: 18,
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.WindowTreeOperation",
                                {
                                  winParent: me
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "combobox",
                        anchor: "100%",
                        forceSelection: true,
                        fieldLabel: getName("EWRecoveryFixedAsset"),
                        hidden: hidden("EWRecoveryFixedAsset"),
                        allowBlank: blank("EWRecoveryFixedAsset"),
                        fieldCls: styleField("EWRecoveryFixedAsset"),
                        editable: false,
                        name: "recoveryFixedAsset",
                        itemId: "recoveryFixedAsset",
                        queryMode: "local",
                        displayField: "description",
                        valueField: "value",
                        store: {
                          fields: ["value", "description"],
                          pageSize: 9999,
                          autoLoad: true,
                          proxy: {
                            actionMethods: {
                              create: "POST",
                              read: "POST",
                              update: "POST"
                            },
                            type: "ajax",
                            url:
                              "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                            extraParams: {
                              propertyOrder: "description",
                              propertyFind: "identified",
                              valueFind: "RECOVERY_ACTIVE"
                            },
                            reader: {
                              type: "json",
                              root: "data",
                              successProperty: "success"
                            }
                          }
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ],
        dockedItems: [
          {
            xtype: "toolbar",
            width: 220,
            padding: 5,
            layout: "anchor",
            dock: "left",
            items: [
              {
                xtype: "textfield",
                fieldLabel: "Código",
                readOnly:
                  eventCodeAutomatic === DukeSource.global.GiroConstants.YES,
                fieldCls: "codeIncident",
                name: "codeEventTemp",
                itemId: "codeEventTemp",
                value: DukeSource.global.GiroConstants.DRAFT,
                labelAlign: "top"
              },
              {
                xtype: "button",
                text: "Aprobar",
                hidden: true,
                iconCls: "evaluate",
                name: "btnApproveEvent",
                itemId: "btnApproveEvent",
                cls: "my-btn",
                overCls: "my-over",
                handler: function() {
                  var win = Ext.create(
                    "DukeSource.view.risk.DatabaseEventsLost.windows.WindowApproveCorrelative",
                    {
                      modal: true,
                      id: me.down("#idEvent").getValue(),
                      parentWindow: me,
                      actionExecute: "approveEvent"
                    }
                  );
                  if (
                    me.down("#checkApprove").getValue() ===
                    DukeSource.global.GiroConstants.YES
                  ) {
                    win
                      .down("#dateApprove")
                      .setValue(me.down("#dateApprove").getValue());
                    win
                      .down("#userApprove")
                      .setValue(me.down("#userApprove").getValue());
                    win
                      .down("#commentApprove")
                      .setValue(me.down("#commentApprove").getValue());
                    win.down("#saveApprove").setDisabled(true);
                    win.down("#saveApprove").setVisible(false);
                  }
                  win.show();
                }
              },
              {
                xtype: "container",
                layout: {
                  type: "vbox",
                  align: "stretch"
                },
                items: [
                  {
                    xtype: "fieldset",
                    title: "Inicio",
                    itemId: "options",
                    layout: {
                      type: "vbox",
                      align: "stretch",
                      defaultMargins: {
                        bottom: 1
                      }
                    },

                    items: [
                      {
                        xtype: "button",
                        textAlign: "left",
                        text: "Datos del evento",
                        handler: function(btn) {
                          me.coloredButtonActive(btn, 0);
                        }
                      },
                      {
                        xtype: "button",
                        textAlign: "left",
                        text: "Impacto",
                        handler: function(btn) {
                          me.coloredButtonActive(btn, 1);
                        }
                      },
                      {
                        xtype: "button",
                        textAlign: "left",
                        text: "Contabilidad",
                        hidden: hidden("EVN_WRE_BTN_Accountant"),
                        handler: function(btn) {
                          me.coloredButtonActive(btn, 2);
                        }
                      },
                      {
                        xtype: "button",
                        textAlign: "left",
                        hidden: hidden("EVN_WRE_BTN_EntitySupervisor"),
                        text: "ASFI",
                        handler: function(btn) {
                          me.coloredButtonActive(btn, 3);
                        }
                      }
                    ]
                  },
                  {
                    xtype: "fieldset",
                    layout: "anchor",
                    title: "Informaci&oacute;n de registro",
                    items: [
                      {
                        xtype: "datetimefield",
                        format: "d/m/Y H:i:s",
                        readOnly: true,
                        value: new Date(),
                        anchor: "95%",
                        labelAlign: "top",
                        fieldCls: "readOnlyText",
                        fieldLabel: "Fecha de registro",
                        name: "dateRegisterTemp",
                        itemId: "dateRegisterTemp",
                        enforceMaxLength: true
                      },
                      {
                        xtype: "textfield",
                        readOnly: true,
                        value: fullName,
                        anchor: "95%",
                        labelAlign: "top",
                        fieldCls: "readOnlyText",
                        fieldLabel: "Usuario de registro",
                        name: "fullNameUserRegisterTemp",
                        itemId: "fullNameUserRegisterTemp",
                        enforceMaxLength: true
                      },
                      {
                        xtype: "textfield",
                        readOnly: true,
                        value: descriptionWorkArea,
                        anchor: "95%",
                        labelAlign: "top",
                        fieldCls: "readOnlyText",
                        fieldLabel: "&Aacute;rea",
                        name: "workAreaUserRegisterTemp",
                        itemId: "workAreaUserRegisterTemp",
                        enforceMaxLength: true
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ],
        buttons: [
          {
            text: "Salir",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          },
          {
            text: "Guardar",
            iconCls: "save",
            itemId: "saveEventOperational",
            scale: "medium",
            handler: function() {
              me.saveEvent();
            }
          }
        ],
        buttonAlign: "right",
        listeners: {
          afterrender: function(view) {
            view.calculateNetLoss();
          },
          show: function(win) {
            this.heightMain = win.getHeight();
            me.buttonActive = win.down("#options").items.items[0];
            me.buttonTextActive = win.down("#options").items.items[0].getText();

            me.buttonActive.setText(
              me.buttonActive.getText() + '<span style="color:red"> * </span>'
            );
          },
          close: function(win) {
            clearInterval(win.timer);
          }
        }
      });
      me.callParent(arguments);
    },

    close: function() {
      if (this.fireEvent("beforeclose", this) !== false) {
        var me = this;
        if (me.actionType === "new") {
          DukeSource.global.DirtyView.messageOut(me);
        } else {
          me.doClose();
        }
      }
    }
  }
);
