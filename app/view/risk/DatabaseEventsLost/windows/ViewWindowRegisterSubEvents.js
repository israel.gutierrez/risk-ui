Ext.define('ModelSubEventsLost', {
    extend: 'Ext.data.Model',
    fields: [
        'idEvent',
        'codeEvent',
        'workArea',
        'isRiskEventIncidents',
        'dateRegister',
        'userRegister',
        'fullNameUserRegister',
        'workAreaUserRegister',
        'businessLineOne',
        'businessLineTwo',
        'businessLineThree',
        'descriptionBusinessLineOne',
        'factorRisk',
        'idEventThree',
        'eventTwo',
        'eventOne',
        'managerRisk',
        'agency',
        'currency',
        'riskType',
        'actionPlan',
        'idProcessType',
        'idProcess',
        'idSubProcess',
        'idActivity',
        'insuranceCompany',
        'descriptionProcess',
        'descriptionSubProcess',
        'eventState',
        'nameEventState',
        'colorEventState',
        'sequenceEventState',
        'eventType',
        'lossType',
        'totalLossRecovery',
        'totalLoss',
        'accountingEntryLoss',
        'accountingEntryRecoveryLoss',
        'descriptionLarge',
        'descriptionAgency',
        'descriptionRegion',
        'descriptionWorkArea',
        'descriptionUnity',
        {
            name: 'dateOccurrence', type: 'date', format: 'd/m/Y H:i:s', convert: function (value) {
                return Ext.Date.parse(value, "d/m/Y H:i:s");
            }
        },
        {
            name: 'dateAcceptLossEvent', type: 'date', format: 'd/m/Y H:i:s', convert: function (value) {
                return Ext.Date.parse(value, "d/m/Y H:i:s");
            }
        },
        {
            name: 'dateCloseEvent', type: 'date', format: 'd/m/Y H:i:s', convert: function (value) {
                return Ext.Date.parse(value, "d/m/Y H:i:s");
            }
        },
        {
            name: 'dateDiscovery', type: 'date', format: 'd/m/Y H:i:s', convert: function (value) {
                return Ext.Date.parse(value, "d/m/Y H:i:s");
            }
        },
        {
            name: 'dateAccountingEvent', type: 'date', format: 'd/m/Y H:i:s', convert: function (value) {
                return Ext.Date.parse(value, "d/m/Y H:i:s");
            }
        },
        'entityPenalizesLoss',
        'grossLoss',
        'lossRecovery',
        'sureAmountRecovery',
        'totalAmountSpend',
        'amountSubEvent',
        'amountProvision',
        'sureAmount',
        'netLoss',
        'typeChange',
        'typeEvent',
        'descriptionEventThree',
        'descriptionEventTwo',
        'descriptionEventOne',
        'nameFactorRisk',
        'descriptionFactorRisk',
        'nameAction',
        'nameCurrency',
        'descriptionShort',
        'idPlanAccountSureAmountRecovery',
        'planAccountSureAmountRecovery',
        'planAccountLossRecovery',
        'idPlanAccountLossRecovery',
        'bookkeepingEntryLossRecovery',
        'bookkeepingEntrySureAmountRecovery',
        'amountOrigin',
        'codeGeneralIncidents',
        'idIncident',
        'cause',
        'fileAttachment',
        'codesRiskAssociated',
        'numberRiskAssociated',
        'indicatorAtRisk',
        'eventMain',
        'numberRelation',
        'numberActionPlan',
        'codesActionPlan',
        'descriptionTittleEvent',
        'costCenter',
        'codeMigration',
        'comments',
        'order',
        'stateTemp',
        'descriptionPlanAccountLoss',
        'codeAccountLoss',
        'codeCorrelative',
        'checkApprove',
        'userApprove',
        'commentApprove',
        'dateApprove',
        'processRecursive',
        'descriptionProcessRecursive',
        'operation',
        'descriptionOperation',
        'recoveryFixedAsset',
        'isCritic',
        'descriptionStateEvent',
        'descriptionCriticEvent',
        'descriptionTypeEvent',
        'product',
        'descriptionProduct',
        'processImpact',
        'subProcessImpact'
    ]
});

var StoreSubEventsLost = Ext.create('Ext.data.Store', {
    model: 'ModelSubEventsLost',
    autoLoad: false,
    proxy: {
        actionMethods: {
            create: 'POST',
            read: 'POST',
            update: 'POST'
        },
        type: 'ajax',
        url: 'loadGridDefault.htm',
        reader: {
            totalProperty: 'totalCount',
            root: 'data',
            successProperty: 'success'
        }
    }
});

Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterSubEvents', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowRegisterSubEvents',
    layout: {
        type: 'fit'
    },
    title: 'Sub Eventos relacionados al evento múltiple',
    border: false,
    height: 600,
    width: 900,
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        var recordMainEvent = this.recordMainEvent;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'panel',
                    layout: 'fit',
                    tbar: [
                        /*{
                            text: 'Nuevo',
                            iconCls: 'add',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            handler: function (button) {
                                var view = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterSubEventsDetail', {
                                    modal: true,
                                    width: 900,
                                    actionType: 'new',
                                    recordMainEvent: me.recordMainEvent
                                });
                                view.down('#eventMain').setValue('N');
                                view.down('#numberRelation').setValue(recordMainEvent['idEvent']);
                                view.down('#idEvent').setValue('id');
                                view.show();
                                if (category === DukeSource.global.GiroConstants.ANALYST) {
                                    view.down('#userReport').setValue(userName);
                                    view.down('#fullNameUserReport').setValue(fullName);
                                }
                            }
                        },*/
                        {
                            text: 'Convertir a sub-evento',
                            iconCls: 'import',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            handler: function (button) {
                                var view = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.SearchSimpleEvent', {
                                    modal: true,
                                    recordMainEvent: me.recordMainEvent
                                });
                                view.show();
                            }
                        },
                        {
                            text: 'Revertir a evento',
                            iconCls: 'export',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            handler: function (button) {
                                var recordSubEvent = me.down('grid').getSelectionModel().getSelection()[0];

                                if (recordSubEvent === undefined) {
                                    DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
                                } else {
                                    Ext.MessageBox.show({
                                        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                                        msg: 'Está seguro que desea revertir el sub-evento en un evento?',
                                        icon: Ext.Msg.WARNING,
                                        buttonText: {
                                            yes: "Si"
                                        },
                                        buttons: Ext.MessageBox.YESNO,
                                        fn: function (btn) {
                                            if (btn === 'yes') {
                                                DukeSource.lib.Ajax.request({
                                                    method: 'POST',
                                                    url: 'revertSubEventInEvent.htm',
                                                    params: {
                                                        idMainEvent: recordMainEvent['idEvent'],
                                                        idSubEvent: recordSubEvent.get('idEvent')
                                                    },
                                                    success: function (response) {
                                                        response = Ext.decode(response.responseText);
                                                        if (response.success) {
                                                            DukeSource.global.DirtyView.messageNormal(response.message);

                                                            var grid = Ext.ComponentQuery.query('ViewWindowRegisterSubEvents grid')[0];
                                                            grid.down('pagingtoolbar').moveFirst();

                                                        } else {
                                                            DukeSource.global.DirtyView.messageWarning(response.message);
                                                        }
                                                    },
                                                    failure: function () {
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    ],
                    name: 'gridPanelSubEvents',
                    itemId: 'gridPanelSubEvents',
                    items: [
                        {
                            xtype: 'gridpanel',
                            name: 'gridSubEvents',
                            itemId: 'gridSubEvents',
                            store: StoreSubEventsLost,
                            loadMask: true,
                            border: false,
                            columnLines: true,
                            region: 'center',
                            columns: [
                                {xtype: 'rownumberer', width: 25, sortable: false},
                                {
                                    header: 'Código sub-evento',
                                    align: 'left',
                                    dataIndex: 'codeEvent',
                                    width: 150,
                                    renderer: function (value, metaData, record) {
                                        var state;
                                        var type = '';
                                        metaData.tdAttr = 'style="background-color: #' + record.get('colorEventState') + ' !important; height:40px;"';
                                        state = '<br><span style="font-size:7pt">' + record.get('nameEventState') + '</span>';
                                        if ((record.get('eventType') === '2')) {
                                            type = '<i class="tesla even-stack2" style="margin-left:18px;line-height: 0.2 !important;"></i>';
                                        } else if ((record.get('eventState') === 'X')) {
                                            metaData.tdAttr = 'style="background-color: #cccccc !important; height:40px;"';
                                            type = '<i class="tesla even-blocked-24" style="margin-left:18px;line-height: 0.2 !important;"></i>';
                                        }

                                        if (record.get('fileAttachment') === 'S') {
                                            return '<div style="display:inline-block;height:20px;width:20px;padding:2px;"><i class="tesla even-attachment"></i></div><div style="display:inline-block;position:absolute;">' +
                                                '<span style="font-size: 12px;font-weight: bold">' + value + type + '</span> ' + state + '</div>';
                                        } else {
                                            return '<div style="display:inline-block;height:20px;width:20px;padding:2px;"></div><div style="display:inline-block;position:absolute;">' +
                                                '<span style="font-size: 12px;font-weight: bold">' + value + type + '</span> ' + state + '</div>';
                                        }
                                    }
                                },
                                {
                                    header: 'Descripci&oacute;n corta',
                                    align: 'left',
                                    dataIndex: 'descriptionShort',
                                    width: 200
                                },
                                {
                                    header: 'Moneda',
                                    align: 'center',
                                    dataIndex: 'nameCurrency',
                                    width: 60
                                },
                                {
                                    header: 'Monto inicial',
                                    dataIndex: 'amountOrigin',
                                    align: 'center',
                                    xtype: 'numbercolumn',
                                    format: '0,0.00',
                                    width: 100
                                },
                                {
                                    header: 'Tipo de cambio',
                                    align: 'center',
                                    dataIndex: 'typeChange',
                                    width: 60
                                },
                                {
                                    header: 'Monto total de pérdida',
                                    dataIndex: 'totalLoss',
                                    align: 'center',
                                    tdCls: 'custom-column',
                                    xtype: 'numbercolumn',
                                    format: '0,0.00',
                                    width: 100
                                },
                                {
                                    header: 'Monto total de recupero',
                                    dataIndex: 'totalLossRecovery',
                                    align: 'center',
                                    xtype: 'numbercolumn',
                                    format: '0,0.00',
                                    width: 100
                                },
                                {
                                    header: 'Gastos',
                                    dataIndex: 'totalAmountSpend',
                                    align: 'center',
                                    xtype: 'numbercolumn',
                                    format: '0,0.00',
                                    width: 60
                                },
                                {
                                    header: 'Pérdida neta',
                                    dataIndex: 'netLoss',
                                    tdCls: 'custom-column',
                                    align: 'center',
                                    xtype: 'numbercolumn',
                                    format: '0,0.00',
                                    width: 100
                                },
                                {
                                    header: 'Fecha de ocurrencia',
                                    align: 'center',
                                    dataIndex: 'dateOccurrence',
                                    format: "d/m/Y",
                                    xtype: 'datecolumn',
                                    width: 90
                                },
                                {
                                    header: 'Fecha de descubrimiento',
                                    align: 'center',
                                    dataIndex: 'dateDiscovery',
                                    xtype: 'datecolumn',
                                    format: 'd/m/Y',
                                    width: 100,
                                    renderer: Ext.util.Format.dateRenderer('d/m/Y')
                                },
                                {
                                    header: 'Fecha de contabilizaci&oacute;n',
                                    align: 'center',
                                    dataIndex: 'dateAccountingEvent',
                                    width: 105,
                                    xtype: 'datecolumn',
                                    format: 'd/m/Y',
                                    renderer: Ext.util.Format.dateRenderer('d/m/Y')
                                },
                                {
                                    header: 'Factor de riesgo',
                                    dataIndex: 'descriptionFactorRisk',
                                    width: 200,
                                    tdCls: 'process-columnFree',
                                    renderer: function (value) {
                                        var s = value.split(' &#8702; ');
                                        var path = "";
                                        for (var i = 0; i < s.length; i++) {
                                            var text = s[i];
                                            text = s[i] + '<br>';
                                            path = path + ' &#8702; ' + text;
                                        }
                                        return path;
                                    }
                                },
                                {
                                    header: 'Eventos n1',
                                    text: getName('EWCBEventTwo'),
                                    dataIndex: 'descriptionEventOne',
                                    width: 270,
                                    tdCls: 'process-columnFree',
                                    renderer: function (value) {
                                        var s = value.split(' &#8702; ');
                                        var path = "";
                                        for (var i = 0; i < s.length; i++) {
                                            var text = s[i];
                                            text = s[i] + '<br>';
                                            path = path + ' &#8702; ' + text;
                                        }
                                        return path;
                                    }
                                },
                                {
                                    header: 'Tipos de p&eacute;rdida',
                                    align: 'center',
                                    dataIndex: 'lossType',
                                    width: 90
                                }
                            ],
                            bbar: {
                                xtype: 'pagingtoolbar',
                                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                                store: StoreSubEventsLost,
                                displayInfo: true,
                                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                                items: ['-', {
                                    iconCls: 'print', handler: function () {
                                        DukeSource.global.DirtyView.printElementTogrid(me.down('grid[name=gridSubEvents]'));
                                    }
                                }, '-', {
                                    xtype:"UpperCaseTrigger", fieldLabel: 'FILTRAR', listeners: {
                                        keyup: function (text) {
                                            DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, text.up('grid'));
                                        }
                                    }, labelWidth: 60, width: 300
                                }],
                                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                            },
                            listeners: {
                                render: function (grid) {
                                    var rowEvent = Ext.ComponentQuery.query('ViewPanelEventsRiskOperational grid')[0].getSelectionModel()
                                        .getSelection()[0];
                                    grid.store.getProxy().url = 'getSubEventsByEvent.htm';
                                    grid.store.getProxy().extraParams = {
                                        idEvent: rowEvent.get('idEvent')
                                    };
                                    grid.down('pagingtoolbar').moveFirst();
                                }
                            }
                        }
                    ],
                    buttons: [
                        {
                            text: 'Salir',
                            scale: 'medium',
                            scope: this,
                            handler: this.close,
                            iconCls: 'logout'
                        }
                    ],
                    buttonAlign: 'center'
                }
            ]
        });
        me.callParent(arguments);
    }
});