Ext.define("ModelTraceEvent", {
  extend: "Ext.data.Model",
  fields: [
    "idTracking",
    "idEvent",
    {
      name: "dateTracking",
      type: "date",
      format: "d/m/Y",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y");
      }
    },
    "stateEvent",
    "colorStateEvent",
    "descriptionStateEvent",
    "typeIncident",
    "description",
    "fullName",
    "state",
    "indicatorAttachment",
    "descriptionIndicator"
  ]
});
var StorTraceEvent = Ext.create("Ext.data.Store", {
  model: "ModelTraceEvent",
  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.WindowRegisterTraceEvent",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowRegisterTrace",
    height: 500,
    width: 1000,
    title: "Seguimiento del evento",
    titleAlign: "center",
    layout: {
      type: "hbox",
      align: "stretch"
    },

    initComponent: function() {
      var me = this;
      var idEvent = me.idEvent;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "panel",
            flex: 0.8,
            padding: 2,
            tbar: [
              {
                xtype: "button",
                iconCls: "add",
                text: "Nuevo",
                handler: function() {
                  me.down("form")
                    .getForm()
                    .reset();
                  me.down("#idEvent").setValue(idEvent);
                  me.down("#typeEvent").setValue(DukeSource.global.GiroConstants.EVENT);
                }
              },
              {
                xtype: "button",
                iconCls: "save",
                text: "Guardar",
                handler: function() {
                  var form = me.down("form");
                  if (form.getForm().isValid()) {
                    form.getForm().submit({
                      url:
                        "http://localhost:9000/giro/saveTrackingEvent.htm?nameView=ViewPanelEventsRiskOperational",
                      waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                      method: "POST",
                      success: function(form, action) {
                        var valor = Ext.decode(action.response.responseText);
                        if (valor.success) {
                          me
                            .down("#gridTrackEvent")
                            .store.getProxy().extraParams = {
                            idEvent: me.idEvent,
                            typeIncident: DukeSource.global.GiroConstants.EVENT
                          };
                          me.down("#gridTrackEvent").store.getProxy().url =
                            "http://localhost:9000/giro/showListTrackingEventActives.htm";
                          me.down("#gridTrackEvent")
                            .down("pagingtoolbar")
                            .moveFirst();
                          me.down("form")
                            .getForm()
                            .reset();
                          me.down("#idEvent").setValue(me.idEvent);
                          me.down("#typeEvent").setValue(DukeSource.global.GiroConstants.EVENT);

                          var win = Ext.ComponentQuery.query(
                            "ViewPanelEventsRiskOperational"
                          )[0];
                          win
                            .down("#gridEvents")
                            .down("pagingtoolbar")
                            .moveFirst();

                          DukeSource.global.DirtyView.messageNormal(valor.message);
                        }
                      },
                      failure: function(form, action) {
                        var valor = Ext.decode(action.response.responseText);
                        if (!valor.success) {
                          DukeSource.global.DirtyView.messageWarning(valor.message);
                        }
                      }
                    });
                  } else {
                    DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
                  }
                }
              }
            ],
            items: [
              {
                xtype: "form",
                itemId: "formRegisterTrace",
                border: false,
                layout: "anchor",
                bodyPadding: 5,
                items: [
                  {
                    xtype: "textfield",
                    name: "idTracking",
                    itemId: "idTracking",
                    value: "id",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    name: "idEvent",
                    itemId: "idEvent",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    name: "typeEvent",
                    itemId: "typeEvent",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    name: "indicatorAttachment",
                    itemId: "indicatorAttachment",
                    value: "N",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    name: "username",
                    itemId: "username",
                    hidden: true
                  },
                  {
                    xtype: "combobox",
                    allowBlank: false,
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    name: "stateEvent",
                    itemId: "stateEvent",
                    displayField: "description",
                    valueField: "id",
                    editable: false,
                    forceSelection: true,
                    fieldLabel: "Estado",
                    msgTarget: "side",

                    store: {
                      autoLoad: true,
                      fields: ["id", "description", "sequence"],
                      proxy: {
                        actionMethods: {
                          create: "POST",
                          read: "POST",
                          update: "POST"
                        },
                        type: "ajax",
                        url: "http://localhost:9000/giro/findStateIncident.htm",
                        extraParams: {
                          propertyFind: "si.typeIncident",
                          valueFind: DukeSource.global.GiroConstants.EVENT,
                          propertyOrder: "si.sequence"
                        },
                        reader: {
                          type: "json",
                          root: "data",
                          successProperty: "success"
                        }
                      }
                    }
                  },
                  {
                    xtype: "datefield",
                    name: "dateTracking",
                    itemId: "dateTracking",
                    allowBlank: false,
                    maxValue: new Date(),
                    format: "d/m/Y",
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    fieldLabel: "Fecha"
                  },
                  {
                    xtype: 'UpperCaseTextArea',
                    name: "description",
                    itemId: "description",
                    allowBlank: false,
                    maxLength: 600,
                    height: 100,
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    fieldLabel: "Descripción",
                    listeners: {
                      expand: function(field) {
                        field.setMaxValue(new Date());
                      }
                    }
                  },
                  {
                    xtype: "filefield",
                    fieldLabel: "Documento adjunto",
                    name: "document",
                    itemId: "document",
                    buttonConfig: {
                      iconCls: "tesla even-attachment"
                    },
                    buttonText: ""
                  }
                ]
              }
            ]
          },
          {
            xtype: "gridpanel",
            itemId: "gridTrackEvent",
            flex: 2,
            padding: "2 0 2 2",
            tbar: [
              {
                xtype: "button",
                iconCls: "delete",
                text: "Eliminar",
                handler: function() {
                  var grid = me.down("#gridTrackEvent");
                  var recordEvent = grid.getSelectionModel().getSelection()[0];
                  if (recordEvent === undefined) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      DukeSource.global.GiroMessages.MESSAGE_ITEM,
                      Ext.Msg.WARNING
                    );
                  } else {
                    Ext.MessageBox.show({
                      title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                      msg:
                        "Se eliminará toda la información relacionada al seguimiento, esta seguro de continuar?",
                      icon: Ext.Msg.WARNING,
                      buttonText: {
                        yes: "Si"
                      },
                      buttons: Ext.MessageBox.YESNO,
                      fn: function(btn) {
                        if (btn === "yes") {
                          DukeSource.lib.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/deleteTrackingEvent.htm?nameView=ViewPanelEventsRiskOperational",
                            params: {
                              idTrackEvent: recordEvent.get("idTracking")
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              if (response.success) {
                                DukeSource.global.DirtyView.messageNormal(response.message);
                                me.down("form")
                                  .getForm()
                                  .reset();
                                grid.down("pagingtoolbar").moveFirst();
                              } else {
                                DukeSource.global.DirtyView.messageWarning(response.message);
                              }
                            },
                            failure: function() {
                              DukeSource.global.DirtyView.messageWarning(response.message);
                            }
                          });
                        }
                      }
                    });
                  }
                }
              },
              "-",
              {
                xtype: "button",
                iconCls: "attachFile",
                text: "Adjunto",
                handler: function() {
                  var grid = me.down("#gridTrackEvent");
                  var recordEvent = grid.getSelectionModel().getSelection()[0];
                  if (recordEvent === undefined) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      DukeSource.global.GiroMessages.MESSAGE_ITEM,
                      Ext.Msg.WARNING
                    );
                  } else {
                    var row = grid.getSelectionModel().getSelection()[0];
                    var k = Ext.create(
                      "DukeSource.view.fulfillment.window.ViewWindowFileAttachment",
                      {
                        modal: true,
                        saveFile: "saveFileAttachTrackEvent",
                        deleteFile: "deleteFileAttachTrackEvent",
                        searchGridTrigger: "searchGridFileAttachTrackEvent",
                        src:
                          "http://localhost:9000/giro/downloadAttachTrackEvent.htm",
                        params: ["idFileAttachment", "nameFile"]
                      }
                    );
                    var gridFileAttachment = k.down("grid");
                    gridFileAttachment.store.getProxy().extraParams = {
                      idTracking: row.get("idTracking")
                    };
                    gridFileAttachment.store.getProxy().url =
                      "http://localhost:9000/giro/findFileAttachmentTrackingEvent.htm";
                    gridFileAttachment.down("pagingtoolbar").moveFirst();
                    k.idTracking = row.get("idTracking");
                    k.show();
                  }
                }
              }
            ],
            columns: [
              {
                xtype: "rownumberer",
                width: 25,
                sortable: true
              },
              {
                header: "Doc",
                width: 35,
                renderer: function(value, metaData, record) {
                  if (record.get("indicatorAttachment") === "S") {
                    return (
                      '<div style="display:inline-block;height:20px;width:20px;padding:2px;"><i class="tesla even-attachment"></i></div>' +
                      '<span style="font-size:12px;"></span></div>'
                    );
                  } else {
                    return (
                      '<div style="display:inline-block;height:20px;width:20px;padding:2px;"></div>' +
                      '<span style="font-size:12px;"></span></div>'
                    );
                  }
                }
              },
              {
                header: "Fecha",
                width: 90,
                dataIndex: "dateTracking",
                format: "d/m/Y",
                renderer: Ext.util.Format.dateRenderer("d/m/Y")
              },
              {
                header: "Estado",
                width: 110,
                align: "center",
                dataIndex: "indicatorIncident",
                renderer: function(c, b, a) {
                  b.tdAttr =
                    'style="background-color: #' +
                    a.get("colorStateEvent") +
                    ' !important;"';
                  return "<span>" + a.get("descriptionStateEvent") + "</span>";
                }
              },
              {
                header: "Seguimiento",
                width: 400,
                dataIndex: "description"
              },
              {
                header: "Usuario",
                width: 185,
                dataIndex: "fullName"
              }
            ],
            viewConfig: {
              loadingText: false
            },
            store: StorTraceEvent,
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StorTraceEvent,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE
            },
            listeners: {
              itemdblclick: function(dv, record) {
                me.down("#stateEvent").store.load();
                me.down("form")
                  .getForm()
                  .setValues(record.raw);
              }
            }
          }
        ],
        buttons: [
          {
            text: "Salir",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          }
        ],
        buttonAlign: "center",
        listeners: {
          beforeshow: function() {
            if (codexCompany === "es_cl_0001") {
              me.down("#stateEvent").store.load({
                callback: function() {
                  me.down("#stateEvent").setValue("18");
                }
              });
              me.down("#stateEvent").setReadOnly(true);
              me.down("#stateEvent").setFieldStyle("background: #e2e2e2");
            }
          }
        }
      });

      me.callParent(arguments);
    }
  }
);
