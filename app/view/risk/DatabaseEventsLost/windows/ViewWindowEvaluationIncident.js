Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowEvaluationIncident",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowEvaluationIncident",

    title: "Impacto del incidente",
    titleAlign: "center",
    modal: true,
    border: false,
    doEvaluationImpact: function(win, value, i) {
      this.values[i] = parseInt(value);
      var max = Math.max.apply(null, this.values);
      win.down("#impactIncident").setValue(max.toString());
    },
    initComponent: function() {
      var me = this;
      var idIncident = this.idIncident;
      this.values = [];

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 10,
            items: [
              {
                xtype: "combobox",
                allowBlank: false,
                fieldCls: "obligatoryTextField",
                anchor: "100%",
                itemId: "impactSecurity",
                name: "impactSecurity",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                displayField: "description",
                valueField: "value",
                editable: false,
                forceSelection: true,
                msgTarget: "side",
                fieldLabel: "Seguridad",
                store: {
                  fields: ["value", "description"],
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url:
                      "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                    extraParams: {
                      propertyFind: "identified",
                      valueFind: "IMPACT_SECURITY",
                      propertyOrder: "value"
                    },
                    reader: {
                      type: "json",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                },
                listeners: {
                  select: function(cbo) {
                    me.doEvaluationImpact(me, cbo.getValue(), 0);
                  }
                }
              },
              {
                xtype: "combobox",
                allowBlank: false,
                fieldCls: "obligatoryTextField",
                anchor: "100%",
                itemId: "impactContinuity",
                name: "impactContinuity",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                displayField: "description",
                valueField: "value",
                editable: false,
                forceSelection: true,
                msgTarget: "side",
                fieldLabel: "Continuidad",
                store: {
                  fields: ["value", "description"],
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url:
                      "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                    extraParams: {
                      propertyFind: "identified",
                      valueFind: "IMPACT_CONTINUITY",
                      propertyOrder: "value"
                    },
                    reader: {
                      type: "json",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                },
                listeners: {
                  select: function(cbo) {
                    me.doEvaluationImpact(me, cbo.getValue(), 1);
                  }
                }
              },
              {
                xtype: "combobox",
                allowBlank: false,
                fieldCls: "obligatoryTextField",
                anchor: "100%",
                itemId: "impactImage",
                name: "impactImage",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                displayField: "description",
                valueField: "value",
                editable: false,
                forceSelection: true,
                msgTarget: "side",
                fieldLabel: "Imagen",
                store: {
                  fields: ["value", "description"],
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url:
                      "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                    extraParams: {
                      propertyFind: "identified",
                      valueFind: "IMPACT_IMAGE",
                      propertyOrder: "value"
                    },
                    reader: {
                      type: "json",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                },
                listeners: {
                  select: function(cbo) {
                    me.doEvaluationImpact(me, cbo.getValue(), 2);
                  }
                }
              },
              {
                xtype: "combobox",
                allowBlank: false,
                fieldCls: "obligatoryTextField",
                anchor: "100%",
                itemId: "impactIncident",
                name: "impactIncident",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                displayField: "description",
                valueField: "value",
                readOnly: true,
                typeAhead: true,
                msgTarget: "side",
                fieldLabel: "Impacto total",
                store: {
                  fields: ["value", "description"],
                  autoLoad: true,
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url:
                      "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                    extraParams: {
                      propertyFind: "identified",
                      valueFind: "IMPACT_INCIDENT",
                      propertyOrder: "value"
                    },
                    reader: {
                      type: "json",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                }
              }
            ]
          }
        ],
        buttonAlign: "center",
        buttons: [
          {
            text: "Guardar",
            align: "center",
            scale: "medium",
            iconCls: "save",
            handler: function(btn) {
              var impactSecurity = me.down("#impactSecurity").getValue();
              var impactContinuity = me.down("#impactContinuity").getValue();
              var impactImage = me.down("#impactImage").getValue();

              var impactIncident = me.down("#impactIncident").getValue();

              var detail = [];
              detail.push(impactSecurity);
              detail.push(impactContinuity);
              detail.push(impactImage);

              DukeSource.lib.Ajax.request({
                method: "POST",
                url: "issue/impact/test/register",
                params: {
                  idIncident: idIncident,
                  impactIncident: impactIncident,
                  detailEvaluation: detail
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    Ext.ComponentQuery.query(
                      "ViewGridIncidentsRevisedRiskOperational"
                    )[0]
                      .down("pagingtoolbar")
                      .doRefresh();

                    me.close();
                  } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                  }
                },
                failure: function(response) {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              });
            }
          },
          {
            text: "Salir",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          }
        ],
        listeners: {
          beforeshow: function(win) {
            DukeSource.lib.Ajax.request({
              method: "GET",
              url: "issue/impact/test/" + idIncident,

              success: function(response) {
                response = Ext.decode(response.responseText);

                if (response.success) {
                  if (response.data) {
                    var impactTest = response.data;

                    me.query(".combo").forEach(function(c) {
                      c.store.load();
                    });
                    me.down("#impactSecurity").setValue(
                      impactTest.detailTest[0]["idImpact"].toString()
                    );
                    me.down("#impactContinuity").setValue(
                      impactTest.detailTest[1]["idImpact"].toString()
                    );
                    me.down("#impactImage").setValue(
                      impactTest.detailTest[2]["idImpact"].toString()
                    );

                    me.down("#impactIncident").setValue(
                      impactTest["idImpact"].toString()
                    );
                  }
                } else {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              },
              failure: function(response) {
                DukeSource.global.DirtyView.messageWarning(response.message);
              }
            });
          }
        }
      });

      me.callParent(arguments);
    }
  }
);
