Ext.define("ModelDetailChartEvents", {
  extend: "Ext.data.Model",
  fields: [
    "currency",
    "dateAcceptLossEvent",
    "dateOccurrence",
    "descriptionShort",
    "grossLoss",
    "idEvent",
    "isRiskEventIncidents",
    "lossRecovery",
    "lossType",
    "nameCurrency",
    "nameEventOne",
    "nameFactorRisk"
  ]
});

var storeDetailChartEvents = Ext.create("Ext.data.Store", {
  model: "ModelDetailChartEvents",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowDetailChartEventLostRiskOperational",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowDetailChartEventLostRiskOperational",
    layout: {
      align: "stretch",
      type: "vbox"
    },
    anchorSize: 100,
    title: "DETALLE EVENTOS DE PÉRDIDA",
    titleAlign: "center",
    width: 800,
    border: false,
    height: 500,
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "container",
            flex: 1,
            layout: {
              align: "stretch",
              type: "hbox"
            },
            items: [
              {
                xtype: "gridpanel",
                padding: "0 1 0 0",
                store: storeDetailChartEvents,
                flex: 1,
                titleAlign: "center",
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    dataIndex: "idEvent",
                    width: 70,
                    align: "center",
                    text: "CODIGO"
                  },
                  {
                    dataIndex: "descriptionShort",
                    flex: 1,
                    text: "DESCRIPCION CORTA"
                  },
                  {
                    dataIndex: "grossLoss",
                    align: "right",
                    xtype: "numbercolumn",
                    format: "0,0.00",
                    width: 90,
                    text: "MONTO <br> PÉRDIDA"
                  },
                  {
                    dataIndex: "lossRecovery",
                    align: "right",
                    xtype: "numbercolumn",
                    format: "0,0.00",
                    width: 90,
                    text: "MONTO <br>RECUPERADO"
                  },
                  {
                    dataIndex: "nameCurrency",
                    width: 80,
                    align: "center",
                    text: "MONEDA"
                  },
                  {
                    dataIndex: "dateOccurrence",
                    width: 90,
                    align: "center",
                    text: "FECHA <br>OCURRENCIA"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: storeDetailChartEvents,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                }
              }
            ]
          }
        ],
        buttons: [
          {
            text: "SALIR",
            scale: "medium",
            iconCls: "logout",
            handler: function() {
              me.close();
            }
          }
        ],
        buttonAlign: "center"
      });
      me.callParent(arguments);
    }
  }
);
