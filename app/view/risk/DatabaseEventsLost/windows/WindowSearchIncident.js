Ext.define("ModelGridIncidentsRevisedRiskOperational", {
  extend: "Ext.data.Model",
  fields: [
    "idIncident",
    "idDetailIncidents",
    "descriptionLarge",
    "descriptionShort",
    {
      name: "dateOccurrence",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateFinal",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateDiscovery",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateRegister",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateReport",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    "indicatorIncidents",
    "stateIncident",
    "colorStateIncident",
    "descriptionStateIncident",
    "sequenceStateIncident",
    "collaborator",
    "nameCollaborator",
    "agency",
    "descriptionAgency",
    "causeCollaborator",
    "workArea",
    "descriptionWorkArea",
    "confidential",
    "typeIncident",
    "subCategory",
    "typeBreach",
    "nameBreach",
    "commentBreach",
    "hasTicket",
    "numberTicket",
    "fileAttachment",
    "lostType",
    "descriptionLostType",
    "factorRisk",
    "descriptionFactorRisk",
    "currency",
    "descriptionCurrency",
    "amountLoss",
    "dateRegisterPreIncident",
    "workAreaPreIncident",
    "userRegister",
    "fullNameUserRegister",
    "workAreaRegister",
    "userReport",
    "fullNameUserReport",
    "fullNameUserReport",
    "commentManager",
    "impact",
    "descriptionImpact",
    "idProcessType",
    "idProcess",
    "idSubProcess",
    "idActivity",
    "descriptionProcess",
    "descriptionIndicatorIncidents",
    "eventOne",
    "eventTwo",
    "eventThree",
    "businessLineOne",
    "businessLineTwo",
    "businessLineThree",
    "riskType",
    "originIncident",
    "incidentsGroup",
    "indicatorIncentive",
    "comments",
    "numberRisk",
    "codeIncident",
    "codesRisk",
    "codeIncident",
    "codeCorrelative",
    "checkApprove",
    "userApprove",
    "commentApprove",
    "dateApprove",
    "materialization",
    "typeVulnerability",
    "typeThreat",
    "assetInvolved",
    "product",
    "criticalLevel",
    {
      name: "numberCustomerAffect",
      type: "number",
      convert: function(value) {
        return parseInt(value);
      }
    },
    {
      name: "lossCustomer",
      type: "number",
      convert: function(value) {
        return parseFloat(value);
      }
    },
    {
      name: "costRecoveryMitigation",
      type: "number",
      convert: function(value) {
        return parseFloat(value);
      }
    },
    {
      name: "percentRecoveryMitigation",
      type: "number",
      convert: function(value) {
        return parseFloat(value);
      }
    },
    "nameSupplier",
    "rutSupplier"
  ]
});

var storeIncidentSearch = Ext.create("Ext.data.Store", {
  model: "ModelGridIncidentsRevisedRiskOperational",
  autoLoad: true,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    extraParams: {
      typeIncident: "RO"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/getIncidentsByState.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.WindowSearchIncident",
  {
    extend: "Ext.window.Window",
    alias: "widget.WindowSearchIncident",
    layout: "fit",
    anchorSize: 100,
    title: "Incidentes",
    titleAlign: "center",
    width: 1000,
    height: 500,
    border: false,

    initComponent: function() {
      var me = this;

      function selectCombo(cbo) {
        var grid = me.down("grid");

        grid.store.proxy.url =
          "http://localhost:9000/giro/advancedSearchIncident.htm";
        grid.store.proxy.extraParams = {
          typeIncident: DukeSource.global.GiroConstants.OPERATIONAL,
          fields: "si.id",
          values: cbo.getValue(),
          types: "Integer",
          operators: "equal"
        };
        grid.store.load();
      }

      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            padding: "0 1 0 0",
            columnLines: true,
            store: storeIncidentSearch,
            flex: 1,
            titleAlign: "center",
            columns: [
              {
                header: "Código incidente",
                align: "left",
                dataIndex: "codeIncident",
                width: 130,
                renderer: function(value, metaData, record) {
                  var attachment = "";
                  var secret = "";
                  var state =
                    '<div style="font-size:9px">' +
                    record.get("descriptionStateIncident") +
                    "</div>";
                  if (record.get("fileAttachment") === "S") {
                    attachment =
                      '<div style="display:inline-block;"><i class="tesla even-attachment"></i></div>';
                  }
                  if (record.get("confidential") === "S") {
                    secret =
                      '<div style="display:inline-block;padding-left:2px;"><i class="tesla even-user-secret"></i></div>';
                  }
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("colorStateIncident") +
                    ' !important;"';
                  return (
                    '<div style="display:inline-block;font-size:12px;padding:5px;">' +
                    attachment +
                    secret +
                    "" +
                    '<span style="padding-left:5px;font-weight:bold">' +
                    value +
                    "</span></div>" +
                    state
                  );
                }
              },
              {
                header: "Fecha de reporte",
                align: "center",
                dataIndex: "dateReport",
                format: "d/m/Y H:i:s",
                xtype: "datecolumn",
                width: 80,
                renderer: function(a) {
                  return (
                    '<span style="color:red;">' +
                    Ext.util.Format.date(a, "d/m/Y H:i:s") +
                    "</span>"
                  );
                }
              },
              {
                header: "Usuario que reporta",
                dataIndex: "fullNameUserReport",
                width: 150
              },
              {
                header: "Descripci&oacute;n corta",
                dataIndex: "descriptionShort",
                width: 150
              },
              {
                header: "Descripci&oacute;n larga",
                dataIndex: "descriptionLarge",
                width: 480
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: storeIncidentSearch,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            },
            tbar: [
              {
                xtype: "textfield",
                fieldLabel: "C&oacute;digo",
                padding: "0 0 0 5",
                labelWidth: 70,
                width: 150,
                name: "codeIncident",
                itemId: "codeIncident",
                listeners: {
                  specialkey: function(f, e) {
                    var grid = me.down("grid");

                    if (e.getKey() === e.ENTER) {
                      if (f.getValue().length > 0) {
                        grid.store.proxy.url =
                          "http://localhost:9000/giro/advancedSearchIncident.htm";
                        grid.store.proxy.extraParams = {
                          typeIncident: DukeSource.global.GiroConstants.OPERATIONAL,
                          fields: "i.codeIncident",
                          values: f.getValue(),
                          types: "String",
                          operators: "equal"
                        };
                        grid.store.load();
                      } else {
                        DukeSource.global.DirtyView.messageWarning(
                          "Ingrese el c&oacute;digo del incidente"
                        );
                      }
                    }
                  }
                }
              },
              {
                xtype: "combobox",
                labelAlign: "left",
                fieldLabel: "Estado",
                labelWidth: 70,
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                name: "si.id",
                displayField: "description",
                valueField: "id",
                itemId: "stateIncident",
                trigger1Cls: "x-form-clear-trigger",
                trigger2Cls: "x-form-arrow-trigger",
                onTrigger1Click: function() {
                  this.reset();
                  selectCombo(me.down("combo"));
                },
                store: {
                  fields: ["id", "description", "sequence"],
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url: "http://localhost:9000/giro/findStateIncident.htm",
                    extraParams: {
                      propertyFind: "si.typeIncident",
                      valueFind: DukeSource.global.GiroConstants.OPERATIONAL,
                      propertyOrder: "si.sequence"
                    },
                    reader: {
                      type: "json",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                },
                listeners: {
                  select: function(cbo) {
                    selectCombo(cbo);
                  }
                }
              }
            ]
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
