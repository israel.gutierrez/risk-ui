Ext.define("ModelGridRegisterTrace", {
  extend: "Ext.data.Model",
  fields: [
    "idTrackIncident",
    "idIncident",
    "dateTrack",
    "indicatorIncidents",
    "stateIncident",
    "colorStateIncident",
    "descriptionStateIncident",
    "typeIncident",
    "descriptionTrack",
    "manager",
    "state",
    "indicatorAttachment",
    "descriptionIndicator"
  ]
});
var StoreGridRegisterTrace = Ext.create("Ext.data.Store", {
  model: "ModelGridRegisterTrace",
  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterTrace",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowRegisterTrace",
    height: 580,
    width: 1100,
    title: "SEGUIMIENTO DEL INCIDENTE",
    titleAlign: "center",
    layout: {
      type: "hbox",
      align: "stretch"
    },

    initComponent: function() {
      var me = this;
      var idIncident = me.idIncident;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            itemId: "gridTrack",
            flex: 2,
            padding: "2 0 2 2",
            tbar: [
              {
                xtype: "button",
                iconCls: "delete",
                text: "ELIMINAR",
                handler: function(btn) {
                  var grid = me.down("#gridTrack");
                  var recordIncident = grid
                    .getSelectionModel()
                    .getSelection()[0];
                  if (recordIncident == undefined) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      DukeSource.global.GiroMessages.MESSAGE_ITEM,
                      Ext.Msg.WARNING
                    );
                  } else {
                    Ext.MessageBox.show({
                      title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                      msg:
                        "Esta seguro que desea continuar?" +
                        " tenga en cuenta que se eliminara tambien,<br> " +
                        "el adjunto del seguimiento",
                      icon: Ext.Msg.QUESTION,
                      buttonText: {
                        yes: "Si"
                      },
                      buttons: Ext.MessageBox.YESNO,
                      fn: function(btn) {
                        if (btn == "yes") {
                          DukeSource.lib.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/deleteTrackIncidentActives.htm?nameView=ViewPanelIncidentsRiskOperational",
                            params: {
                              idTrackIncident: recordIncident.get(
                                "idTrackIncident"
                              )
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              if (response.success) {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_MESSAGE,
                                  response.message,
                                  Ext.Msg.INFO
                                );
                                grid.down("pagingtoolbar").moveFirst();
                              } else {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_WARNING,
                                  response.message,
                                  Ext.Msg.ERROR
                                );
                              }
                            },
                            failure: function() {
                              DukeSource.global.DirtyView.messageAlert(
                                DukeSource.global.GiroMessages.TITLE_WARNING,
                                response.message,
                                Ext.Msg.ERROR
                              );
                            }
                          });
                        }
                      }
                    });
                  }
                }
              },
              "-",
              {
                xtype: "button",
                iconCls: "attachFile",
                text: "ADJUNTO",
                handler: function() {
                  var grid = me.down("#gridTrack");
                  var recordIncident = grid
                    .getSelectionModel()
                    .getSelection()[0];
                  if (recordIncident == undefined) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      DukeSource.global.GiroMessages.MESSAGE_ITEM,
                      Ext.Msg.WARNING
                    );
                  } else {
                    var grid = me.down("#gridTrack");
                    var row = grid.getSelectionModel().getSelection()[0];
                    var k = Ext.create(
                      "DukeSource.view.fulfillment.window.ViewWindowFileAttachment",
                      {
                        modal: true,
                        saveFile: "saveFileAttachTrackIncident",
                        deleteFile: "deleteFileAttachTrackIncident",
                        searchGridTrigger: "searchGridFileAttachTrackIncident",
                        src:
                          "http://localhost:9000/giro/downloadAttachTrackIncident.htm",
                        params: ["idFileAttachment", "nameFile"]
                      }
                    );
                    var gridFileAttachment = k.down("grid");
                    gridFileAttachment.store.getProxy().extraParams = {
                      idTracking: row.get("idTrackIncident")
                    };
                    gridFileAttachment.store.getProxy().url =
                      "http://localhost:9000/giro/findFileAttachmentTrackingIncident.htm";
                    gridFileAttachment.down("pagingtoolbar").moveFirst();
                    k.idTrackIncident = row.get("idTrackIncident");
                    k.show();
                  }
                  /*if (row.get('stateDocument') == DukeSource.global.GiroConstants.FINISHED) {
                                 k.down('#deleteFile').setVisible(false);
                                 k.down('#saveFile').setVisible(false);
                                 }*/
                }
              }
            ],
            columns: [
              {
                xtype: "rownumberer",
                width: 25,
                sortable: true
              },
              {
                header: "DOC",
                width: 30,
                renderer: function(value, metaData, record) {
                  if (record.get("indicatorAttachment") == "S") {
                    return (
                      '<div style="display:inline-block;height:30px;width:30px;padding:8px;background-image: url(&quot;images/fileAttach.png&quot;);background-repeat: no-repeat;background-position-y: 12px;"></div><div style="display:inline-block;position:absolute;padding: 5px 0px 0px 0px;">' +
                      '<span style="font-size: 12px;"></span></div>'
                    );
                  } else {
                    return (
                      '<div style="display:inline-block;height:30px;width:30px;padding:8px;"></div><div style="display:inline-block;position:absolute;padding: 5px 0px 0px 0px;">' +
                      '<span style="font-size: 12px;"></span></div>'
                    );
                  }
                }
              },
              {
                header: "FECHA",
                width: 90,
                dataIndex: "dateTrack"
              },
              {
                header: "ESTADO",
                width: 110,
                align: "center",
                dataIndex: "indicatorIncident",
                renderer: function(c, b, a) {
                  b.tdAttr =
                    'style="background-color: #' +
                    a.get("colorStateIncident") +
                    ' !important;"';
                  return (
                    "<span>" + a.get("descriptionStateIncident") + "</span>"
                  );
                }
              },
              {
                header: "DESCRIPCION SEGUIMIENTO",
                width: 400,
                dataIndex: "descriptionTrack"
              },
              {
                header: "ANALISTA",
                width: 185,
                dataIndex: "manager"
              }
            ],
            viewConfig: {
              loadingText: false
            },
            store: StoreGridRegisterTrace,
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridRegisterTrace,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE
            },
            listeners: {
              itemdblclick: function(dv, record, item, index, e) {
                me.down("#stateIncident").store.load();
                me.down("form")
                  .getForm()
                  .setValues(record.raw);
              }
            }
          },
          {
            xtype: "panel",
            flex: 1,
            padding: 2,
            tbar: [
              {
                xtype: "button",
                iconCls: "new",
                text: "NUEVO",
                handler: function() {
                  me.down("form")
                    .getForm()
                    .reset();
                  me.down("#idIncident").setValue(idIncident);
                  me.down("#typeIncident").setValue(DukeSource.global.GiroConstants.OPERATIONAL);
                }
              }
            ],
            items: [
              {
                xtype: "form",
                itemId: "formRegisterTrace",
                border: false,
                layout: "anchor",
                bodyPadding: 5,
                items: [
                  {
                    xtype: "textfield",
                    name: "idTrackIncident",
                    itemId: "idTrackIncident",
                    value: "id",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    name: "idIncident",
                    itemId: "idIncident",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    name: "typeIncident",
                    itemId: "typeIncident",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    name: "indicatorAttachment",
                    itemId: "indicatorAttachment",
                    value: "N",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    name: "manager",
                    itemId: "manager",
                    hidden: true
                  },
                  {
                    xtype: "combobox",
                    allowBlank: false,
                    fieldCls: "obligatoryTextField",
                    anchor: "100%",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    name: "stateIncident",
                    itemId: "stateIncident",
                    displayField: "description",
                    valueField: "id",

                    editable: false,
                    forceSelection: true,
                    fieldLabel: "ESTADO",
                    msgTarget: "side",
                    store: {
                      fields: ["id", "description", "sequence"],
                      proxy: {
                        actionMethods: {
                          create: "POST",
                          read: "POST",
                          update: "POST"
                        },
                        type: "ajax",
                        url: "http://localhost:9000/giro/findStateIncident.htm",
                        extraParams: {
                          propertyFind: "si.typeIncident",
                          valueFind: DukeSource.global.GiroConstants.OPERATIONAL,
                          propertyOrder: "si.sequence"
                        },
                        reader: {
                          type: "json",
                          root: "data",
                          successProperty: "success"
                        }
                      }
                    }
                  },
                  {
                    xtype: "datefield",
                    name: "dateTrack",
                    itemId: "dateTrack",
                    anchor: "100%",
                    allowBlank: false,
                    maxValue: new Date(),
                    format: "d/m/Y",
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    fieldLabel: "FECHA"
                  },
                  {
                    xtype: 'UpperCaseTextArea',
                    anchor: "100%",
                    name: "descriptionTrack",
                    itemId: "descriptionTrack",
                    allowBlank: false,
                    maxLength: 600,
                    height: 100,
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    fieldLabel: "DESCRIPCION",
                    listeners: {
                      expand: function(field, value) {
                        field.setMaxValue(new Date());
                      }
                    }
                  },
                  {
                    xtype: "filefield",
                    anchor: "100%",
                    fieldLabel: "ADJUNTO",
                    name: "document",
                    itemId: "document",
                    buttonConfig: {
                      iconCls: "tesla even-attachment"
                    },
                    buttonText: ""
                  },
                  {
                    xtype: "container",
                    layout: {
                      type: "hbox",
                      pack: "end"
                    },
                    items: [
                      {
                        xtype: "button",
                        iconCls: "save",
                        scale: "medium",
                        anchor: "100%",
                        text: "GUARDAR",
                        handler: function() {
                          var form = me.down("form");
                          if (form.getForm().isValid()) {
                            form.getForm().submit({
                              url:
                                "http://localhost:9000/giro/saveTrackIncidentActives.htm?nameView=ViewPanelIncidentsRiskOperational",
                              waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                              method: "POST",
                              success: function(form, action) {
                                var valor = Ext.decode(
                                  action.response.responseText
                                );
                                if (valor.success) {
                                  me
                                    .down("#gridTrack")
                                    .store.getProxy().extraParams = {
                                    idIncident: me.idIncident,
                                    typeIncident: DukeSource.global.GiroConstants.OPERATIONAL
                                  };
                                  me.down("#gridTrack").store.getProxy().url =
                                    "http://localhost:9000/giro/showListTrackIncidentActives.htm";
                                  me.down("#gridTrack")
                                    .down("pagingtoolbar")
                                    .moveFirst();
                                  me.down("form")
                                    .getForm()
                                    .reset();
                                  me.down("#idIncident").setValue(
                                    me.idIncident
                                  );
                                  me.down("#typeIncident").setValue(
                                    DukeSource.global.GiroConstants.OPERATIONAL
                                  );
                                  DukeSource.global.DirtyView.messageAlert(
                                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                                    valor.message,
                                    Ext.Msg.INFO
                                  );
                                }
                              },
                              failure: function(form, action) {
                                var valor = Ext.decode(
                                  action.response.responseText
                                );
                                if (!valor.success) {
                                  DukeSource.global.DirtyView.messageAlert(
                                    DukeSource.global.GiroMessages.TITLE_ERROR,
                                    valor.message,
                                    Ext.Msg.ERROR
                                  );
                                }
                              }
                            });
                          } else {
                            DukeSource.global.DirtyView.messageAlert(
                              DukeSource.global.GiroMessages.TITLE_WARNING,
                              DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
                              Ext.Msg.ERROR
                            );
                          }
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ],
        buttons: [
          {
            text: "SALIR",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
