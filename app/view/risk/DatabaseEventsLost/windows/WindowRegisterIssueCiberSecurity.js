Ext.require(['Ext.ux.DateTimeField']);
Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.WindowRegisterIssueCiberSecurity', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowRegisterIssueCiberSecurity',
    layout: {
        type: 'fit'
    },
    width: 750,
    title: 'Ingreso incidente de ciberseguridad',
    border: false,
    titleAlign: 'center',
    coloredButtonActive: function (btn, index) {
        this.setHeight(this.heightMain);
        this.down('form').getLayout().setActiveItem(index);

        this.buttonActive.setText(this.buttonTextActive);
        this.buttonTextActive = btn.getText();

        btn.setText(btn.getText() + '<span style="color:red"> * </span>');
        this.buttonActive = btn;
    },
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    padding: '0 0 0 2',
                    fieldDefaults: {
                        labelCls: 'changeSizeFontToEightPt',
                        fieldCls: 'changeSizeFontToEightPt'
                    },
                    autoScroll: true,
                    layout: 'card',
                    bodyPadding: 10,
                    items: [
                        {
                            itemId: 'card-0',
                            xtype: 'container',
                            layout: 'fit',
                            items: [

                                {
                                    xtype: 'fieldset',
                                    title: 'Datos del incidente',
                                    bodyStyle: 'padding:0px',
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            value: 'id',
                                            name: 'idIncident',
                                            itemId: 'idIncident'
                                        },
                                        {
                                            xtype: 'datetimefield',
                                            format: 'd/m/Y H:i:s',
                                            hidden: true,
                                            name: 'dateRegister',
                                            itemId: 'dateRegister'
                                        },
                                        {
                                            xtype: 'textfield',
                                            value: userName,
                                            hidden: true,
                                            name: 'userRegister',
                                            itemId: 'userRegister'
                                        },
                                        {
                                            xtype: 'textfield',
                                            value: DukeSource.global.GiroConstants.DRAFT,
                                            hidden: true,
                                            name: 'codeIncident',
                                            itemId: 'codeIncident'
                                        },

                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'codeCorrelative',
                                            name: 'codeCorrelative'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'checkApprove',
                                            name: 'checkApprove'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'userApprove',
                                            name: 'userApprove'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'commentApprove',
                                            name: 'commentApprove'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'dateApprove',
                                            name: 'dateApprove'
                                        },

                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            name: 'idDetailIncidents',
                                            itemId: 'idDetailIncidents'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'collaborator',
                                            name: 'collaborator'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'originIncident',
                                            name: 'originIncident',
                                            value: 'GIRO'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            name: 'fileAttachment',
                                            itemId: 'fileAttachment'
                                        },
                                        {
                                            xtype: "textfield",
                                            hidden: true,
                                            itemId: 'commentManager',
                                            name: "commentManager"
                                        },
                                        {
                                            xtype: "textfield",
                                            hidden: true,
                                            itemId: 'descriptionIndicatorIncidents',
                                            name: "descriptionIndicatorIncidents"
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            name: 'numberRisk',
                                            itemId: 'numberRisk',
                                            value: '0'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            name: 'codesRisk',
                                            itemId: 'codesRisk'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            name: 'generateLoss',
                                            itemId: 'generateLoss',
                                            value: 'N'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            name: 'sequenceStateIncident',
                                            itemId: 'sequence'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            value: DukeSource.global.GiroConstants.CYBER_SECURITY,
                                            name: 'typeIncident',
                                            itemId: 'typeIncident'
                                        },
                                        {
                                            xtype:"UpperCaseTextField",
                                            allowBlank: false,
                                            anchor: '100%',
                                            fieldLabel: 'Descripci&oacute;n corta',
                                            maxLength: 300,
                                            msgTarget: 'side',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'descriptionShort',
                                            itemId: 'descriptionShort',
                                            fieldCls: 'obligatoryTextField'
                                        },
                                        {
                                            xtype: 'UpperCaseTextArea',
                                            allowBlank: false,
                                            fieldCls: 'obligatoryTextField',
                                            height: 30,
                                            maxLength: 3000,
                                            msgTarget: 'side',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            anchor: '100%',
                                            fieldLabel: 'Descripci&oacute;n larga',
                                            name: 'descriptionLarge',
                                            itemId: 'descriptionLarge'
                                        },
                                        {
                                            xtype: 'combobox',
                                            allowBlank: false,
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'stateIncident',
                                            itemId: 'stateIncident',
                                            displayField: 'description',
                                            valueField: 'id',
                                            editable: false,
                                            forceSelection: true,
                                            fieldLabel: 'Estado',
                                            msgTarget: 'side',
                                            store: {
                                                fields: ['id', 'description', 'sequence'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/findStateIncident.htm',
                                                    extraParams: {
                                                        propertyFind: 'si.typeIncident',
                                                        valueFind: DukeSource.global.GiroConstants.CYBER_SECURITY,
                                                        propertyOrder: 'si.sequence'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'datetimefield',
                                            allowBlank: false,
                                            fieldCls: 'obligatoryTextField',
                                            format: 'd/m/Y H:i:s',
                                            maxValue: new Date(),
                                            name: 'dateOccurrence',
                                            itemId: 'dateOccurrence',
                                            forceSelection: true,
                                            msgTarget: 'side',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,

                                            fieldLabel: 'Fecha del evento'
                                        },
                                        {
                                            xtype: 'datetimefield',
                                            allowBlank: false,
                                            fieldCls: 'obligatoryTextField',
                                            format: 'd/m/Y H:i:s',
                                            maxValue: new Date(),
                                            value: new Date(),
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'dateReport',
                                            itemId: 'dateReport',
                                            fieldLabel: 'Fecha de reporte',
                                            msgTarget: 'side'
                                        },
                                        {
                                            xtype: 'datetimefield',
                                            allowBlank: false,
                                            maxValue: new Date(),
                                            fieldCls: 'obligatoryTextField',
                                            format: 'd/m/Y H:i:s',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'dateDiscovery',
                                            itemId: 'dateDiscovery',

                                            fieldLabel: 'Fecha de mitigaci&oacute;n',
                                            msgTarget: 'side'
                                        },
                                        {
                                            xtype: 'combobox',
                                            allowBlank: false,
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'materialization',
                                            itemId: 'materialization',
                                            displayField: 'description',
                                            valueField: 'value',
                                            editable: false,
                                            forceSelection: true,
                                            fieldLabel: 'Tipo de registro',

                                            msgTarget: 'side',
                                            store: {
                                                fields: ['value', 'description'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                    extraParams: {
                                                        propertyFind: 'identified',
                                                        valueFind: 'INCIDENT_MATERIALIZED',
                                                        propertyOrder: 'description'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            fieldLabel: 'Tipo de vulnerabilidad',
                                            name: 'typeVulnerability',
                                            itemId: 'typeVulnerability',
                                            displayField: 'description',
                                            valueField: 'value',
                                            editable: false,
                                            store: {
                                                fields: ['value', 'description'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                    extraParams: {
                                                        propertyFind: 'identified',
                                                        valueFind: 'INCIDENT_VULNERABILITY',
                                                        propertyOrder: 'description'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            allowBlank: true,
                                            msgTarget: 'side',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            fieldLabel: 'Tipo de amenaza',
                                            itemId: 'typeThreat',
                                            name: 'typeThreat',
                                            displayField: 'description',
                                            valueField: 'value',
                                            editable: false,
                                            store: {
                                                fields: ['value', 'description'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                    extraParams: {
                                                        propertyFind: 'identified',
                                                        valueFind: 'INCIDENT_THREAT',
                                                        propertyOrder: 'description'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            allowBlank: false,
                                            msgTarget: 'side',

                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            displayField: 'description',
                                            valueField: 'value',
                                            fieldLabel: 'Tipos de Activos',
                                            name: 'assetInvolved',
                                            itemId: 'assetInvolved',
                                            store: {
                                                fields: ['value', 'description'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                    extraParams: {
                                                        propertyFind: 'identified',
                                                        valueFind: 'INCIDENT_ASSET_INFORMATION',
                                                        propertyOrder: 'description'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            msgTarget: 'side',

                                            fieldLabel: 'Canal del prod.o serv',
                                            name: 'product',
                                            itemId: 'product',
                                            displayField: 'description',
                                            valueField: 'value',
                                            store: {
                                                fields: ['value', 'description'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                    extraParams: {
                                                        propertyFind: 'identified',
                                                        valueFind: 'INCIDENT_CHANNEL_PROD',
                                                        propertyOrder: 'description'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            allowBlank: false,
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'criticalLevel',
                                            itemId: 'criticalLevel',
                                            msgTarget: 'side',
                                            fieldLabel: 'Nivel de criticidad',

                                            editable: false,
                                            forceSelection: true,
                                            displayField: 'description',
                                            valueField: 'value',
                                            store: {
                                                fields: ['value', 'description'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                    extraParams: {
                                                        propertyFind: 'identified',
                                                        valueFind: 'INCIDENT_LEVEL_RISK',
                                                        propertyOrder: 'description'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },

                                        {
                                            xtype: 'fileuploadfield',
                                            fieldLabel: 'Documento',
                                            name: 'document',
                                            itemId: 'document',

                                            anchor: "100%",
                                            buttonConfig: {
                                                iconCls: 'tesla even-attachment'
                                            },
                                            buttonText: ""
                                        }
                                    ]
                                }
                            ]
                        },

                        {
                            itemId: 'card-1',
                            xtype: 'container',
                            layout: 'fit',
                            items: [

                                {
                                    xtype: 'fieldset',
                                    title: 'Impacto y recuperaci&oacute;n',
                                    bodyStyle: 'padding:0px',
                                    items: [
                                        {
                                            xtype: 'numberfield',
                                            allowBlank: false,
                                            value: 0,
                                            minValue: 0,
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'numberCustomerAffect',
                                            itemId: 'numberCustomerAffect',
                                            fieldLabel: 'Nro clientes afectados',
                                            msgTarget: 'side'
                                        },
                                        {
                                            xtype: 'NumberDecimalNumberObligatory',
                                            value: '0.00',
                                            allowBlank: false,
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'lossCustomer',
                                            itemId: 'lossCustomer',
                                            queryMode: 'local',

                                            fieldLabel: 'P&eacute;rdidas asociadas',
                                            msgTarget: 'side'
                                        },
                                        {
                                            xtype: 'NumberDecimalNumberObligatory',
                                            value: '0.00',
                                            allowBlank: false,
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'costRecoveryMitigation',
                                            itemId: 'costRecoveryMitigation',
                                            displayField: 'description',
                                            fieldLabel: 'Costos de mitigaci&oacute;n',
                                            msgTarget: 'side'
                                        },
                                        {
                                            xtype: 'NumberDecimalNumberObligatory',
                                            value: '0.00',
                                            allowBlank: false,
                                            decimalPrecision: 2,
                                            maxValue: 100,
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'percentRecoveryMitigation',
                                            itemId: 'percentRecoveryMitigation',
                                            displayField: 'description',
                                            valueField: 'idLostType',
                                            msgTarget: 'side',
                                            fieldLabel: '% Recuperaci&oacute;n'
                                        }
                                    ]
                                }
                            ]
                        },

                        {
                            itemId: 'card-2',
                            xtype: 'container',
                            layout: 'fit',
                            items: [

                                {
                                    xtype: 'fieldset',
                                    title: 'Informaci&oacute;n del proveedor',
                                    bodyStyle: 'padding:0px',
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'Proveedor involucrado',
                                            name: 'nameSupplier',
                                            itemId: 'nameSupplier',
                                            maxLength: 300,
                                            msgTarget: 'side'
                                        },
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'Rut del proveedor',
                                            name: 'rutSupplier',
                                            itemId: 'rutSupplier',
                                            maxLength: 15,
                                            msgTarget: 'side'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    width: 220,
                    padding: 5,
                    layout: 'anchor',
                    dock: 'left',
                    items: [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Código',
                            labelAlign: 'top',
                            readOnly: true,
                            labelWidth: 50,
                            height: 40,
                            value: DukeSource.global.GiroConstants.DRAFT,
                            fieldCls: 'box-draft',
                            name: 'codeIncidentTemp',
                            itemId: 'codeIncidentTemp'
                        },
                        {
                            xtype: 'button',
                            text: 'Aprobaci&oacute;n',
                            flex: 0.5,
                            hidden: hidden('INC_WIR_BTN_ApproveIncident'),
                            margin: '0 0 0 2',
                            iconCls: 'evaluate',
                            name: 'btnApproveIncident',
                            itemId: 'btnApproveIncident',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            handler: function () {
                                var win = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.WindowApproveCorrelative', {
                                    modal: true,
                                    id: me.down('#idIncident').getValue(),
                                    actionExecute: 'approveIncident',
                                    module: DukeSource.global.GiroConstants.CYBER_SECURITY,
                                    parentWindow: me
                                });
                                if (me.down('#checkApprove').getValue() === DukeSource.global.GiroConstants.YES) {
                                    win.down('#dateApprove').setValue(me.down('#dateApprove').getValue());
                                    win.down('#userApprove').setValue(me.down('#userApprove').getValue());
                                    win.down('#commentApprove').setValue(me.down('#commentApprove').getValue());
                                    win.down('#saveApprove').setDisabled(true);
                                    win.down('#saveApprove').setVisible(false);
                                }
                                win.show();
                            }
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Inicio',
                                    itemId: 'options',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch',
                                        defaultMargins: {
                                            bottom: 1
                                        }
                                    },

                                    items: [
                                        {
                                            xtype: 'button',
                                            textAlign: 'left',
                                            text: 'Datos del incidente',
                                            handler: function (btn) {
                                                me.coloredButtonActive(btn, 0);
                                            }
                                        },
                                        {
                                            xtype: 'button',
                                            textAlign: 'left',
                                            text: 'Impacto y recuperaci&oacute;n',
                                            handler: function (btn) {
                                                me.coloredButtonActive(btn, 1);
                                            }
                                        },
                                        {
                                            xtype: 'button',
                                            textAlign: 'left',
                                            text: 'Informaci&oacute;n del proveedor',
                                            handler: function (btn) {
                                                me.coloredButtonActive(btn, 2);
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    layout: 'anchor',
                                    title: 'Informaci&oacute;n de registro',
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            readOnly: true,
                                            value: userName,
                                            fieldCls: 'readOnlyText',
                                            name: 'userRegisterTemp',
                                            itemId: 'userRegisterTemp',
                                            enforceMaxLength: true
                                        },
                                        {
                                            xtype: 'datetimefield',
                                            format: 'd/m/Y H:i:s',
                                            readOnly: true,
                                            anchor: '95%',
                                            labelAlign: 'top',
                                            fieldCls: 'readOnlyText',
                                            fieldLabel: 'Fecha de registro',
                                            name: 'dateRegisterTemp',
                                            itemId: 'dateRegisterTemp',
                                            enforceMaxLength: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            readOnly: true,
                                            value: fullName,
                                            anchor: '95%',
                                            labelAlign: 'top',
                                            fieldCls: 'readOnlyText',
                                            fieldLabel: 'Usuario de registro',
                                            name: 'fullNameUserRegisterTemp',
                                            itemId: 'fullNameUserRegisterTemp',
                                            enforceMaxLength: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            readOnly: true,
                                            value: descriptionWorkArea,
                                            anchor: '95%',
                                            labelAlign: 'top',
                                            fieldCls: 'readOnlyText',
                                            fieldLabel: '&Aacute;rea',
                                            name: 'workAreaRegisterTemp',
                                            itemId: 'workAreaRegisterTemp',
                                            enforceMaxLength: true
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            buttonAlign: 'right',

            buttons: [
                {
                    text: 'Salir',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                },
                {
                    text: 'Registrar',
                    action: 'saveIncidents',
                    scale: 'medium',
                    iconCls: 'save'
                }
            ],

            listeners: {
                show: function (win) {
                    this.heightMain = win.getHeight();
                    me.buttonActive = win.down('#options').items.items[0];
                    me.buttonTextActive = win.down('#options').items.items[0].getText();

                    me.buttonActive.setText(me.buttonActive.getText() + '<span style="color:red"> * </span>')
                },
                close: function (win) {
                    clearInterval(win.timer);
                }
            }
        });
        me.callParent(arguments);
    }

});
