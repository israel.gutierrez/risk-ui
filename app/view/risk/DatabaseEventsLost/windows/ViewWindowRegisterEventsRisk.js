Ext.define("ModelGridPanelRisk", {
  extend: "Ext.data.Model",
  fields: [
    "idRelationEventRisk",
    "idRisk",
    "versionCorrelative",
    "descriptionRisk",
    "descriptionCorrelative",
    "description",
    "codeRisk"
  ]
});

var StoreGridPanelRisk = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelRisk",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("ModelGridPanelControl", {
  extend: "Ext.data.Model",
  fields: [
    "versionCorrelative",
    "valueScoreControl",
    "percentScoreControl",
    "nameQualification",
    "idRiskWeaknessDetail",
    "idRisk",
    "idDetailControl",
    "idControl",
    "codeControl",
    "descriptionControl",
    "colorQualification"
  ]
});

var StoreGridPanelControl = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelControl",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("ModelGridPanelDebilityEvent", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "relationEventRisk",
    "idRiskWeaknessDetail",
    "idRisk",
    "versionCorrelative",
    "weakness",
    "descriptionWeakness",
    "codeWeakness",
    "descriptionFactorRisk",
    "state"
  ]
});

var StoreGridPanelDebilityEvent = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelDebilityEvent",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterEventsRisk",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowRegisterEventsRisk",
    height: 580,
    width: 1100,
    layout: "border",
    title: "RELACI&Oacute;N EVENTO CON EL RIESGO",
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        dockedItems: [
          {
            xtype: "toolbar",
            dock: "top",
            items: [
              {
                xtype: "button",
                scale: "medium",
                cls: "my-btn",
                overCls: "my-over",
                iconCls: "add",
                text: "AGREGAR",
                handler: function() {
                  if (
                    Ext.ComponentQuery.query(
                      "ViewWindowRegisterEventsRiskDetail"
                    )[0] == undefined
                  ) {
                    var k = Ext.create(
                      "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterEventsRiskDetail",
                      { maximized: true }
                    ).show();
                    k.down("#codeRisk").focus(false, 100);
                    var grid = Ext.ComponentQuery.query(
                      "ViewPanelEventsRiskOperational grid"
                    )[0];
                    var record = grid.getSelectionModel().getSelection()[0];
                    var fields = "p.idProcess";
                    var values = record.get("process");
                    var types = "Integer";
                    var operator = "equal";
                    k.down("#gridRisk").store.getProxy().extraParams = {
                      fields: fields,
                      values: values,
                      types: types,
                      operators: operator,
                      search: "simple"
                    };
                    k.down("#gridRisk").store.getProxy().url =
                      "http://localhost:9000/giro/findMatchRisk.htm";
                    k.down("#gridRisk")
                      .down("pagingtoolbar")
                      .moveFirst();
                  }
                }
              },
              "-",
              {
                xtype: "button",
                iconCls: "delete",
                scale: "medium",
                cls: "my-btn",
                overCls: "my-over",
                text: "ELIMINAR",
                action: "deleteRelationEventRisk"
              },
              "-",
              {
                xtype: "button",
                scale: "medium",
                cls: "my-btn",
                overCls: "my-over",
                text: "SALIR",
                scope: this,
                handler: this.close,
                iconCls: "logout"
              }
            ]
          }
        ],
        items: [
          {
            xtype: "gridpanel",
            region: "center",
            itemId: "gridRiskEvent",
            width: 450,
            padding: "2 0 2 2",
            title: "RIESGOS RELACIONADOS",
            titleAlign: "center",
            store: StoreGridPanelRisk,
            columns: [
              { xtype: "rownumberer", width: 25, sortable: false },
              { header: "C&Oacute;DIGO", dataIndex: "codeRisk", width: 60 },
              { header: "RIESGO", dataIndex: "description", flex: 1 },
              {
                header: "GLOSA",
                dataIndex: "descriptionRisk",
                width: 400,
                hidden: hidden("EWCDescription")
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridPanelRisk,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
              doRefresh: function() {
                var grid = this,
                  current = grid.store.currentPage;
                if (grid.fireEvent("beforechange", grid, current) !== false) {
                  grid.store.loadPage(current);
                  DukeSource.global.DirtyView.loadGridDefault(me.down("#riskDebilityStart"));
                  DukeSource.global.DirtyView.loadGridDefault(me.down("#gridControlSel"));
                }
              }
            },
            listeners: {
              render: function() {
                var gridPanel = Ext.ComponentQuery.query(
                  "ViewPanelEventsRiskOperational"
                )[0].down("#gridEvents");
                var grid = me.down("#gridRiskEvent");
                grid.store.getProxy().extraParams = {
                  idEvent: gridPanel
                    .getSelectionModel()
                    .getSelection()[0]
                    .get("idEvent")
                };
                grid.store.getProxy().url =
                  "http://localhost:9000/giro/showRelationEventRisk.htm";
                grid.down("pagingtoolbar").moveFirst();
              },
              itemclick: function(grid, record) {
                var grid = me.down("#riskDebilityStart");
                grid.store.getProxy().extraParams = {
                  idRiskEvent: record.get("idRelationEventRisk"),
                  idRisk: record.get("idRisk"),
                  versionCorrelative: record.get("versionCorrelative")
                };
                grid.store.getProxy().url =
                  "http://localhost:9000/giro/findRelationWeaknessEvent.htm";
                grid.down("pagingtoolbar").moveFirst();
                DukeSource.global.DirtyView.loadGridDefault(me.down("#gridControlSel"));
              }
            }
          },
          {
            xtype: "container",
            region: "east",
            flex: 1,
            split: true,
            padding: "2 2 2 0",
            layout: {
              type: "vbox",
              align: "stretch"
            },
            items: [
              {
                xtype: "gridpanel",
                flex: 0.8,
                itemId: "riskDebilityStart",
                title: "CAUSAS",
                titleAlign: "center",
                store: StoreGridPanelDebilityEvent,
                loadMask: true,
                columnLines: true,
                columns: [
                  { xtype: "rownumberer", width: 25, sortable: false },
                  {
                    header: "C&Oacute;DIGO",
                    dataIndex: "codeWeakness",
                    width: 60
                  },
                  {
                    header: "DESCRIPCI&Oacute;N",
                    dataIndex: "descriptionWeakness",
                    flex: 2
                  },
                  {
                    header: "FACTOR",
                    align: "center",
                    dataIndex: "descriptionFactorRisk",
                    width: 80
                  }
                ],

                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: StoreGridPanelDebilityEvent,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
                  doRefresh: function() {
                    var grid = this,
                      current = grid.store.currentPage;
                    if (
                      grid.fireEvent("beforechange", grid, current) !== false
                    ) {
                      grid.store.loadPage(current);
                      DukeSource.global.DirtyView.loadGridDefault(me.down("#gridControlSel"));
                    }
                  }
                },
                listeners: {
                  render: function() {
                    var grid = this;
                    DukeSource.global.DirtyView.loadGridDefault(grid);
                  },
                  itemclick: function(grid, record) {
                    var grid = me.down("#gridControlSel");
                    grid.store.getProxy().extraParams = {
                      idWeaknessEvent: record.get("id")
                    };
                    grid.store.getProxy().url =
                      "http://localhost:9000/giro/showRelationDetailControlEvent.htm";
                    grid.down("pagingtoolbar").moveFirst();
                  }
                }
              },
              {
                xtype: "gridpanel",
                itemId: "gridControlSel",
                flex: 1,
                padding: "2 0 0 0",
                titleAlign: "center",
                title: "CONTROLES ASOCIADOS",
                store: StoreGridPanelControl,
                columns: [
                  { xtype: "rownumberer", width: 25, sortable: false },
                  {
                    header: "C&Oacute;DIGO",
                    dataIndex: "codeControl",
                    width: 60
                  },
                  {
                    header: "CALIF.",
                    align: "center",
                    dataIndex: "nameQualification",
                    width: 90,
                    renderer: function(value, metaData, record) {
                      if (value != " ") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("colorQualification") +
                          ' !important;"';
                        return "<span>" + value + "</span>";
                      }
                    }
                  },
                  //, {header: 'SCORE (%)', dataIndex: 'percentScoreControl', width: 70}
                  {
                    header: "CONTROL",
                    dataIndex: "descriptionControl",
                    width: 400
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: StoreGridPanelControl,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  render: function() {
                    var grid = this;
                    DukeSource.global.DirtyView.loadGridDefault(grid);
                  }
                }
              }
            ]
          }
        ],
        buttonAlign: "center",
        buttons: [
          {
            xtype: "button",
            scale: "medium",
            cls: "my-btn",
            overCls: "my-over",
            text: "SALIR",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
