Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowHistoricalEvent', {
    extend: 'Ext.window.Window',
    requires: [
        'DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridHistoricalEvent'
    ],
    alias: 'widget.ViewWindowHistoricalEvent',
    layout: {
        type: 'fit'
    },
    title: 'HISTORICO DE EVENTOS',
    border: false,
    width: 950,
    height: 500,

    titleAlign: 'center',
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'ViewGridHistoricalEvent'
                }
            ]
        });

        me.callParent(arguments);
    }

});

