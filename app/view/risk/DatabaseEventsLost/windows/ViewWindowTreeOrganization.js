Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeOrganization', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowTreeOrganization',
    requires: [
        'Ext.tree.Panel',
        'DukeSource.view.risk.DatabaseEventsLost.treepanel.TreePanelArea',
        'Ext.tree.View'
    ],
    title: 'Organización interna',
    border: false,
    titleAlign: 'center',
    layout: 'fit',
    modal: true,
    height: 570,
    width: 900,
    initComponent: function () {
        var me = this;
        var backWindow = this.backWindow;
        var descriptionUnity = this.descriptionUnity;
        var unity = this.unity;
        var costCenter = this.costCenter;
        var valueNode;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'TreePanelArea',
                    listeners: {
                        select: function (view, record) {
                            valueNode = record;
                            if (record.data['depth'] === 1) {
                                me.down('#selectUnity').setDisabled(true);
                            } else {
                                me.down('#selectUnity').setDisabled(false);
                            }
                        },
                        itemdblclick: function (view, record) {
                            if (record.data['depth'] === 1) {
                                DukeSource.global.DirtyView.messageWarning('Registro no permitido, seleccione otra');
                            } else {
                                var description = record.getPath('text', ' &#8702; ');
                                backWindow.down('#' + descriptionUnity).setValue(description.substring(22));
                                backWindow.down('#' + unity).setValue(record.data['id']);
                                backWindow.down('#' + costCenter).setValue(record.raw.costCenter);

                                me.close();
                            }
                        }
                    }
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    itemId: 'selectUnity',
                    iconCls: 'save',
                    scale: 'medium',
                    handler: function () {
                        if (valueNode !== undefined) {
                            var tree = Ext.ComponentQuery.query('ViewWindowTreeOrganization treepanel')[0];
                            var node = tree.getSelectionModel().getSelection()[0];
                            var description = node.getPath('text', ' &#8702; ');
                            backWindow.down('#' + descriptionUnity).setValue(description.substring(22));
                            backWindow.down('#' + unity).setValue(node.data['id']);
                            backWindow.down('#' + costCenter).setValue(node.raw.costCenter);

                            me.close();
                        } else {
                            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM_CHECK);
                        }


                    }
                },
                {
                    text: 'Salir',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }
});
