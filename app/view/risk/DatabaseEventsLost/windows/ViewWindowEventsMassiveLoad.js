Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowEventsMassiveLoad', {
    extend: 'Ext.window.Window',
    border: false,
    alias: 'widget.ViewWindowEventsMassiveLoad',
    height: 120,
    width: 350,

    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    title: 'DOCUMENTOS ADJUNTOS',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    padding: '2 2 2 2',
                    layout: {
                        align: 'middle',
                        type: 'hbox'
                    },
                    bodyPadding: 10,
                    items: [

                        {
                            xtype: 'container',
                            height: 30,
                            layout: {
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'fileuploadfield',
                                    flex: 1,
                                    fieldLabel: 'ARCHIVO'
                                }
                            ]
                        }
                    ],
                    buttons: [
                        {
                            text: 'CARGAR',
                            iconCls: 'save',
                            action: 'saveMassiveLoadEvent'
                        },
                        {
                            text: 'SALIR',
                            scope: this,
                            handler: this.close,
                            iconCls: 'logout'

                        }
                    ]
                }


            ]
        });

        me.callParent(arguments);
    }

});