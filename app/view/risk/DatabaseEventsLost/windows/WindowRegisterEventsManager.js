Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.WindowRegisterEventsManager', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowRegisterEventsManager',
    layout: {
        type: 'fit'
    },
    width: 800,
    height: 350,
    title: 'Ingreso de eventos',
    requires: [
        'Ext.ux.DateTimeField'
    ],
    border: false,
    initComponent: function () {
        var me = this;
        var actionType = this.actionType;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    layout: {
                        align: 'stretch',
                        type: 'hbox'
                    },
                    padding: '0 0 0 2',
                    fieldDefaults: {
                        labelCls: 'changeSizeFontToEightPt'
                    },
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'container',
                            flex: 1,
                            layout: {
                                type: 'anchor'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    name: 'userRegister',
                                    itemId: 'userRegister',
                                    value: userName,
                                    hidden: true
                                },
                                {
                                    xtype: 'datetimefield',
                                    format: 'd/m/Y H:i:s',
                                    hidden: true,
                                    value: new Date(),
                                    name: 'dateRegister',
                                    itemId: 'dateRegister'
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'numberActionPlan',
                                    itemId: 'numberActionPlan',
                                    value: '0',
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'numberRiskAssociated',
                                    itemId: 'numberRiskAssociated',
                                    value: '0',
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'codeCorrelative',
                                    name: 'codeCorrelative'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'checkApprove',
                                    name: 'checkApprove'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'userApprove',
                                    name: 'userApprove'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'commentApprove',
                                    name: 'commentApprove'
                                },
                                {
                                    xtype: 'datetimefield',
                                    format: 'd/m/Y H:i:s',
                                    hidden: true,
                                    itemId: 'dateApprove',
                                    name: 'dateApprove'
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'codeEvent',
                                    itemId: 'codeEvent',
                                    value: DukeSource.global.GiroConstants.DRAFT,
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    itemId: 'eventMain',
                                    name: 'eventMain',
                                    value: 'S',
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    itemId: 'actionType',
                                    name: 'actionType',
                                    value: actionType,
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    name: 'fileAttachment',
                                    value: 'N',
                                    itemId: 'fileAttachment'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    name: 'stateTemp',
                                    value: 'P',
                                    itemId: 'stateTemp'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    name: 'numberRelation',
                                    value: 0,
                                    itemId: 'numberRelation'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    value: 'id',
                                    name: 'idEvent'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'idProcessType',
                                    name: 'idProcessType'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'idProcess',
                                    name: 'idProcess'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'idSubProcess',
                                    name: 'idSubProcess'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'idActivity',
                                    name: 'idActivity'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'businessLineOne',
                                    name: 'businessLineOne'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'businessLineTwo',
                                    name: 'businessLineTwo'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'businessLineThree',
                                    name: 'businessLineThree'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'eventOne',
                                    name: 'eventOne'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'eventTwo',
                                    name: 'eventTwo'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'eventThree',
                                    name: 'eventThree'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'factorRisk',
                                    name: 'factorRisk'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'product',
                                    name: 'product'
                                },
                                {
                                    xtype: 'NumberDecimalNumber',
                                    hidden: true,
                                    name: 'amountPayment',
                                    itemId: 'amountPayment',
                                    value: 0
                                },
                                {
                                    xtype: 'fieldset',
                                    title: 'Datos del Evento',
                                    anchor: '100%',
                                    layout: {
                                        type: 'anchor'
                                    },
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            anchor: '100%',
                                            fieldLabel: getName('EWMBusinessLineThree'),
                                            labelWidth: 110,
                                            msgTarget: 'side',
                                            allowBlank: blank('EWMBusinessLineThree'),
                                            hidden: hidden('EWMBusinessLineThree'),
                                            fieldCls: styleField('EWMBusinessLineThree'),
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            editable: false,
                                            name: 'businessLineThree',
                                            itemId: 'businessLineThree',
                                            displayField: 'description',
                                            valueField: 'idBusinessLineThree',

                                            forceSelection: true,
                                            store: {
                                                fields: ["idBusinessLineThree", "description"],
                                                autoLoad: false,
                                                proxy: {
                                                    actionMethods: {
                                                        create: "POST",
                                                        read: "POST",
                                                        update: "POST"
                                                    },
                                                    type: "ajax",
                                                    url: "http://localhost:9000/giro/showListBusinessLineThreeActives.htm",
                                                    extraParams: {
                                                        propertyOrder: "description"
                                                    },
                                                    reader: {
                                                        type: "json",
                                                        root: "data",
                                                        successProperty: "success"
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'datetimefield',
                                            format: 'd/m/Y H:i',
                                            fieldLabel: 'Fecha de ocurrencia',
                                            name: 'dateOccurrence',
                                            itemId: 'dateOccurrence',
                                            labelWidth: 110,
                                            enforceMaxLength: true,
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            allowBlank: false,
                                            listeners: {
                                                expand: function (field, value) {
                                                    if (me.down('#dateDiscovery').getValue() !== null) {
                                                        field.setMaxValue(me.down('#dateDiscovery').getValue());
                                                    } else {
                                                        field.setMaxValue(new Date());
                                                    }
                                                },
                                                blur: function (field, value) {
                                                    if (me.down('#dateDiscovery').getValue() !== null) {
                                                        field.setMaxValue(me.down('#dateDiscovery').getValue());
                                                    } else {
                                                        field.setMaxValue(new Date());
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'datetimefield',
                                            format: 'd/m/Y H:i',
                                            labelWidth: 110,
                                            hidden: hidden('EWMDateDiscovery'),
                                            fieldLabel: 'Fecha descubrimiento',
                                            name: 'dateDiscovery',
                                            itemId: 'dateDiscovery',
                                            enforceMaxLength: true,
                                            listeners: {
                                                expand: function (field, value) {
                                                    field.setMinValue(me.down('#dateOccurrence').getValue());
                                                    field.setMaxValue(new Date());
                                                },
                                                blur: function (field, value) {
                                                    field.setMinValue(me.down('#dateOccurrence').getValue());
                                                    field.setMaxValue(new Date());
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'UpperCaseTextFieldObligatory',
                                            fieldLabel: getName('EVN_WER_TXF_DescriptionShort'),
                                            anchor: '100%',
                                            labelWidth: 110,
                                            hidden: hidden('EVN_WER_TXF_DescriptionShort'),
                                            allowBlank: blank('EVN_WER_TXF_DescriptionShort'),
                                            name: 'descriptionShort',
                                            itemId: 'descriptionShort',
                                            enforceMaxLength: true,
                                            maxLength: 300
                                        },
                                        {
                                            xtype: 'UpperCaseTextArea',
                                            anchor: '100%',
                                            labelWidth: 110,
                                            height: 52,
                                            fieldLabel: getName('EWMDescriptionLarge'),
                                            name: 'descriptionLarge',
                                            itemId: 'descriptionLarge',
                                            enforceMaxLength: true,
                                            allowBlank: false,
                                            msgTarget: "side",
                                            fieldCls: "obligatoryTextField",
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            maxLength: 2000
                                        },
                                        {
                                            xtype: 'combobox',
                                            fieldLabel: 'Moneda',
                                            labelWidth: 110,
                                            msgTarget: 'side',
                                            fieldCls: 'obligatoryTextField',
                                            displayField: 'description',
                                            valueField: 'idCurrency',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            allowBlank: false,
                                            editable: false,
                                            forceSelection: true,
                                            name: 'currency',
                                            itemId: 'currency',
                                            store: {
                                                fields: ['idCurrency', 'description'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: "POST",
                                                        read: "POST",
                                                        update: "POST"
                                                    },
                                                    type: "ajax",
                                                    url: "http://localhost:9000/giro/showListCurrencyActivesComboBox.htm",
                                                    extraParams: {
                                                        propertyOrder: "description"
                                                    },
                                                    reader: {
                                                        type: "json",
                                                        root: "data",
                                                        successProperty: "success"
                                                    }
                                                }
                                            },
                                            listeners: {
                                                select: function (cbo) {
                                                    calculateNetLoss(me);
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'NumberDecimalNumberObligatory',
                                            allowBlank: false,
                                            labelWidth: 110,
                                            fieldLabel: getName('EWMAmountOrigin'),
                                            value: 0,
                                            name: 'amountOrigin',
                                            itemId: 'amountOrigin',
                                            listeners: {
                                                blur: function (field) {
                                                    calculateNetLoss(me);
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'NumberDecimalNumberObligatory',
                                            hidden: true,
                                            fieldLabel: 'Pérdida bruta',
                                            name: 'grossLoss',
                                            itemId: 'grossLoss',
                                            enforceMaxLength: true,
                                            fieldCls: 'readOnlyText',
                                            readOnly: true,
                                            maxLength: 10
                                        },
                                        {
                                            xtype: 'NumberDecimalNumber',
                                            fieldLabel: 'Recuperación',
                                            name: 'lossRecovery',
                                            itemId: 'lossRecovery',
                                            hidden: hidden('EWMLossRecovery'),
                                            enforceMaxLength: true,
                                            fieldCls: 'fieldBlue',
                                            maxLength: 20,
                                            value: 0,
                                            listeners: {
                                                blur: function (field) {
                                                    calculateNetLoss(me);
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'NumberDecimalNumber',
                                            allowBlank: true,
                                            fieldCls: 'numberNegative',
                                            name: 'totalAmountSpend',
                                            itemId: 'totalAmountSpend',
                                            hidden: hidden('EWMTotalAmountSpend'),
                                            value: 0,
                                            maxLength: 20,
                                            fieldLabel: 'Gastos',
                                            listeners: {
                                                blur: function (field) {
                                                    calculateNetLoss(me);
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'NumberDecimalNumber',
                                            allowBlank: true,
                                            fieldCls: 'numberNegative',
                                            name: 'amountProvision',
                                            itemId: 'amountProvision',
                                            hidden: hidden('EWMAmountProvision'),
                                            value: 0,
                                            maxLength: 20,
                                            fieldLabel: 'PROVISION',
                                            listeners: {
                                                blur: function (field) {
                                                    calculateNetLoss(me);
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'NumberDecimalNumberObligatory',
                                            fieldLabel: getName('EWMNetLoss'),
                                            hidden: hidden('EWMNetLoss'),
                                            name: 'netLoss',
                                            value: 0,
                                            itemId: 'netLoss',
                                            minValue: -1000000,
                                            enforceMaxLength: true,
                                            fieldCls: 'readOnlyText',
                                            readOnly: true,
                                            maxLength: 20
                                        },
                                        {
                                            xtype: 'NumberDecimalNumber',
                                            name: 'sureAmountRecovery',
                                            hidden: true,
                                            value: 0,
                                            itemId: 'sureAmountRecovery'
                                        },
                                        {
                                            xtype: 'NumberDecimalNumber',
                                            hidden: true,
                                            value: 0,
                                            name: 'amountThirdRecovery',
                                            itemId: 'amountThirdRecovery'
                                        },
                                        {
                                            xtype: 'filefield',
                                            anchor: '100%',
                                            labelWidth: 110,
                                            fieldLabel: 'Documento adjunto',
                                            name: 'document',
                                            buttonConfig: {
                                                iconCls: 'tesla even-attachment'
                                            },
                                            buttonText: ""
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    width: 200,
                    padding: 5,
                    layout: 'anchor',
                    dock: 'left',
                    items: [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Código',
                            labelAlign: 'top',
                            readOnly: true,
                            labelWidth: 50,
                            height: 40,
                            value: DukeSource.global.GiroConstants.DRAFT,
                            fieldCls: 'box-draft',
                            name: 'codeEventTemp',
                            itemId: 'codeEventTemp'
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    layout: 'anchor',
                                    title: 'Informaci&oacute;n de registro',
                                    items: [
                                        {
                                            xtype: 'datetimefield',
                                            format: 'd/m/Y H:i:s',
                                            readOnly: true,
                                            anchor: '95%',
                                            labelAlign: 'top',
                                            fieldCls: 'readOnlyText',
                                            fieldLabel: 'Fecha de registro',
                                            name: 'dateRegisterTemp',
                                            itemId: 'dateRegisterTemp',
                                            enforceMaxLength: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            readOnly: true,
                                            value: fullName,
                                            anchor: '95%',
                                            labelAlign: 'top',
                                            fieldCls: 'readOnlyText',
                                            fieldLabel: 'Usuario de registro',
                                            name: 'fullNameUserRegisterTemp',
                                            itemId: 'fullNameUserRegisterTemp',
                                            enforceMaxLength: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            readOnly: true,
                                            value: descriptionWorkArea,
                                            anchor: '95%',
                                            labelAlign: 'top',
                                            fieldCls: 'readOnlyText',
                                            fieldLabel: '&Aacute;rea',
                                            name: 'workAreaUserRegisterTemp',
                                            itemId: 'workAreaUserRegisterTemp',
                                            enforceMaxLength: true
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Salir',
                    scope: this,
                    handler: this.close,
                    scale: 'medium',
                    iconCls: 'logout'
                },
                {
                    text: 'Reportar',
                    itemId: 'saveEventOperational',
                    iconCls: 'send',
                    scale: 'medium',
                    handler: function () {
                        var panel = Ext.ComponentQuery.query('ViewPanelEventsManager')[0];

                        var form = me.down('form');
                        if (form.getForm().isValid()) {
                            form.getForm().submit({

                                url: 'http://localhost:9000/giro/saveEventMaster.htm?nameView=ViewPanelEventsRiskOperational',
                                waitMsg: 'Saving...',
                                method: 'POST',
                                success: function (form, action) {
                                    var valor = Ext.decode(action.response.responseText);

                                    if (valor.success) {
                                        DukeSource.global.DirtyView.messageNormal(valor.message);
                                        me.close();
                                        var grid = panel.down('grid');
                                        grid.store.getProxy().url = 'http://localhost:9000/giro/getAllEventForManager.htm';
                                        grid.down('pagingtoolbar').moveFirst();

                                    } else {
                                        DukeSource.global.DirtyView.messageWarning(valor.message);
                                    }
                                },
                                failure: function (form, action) {
                                    var valor = Ext.decode(action.response.responseText);
                                    if (!valor.success) {
                                        DukeSource.global.DirtyView.messageWarning(valor.message);
                                    }
                                }
                            });
                        } else {
                            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
                        }
                    }
                }
            ],
            buttonAlign: 'right',
            listeners: {
                close: function () {
                    clearInterval(me.timer);
                }
            }
        });
        me.callParent(arguments);
    }
});

function calculateNetLoss(me) {
    var amountOrigin = me.down('#amountOrigin').getValue() === '' ? '0.00' : me.down('#amountOrigin').getValue();
    var grossLoss = me.down('#grossLoss');

    var spend = me.down('#totalAmountSpend').getValue() === '' ? '0.00' : me.down('#totalAmountSpend').getValue();
    var provision = me.down('#amountProvision').getValue() === '' ? '0.00' : me.down('#amountProvision').getValue();
    var lossRecovery = me.down('#lossRecovery').getValue() === '' ? '0.00' : me.down('#lossRecovery').getValue();

    grossLoss.setValue(parseFloat(amountOrigin));

    var result = (parseFloat(grossLoss.getValue()) - parseFloat(lossRecovery) + parseFloat(spend) + parseFloat(provision));

    me.down('#totalAmountSpend').setValue(spend);
    me.down('#lossRecovery').setValue(lossRecovery);
    me.down('#amountProvision').setValue(provision);
    me.down('#netLoss').setValue(result);
}
