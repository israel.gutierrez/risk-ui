Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeProcess', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowTreeProcess',
    requires: [
        'Ext.tree.Panel',
        'DukeSource.view.risk.DatabaseEventsLost.treepanel.TreePanelProcess',
        'Ext.tree.View'
    ],
    title: 'Procesos',
    border: false,
    titleAlign: 'center',
    layout: 'fit',
    modal: true,
    height: 570,
    width: 900,
    initComponent: function () {
        var me = this;
        var valueNode;
        var backWindow = this.backWindow;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'TreePanelProcess',
                    listeners: {
                        select: function (view, record) {
                            valueNode = record
                        },
                        itemdblclick: function (view, record) {
                            var description = record.getPath('text', ' &#8702; ');
                            backWindow.down('#descriptionProcess').setValue(description.substring(22));
                            backWindow.down('#idProcessType').setValue(record.data['idProcessType']);
                            backWindow.down('#idProcess').setValue(record.data['idProcess']);
                            backWindow.down('#idSubProcess').setValue(record.data['idSubProcess']);
                            backWindow.down('#idActivity').setValue(record.data['idActivity']);
                            backWindow.down('#codeProcess').setValue(record.data['abbreviation']);
                            me.close();
                        }
                    }
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    iconCls: 'save',
                    scale: 'medium',
                    handler: function () {
                        if (valueNode !== undefined) {
                            var description = valueNode.getPath('text', ' &#8702; ');
                            backWindow.down('#descriptionProcess').setValue(description.substring(22));
                            backWindow.down('#idProcessType').setValue(valueNode.data['idProcessType']);
                            backWindow.down('#idProcess').setValue(valueNode.data['idProcess']);
                            backWindow.down('#idSubProcess').setValue(valueNode.data['idSubProcess']);
                            backWindow.down('#idActivity').setValue(valueNode.data['idActivity']);
                            backWindow.down('#codeProcess').setValue(valueNode.data['abbreviation']);
                            me.close();
                        } else {
                            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
                        }


                    }
                },
                {
                    text: 'Salir',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }
});
