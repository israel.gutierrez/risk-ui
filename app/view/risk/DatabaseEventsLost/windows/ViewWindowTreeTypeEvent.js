Ext.define("ModelTreeWindowEventRisk", {
  extend: "Ext.data.Model",
  fields: [
    { name: "id", type: "string" },
    { name: "text", type: "string" },
    { name: "idEventOne", type: "string" },
    { name: "idEventTwo", type: "string" },
    { name: "idEventThree", type: "string" },
    { name: "description", type: "string" },
    { name: "parent", type: "string" },
    { name: "parentId", type: "string" },
    { name: "state", type: "string" }
  ]
});

var StoreTreeWindowEventRisk = Ext.create("Ext.data.TreeStore", {
  extend: "Ext.data.Store",
  model: "ModelTreeWindowEventRisk",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListEventActives.htm",
    extraParams: {
      node: "root",
      depth: 0
    },
    reader: {
      type: "json",
      totalProperty: "totalCount",
      successProperty: "success"
    }
  },
  root: {
    id: "root",
    expanded: true
  },
  folderSort: true,
  sorters: [
    {
      property: "text",
      direction: "ASC"
    }
  ]
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeTypeEvent",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowTreeTypeEvent",
    requires: ["Ext.tree.Panel", "Ext.tree.View"],
    title: "Tipo de evento",
    border: false,
    titleAlign: "center",
    layout: "fit",
    modal: true,
    height: 570,
    width: 900,
    initComponent: function() {
      var me = this;
      var valueNode;
      var winParent = this.winParent;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "treepanel",
            useArrows: true,
            multiSelect: true,
            singleExpand: true,
            rootVisible: false,
            store: StoreTreeWindowEventRisk,
            listeners: {
              select: function(view, record) {
                valueNode = record;
              },
              itemdblclick: function(view, record) {
                var description = record.getPath("text", " &#8702; ");
                winParent
                  .down("#descriptionEventOne")
                  .setValue(description.substring(22));
                winParent.down("#eventOne").setValue(record.raw.idEventOne);
                winParent.down("#eventTwo").setValue(record.raw.idEventTwo);
                winParent.down("#eventThree").setValue(record.raw.idEventThree);
                me.close();
              }
            }
          }
        ],
        buttons: [
          {
            text: "Guardar",
            iconCls: "save",
            scale: "medium",
            handler: function() {
              if (valueNode !== undefined) {
                var tree = me.down("treepanel");
                var node = tree.getSelectionModel().getSelection()[0];

                var description = valueNode.getPath("text", " &#8702; ");
                winParent
                  .down("#descriptionEventOne")
                  .setValue(description.substring(22));
                winParent.down("#eventOne").setValue(node.raw.idEventOne);
                winParent.down("#eventTwo").setValue(node.raw.idEventTwo);
                winParent.down("#eventThree").setValue(node.raw.idEventThree);
                me.close();
              } else {
                DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM_CHECK);
              }
            }
          },
          {
            text: "Salir",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      });
      me.callParent(arguments);
    }
  }
);
