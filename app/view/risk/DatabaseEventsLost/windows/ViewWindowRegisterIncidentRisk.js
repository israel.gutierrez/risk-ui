Ext.define('ModelGridPanelRisk', {
    extend: 'Ext.data.Model',
    fields: [
        'idIncidentRisk'
        , 'risk'
        , 'versionCorrelative'
        , 'descriptionRisk'
        , 'incident'
        , 'description'
        , 'codeRisk'
    ]
});

Ext.define('ModelGridPanelControl', {
    extend: 'Ext.data.Model',
    fields: [
        'versionCorrelative'
        , 'valueScoreControl'
        , 'percentScoreControl'
        , 'nameQualification'
        , 'idRiskWeaknessDetail'
        , 'idRisk'
        , 'idDetailControl'
        , 'idControl'
        , 'codeControl'
        , 'descriptionControl'
        , 'colorQualification'

    ]
});
var StoreGridIncidentRisk = Ext.create('Ext.data.Store', {
    model: 'ModelGridPanelRisk',
    autoLoad: false,
    proxy: {
        actionMethods: {
            create: 'POST',
            read: 'POST',
            update: 'POST'
        },
        type: 'ajax',
        url: 'loadGridDefault.htm',
        reader: {
            totalProperty: 'totalCount',
            root: 'data',
            successProperty: 'success'
        }
    }
});

Ext.define('ModelGridPanelIncident', {
    extend: 'Ext.data.Model',
    fields: [
        'idRelationEventRisk'
        , 'idRisk'
        , 'versionCorrelative'
        , 'descriptionRisk'
        , 'descriptionCorrelative'
        , 'description'
        , 'codeRisk'
    ]
});
var StoreGridIncident = Ext.create('Ext.data.Store', {
    model: 'ModelGridPanelIncident',
    autoLoad: false,
    proxy: {
        actionMethods: {
            create: 'POST',
            read: 'POST',
            update: 'POST'
        },
        type: 'ajax',
        url: 'loadGridDefault.htm',
        reader: {
            totalProperty: 'totalCount',
            root: 'data',
            successProperty: 'success'
        }
    }
});

Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterIncidentRisk', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowRegisterIncidentRisk',
    requires: [
        'DukeSource.view.risk.parameter.combos.ViewComboProcess',
        'DukeSource.view.risk.util.search.AdvancedSearchRisk'
    ],
    height: 580,
    width: 1100,
    layout: 'border',
    title: 'Relaci&oacute;n incidente con el riesgo',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        var process = me.process;
        var idIncident = me.idIncident;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'gridpanel',
                    region: 'center',
                    itemId: 'gridIncidentRisk',
                    width: 450,
                    padding: '2 0 2 2',
                    title: 'RIESGOS RELACIONADOS',
                    store: StoreGridIncidentRisk,
                    tbar: [
                        {
                            xtype: 'button',
                            iconCls: 'delete',
                            text: 'ELIMINAR',
                            handler: function (btn) {
                                var grid = me.down('#gridIncidentRisk');
                                var assigned = grid.getSelectionModel().getSelection()[0];
                                if (assigned == undefined) {
                                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_ITEM, Ext.Msg.WARNING);
                                }
                                else {
                                    Ext.MessageBox.show({
                                        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                                        msg: 'Esta seguro que desea eliminar el riesgo',
                                        icon: Ext.Msg.QUESTION,
                                        buttonText: {
                                            yes: "Si"
                                        },
                                        buttons: Ext.MessageBox.YESNO,
                                        fn: function (btn) {
                                            if (btn == 'yes') {
                                                DukeSource.lib.Ajax.request({
                                                    method: 'POST',
                                                    url: 'deleteRelationIncidentRisk.htm',
                                                    params: {
                                                        idIncidentRisk: assigned.get('idIncidentRisk'),
                                                        idRisk: assigned.get('risk'),
                                                        idIncident: assigned.get('incident')
                                                    },
                                                    success: function (response) {
                                                        response = Ext.decode(response.responseText);
                                                        if (response.success) {
                                                            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                                                            grid.down('pagingtoolbar').moveFirst();
                                                        } else {
                                                            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, response.mensaje, Ext.Msg.ERROR);
                                                        }
                                                    },
                                                    failure: function () {
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    ],
                    columns: [
                        {header: 'ID', dataIndex: 'risk', width: 60}
                        , {header: 'Código', dataIndex: 'codeRisk', width: 120}
                        , {header: 'Riesgo', dataIndex: 'descriptionRisk', width: 500}
                    ],
                    bbar: {
                        xtype: 'pagingtoolbar',
                        pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                        store: StoreGridIncidentRisk,
                        displayInfo: true,
                        displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                        emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
                        doRefresh: function () {
                            var grid = this,
                                current = grid.store.currentPage;
                            if (grid.fireEvent('beforechange', grid, current) !== false) {
                                grid.store.loadPage(current);
                            }
                        }
                    },
                    listeners: {
                        render: function () {
                            var grid = me.down('#gridIncidentRisk');
                            grid.store.getProxy().extraParams = {
                                idRisk: '',
                                idIncident: idIncident
                            };
                            grid.store.getProxy().url = 'showRelationIncidentRisk.htm';
                            grid.down('pagingtoolbar').moveFirst();
                        }
                    }
                },
                {
                    xtype: 'container',
                    region: 'west',
                    flex: 1,
                    split: true,
                    padding: '2 2 2 0',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'AdvancedSearchRisk',
                            collapsed: false,
                            padding: '2 2 2 2',
                            flex: 1.5,
                            buttons: [
                                {
                                    text: 'BUSCAR',
                                    scale: 'medium',
                                    iconCls: 'search',
                                    handler: function () {
                                        var riskAvailable = me.down('#riskIncident');
                                        var process = me.down('#process').getValue() == '' ? '' : '(' + me.down('#process').getValue() + ')';
                                        var processType = me.down('#processType').getValue() == undefined ? '' : me.down('#processType').getValue();
                                        var fields = 'r.idRisk,r.codeRisk,pt.idProcessType,p.idProcess';
                                        var values = me.down('#idRisk').getValue() + ';' + me.down('#codeRisk').getValue() + ';' + processType + ';' + process;
                                        var types = 'Long,String,Long,Long';
                                        var operator = 'equal,equal,equal,multiple';
                                        riskAvailable.store.getProxy().extraParams = {
                                            fields: fields,
                                            values: values,
                                            types: types,
                                            operators: operator,
                                            search: 'simple'
                                        };
                                        riskAvailable.store.getProxy().url = 'findMatchRisk.htm';
                                        riskAvailable.down('pagingtoolbar').moveFirst();
                                    }
                                },
                                {
                                    text: 'LIMPIAR',
                                    scale: 'medium',
                                    iconCls: 'clear',
                                    handler: function () {
                                        me.down('#processType').reset();
                                        me.down('#process').reset();
                                        me.down('#codeRisk').reset();
                                        me.down('grid').store.getProxy().extraParams = {idRiskEvaluationMatrix: Ext.ComponentQuery.query('ViewPanelPreviousIdentifyRiskOperational')[0].down('grid').getSelectionModel().getSelection()[0].get('idRiskEvaluationMatrix')};
                                        me.down('grid').store.getProxy().url = 'getRiskByRiskEvaluationMatrix.htm';
                                        me.down('grid').down('pagingtoolbar').moveFirst();
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'gridpanel',
                            flex: 2,
                            itemId: 'riskIncident',
                            title: 'RIESGOS',
                            titleAlign: 'center',
                            store: StoreGridIncident,
                            loadMask: true,
                            columnLines: true,
                            tbar: [
                                {
                                    xtype: 'button',
                                    iconCls: 'add',
                                    text: 'Agregar',
                                    handler: function (btn) {
                                        var grid = me.down('#riskIncident');
                                        var recordRisk = grid.getSelectionModel().getSelection()[0];
                                        if (recordRisk == undefined) {
                                            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_ITEM, Ext.Msg.WARNING);
                                        } else {
                                            var grid = me.down('#gridIncidentRisk').getStore().getRange();
                                            var flag = true
                                            for (var i = 0; i < grid.length; i++) {
                                                if (recordRisk.get('idRisk') == grid[i].get('risk') * 1) {
                                                    flag = true
                                                    break;
                                                } else {
                                                    flag = false
                                                }
                                            }
                                            if (flag && grid.length > 0) {
                                                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, 'El RIESGO se encuentra relacionado seleccione otro por favor', Ext.Msg.WARNING);
                                            } else {
                                                Ext.MessageBox.show({
                                                    title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                                                    msg: 'Esta seguro que desea relacionar el riesgo',
                                                    icon: Ext.Msg.QUESTION,
                                                    buttonText: {
                                                        yes: "Si"
                                                    },
                                                    buttons: Ext.MessageBox.YESNO,
                                                    fn: function (btn) {
                                                        if (btn == 'yes') {
                                                            DukeSource.lib.Ajax.request({
                                                                method: 'POST',
                                                                url: 'saveRelationIncidentRisk.htm',
                                                                params: {
                                                                    idRisk: recordRisk.get('idRisk'),
                                                                    idIncident: idIncident
                                                                },
                                                                success: function (response) {
                                                                    response = Ext.decode(response.responseText);
                                                                    if (response.success) {
                                                                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                                                                        me.down('#gridIncidentRisk').down('pagingtoolbar').moveFirst();
                                                                    } else {
                                                                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, response.mensaje, Ext.Msg.ERROR);
                                                                    }
                                                                },
                                                                failure: function () {
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                            ],
                            columns: [
                                {header: 'ID', dataIndex: 'idRisk', width: 60}
                                , {header: 'Código', dataIndex: 'codeRisk', width: 120}
                                , {header: 'Riesgo', dataIndex: 'description', width: 450}
                            ],
                            bbar: {
                                xtype: 'pagingtoolbar',
                                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                                store: StoreGridIncident,
                                displayInfo: true,
                                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
                                doRefresh: function () {
                                    var grid = this,
                                        current = grid.store.currentPage;
                                    if (grid.fireEvent('beforechange', grid, current) !== false) {
                                        grid.store.loadPage(current);
                                    }
                                }
                            },
                            listeners: {
                                render: function () {
                                    var grid = me.down('#riskIncident');
                                    var fields = 'p.idProcess';
                                    var values = process;
                                    var types = 'Integer';
                                    var operator = 'equal';
                                    grid.store.getProxy().extraParams = {
                                        fields: fields,
                                        values: values,
                                        types: types,
                                        operators: operator,
                                        search: 'simple'
                                    };
                                    grid.store.getProxy().url = 'findMatchRisk.htm';
                                    grid.down('pagingtoolbar').moveFirst();
                                }
                            }
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'SALIR',
                    scale: 'medium',
                    iconCls: 'logout',
                    handler: function (btn) {
                        me.close();
                    }
                }
            ]
        });
        me.callParent(arguments);
    }
});
