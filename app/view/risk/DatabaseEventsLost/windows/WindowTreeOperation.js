Ext.define("ModelTreeWindowOperation", {
  extend: "Ext.data.Model",
  fields: [
    { name: "id", type: "string" },
    { name: "text", type: "string" },
    { name: "idOperation", type: "string" },
    { name: "description", type: "string" },
    { name: "parent", type: "string" },
    { name: "parentId", type: "string" },
    { name: "state", type: "string" }
  ]
});

var StoreTreeWindowOperation = Ext.create("Ext.data.TreeStore", {
  extend: "Ext.data.Store",
  model: "ModelTreeWindowOperation",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showOperationActives.htm",
    extraParams: {
      node: "root",
      depth: 0
    },
    reader: {
      type: "json",
      totalProperty: "totalCount",
      successProperty: "success"
    }
  },
  root: {
    id: "root",
    expanded: true
  },
  folderSort: true,
  sorters: [
    {
      property: "text",
      direction: "ASC"
    }
  ]
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.WindowTreeOperation",
  {
    extend: "Ext.window.Window",
    alias: "widget.WindowTreeOperation",
    requires: ["Ext.tree.Panel", "Ext.tree.View"],
    title: "Operacion",
    border: false,
    titleAlign: "center",
    layout: "fit",
    modal: true,
    height: 570,
    width: 900,
    initComponent: function() {
      var me = this;
      var valueNode = this.valueNode;
      var winParent = this.winParent;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "treepanel",
            cls: "customTree-grid",
            useArrows: true,
            multiSelect: true,
            singleExpand: true,
            rootVisible: false,
            store: StoreTreeWindowOperation,
            listeners: {
              select: function(view, record) {
                valueNode = record;
              },
              itemdblclick: function(view, record) {
                var description = record.raw.description;
                winParent.down("#descriptionOperation").setValue(description);
                winParent
                  .down("#descriptionOperation")
                  .setFieldStyle("color:#0000EE");
                winParent.down("#operation").setValue(record.raw.id);
                me.close();
              }
            }
          }
        ],
        buttons: [
          {
            text: "Guardar",
            iconCls: "save",
            scale: "medium",
            handler: function() {
              if (valueNode !== undefined) {
                var tree = me.down("treepanel");
                var node = tree.getSelectionModel().getSelection()[0];
                var description = node.raw.description;
                winParent
                  .down("#descriptionOperation")
                  .setFieldStyle("color:#0000EE");
                winParent.down("#descriptionOperation").setValue(description);
                winParent.down("#operation").setValue(node.raw.id);
                me.close();
              } else {
                DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM_CHECK);
              }
            }
          },
          {
            text: "Salir",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);
