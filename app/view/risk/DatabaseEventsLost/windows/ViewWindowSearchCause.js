Ext.define("ModelSearchCause", {
  extend: "Ext.data.Model",
  fields: ["idReasonSafety", "codeCause", "description"]
});

var storeSearchCause = Ext.create("Ext.data.Store", {
  model: "ModelSearchCause",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowSearchCause",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowSearchCause",
    layout: {
      align: "stretch",
      type: "vbox"
    },
    anchorSize: 100,
    title: "BUSCAR CAUSA",
    titleAlign: "center",
    width: 800,
    height: 500,
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            height: 45,
            padding: "2 2 2 2",
            bodyPadding: 10,
            items: [
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "combobox",
                    value: "2",
                    labelWidth: 40,
                    width: 190,
                    fieldLabel: "TIPO",
                    store: [
                      ["1", "CODIGO"],
                      ["2", "DESCRIPCION"]
                    ]
                  },
                  {
                    xtype:"UpperCaseTextField",
                    flex: 2,
                    listeners: {
                      afterrender: function(field) {
                        field.focus(false, 200);
                      },
                      specialkey: function(field, e) {
                        var property = "";
                        if (
                          Ext.ComponentQuery.query(
                            "ViewWindowSearchCause combobox"
                          )[0].getValue() == "1"
                        ) {
                          property = "codeCause";
                        } else {
                          property = "description";
                        }
                        if (e.getKey() === e.ENTER) {
                          var grid = me.down("grid");
                          DukeSource.global.DirtyView.searchPaginationGridToEnter(
                            field,
                            grid,
                            grid.down("pagingtoolbar"),
                            "http://localhost:9000/giro/findReasonSafety.htm",
                            property,
                            "description"
                          );
                        }
                      }
                    }
                  }
                ]
              }
            ]
          },
          {
            xtype: "container",
            flex: 3,
            padding: "2 2 2 2",
            layout: {
              align: "stretch",
              type: "hbox"
            },
            items: [
              {
                xtype: "gridpanel",
                padding: "0 1 0 0",
                store: storeSearchCause,
                flex: 1,
                titleAlign: "center",
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    dataIndex: "codeCause",
                    width: 120,
                    text: "CODIGO"
                  },
                  {
                    dataIndex: "description",
                    flex: 1,
                    text: "CAUSAS DEL INCIDENTE"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: storeSearchCause,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  render: function() {
                    var me = this;
                    DukeSource.global.DirtyView.searchPaginationGridNormal(
                      "",
                      me,
                      me.down("pagingtoolbar"),
                      "http://localhost:9000/giro/findReasonSafety.htm",
                      "description",
                      "description"
                    );
                  }
                }
              }
            ]
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
