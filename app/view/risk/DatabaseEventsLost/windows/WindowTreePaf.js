Ext.define("ModelTreeWindowPaf", {
  extend: "Ext.data.Model",
  fields: [
    { name: "id", type: "string" },
    { name: "text", type: "string" },
    { name: "idOperation", type: "string" },
    { name: "description", type: "string" },
    { name: "codePlace", type: "string" },
    { name: "parent", type: "string" },
    { name: "parentId", type: "string" },
    { name: "state", type: "string" }
  ]
});

var StoreTreeWindowPaf = Ext.create("Ext.data.TreeStore", {
  extend: "Ext.data.Store",
  model: "ModelTreeWindowPaf",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showPafActives.htm",
    extraParams: {
      node: "root",
      depth: 0
    },
    reader: {
      type: "json",
      totalProperty: "totalCount",
      successProperty: "success"
    }
  },
  root: {
    id: "root",
    expanded: true
  },
  folderSort: true,
  sorters: [
    {
      property: "text",
      direction: "ASC"
    }
  ]
});

Ext.define("DukeSource.view.risk.DatabaseEventsLost.windows.WindowTreePaf", {
  extend: "Ext.window.Window",
  alias: "widget.WindowTreePaf",
  requires: ["Ext.tree.Panel", "Ext.tree.View"],
  title: "Punto de atención financiera",
  border: false,
  titleAlign: "center",
  layout: "fit",
  modal: true,
  height: 570,
  width: 900,
  initComponent: function() {
    var me = this;
    var winParent = this.winParent;
    var valueNode;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "treepanel",
          cls: "customTree-grid",
          useArrows: true,
          multiSelect: true,
          singleExpand: true,
          rootVisible: false,
          store: StoreTreeWindowPaf,
          listeners: {
            select: function(view, record) {
              valueNode = record;
            },
            itemdblclick: function(view, record) {
              var tree = me.down("treepanel");
              var codePaf = "";
              var codesPlace = "";
              var selections = tree.getSelectionModel().getSelection();
              Ext.Array.each(selections, function(record) {
                codePaf = codePaf + record.raw["codeIntern"] + ",";
                if (record.raw["codePlace"] !== undefined)
                  codesPlace = codesPlace + record.raw["codePlace"] + ",";
              });

              winParent
                .down("#descriptionPaf")
                .setValue(codePaf.substr(0, codePaf.length - 1));
              winParent
                .down("#codePaf")
                .setValue(codePaf.substr(0, codePaf.length - 1));
              winParent
                .down("#codePlace")
                .setValue(codesPlace.substr(0, codesPlace.length - 1));
              me.close();
            }
          }
        }
      ],
      buttons: [
        {
          text: "Guardar",
          iconCls: "save",
          scale: "medium",
          handler: function() {
            if (valueNode !== undefined) {
              var tree = me.down("treepanel");
              var codePaf = "";
              var codesPlace = "";
              var selections = tree.getSelectionModel().getSelection();
              Ext.Array.each(selections, function(record) {
                codePaf = codePaf + record.raw["codeIntern"] + ",";
                if (record.raw["codePlace"] !== undefined)
                  codesPlace = codesPlace + record.raw["codePlace"] + ",";
              });

              winParent
                .down("#descriptionPaf")
                .setValue(codePaf.substr(0, codePaf.length - 1));
              winParent
                .down("#codePaf")
                .setValue(codePaf.substr(0, codePaf.length - 1));
              winParent
                .down("#codePlace")
                .setValue(codesPlace.substr(0, codesPlace.length - 1));
              me.close();
            } else {
              DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM_CHECK);
            }
          }
        },
        {
          text: "Salir",
          scope: this,
          scale: "medium",
          handler: this.close,
          iconCls: "Log-Out-icon"
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});
