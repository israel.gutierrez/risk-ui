Ext.require(["Ext.ux.CheckColumn"]);
Ext.require(["Ext.ux.CheckedColumn"]);
Ext.require(["DukeSource.view.risk.util.search.AdvancedSearchRisk"]);
Ext.define("ModelGridPanelRisk", {
  extend: "Ext.data.Model",
  fields: [
    "codeRisk",
    "descriptionCorrelative",
    "descriptionRisk",
    "description",
    "idRisk",
    "versionCorrelative"
  ]
});

var StoreGridPanelRisk = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelRisk",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("ModelGridPanelControl", {
  extend: "Ext.data.Model",
  fields: [
    "versionCorrelative",
    "valueScoreControl",
    "percentScoreControl",
    "nameQualification",
    "idRiskWeaknessDetail",
    "idRisk",
    "idDetailControl",
    "idControl",
    "codeControl",
    "descriptionControl",
    "colorQualification"
  ]
});

var StoreGridPanelControl = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelControl",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("ModelGridPanelDebilityEventRelation", {
  extend: "Ext.data.Model",
  fields: [
    "idRiskWeaknessDetail",
    "description",
    "factorRisk",
    "nameFactorRisk",
    "average",
    "idRisk",
    "versionCorrelative",
    "idWeakness",
    "codeWeakness",
    "eventOne",
    "eventTwo",
    "eventThree",
    "idRelation",
    "totalRelation"
  ]
});

var StoreGridPanelDebilityEventRelation = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelDebilityEventRelation",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterEventsRiskDetail",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowRegisterEventsRiskDetail",
    height: 580,
    width: 1100,
    layout: "border",
    title: "AGREGAR RELACI&Oacute;N",
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "container",
            region: "center",
            border: false,
            flex: 1,
            layout: {
              type: "vbox",
              align: "stretch"
            },
            items: [
              {
                xtype: "AdvancedSearchRisk",
                collapsed: false,
                padding: "2 2 2 2",
                height: 210,
                buttons: [
                  {
                    text: "BUSCAR",
                    scale: "medium",
                    iconCls: "search",
                    handler: function() {
                      var process =
                        me.down("#process").getValue() == ""
                          ? ""
                          : "(" + me.down("#process").getValue() + ")";
                      var processType =
                        me.down("#processType").getValue() == undefined
                          ? ""
                          : me.down("#processType").getValue();
                      var fields =
                        "r.idRisk,r.codeRisk,pt.idProcessType,p.idProcess";
                      var values =
                        me.down("#idRisk").getValue() +
                        ";" +
                        me.down("#codeRisk").getValue() +
                        ";" +
                        processType +
                        ";" +
                        process;
                      var types = "Long,String,Long,Long";
                      var operator = "equal,equal,equal,multiple";
                      me.down("grid").store.getProxy().extraParams = {
                        fields: fields,
                        values: values,
                        types: types,
                        operators: operator,
                        search: "simple"
                      };
                      me.down("grid").store.getProxy().url =
                        "http://localhost:9000/giro/findMatchRisk.htm";
                      me.down("grid")
                        .down("pagingtoolbar")
                        .moveFirst();
                    }
                  },
                  {
                    text: "LIMPIAR",
                    scale: "medium",
                    iconCls: "clear",
                    handler: function() {
                      me.down("#processType").reset();
                      me.down("#process").reset();
                      me.down("#codeRisk").reset();
                      me.down("grid").store.getProxy().extraParams = {
                        idRiskEvaluationMatrix: Ext.ComponentQuery.query(
                          "ViewPanelPreviousIdentifyRiskOperational"
                        )[0]
                          .down("grid")
                          .getSelectionModel()
                          .getSelection()[0]
                          .get("idRiskEvaluationMatrix")
                      };
                      me.down("grid").store.getProxy().url =
                        "http://localhost:9000/giro/getRiskByRiskEvaluationMatrix.htm";
                      me.down("grid")
                        .down("pagingtoolbar")
                        .moveFirst();
                    }
                  }
                ]
              },
              {
                xtype: "gridpanel",
                itemId: "gridRisk",
                flex: 2,
                padding: "2 0 2 2",
                title: "RIESGOS",
                titleAlign: "center",
                store: StoreGridPanelRisk,
                columns: [
                  { xtype: "rownumberer", width: 25, sortable: false },
                  { header: "ID", dataIndex: "idRisk", width: 60 },
                  { header: "CODIGO", dataIndex: "codeRisk", width: 60 },
                  { header: "RIESGO", dataIndex: "description", flex: 1 }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: StoreGridPanelRisk,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  items: [
                    {
                      xtype:"UpperCaseTrigger",
                      fieldLabel: "FILTRAR",
                      listeners: {
                        keyup: function(text) {
                          DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(
                            text,
                            text.up("grid")
                          );
                        }
                      },
                      labelWidth: 60,
                      width: 200
                    }
                  ],
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
                  doRefresh: function() {
                    var grid = this,
                      current = grid.store.currentPage;
                    if (
                      grid.fireEvent("beforechange", grid, current) !== false
                    ) {
                      grid.store.loadPage(current);
                      DukeSource.global.DirtyView.loadGridDefault(
                        me.down("#riskDebilityEventRelation")
                      );
                      DukeSource.global.DirtyView.loadGridDefault(me.down("#gridControl"));
                    }
                  }
                },
                listeners: {
                  itemclick: function(view, record) {
                    var grid = me.down("#riskDebilityEventRelation");
                    grid.store.getProxy().extraParams = {
                      idRisk: record.get("idRisk"),
                      versionCorrelative: record.get("versionCorrelative")
                    };
                    grid.store.getProxy().url =
                      "http://localhost:9000/giro/findRiskWeaknessDetail.htm";
                    grid.down("pagingtoolbar").moveFirst();
                    DukeSource.global.DirtyView.loadGridDefault(me.down("#gridControl"));
                  }
                }
              }
            ]
          },
          {
            xtype: "container",
            region: "east",
            flex: 1.1,
            split: true,
            padding: "2 2 2 0",
            layout: {
              type: "vbox",
              align: "stretch"
            },
            items: [
              {
                xtype: "gridpanel",
                flex: 0.8,
                name: "riskDebilityStart",
                itemId: "riskDebilityEventRelation",
                title: "CAUSAS",
                titleAlign: "center",
                store: StoreGridPanelDebilityEventRelation,
                loadMask: true,
                columnLines: true,
                columns: [
                  { xtype: "rownumberer", width: 25, sortable: false },
                  {
                    header: "C&Oacute;DIGO",
                    dataIndex: "codeWeakness",
                    width: 60
                  },
                  {
                    header: "DESCRIPCI&Oacute;N",
                    dataIndex: "description",
                    flex: 2
                  },
                  {
                    header: "FACTOR",
                    align: "center",
                    dataIndex: "nameFactorRisk",
                    width: 80
                  }
                ],

                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: StoreGridPanelDebilityEventRelation,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
                  listeners: {
                    afterrender: function() {
                      this.child("#refresh").hide();
                    }
                  }
                },
                listeners: {
                  render: function() {
                    var grid = this;
                    DukeSource.global.DirtyView.loadGridDefault(grid);
                  },
                  itemclick: function(view, record) {
                    var grid = me.down("#gridControl");
                    grid.store.getProxy().extraParams = {
                      idRiskWeaknessDetail: record.get("idRiskWeaknessDetail"),
                      idRisk: record.get("idRisk"),
                      versionCorrelative: record.get("versionCorrelative")
                    };
                    grid.store.getProxy().url =
                      "http://localhost:9000/giro/findDetailControl.htm";
                    grid.down("pagingtoolbar").moveFirst();
                  }
                }
              },
              {
                xtype: "gridpanel",
                itemId: "gridControl",
                flex: 1,
                padding: "2 0 0 0",
                title: "CONTROLES",
                titleAlign: "center",
                store: StoreGridPanelControl,
                columns: [
                  { xtype: "rownumberer", width: 25, sortable: false },
                  {
                    text: "SEL",
                    dataIndex: "indicatorSave",
                    flex: 1.5,
                    xtype: "checkedcolumn",
                    store: StoreGridPanelControl,
                    sortable: false,
                    hideable: false,
                    menuDisabled: true,
                    stopSelection: false
                  },
                  { header: "CODIGO", dataIndex: "codeControl", width: 60 },
                  {
                    header: "CALIF.",
                    align: "center",
                    dataIndex: "nameQualification",
                    width: 90,
                    renderer: function(value, metaData, record) {
                      if (value != " ") {
                        metaData.tdAttr =
                          'style="background-color: #' +
                          record.get("colorQualification") +
                          ' !important;"';
                        return "<span>" + value + "</span>";
                      }
                    }
                  },
                  //, {header: 'SCORE (%)', dataIndex: 'percentScoreControl', width: 70}
                  {
                    header: "CONTROL",
                    dataIndex: "descriptionControl",
                    width: 400
                  }
                ],
                //dockedItems: [
                //    {
                //        xtype: 'toolbar',
                //        dock: 'bottom',
                //        items: [
                //
                //        ]
                //    }
                //],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: StoreGridPanelControl,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
                  listeners: {
                    afterrender: function() {
                      this.child("#refresh").hide();
                    }
                  }
                },
                listeners: {
                  render: function() {
                    var grid = this;
                    DukeSource.global.DirtyView.loadGridDefault(grid);
                  }
                }
              }
            ]
          }
        ],
        buttonAlign: "center",
        buttons: [
          {
            xtype: "button",
            iconCls: "save",
            scale: "medium",
            text: "GUARDAR",
            handler: function() {
              var gridPanel = Ext.ComponentQuery.query(
                "ViewPanelEventsRiskOperational"
              )[0].down("#gridEvents");
              var recordMain = gridPanel.getSelectionModel().getSelection()[0];

              var gridCause = me.down("#riskDebilityEventRelation");
              var rowCause = gridCause.getSelectionModel().getSelection()[0];

              var rowRisk = me
                .down("#gridRisk")
                .getSelectionModel()
                .getSelection()[0];

              var arrayDataToSave = [];
              var records = me
                .down("#gridControl")
                .getStore()
                .getRange();

              Ext.Array.each(records, function(record, index, countriesItSelf) {
                if (record.get("indicatorSave") == true) {
                  arrayDataToSave.push(record.data);
                }
              });
              if (rowRisk === undefined) {
                DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
              } else {
                DukeSource.lib.Ajax.request({
                  method: "POST",
                  url:
                    "http://localhost:9000/giro/saveRelationEventRisk.htm?nameView=ViewPanelEventsRiskOperational",
                  params: {
                    jsonData: Ext.JSON.encode(arrayDataToSave),
                    idEvent: recordMain.get("idEvent"),
                    idWeakness:
                      rowCause === undefined ? "" : rowCause.get("idWeakness"),
                    idRisk: rowRisk.get("idRisk"),
                    versionRisk: rowRisk.get("versionCorrelative"),
                    idRiskWeaknessDetail:
                      rowCause === undefined
                        ? ""
                        : rowCause.get("idRiskWeaknessDetail")
                  },
                  success: function(response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                      DukeSource.global.DirtyView.messageNormal(response.message);
                      Ext.ComponentQuery.query(
                        "ViewWindowRegisterEventsRisk"
                      )[0]
                        .down("#gridRiskEvent")
                        .getStore()
                        .load();

                      var panelRisk = Ext.ComponentQuery.query(
                        "ViewPanelEvaluationRiskOperational"
                      )[0];

                      if (panelRisk !== undefined) {
                        panelRisk
                          .down("#amountEventLost")
                          .setValue(response.data["amountEventLost"]);
                        panelRisk
                          .down("#numberEventLost")
                          .setValue(response.data["numberEventLost"]);
                      }

                      recordMain.set("indicatorAtRisk", "S");
                      me.close();
                    } else {
                      DukeSource.global.DirtyView.messageWarning(response.message);
                    }
                  },
                  failure: function() {}
                });
              }
            }
          },
          {
            xtype: "button",
            text: "SALIR",
            scale: "medium",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
