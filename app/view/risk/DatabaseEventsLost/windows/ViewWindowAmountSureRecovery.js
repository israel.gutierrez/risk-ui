Ext.define('ModelAmountSureRecovery', {
    extend: 'Ext.data.Model',
    fields: [
        'idEvent',
        'amountRecovery',
        'descriptionShort',
        'descriptionLarge',
        'amountTotal',
        'dateRecovery',
        'nameCurrency',
        'typeRecovery',
        'typeRegister',
        'descriptionTypeRecovery',
        'insuranceCompany',
        'codeAccount',
        'typeChange',
        'code'
    ]
});

var storeAmountSureRecovery = Ext.create('Ext.data.Store', {
    model: 'ModelAmountSureRecovery',
    autoLoad: false,
    proxy: {
        actionMethods: {
            create: 'POST',
            read: 'POST',
            update: 'POST'
        },
        type: 'ajax',
        url: 'http://localhost:9000/giro/loadGridDefault.htm',
        reader: {
            totalProperty: 'totalCount',
            root: 'data',
            successProperty: 'success'
        }
    }
});
Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowAmountSureRecovery', {
    extend: 'Ext.window.Window',
    requires: [
        'DukeSource.view.risk.parameter.combos.ViewComboCurrency'
    ],
    alias: 'widget.ViewWindowAmountSureRecovery',
    height: 450,
    width: 850,
    layout: {
        align: 'stretch',
        type: 'hbox'
    },
    titleAlign: 'center',
    title: 'Recuperación de eventos de pérdida',
    border: false,
    initComponent: function () {
        var me = this;
        var recordMainEvent = this.recordMainEvent;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    flex: 0.8,
                    bodyPadding: 5,
                    fieldDefaults: {
                        labelCls: 'changeSizeFontToEightPt',
                        fieldCls: 'changeSizeFontToEightPt'
                    },
                    layout: 'anchor',
                    items: [
                        {
                            xtype: 'fieldset',
                            title: 'Datos del recupero',
                            items: [
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    value: 'id',
                                    name: 'idRecoveryEvent'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    name: 'idEvent',
                                    itemId: 'idEvent'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    name: 'numberRelation',
                                    itemId: 'numberRelation'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    name: 'typeRegister',
                                    value: DukeSource.global.GiroConstants.TYPE_RECOVERY,
                                    itemId: 'typeRegister'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    name: 'code',
                                    itemId: 'code'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    value: 'S',
                                    name: 'state'
                                },
                                {
                                    xtype: 'datefield',
                                    allowBlank: false,
                                    msgTarget: 'side',
                                    name: 'dateRecovery',
                                    value: new Date(),
                                    itemId: 'dateRecovery',
                                    format: 'd/m/Y',
                                    fieldCls: 'obligatoryTextField',
                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                    fieldLabel: 'Fecha de recupero',
                                    maxValue: new Date(),
                                    listeners: {
                                        expand: function (field, value) {
                                            field.setMinValue(recordMainEvent.get('dateOccurrence'));
                                            field.setMaxValue(new Date());
                                        },
                                        blur: function (field, value) {
                                            field.setMinValue(recordMainEvent.get('dateOccurrence'));
                                            field.setMaxValue(new Date());
                                        }
                                    }
                                },
                                {
                                    xtype: 'combobox',
                                    fieldLabel: 'Tipo de recupero',
                                    msgTarget: 'side',
                                    displayField: 'description',
                                    valueField: 'value',
                                    forceSelection: true,
                                    editable: false,
                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                    allowBlank: false,
                                    name: 'typeRecovery',
                                    itemId: 'typeRecovery',
                                    store: {
                                        fields: ['value', 'description'],
                                        pageSize: 9999,
                                        autoLoad: true,
                                        proxy: {
                                            actionMethods: {
                                                create: 'POST',
                                                read: 'POST',
                                                update: 'POST'
                                            },
                                            type: 'ajax',
                                            url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                            extraParams: {
                                                propertyOrder: 'description',
                                                propertyFind: 'identified',
                                                valueFind: 'TYPERECOVERY'
                                            },
                                            reader: {
                                                type: 'json',
                                                root: 'data',
                                                successProperty: 'success'
                                            }
                                        }
                                    },
                                    listeners: {
                                        specialkey: function (f, e) {
                                            DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#insuranceCompany'))
                                        },
                                        select: function (cbo) {
                                            var sureCompany = me.down('#insuranceCompany');
                                            if (cbo.getValue() === 'A') {
                                                sureCompany.setVisible(true);
                                            } else {
                                                sureCompany.setVisible(false);
                                                me.down('#numberPolicy').setVisible(false);
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'combobox',
                                    fieldLabel: getName('EWInsuranceCompany'),
                                    name: 'insuranceCompany',
                                    itemId: 'insuranceCompany',
                                    msgTarget: 'side',
                                    hidden: true,
                                    enforceMaxLength: true,
                                    displayField: 'description',
                                    valueField: 'idInsuranceCompany',
                                    store: {
                                        fields: ["idInsuranceCompany", "description"],
                                        proxy: {
                                            actionMethods: {
                                                create: "POST",
                                                read: "POST",
                                                update: "POST"
                                            },
                                            type: "ajax",
                                            url: "http://localhost:9000/giro/showListInsuranceCompanyActives.htm",
                                            extraParams: {
                                                propertyOrder: "description"
                                            },
                                            reader: {
                                                type: "json",
                                                root: "data",
                                                successProperty: "success"
                                            }
                                        }
                                    },
                                    listeners: {
                                        specialkey: function (f, e) {
                                            DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#numberPolicy'))
                                        },
                                        select: function (cbo) {
                                            me.down('#numberPolicy').setVisible(true);
                                        }
                                    }
                                },
                                {
                                    xtype: 'UpperCaseTextField',
                                    fieldLabel: 'Nro de poliza',
                                    name: 'numberPolicy',
                                    hidden: true,
                                    itemId: 'numberPolicy'
                                },
                                {
                                    xtype: 'ViewComboCurrency',
                                    fieldLabel: 'Moneda',
                                    name: 'currency',
                                    itemId: 'currency',
                                    fieldCls: 'obligatoryTextField',
                                    allowBlank: false,
                                    listeners: {
                                        specialkey: function (f, e) {
                                            DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#typeChange'));
                                        },
                                        select: function (cbo) {
                                            var typeChange = me.down('#typeChange');
                                            if (cbo.getValue() === '1') {
                                                typeChange.setValue('1.00');
                                                typeChange.setFieldStyle('background: #D8D8D8');
                                                typeChange.setReadOnly(true);
                                            } else {
                                                typeChange.setValue('');
                                                typeChange.setFieldStyle('background: #d9ffdb');
                                                typeChange.setReadOnly(false);
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'NumberDecimalNumberObligatory',
                                    fieldLabel: ' Tipo de cambio',
                                    name: 'typeChange',
                                    fieldCls: 'obligatoryTextField',
                                    itemId: 'typeChange',
                                    maxValue: 9999,
                                    forcePrecision: true,
                                    allowBlank: false,
                                    decimalPrecision: 4,
                                    listeners: {
                                        specialkey: function (f, e) {
                                            if (e.getKey() === e.ENTER || e.getKey() === e.TAB) {
                                                calculateNetLossRecovery(me);
                                                DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#amountRecovery'))
                                            }
                                        },
                                        blur: function (f, e) {
                                            calculateNetLossRecovery(me);
                                        }
                                    }
                                },
                                {
                                    xtype: 'NumberDecimalNumberObligatory',
                                    name: 'amountRecovery',
                                    itemId: 'amountRecovery',
                                    maxLength: 13,
                                    fieldLabel: 'Monto',
                                    listeners: {
                                        specialkey: function (f, e) {
                                            if (e.getKey() === e.ENTER || e.getKey() === e.TAB) {
                                                calculateNetLossRecovery(me);
                                                DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('#typeRecovery'))
                                            }
                                        },
                                        blur: function (f, e) {
                                            calculateNetLossRecovery(me);
                                        }
                                    }
                                },
                                {
                                    xtype: 'NumberDecimalNumberObligatory',
                                    name: 'netRecovery',
                                    itemId: 'netRecovery',
                                    hidden: true,
                                    readOnly: true,
                                    fieldLabel: 'Total recuperado',
                                    fieldCls: 'fieldBlue'
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'accountPlan',
                                    itemId: 'accountPlan',
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Cuenta contable',
                                    emptyText: 'Buscar cuenta',
                                    anchor: '100%',
                                    name: 'codeAccount',
                                    itemId: 'codeAccount',
                                    allowBlank: true,
                                    listeners: {
                                        specialkey: function (f, e) {
                                            if (e.getKey() === e.ENTER) {
                                                var win = Ext.create('DukeSource.view.risk.parameter.windows.ViewWindowSearchPlanAccountCountable', {modal: true}).show();
                                                win.down('UpperCaseTextField').focus(false, 100);
                                                win.down('UpperCaseTextField').setValue(me.down('#codeAccount').getValue());

                                                win.down('grid').on('itemdblclick', function (grid, row) {
                                                    me.down('#codeAccount').setValue(row.get('codeAccount'));
                                                    me.down('#accountPlan').setValue(row.get('idAccountPlan'));
                                                    me.down('UpperCaseTextField[name=bookkeepingRecovery]').focus(false, 100);
                                                    win.close();
                                                });
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'UpperCaseTextField',
                                    fieldLabel: 'Asiento contable',
                                    anchor: '100%',
                                    name: 'bookkeepingRecovery',
                                    itemId: 'bookkeepingRecovery',
                                    maxLength: 50,
                                    listeners: {
                                        specialkey: function (f, e) {
                                            DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('button[text=GUARDAR]'))
                                        }
                                    }
                                },
                                {
                                    xtype: 'UpperCaseTextArea',
                                    height: 30,
                                    anchor: '100%',
                                    fieldLabel: 'Comentarios',
                                    name: 'descriptionLarge',
                                    itemId: 'descriptionLarge',
                                    maxLength: 600
                                }
                            ]
                        }
                    ],
                    tbar: [
                        {
                            text: 'Nuevo',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            iconCls: 'add',
                            handler: function () {
                                var form = me.down('form');
                                var idEvent = me.down('#idEvent').getValue();
                                form.getForm().reset();
                                me.down('#idEvent').setValue(recordMainEvent.get('idEvent'));
                            }
                        },
                        {
                            text: 'Guardar',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            iconCls: 'save',
                            handler: function () {
                                var form = me.down('form');
                                if (form.getForm().isValid()) {
                                    DukeSource.lib.Ajax.request({
                                        method: 'POST',
                                        url: 'http://localhost:9000/giro/saveRecoveryEvent.htm?nameView=ViewPanelEventsRiskOperational',
                                        params: {
                                            jsonData: Ext.JSON.encode(form.getValues())
                                        },
                                        success: function (response) {
                                            var idEvent = me.down('#idEvent').getValue();
                                            var numberRelation = me.down('#numberRelation').getValue();

                                            response = Ext.decode(response.responseText);
                                            if (response.success) {
                                                var grid = me.down('grid');
                                                DukeSource.global.DirtyView.searchPaginationGridNormal(idEvent, grid, grid.down('pagingtoolbar'), 'http://localhost:9000/giro/findRecoveryEvent.htm', 'event.idEvent', 'event.idEvent');
                                                var gridSubEvent = Ext.ComponentQuery.query('ViewWindowRegisterSubEvents grid')[0];
                                                if (gridSubEvent !== undefined) {
                                                    gridSubEvent.store.getProxy().extraParams = {idEvent: numberRelation};
                                                    gridSubEvent.store.getProxy().url = 'http://localhost:9000/giro/getSubEventsByEvent.htm';
                                                    gridSubEvent.down('pagingtoolbar').doRefresh();
                                                }
                                                var gridEvent = Ext.ComponentQuery.query('ViewPanelEventsRiskOperational grid')[0];
                                                gridEvent.down('pagingtoolbar').doRefresh();

                                                DukeSource.global.DirtyView.messageNormal(response.message);
                                                form.getForm().reset();
                                                me.down('#idEvent').setValue(recordMainEvent.get('idEvent'));
                                            } else {
                                                DukeSource.global.DirtyView.messageWarning(response.message);
                                            }
                                        },
                                        failure: function () {
                                        }
                                    });
                                } else {
                                    DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
                                }
                            }
                        }
                    ]
                },
                {
                    xtype: 'gridpanel',
                    tbar: [
                        {
                            text: 'Eliminar',
                            scale: 'medium',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            iconCls: 'delete',
                            handler: function () {
                                var grid = me.down('grid');
                                var form = me.down('form');
                                var record = grid.getSelectionModel().getSelection()[0];
                                if (record !== undefined) {
                                    Ext.MessageBox.show({
                                        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                                        msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
                                        icon: Ext.Msg.QUESTION,
                                        buttonText: {
                                            yes: "Si"
                                        },
                                        buttons: Ext.MessageBox.YESNO,
                                        fn: function (btn) {
                                            if (btn === 'yes') {
                                                DukeSource.lib.Ajax.request({
                                                    method: 'POST',
                                                    url: 'http://localhost:9000/giro/deleteRecoveryEvent.htm?nameView=ViewPanelEventsRiskOperational',
                                                    params: {
                                                        jsonData: Ext.JSON.encode(record.raw)
                                                    },
                                                    success: function (response) {
                                                        var numberRelation = me.down('#numberRelation').getValue();

                                                        response = Ext.decode(response.responseText);

                                                        if (response.success) {
                                                            var gridSubEvent = Ext.ComponentQuery.query('ViewWindowRegisterSubEvents grid')[0];

                                                            if (gridSubEvent !== undefined) {
                                                                gridSubEvent.store.getProxy().extraParams = {idEvent: numberRelation};
                                                                gridSubEvent.store.getProxy().url = 'http://localhost:9000/giro/getSubEventsByEvent.htm';
                                                                gridSubEvent.down('pagingtoolbar').doRefresh();
                                                            }

                                                            var gridEvent = Ext.ComponentQuery.query('ViewPanelEventsRiskOperational grid')[0];
                                                            gridEvent.down('pagingtoolbar').doRefresh();
                                                            me.down('grid').down('pagingtoolbar').doRefresh();
                                                            form.getForm().reset();
                                                            me.down('#idEvent').setValue(recordMainEvent.get('idEvent'));

                                                            DukeSource.global.DirtyView.messageNormal(response.message);
                                                        } else {
                                                            DukeSource.global.DirtyView.messageWarning(response.message);
                                                        }
                                                    },
                                                    failure: function () {
                                                    }
                                                });
                                            }
                                        }
                                    });
                                } else {
                                    DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
                                }
                            }
                        }
                    ],
                    padding: '0 0 0 2',
                    store: storeAmountSureRecovery,
                    flex: 1,
                    columns: [
                        {
                            dataIndex: 'code',
                            align: 'center',
                            hidden: hidden('EVN_WRE_Code'),
                            text: 'ID',
                            width: 95,
                            renderer: function (value) {
                                return '<b>' + value + '</b>';
                            }
                        },
                        {
                            dataIndex: 'descriptionTypeRecovery',
                            align: 'center',
                            text: 'Tipo de recupero',
                            width: 95,
                            renderer: function (value, metaData) {
                                metaData.tdAttr = 'style="background-color: #60ff97 !important;border:1px solid #60ff97 !important;"';
                                return '<div style="background-color:#60ff97;">' + value + '</div>';
                            }
                        },
                        {
                            dataIndex: 'dateRecovery',
                            text: 'Fecha',
                            width: 90,
                            align: 'center'

                        },
                        {
                            dataIndex: 'amountRecovery',
                            width: 100,
                            xtype: 'numbercolumn',
                            align: 'center',
                            format: '0,0.00',
                            text: 'Monto original'
                        },
                        {
                            dataIndex: 'typeChange',
                            width: 60,
                            align: 'center',
                            text: 'Tipo de cambio'
                        },
                        {
                            dataIndex: 'amountTotal',
                            xtype: 'numbercolumn',
                            width: 100,
                            align: 'center',
                            format: '0,0.00',
                            text: 'Monto total'
                        },
                        {
                            header: 'Comentarios',
                            width: 200,
                            align: 'left',
                            dataIndex: 'descriptionLarge'
                        }
                    ],
                    bbar: {
                        xtype: 'pagingtoolbar',
                        pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                        store: storeAmountSureRecovery,
                        displayInfo: true,
                        displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                        emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                    },
                    listeners: {
                        itemclick: function (grid, record) {
                            me.down('#currency').getStore().load();
                            me.down('form').getForm().setValues(record.raw);
                            me.down('#netRecovery').setValue(record.raw['amountTotal']);
                            me.down('#idEvent').setValue(record.raw['idEvent']);
                            if (record.raw['typeRecovery'] === 'A') {
                                me.down('#insuranceCompany').getStore().load();
                                me.down('#insuranceCompany').setDisabled(false);
                                me.down('#numberPolicy').setVisible(true);
                                me.down('#insuranceCompany').setValue(record.raw['insuranceCompany']);
                            } else {
                                me.down('#insuranceCompany').setDisabled(true);
                                me.down('#insuranceCompany').setValue(record.raw['insuranceCompany']);
                            }
                        }
                    }
                }
            ],
            buttons: [
                {
                    text: 'Salir',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});

function calculateNetLossRecovery(me) {
    var typeChange = me.down('#typeChange').getValue();
    var amountOrigin = me.down('#amountRecovery').getValue();
    var result = amountOrigin * typeChange;
    me.down('#netRecovery').setValue(result);
}
