Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeProduct', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowTreeProduct',
    requires: [
        'Ext.tree.Panel',
        'DukeSource.view.risk.DatabaseEventsLost.treepanel.TreePanelProduct',
        'Ext.tree.View'
    ],
    title: 'Producto',
    border: false,
    titleAlign: 'center',
    layout: 'fit',
    modal: true,
    height: 570,
    width: 900,
    initComponent: function () {
        var me = this;
        var valueNode = this.valueNode;
        var winParent = this.winParent;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'TreePanelProduct',
                    listeners: {
                        select: function (view, record) {
                            valueNode = record;
                            if (record.data['depth'] === 1) {
                                me.down('#selectProduct').setDisabled(true);
                            } else {
                                me.down('#selectProduct').setDisabled(false);
                            }
                        },
                        itemdblclick: function (view, record) {
                            if (record.data['depth'] === 1) {
                                DukeSource.global.DirtyView.messageWarning('Producto no permitido, seleccione otro');
                            } else {
                                var description = record.getPath('text', ' &#8702; ');
                                winParent.down('#descriptionProduct').setValue(description.substring(22));
                                winParent.down('#product').setValue(record.data['idProduct']);
                                me.close();
                            }
                        }
                    }

                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    itemId: 'selectProduct',
                    iconCls: 'save',
                    scale: 'medium',
                    handler: function () {
                        if (valueNode !== undefined) {
                            var description = valueNode.getPath('text', ' &#8702; ');
                            winParent.down('#descriptionProduct').setValue(description.substring(22));
                            winParent.down('#product').setValue(valueNode.data['idProduct']);
                            me.close();
                        } else {
                            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_ITEM_CHECK, Ext.Msg.WARNING);
                        }
                    }
                },
                {
                    text: 'Salir',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }
});
