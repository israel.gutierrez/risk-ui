Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterSubEventsDetail",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowRegisterSubEventsDetail",
    requires: [
      "DukeSource.view.risk.parameter.combos.ViewComboRiskType",
      "Ext.form.Panel",
      "Ext.ux.DateTimeField",
      "Ext.form.field.TextArea",
      "Ext.form.field.ComboBox",
      "Ext.form.field.Checkbox",
      "Ext.form.field.Date",
      "Ext.button.Button",
      "Ext.form.field.File"
    ],
    layout: {
      type: "fit"
    },
    title: "Ingreso de sub-eventos",
    border: false,
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      var recordMainEvent = this.recordMainEvent;
      var actionType = this.actionType;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            layout: {
              type: "fit"
            },
            fieldDefaults: {
              labelCls: "changeSizeFontToEightPt",
              fieldCls: "changeSizeFontToEightPt"
            },
            bodyPadding: "0 10 0 10",
            title: "",
            items: [
              {
                xtype: "container",
                layout: {
                  type: "anchor"
                },
                items: [
                  {
                    xtype: "fieldset",
                    padding: 5,
                    title: "<b>EVENTO PRINCIPAL</b>",
                    layout: "hbox",
                    border: 2,
                    style: {
                      borderColor: "#78abf5",
                      background: "#edf6ff"
                    },
                    height: 60,
                    items: [
                      {
                        xtype: "textfield",
                        flex: 3,
                        labelWidth: 125,
                        fieldLabel: "Descripci&oacute;n",
                        name: "descriptionShortMainEvent",
                        itemId: "descriptionShortMainEvent",
                        fieldCls: "readOnlyText",
                        value: recordMainEvent["descriptionShort"],
                        maxLength: 300
                      },
                      {
                        xtype: "textfield",
                        fieldLabel: "C&oacute;digo",
                        padding: "0 0 0 5",
                        value: recordMainEvent["codeEvent"],
                        readOnly: true,
                        labelWidth: 80,
                        flex: 1,
                        fieldCls: "codeIncident",
                        name: "codeMainEvent",
                        itemId: "codeMainEvent"
                      }
                    ]
                  },
                  {
                    xtype: "fieldset",
                    padding: 5,
                    title: "<b>SUB EVENTO</b>",
                    layout: "anchor",
                    items: [
                      {
                        xtype: "container",
                        layout: {
                          type: "hbox",
                          align: "stretch"
                        },
                        height: 35,
                        items: [
                          {
                            xtype: "UpperCaseTextFieldObligatory",
                            flex: 4,
                            labelWidth: 125,
                            fieldLabel: "Descripci&oacute;n corta",
                            name: "descriptionShort",
                            itemId: "descriptionShort",
                            fieldCls: "obligatoryTextField",
                            maxLength: 300,
                            listeners: {
                              specialkey: function(f, e) {
                                if (
                                  e.getKey() === e.ENTER ||
                                  e.getKey() === e.TAB
                                ) {
                                  DukeSource.global.DirtyView.focusEventEnter(
                                    f,
                                    e,
                                    me.down("#descriptionLarge")
                                  );
                                }
                              }
                            }
                          },
                          {
                            xtype: "textfield",
                            fieldLabel: "C&oacute;digo",
                            padding: "0 0 0 5",
                            readOnly:
                              eventCodeAutomatic ===
                              DukeSource.global.GiroConstants.YES,
                            labelWidth: 50,
                            flex: 1,
                            fieldCls: "codeIncident",
                            name: "codeEvent",
                            itemId: "codeEvent",
                            value: DukeSource.global.GiroConstants.DRAFT
                          },
                          {
                            xtype: "button",
                            text: "Aprobar",
                            flex: 0.5,
                            hidden: true,
                            margin: "0 0 0 2",
                            iconCls: "evaluate",
                            name: "btnApproveSubEvent",
                            itemId: "btnApproveSubEvent",
                            cls: "my-btn",
                            overCls: "my-over",
                            handler: function() {
                              var win = Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.WindowApproveCorrelative",
                                {
                                  modal: true,
                                  id: me.down("#idEvent").getValue(),
                                  actionExecute: "approveSubEvent"
                                }
                              );
                              if (
                                me.down("#checkApprove").getValue() ===
                                DukeSource.global.GiroConstants.YES
                              ) {
                                win
                                  .down("#dateApprove")
                                  .setValue(me.down("#dateApprove").getValue());
                                win
                                  .down("#userApprove")
                                  .setValue(me.down("#userApprove").getValue());
                                win
                                  .down("#commentApprove")
                                  .setValue(
                                    me.down("#commentApprove").getValue()
                                  );
                                win.down("#saveApprove").setDisabled(true);
                                win.down("#saveApprove").setVisible(false);
                              }
                              win.show();
                            }
                          }
                        ]
                      },
                      {
                        xtype: 'UpperCaseTextArea',
                        anchor: "100%",
                        labelWidth: 125,
                        padding: "5 0 0 0",
                        height: 40,
                        fieldLabel: "Descripción larga",
                        name: "descriptionLarge",
                        itemId: "descriptionLarge",
                        fieldCls: "obligatoryTextField",
                        allowBlank: false,
                        maxLength: 4000
                      },
                      {
                        xtype: "container",
                        layout: {
                          type: "hbox"
                        },
                        items: [
                          {
                            xtype: "container",
                            flex: 1,
                            layout: {
                              type: "anchor"
                            },
                            items: [
                              {
                                xtype: "textfield",
                                hidden: true,
                                itemId: "codeCorrelative",
                                name: "codeCorrelative"
                              },
                              {
                                xtype: "textfield",
                                hidden: true,
                                itemId: "checkApprove",
                                name: "checkApprove"
                              },
                              {
                                xtype: "textfield",
                                hidden: true,
                                itemId: "userApprove",
                                name: "userApprove"
                              },
                              {
                                xtype: "textfield",
                                hidden: true,
                                itemId: "commentApprove",
                                name: "commentApprove"
                              },
                              {
                                xtype: "textfield",
                                hidden: true,
                                itemId: "dateApprove",
                                name: "dateApprove"
                              },
                              {
                                xtype: "textfield",
                                itemId: "eventMain",
                                name: "eventMain",
                                value: "S",
                                hidden: true
                              },
                              {
                                xtype: "textfield",
                                itemId: "actionType",
                                name: "actionType",
                                value: actionType,
                                hidden: true
                              },
                              {
                                xtype: "textfield",
                                name: "fileAttachment",
                                itemId: "fileAttachment",
                                value: "N",
                                hidden: true
                              },
                              {
                                xtype: "textfield",
                                itemId: "numberRelation",
                                name: "numberRelation",
                                hidden: true
                              },
                              {
                                xtype: "textfield",
                                itemId: "diffNetLoss",
                                name: "diffNetLoss",
                                value: 0,
                                hidden: true
                              },
                              {
                                xtype: "textfield",
                                itemId: "netLossOrigin",
                                name: "netLossOrigin",
                                value: 0,
                                hidden: true
                              },
                              {
                                xtype: "textfield",
                                name: "indicatorAtRisk",
                                itemId: "indicatorAtRisk",
                                hidden: true
                              },
                              {
                                xtype:
                                  DukeSource.view.risk.util
                                    .UpperCaseTextFieldReadOnly,
                                name: "codesRiskAssociated",
                                itemId: "codesRiskAssociated",
                                hidden: true
                              },
                              {
                                xtype: "textfield",
                                name: "numberRiskAssociated",
                                itemId: "numberRiskAssociated",
                                value: "0",
                                hidden: true
                              },
                              {
                                xtype: "datefield",
                                format: "d/m/Y",
                                width: 240,
                                labelWidth: 125,
                                disabled: true,
                                hidden: true,
                                name: "dateProcess",
                                itemId: "dateProcess"
                              },
                              {
                                xtype: "textfield",
                                hidden: true,
                                name: "idIncident",
                                itemId: "idIncident"
                              },
                              {
                                xtype: "textfield",
                                name: "numberActionPlan",
                                itemId: "numberActionPlan",
                                value: "0",
                                hidden: true
                              },
                              {
                                xtype: "textfield",
                                name: "codesActionPlan",
                                itemId: "codesActionPlan",
                                hidden: true
                              },
                              {
                                xtype: "textfield",
                                hidden: true,
                                name: "idDetailIncidents"
                              },
                              {
                                xtype: "textfield",
                                hidden: true,
                                value: "id",
                                itemId: "idEvent",
                                name: "idEvent"
                              },
                              {
                                xtype: "textfield",
                                hidden: true,
                                value: "0",
                                itemId: "year",
                                name: "year"
                              },
                              {
                                xtype: "textfield",
                                hidden: true,
                                name: "sequenceEventState",
                                itemId: "sequenceEventState"
                              },
                              {
                                xtype: "textfield",
                                hidden: true,
                                itemId: "product",
                                name: "product"
                              },
                              {
                                xtype: "textfield",
                                hidden: true,
                                itemId: "processRecursive",
                                name: "processRecursive"
                              },
                              {
                                xtype: "textfield",
                                hidden: true,
                                itemId: "unity",
                                name: "unity"
                              },
                              {
                                xtype: "textfield",
                                hidden: true,
                                itemId: "costCenter",
                                name: "costCenter"
                              },
                              {
                                xtype: "textfield",
                                hidden: true,
                                itemId: "factorRisk",
                                name: "factorRisk"
                              },
                              {
                                xtype: "textfield",
                                hidden: true,
                                itemId: "operation",
                                name: "operation"
                              },
                              {
                                xtype: "container",
                                anchor: "100%",
                                height: 27,
                                layout: {
                                  type: "hbox"
                                },
                                items: [
                                  {
                                    xtype: "combobox",
                                    flex: 1,
                                    labelWidth: 125,
                                    fieldLabel: getName(
                                      "EVN_WER_CBX_EventType"
                                    ),
                                    msgTarget: "side",
                                    fieldCls: "obligatoryTextField",
                                    displayField: "description",
                                    valueField: "idTypeEvent",

                                    forceSelection: true,
                                    editable: false,
                                    blankText:
                                      DukeSource.global.GiroMessages
                                        .MESSAGE_REQUIRE,
                                    allowBlank: false,
                                    name: "eventType",
                                    itemId: "eventType",
                                    store: {
                                      fields: [
                                        "idTypeEvent",
                                        "description",
                                        "state"
                                      ],
                                      data: [
                                        {
                                          idTypeEvent: "1",
                                          description: "Simple",
                                          state: "S"
                                        }
                                      ]
                                    }
                                  },
                                  {
                                    xtype: "combobox",
                                    flex: 0.8,
                                    labelWidth: 50,
                                    padding: "0 0 0 5",
                                    fieldLabel: "Estado",
                                    editable: false,
                                    allowBlank: false,
                                    name: "eventState",
                                    itemId: "eventState",
                                    fieldCls: "obligatoryTextField",
                                    displayField: "description",
                                    valueField: "id",
                                    store: {
                                      fields: ["id", "description", "sequence"],
                                      proxy: {
                                        actionMethods: {
                                          create: "POST",
                                          read: "POST",
                                          update: "POST"
                                        },
                                        type: "ajax",
                                        url:
                                          "http://localhost:9000/giro/findStateIncident.htm",
                                        extraParams: {
                                          propertyFind: "si.typeIncident",
                                          valueFind:
                                            DukeSource.global.GiroConstants
                                              .EVENT,
                                          propertyOrder: "si.sequence"
                                        },
                                        reader: {
                                          type: "json",
                                          root: "data",
                                          successProperty: "success"
                                        }
                                      }
                                    },
                                    listeners: {
                                      select: function(cbo) {
                                        var record = cbo.findRecord(
                                          cbo.valueField || cbo.displayField,
                                          cbo.getValue()
                                        );
                                        var index = cbo.store.indexOf(record);
                                        me.down("#sequenceEventState").setValue(
                                          cbo.store.data.items[index].data
                                            .sequence
                                        );
                                      }
                                    }
                                  }
                                ]
                              },
                              {
                                xtype:
                                  'UpperCaseTextArea',
                                anchor: "100%",
                                labelWidth: 125,
                                height: 40,
                                hidden: hidden("EWDescriptionStateEvent"),
                                allowBlank: blank("EWDescriptionStateEvent"),
                                fieldCls: styleField("EWDescriptionStateEvent"),
                                fieldLabel: "Detalle del estado",
                                name: "descriptionStateEvent",
                                itemId: "descriptionStateEvent",
                                maxLength: 2000
                              },
                              {
                                xtype: "combobox",
                                anchor: "100%",
                                labelWidth: 125,
                                fieldLabel: getName("EVN_WER_CBX_EventClass"),
                                editable: false,
                                allowBlank: false,
                                forceSelection: true,
                                hidden: hidden("EVN_WER_CBX_EventClass"),
                                name: "eventClass",
                                itemId: "eventClass",
                                fieldCls: "obligatoryTextField",
                                queryMode: "local",
                                displayField: "description",
                                valueField: "value",
                                store: {
                                  fields: ["value", "description"],
                                  pageSize: 9999,
                                  autoLoad: true,
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                                    extraParams: {
                                      propertyOrder: "description",
                                      propertyFind: "identified",
                                      valueFind: "CLASSEVENT"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                }
                              },
                              {
                                xtype: "combobox",
                                allowBlank: false,
                                forceSelection: true,
                                msgTarget: "side",
                                fieldCls: "obligatoryTextField",
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                anchor: "100%",
                                fieldLabel: "Agencia",
                                plugins: ["ComboSelectCount"],
                                name: "agency",
                                itemId: "agency",
                                flex: 1,
                                labelWidth: 125,
                                displayField: "description",
                                valueField: "idAgency",
                                store: {
                                  fields: ["idAgency", "description"],
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "http://localhost:9000/giro/showListAgencyActivesComboBox.htm",
                                    extraParams: {
                                      propertyOrder: "a.description"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                }
                              },
                              {
                                xtype: "combobox",
                                queryMode: "remote",
                                anchor: "100%",
                                labelWidth: 125,
                                fieldLabel: "Proceso",
                                plugins: ["ComboSelectCount"],
                                name: "process",
                                itemId: "process",
                                msgTarget: "side",
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                allowBlank: blank("EWProcess"),
                                fieldCls: styleField("EWProcess"),
                                forceSelection: true,
                                displayField: "description",
                                valueField: "idProcess",
                                store: {
                                  fields: ["idProcess", "description"],
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "http://localhost:9000/giro/showListProcessActivesComboBox.htm",
                                    extraParams: {
                                      propertyOrder: "description",
                                      start: 0,
                                      limit: 9999
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                },
                                listeners: {
                                  select: function(cbo) {
                                    var panel = cbo.up("window");
                                    var comboSubProcess = panel.down(
                                      "#idSubProcess"
                                    );
                                    comboSubProcess.getStore().load({
                                      url:
                                        "http://localhost:9000/giro/showListSubProcessActives.htm",
                                      params: {
                                        valueFind: cbo.getValue()
                                      },
                                      callback: function(cbo) {
                                        comboSubProcess.reset();
                                      }
                                    });
                                    comboSubProcess.setDisabled(false);
                                  }
                                }
                              },
                              {
                                xtype: "combobox",
                                disabled: true,
                                editable: false,
                                anchor: "100%",
                                labelWidth: 125,
                                fieldLabel: "SubProceso",
                                msgTarget: "side",
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                allowBlank: blank("EWSubProcess"),
                                fieldCls: styleField("EWSubProcess"),
                                forceSelection: true,
                                queryMode: "local",
                                displayField: "description",
                                valueField: "idSubProcess",
                                name: "idSubProcess",
                                itemId: "idSubProcess",
                                store: {
                                  fields: ["idSubProcess", "description"],
                                  autoLoad: false,
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "data/http://localhost:9000/giro/loadGridDefault.htm",
                                    extraParams: {
                                      propertyOrder: "description"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                },
                                listeners: {
                                  specialkey: function(f, e) {
                                    DukeSource.global.DirtyView.focusEventEnter(
                                      f,
                                      e,
                                      me.down("#idBusinessLineOne")
                                    );
                                  }
                                }
                              },
                              {
                                xtype: "combobox",
                                queryMode: "remote",
                                anchor: "100%",
                                labelWidth: 125,
                                fieldLabel: "Proceso afectado",
                                plugins: ["ComboSelectCount"],
                                name: "processImpact",
                                itemId: "processImpact",
                                msgTarget: "side",
                                allowBlank: blank("EWProcessImpact"),
                                fieldCls: styleField("EWProcessImpact"),
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                forceSelection: true,
                                displayField: "description",
                                valueField: "idProcess",
                                store: {
                                  fields: ["idProcess", "description"],
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "http://localhost:9000/giro/showListProcessActivesComboBox.htm",
                                    extraParams: {
                                      propertyOrder: "description",
                                      start: 0,
                                      limit: 9999
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                },
                                listeners: {
                                  select: function(cbo) {
                                    var panel = cbo.up("window");
                                    var comboSubProcess = panel.down(
                                      "#subProcessImpact"
                                    );
                                    comboSubProcess.getStore().load({
                                      url:
                                        "http://localhost:9000/giro/showListSubProcessActives.htm",
                                      params: {
                                        valueFind: cbo.getValue()
                                      },
                                      callback: function(cbo) {
                                        comboSubProcess.reset();
                                      }
                                    });
                                    comboSubProcess.setDisabled(false);
                                  }
                                }
                              },
                              {
                                xtype: "combobox",
                                disabled: true,
                                editable: false,
                                anchor: "100%",
                                labelWidth: 125,
                                fieldLabel: "Subproceso afectado",
                                msgTarget: "side",
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                allowBlank: blank("EWSubProcessImpact"),
                                fieldCls: styleField("EWSubProcessImpact"),
                                forceSelection: true,
                                queryMode: "local",
                                displayField: "description",
                                valueField: "idSubProcess",
                                name: "subProcessImpact",
                                itemId: "subProcessImpact",
                                store: {
                                  fields: ["idSubProcess", "description"],
                                  autoLoad: false,
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "data/http://localhost:9000/giro/loadGridDefault.htm",
                                    extraParams: {
                                      propertyOrder: "description"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                },
                                listeners: {
                                  specialkey: function(f, e) {
                                    DukeSource.global.DirtyView.focusEventEnter(
                                      f,
                                      e,
                                      me.down("#idBusinessLineOne")
                                    );
                                  }
                                }
                              },
                              {
                                xtype: "displayfield",
                                anchor: "100%",
                                fieldLabel: "Proceso asfi",
                                itemId: "descriptionProcessRecursive",
                                name: "descriptionProcessRecursive",
                                hidden: hidden("EWDescriptionProcessRecursive"),
                                labelWidth: 125,
                                value: "(Seleccionar)",
                                fieldCls: "style-for-url",
                                listeners: {
                                  afterrender: function(view) {
                                    view.getEl().on("click", function() {
                                      Ext.create(
                                        "DukeSource.view.risk.DatabaseEventsLost.windows.WindowTreeProcessRecursive",
                                        {
                                          winParent: me
                                        }
                                      ).show();
                                    });
                                  }
                                }
                              },
                              {
                                xtype: "displayfield",
                                anchor: "100%",
                                fieldLabel: getName("EWCBUnity"),
                                itemId: "descriptionUnity",
                                name: "descriptionUnity",
                                padding: "4 0 4 0",
                                labelWidth: 125,
                                value: "(Seleccionar)",
                                fieldCls: "style-for-url",
                                listeners: {
                                  afterrender: function(view) {
                                    view.getEl().on("click", function() {
                                      Ext.create(
                                        "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeOrganization",
                                        {
                                          backWindow: me,
                                          descriptionUnity: "descriptionUnity",
                                          unity: "unity",
                                          costCenter: "costCenter"
                                        }
                                      ).show();
                                    });
                                  }
                                }
                              },
                              {
                                xtype: "displayfield",
                                anchor: "100%",
                                padding: "4 0 4 0",
                                fieldLabel: "Factor de riesgo",
                                itemId: "descriptionFactorRisk",
                                name: "descriptionFactorRisk",
                                labelWidth: 125,
                                value: "(Seleccionar)",
                                fieldCls: "style-for-url",
                                listeners: {
                                  afterrender: function(view) {
                                    view.getEl().on("click", function() {
                                      Ext.create(
                                        "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeFactorRisk",
                                        {
                                          winParent: me
                                        }
                                      ).show();
                                    });
                                  }
                                }
                              },
                              {
                                xtype: "combobox",
                                anchor: "100%",
                                labelWidth: 125,
                                editable: false,
                                fieldLabel: getName("EWCBIdBusinessLineOne"),
                                hidden: hidden("EWCBIdBusinessLineOne"),
                                name: "businessLineOne",
                                itemId: "businessLineOne",
                                msgTarget: "side",
                                fieldCls: styleField("EWCBIdBusinessLineOne"),
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                allowBlank: blank("EWCBIdBusinessLineOne"),
                                displayField: "description",
                                valueField: "idBusinessLineOne",

                                forceSelection: true,
                                store: {
                                  fields: ["idBusinessLineOne", "description"],
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "http://localhost:9000/giro/showListBusinessLineOneActives.htm",
                                    extraParams: {
                                      propertyOrder: "description"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                },
                                listeners: {
                                  select: function(cbo) {
                                    var panel = cbo.up("window");
                                    var comboBusineeLineTwo = panel.down(
                                      "#businessLineTwo"
                                    );
                                    comboBusineeLineTwo.getStore().load({
                                      url:
                                        "http://localhost:9000/giro/showListBusinessLineTwoActivesComboBox.htm",
                                      params: {
                                        valueFind: cbo.getValue()
                                      },
                                      callback: function(cbo) {
                                        comboBusineeLineTwo.reset();
                                      }
                                    });
                                    comboBusineeLineTwo.setDisabled(false);
                                    panel
                                      .down("#businessLineThree")
                                      .setDisabled(true);
                                    panel.down("#businessLineThree").reset();
                                  }
                                }
                              },
                              {
                                xtype: "combobox",
                                anchor: "100%",
                                disabled: true,
                                editable: false,
                                labelWidth: 125,
                                fieldLabel: getName("EWCBIdBusinessLineTwo"),
                                hidden: hidden("EWCBIdBusinessLineTwo"),
                                msgTarget: "side",
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                allowBlank: blank("EWCBIdBusinessLineTwo"),
                                fieldCls: styleField("EWCBIdBusinessLineTwo"),
                                name: "businessLineTwo",
                                itemId: "businessLineTwo",
                                queryMode: "local",
                                displayField: "description",
                                valueField: "idBusinessLineTwo",

                                forceSelection: true,
                                store: {
                                  fields: ["idBusinessLineTwo", "description"],
                                  autoLoad: false,
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "data/http://localhost:9000/giro/loadGridDefault.htm",
                                    extraParams: {
                                      propertyOrder: "description"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                },
                                listeners: {
                                  select: function(cbo) {
                                    var panel = cbo.up("window");
                                    var comboBusinessLineThree = panel.down(
                                      "#businessLineThree"
                                    );
                                    var comboBusinessLineOne = panel.down(
                                      "#businessLineOne"
                                    );
                                    comboBusinessLineThree.getStore().load({
                                      url:
                                        "http://localhost:9000/giro/showListBusinessLineThreeActivesComboBox.htm",
                                      params: {
                                        idBusinessLineOne: comboBusinessLineOne.getValue(),
                                        idBusinessLineTwo: cbo.getValue()
                                      },
                                      callback: function(cbo) {
                                        comboBusinessLineThree.reset();
                                      }
                                    });
                                    comboBusinessLineThree.setDisabled(false);
                                  }
                                }
                              },
                              {
                                xtype: "combobox",
                                anchor: "100%",
                                fieldLabel: getName("EWCBBusinessLineThree"),
                                fieldCls: styleField("EWCBBusinessLineThree"),
                                allowBlank: blank("EWCBBusinessLineThree"),
                                hidden: hidden("EWCBBusinessLineThree"),
                                labelWidth: 125,
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                msgTarget: "side",
                                editable: false,
                                disabled: true,
                                name: "businessLineThree",
                                itemId: "businessLineThree",
                                queryMode: "local",
                                displayField: "description",
                                valueField: "idBusinessLineThree",

                                forceSelection: true,
                                store: {
                                  fields: [
                                    "idBusinessLineThree",
                                    "description"
                                  ],
                                  autoLoad: false,
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "data/http://localhost:9000/giro/loadGridDefault.htm",
                                    extraParams: {
                                      propertyOrder: "description"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                }
                              },
                              {
                                xtype: "displayfield",
                                anchor: "100%",
                                fieldLabel: "Producto afectado",
                                itemId: "descriptionProduct",
                                name: "descriptionProduct",
                                labelWidth: 125,
                                padding: "4 0 4 0",
                                allowBlank: false,
                                value: "(Seleccionar)",
                                fieldCls: "style-for-url",
                                listeners: {
                                  afterrender: function(view) {
                                    view.getEl().on("click", function() {
                                      Ext.create(
                                        "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeProduct",
                                        {
                                          winParent: me
                                        }
                                      ).show();
                                    });
                                  }
                                }
                              },
                              {
                                xtype: "displayfield",
                                anchor: "100%",
                                fieldLabel: "Operacion",
                                hidden: hidden("EWDescriptionOperation"),
                                itemId: "descriptionOperation",
                                name: "descriptionOperation",
                                labelWidth: 125,
                                value: "(Seleccionar)",
                                fieldCls: "style-for-url",
                                listeners: {
                                  afterrender: function(view) {
                                    view.getEl().on("click", function() {
                                      Ext.create(
                                        "DukeSource.view.risk.DatabaseEventsLost.windows.WindowTreeOperation",
                                        {
                                          winParent: me
                                        }
                                      ).show();
                                    });
                                  }
                                }
                              },
                              {
                                xtype: "combobox",
                                anchor: "100%",
                                labelWidth: 125,
                                fieldLabel: getName("EWCBEventOne"),
                                msgTarget: "side",
                                fieldCls: "obligatoryTextField",
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                allowBlank: false,
                                editable: false,
                                name: "eventOne",
                                itemId: "eventOne",
                                displayField: "description",
                                valueField: "idEventOne",

                                forceSelection: true,
                                store: {
                                  fields: ["idEventOne", "description"],
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "http://localhost:9000/giro/showListEventOneActives.htm",
                                    extraParams: {
                                      propertyOrder: "description"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                },
                                listeners: {
                                  select: function(cbo) {
                                    var panel = cbo.up("window");
                                    var comboTwo = panel.down("#eventTwo");
                                    comboTwo.getStore().load({
                                      url:
                                        "http://localhost:9000/giro/showListEventTwoActivesComboBox.htm",
                                      params: {
                                        valueFind: cbo.getValue()
                                      },
                                      callback: function(cbo) {
                                        comboTwo.reset();
                                      }
                                    });
                                    comboTwo.setDisabled(false);
                                    panel.down("#eventThree").setDisabled(true);
                                    panel.down("#eventThree").reset();
                                  }
                                }
                              },
                              {
                                xtype: "combobox",
                                anchor: "100%",
                                disabled: true,
                                labelWidth: 125,
                                fieldLabel: getName("EWCBEventTwo"),
                                msgTarget: "side",
                                fieldCls: "obligatoryTextField",
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                allowBlank: false,
                                editable: false,
                                name: "eventTwo",
                                itemId: "eventTwo",
                                queryMode: "local",
                                displayField: "description",
                                valueField: "idEventTwo",

                                forceSelection: true,
                                store: {
                                  fields: ["idEventTwo", "description"],
                                  autoLoad: false,
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "data/http://localhost:9000/giro/loadGridDefault.htm",
                                    extraParams: {
                                      propertyOrder: "description"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                },
                                listeners: {
                                  select: function(cbo) {
                                    var panel = cbo.up("window");
                                    var comboTree = panel.down("#eventThree");
                                    comboTree.getStore().load({
                                      url:
                                        "http://localhost:9000/giro/showListEventThreeActivesComboBox.htm",
                                      params: {
                                        valueFind: panel
                                          .down("#eventOne")
                                          .getValue(),
                                        valueFind2: cbo.getValue()
                                      },
                                      callback: function() {
                                        comboTree.reset();
                                      }
                                    });
                                    comboTree.setDisabled(false);
                                  }
                                }
                              },
                              {
                                xtype: "combobox",
                                anchor: "100%",
                                disabled: true,
                                labelWidth: 125,
                                fieldLabel: getName("EWCBEventThree"),
                                hidden: hidden("EWCBEventThree"),
                                msgTarget: "side",
                                fieldCls: styleField("EWCBEventThree"),
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                allowBlank: blank("EWCBEventThree"),
                                editable: false,
                                name: "eventThree",
                                itemId: "eventThree",
                                queryMode: "local",
                                displayField: "description",
                                valueField: "idEventThree",

                                forceSelection: true,
                                store: {
                                  fields: [
                                    "idEventThree",
                                    "description",
                                    "codeEntitySupervisor"
                                  ],
                                  autoLoad: false,
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "data/http://localhost:9000/giro/loadGridDefault.htm",
                                    extraParams: {
                                      propertyOrder: "description"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                },
                                listeners: {
                                  select: function(cbo) {
                                    var record = cbo.findRecordByValue(
                                      cbo.getValue()
                                    );
                                    var code =
                                      record.data["codeEntitySupervisor"];
                                    var isOther = code.substring(
                                      code.length - 2,
                                      code.length
                                    );
                                    if (isOther === "99") {
                                      me.down(
                                        "#descriptionTypeEvent"
                                      ).setVisible(true);
                                    } else {
                                      me.down(
                                        "#descriptionTypeEvent"
                                      ).setVisible(false);
                                    }
                                  },
                                  render: function(cbo) {
                                    if (cbo.getValue() !== null) {
                                      var record = cbo.findRecordByValue(
                                        cbo.getValue()
                                      );
                                      var code =
                                        record.data["codeEntitySupervisor"];
                                      var isOther = code.substring(
                                        code.length - 2,
                                        code.length
                                      );
                                      if (isOther === "99") {
                                        me.down(
                                          "#descriptionTypeEvent"
                                        ).setVisible(true);
                                      } else {
                                        me.down(
                                          "#descriptionTypeEvent"
                                        ).setVisible(false);
                                      }
                                    }
                                  }
                                }
                              },
                              {
                                xtype:
                                  'UpperCaseTextArea',
                                anchor: "100%",
                                labelWidth: 125,
                                height: 40,
                                hidden: true,
                                fieldLabel: "Detalle del evento",
                                name: "descriptionTypeEvent",
                                itemId: "descriptionTypeEvent",
                                maxLength: 2000
                              },
                              {
                                xtype: "container",
                                height: 26,
                                layout: "hbox",
                                items: [
                                  {
                                    xtype: "combobox",
                                    flex: 1.5,
                                    labelWidth: 125,
                                    fieldLabel: getName("EWCBChannelAttention"),
                                    hidden: hidden("EWCBChannelAttention"),
                                    editable: false,
                                    name: "channelAttention",
                                    itemId: "channelAttention",
                                    queryMode: "local",
                                    displayField: "description",
                                    valueField: "value",
                                    store: {
                                      fields: ["value", "description"],
                                      pageSize: 9999,
                                      autoLoad: true,
                                      proxy: {
                                        actionMethods: {
                                          create: "POST",
                                          read: "POST",
                                          update: "POST"
                                        },
                                        type: "ajax",
                                        url:
                                          "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                                        extraParams: {
                                          propertyOrder: "description",
                                          propertyFind: "identified",
                                          valueFind: "CHANNELATTENTION"
                                        },
                                        reader: {
                                          type: "json",
                                          root: "data",
                                          successProperty: "success"
                                        }
                                      }
                                    }
                                  },
                                  {
                                    xtype: "checkboxfield",
                                    flex: 0.5,
                                    hidden: hidden("EWCBCriminal"),
                                    labelWidth: 60,
                                    name: "isSteal",
                                    itemId: "isSteal",
                                    padding: "0 0 0 10",
                                    fieldLabel: getName("EWCBCriminal"),
                                    inputValue: "S",
                                    uncheckedValue: "N",
                                    value: "N"
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            xtype: "container",
                            flex: 1.2,
                            anchor: "100%",
                            padding: "0 0 0 10",
                            layout: {
                              type: "anchor"
                            },
                            items: [
                              {
                                xtype: "checkboxfield",
                                hidden: hidden("EWIsCritic"),
                                fieldLabel: getName("EWIsCritic"),
                                allowBlank: blank("EWIsCritic"),
                                labelWidth: 120,
                                name: "isCritic",
                                itemId: "isCritic",
                                inputValue: "S",
                                uncheckedValue: "N",
                                value: "N",
                                listeners: {
                                  change: function(cbo) {
                                    if (cbo.getValue() === true) {
                                      me.down(
                                        "#descriptionCriticEvent"
                                      ).setVisible(true);
                                    } else {
                                      me.down(
                                        "#descriptionCriticEvent"
                                      ).setVisible(false);
                                    }
                                  }
                                }
                              },
                              {
                                xtype:
                                  'UpperCaseTextArea',
                                anchor: "100%",
                                labelWidth: 120,
                                height: 40,
                                hidden: true,
                                fieldLabel: "Detalle ev.critico",
                                name: "descriptionCriticEvent",
                                itemId: "descriptionCriticEvent",
                                maxLength: 2000
                              },
                              {
                                xtype: "combobox",
                                anchor: "100%",
                                labelWidth: 120,
                                forceSelection: true,
                                fieldLabel: getName("EWRecoveryFixedAsset"),
                                hidden: hidden("EWRecoveryFixedAsset"),
                                allowBlank: blank("EWRecoveryFixedAsset"),
                                fieldCls: styleField("EWRecoveryFixedAsset"),
                                editable: false,
                                name: "recoveryFixedAsset",
                                itemId: "recoveryFixedAsset",
                                queryMode: "local",
                                displayField: "description",
                                valueField: "value",
                                store: {
                                  fields: ["value", "description"],
                                  pageSize: 9999,
                                  autoLoad: true,
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                                    extraParams: {
                                      propertyOrder: "description",
                                      propertyFind: "identified",
                                      valueFind: "RECOVERY_ACTIVE"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                }
                              },
                              {
                                xtype: "combobox",
                                anchor: "100%",
                                labelWidth: 120,
                                fieldLabel: getName("EWCBEffectOtherRisk"),
                                hidden: hidden("EWCBEffectOtherRisk"),
                                allowBlank: blank("EWEffectOtherRisk"),
                                fieldCls: styleField("EWEffectOtherRisk"),
                                msgTarget: "side",
                                displayField: "description",
                                valueField: "value",

                                editable: false,
                                name: "effectOtherRisk",
                                itemId: "effectOtherRisk",
                                store: {
                                  fields: ["value", "description"],
                                  pageSize: 9999,
                                  autoLoad: true,
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url:
                                      "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                                    extraParams: {
                                      propertyOrder: "description",
                                      propertyFind: "identified",
                                      valueFind: "EFFECT_OTHER_RISK"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                }
                              },
                              {
                                xtype: "container",
                                itemId: "containerUserReport",
                                height: 26,
                                layout: {
                                  type: "hbox"
                                },
                                items: [
                                  {
                                    xtype: "textfield",
                                    name: "userReport",
                                    itemId: "userReport",
                                    hidden: true
                                  },
                                  {
                                    xtype:
                                      DukeSource.view.risk.util
                                        .UpperCaseTextFieldReadOnly,
                                    flex: 1.3,
                                    allowBlank: blank("EWFullNameUserReport"),
                                    fieldCls: styleField(
                                      "EWFullNameUserReport"
                                    ),
                                    labelWidth: 120,
                                    name: "fullNameUserReport",
                                    itemId: "fullNameUserReport",
                                    fieldLabel: "Usuario que reporta"
                                  },
                                  {
                                    xtype: "button",
                                    iconCls: "search",
                                    handler: function() {
                                      var win = Ext.create(
                                        "DukeSource.view.risk.util.search.SearchUser",
                                        {
                                          modal: true
                                        }
                                      ).show();
                                      win
                                        .down("grid")
                                        .on("itemdblclick", function(
                                          grid,
                                          record,
                                          item,
                                          index,
                                          e
                                        ) {
                                          me.down(
                                            "textfield[name=userReport]"
                                          ).setValue(record.get("userName"));
                                          me.down(
                                            "UpperCaseTextFieldReadOnly[name=fullNameUserReport]"
                                          ).setValue(record.get("fullName"));
                                          win.close();
                                        });
                                    }
                                  }
                                ]
                              },
                              {
                                xtype:
                                 "UpperCaseTextField",
                                hidden: hidden("EWCPersonImplicated"),
                                anchor: "100%",
                                labelWidth: 120,
                                name: "executiveInvolved",
                                fieldLabel: getName("EWCPersonImplicated"),
                                maxLength: 300
                              },
                              {
                                xtype:
                                 "UpperCaseTextField",
                                hidden: hidden("EWTFPersonAffected"),
                                fieldLabel: getName("EWPersonAffected"),
                                anchor: "100%",
                                labelWidth: 120,
                                maxLength: 300,
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                name: "personAffected",
                                listeners: {
                                  specialkey: function(f, e) {
                                    DukeSource.global.DirtyView.focusEventEnter(
                                      f,
                                      e,
                                      me.down("#descriptionShort")
                                    );
                                  }
                                }
                              },
                              {
                                xtype: "container",
                                hidden: hidden("EWCPersonAuthorized"),
                                height: 26,
                                layout: "hbox",
                                items: [
                                  {
                                    xtype: "textfield",
                                    name: "userAuthorized",
                                    itemId: "userAuthorized",
                                    hidden: true
                                  },
                                  {
                                    xtype:
                                      DukeSource.view.risk.util
                                        .UpperCaseTextFieldReadOnly,
                                    flex: 1,
                                    labelWidth: 120,
                                    name: "fullNameAuthorized",
                                    itemId: "fullNameAuthorized",
                                    fieldLabel: "Autorizado por"
                                  },
                                  {
                                    xtype: "button",
                                    iconCls: "search",
                                    handler: function() {
                                      var win = Ext.create(
                                        "DukeSource.view.risk.util.search.SearchUser",
                                        {
                                          modal: true
                                        }
                                      ).show();
                                      win
                                        .down("grid")
                                        .on("itemdblclick", function(
                                          grid,
                                          record,
                                          item,
                                          index,
                                          e
                                        ) {
                                          me.down("#userAuthorized").setValue(
                                            record.get("userName")
                                          );
                                          me.down(
                                            "#fullNameAuthorized"
                                          ).setValue(record.get("fullName"));
                                          win.close();
                                        });
                                    }
                                  }
                                ]
                              },
                              {
                                xtype: "container",
                                height: 26,
                                layout: "hbox",
                                items: [
                                  {
                                    xtype: "ViewComboRiskType",
                                    fieldLabel: "Riesgo relacionado",
                                    labelStyle: "font-size:8pt;",
                                    flex: 1,
                                    forceSelection: true,
                                    labelWidth: 120,
                                    allowBlank: blank("EWRiskType"),
                                    fieldCls: styleField("EWRiskType"),
                                    msgTarget: "side",
                                    blankText:
                                      DukeSource.global.GiroMessages
                                        .MESSAGE_REQUIRE,
                                    name: "riskType",
                                    itemId: "riskType",
                                    listeners: {
                                      specialkey: function(f, e) {
                                        DukeSource.global.DirtyView.focusEventEnter(
                                          f,
                                          e,
                                          me.down("#descriptionShort")
                                        );
                                      }
                                    }
                                  },
                                  {
                                    xtype: "combobox",
                                    flex: 1,
                                    padding: "0 0 0 5",
                                    fieldLabel: "Tipo de p&eacute;rdida",
                                    editable: false,
                                    forceSelection: true,
                                    allowBlank: blank("EWTypeEventLost"),
                                    fieldCls: styleField("EWTypeEventLost"),
                                    hidden: hidden("EWTypeEventLost"),
                                    name: "typeEventLost",
                                    itemId: "typeEventLost",
                                    queryMode: "local",
                                    displayField: "description",
                                    valueField: "value",
                                    store: {
                                      fields: ["value", "description"],
                                      pageSize: 9999,
                                      autoLoad: true,
                                      proxy: {
                                        actionMethods: {
                                          create: "POST",
                                          read: "POST",
                                          update: "POST"
                                        },
                                        type: "ajax",
                                        url:
                                          "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                                        extraParams: {
                                          propertyOrder: "description",
                                          propertyFind: "identified",
                                          valueFind: "EVENT_LOSS_TYPE"
                                        },
                                        reader: {
                                          type: "json",
                                          root: "data",
                                          successProperty: "success"
                                        }
                                      }
                                    }
                                  }
                                ]
                              },
                              {
                                xtype: "container",
                                layout: {
                                  type: "hbox",
                                  align: "stretch"
                                },
                                items: [
                                  {
                                    xtype: "container",
                                    flex: 1,
                                    layout: "anchor",
                                    items: [
                                      {
                                        xtype: "combobox",
                                        padding: "0 5 0 0",
                                        labelWidth: 120,
                                        anchor: "100%",
                                        fieldLabel: "Moneda",
                                        msgTarget: "side",
                                        fieldCls: "obligatoryTextField",
                                        displayField: "description",
                                        valueField: "idCurrency",

                                        blankText:
                                          DukeSource.global.GiroMessages
                                            .MESSAGE_REQUIRE,
                                        allowBlank: false,
                                        editable: false,
                                        forceSelection: true,
                                        name: "currency",
                                        itemId: "currency",
                                        store: {
                                          fields: ["idCurrency", "description"],
                                          proxy: {
                                            actionMethods: {
                                              create: "POST",
                                              read: "POST",
                                              update: "POST"
                                            },
                                            type: "ajax",
                                            url:
                                              "http://localhost:9000/giro/showListCurrencyActivesComboBox.htm",
                                            extraParams: {
                                              propertyOrder: "description"
                                            },
                                            reader: {
                                              type: "json",
                                              root: "data",
                                              successProperty: "success"
                                            }
                                          }
                                        },
                                        listeners: {
                                          specialkey: function(f, e) {
                                            DukeSource.global.DirtyView.focusEventEnter(
                                              f,
                                              e,
                                              me.down("#typeChange")
                                            );
                                          },
                                          select: function(cbo) {
                                            var typeChange = me.down(
                                              "#typeChange"
                                            );
                                            if (cbo.getValue() == "1") {
                                              typeChange.setValue("1.00");
                                              typeChange.setFieldStyle(
                                                "background: #D8D8D8"
                                              );
                                              typeChange.setReadOnly(true);
                                            } else {
                                              typeChange.setValue("");
                                              typeChange.setFieldStyle(
                                                "background: #d9ffdb"
                                              );
                                              typeChange.setReadOnly(false);
                                            }
                                          }
                                        }
                                      },
                                      {
                                        xtype: "NumberDecimalNumberObligatory",
                                        labelWidth: 120,
                                        padding: "0 5 0 0",
                                        anchor: "100%",
                                        decimalPrecision: 4,
                                        fieldLabel: "Tipo de cambio",
                                        name: "typeChange",
                                        itemId: "typeChange",
                                        listeners: {
                                          specialkey: function(f, e) {
                                            if (
                                              e.getKey() === e.ENTER ||
                                              e.getKey() === e.TAB
                                            ) {
                                              calculateNetLoss(me);
                                              DukeSource.global.DirtyView.focusEventEnter(
                                                f,
                                                e,
                                                me.down("#amountOrigin")
                                              );
                                            }
                                          },
                                          blur: function(field) {
                                            calculateNetLoss(me);
                                          }
                                        }
                                      },
                                      {
                                        xtype: "NumberDecimalNumberObligatory",
                                        labelWidth: 120,
                                        padding: "0 5 0 0",
                                        anchor: "100%",
                                        fieldLabel: "Monto",
                                        name: "amountOrigin",
                                        itemId: "amountOrigin",
                                        fieldCls: "numberPositive",
                                        maxLength: 20,
                                        listeners: {
                                          specialkey: function(f, e) {
                                            if (
                                              e.getKey() === e.ENTER ||
                                              e.getKey() === e.TAB
                                            ) {
                                              calculateNetLoss(me);
                                              DukeSource.global.DirtyView.focusEventEnter(
                                                f,
                                                e,
                                                me.down("#buttonSearchCta1")
                                              );
                                            }
                                          },
                                          blur: function(field) {
                                            calculateNetLoss(me);
                                          }
                                        }
                                      },
                                      {
                                        xtype: "NumberDecimalNumberObligatory",
                                        anchor: "100%",
                                        labelWidth: 120,
                                        padding: "0 5 0 0",
                                        fieldLabel: "P&eacute;rdida bruta",
                                        name: "grossLoss",
                                        itemId: "grossLoss",
                                        enforceMaxLength: true,
                                        fieldCls: "numberNegative",
                                        readOnly: true,
                                        maxLength: 20,
                                        value: 0
                                      },
                                      {
                                        xtype: "NumberDecimalNumber",
                                        anchor: "100%",
                                        labelWidth: 120,
                                        padding: "0 5 0 0",
                                        fieldLabel: "Recupero seguro",
                                        name: "sureAmountRecovery",
                                        itemId: "sureAmountRecovery",
                                        enforceMaxLength: true,
                                        readOnly: true,
                                        fieldCls: "fieldBlue",
                                        value: 0,
                                        maxLength: 20
                                      },
                                      {
                                        xtype: "NumberDecimalNumber",
                                        anchor: "100%",
                                        labelWidth: 120,
                                        padding: "0 5 0 0",
                                        fieldLabel: "Recup. funcionario",
                                        name: "lossRecovery",
                                        itemId: "lossRecovery",
                                        enforceMaxLength: true,
                                        readOnly: true,
                                        fieldCls: "fieldBlue",
                                        maxLength: 20,
                                        value: 0
                                      },
                                      {
                                        xtype: "NumberDecimalNumber",
                                        anchor: "100%",
                                        labelWidth: 120,
                                        padding: "0 5 0 0",
                                        fieldLabel: "Recupero tercero",
                                        name: "amountThirdRecovery",
                                        itemId: "amountThirdRecovery",
                                        enforceMaxLength: true,
                                        readOnly: true,
                                        fieldCls: "fieldBlue",
                                        maxLength: 20,
                                        value: 0
                                      },
                                      {
                                        xtype: "NumberDecimalNumber",
                                        anchor: "100%",
                                        labelWidth: 120,
                                        padding: "0 5 0 0",
                                        fieldLabel: "Total recupero",
                                        name: "totalRecovery",
                                        itemId: "totalRecovery",
                                        enforceMaxLength: true,
                                        readOnly: true,
                                        fieldCls: "fieldBlue",
                                        maxLength: 20,
                                        value: 0
                                      },
                                      {
                                        xtype: "NumberDecimalNumber",
                                        anchor: "100%",
                                        labelWidth: 120,
                                        padding: "0 5 0 0",
                                        allowBlank: true,
                                        readOnly: true,
                                        fieldCls: "numberNegative",
                                        name: "totalAmountSpend",
                                        itemId: "totalAmountSpend",
                                        value: 0,
                                        maxLength: 20,
                                        fieldLabel: "Gastos asociados"
                                      },
                                      {
                                        xtype: "NumberDecimalNumber",
                                        anchor: "100%",
                                        labelWidth: 120,
                                        padding: "0 5 0 0",
                                        allowBlank: true,
                                        readOnly: true,
                                        fieldCls: "numberNegative",
                                        name: "amountProvision",
                                        itemId: "amountProvision",
                                        value: 0,
                                        maxLength: 20,
                                        fieldLabel: "Provisiones"
                                      },
                                      {
                                        xtype: "NumberDecimalNumber",
                                        anchor: "100%",
                                        labelWidth: 120,
                                        padding: "0 5 0 0",
                                        allowBlank: true,
                                        readOnly: true,
                                        fieldCls: "numberNegative",
                                        name: "amountSubEvent",
                                        itemId: "amountSubEvent",
                                        value: 0,
                                        maxLength: 20,
                                        fieldLabel: "Sub-eventos"
                                      },
                                      {
                                        xtype: "NumberDecimalNumberObligatory",
                                        anchor: "100%",
                                        labelWidth: 120,
                                        padding: "0 5 0 0",
                                        fieldLabel: "Perdida neta",
                                        name: "netLoss",
                                        itemId: "netLoss",
                                        minValue: -1000000,
                                        enforceMaxLength: true,
                                        fieldCls: "readOnlyText",
                                        readOnly: true,
                                        maxLength: 20
                                      }
                                    ]
                                  },
                                  {
                                    xtype: "container",
                                    flex: 1,
                                    layout: "anchor",
                                    items: [
                                      {
                                        xtype: "datetimefield",
                                        format: "d/m/Y H:i",
                                        anchor: "100%",
                                        padding: "0 0 0 5",
                                        labelWidth: 100,
                                        fieldLabel: getName("EWDateOccurrence"),
                                        name: "dateOccurrence",
                                        itemId: "dateOccurrence",
                                        enforceMaxLength: true,
                                        fieldCls: "obligatoryTextField",
                                        blankText:
                                          DukeSource.global.GiroMessages
                                            .MESSAGE_REQUIRE,
                                        allowBlank: false,
                                        listeners: {
                                          expand: function(field, value) {
                                            if (
                                              me
                                                .down(
                                                  "datefield[name=dateDiscovery]"
                                                )
                                                .getValue() != undefined
                                            ) {
                                              field.setMaxValue(
                                                me
                                                  .down(
                                                    "datefield[name=dateDiscovery]"
                                                  )
                                                  .getValue()
                                              );
                                            } else {
                                              field.setMaxValue(new Date());
                                            }
                                          },
                                          specialkey: function(f, e) {
                                            DukeSource.global.DirtyView.focusEventEnter(
                                              f,
                                              e,
                                              me.down("#dateAccountingEvent")
                                            );
                                          }
                                        }
                                      },
                                      {
                                        xtype: "datetimefield",
                                        format: "d/m/Y H:i",
                                        anchor: "100%",
                                        padding: "0 0 0 5",
                                        labelWidth: 100,
                                        fieldLabel: "Fecha fin",
                                        name: "dateEnd",
                                        itemId: "dateEnd",
                                        enforceMaxLength: true,
                                        fieldCls: "obligatoryTextField",
                                        blankText:
                                          DukeSource.global.GiroMessages
                                            .MESSAGE_REQUIRE,
                                        allowBlank: false,
                                        listeners: {
                                          expand: function(field, value) {
                                            field.setMinValue(
                                              me
                                                .down("#dateOccurrence")
                                                .getValue()
                                            );
                                            field.setMaxValue(new Date());
                                          },
                                          blur: function(field, value) {
                                            field.setMinValue(
                                              me
                                                .down("#dateOccurrence")
                                                .getValue()
                                            );
                                            field.setMaxValue(new Date());
                                          }
                                        }
                                      },
                                      {
                                        xtype: "datetimefield",
                                        format: "d/m/Y H:i",
                                        padding: "0 0 0 5",
                                        labelWidth: 100,
                                        anchor: "100%",
                                        fieldLabel: "Fecha que descubre",
                                        name: "dateDiscovery",
                                        itemId: "dateDiscovery",
                                        enforceMaxLength: true,
                                        listeners: {
                                          expand: function(field, value) {
                                            field.setMinValue(
                                              me
                                                .down("#dateOccurrence")
                                                .getValue()
                                            );
                                            field.setMaxValue(new Date());
                                          },
                                          blur: function(field, value) {
                                            field.setMinValue(
                                              me
                                                .down("#dateOccurrence")
                                                .getValue()
                                            );
                                            field.setMaxValue(new Date());
                                          }
                                        }
                                      },
                                      {
                                        xtype: "datetimefield",
                                        format: "d/m/Y H:i",
                                        anchor: "100%",
                                        padding: "0 0 0 5",
                                        labelWidth: 100,
                                        fieldLabel: "Fecha de reporte",
                                        name: "dateAcceptLossEvent",
                                        itemId: "dateAcceptLossEvent",
                                        enforceMaxLength: true,
                                        value: new Date(),
                                        readOnly: false,
                                        editable: true,
                                        listeners: {
                                          expand: function(field, value) {
                                            field.setMinValue(
                                              me
                                                .down("#dateOccurrence")
                                                .getValue()
                                            );
                                            field.setMaxValue(new Date());
                                          },
                                          blur: function(field, value) {
                                            field.setMinValue(
                                              me
                                                .down("#dateOccurrence")
                                                .getValue()
                                            );
                                            field.setMaxValue(new Date());
                                          }
                                        }
                                      },
                                      {
                                        xtype: "datetimefield",
                                        format: "d/m/Y H:i",
                                        anchor: "100%",
                                        labelWidth: 100,
                                        padding: "0 0 0 5",
                                        fieldLabel: "Fecha contable",
                                        name: "dateAccountingEvent",
                                        itemId: "dateAccountingEvent",
                                        enforceMaxLength: true,
                                        listeners: {
                                          expand: function(field, value) {
                                            field.setMinValue(
                                              me
                                                .down("#dateOccurrence")
                                                .getValue()
                                            );
                                            field.setMaxValue(new Date());
                                          },
                                          blur: function(field, value) {
                                            field.setMinValue(
                                              me
                                                .down("#dateOccurrence")
                                                .getValue()
                                            );
                                            field.setMaxValue(new Date());
                                          }
                                        }
                                      },
                                      {
                                        xtype: "datetimefield",
                                        format: "d/m/Y H:i",
                                        anchor: "100%",
                                        labelWidth: 100,
                                        padding: "0 0 0 5",
                                        fieldLabel: "Fecha de cierre",
                                        name: "dateCloseEvent",
                                        enforceMaxLength: true,
                                        listeners: {
                                          expand: function(field, value) {
                                            field.setMinValue(
                                              me
                                                .down("#dateOccurrence")
                                                .getValue()
                                            );
                                            field.setMaxValue(new Date());
                                          },
                                          blur: function(field, value) {
                                            field.setMinValue(
                                              me
                                                .down("#dateOccurrence")
                                                .getValue()
                                            );
                                            field.setMaxValue(new Date());
                                          }
                                        }
                                      },
                                      {
                                        xtype: "container",
                                        anchor: "100%",
                                        height: 26,
                                        padding: "0 0 0 5",
                                        layout: {
                                          type: "hbox"
                                        },
                                        items: [
                                          {
                                            xtype: "textfield",
                                            name: "idPlanAccountLoss",
                                            itemId: "idPlanAccountLoss",
                                            hidden: true
                                          },
                                          {
                                            xtype:
                                              "TextFieldNumberFormatReadOnly",
                                            flex: 1,
                                            labelWidth: 100,
                                            fieldLabel: "Cuenta contable",
                                            name: "planAccountLoss",
                                            itemId: "planAccountLoss",
                                            enforceMaxLength: true
                                          },
                                          {
                                            xtype: "button",
                                            name: "buttonSearchCta1",
                                            itemId: "buttonSearchCta1",
                                            iconCls: "search",
                                            handler: function() {
                                              var window = Ext.create(
                                                "DukeSource.view.risk.parameter.windows.ViewWindowSearchPlanAccountCountable",
                                                { modal: true }
                                              ).show();
                                              window
                                                .down("grid")
                                                .on("itemdblclick", function() {
                                                  me.down(
                                                    "#planAccountLoss"
                                                  ).setValue(
                                                    window
                                                      .down("grid")
                                                      .getSelectionModel()
                                                      .getSelection()[0]
                                                      .get("codeAccount")
                                                  );
                                                  me.down(
                                                    "#idPlanAccountLoss"
                                                  ).setValue(
                                                    window
                                                      .down("grid")
                                                      .getSelectionModel()
                                                      .getSelection()[0]
                                                      .get("idAccountPlan")
                                                  );
                                                  me.down(
                                                    "#bookkeepingEntryLoss"
                                                  ).focus(false, 100);
                                                  window.close();
                                                });
                                            }
                                          }
                                        ]
                                      },
                                      {
                                        xtype:
                                          DukeSource.view.risk.util
                                            .UpperCaseTextField,
                                        labelWidth: 100,
                                        padding: "0 0 0 5",
                                        anchor: "100%",
                                        fieldLabel: "Asiento contable",
                                        name: "bookkeepingEntryLoss",
                                        itemId: "bookkeepingEntryLoss",
                                        enforceMaxLength: true,
                                        maxLength: 50,
                                        listeners: {
                                          specialkey: function(f, e) {
                                            DukeSource.global.DirtyView.focusEventEnter(
                                              f,
                                              e,
                                              me.down("#dateAcceptLossEvent")
                                            );
                                          }
                                        }
                                      },
                                      {
                                        xtype: "combobox",
                                        anchor: "100%",
                                        allowBlank: blank(
                                          "EWEntityPenalizesLoss"
                                        ),
                                        fieldCls: styleField(
                                          "EWEntityPenalizesLoss"
                                        ),
                                        fieldLabel: "Empresa que sanciona",
                                        labelWidth: 100,
                                        padding: "0 0 0 5",
                                        msgTarget: "side",
                                        displayField: "description",
                                        valueField: "idSanctioningCompany",

                                        forceSelection: true,
                                        editable: false,
                                        blankText:
                                          DukeSource.global.GiroMessages
                                            .MESSAGE_REQUIRE,
                                        name: "entityPenalizesLoss",
                                        itemId: "entityPenalizesLoss",
                                        store: {
                                          fields: [
                                            "idSanctioningCompany",
                                            "description"
                                          ],
                                          proxy: {
                                            actionMethods: {
                                              create: "POST",
                                              read: "POST",
                                              update: "POST"
                                            },
                                            type: "ajax",
                                            url:
                                              "http://localhost:9000/giro/showListSanctioningCompanyActivesComboBox.htm",
                                            extraParams: {
                                              propertyOrder: "description"
                                            },
                                            reader: {
                                              type: "json",
                                              root: "data",
                                              successProperty: "success"
                                            }
                                          }
                                        },
                                        listeners: {
                                          specialkey: function(f, e) {
                                            DukeSource.global.DirtyView.focusEventEnter(
                                              f,
                                              e,
                                              me.down("#eventType")
                                            );
                                          }
                                        }
                                      },
                                      {
                                        xtype: "combobox",
                                        anchor: "100%",
                                        allowBlank: blank("EWInsuranceCompany"),
                                        fieldCls: styleField(
                                          "EWInsuranceCompany"
                                        ),
                                        labelWidth: 100,
                                        padding: "0 0 0 5",
                                        fieldLabel: getName(
                                          "EWInsuranceCompany"
                                        ),
                                        msgTarget: "side",
                                        blankText:
                                          DukeSource.global.GiroMessages
                                            .MESSAGE_REQUIRE,
                                        editable: false,
                                        name: "insuranceCompany",
                                        itemId: "insuranceCompany",
                                        enforceMaxLength: true,
                                        displayField: "description",
                                        valueField: "idInsuranceCompany",

                                        forceSelection: true,
                                        maxLength: 50,
                                        store: {
                                          fields: [
                                            "idInsuranceCompany",
                                            "description"
                                          ],
                                          proxy: {
                                            actionMethods: {
                                              create: "POST",
                                              read: "POST",
                                              update: "POST"
                                            },
                                            type: "ajax",
                                            url:
                                              "http://localhost:9000/giro/showListInsuranceCompanyActives.htm",
                                            extraParams: {
                                              propertyOrder: "description"
                                            },
                                            reader: {
                                              type: "json",
                                              root: "data",
                                              successProperty: "success"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        xtype: "filefield",
                                        anchor: "100%",
                                        labelWidth: 100,
                                        padding: "0 0 0 5",
                                        fieldLabel: "Documento adjunto",
                                        name: "document",
                                        itemId: "document",
                                        buttonConfig: {
                                          iconCls: "tesla even-attachment"
                                        },
                                        buttonText: ""
                                      }
                                    ]
                                  }
                                ]
                              }
                            ]
                          }
                        ]
                      },
                      {
                        xtype: 'UpperCaseTextArea',
                        anchor: "100%",
                        labelWidth: 125,
                        height: 40,
                        fieldLabel: "Comentarios",
                        name: "comments",
                        itemId: "comments",
                        allowBlank: blank("EWComments"),
                        fieldCls: styleField("EWComments"),
                        maxLength: 2000,
                        listeners: {
                          specialkey: function(f, e) {
                            if (
                              e.getKey() === e.ENTER ||
                              e.getKey() === e.TAB
                            ) {
                              DukeSource.global.DirtyView.focusEventEnter(
                                f,
                                e,
                                me.down("#document")
                              );
                            }
                          }
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ],
        buttons: [
          {
            text: "Guardar",
            iconCls: "save",
            itemId: "saveEventOperational",
            scale: "medium",
            handler: function(button) {
              calculateNetLoss(me);

              var panel = Ext.ComponentQuery.query(
                "ViewPanelEventsRiskOperational"
              )[0];
              var gridSubEvent = Ext.ComponentQuery.query(
                "ViewWindowRegisterSubEvents grid"
              )[0];
              var form = me.down("form");

              var validTreeNotBlank =
                form.down("#factorRisk").getValue() !== "" &&
                form.down("#unity").getValue() !== "";

              if (form.getForm().isValid() && validTreeNotBlank) {
                form.getForm().submit({
                  url:
                    "http://localhost:9000/giro/saveEventMaster.htm?nameView=ViewPanelEventsRiskOperational",
                  waitMsg: "Saving...",
                  method: "POST",
                  success: function(form, action) {
                    var valor = Ext.decode(action.response.responseText);
                    if (valor.success) {
                      DukeSource.global.DirtyView.messageAlert(
                        DukeSource.global.GiroMessages.TITLE_MESSAGE,
                        valor.message,
                        Ext.Msg.INFO
                      );
                      me.close();
                      var grid = panel.down("grid");

                      grid.store.getProxy().url =
                        "http://localhost:9000/giro/getAllEventMaster.htm";
                      gridSubEvent.down("pagingtoolbar").moveFirst();
                      grid.down("pagingtoolbar").moveFirst();
                    } else {
                      DukeSource.global.DirtyView.messageAlert(
                        DukeSource.global.GiroMessages.TITLE_ERROR,
                        valor.message,
                        Ext.Msg.WARNING
                      );
                    }
                  },
                  failure: function(form, action) {
                    var valor = Ext.decode(action.response.responseText);
                    if (!valor.success) {
                      DukeSource.global.DirtyView.messageAlert(
                        DukeSource.global.GiroMessages.TITLE_ERROR,
                        valor.message,
                        Ext.Msg.WARNING
                      );
                    }
                  }
                });
              } else {
                me.down("#descriptionFactorRisk").setFieldStyle("color:red");
                me.down("#descriptionUnity").setFieldStyle("color:red");
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_WARNING,
                  DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
                  Ext.Msg.WARNING
                );
              }
            }
          },
          {
            text: "Ficha",
            itemId: "fileEvents",
            hidden: true,
            scale: "medium",
            handler: function() {
              var window = Ext.create("Ext.window.Window", {
                height: 500,
                width: 800,
                layout: "fit",
                modal: true
              }).show();
              var value = me.down("#idEvent").getValue();
              window.removeAll();
              window.add({
                xtype: "component",
                autoEl: {
                  tag: "iframe",
                  src:
                    "http://localhost:9000/giro/pdfGeneralReport.htm?values=" +
                    value +
                    "&names=" +
                    "idEvent" +
                    "&types=" +
                    "String" +
                    "&nameReport=" +
                    "ROEVEN0008"
                }
              });
            },
            iconCls: "pdf"
          },
          {
            text: "Salir",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          }
        ],
        buttonAlign: "center",
        listeners: {
          afterrender: function(view) {
            calculateNetLoss(view);
          }
        }
      });
      me.callParent(arguments);
    }
  }
);

function calculateNetLoss(me) {
  var typeChange = me.down("#typeChange").getValue();
  var amountOrigin = me.down("#amountOrigin").getValue();
  var grossLoss = me.down("#grossLoss");
  var provision = me.down("#amountProvision").getValue();
  var spend = me.down("#totalAmountSpend").getValue();
  var subEvent = me.down("#amountSubEvent").getValue();
  grossLoss.setValue(
    parseFloat(amountOrigin) * parseFloat(typeChange) + parseFloat(subEvent)
  );
  var lossRecovery = me.down("#lossRecovery").getValue();
  var thirdRecovery = me.down("#amountThirdRecovery").getValue();
  var sureAmountRecovery = me.down("#sureAmountRecovery").getValue();
  var totalRecovery =
    parseFloat(lossRecovery) +
    parseFloat(sureAmountRecovery) +
    parseFloat(thirdRecovery);
  var result =
    parseFloat(grossLoss.getValue()) -
    totalRecovery +
    parseFloat(spend) +
    parseFloat(provision);

  me.down("#totalAmountSpend").setValue(spend);
  me.down("#amountSubEvent").setValue(subEvent);
  me.down("#amountProvision").setValue(provision);
  me.down("#lossRecovery").setValue(lossRecovery);
  me.down("#amountThirdRecovery").setValue(thirdRecovery);
  me.down("#sureAmountRecovery").setValue(sureAmountRecovery);
  me.down("#totalRecovery").setValue(totalRecovery);
  me.down("#netLoss").setValue(result);

  var netLossOrigin = me.down("#netLossOrigin").getValue();
  var diffNetLoss = result - parseFloat(netLossOrigin);
  me.down("#diffNetLoss").setValue(diffNetLoss);
}
