Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterIncidents', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowRegisterIncidents',
    layout: 'fit',
    requires: [
        'DukeSource.view.risk.util.ViewComboYesNo',
        'Ext.ux.DateTimeField'
    ],
    width: 800,
    title: 'Ingreso incidente',
    border: false,

    coloredButtonActive: function (btn, index) {
        this.setHeight(this.heightMain);
        this.down('form').getLayout().setActiveItem(index);

        this.buttonActive.setText(this.buttonTextActive);
        this.buttonTextActive = btn.getText();

        btn.setText(btn.getText() + '<span style="color:red"> * </span>');
        this.buttonActive = btn;
    },
    initComponent: function () {
        var me = this;
        var hasPreIncident = this.hasPreIncident;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    padding: '0 0 0 2',
                    fieldDefaults: {
                        labelCls: 'changeSizeFontToEightPt',
                        fieldCls: 'changeSizeFontToEightPt'
                    },
                    autoScroll: true,
                    layout: 'card',
                    bodyPadding: 10,
                    items: [
                        {
                            itemId: 'card-0',
                            xtype: 'container',
                            layout: 'fit',
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Datos del incidente',
                                    bodyStyle: 'padding:0px',
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            name: 'idDetailIncidents'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            value: 'id',
                                            name: 'idIncident',
                                            itemId: 'idIncident'
                                        },
                                        {
                                            xtype: 'textfield',
                                            value: DukeSource.global.GiroConstants.DRAFT,
                                            hidden: true,
                                            name: 'codeIncident',
                                            itemId: 'codeIncident'
                                        },
                                        {
                                            xtype: 'datetimefield',
                                            format: 'd/m/Y H:i:s',
                                            hidden: true,
                                            name: 'dateRegister',
                                            itemId: 'dateRegister'
                                        },
                                        {
                                            xtype: 'textfield',
                                            value: userName,
                                            hidden: true,
                                            name: 'userRegister',
                                            itemId: 'userRegister'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'collaborator',
                                            name: 'collaborator'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'codeCorrelative',
                                            name: 'codeCorrelative'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'checkApprove',
                                            name: 'checkApprove'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'userApprove',
                                            name: 'userApprove'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'commentApprove',
                                            name: 'commentApprove'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'dateApprove',
                                            name: 'dateApprove'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            name: 'fileAttachment',
                                            itemId: 'fileAttachment',
                                            value: 'N'
                                        },
                                        {
                                            xtype: 'textfield',
                                            name: 'userReport',
                                            itemId: 'userReport',
                                            hidden: true
                                        },
                                        {
                                            xtype: "textfield",
                                            hidden: true,
                                            itemId: 'commentManager',
                                            name: "commentManager"
                                        },
                                        {
                                            xtype: "textfield",
                                            hidden: true,
                                            itemId: 'descriptionIndicatorIncidents',
                                            name: "descriptionIndicatorIncidents"
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            name: 'numberRisk',
                                            itemId: 'numberRisk',
                                            value: '0'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            name: 'codesRisk',
                                            itemId: 'codesRisk'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            name: 'generateLoss',
                                            itemId: 'generateLoss',
                                            value: 'N'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            name: 'sequenceStateIncident',
                                            itemId: 'sequence'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            value: DukeSource.global.GiroConstants.OPERATIONAL,
                                            name: 'typeIncident',
                                            itemId: 'typeIncident'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'businessLineOne',
                                            name: 'businessLineOne'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'businessLineTwo',
                                            name: 'businessLineTwo'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'businessLineThree',
                                            name: 'businessLineThree'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'eventOne',
                                            name: 'eventOne'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'eventTwo',
                                            name: 'eventTwo'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'eventThree',
                                            name: 'eventThree'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'factorRisk',
                                            name: 'factorRisk'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'idProcessType',
                                            name: 'idProcessType'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'idProcess',
                                            name: 'idProcess'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'idSubProcess',
                                            name: 'idSubProcess'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'idActivity',
                                            name: 'idActivity'
                                        },
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            itemId: 'codeProcess',
                                            name: 'codeProcess'
                                        },
                                        {
                                            xtype:"UpperCaseTextField",
                                            allowBlank: false,
                                            fieldLabel: 'Descripci&oacute;n corta',
                                            maxLength: 300,
                                            msgTarget: 'side',
                                            anchor: '100%',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'descriptionShort',
                                            itemId: 'descriptionShort',
                                            fieldCls: 'obligatoryTextField'
                                        },
                                        {
                                            xtype: 'UpperCaseTextArea',
                                            allowBlank: false,
                                            padding: '5 0 0 0',
                                            fieldCls: 'obligatoryTextField',
                                            height: 30,
                                            maxLength: 3000,
                                            msgTarget: 'side',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            anchor: '100%',
                                            fieldLabel: 'Descripci&oacute;n larga',
                                            name: 'descriptionLarge',
                                            itemId: 'descriptionLarge'
                                        },
                                        {
                                            xtype: 'UpperCaseTextArea',
                                            fieldLabel: getName('INC_WIR_TXA_CauseCollaborator'),
                                            allowBlank: blank('IWCCauseCollaborator'),
                                            fieldCls: styleField('IWCCauseCollaborator'),
                                            name: 'causeCollaborator',
                                            itemId: 'causeCollaborator',
                                            height: 30,
                                            maxLength: 600,
                                            msgTarget: 'side',
                                            anchor: '100%',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE
                                        },
                                        {
                                            xtype: 'combobox',
                                            allowBlank: false,
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'stateIncident',
                                            itemId: 'stateIncident',
                                            displayField: 'description',
                                            valueField: 'id',
                                            editable: false,
                                            forceSelection: true,
                                            fieldLabel: 'Estado',
                                            msgTarget: 'side',
                                            store: {
                                                fields: ['id', 'description', 'sequence'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/findStateIncident.htm',
                                                    extraParams: {
                                                        propertyFind: 'si.typeIncident',
                                                        valueFind: DukeSource.global.GiroConstants.OPERATIONAL,
                                                        propertyOrder: 'si.sequence'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            },
                                            listeners: {
                                                select: function (cbo) {
                                                    var record = cbo.findRecord(cbo.valueField || cbo.displayField, cbo.getValue());
                                                    var index = cbo.store.indexOf(record);
                                                    me.down('#sequence').setValue(cbo.store.data.items[index].data.sequence);

                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            fieldLabel: getName('INC_CBX_WorkArea'),
                                            name: 'workArea',
                                            itemId: 'workArea',
                                            displayField: 'description',
                                            valueField: 'idWorkArea',

                                            editable: true,
                                            plugins: ['ComboSelectCount'],
                                            store: {
                                                fields: ['idWorkArea', 'description'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListWorkAreaActivesComboBox.htm',
                                                    extraParams: {
                                                        propertyOrder: 'description'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            hidden: hidden('INC_WIR_CBX_Agency'),
                                            msgTarget: 'side',
                                            allowBlank: blank('INC_WIR_CBX_Agency'),
                                            fieldCls: styleField('INC_WIR_CBX_Agency'),
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            fieldLabel: 'Agencia',
                                            itemId: 'agency',
                                            name: 'agency',
                                            displayField: 'description',
                                            valueField: 'idAgency',
                                            editable: true,
                                            plugins: ['ComboSelectCount'],
                                            store: {
                                                fields: ['idAgency', 'description'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListAgencyActivesComboBox.htm',
                                                    extraParams: {
                                                        propertyOrder: 'a.description'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            },
                                            listeners: {
                                                specialkey: function (f, e) {
                                                    DukeSource.global.DirtyView.focusEventEnterObligatory(f, e, me.down('datefield[name=dateOccurrence]'))

                                                }
                                            }
                                        },
                                        {
                                            xtype: 'datetimefield',
                                            allowBlank: false,
                                            format: 'd/m/Y H:i:s',
                                            msgTarget: 'side',
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            fieldLabel: 'Fecha ocurrencia',
                                            name: 'dateOccurrence',
                                            itemId: 'dateOccurrence',
                                            listeners: {
                                                expand: function (field, value) {
                                                    field.setMaxValue(new Date());
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'datetimefield',
                                            allowBlank: blank('INC_WIR_DTF_DateDiscovery'),
                                            fieldCls: styleField('INC_WIR_DTF_DateDiscovery'),
                                            hidden: hidden('INC_WIR_DTF_DateDiscovery'),
                                            format: 'd/m/Y H:i:s',
                                            msgTarget: 'side',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            fieldLabel: 'Fecha descubrimiento',
                                            name: 'dateDiscovery',
                                            itemId: 'dateDiscovery',
                                            listeners: {
                                                expand: function (field, value) {
                                                    field.setMinValue(me.down('#dateOccurrence').getRawValue());
                                                    field.setMaxValue(new Date());
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'datetimefield',
                                            allowBlank: false,
                                            format: 'd/m/Y H:i:s',
                                            msgTarget: 'side',
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            fieldLabel: 'Fecha de reporte',
                                            name: 'dateReport',
                                            itemId: 'dateReport',
                                            value: new Date(),
                                            listeners: {
                                                expand: function (field, value) {
                                                    field.setMinValue(me.down('#dateOccurrence').getRawValue());
                                                    field.setMaxValue(new Date());
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'datetimefield',
                                            format: 'd/m/Y H:i:s',
                                            msgTarget: 'side',
                                            fieldLabel: 'Fecha fin',
                                            hidden: hidden('INC_WIR_DTF_DateFinal'),
                                            name: 'dateFinal',
                                            itemId: 'dateFinal',
                                            listeners: {
                                                expand: function (field, value) {
                                                    field.setMinValue(me.down('#dateOccurrence').getRawValue());
                                                    field.setMaxValue(new Date());
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            allowBlank: blank('IWIncidentsGroup'),
                                            fieldCls: styleField('IWIncidentsGroup'),
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'incidentsGroup',
                                            itemId: 'incidentsGroup',
                                            displayField: 'description',
                                            valueField: 'idIncidentGroup',
                                            editable: false,
                                            forceSelection: true,
                                            fieldLabel: 'Grupo',
                                            msgTarget: 'side',
                                            store: {
                                                fields: ['idIncidentGroup', 'description'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListIncidentGroupActives.htm',
                                                    extraParams: {
                                                        propertyOrder: 'description'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'displayfield',
                                            hidden: hidden('INC_WIR_DSF_DescriptionProcess'),
                                            anchor: '100%',
                                            fieldLabel: 'Proceso afectado',
                                            itemId: 'descriptionProcess',
                                            name: 'descriptionProcess',
                                            height: 18,
                                            value: '(Seleccionar)',
                                            fieldCls: 'style-for-url',
                                            listeners: {
                                                afterrender: function (view) {
                                                    view.getEl().on('click', function () {
                                                        Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeProcess', {
                                                            backWindow: me
                                                        }).show();
                                                    })
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'displayfield',
                                            itemId: 'descriptionBusinessLineOne',
                                            hidden: hidden('INC_WIR_DSF_DescriptionBusinessLine'),
                                            name: 'descriptionBusinessLineOne',
                                            fieldLabel: 'L&iacute;nea de negocio',
                                            height: 18,
                                            value: '(Seleccionar)',
                                            fieldCls: 'style-for-url',
                                            listeners: {
                                                afterrender: function (view) {
                                                    view.getEl().on('click', function () {
                                                        Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeBusinessLine', {
                                                            winParent: me
                                                        }).show();
                                                    })
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'displayfield',
                                            itemId: 'descriptionEventOne',
                                            hidden: hidden('INC_WIR_DSF_DescriptionEventOne'),
                                            name: 'descriptionEventOne',
                                            fieldLabel: 'Evento de riesgo',
                                            height: 18,
                                            value: '(Seleccionar)',
                                            fieldCls: 'style-for-url',
                                            listeners: {
                                                afterrender: function (view) {
                                                    view.getEl().on('click', function () {
                                                        Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeTypeEvent', {
                                                            winParent: me
                                                        }).show();
                                                    })
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'displayfield',
                                            hidden: hidden('INC_WIR_DSF_DescriptionFactorRisk'),
                                            fieldLabel: 'Factor de riesgo',
                                            height: 18,
                                            itemId: 'descriptionFactorRisk',
                                            name: 'descriptionFactorRisk',
                                            value: '(Seleccionar)',
                                            fieldCls: 'style-for-url',
                                            listeners: {
                                                afterrender: function (view) {
                                                    view.getEl().on('click', function () {
                                                        Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeFactorRisk', {
                                                            winParent: me
                                                        }).show();
                                                    })
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'combobox',
                                            allowBlank: blank('INC_WIR_CBX_RiskType'),
                                            fieldCls: styleField('INC_WIR_CBX_RiskType'),
                                            hidden: hidden('INC_WIR_CBX_RiskType'),
                                            displayField: 'description',
                                            valueField: 'idRiskType',
                                            editable: false,
                                            forceSelection: true,
                                            fieldLabel: 'Riesgo relacionado',
                                            msgTarget: 'side',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'riskType',
                                            itemId: 'riskType',
                                            store: {
                                                fields: ['idRiskType', 'description'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListRiskTypeActives.htm',
                                                    extraParams: {
                                                        propertyOrder: 'description'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: "checkbox",
                                            fieldLabel: "Confidencial",
                                            name: "confidential",
                                            itemId: 'confidential',
                                            inputValue: "S",
                                            uncheckedValue: "N",
                                            hidden: hidden('IWCBConfidential'),
                                            checked: false,
                                            listeners: {
                                                specialkey: function (c, d) {
                                                    DukeSource.global.DirtyView.focusEventEnterObligatory(c, d, a.down("#processOrigin"))
                                                },
                                                change: function (c) {
                                                    if (c.checked === true) {
                                                        c.up("form").setBodyStyle("background", "#FFDC9E");
                                                    } else {
                                                        c.up("form").setBodyStyle("background", "#FFF");
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'UpperCaseTextArea',
                                            fieldLabel: 'Comentarios',
                                            name: 'comments',
                                            itemId: 'comments',
                                            height: 30,
                                            maxLength: 1000,
                                            msgTarget: 'side',
                                            anchor: '100%',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE
                                        },
                                        {
                                            xtype: 'fileuploadfield',
                                            fieldLabel: 'Documento',
                                            name: 'document',
                                            itemId: 'document',
                                            anchor: "100%",
                                            buttonConfig: {
                                                iconCls: 'tesla even-attachment'
                                            },
                                            buttonText: ""
                                        },
                                        {
                                            xtype: 'fieldset',
                                            title: 'Mesa de ayuda',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            padding: 5,
                                            items: [
                                                {
                                                    xtype: 'ViewComboYesNo',
                                                    name: 'hasTicket',
                                                    itemId: 'hasTicket',
                                                    msgTarget: 'side',
                                                    fieldCls:"UpperCaseTextFieldReadOnly",
                                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                    fieldLabel: 'Tiene ticket M.A',
                                                    listeners: {
                                                        select: function (cbo) {
                                                            if (cbo.getValue() === DukeSource.global.GiroConstants.YES) {
                                                                me.down('#numberTicket').setVisible(true);
                                                            } else {
                                                                me.down('#numberTicket').setVisible(false);
                                                            }
                                                        },
                                                        render: function (cbo) {
                                                            if (cbo.getValue() === DukeSource.global.GiroConstants.YES) {
                                                                me.down('#numberTicket').setVisible(true);
                                                            } else {
                                                                me.down('#numberTicket').setVisible(false);
                                                            }
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype: "textfield",
                                                    margin: '0 0 0 10',
                                                    hidden: true,
                                                    fieldLabel: "N&uacute;mero",
                                                    value: '',
                                                    name: "numberTicket",
                                                    itemId: "numberTicket"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            itemId: 'card-1',
                            xtype: 'container',
                            layout: 'fit',
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Impacto económico',
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            allowBlank: true,
                                            multiSelect: true,
                                            hidden: hidden('INC_WIR_CBX_Impact'),
                                            itemId: 'impact',
                                            name: 'impact',
                                            displayField: 'description',
                                            valueField: 'value',
                                            fieldLabel: 'Impacto',
                                            store: {
                                                fields: ['value', 'description'],
                                                autoLoad: true,
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                    extraParams: {
                                                        propertyFind: 'identified',
                                                        valueFind: 'IMPACT_INCIDENT',
                                                        propertyOrder: 'value'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }

                                        },
                                        {
                                            xtype: "combobox",
                                            editable: false,
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            fieldLabel: "Moneda",
                                            name: "currency",
                                            itemId: "currency",
                                            displayField: "description",
                                            valueField: "idCurrency",
                                            forceSelection: false,
                                            store: {
                                                fields: ["idCurrency", "description", "symbol", "state"],
                                                proxy: {
                                                    actionMethods: {
                                                        create: "POST",
                                                        read: "POST",
                                                        update: "POST"
                                                    },
                                                    type: "ajax",
                                                    url: "http://localhost:9000/giro/showListCurrencyActivesComboBox.htm",
                                                    extraParams: {
                                                        propertyOrder: "description"
                                                    },
                                                    reader: {
                                                        type: "json",
                                                        root: "data",
                                                        successProperty: "success"
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: "NumberDecimalNumber",
                                            fieldLabel: "Monto",
                                            value: 0,
                                            name: "amountLoss",
                                            itemId: "amountLoss"
                                        },
                                        {
                                            xtype: 'combobox',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'typeBreach',
                                            itemId: 'typeBreach',
                                            displayField: 'description',
                                            valueField: 'value',
                                            editable: false,
                                            forceSelection: true,
                                            fieldLabel: 'Se incumplio',
                                            msgTarget: 'side',
                                            store: {
                                                fields: ['value', 'description'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                    extraParams: {
                                                        propertyFind: 'identified',
                                                        valueFind: 'INC_TYPE_BREACH',
                                                        propertyOrder: 'description'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'UpperCaseTextArea',
                                            fieldLabel: 'Normativa o documento incumplido',
                                            name: 'nameBreach',
                                            itemId: 'nameBreach',
                                            height: 30,
                                            maxLength: 2000,
                                            msgTarget: 'side',
                                            anchor: '100%',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE
                                        },
                                        {
                                            xtype: 'UpperCaseTextArea',
                                            fieldLabel: 'Detallar incumplimiento',
                                            name: 'commentBreach',
                                            itemId: 'commentBreach',
                                            height: 30,
                                            maxLength: 2000,
                                            msgTarget: 'side',
                                            anchor: '100%',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE
                                        }

                                    ]
                                }
                            ]
                        },
                        {
                            itemId: 'card-2',
                            xtype: 'container',
                            layout: 'fit',
                            items: [
                                {
                                    xtype: 'fieldset',
                                    itemId: 'reporterBy',
                                    title: 'Reportado por',
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            anchor: '100%',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'originIncident',
                                            value: DukeSource.global.GiroConstants.GIRO,
                                            fieldCls: hasPreIncident ? 'readOnlyText' : '',
                                            itemId: 'originIncident',
                                            displayField: 'description',
                                            valueField: 'value',
                                            editable: false,
                                            forceSelection: true,
                                            msgTarget: 'side',
                                            fieldLabel: 'Procedencia',
                                            store: {
                                                fields: ['value', 'description'],
                                                proxy: {
                                                    actionMethods: {
                                                        create: 'POST',
                                                        read: 'POST',
                                                        update: 'POST'
                                                    },
                                                    type: 'ajax',
                                                    url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                    extraParams: {
                                                        propertyFind: 'identified',
                                                        valueFind: 'ORIGININC',
                                                        propertyOrder: 'description'
                                                    },
                                                    reader: {
                                                        type: 'json',
                                                        root: 'data',
                                                        successProperty: 'success'
                                                    }
                                                }
                                            },
                                            changeCbo: function (cbo) {
                                                if (cbo.getValue() === DukeSource.global.GiroConstants.MAIL || cbo.getValue() === DukeSource.global.GiroConstants.GIRO) {
                                                    me.down('#containerUserReport').setVisible(true);
                                                } else {
                                                    me.down('#containerUserReport').setVisible(false);
                                                    me.down('textfield[name=userRegister]').setValue(null);
                                                }
                                            },
                                            listeners: {
                                                select: function (cbo) {
                                                    this.changeCbo(cbo);
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'container',
                                            itemId: 'containerUserReport',
                                            height: 26,
                                            layout: 'hbox',
                                            items: [
                                                {
                                                    xtype: 'textfield',
                                                    flex: 1,
                                                    name: 'fullNameUserReport',
                                                    itemId: 'fullNameUserReport',
                                                    fieldCls: hasPreIncident ? 'readOnlyText' : '',
                                                    fieldLabel: 'Quien reporta'
                                                },
                                                {
                                                    xtype: 'button',
                                                    disabled: hasPreIncident,
                                                    iconCls: 'search',
                                                    handler: function () {
                                                        var win = Ext.create('DukeSource.view.risk.util.search.SearchUser', {
                                                            modal: true
                                                        }).show();
                                                        win.down('grid').on('itemdblclick', function (grid, record) {
                                                            me.down('#userReport').setValue(record.get('userName'));
                                                            me.down('#fullNameUserReport').setValue(record.get('fullName'));
                                                            win.close();
                                                        });
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'datetimefield',
                                            format: 'd/m/Y H:i:s',
                                            readOnly: true,
                                            fieldCls: 'readOnlyText',
                                            hidden: !hasPreIncident,
                                            anchor: '60%',
                                            fieldLabel: 'Fecha de registro',
                                            name: 'dateRegisterPreIncident',
                                            itemId: 'dateRegisterPreIncident'
                                        },
                                        {
                                            xtype: 'textfield',
                                            readOnly: true,
                                            anchor: '60%',
                                            fieldCls: 'readOnlyText',
                                            hidden: !hasPreIncident,
                                            fieldLabel: '&Aacute;rea',
                                            name: 'workAreaPreIncident',
                                            itemId: 'workAreaPreIncident'
                                        }
                                    ],
                                    listeners: {
                                        render: function (fds) {
                                            if (hasPreIncident) {
                                                fds.query('.combobox').forEach(function (c) {
                                                    c.setReadOnly(true);
                                                });
                                            }
                                        }
                                    }
                                }

                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    width: 220,
                    padding: 5,
                    layout: 'anchor',
                    dock: 'left',
                    items: [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Código',
                            labelAlign: 'top',
                            readOnly: true,
                            labelWidth: 50,
                            height: 40,
                            value: DukeSource.global.GiroConstants.DRAFT,
                            fieldCls: 'box-draft',
                            name: 'codeIncidentTemp',
                            itemId: 'codeIncidentTemp'
                        },
                        {
                            xtype: 'button',
                            text: 'Aprobaci&oacute;n',
                            flex: 0.5,
                            hidden: hidden('INC_WIR_BTN_ApproveIncident'),
                            margin: '0 0 0 2',
                            iconCls: 'evaluate',
                            name: 'btnApproveIncident',
                            itemId: 'btnApproveIncident',
                            cls: 'my-btn',
                            overCls: 'my-over',
                            handler: function () {
                                var win = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.WindowApproveCorrelative', {
                                    modal: true,
                                    id: me.down('#idIncident').getValue(),
                                    actionExecute: 'approveIncident',
                                    module: DukeSource.global.GiroConstants.OPERATIONAL,
                                    parentWindow: me
                                });
                                if (me.down('#checkApprove').getValue() === DukeSource.global.GiroConstants.YES) {
                                    win.down('#dateApprove').setValue(me.down('#dateApprove').getValue());
                                    win.down('#userApprove').setValue(me.down('#userApprove').getValue());
                                    win.down('#commentApprove').setValue(me.down('#commentApprove').getValue());
                                    win.down('#saveApprove').setDisabled(true);
                                    win.down('#saveApprove').setVisible(false);
                                }
                                win.show();
                            }
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'fieldset',
                                    title: 'Inicio',
                                    itemId: 'options',
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch',
                                        defaultMargins: {
                                            bottom: 1
                                        }
                                    },

                                    items: [
                                        {
                                            xtype: 'button',
                                            textAlign: 'left',
                                            text: 'Datos del incidente',
                                            handler: function (btn) {
                                                me.coloredButtonActive(btn, 0);
                                            }
                                        },
                                        {
                                            xtype: 'button',
                                            textAlign: 'left',
                                            text: 'Impacto económico',
                                            handler: function (btn) {
                                                me.coloredButtonActive(btn, 1);
                                            }
                                        },
                                        {
                                            xtype: 'button',
                                            textAlign: 'left',
                                            text: 'Reportado por',
                                            handler: function (btn) {
                                                me.coloredButtonActive(btn, 2);
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    layout: 'anchor',
                                    title: 'Informaci&oacute;n de registro',
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            hidden: true,
                                            readOnly: true,
                                            value: userName,
                                            fieldCls: 'readOnlyText',
                                            name: 'userRegisterTemp',
                                            itemId: 'userRegisterTemp',
                                            enforceMaxLength: true
                                        },
                                        {
                                            xtype: 'datetimefield',
                                            format: 'd/m/Y H:i:s',
                                            readOnly: true,
                                            anchor: '95%',
                                            labelAlign: 'top',
                                            fieldCls: 'readOnlyText',
                                            fieldLabel: 'Fecha de registro',
                                            name: 'dateRegisterTemp',
                                            itemId: 'dateRegisterTemp',
                                            enforceMaxLength: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            readOnly: true,
                                            value: fullName,
                                            anchor: '95%',
                                            labelAlign: 'top',
                                            fieldCls: 'readOnlyText',
                                            fieldLabel: 'Usuario de registro',
                                            name: 'fullNameUserRegisterTemp',
                                            itemId: 'fullNameUserRegisterTemp',
                                            enforceMaxLength: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            readOnly: true,
                                            value: descriptionWorkArea,
                                            anchor: '95%',
                                            labelAlign: 'top',
                                            fieldCls: 'readOnlyText',
                                            fieldLabel: '&Aacute;rea',
                                            name: 'workAreaRegisterTemp',
                                            itemId: 'workAreaRegisterTemp',
                                            enforceMaxLength: true
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            buttonAlign: 'right',
            buttons: [
                {
                    text: 'Salir',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                },
                {
                    text: 'Guardar',
                    action: 'saveIncidents',
                    scale: 'medium',
                    iconCls: 'save'
                }
            ],
            listeners: {
                show: function (win) {
                    this.heightMain = win.getHeight();
                    me.buttonActive = win.down('#options').items.items[0];
                    me.buttonTextActive = win.down('#options').items.items[0].getText();

                    me.buttonActive.setText(me.buttonActive.getText() + '<span style="color:red"> * </span>')
                },
                close: function (win) {
                    clearInterval(win.timer);
                }
            }
        });
        me.callParent(arguments);
    }

});