Ext.define("ModelSpendingEventLost", {
  extend: "Ext.data.Model",
  fields: [
    "idEvent",
    "amountRecovery",
    "amountTotal",
    "dateRecovery",
    "descriptionLarge",
    "descriptionTypeRecovery",
    "nameCurrency",
    "typeChange",
    "typeRegister",
    "typeRecovery",
    "code"
  ]
});

var storeSpendEventLost = Ext.create("Ext.data.Store", {
  model: "ModelSpendingEventLost",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.WindowSpendEventLost",
  {
    extend: "Ext.window.Window",
    requires: ["DukeSource.view.risk.parameter.combos.ViewComboCurrency"],
    alias: "widget.WindowSpendEventLost",
    height: 450,
    width: 850,
    layout: {
      align: "stretch",
      type: "hbox"
    },
    border: false,
    title: "Gastos relacionados al evento de pérdida",
    initComponent: function() {
      var me = this;
      var recordMainEvent = this.recordMainEvent;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            bodyPadding: 5,
            flex: 0.6,
            fieldDefaults: {
              labelCls: "changeSizeFontToEightPt",
              fieldCls: "changeSizeFontToEightPt"
            },
            items: [
              {
                xtype: "fieldset",
                title: "Datos del " + getName("EVN_CPE_RCLK_Spends"),
                items: [
                  {
                    xtype: "textfield",
                    hidden: true,
                    value: "id",
                    name: "idRecoveryEvent"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    name: "idEvent",
                    itemId: "idEvent"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    name: "numberRelation",
                    itemId: "numberRelation"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    name: "typeRegister",
                    value: DukeSource.global.GiroConstants.TYPE_SPEND,
                    itemId: "typeRegister"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    name: "code",
                    itemId: "code"
                  },
                  {
                    xtype: "datefield",
                    allowBlank: false,
                    msgTarget: "side",
                    name: "dateRecovery",
                    itemId: "dateRecovery",
                    format: "d/m/Y",
                    value: new Date(),
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    fieldLabel: "Fecha",
                    listeners: {
                      expand: function(field) {
                        field.setMinValue(
                          recordMainEvent.get("dateOccurrence")
                        );
                        field.setMaxValue(new Date());
                      },
                      blur: function(field) {
                        field.setMinValue(
                          recordMainEvent.get("dateOccurrence")
                        );
                        field.setMaxValue(new Date());
                      }
                    }
                  },
                  {
                    xtype: "combobox",
                    fieldLabel: "Tipo",
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    displayField: "description",
                    valueField: "value",
                    editable: false,
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    allowBlank: false,
                    name: "typeRecovery",
                    itemId: "typeRecovery",
                    store: {
                      fields: ["value", "description"],
                      pageSize: 9999,
                      autoLoad: true,
                      proxy: {
                        actionMethods: {
                          create: "POST",
                          read: "POST",
                          update: "POST"
                        },
                        type: "ajax",
                        url:
                          "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                        extraParams: {
                          propertyOrder: "description",
                          propertyFind: "identified",
                          valueFind: "TYPESPEND"
                        },
                        reader: {
                          type: "json",
                          root: "data",
                          successProperty: "success"
                        }
                      }
                    }
                  },
                  {
                    xtype: "ViewComboCurrency",
                    fieldLabel: "Moneda",
                    name: "currency",
                    itemId: "currency",
                    fieldCls: "obligatoryTextField",
                    allowBlank: false,
                    listeners: {
                      select: function(cbo) {
                        var typeChange = me.down("#typeChange");
                        if (cbo.getValue() === "1") {
                          typeChange.setValue("1.00");
                          typeChange.setFieldStyle("background: #D8D8D8");
                          typeChange.setReadOnly(true);
                        } else {
                          typeChange.setValue("");
                          typeChange.setFieldStyle("background: #d9ffdb");
                          typeChange.setReadOnly(false);
                        }
                      }
                    }
                  },
                  {
                    xtype: "NumberDecimalNumberObligatory",
                    fieldLabel: "Tipo de cambio",
                    name: "typeChange",
                    itemId: "typeChange",
                    allowBlank: false,
                    maxValue: 9999,
                    forcePrecision: true,
                    decimalPrecision: 4,
                    listeners: {
                      specialkey: function(f, e) {
                        if (e.getKey() === e.ENTER || e.getKey() === e.TAB) {
                          calculateTotalSpend(me);
                          DukeSource.global.DirtyView.focusEventEnter(
                            f,
                            e,
                            me.down("#amountRecovery")
                          );
                        }
                      },
                      blur: function(f, e) {
                        calculateTotalSpend(me);
                      }
                    }
                  },
                  {
                    xtype: "NumberDecimalNumberObligatory",
                    name: "amountRecovery",
                    allowBlank: false,
                    itemId: "amountRecovery",
                    fieldLabel: "Monto",
                    maxLength: 13,
                    listeners: {
                      specialkey: function(f, e) {
                        if (e.getKey() === e.ENTER || e.getKey() === e.TAB) {
                          calculateTotalSpend(me);
                        }
                      },
                      blur: function(f, e) {
                        calculateTotalSpend(me);
                      }
                    }
                  },
                  {
                    xtype: "NumberDecimalNumberObligatory",
                    name: "totalSpend",
                    itemId: "totalSpend",
                    hidden: true,
                    fieldLabel: "Total del gasto",
                    fieldCls: "readOnlyText"
                  },
                  {
                    xtype: "textfield",
                    name: "accountPlan",
                    itemId: "accountPlan",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    anchor: "100%",
                    allowBlank: true,
                    fieldLabel: "Cuenta contable",
                    emptyText: "Buscar cuenta",
                    name: "codeAccount",
                    itemId: "codeAccount",
                    listeners: {
                      specialkey: function(f, e) {
                        if (e.getKey() === e.ENTER) {
                          var win = Ext.create(
                            "DukeSource.view.risk.parameter.windows.ViewWindowSearchPlanAccountCountable",
                            { modal: true }
                          ).show();
                          win.down("UpperCaseTextField").focus(false, 100);
                          win
                            .down("UpperCaseTextField")
                            .setValue(me.down("#codeAccount").getValue());

                          win
                            .down("grid")
                            .on("itemdblclick", function(grid, record) {
                              me.down("#codeAccount").setValue(
                                record.get("codeAccount")
                              );
                              me.down("#accountPlan").setValue(
                                record.get("idAccountPlan")
                              );
                              me.down("#bookkeepingRecovery").focus(false, 100);
                              win.close();
                            });
                        }
                      }
                    }
                  },
                  {
                    xtype: "UpperCaseTextField",
                    anchor: "100%",
                    fieldLabel: "Asiento contable",
                    name: "bookkeepingRecovery",
                    itemId: "bookkeepingRecovery",
                    maxLength: 50
                  },
                  {
                    xtype: "UpperCaseTextArea",
                    anchor: "100%",
                    height: 60,
                    fieldLabel: "Comentarios",
                    name: "descriptionLarge",
                    itemId: "descriptionLarge",
                    maxLength: 250
                  }
                ]
              }
            ],
            tbar: [
              {
                text: "Nuevo",
                scale: "medium",
                cls: "my-btn",
                overCls: "my-over",
                iconCls: "add",
                handler: function() {
                  var form = me.down("form");
                  form.getForm().reset();
                  me.down("#idEvent").setValue(recordMainEvent.get("idEvent"));
                }
              },
              {
                text: "Guardar",
                scale: "medium",
                cls: "my-btn",
                overCls: "my-over",
                iconCls: "save",
                handler: function() {
                  var form = me.down("form");
                  if (form.getForm().isValid()) {
                    Ext.Ajax.request({
                      method: "POST",
                      url:
                        "http://localhost:9000/giro/saveSpendEvent.htm?nameView=ViewPanelEventsRiskOperational",
                      params: {
                        jsonData: Ext.JSON.encode(form.getValues())
                      },
                      success: function(response) {
                        var numberRelation = me
                          .down("#numberRelation")
                          .getValue();
                        response = Ext.decode(response.responseText);

                        if (response.success) {
                          DukeSource.global.DirtyView.messageNormal(
                            response.message
                          );
                          var gridEvent = Ext.ComponentQuery.query(
                            "ViewPanelEventsRiskOperational grid"
                          )[0];

                          var gridSubEvent = Ext.ComponentQuery.query(
                            "ViewWindowRegisterSubEvents grid"
                          )[0];

                          if (gridSubEvent !== undefined) {
                            gridSubEvent.store.getProxy().extraParams = {
                              idEvent: numberRelation
                            };
                            gridSubEvent.store.getProxy().url =
                              "http://localhost:9000/giro/getSubEventsByEvent.htm";
                            gridSubEvent.down("pagingtoolbar").doRefresh();
                          }

                          gridEvent.down("pagingtoolbar").doRefresh();
                          me.down("grid")
                            .down("pagingtoolbar")
                            .doRefresh();
                          form.getForm().reset();
                          me.down("#idEvent").setValue(
                            recordMainEvent.get("idEvent")
                          );
                        } else {
                          DukeSource.global.DirtyView.messageWarning(
                            response.message
                          );
                        }
                      },
                      failure: function() {}
                    });
                  } else {
                    DukeSource.global.DirtyView.messageWarning(
                      DukeSource.global.GiroMessages.MESSAGE_COMPLETE
                    );
                  }
                }
              }
            ]
          },
          {
            xtype: "gridpanel",
            store: storeSpendEventLost,
            flex: 1,
            padding: "0 0 0 2",
            tbar: [
              {
                text: "Eliminar",
                scale: "medium",
                cls: "my-btn",
                overCls: "my-over",
                iconCls: "delete",
                handler: function() {
                  var grid = me.down("grid");
                  var form = me.down("form");
                  var record = grid.getSelectionModel().getSelection()[0];
                  if (record !== undefined) {
                    Ext.MessageBox.show({
                      title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                      msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
                      icon: Ext.Msg.QUESTION,
                      buttonText: {
                        yes: "Si"
                      },
                      buttons: Ext.MessageBox.YESNO,
                      fn: function(btn) {
                        if (btn === "yes") {
                          DukeSource.lib.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/deleteRecoveryEvent.htm?nameView=ViewPanelEventsRiskOperational",
                            params: {
                              jsonData: Ext.JSON.encode(record.raw)
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              var numberRelation = me
                                .down("#numberRelation")
                                .getValue();
                              if (response.success) {
                                DukeSource.global.DirtyView.messageNormal(
                                  response.message
                                );
                                var gridEvent = Ext.ComponentQuery.query(
                                  "ViewPanelEventsRiskOperational grid"
                                )[0];
                                var gridSubEvent = Ext.ComponentQuery.query(
                                  "ViewWindowRegisterSubEvents grid"
                                )[0];

                                if (gridSubEvent !== undefined) {
                                  gridSubEvent.store.getProxy().extraParams = {
                                    idEvent: numberRelation
                                  };
                                  gridSubEvent.store.getProxy().url =
                                    "http://localhost:9000/giro/getSubEventsByEvent.htm";
                                  gridSubEvent
                                    .down("pagingtoolbar")
                                    .doRefresh();
                                }

                                gridEvent.down("pagingtoolbar").doRefresh();
                                me.down("grid")
                                  .down("pagingtoolbar")
                                  .doRefresh();
                                form.getForm().reset();
                                me.down("#idEvent").setValue(
                                  recordMainEvent.get("idEvent")
                                );
                              } else {
                                DukeSource.global.DirtyView.messageWarning(
                                  response.message
                                );
                              }
                            },
                            failure: function() {}
                          });
                        }
                      }
                    });
                  } else {
                    DukeSource.global.DirtyView.messageWarning(
                      DukeSource.global.GiroMessages.MESSAGE_ITEM
                    );
                  }
                }
              }
            ],
            columns: [
              {
                dataIndex: "code",
                align: "center",
                hidden: hidden("EVN_WSL_Code"),
                text: "ID",
                width: 95,
                renderer: function(value) {
                  return "<b>" + value + "</b>";
                }
              },
              {
                dataIndex: "descriptionTypeRecovery",
                align: "center",
                text: "Tipo",
                width: 95,
                renderer: function(value, metaData) {
                  metaData.tdAttr =
                    'style="background-color: #60ff97 !important;border:1px solid #60ff97 !important;"';
                  return (
                    '<div style="background-color:#60ff97;">' + value + "</div>"
                  );
                }
              },
              {
                dataIndex: "dateRecovery",
                text: "Fecha",
                align: "center",
                width: 90
              },
              {
                dataIndex: "typeChange",
                width: 60,
                align: "center",
                text: "Tipo de cambio"
              },
              {
                dataIndex: "amountRecovery",
                width: 100,
                xtype: "numbercolumn",
                format: "0,0.00",
                align: "center",
                text: "Monto original"
              },
              {
                dataIndex: "amountTotal",
                width: 100,
                xtype: "numbercolumn",
                format: "0,0.00",
                align: "center",
                text: "Monto total"
              },
              {
                header: "Comentarios",
                width: 200,
                align: "left",
                dataIndex: "descriptionLarge"
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: storeSpendEventLost,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            },
            listeners: {
              itemclick: function(grid, record) {
                me.down("#currency")
                  .getStore()
                  .load();
                me.down("form")
                  .getForm()
                  .setValues(record.raw);
                me.down("#totalSpend").setValue(record.raw["amountTotal"]);
              }
            }
          }
        ],
        buttons: [
          {
            text: "Salir",
            scale: "medium",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      });
      me.callParent(arguments);
    }
  }
);

function calculateTotalSpend(me) {
  var typeChange = me.down("#typeChange").getValue();
  var amountOrigin = me.down("#amountRecovery").getValue();
  var result = amountOrigin * typeChange;
  me.down("#totalSpend").setValue(result);
}
