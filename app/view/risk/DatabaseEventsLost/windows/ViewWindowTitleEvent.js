Ext.define("ModelTitleEvent", {
  extend: "Ext.data.Model",
  fields: ["id", "description"]
});

var storeTitleEvent = Ext.create("Ext.data.Store", {
  model: "ModelTitleEvent",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTitleEvent",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowTitleEvent",
    layout: {
      align: "stretch",
      type: "vbox"
    },
    anchorSize: 100,
    title: "Buscar t&iacute;tulo de evento",
    titleAlign: "center",
    width: 900,
    height: 500,
    modal: true,
    tbar: [
      {
        text: "Nuevo",
        iconCls: "add",
        scale: "medium",
        cls: "my-btn",
        overCls: "my-over",
        handler: function() {
          Ext.create(
            "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTitleEventEntry",
            { modal: true }
          ).show();
        }
      }
    ],
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            height: 45,
            padding: 2,
            bodyPadding: 10,
            items: [
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "combobox",
                    value: "2",
                    labelWidth: 40,
                    width: 190,
                    fieldLabel: "Tipo",
                    store: [
                      ["1", "C&oacute;digo"],
                      ["2", "Descripci&oacute;n"]
                    ]
                  },
                  {
                    xtype:"UpperCaseTextField",
                    flex: 2,
                    listeners: {
                      afterrender: function(field, e) {
                        field.focus(false, 100);
                      },
                      specialkey: function(field, e) {
                        var property = "";
                        if (
                          Ext.ComponentQuery.query(
                            "ViewWindowTitleEvent combobox"
                          )[0].getValue() === "1"
                        ) {
                          property = "id";
                        } else {
                          property = "description";
                        }
                        if (e.getKey() === e.ENTER) {
                          var grid = me.down("grid");
                          var toolbar = grid.down("pagingtoolbar");
                          DukeSource.global.DirtyView.searchPaginationGridToEnter(
                            field,
                            grid,
                            toolbar,
                            "http://localhost:9000/giro/findEventTittle.htm",
                            property,
                            "id"
                          );
                        }
                      }
                    }
                  }
                ]
              }
            ]
          },
          {
            xtype: "container",
            flex: 3,
            padding: 2,
            layout: {
              align: "stretch",
              type: "hbox"
            },
            items: [
              {
                xtype: "gridpanel",
                padding: "0 1 0 0",
                store: storeTitleEvent,
                flex: 1,
                titleAlign: "center",
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    dataIndex: "id",
                    width: 60,
                    text: "C&oacute;digo"
                  },
                  {
                    dataIndex: "description",
                    flex: 1,
                    text: "Descripci&oacute;n"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: storeTitleEvent,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  render: function() {
                    var me = this;
                    DukeSource.global.DirtyView.searchPaginationGridNormal(
                      "",
                      me,
                      me.down("pagingtoolbar"),
                      "http://localhost:9000/giro/showListEventTittleActives.htm",
                      "description",
                      "id"
                    );
                  }
                }
              }
            ]
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
