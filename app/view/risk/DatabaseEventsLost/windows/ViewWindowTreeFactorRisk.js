Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeFactorRisk', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowTreeFactorRisk',
    requires: [
        'Ext.tree.Panel',
        'DukeSource.view.risk.DatabaseEventsLost.treepanel.TreePanelFactorRisk',
        'Ext.tree.View'
    ],
    title: 'Factores',
    border: false,
    titleAlign: 'center',
    layout: 'fit',
    modal: true,
    height: 570,
    width: 900,
    initComponent: function () {
        var me = this;
        var winParent = this.winParent;
        var valueNode;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'TreePanelFactorRisk',
                    listeners: {
                        select: function (view, record) {
                            valueNode = record;
                            if (record.data['depth'] === 1) {
                                me.down('#selectFactor').setDisabled(true);
                            } else {
                                me.down('#selectFactor').setDisabled(false);
                            }
                        },
                        itemdblclick: function (view, record) {
                            if (record.data['depth'] === 1) {
                                DukeSource.global.DirtyView.messageWarning('Factor no permitido, seleccione otro');
                            } else {
                                var description = record.getPath('text', ' &#8702; ');
                                winParent.down('#descriptionFactorRisk').setValue(description.substring(22));
                                winParent.down('#descriptionFactorRisk').setFieldStyle("color:#0000EE");
                                winParent.down('#factorRisk').setValue(record.raw.id);
                                me.close();
                            }
                        }
                    }
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    iconCls: 'save',
                    itemId: 'selectFactor',
                    scale: 'medium',
                    handler: function () {
                        if (valueNode !== undefined) {
                            var tree = me.down('treepanel');
                            var node = tree.getSelectionModel().getSelection()[0];
                            var description = node.getPath('text', ' &#8702; ');
                            winParent.down('#descriptionFactorRisk').setFieldStyle("color:#0000EE");
                            winParent.down('#descriptionFactorRisk').setValue(description.substring(22));
                            winParent.down('#factorRisk').setValue(node.raw.id);
                            me.close();
                        } else {
                            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM_CHECK);
                        }


                    }
                },
                {
                    text: 'Salir',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }
});
