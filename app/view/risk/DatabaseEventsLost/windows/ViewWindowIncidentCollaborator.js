Ext.require(['Ext.ux.DateTimeField']);
Ext.define("DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowIncidentCollaborator", {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowIncidentCollaborator",
    width: 800,
    layout: {
        type: "fit"
    },
    title: "INCIDENTE",
    border: false,
    titleAlign: "center",
    initComponent: function () {
        var a = this;
        var b = this.typeIncident;
        Ext.applyIf(a, {
            items: [{
                xtype: "form",
                itemId: "generalForm",
                bodyPadding: 10,
                fieldDefaults: {
                    labelCls: 'changeSizeFontToEightPt',
                    fieldCls: 'changeSizeFontToEightPt'
                },
                items: [
                    {
                        xtype: "textfield",
                        hidden: true,
                        value: "id",
                        name: "idDetailIncidents"
                    },
                    {
                        xtype: "datefield",
                        hidden: true,
                        name: "dateProcess",
                        format: 'd/m/Y',
                        itemId: "dateProcess"
                    },
                    {
                        xtype: 'textfield',
                        hidden: true,
                        value: 'N',
                        name: 'fileAttachment',
                        itemId: 'fileAttachment'
                    },
                    {
                        xtype: 'textfield',
                        hidden: true,
                        name: 'userRegister',
                        itemId: 'userRegister'
                    },
                    {
                        xtype: 'radiogroup',
                        itemId: 'typeIncident',
                        hidden: hidden('IWRGTypeIncident'),
                        margin: '0 0 0 120',
                        labelWidth: 140,
                        style: 'font-weight: bold;',
                        items: [
                            {
                                xtype: 'radiofield',
                                boxLabelCls: 'changeSizeFontToEightPt',
                                name: 'typeIncident',
                                style: 'font-weight: bold;',
                                inputValue: "RO",
                                checked: true,
                                boxLabel: ' Riesgo operacional'
                            },
                            {
                                xtype: 'radiofield',
                                boxLabelCls: 'changeSizeFontToEightPt',
                                name: 'typeIncident',
                                style: 'font-weight: bold;',
                                inputValue: "SI",
                                boxLabel: ' Seguridad de información'
                            },
                            {
                                xtype: 'radiofield',
                                boxLabelCls: 'changeSizeFontToEightPt',
                                name: 'typeIncident',
                                style: 'font-weight: bold;',
                                inputValue: "CN",
                                boxLabel: ' Continuidad del negocio'
                            }
                        ]
                    },
                    {
                        xtype:"UpperCaseTextField",
                        allowBlank: false,
                        fieldLabel: "Descripci&oacute;n corta",
                        maxLength: 300,
                        msgTarget: "side",
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        name: "descriptionShort",
                        fieldCls: "obligatoryTextField",
                        anchor: "100%",
                        labelWidth: 120,
                        listeners: {
                            specialkey: function (c, d) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(c, d, a.down("#descriptionLarge"))
                            }
                        }
                    },
                    {
                        xtype: 'UpperCaseTextArea',
                        allowBlank: false,
                        fieldCls: "obligatoryTextField",
                        labelWidth: 120,
                        height: 50,
                        maxLength: 3000,
                        msgTarget: "side",
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        anchor: "100%",
                        fieldLabel: "Descripci&oacute;n larga",
                        name: "descriptionLarge",
                        itemId: "descriptionLarge",
                        listeners: {
                            specialkey: function (c, d) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(c, d, a.down("#workArea"))
                            }
                        }
                    },
                    {
                        xtype: "container",
                        layout: {
                            type: "hbox"
                        },
                        items: [{
                            xtype: "container",
                            flex: 1.2,
                            layout: {
                                type: "anchor"
                            },
                            name: "containerRight",
                            items: [
                                {
                                    xtype: "combobox",
                                    anchor: "100%",
                                    fieldLabel: "Area origen",
                                    name: "workArea",
                                    itemId: "workArea",
                                    plugins: ['ComboSelectCount'],
                                    labelWidth: 120,
                                    displayField: "description",
                                    valueField: "idWorkArea",
                                    store: {
                                        fields: ["idWorkArea", "description"],
                                        proxy: {
                                            actionMethods: {
                                                create: "POST",
                                                read: "POST",
                                                update: "POST"
                                            },
                                            type: "ajax",
                                            url: "showListWorkAreaActivesComboBox.htm",
                                            extraParams: {
                                                propertyOrder: "description"
                                            },
                                            reader: {
                                                type: "json",
                                                root: "data",
                                                successProperty: "success"
                                            }
                                        }
                                    },
                                    listeners: {
                                        specialkey: function (c, d) {
                                            DukeSource.global.DirtyView.focusEventEnterObligatory(c, d, a.down("#agency"))
                                        }
                                    }
                                },
                                {
                                    xtype: "combobox",
                                    allowBlank: blank('INC_WIR_CBX_Agency'),
                                    fieldCls: styleField('INC_WIR_CBX_Agency'),
                                    msgTarget: "side",
                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                    anchor: "100%",
                                    fieldLabel: "Agencia",
                                    name: "agency",
                                    itemId: "agency",
                                    plugins: ['ComboSelectCount'],
                                    labelWidth: 120,
                                    displayField: "description",
                                    valueField: "idAgency",
                                    store: {
                                        fields: ["idAgency", "description"],
                                        pageSize: 999,
                                        proxy: {
                                            actionMethods: {
                                                create: "POST",
                                                read: "POST",
                                                update: "POST"
                                            },
                                            type: "ajax",
                                            url: "showListAgencyActivesComboBox.htm",
                                            extraParams: {
                                                propertyOrder: "a.description"
                                            },
                                            reader: {
                                                type: "json",
                                                root: "data",
                                                successProperty: "success"
                                            }
                                        }
                                    },
                                    listeners: {
                                        specialkey: function (c, d) {
                                            DukeSource.global.DirtyView.focusEventEnterObligatory(c, d, a.down("#dateOccurrence"))
                                        }
                                    }
                                },
                                {
                                    xtype: "datetimefield",
                                    allowBlank: false,
                                    format: "d/m/Y H:i",
                                    msgTarget: "side",
                                    fieldCls: "obligatoryTextField",
                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                    fieldLabel: "Fecha ocurrencia",
                                    name: "dateOccurrence",
                                    itemId: "dateOccurrence",
                                    labelWidth: 120,
                                    listeners: {
                                        expand: function (d, c) {
                                            if (a.down("#dateDiscovery").getValue() !== undefined) {
                                                d.setMaxValue(a.down("#dateDiscovery").getValue())
                                            } else {
                                                d.setMaxValue(new Date())
                                            }
                                        },
                                        specialkey: function (c, d) {
                                            DukeSource.global.DirtyView.focusEventEnterObligatory(c, d, a.down("#dateFinal"))
                                        }
                                    }
                                },
                                {
                                    xtype: "datetimefield",
                                    allowBlank: blank('INC_WIR_DTF_DateDiscovery'),
                                    fieldCls: styleField('INC_WIR_DTF_DateDiscovery'),
                                    format: "d/m/Y H:i",
                                    msgTarget: "side",
                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                    fieldLabel: "Fec. descubrimiento",
                                    name: "dateDiscovery",
                                    itemId: "dateDiscovery",
                                    labelWidth: 120,
                                    listeners: {
                                        expand: function (d, c) {
                                            d.setMinValue(a.down("#dateOccurrence").getRawValue());
                                            d.setMaxValue(new Date())
                                        }
                                    }
                                },
                                {
                                    xtype: "datetimefield",
                                    format: "d/m/Y H:i",
                                    msgTarget: "side",
                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                    fieldLabel: "Fecha fin",
                                    name: "dateFinal",
                                    itemId: "dateFinal",
                                    labelWidth: 120,
                                    listeners: {
                                        expand: function (d, c) {
                                            d.setMinValue(a.down("#dateOccurrence").getRawValue());
                                            d.setMaxValue(new Date())
                                        }
                                    }
                                }
                            ]
                        }, {
                            xtype: "container",
                            flex: 1,
                            padding: "0 0 0 10",
                            name: "containerLeft",
                            itemId: 'containerLeft',
                            layout: {
                                type: "anchor"
                            },
                            items: [
                                {
                                    xtype: "checkbox",
                                    labelWidth: 105,
                                    fieldLabel: "Confidencial",
                                    name: "confidential",
                                    itemId: 'confidential',
                                    inputValue: "S",
                                    uncheckedValue: "N",
                                    hidden: hidden('IWCBConfidential'),
                                    checked: false,
                                    anchor: '100%',
                                    listeners: {
                                        specialkey: function (c, d) {
                                            DukeSource.global.DirtyView.focusEventEnterObligatory(c, d, a.down("button[text=REPORTAR]"))
                                        },
                                        change: function (c) {
                                            if (c.checked == true) {
                                                c.up("form").setBodyStyle("background", "#FFDC9E")
                                            } else {
                                                c.up("form").setBodyStyle("background", "#FFF")
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'UpperCaseTextArea',
                                    fieldLabel: "Acci&oacute;n tomada",
                                    name: "causeCollaborator",
                                    itemId: "causeCollaborator",
                                    labelWidth: 105,
                                    height: 52,
                                    allowBlank: blank('IWCCauseCollaborator'),
                                    fieldCls: styleField('IWCCauseCollaborator'),
                                    maxLength: 600,
                                    msgTarget: "side",
                                    anchor: "100%",
                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                    listeners: {
                                        specialkey: function (c, d) {
                                            DukeSource.global.DirtyView.focusEventEnterObligatory(c, d, a.down("checkbox[name=confidential]"))
                                        }
                                    }
                                },
                                {
                                    xtype: "fileuploadfield",
                                    labelWidth: 105,
                                    fieldLabel: "Subir documento",
                                    name: "document",
                                    itemId: 'document',
                                    anchor: "100%",
                                    buttonConfig: {
                                        iconCls: 'tesla even-attachment'
                                    }
                                }]
                        }]
                    }]
            }],
            buttonAlign: 'center',
            buttons: [{
                text: "Reportar",
                scale: 'medium',
                action: "savePreIncidents",
                iconCls: "send"
            }, {
                text: "Salir",
                scope: this,
                handler: this.close,
                scale: 'medium',
                iconCls: "logout"
            }]
        });
        a.callParent(arguments)
    }
});
