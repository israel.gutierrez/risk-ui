Ext.define("ModelTreeGridPanelUserAndDate", {
  extend: "Ext.data.Model",
  fields: [
    { name: "id", type: "string" },
    { name: "text", type: "string" },
    { name: "fieldOne", type: "string" },
    { name: "fieldTwo", type: "string" }
  ]
});
//var storeUserActives = Ext.create('Ext.data.Store', {
//    model: 'ModelUserActives',
//    proxy: {
//        actionMethods: {
//            create: 'POST',
//            read: 'POST',
//            update: 'POST'
//        },
//        type: 'ajax',
//        url: 'http://localhost:9000/giro/listCollaboratorAll.htm',
//        reader: {
//            totalProperty: 'totalCount',
//            root: 'data',
//            successProperty: 'success'
//        }
//    }
//});
var storeTreeGridPanelUserAndDate = Ext.create("Ext.data.TreeStore", {
  //    Ext.define('DukeSource.store.risk.DatabaseEventsLost.grids.StoreTreeGridPanelIncidentsRiskOperational',{
  //    extend: 'Ext.data.TreeStore',
  model: "ModelTreeGridPanelUserAndDate",
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/listCollaboratorAll.htm"
  },
  root: {
    text: "Tree display of Countries",
    id: "myTree",
    expanded: true
  },
  folderSort: true,
  sorters: [
    {
      property: "text",
      direction: "ASC"
    }
  ],
  listeners: {
    load: function(tree, node, records) {
      if (node.get("checked")) {
        node.eachChild(function(childNode) {
          childNode.set("checked", true);
        });
      }
    }
  }
});
Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowSearchByUserAndDate",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowSearchByUserAndDate",
    height: 350,
    width: 600,
    layout: {
      type: "fit"
    },
    title: "BUSQUEDA POR DEPENDIENTES",

    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        dockedItems: [
          {
            xtype: "toolbar",
            dock: "top",
            items: [
              {
                xtype: "datefield",
                format: "d/m/Y",
                width: 210,
                fieldLabel: "FECHA INICIO"
              },
              {
                xtype: "datefield",
                format: "d/m/Y",
                width: 210,
                fieldLabel: "FECHA FIN"
              },
              "-",
              {
                xtype: "button",
                iconCls: "search",
                text: "BUSCAR"
              },
              "-"
            ]
          }
        ],
        items: [
          {
            xtype: "treepanel",
            store: storeTreeGridPanelUserAndDate,
            border: false,
            useArrows: true,
            multiSelect: true,
            singleExpand: true,
            rootVisible: false,
            columns: [
              {
                //treecolumn xtype tells the Grid which column will show the tree
                xtype: "treecolumn",
                text: "HISTORICO",
                flex: 2,
                sortable: true,
                dataIndex: "text"
              },
              {
                text: "CATEGORIA",
                flex: 1,
                sortable: true,
                dataIndex: "fieldOne",
                align: "center"
              }
            ],
            listeners: {
              itemclick: function(view, rec) {
                var grid = Ext.ComponentQuery.query(
                  "ViewPanelIncidentsRiskOperational ViewGridIncidentsRiskOperational"
                )[0];
                if (me.down("datefield").getRawValue() == "") {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_WARNING,
                    "Seleccione una Fecha por Favor",
                    Ext.Msg.WARNING
                  );
                } else {
                  grid.store.getProxy().extraParams = {
                    dateIncidens: me.down("datefield").getRawValue(),
                    category: rec.get("fieldOne"),
                    userName: rec.get("fieldTwo")
                  };
                  grid.store.getProxy().url =
                    "http://localhost:9000/giro/getDetailIncidentsByDateAndGestor.htm";
                  grid.down("pagingtoolbar").moveFirst();
                  Ext.ComponentQuery.query(
                    "ViewPanelIncidentsRiskOperational datefield"
                  )[0].setValue(me.down("datefield").getRawValue());
                  Ext.ComponentQuery.query(
                    "ViewPanelIncidentsRiskOperational UpperCaseTextFieldReadOnly[name=userName]"
                  )[0].setValue(rec.get("text"));
                  //                                  Ext.Ajax.request({
                  ////                                      waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                  //                                      method: 'POST',
                  //                                      url: 'http://localhost:9000/giro/getDescriptionIncidents.htm',
                  //                                      params: {
                  //                                          dateIncident: me.down('datefield').getRawValue(),
                  //                                          username:rec.get('fieldTwo')
                  //                                      },
                  //                                      success:function(response){
                  //                                          response = Ext.decode(response.responseText);
                  //                                          if (response.success) {
                  //                                              Ext.ComponentQuery.query('ViewPanelIncidentsRiskOperational 'UpperCaseTextArea'[name=descriptionIncidents]')[0].setValue(response.data);
                  //
                  ////                                              DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                  ////                                              grid.store.getProxy().extraParams = {idKeyRiskIndicator:grid1.getSelectionModel().getSelection()[0].get('idKeyRiskIndicator')};
                  ////                                              grid.store.getProxy().url = 'http://localhost:9000/giro/findWeighingKri.htm';
                  //////                    grid.down('pagingtoolbar').moveFirst();
                  ////                                              panel.close();
                  //
                  //
                  //                                          } else {
                  //                                              DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_ERROR,response.mensaje,Ext.Msg.ERROR);
                  //                                          }
                  //                                      },
                  //                                      failure: function () {
                  //                                      }
                  //                                  });
                }
              }
            }
          }
        ],
        buttons: [
          {
            text: "SALIR",
            iconCls: "logout",
            scope: this,
            handler: this.close
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
