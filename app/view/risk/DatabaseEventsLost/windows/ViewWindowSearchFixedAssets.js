Ext.define("ModelSearchFixedAssets", {
  extend: "Ext.data.Model",
  fields: ["idFixedAssets", "codeAssets", "description"]
});

var storeSearchFixedAssets = Ext.create("Ext.data.Store", {
  model: "ModelSearchFixedAssets",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowSearchFixedAssets",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowSearchFixedAssets",
    layout: {
      align: "stretch",
      type: "vbox"
    },
    anchorSize: 100,
    title: "BUSCAR ACTIVO",
    titleAlign: "center",
    width: 800,
    height: 500,
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            height: 45,
            padding: "2 2 2 2",
            bodyPadding: 10,
            items: [
              {
                xtype: "container",
                anchor: "100%",
                height: 26,
                layout: {
                  type: "hbox"
                },
                items: [
                  {
                    xtype: "combobox",
                    value: "2",
                    labelWidth: 40,
                    width: 190,
                    //                                    flex:1,
                    fieldLabel: "TIPO",
                    store: [
                      ["1", "CODIGO"],
                      ["2", "DESCRIPCION"]
                    ]
                  },
                  {
                    xtype:"UpperCaseTextField",
                    flex: 2,
                    listeners: {
                      afterrender: function(field) {
                        field.focus(false, 200);
                      },
                      specialkey: function(field, e) {
                        var property = "";
                        if (
                          Ext.ComponentQuery.query(
                            "ViewWindowSearchFixedAssets combobox"
                          )[0].getValue() == "1"
                        ) {
                          property = "codeAssets";
                        } else {
                          property = "description";
                        }
                        if (e.getKey() === e.ENTER) {
                          var grid = me.down("grid");
                          DukeSource.global.DirtyView.searchPaginationGridToEnter(
                            field,
                            grid,
                            grid.down("pagingtoolbar"),
                            "http://localhost:9000/giro/findFixedAssets.htm",
                            property,
                            "description"
                          );
                        }
                      }
                    }
                  }
                ]
              }
            ]
          },
          {
            xtype: "container",
            flex: 3,
            padding: "2 2 2 2",
            layout: {
              align: "stretch",
              type: "hbox"
            },
            items: [
              {
                xtype: "gridpanel",
                padding: "0 1 0 0",
                store: storeSearchFixedAssets,
                flex: 1,
                titleAlign: "center",
                columns: [
                  {
                    xtype: "rownumberer",
                    width: 25,
                    sortable: false
                  },
                  {
                    dataIndex: "codeAssets",
                    width: 120,
                    text: "CODIGO"
                  },
                  {
                    dataIndex: "description",
                    flex: 1,
                    text: "ACTIVO DE INFORMACION"
                  }
                ],
                bbar: {
                  xtype: "pagingtoolbar",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: storeSearchFixedAssets,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                },
                listeners: {
                  render: function() {
                    var me = this;
                    DukeSource.global.DirtyView.searchPaginationGridNormal(
                      "",
                      me,
                      me.down("pagingtoolbar"),
                      "http://localhost:9000/giro/findFixedAssets.htm",
                      "description",
                      "description"
                    );
                  }
                }
              }
            ]
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
