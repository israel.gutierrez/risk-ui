function disableFieldsOfIncidents(me) {
  me.query(".textfield,.numberfield").forEach(function(c) {
    c.setFieldStyle("background: #FFF");
    c.allowBlank = true;
  });
  me.down("#idIncident").allowBlank = true;
  me.down("#idIncident").setFieldStyle(
    "border: 1px solid #ff524f;background: #ffd6d5;text-align: center"
  );
  me.down("#descriptionShort").allowBlank = false;
  me.down("#descriptionShort").setFieldStyle("background: #d9ffdb");
  me.down("#descriptionLarge").allowBlank = false;
  me.down("#descriptionLarge").setFieldStyle("background: #d9ffdb");
  me.down("#workArea").allowBlank = false;
  me.down("#workArea").setFieldStyle("background: #d9ffdb");
  me.down("#agency").allowBlank = false;
  me.down("#agency").setFieldStyle("background: #d9ffdb");
  me.down("#originIncident").allowBlank = false;
  me.down("#originIncident").setFieldStyle("background: #d9ffdb");
  me.down("#dateOccurrence").allowBlank = false;
  me.down("#dateOccurrence").setFieldStyle("background: #d9ffdb");
  me.down("#dateDiscovery").allowBlank = false;
  me.down("#dateDiscovery").setFieldStyle("background: #d9ffdb");
}

function enabledFieldsIncidents(me) {
  me.query(".textfield,.numberfield").forEach(function(c) {
    c.setFieldStyle("background: #d9ffdb");
    c.allowBlank = true;
  });
  me.down("#idIncident").allowBlank = true;
  me.down("#idIncident").setFieldStyle(
    "border: 1px solid #ff524f;background: #ffd6d5;text-align: center"
  );
  me.down("#fileAttachment").allowBlank = true;
  me.down("#document").allowBlank = true;
  me.down("#document").setFieldStyle("background: #FFF");
  me.down("#causeCollaborator").allowBlank = true;
  me.down("#causeCollaborator").setFieldStyle("background: #FFF");
  if (me.down("#lostType").getValue() == "2") {
    var amountInt = me.down("#amountLoss");
    var currency = me.down("#currency");
    amountInt.setValue("");
    amountInt.allowBlank = true;
    amountInt.setEditable(false);
    currency.allowBlank = true;
    currency.setFieldStyle("background: #FFF");
    amountInt.setFieldStyle("background: #FFF");
  }
}

Ext.require(["Ext.ux.DateTimeField"]);
Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterIncidentsCN",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowRegisterIncidentsCN",
    layout: {
      type: "fit"
    },
    width: 880,
    title: "INCIDENTE DE CONTINUIDAD DEL NEGOCIO",
    border: false,
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      var typeIncident = this.typeIncident;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            fieldDefaults: {
              labelCls: "changeSizeFontToEightPt",
              fieldCls: "changeSizeFontToEightPt"
            },
            autoScroll: true,
            bodyPadding: 3,
            items: [
              {
                xtype: "textfield",
                hidden: true,
                name: "idDetailIncidents"
              },
              {
                xtype: "textfield",
                hidden: true,
                itemId: "collaborator",
                name: "collaborator"
              },
              {
                xtype: "textfield",
                hidden: true,
                name: "fileAttachment",
                itemId: "fileAttachment"
              },
              {
                xtype: "textfield",
                hidden: true,
                name: "userRegister",
                itemId: "userRegister"
              },
              {
                xtype: "datefield",
                format: "d/m/Y",
                hidden: true,
                itemId: "dateProcess",
                name: "dateProcess"
              },
              {
                xtype: "textfield",
                hidden: true,
                itemId: "commentManager",
                name: "commentManager"
              },
              {
                xtype: "textfield",
                hidden: true,
                itemId: "descriptionIndicatorIncidents",
                name: "descriptionIndicatorIncidents"
              },
              {
                xtype: "textfield",
                hidden: true,
                name: "numberRisk",
                itemId: "numberRisk",
                value: "0"
              },
              {
                xtype: "textfield",
                hidden: true,
                name: "codesRisk",
                itemId: "codesRisk"
              },
              {
                xtype: "textfield",
                hidden: true,
                name: "generateLoss",
                itemId: "generateLoss",
                value: "N"
              },
              {
                xtype: "textfield",
                hidden: true,
                name: "sequenceStateIncident",
                itemId: "sequence"
              },
              {
                xtype: "textfield",
                hidden: true,
                value: DukeSource.global.GiroConstants.CONTINUITY,
                name: "typeIncident"
              },
              {
                xtype: "container",
                layout: "hbox",
                height: 27,
                items: [
                  {
                    xtype: "UpperCaseTextField",
                    allowBlank: false,
                    fieldLabel: "DESCRIPCI&Oacute;N CORTA",
                    maxLength: 300,
                    padding: "0 5 0 0",
                    msgTarget: "side",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    name: "descriptionShort",
                    itemId: "descriptionShort",
                    fieldCls: "obligatoryTextField",
                    flex: 4,
                    labelWidth: 120,
                    listeners: {
                      specialkey: function(f, e) {
                        DukeSource.global.DirtyView.focusEventEnterObligatory(
                          f,
                          e,
                          me.down("UpperCaseTextArea[name=descriptionLarge]")
                        );
                      }
                    }
                  },
                  {
                    xtype: "textfield",
                    fieldLabel: "CODIGO",
                    readOnly: true,
                    padding: "0 0 0 8",
                    labelWidth: 80,
                    flex: 1.55,
                    fieldCls: "codeIncident",
                    name: "idIncident",
                    itemId: "idIncident"
                  }
                ]
              },
              {
                xtype: "UpperCaseTextArea",
                allowBlank: false,
                fieldCls: "obligatoryTextField",
                labelWidth: 120,
                height: 50,
                maxLength: 3000,
                msgTarget: "side",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                anchor: "100%",
                fieldLabel: "DESCRIPCI&Oacute;N LARGA",
                name: "descriptionLarge",
                itemId: "descriptionLarge",
                listeners: {
                  specialkey: function(f, e) {
                    DukeSource.global.DirtyView.focusEventEnterObligatory(
                      f,
                      e,
                      me.down("#stateIncident")
                    );
                  }
                }
              },
              {
                xtype: "container",
                itemId: "containerGeneral",
                anchor: "100%",
                layout: {
                  align: "stretch",
                  type: "vbox"
                },
                items: [
                  {
                    xtype: "container",
                    name: "containerParams",
                    itemId: "containerParams",
                    layout: {
                      type: "hbox",
                      align: "stretch"
                    },
                    items: [
                      {
                        xtype: "container",
                        flex: 1,
                        layout: "anchor",
                        items: [
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            anchor: "100%",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "stateIncident",
                            itemId: "stateIncident",
                            displayField: "description",
                            valueField: "id",

                            editable: false,
                            forceSelection: true,
                            fieldLabel: "PROCESAR COMO",
                            labelWidth: 120,
                            msgTarget: "side",
                            store: {
                              fields: ["id", "description", "sequence"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url: "findStateIncident.htm",
                                extraParams: {
                                  propertyFind: "si.typeIncident",
                                  valueFind:
                                    DukeSource.global.GiroConstants.SECURITY,
                                  propertyOrder: "si.sequence"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#workArea")
                                );
                              },
                              select: function(cbo) {
                                var record = cbo.findRecord(
                                  cbo.valueField || cbo.displayField,
                                  cbo.getValue()
                                );
                                var index = cbo.store.indexOf(record);
                                me.down("#sequence").setValue(
                                  cbo.store.data.items[index].data.sequence
                                );
                                if (
                                  cbo.store.data.items[index].data.sequence == 0
                                ) {
                                  disableFieldsOfIncidents(me);
                                  me.down("#comments").allowBlank = false;
                                  me.down("#comments").setFieldStyle(
                                    "background: #d9ffdb"
                                  );
                                } else {
                                  enabledFieldsIncidents(me);
                                }
                              }
                            }
                          },
                          {
                            xtype: "combobox",
                            anchor: "100%",
                            fieldLabel: "AREA ORIGEN",
                            name: "workArea",
                            itemId: "workArea",
                            labelWidth: 120,
                            displayField: "description",
                            valueField: "idWorkArea",

                            editable: false,
                            plugins: ["ComboSelectCount"],
                            store: {
                              fields: ["idWorkArea", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url: "showListWorkAreaActivesComboBox.htm",
                                extraParams: {
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#agency")
                                );
                              }
                            }
                          },
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            msgTarget: "side",
                            fieldCls: "obligatoryTextField",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            anchor: "100%",
                            fieldLabel: "AGENCIA ORIGEN",
                            itemId: "agency",
                            name: "agency",
                            labelWidth: 120,
                            displayField: "description",
                            valueField: "idAgency",
                            editable: false,
                            plugins: ["ComboSelectCount"],

                            store: {
                              fields: ["idAgency", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url: "showListAgencyActivesComboBox.htm",
                                extraParams: {
                                  propertyOrder: "a.description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("datefield[name=dateOccurrence]")
                                );
                              }
                            }
                          },
                          {
                            xtype: "datetimefield",
                            allowBlank: false,
                            format: "d/m/Y H:i",
                            msgTarget: "side",
                            labelWidth: 120,
                            fieldCls: "obligatoryTextField",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            anchor: "100%",
                            fieldLabel: "FECHA OCURRENCIA",
                            name: "dateOccurrence",
                            itemId: "dateOccurrence",
                            listeners: {
                              expand: function(field, value) {
                                if (
                                  me
                                    .down("datefield[name=dateDiscovery]")
                                    .getValue() != undefined
                                ) {
                                  field.setMaxValue(
                                    me
                                      .down("datefield[name=dateDiscovery]")
                                      .getValue()
                                  );
                                } else {
                                  field.setMaxValue(new Date());
                                }
                              },
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("datefield[name=dateFinal]")
                                );
                              }
                            }
                          },
                          {
                            xtype: "datetimefield",
                            allowBlank: false,
                            format: "d/m/Y H:i",
                            msgTarget: "side",
                            labelWidth: 120,
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            anchor: "100%",
                            fieldLabel: "FECHA FIN",
                            name: "dateFinal",
                            itemId: "dateFinal",
                            listeners: {
                              expand: function(field, value) {
                                if (
                                  me
                                    .down("datefield[name=dateDiscovery]")
                                    .getValue() != undefined
                                ) {
                                  field.setMaxValue(
                                    me
                                      .down("datefield[name=dateDiscovery]")
                                      .getValue()
                                  );
                                } else {
                                  field.setMaxValue(new Date());
                                }
                              },
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("datefield[name=dateDiscovery]")
                                );
                              }
                            }
                          },
                          {
                            xtype: "datetimefield",
                            allowBlank: false,
                            format: "d/m/Y H:i",
                            msgTarget: "side",
                            fieldCls: "obligatoryTextField",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            anchor: "100%",
                            labelWidth: 120,
                            fieldLabel: "FECHA DESCUBRIMIENT",
                            name: "dateDiscovery",
                            itemId: "dateDiscovery",
                            listeners: {
                              expand: function(field, value) {
                                field.setMinValue(
                                  me
                                    .down("datefield[name=dateOccurrence]")
                                    .getRawValue()
                                );
                                field.setMaxValue(new Date());
                              },
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#factorRisk")
                                );
                              }
                            }
                          },
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            anchor: "100%",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "factorRisk",
                            itemId: "factorRisk",
                            displayField: "description",
                            valueField: "idFactorRisk",

                            msgTarget: "side",
                            fieldLabel: "FACTOR RIESGO",
                            labelWidth: 120,
                            editable: false,
                            forceSelection: true,
                            store: {
                              fields: ["idFactorRisk", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url: "showListFactorRiskActivesComboBox.htm",
                                extraParams: {
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#eventOne")
                                );
                              }
                            }
                          }
                        ]
                      },
                      {
                        xtype: "container",
                        flex: 1,
                        itemId: "containerTwoColumn",
                        padding: "0 5 0 5",
                        layout: "anchor",
                        items: [
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            anchor: "100%",
                            displayField: "description",
                            valueField: "idEventOne",
                            itemId: "eventOne",
                            editable: false,
                            forceSelection: true,

                            msgTarget: "side",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "eventOne",
                            action: "selectEventOneSecurity",
                            fieldLabel: "EVENTOS NIVEL 1",
                            store: {
                              fields: ["idEventOne", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url: "showListEventOneActives.htm",
                                extraParams: {
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#eventTwo")
                                );
                              }
                            }
                          },
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            anchor: "100%",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "eventTwo",
                            itemId: "eventTwo",
                            action: "selectEventTwoSecurity",
                            displayField: "description",
                            valueField: "idEventTwo",

                            editable: false,
                            disabled: true,
                            forceSelection: true,
                            fieldLabel: "EVENTOS NIVEL 2",
                            msgTarget: "side",
                            store: {
                              fields: ["idEventTwo", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url: "data/loadGridDefault.htm",
                                extraParams: {
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#eventThree")
                                );
                              }
                            }
                          },
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            anchor: "100%",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "eventThree",
                            itemId: "eventThree",
                            disabled: true,
                            displayField: "description",
                            valueField: "idEventThree",

                            fieldLabel: "EVENTOS NIVEL 3",
                            msgTarget: "side",
                            editable: false,
                            forceSelection: true,
                            store: {
                              fields: ["idEventThree", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url: "data/loadGridDefault.htm",
                                extraParams: {
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#businessLineOne")
                                );
                              }
                            }
                          },
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            anchor: "100%",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "businessLineOne",
                            itemId: "businessLineOne",
                            action: "selectBusinessLineOneSecurity",
                            displayField: "description",
                            valueField: "idBusinessLineOne",

                            fieldLabel: "LINEA NEGOCIO N1",
                            msgTarget: "side",
                            editable: false,
                            forceSelection: true,
                            store: {
                              fields: ["idBusinessLineOne", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url: "showListBusinessLineOneActives.htm",
                                extraParams: {
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#businessLineTwo")
                                );
                              }
                            }
                          },
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            anchor: "100%",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "businessLineTwo",
                            action: "selectBusinessLineTwoSecurity",
                            itemId: "businessLineTwo",
                            queryMode: "local",
                            displayField: "description",
                            valueField: "idBusinessLineTwo",

                            editable: false,
                            disabled: true,
                            forceSelection: true,
                            fieldLabel: "LINEA NEGOCIO N2",
                            msgTarget: "side",
                            store: {
                              fields: ["idBusinessLineTwo", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url: "loadGridDefault.htm",
                                extraParams: {
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#businessLineThree")
                                );
                              }
                            }
                          },
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            anchor: "100%",
                            queryMode: "local",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "businessLineThree",
                            itemId: "businessLineThree",
                            displayField: "description",
                            valueField: "idBusinessLineThree",

                            editable: false,
                            disabled: true,
                            forceSelection: true,
                            fieldLabel: "LINEA NEGOCIO N3",
                            msgTarget: "side",
                            store: {
                              fields: ["idBusinessLineThree", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url: "loadGridDefault.htm",
                                extraParams: {
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#lostType")
                                );
                              }
                            }
                          },

                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            anchor: "100%",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "lostType",
                            itemId: "lostType",
                            displayField: "description",
                            valueField: "idLostType",
                            msgTarget: "side",
                            fieldLabel: "TIPO P&Eacute;RDIDA",

                            editable: false,
                            forceSelection: true,
                            store: {
                              fields: ["idLostType", "description", "state"],
                              data: [
                                {
                                  idLostType: "2",
                                  description: "SE DESCONOCE",
                                  state: "S"
                                },
                                {
                                  idLostType: "3",
                                  description: "EXACTO",
                                  state: "S"
                                },
                                {
                                  idLostType: "1",
                                  description: "ESTIMADO",
                                  state: "S"
                                }
                              ]
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#originIncident")
                                );
                              },
                              select: function(cbo) {
                                var amountInt = me.down("#amountLoss");
                                var currency = me.down("#currency");
                                if (cbo.getValue() == "2") {
                                  me.down("#groupLostAmount").setVisible(false);
                                  amountInt.reset();
                                  amountInt.allowBlank = true;
                                  currency.reset();
                                  currency.allowBlank = true;
                                } else {
                                  me.down("#groupLostAmount").setVisible(true);
                                  amountInt.setValue("0.00");
                                  amountInt.allowBlank = false;
                                  amountInt.setFieldStyle(
                                    "background: #d9ffdb"
                                  );
                                  currency.allowBlank = false;
                                  currency.setFieldStyle("background: #d9ffdb");
                                }
                              }
                            }
                          },
                          {
                            xtype: "container",
                            hidden: true,
                            itemId: "groupLostAmount",
                            layout: "hbox",
                            height: 27,
                            items: [
                              {
                                xtype: "combobox",
                                flex: 1.4,
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                name: "currency",
                                itemId: "currency",
                                displayField: "description",
                                valueField: "idCurrency",

                                msgTarget: "side",
                                fieldLabel: "MONEDA",
                                editable: false,
                                forceSelection: true,
                                store: {
                                  fields: ["idCurrency", "description"],
                                  proxy: {
                                    actionMethods: {
                                      create: "POST",
                                      read: "POST",
                                      update: "POST"
                                    },
                                    type: "ajax",
                                    url: "showListCurrencyActivesComboBox.htm",
                                    extraParams: {
                                      propertyOrder: "description"
                                    },
                                    reader: {
                                      type: "json",
                                      root: "data",
                                      successProperty: "success"
                                    }
                                  }
                                },
                                listeners: {
                                  specialkey: function(f, e) {
                                    DukeSource.global.DirtyView.focusEventEnterObligatory(
                                      f,
                                      e,
                                      me.down("#netLoss")
                                    );
                                  }
                                }
                              },
                              {
                                xtype: "NumberDecimalNumberObligatory",
                                value: "0.00",
                                blankText:
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_REQUIRE,
                                fieldLabel: "MONTO",
                                labelWidth: 40,
                                name: "amountLoss",
                                itemId: "amountLoss",
                                flex: 1,
                                listeners: {
                                  specialkey: function(f, e) {
                                    DukeSource.global.DirtyView.focusEventEnter(
                                      f,
                                      e,
                                      me.down("#originIncident")
                                    );
                                  },
                                  select: function(cbo) {
                                    var amountInt = me.down(
                                      "NumberDecimalNumberObligatory"
                                    );
                                    if (cbo.getValue() == "2") {
                                      amountInt.setValue("");
                                      amountInt.allowBlank = true;
                                      amountInt.setEditable(false);
                                      amountInt.setReadOnly(true);
                                      amountInt.setFieldStyle(
                                        "background: #FFF"
                                      );
                                    } else {
                                      amountInt.setValue("0.00");
                                      amountInt.allowBlank = false;
                                      amountInt.setEditable(true);
                                      amountInt.setReadOnly(false);
                                      amountInt.setFieldStyle(
                                        "background: #d9ffdb"
                                      );
                                    }
                                  }
                                }
                              }
                            ]
                          }
                        ]
                      },
                      {
                        xtype: "container",
                        itemId: "containerThreeColumn",
                        flex: 0.7,
                        layout: "anchor",
                        items: [
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            anchor: "100%",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "originIncident",
                            itemId: "originIncident",
                            displayField: "description",
                            valueField: "value",

                            editable: false,
                            forceSelection: true,
                            msgTarget: "side",
                            fieldLabel: "PROCEDENCIA",
                            store: {
                              fields: ["value", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url:
                                  "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                                extraParams: {
                                  propertyFind: "identified",
                                  valueFind: "ORIGININC",
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#incidentsGroup")
                                );
                              },
                              select: function(cbo) {
                                if (cbo.getValue() == "CO") {
                                  if (
                                    me.down("#containerUserReport") == undefined
                                  ) {
                                    me.down("#containerThreeColumn").add(2, {
                                      xtype: "container",
                                      itemId: "containerUserReport",
                                      height: 26,
                                      layout: "hbox",
                                      items: [
                                        {
                                          xtype: "textfield",
                                          name: "userReport",
                                          itemId: "userReport",
                                          hidden: true
                                        },
                                        {
                                          xtype:
                                            DukeSource.view.risk.util
                                              .UpperCaseTextFieldReadOnly,
                                          flex: 1,
                                          allowBlank: false,
                                          name: "fullNameUserReport",
                                          itemId: "fullNameUserReport",
                                          fieldLabel: "QUIEN REPORTÓ"
                                        },
                                        {
                                          xtype: "button",
                                          iconCls: "search",
                                          handler: function() {
                                            var win = Ext.create(
                                              "DukeSource.view.risk.util.search.SearchUser",
                                              {
                                                modal: true
                                              }
                                            ).show();
                                            win
                                              .down("grid")
                                              .on("itemdblclick", function() {
                                                me.down(
                                                  "textfield[name=userReport]"
                                                ).setValue(
                                                  win
                                                    .down("grid")
                                                    .getSelectionModel()
                                                    .getSelection()[0]
                                                    .get("userName")
                                                );
                                                me.down(
                                                  "textfield[name=userRegister]"
                                                ).setValue(
                                                  win
                                                    .down("grid")
                                                    .getSelectionModel()
                                                    .getSelection()[0]
                                                    .get("userName")
                                                );
                                                me.down(
                                                  "UpperCaseTextFieldReadOnly[name=fullNameUserReport]"
                                                ).setValue(
                                                  win
                                                    .down("grid")
                                                    .getSelectionModel()
                                                    .getSelection()[0]
                                                    .get("fullName")
                                                );
                                                win.close();
                                              });
                                          }
                                        }
                                      ]
                                    });
                                  } else {
                                    me.down("#containerUserReport").setVisible(
                                      true
                                    );
                                    me
                                      .down("#containerUserReport")
                                      .down(
                                        "#fullNameUserReport"
                                      ).allowBlank = true;
                                  }
                                } else {
                                  if (
                                    !(
                                      me.down("#containerUserReport") ==
                                      undefined
                                    )
                                  ) {
                                    me.down("#containerUserReport").setVisible(
                                      false
                                    );
                                    me
                                      .down("#containerUserReport")
                                      .down(
                                        "#fullNameUserReport"
                                      ).allowBlank = true;
                                    me.down(
                                      "textfield[name=userRegister]"
                                    ).setValue(null);
                                  }
                                }
                              }
                            }
                          },
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            anchor: "100%",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "incidentsGroup",
                            itemId: "incidentsGroup",
                            displayField: "description",
                            valueField: "idIncidentGroup",

                            editable: false,
                            forceSelection: true,
                            fieldLabel: "GRUPO",
                            msgTarget: "side",
                            store: {
                              fields: ["idIncidentGroup", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url: "showListIncidentGroupActives.htm",
                                extraParams: {
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#impactReport")
                                );
                              }
                            }
                          },
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            anchor: "100%",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "impactReport",
                            itemId: "impactReport",
                            displayField: "description",
                            valueField: "value",

                            editable: false,
                            forceSelection: true,
                            fieldLabel: "IMPACTO",
                            msgTarget: "side",
                            store: {
                              fields: ["value", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url:
                                  "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                                extraParams: {
                                  propertyFind: "identified",
                                  valueFind: "IMPACTINC",
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#document")
                                );
                              }
                            }
                          },
                          {
                            xtype: "fileuploadfield",
                            fieldLabel: "ADJUNTAR DOCUM",
                            name: "document",
                            itemId: "document",
                            anchor: "100%",
                            buttonConfig: {
                              iconCls: "tesla even-attachment"
                            },
                            buttonText: ""
                          },
                          {
                            xtype: "container",
                            layout: "hbox",
                            height: 26,
                            items: [
                              {
                                xtype: "checkbox",
                                fieldLabel: "CONFIDENCIAL",
                                name: "confidential",
                                itemId: "confidential",
                                inputValue: "S",
                                uncheckedValue: "N",
                                checked: false,
                                anchor: "100%",
                                listeners: {
                                  specialkey: function(c, d) {
                                    DukeSource.global.DirtyView.focusEventEnterObligatory(
                                      c,
                                      d,
                                      a.down("#processOrigin")
                                    );
                                  },
                                  change: function(c) {
                                    if (c.checked == true) {
                                      c.up("form").setBodyStyle(
                                        "background",
                                        "#FFDC9E"
                                      );
                                    } else {
                                      c.up("form").setBodyStyle(
                                        "background",
                                        "#FFF"
                                      );
                                    }
                                  }
                                }
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  },
                  {
                    xtype: "fieldset",
                    padding: 0,
                    bodyPadding: 0,
                    title: "continuidad del Negocio",
                    checkboxToggle: true,
                    collapsed: false,
                    layout: {
                      type: "hbox",
                      align: "stretch"
                    },
                    itemId: "containerContinuity",
                    items: [
                      {
                        xtype: "container",
                        flex: 1,
                        layout: "anchor",
                        items: [
                          {
                            xtype: "container",
                            height: 26,
                            layout: "hbox",
                            items: [
                              {
                                xtype: "textfield",
                                name: "idFixedAssets",
                                hidden: true
                              },
                              {
                                xtype:
                                  DukeSource.view.risk.util
                                    .UpperCaseTextFieldReadOnly,
                                labelWidth: 120,
                                flex: 1,
                                name: "descriptionFixedAssets",
                                fieldLabel: "RECURSOS"
                              },
                              {
                                xtype: "button",
                                iconCls: "search",
                                handler: function() {
                                  var win = Ext.create(
                                    "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowSearchFixedAssets",
                                    {
                                      modal: true
                                    }
                                  ).show();
                                  win
                                    .down("grid")
                                    .on("itemdblclick", function() {
                                      me.down(
                                        "textfield[name=idFixedAssets]"
                                      ).setValue(
                                        win
                                          .down("grid")
                                          .getSelectionModel()
                                          .getSelection()[0]
                                          .get("idFixedAssets")
                                      );
                                      me.down(
                                        "UpperCaseTextFieldReadOnly[name=descriptionFixedAssets]"
                                      ).setValue(
                                        win
                                          .down("grid")
                                          .getSelectionModel()
                                          .getSelection()[0]
                                          .get("description")
                                      );
                                      win.close();
                                    });
                                }
                              }
                            ]
                          },
                          {
                            xtype: "combobox",
                            displayField: "description",
                            valueField: "idSafetyCriterion",

                            multiSelect: true,
                            fieldLabel: "TIPO INTERRUPCI&Oacute;N",
                            fieldCls: "obligatoryTextField",
                            name: "criterionSafety",
                            itemId: "criterionSafety",
                            allowBlank: false,
                            labelWidth: 120,
                            anchor: "100%",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            editable: false,
                            msgTarget: "side",
                            store: {
                              fields: ["idSafetyCriterion", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url:
                                  "showListSafetyCriterionActivesComboBox.htm",
                                extraParams: {
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#document")
                                );
                              }
                            }
                          }
                        ]
                      },
                      {
                        xtype: "container",
                        flex: 1,
                        padding: "0 0 0 5",
                        layout: "anchor",
                        items: [
                          {
                            xtype: "combobox",
                            fieldCls: "obligatoryTextField",
                            name: "subClassification",
                            itemId: "subClassification",
                            allowBlank: false,
                            anchor: "100%",
                            displayField: "description",
                            valueField: "idSubClassification",

                            editable: false,
                            forceSelection: true,
                            fieldLabel: "RESPON.SOLUCI&Oacute;N",
                            store: {
                              fields: ["idSubClassification", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url:
                                  "showListSubClassificationActivesComboBox.htm",
                                extraParams: {
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            }
                          },
                          {
                            xtype: "container",
                            height: 26,
                            layout: "hbox",
                            items: [
                              {
                                xtype: "textfield",
                                name: "reasonSafety",
                                hidden: true
                              },
                              {
                                xtype:
                                  DukeSource.view.risk.util
                                    .UpperCaseTextFieldReadOnly,
                                name: "descriptionReasonSafety",
                                flex: 1,
                                fieldLabel: "TIPO SOLUCI&Oacute;N"
                              },
                              {
                                xtype: "button",
                                iconCls: "search",
                                handler: function() {
                                  var win = Ext.create(
                                    "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowSearchCause",
                                    {
                                      modal: true
                                    }
                                  ).show();
                                  win
                                    .down("grid")
                                    .on("itemdblclick", function() {
                                      me.down(
                                        "textfield[name=reasonSafety]"
                                      ).setValue(
                                        win
                                          .down("grid")
                                          .getSelectionModel()
                                          .getSelection()[0]
                                          .get("idReasonSafety")
                                      );
                                      me.down(
                                        "UpperCaseTextFieldReadOnly[name=descriptionReasonSafety]"
                                      ).setValue(
                                        win
                                          .down("grid")
                                          .getSelectionModel()
                                          .getSelection()[0]
                                          .get("description")
                                      );
                                      win.close();
                                    });
                                }
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  },
                  {
                    xtype: "container",
                    name: "containerProc",
                    itemId: "containerProc",
                    layout: {
                      type: "hbox",
                      align: "stretch"
                    },
                    items: [
                      {
                        xtype: "container",
                        flex: 1,
                        layout: "anchor",
                        items: [
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            labelWidth: 120,
                            anchor: "100%",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "processOrigin",
                            itemId: "processOrigin",
                            action: "selectProcessOriginSecurity",
                            fieldLabel: "PROCESO ORIGEN",
                            displayField: "description",
                            valueField: "idProcess",

                            editable: false,
                            forceSelection: true,
                            plugins: ["ComboSelectCount"],
                            msgTarget: "side",
                            store: {
                              fields: ["idProcess", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url: "showListProcessActivesComboBox.htm",
                                extraParams: {
                                  propertyOrder: "idProcess"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#subProcessOrigin")
                                );
                              }
                            }
                          },
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            labelWidth: 120,
                            anchor: "100%",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "subProcessOrigin",
                            itemId: "subProcessOrigin",
                            fieldLabel: "SUBPROCESO ORIGEN",
                            displayField: "description",
                            valueField: "idSubProcess",

                            editable: false,
                            disabled: true,
                            forceSelection: true,
                            queryMode: "local",
                            msgTarget: "side",
                            store: {
                              fields: ["idSubProcess", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url: "loadGridDefault.htm",
                                extraParams: {
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#processImpact")
                                );
                              }
                            }
                          }
                        ]
                      },
                      {
                        xtype: "container",
                        flex: 1,
                        layout: "anchor",
                        items: [
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            labelWidth: 110,
                            padding: "0 0 0 5",
                            anchor: "100%",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "processImpact",
                            itemId: "processImpact",
                            action: "selectProcessImpactSecurity",
                            displayField: "description",
                            valueField: "idProcess",

                            editable: false,
                            forceSelection: true,
                            plugins: ["ComboSelectCount"],
                            msgTarget: "side",
                            fieldLabel: "PROCESO IMPACTO",
                            store: {
                              fields: ["idProcess", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url: "showListProcessActivesComboBox.htm",
                                extraParams: {
                                  propertyOrder: "idProcess"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#subProcessImpact")
                                );
                              }
                            }
                          },
                          {
                            xtype: "combobox",
                            allowBlank: false,
                            fieldCls: "obligatoryTextField",
                            padding: "0 0 0 5",
                            labelWidth: 110,
                            anchor: "100%",
                            blankText:
                              DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: "subProcessImpact",
                            itemId: "subProcessImpact",
                            fieldLabel: "SUBPROCESO IMPACT",
                            displayField: "description",
                            valueField: "idSubProcess",

                            editable: false,
                            disabled: true,
                            forceSelection: true,
                            queryMode: "local",
                            msgTarget: "side",
                            store: {
                              fields: ["idSubProcess", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url: "loadGridDefault.htm",
                                extraParams: {
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnterObligatory(
                                  f,
                                  e,
                                  me.down("#causeCollaborator")
                                );
                              }
                            }
                          }
                        ]
                      }
                    ]
                  },
                  {
                    xtype: "container",
                    name: "containerLeft",
                    itemId: "containerLeft",
                    layout: "anchor",
                    items: [
                      {
                        xtype: "UpperCaseTextArea",
                        fieldLabel: "CAUSA/GESTION<br>RECOMENDACION",
                        name: "causeCollaborator",
                        itemId: "causeCollaborator",
                        labelWidth: 120,
                        height: 40,
                        allowBlank: false,
                        maxLength: 600,
                        msgTarget: "side",
                        fieldCls: "obligatoryTextField",
                        anchor: "100%",
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        listeners: {
                          specialkey: function(f, e) {
                            DukeSource.global.DirtyView.focusEventEnterObligatory(
                              f,
                              e,
                              me.down("#comments")
                            );
                          }
                        }
                      },
                      {
                        xtype: "UpperCaseTextArea",
                        fieldLabel: "COMENTARIO<br>ACCIONES REALIZADAS",
                        name: "comments",
                        itemId: "comments",
                        labelWidth: 120,
                        height: 40,
                        maxLength: 600,
                        allowBlank: false,
                        msgTarget: "side",
                        fieldCls: "obligatoryTextField",
                        anchor: "100%",
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        listeners: {
                          specialkey: function(f, e) {
                            DukeSource.global.DirtyView.focusEventEnterObligatory(
                              f,
                              e,
                              me.down("button[action=saveIncidents]")
                            );
                          }
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ],
        buttonAlign: "center",
        buttons: [
          {
            text: "GUARDAR",
            action: "saveIncidentsSecurity",
            scale: "medium",
            iconCls: "save"
          },
          {
            text: "SALIR",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
