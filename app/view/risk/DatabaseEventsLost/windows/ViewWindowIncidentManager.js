Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowIncidentManager",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowIncidentManager",
    requires: [
      "DukeSource.view.risk.util.ViewComboYesNo",
      "Ext.ux.DateTimeField"
    ],
    width: 850,
    layout: {
      type: "fit"
    },
    title: "Reportar incidente",
    border: false,
    titleAlign: "center",
    initComponent: function() {
      var a = this;
      var typeIncident = this.typeIncident;
      Ext.applyIf(a, {
        items: [
          {
            xtype: "form",
            padding: "0 0 0 2",
            itemId: "generalForm",
            submitEmptyText: false,
            bodyPadding: 10,
            fieldDefaults: {
              labelCls: "changeSizeFontToEightPt",
              fieldCls: "changeSizeFontToEightPt"
            },
            items: [
              {
                xtype: "textfield",
                hidden: true,
                value: "id",
                name: "idDetailIncidents"
              },
              {
                xtype: "textfield",
                hidden: true,
                value: typeIncident,
                name: "typeIncident"
              },
              {
                xtype: "textfield",
                hidden: true,
                name: "fileAttachment",
                value: "N",
                itemId: "fileAttachment"
              },
              {
                xtype: "textfield",
                hidden: true,
                name: "collaborator",
                itemId: "collaborator"
              },
              {
                xtype: "textfield",
                hidden: true,
                name: "userRegister",
                itemId: "userRegister"
              },
              {
                xtype: "datefield",
                hidden: true,
                name: "dateProcess",
                format: "d/m/Y",
                itemId: "dateProcess"
              },
              {
                xtype: "datetimefield",
                format: "d/m/Y H:i:s",
                hidden: true,
                name: "dateRegister",
                itemId: "dateRegister"
              },
              {
                xtype: "fieldset",
                title: "Datos del incidente",
                items: [
                  {
                    xtype: "checkbox",
                    fieldLabel: "Genera p&eacute;rdida",
                    style: "font-weight: bold;",
                    hidden: true,
                    itemId: "generateLoss",
                    name: "generateLoss",
                    inputValue: "S",
                    uncheckedValue: "N",
                    checked: false,
                    anchor: "100%"
                  },
                  {
                    xtype:"UpperCaseTextField",
                    allowBlank: false,
                    fieldLabel: "Descripci&oacute;n corta",
                    maxLength: 300,
                    msgTarget: "side",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    name: "descriptionShort",
                    itemId: "descriptionShort",
                    fieldCls: "obligatoryTextField",
                    anchor: "100%"
                  },
                  {
                    xtype: 'UpperCaseTextArea',
                    allowBlank: false,
                    fieldCls: "obligatoryTextField",
                    height: 50,
                    maxLength: 3000,
                    msgTarget: "side",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    anchor: "100%",
                    fieldLabel: "Descripci&oacute;n larga",
                    name: "descriptionLarge",
                    itemId: "descriptionLarge"
                  },
                  {
                    xtype: "combobox",
                    fieldLabel: getName("INC_CBX_WorkArea"),
                    name: "workArea",
                    itemId: "workArea",
                    enableKeyEvents: true,
                    forceSelection: true,
                    displayField: "description",
                    valueField: "idWorkArea",
                    plugins: ["ComboSelectCount"],
                    store: {
                      fields: ["idWorkArea", "description"],
                      proxy: {
                        actionMethods: {
                          create: "POST",
                          read: "POST",
                          update: "POST"
                        },
                        type: "ajax",
                        url:
                          "http://localhost:9000/giro/showListWorkAreaActivesComboBox.htm",
                        extraParams: {
                          propertyOrder: "description"
                        },
                        reader: {
                          type: "json",
                          root: "data",
                          successProperty: "success"
                        }
                      }
                    }
                  },
                  {
                    xtype: "combobox",
                    hidden: hidden("INC_WIR_CBX_Agency"),
                    allowBlank: blank("INC_WIR_CBX_Agency"),
                    fieldCls: styleField("INC_WIR_CBX_Agency"),
                    msgTarget: "side",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    anchor: "100%",
                    fieldLabel: "Agencia",
                    forceSelection: true,
                    plugins: ["ComboSelectCount"],
                    name: "agency",
                    itemId: "agency",
                    displayField: "description",
                    valueField: "idAgency",
                    store: {
                      fields: ["idAgency", "description"],
                      proxy: {
                        actionMethods: {
                          create: "POST",
                          read: "POST",
                          update: "POST"
                        },
                        type: "ajax",
                        url:
                          "http://localhost:9000/giro/showListAgencyActivesComboBox.htm",
                        extraParams: {
                          propertyOrder: "a.description"
                        },
                        reader: {
                          type: "json",
                          root: "data",
                          successProperty: "success"
                        }
                      }
                    }
                  },
                  {
                    xtype: "datetimefield",
                    allowBlank: false,
                    format: "d/m/Y H:i",
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    fieldLabel: "Fecha ocurrencia",
                    name: "dateOccurrence",
                    itemId: "dateOccurrence",
                    listeners: {
                      expand: function(d) {
                        if (a.down("#dateDiscovery").getValue() !== null) {
                          d.setMaxValue(a.down("#dateDiscovery").getValue());
                        } else {
                          d.setMaxValue(new Date());
                        }
                      }
                    }
                  },
                  {
                    xtype: "datetimefield",
                    format: "d/m/Y H:i",
                    msgTarget: "side",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    fieldLabel: "Fecha fin",
                    name: "dateFinal",
                    hidden: hidden("INC_WIR_DTF_DateFinal"),
                    itemId: "dateFinal",
                    listeners: {
                      expand: function(d) {
                        d.setMinValue(a.down("#dateOccurrence").getRawValue());
                        d.setMaxValue(new Date());
                      }
                    }
                  },
                  {
                    xtype: "datetimefield",
                    allowBlank: blank("INC_WIR_DTF_DateDiscovery"),
                    fieldCls: styleField("INC_WIR_DTF_DateDiscovery"),
                    format: "d/m/Y H:i",
                    hidden: hidden("INC_WIR_DTF_DateDiscovery"),
                    msgTarget: "side",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    fieldLabel: "F.descubrimiento",
                    name: "dateDiscovery",
                    itemId: "dateDiscovery",
                    listeners: {
                      expand: function(d) {
                        d.setMinValue(
                          a.down("datefield[name=dateOccurrence]").getRawValue()
                        );
                        d.setMaxValue(new Date());
                      }
                    }
                  },
                  {
                    xtype: 'UpperCaseTextArea',
                    fieldLabel: getName("INC_WIR_TXA_CauseCollaborator"),
                    name: "causeCollaborator",
                    itemId: "name",
                    maxLength: 600,
                    height: 52,
                    allowBlank: blank("IWCCauseCollaborator"),
                    fieldCls: styleField("IWCCauseCollaborator"),
                    msgTarget: "side",
                    anchor: "100%",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE
                  },
                  {
                    xtype: "fileuploadfield",
                    fieldLabel: "Subir documento",
                    name: "document",
                    itemId: "document",
                    anchor: "100%",
                    buttonConfig: {
                      iconCls: "tesla even-attachment"
                    },
                    buttonText: ""
                  }
                ]
              },
              {
                xtype: "fieldset",
                title: "Impacto",
                layout: "hbox",
                items: [
                  {
                    xtype: "container",
                    flex: 1,
                    items: [
                      {
                        xtype: "combobox",
                        editable: false,
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        fieldLabel: "Moneda",
                        name: "currency",
                        itemId: "currency",
                        displayField: "description",
                        valueField: "idCurrency",
                        forceSelection: true,
                        store: {
                          fields: [
                            "idCurrency",
                            "description",
                            "symbol",
                            "state"
                          ],
                          proxy: {
                            actionMethods: {
                              create: "POST",
                              read: "POST",
                              update: "POST"
                            },
                            type: "ajax",
                            url:
                              "http://localhost:9000/giro/showListCurrencyActivesComboBox.htm",
                            extraParams: {
                              propertyOrder: "description"
                            },
                            reader: {
                              type: "json",
                              root: "data",
                              successProperty: "success"
                            }
                          }
                        }
                      },
                      {
                        xtype: "NumberDecimalNumber",
                        fieldLabel: "Monto",
                        value: 0,
                        name: "amountLoss",
                        itemId: "amountLoss"
                      }
                    ]
                  },
                  {
                    xtype: "container",
                    flex: 1,
                    items: [
                      {
                        xtype: "combobox",
                        editable: false,
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        fieldLabel: "Impacto",
                        queryMode: "local",
                        name: "impacts",
                        multiSelect: true,
                        itemId: "impacts",
                        value: "1",
                        displayField: "description",
                        valueField: "idImpactFeature",
                        forceSelection: true,
                        store: {
                          autoLoad: true,
                          fields: [
                            "idImpactFeature",
                            "description",
                            "idImpact",
                            "value"
                          ],
                          proxy: {
                            actionMethods: {
                              create: "POST",
                              read: "POST",
                              update: "POST"
                            },
                            type: "ajax",
                            url:
                              "http://localhost:9000/giro/showListImpactFeatureActivesComboBox.htm",
                            extraParams: {
                              propertyOrder: "if.description",
                              idOperationalRiskExposition: "1",
                              idTypeMatrix: "1"
                            },
                            reader: {
                              type: "json",
                              root: "data",
                              successProperty: "success"
                            }
                          }
                        }
                      }
                    ]
                  }
                ]
              },
              {
                xtype: "fieldset",
                hidden: hidden("INC_WIR_FDS_HelpDesk"),
                title: "Mesa de ayuda",
                layout: "hbox",
                items: [
                  {
                    xtype: "ViewComboYesNo",
                    name: "hasTicket",
                    itemId: "hasTicket",
                    msgTarget: "side",
                    fieldCls:"UpperCaseTextFieldReadOnly",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    fieldLabel: "Tiene ticket M.A",
                    listeners: {
                      select: function(cbo) {
                        if (cbo.getValue() === DukeSource.global.GiroConstants.YES) {
                          a.down("#numberTicket").setVisible(true);
                        } else {
                          a.down("#numberTicket").setVisible(false);
                        }
                      },
                      render: function(cbo) {
                        if (cbo.getValue() === DukeSource.global.GiroConstants.YES) {
                          a.down("#numberTicket").setVisible(true);
                        } else {
                          a.down("#numberTicket").setVisible(false);
                        }
                      }
                    }
                  },
                  {
                    xtype: "textfield",
                    margin: "0 0 0 5",
                    hidden: true,
                    fieldLabel: "N&uacute;mero",
                    value: "",
                    name: "numberTicket",
                    itemId: "numberTicket"
                  }
                ]
              },
              {
                xtype: "container",
                flex: 1,
                hidden: true,
                padding: "0 0 0 5",
                name: "containerLeft",
                itemId: "containerLeft",
                layout: {
                  type: "anchor"
                },
                items: [
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "factorRisk",
                    name: "factorRisk",
                    allowBlank: true
                  },
                  {
                    xtype: "displayfield",
                    anchor: "100%",
                    fieldLabel: "Factor de riesgo",
                    hidden: hidden("INC_DSP_DescriptionFactorRisk"),
                    itemId: "descriptionFactorRisk",
                    name: "descriptionFactorRisk",
                    value: "(Seleccionar)",
                    margin: "0 0 10 0",
                    fieldCls: "style-for-url",
                    listeners: {
                      afterrender: function(view) {
                        view.getEl().on("click", function() {
                          Ext.create(
                            "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeFactorRisk",
                            {
                              winParent: a
                            }
                          ).show();
                        });
                      }
                    }
                  },
                  {
                    xtype: "checkbox",
                    labelWidth: 105,
                    fieldLabel: "Confidencial",
                    name: "confidential",
                    itemId: "confidential",
                    inputValue: "S",
                    uncheckedValue: "N",
                    hidden: hidden("IWCBConfidential"),
                    checked: false,
                    anchor: "100%",
                    listeners: {
                      change: function(c) {
                        if (c.checked === true) {
                          c.up("form").setBodyStyle("background", "#FFDC9E");
                        } else {
                          c.up("form").setBodyStyle("background", "#FFF");
                        }
                      }
                    }
                  },
                  {
                    xtype: 'UpperCaseTextArea',
                    fieldLabel: "Gesti&oacute;n realizada",
                    hidden: true,
                    name: "commentManager",
                    itemId: "commentManager",
                    labelWidth: 105,
                    maxLength: 600,
                    height: 52,
                    allowBlank: blank("IWMCommentManager"),
                    fieldCls: styleField("IWMCommentManager"),
                    msgTarget: "side",
                    anchor: "100%",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE
                  }
                ]
              }
            ]
          }
        ],
        dockedItems: [
          {
            xtype: "toolbar",
            padding: 5,
            width: 220,
            layout: "anchor",
            dock: "left",
            items: [
              {
                xtype: "fieldset",
                title: "Informaci&oacute;n de registro",
                items: [
                  {
                    xtype: "textfield",
                    hidden: true,
                    readOnly: true,
                    value: userName,
                    fieldCls: "readOnlyText",
                    name: "userReport",
                    itemId: "userReport",
                    enforceMaxLength: true
                  },
                  {
                    xtype: "datetimefield",
                    format: "d/m/Y H:i:s",
                    readOnly: true,
                    anchor: "95%",
                    labelAlign: "top",
                    fieldCls: "readOnlyText",
                    fieldLabel: "Fecha de registro",
                    name: "dateRegisterTemp",
                    itemId: "dateRegisterTemp",
                    enforceMaxLength: true
                  },
                  {
                    xtype: "textfield",
                    readOnly: true,
                    value: fullName,
                    anchor: "95%",
                    labelAlign: "top",
                    fieldCls: "readOnlyText",
                    fieldLabel: "Usuario que registra",
                    name: "fullNameUserReport",
                    itemId: "fullNameUserReport",
                    enforceMaxLength: true
                  },
                  {
                    xtype: "textfield",
                    readOnly: true,
                    value: descriptionWorkArea,
                    anchor: "95%",
                    labelAlign: "top",
                    fieldCls: "readOnlyText",
                    fieldLabel: "&Aacute;rea",
                    name: "workAreaReport",
                    itemId: "workAreaReport",
                    enforceMaxLength: true
                  }
                ]
              }
            ]
          }
        ],
        buttonAlign: "right",
        buttons: [
          {
            text: "Salir",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          },
          {
            text:
              category === DukeSource.global.GiroConstants.GESTOR ||
              category === DukeSource.global.GiroConstants.COLLABORATOR
                ? "Reportar"
                : "Continuar",
            action:
              category === DukeSource.global.GiroConstants.GESTOR ||
              category === DukeSource.global.GiroConstants.COLLABORATOR
                ? "savePreIncidents"
                : "analysisSaveIncident",
            scale: "medium",
            iconCls: "send"
          }
        ],
        listeners: {
          beforeshow: function(win) {
            win.down("#currency").store.load({
              callback: function() {
                win.down("#currency").setValue("1");
              }
            });
          }
        }
      });
      a.callParent(arguments);
    }
  }
);
