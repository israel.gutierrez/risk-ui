Ext.define("ModelManagerApprove", {
  extend: "Ext.data.Model",
  fields: ["approve", "fullName", "dateRegister", "comments"]
});

var storeManagerApprove = Ext.create("Ext.data.Store", {
  model: "ModelManagerApprove",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      rootProperty: "data",
      successProperty: "success"
    }
  }
});
var storeCollaboratorApprove = Ext.create("Ext.data.Store", {
  model: "ModelManagerApprove",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      rootProperty: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.WindowRegisterEventWizard",
  {
    extend: "Ext.window.Window",
    alias: "widget.WindowRegisterEventWizard",
    requires: [
      "DukeSource.view.risk.parameter.combos.ViewComboRiskType",
      "Ext.form.Panel",
      "Ext.ux.DateTimeField",
      "Ext.form.field.TextArea",
      "Ext.form.field.ComboBox",
      "Ext.form.field.Checkbox",
      "Ext.form.field.Date",
      "Ext.button.Button",
      "Ext.form.field.File",
      "Ext.form.FieldSet",
      "Ext.form.field.Display",
      "Ext.form.field.Number",
      "Ext.toolbar.Toolbar",
      "Ext.grid.Panel",
      "Ext.grid.column.Number",
      "Ext.grid.column.Date",
      "Ext.grid.column.Boolean",
      "Ext.grid.View"
    ],
    layout: {
      type: "auto"
    },
    title: "Flujo de eventos",
    border: true,
    titleAlign: "center",
    autoScroll: true,
    width: 900,
    modal: true,
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            border: false,
            bodyPadding: 5,
            enctype: "multipart/form-data",
            layout: "card",
            fieldDefaults: {
              labelCls: "changeSizeFontToEightPt",
              fieldCls: "changeSizeFontToEightPt"
            },
            items: [
              {
                id: "card-0",
                xtype: "container",
                autoScroll: true,
                layout: "anchor",
                items: [
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "codeCorrelative",
                    name: "codeCorrelative"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    value: "N",
                    itemId: "checkApprove",
                    name: "checkApprove"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "userApprove",
                    name: "userApprove"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "commentApprove",
                    name: "commentApprove"
                  },
                  {
                    xtype: "datetimefield",
                    format: "d/m/Y H:i:s",
                    hidden: true,
                    itemId: "dateApprove",
                    name: "dateApprove"
                  },

                  {
                    xtype: "textfield",
                    itemId: "eventMain",
                    name: "eventMain",
                    value: "S",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    name: "fileAttachment",
                    itemId: "fileAttachment",
                    value: "N",
                    hidden: true
                  },
                  {
                    xtype: "datetimefield",
                    name: "dateAcceptLossEvent",
                    itemId: "dateAcceptLossEvent",
                    fieldLabel: "Fecha de proceso",
                    msgTarget: "side",
                    hidden: true,
                    format: "d/m/Y H:i",
                    value: new Date()
                  },
                  {
                    xtype: "textfield",
                    itemId: "numberRelation",
                    name: "numberRelation",
                    value: 0,
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    name: "indicatorAtRisk",
                    itemId: "indicatorAtRisk",
                    hidden: true
                  },
                  {
                    xtype:"UpperCaseTextFieldReadOnly",
                    name: "codesRiskAssociated",
                    itemId: "codesRiskAssociated",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    name: "numberRiskAssociated",
                    itemId: "numberRiskAssociated",
                    value: "0",
                    hidden: true
                  },
                  {
                    xtype: "datefield",
                    format: "d/m/Y",
                    value: new Date(),
                    width: 240,
                    labelWidth: 125,
                    disabled: true,
                    hidden: true,
                    name: "dateProcess",
                    itemId: "dateProcess"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    name: "idIncident",
                    itemId: "idIncident"
                  },
                  {
                    xtype: "textfield",
                    name: "numberActionPlan",
                    itemId: "numberActionPlan",
                    value: "0",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    name: "codesActionPlan",
                    itemId: "codesActionPlan",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    name: "idDetailIncidents"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    value: "id",
                    itemId: "idEvent",
                    name: "idEvent"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    name: "sequenceEventState",
                    itemId: "sequence",
                    value: 1
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "unity",
                    name: "unity"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "agency",
                    name: "agency"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "product",
                    name: "product"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "codeProcess",
                    name: "codeProcess"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "idProcessType",
                    name: "idProcessType"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "idProcess",
                    name: "idProcess"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "idSubProcess",
                    name: "idSubProcess"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "idActivity",
                    name: "idActivity"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    value: userName,
                    itemId: "userRegister",
                    name: "userRegister"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "costCenter",
                    name: "costCenter"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "businessLineOne",
                    name: "businessLineOne"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "businessLineTwo",
                    name: "businessLineTwo"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "businessLineThree",
                    name: "businessLineThree"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "eventOne",
                    name: "eventOne"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "eventTwo",
                    name: "eventTwo"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "eventThree",
                    name: "eventThree"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "factorRisk",
                    name: "factorRisk"
                  },
                  {
                    xtype: "NumberDecimalNumber",
                    name: "totalAmountSpend",
                    itemId: "totalAmountSpend",
                    hidden: true,
                    value: 0
                  },
                  {
                    xtype: "NumberDecimalNumber",
                    name: "amountSubEvent",
                    itemId: "amountSubEvent",
                    hidden: true,
                    value: 0
                  },
                  {
                    xtype: "NumberDecimalNumber",
                    name: "lossRecovery",
                    itemId: "lossRecovery",
                    hidden: true,
                    value: 0
                  },
                  {
                    xtype: "NumberDecimalNumber",
                    name: "amountThirdRecovery",
                    itemId: "amountThirdRecovery",
                    hidden: true,
                    value: 0
                  },
                  {
                    xtype: "NumberDecimalNumber",
                    name: "sureAmountRecovery",
                    itemId: "sureAmountRecovery",
                    hidden: true,
                    value: 0
                  },
                  {
                    xtype: "NumberDecimalNumber",
                    name: "totalRecovery",
                    itemId: "totalRecovery",
                    hidden: true,
                    value: 0
                  },
                  {
                    xtype: "NumberDecimalNumber",
                    name: "netLoss",
                    itemId: "netLoss",
                    hidden: true,
                    value: 0.0
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    name: "eventState",
                    itemId: "eventState"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    value: "A",
                    itemId: "stateTemp",
                    name: "stateTemp"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "order",
                    value: 1,
                    name: "order"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "state",
                    value: "S",
                    name: "state"
                  },
                  {
                    xtype: "textfield",
                    hidden: true,
                    itemId: "descriptionMotiveRefused",
                    name: "descriptionMotiveRefused"
                  },
                  {
                    xtype: "fieldset",
                    title: "Organizaci&oacute;n",
                    items: [
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        fieldLabel: "Organizaci&oacute;n interna",
                        itemId: "descriptionUnity",
                        name: "descriptionUnity",
                        labelWidth: 150,
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeOrganization",
                                {
                                  backWindow: me,
                                  descriptionUnity: "descriptionUnity",
                                  unity: "unity",
                                  costCenter: "costCenter"
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        fieldLabel: "Oficina (ub. geogr&aacute;fica)",
                        itemId: "descriptionAgency",
                        name: "descriptionAgency",
                        labelWidth: 150,
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        allowBlank: false,
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeAgency",
                                {
                                  backWindow: me
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        fieldLabel: "Producto afectado",
                        itemId: "descriptionProduct",
                        name: "descriptionProduct",
                        labelWidth: 150,
                        allowBlank: false,
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeProduct",
                                {
                                  winParent: me
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        fieldLabel: "Proceso afectado",
                        itemId: "descriptionProcess",
                        name: "descriptionProcess",
                        labelWidth: 150,
                        allowBlank: false,
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeProcess",
                                {
                                  backWindow: me
                                }
                              ).show();
                            });
                          }
                        }
                      }
                    ]
                  },
                  {
                    xtype: "fieldset",
                    title: "Detalle",
                    items: [
                      {
                        xtype: "combobox",
                        width: 360,
                        labelWidth: 150,
                        fieldLabel: "Tipo de evento",
                        editable: false,
                        allowBlank: false,
                        name: "typeEvent",
                        itemId: "typeEvent",
                        fieldCls: "obligatoryTextField",
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        displayField: "description",
                        valueField: "value",
                        store: {
                          fields: ["value", "description"],
                          pageSize: 9999,
                          autoLoad: true,
                          proxy: {
                            actionMethods: {
                              create: "POST",
                              read: "POST",
                              update: "POST"
                            },
                            type: "ajax",
                            url:
                              "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                            extraParams: {
                              propertyOrder: "description",
                              propertyFind: "identified",
                              valueFind: "EVENTTYPE"
                            },
                            reader: {
                              type: "json",
                              root: "data",
                              successProperty: "success"
                            }
                          }
                        }
                      },
                      {
                        xtype:"UpperCaseTextField",
                        anchor: "100%",
                        fieldLabel: "Descripci&oacute;n resumida",
                        msgTarget: "side",
                        name: "descriptionShort",
                        itemId: "descriptionShort",
                        fieldCls: "obligatoryTextField",
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        allowBlank: false,
                        labelWidth: 150
                      },
                      {
                        xtype: 'UpperCaseTextArea',
                        anchor: "100%",
                        name: "descriptionLarge",
                        itemId: "descriptionLarge",
                        fieldLabel: "Descripci&oacute;n detallada",
                        minLength: 50,
                        minLengthText: "Ingrese m&aacute;s de 50 caracteres",
                        msgTarget: "side",
                        fieldCls: "obligatoryTextField",
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        allowBlank: false,
                        height: 50,
                        labelWidth: 150
                      },
                      {
                        xtype: "datetimefield",
                        width: 360,
                        name: "dateDiscovery",
                        itemId: "dateDiscovery",
                        fieldLabel: "Fecha de preparaci&oacute;n",
                        msgTarget: "side",
                        fieldCls: "obligatoryTextField",
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        allowBlank: false,
                        format: "d/m/Y H:i",
                        value: new Date(),
                        labelWidth: 150
                      },
                      {
                        xtype: "datetimefield",
                        width: 360,
                        fieldLabel: "Fecha de ocurrencia",
                        msgTarget: "side",
                        name: "dateOccurrence",
                        itemId: "dateOccurrence",
                        fieldCls: "obligatoryTextField",
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        allowBlank: false,
                        format: "d/m/Y H:i",
                        value: new Date(),
                        labelWidth: 150
                      },
                      {
                        xtype: "container",
                        height: 28,
                        layout: {
                          type: "hbox",
                          align: "top"
                        },
                        items: [
                          {
                            xtype: "NumberDecimalNumberObligatory",
                            width: 360,
                            fieldLabel: "Monto",
                            labelWidth: 150,
                            name: "amountOrigin",
                            itemId: "amountOrigin",
                            allowBlank: false,
                            fieldCls: "numberPositive",
                            maxLength: 15,
                            listeners: {
                              specialkey: function(f, e) {
                                if (
                                  e.getKey() === e.ENTER ||
                                  e.getKey() === e.TAB
                                ) {
                                  me.down("#grossLoss").reset();
                                  calculateNetLoss(me);
                                }
                              },
                              blur: function() {
                                me.down("#grossLoss").reset();
                                calculateNetLoss(me);
                              }
                            }
                          },
                          {
                            xtype: "combobox",
                            flex: 0.3,
                            padding: "0 0 0 4",
                            msgTarget: "side",
                            fieldCls: "obligatoryTextField",
                            displayField: "description",
                            valueField: "idCurrency",
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            allowBlank: false,
                            editable: false,
                            forceSelection: true,
                            name: "currency",
                            itemId: "currency",
                            store: {
                              fields: ["idCurrency", "description"],
                              proxy: {
                                actionMethods: {
                                  create: "POST",
                                  read: "POST",
                                  update: "POST"
                                },
                                type: "ajax",
                                url:
                                  "http://localhost:9000/giro/showListCurrencyActivesComboBox.htm",
                                extraParams: {
                                  propertyOrder: "description"
                                },
                                reader: {
                                  type: "json",
                                  root: "data",
                                  successProperty: "success"
                                }
                              }
                            },
                            listeners: {
                              specialkey: function(f, e) {
                                DukeSource.global.DirtyView.focusEventEnter(
                                  f,
                                  e,
                                  me.down("#typeChange")
                                );
                              },
                              select: function(cbo) {
                                var typeChange = me.down("#typeChange");
                                if (cbo.getValue() === "1") {
                                  typeChange.setValue("1.00");
                                  typeChange.setFieldStyle(
                                    "background: #D8D8D8"
                                  );
                                  typeChange.setReadOnly(true);
                                  calculateNetLoss(me);
                                } else {
                                  typeChange.reset();
                                  me.down("#grossLoss").reset();
                                  typeChange.setFieldStyle(
                                    "background: #BFFEBF"
                                  );
                                  typeChange.setReadOnly(false);
                                }
                              }
                            }
                          },
                          {
                            xtype: "NumberDecimalNumberObligatory",
                            flex: 0.8,
                            allowBlank: false,
                            padding: "0 0 0 30",
                            fieldLabel: "Tipo de cambio",
                            decimalPrecision: 4,
                            name: "typeChange",
                            itemId: "typeChange",
                            maxLength: 8,
                            listeners: {
                              specialkey: function(f, e) {
                                if (
                                  e.getKey() === e.ENTER ||
                                  e.getKey() === e.TAB
                                ) {
                                  me.down("#grossLoss").reset();
                                  calculateNetLoss(me);
                                  DukeSource.global.DirtyView.focusEventEnter(
                                    f,
                                    e,
                                    me.down("#amountOrigin")
                                  );
                                }
                              },
                              blur: function() {
                                me.down("#grossLoss").reset();
                                calculateNetLoss(me);
                              }
                            }
                          },
                          {
                            xtype: "NumberDecimalNumberObligatory",
                            width: 200,
                            padding: "0 5 0 5",
                            name: "grossLoss",
                            itemId: "grossLoss",
                            allowBlank: false,
                            enforceMaxLength: true,
                            fieldCls: "numberNegative",
                            readOnly: true,
                            maxLength: 18
                          }
                        ]
                      },
                      {
                        xtype: "checkboxfield",
                        width: 300,
                        fieldLabel: "Queja",
                        name: "isSteal",
                        itemId: "isSteal",
                        labelWidth: 150,
                        boxLabel: "Si",
                        inputValue: "S",
                        uncheckedValue: "N",
                        value: "N"
                      },
                      {
                        xtype: 'UpperCaseTextArea',
                        anchor: "100%",
                        fieldLabel: "Propuesta de mitigaci&oacute;n",
                        fieldCls: "obligatoryTextField",
                        msgTarget: "side",
                        minLength: 20,
                        minLengthText: "Ingrese m&aacute;s de 20 caracteres",
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        allowBlank: false,
                        name: "mitigation",
                        itemId: "mitigation",
                        height: 50,
                        labelWidth: 150
                      },
                      {
                        xtype: "filefield",
                        width: 420,
                        name: "document",
                        itemId: "document",
                        fieldLabel: "Adjuntar documento",
                        labelWidth: 150
                      },
                      {
                        xtype: "container",
                        hidden: true,
                        itemId: "collaboratorApprove",
                        items: [
                          {
                            xtype: "displayfield",
                            anchor: "100%",
                            fieldLabel: "Aprobadores"
                          },
                          {
                            xtype: "gridpanel",
                            itemId: "gridApproveToCollaborator",
                            store: storeCollaboratorApprove,
                            titleAlign: "center",
                            columns: [
                              {
                                dataIndex: "approve",
                                align: "center",
                                text: "Estado",
                                width: 200
                              },
                              {
                                dataIndex: "fullName",
                                width: 300,
                                text: "Nombre"
                              },
                              {
                                dataIndex: "dateRegister",
                                width: 150,
                                align: "center",
                                text: "Fecha aprobaci&oacute;n"
                              },
                              {
                                dataIndex: "comments",
                                flex: 1,
                                text: "Comentario"
                              }
                            ],
                            bbar: {
                              xtype: "pagingtoolbar",
                              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                              store: storeCollaboratorApprove,
                              displayInfo: true,
                              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                            }
                          }
                        ]
                      }
                    ]
                  }
                ]
              },
              {
                id: "card-1",
                xtype: "container",
                layout: "anchor",
                items: [
                  {
                    xtype: "fieldset",
                    title: "Basilea",
                    items: [
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        itemId: "descriptionBusinessLineOne",
                        name: "descriptionBusinessLineOne",
                        fieldLabel: "L&iacute;nea de negocio",
                        labelWidth: 150,
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        allowBlank: false,
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeBusinessLine",
                                {
                                  winParent: me
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        itemId: "descriptionEventOne",
                        name: "descriptionEventOne",
                        fieldLabel: "Evento de riesgo",
                        labelWidth: 150,
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeTypeEvent",
                                {
                                  winParent: me
                                }
                              ).show();
                            });
                          }
                        }
                      }
                    ]
                  },
                  {
                    xtype: "fieldset",
                    title: "Detalle",
                    items: [
                      {
                        xtype: "container",
                        height: 27,
                        layout: "hbox",
                        items: [
                          {
                            xtype:"UpperCaseTextFieldReadOnly",
                            width: 500,
                            fieldLabel: "Persona que reporta",
                            msgTarget: "side",
                            fieldCls: "readOnlyText",
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            allowBlank: true,
                            itemId: "fullNameUserRegister",
                            name: "fullNameUserRegister",
                            padding: "0 10 0 0",
                            labelWidth: 150
                          },
                          {
                            xtype: "datetimefield",
                            format: "d/m/Y H:i",
                            fieldLabel: "Fecha de reporte",
                            itemId: "dateProcessTempOne",
                            name: "dateProcessTempOne",
                            allowBlank: true,
                            readOnly: EVENT_DATE_REGISTER_FIELD === "N",
                            enableKeyEvents: true,
                            flex: 1,
                            listeners: {
                              select: function(f) {
                                me.down("#dateAcceptLossEvent").setValue(
                                  f.getValue()
                                );
                              },
                              blur: function(f) {
                                me.down("#dateAcceptLossEvent").setValue(
                                  f.getValue()
                                );
                              }
                            }
                          },
                          {
                            xtype:"UpperCaseTextFieldReadOnly",
                            fieldLabel: "C&oacute;digo",
                            fieldCls: "codeIncident",
                            labelWidth: 50,
                            padding: "0 0 0 20",
                            name: "codeEvent",
                            itemId: "codeEvent",
                            value: "",
                            flex: 1
                          },
                          {
                            xtype: "button",
                            iconCls: "attach",
                            itemId: "attachDocument",
                            width: 25,
                            handler: function() {
                              var idEvent = me.down("#idEvent").getValue();
                              var codeEvent = me.down("#codeEvent").getValue();
                              var window = Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowEventsFileAttach",
                                {
                                  idEvent: idEvent,
                                  buttonsDisabled:
                                    category === DukeSource.global.GiroConstants.GESTOR,
                                  modal: true
                                }
                              );
                              window.title = window.title + " - " + codeEvent;
                              window.show();
                            }
                          }
                        ]
                      },
                      {
                        xtype:"UpperCaseTextFieldReadOnly",
                        width: 360,
                        fieldLabel: "Centro de costo",
                        itemId: "costCenterTempOne",
                        name: "costCenterTempOne",
                        fieldCls: "readOnlyText",
                        labelWidth: 150
                      },
                      {
                        xtype: "container",
                        height: 27,
                        layout: "hbox",
                        items: [
                          {
                            xtype: "textfield",
                            name: "tittleEvent",
                            itemId: "tittleEvent",
                            hidden: true
                          },
                          {
                            xtype:"UpperCaseTextFieldReadOnly",
                            name: "descriptionTittleEvent",
                            itemId: "descriptionTittleEvent",
                            flex: 1,
                            fieldLabel: "T&iacute;tulo del evento",
                            msgTarget: "side",
                            fieldCls: "readOnlyText",
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            allowBlank: true,
                            labelWidth: 150
                          },
                          {
                            xtype: "button",
                            iconCls: "search",
                            handler: function() {
                              var window = Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTitleEvent",
                                {}
                              ).show();
                              window
                                .down("grid")
                                .on("itemdblclick", function() {
                                  me.down("#descriptionTittleEvent").setValue(
                                    window
                                      .down("grid")
                                      .getSelectionModel()
                                      .getSelection()[0]
                                      .get("description")
                                  );
                                  me.down("#tittleEvent").setValue(
                                    window
                                      .down("grid")
                                      .getSelectionModel()
                                      .getSelection()[0]
                                      .get("id")
                                  );
                                  window.close();
                                });
                            }
                          }
                        ]
                      },
                      {
                        xtype: "checkboxfield",
                        fieldLabel: "P&eacute;rdida rop",
                        labelWidth: 150,
                        boxLabel: "Si",
                        name: "isOperational",
                        itemId: "isOperational",
                        inputValue: "S",
                        uncheckedValue: "N",
                        value: "N"
                      },
                      {
                        xtype: "datetimefield",
                        width: 360,
                        fieldLabel: "Fecha de finalizaci&oacute;n",
                        msgTarget: "side",
                        name: "dateCloseEvent",
                        itemId: "dateCloseEvent",
                        fieldCls: "obligatoryTextField",
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        allowBlank: true,
                        format: "d/m/Y H:i",
                        value: new Date(),
                        hidden: true,
                        labelWidth: 150
                      },
                      {
                        xtype: "combobox",
                        width: 360,
                        fieldLabel: "Tipo de p&eacute;rdida",
                        msgTarget: "side",
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        labelWidth: 150,
                        allowBlank: true,
                        name: "typeEventLost",
                        itemId: "typeEventLost",
                        fieldCls: "obligatoryTextField",
                        queryMode: "local",
                        displayField: "description",
                        valueField: "value",
                        store: {
                          fields: ["value", "description"],
                          pageSize: 9999,
                          autoLoad: true,
                          proxy: {
                            actionMethods: {
                              create: "POST",
                              read: "POST",
                              update: "POST"
                            },
                            type: "ajax",
                            url:
                              "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                            extraParams: {
                              propertyOrder: "description",
                              propertyFind: "identified",
                              valueFind: "TYPEEVENTLOST"
                            },
                            reader: {
                              type: "json",
                              root: "data",
                              successProperty: "success"
                            }
                          }
                        }
                      },
                      {
                        xtype: "ViewComboRiskType",
                        width: 360,
                        fieldLabel: "Riesgo relacionado",
                        labelStyle: "font-size:8pt;",
                        name: "riskType",
                        itemId: "riskType",
                        msgTarget: "side",
                        fieldCls: "obligatoryTextField",
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        allowBlank: true,
                        labelWidth: 150
                      },
                      {
                        xtype: "container",
                        height: 27,
                        layout: "hbox",
                        items: [
                          {
                            xtype: "textfield",
                            name: "effectEvent",
                            itemId: "effectEvent",
                            hidden: true
                          },
                          {
                            xtype:"UpperCaseTextFieldReadOnly",
                            name: "descriptionEffectEvent",
                            itemId: "descriptionEffectEvent",
                            flex: 1,
                            fieldLabel: "Tipo de efecto",
                            msgTarget: "side",
                            fieldCls: "readOnlyText",
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            allowBlank: true,
                            labelWidth: 150
                          },
                          {
                            xtype: "button",
                            iconCls: "search",
                            handler: function() {
                              var window = Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowEffectEvent",
                                {}
                              ).show();
                              window
                                .down("grid")
                                .on("itemdblclick", function() {
                                  me.down("#descriptionEffectEvent").setValue(
                                    window
                                      .down("grid")
                                      .getSelectionModel()
                                      .getSelection()[0]
                                      .get("description")
                                  );
                                  me.down("#effectEvent").setValue(
                                    window
                                      .down("grid")
                                      .getSelectionModel()
                                      .getSelection()[0]
                                      .get("id")
                                  );
                                  window.close();
                                });
                            }
                          }
                        ]
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        fieldLabel: "Causa",
                        itemId: "descriptionFactorRisk",
                        name: "descriptionFactorRisk",
                        labelWidth: 150,
                        value: "(Seleccionar)",
                        fieldCls: "style-for-url",
                        listeners: {
                          afterrender: function(view) {
                            view.getEl().on("click", function() {
                              Ext.create(
                                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeFactorRisk",
                                {
                                  winParent: me
                                }
                              ).show();
                            });
                          }
                        }
                      },
                      {
                        xtype: "textareafield",
                        anchor: "100%",
                        fieldLabel: "Descripci&oacute;n de la causa",
                        fieldCls: "obligatoryTextField",
                        msgTarget: "side",
                        itemId: "cause",
                        name: "cause",
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        allowBlank: true,
                        height: 50,
                        labelWidth: 150
                      }
                    ]
                  },
                  {
                    xtype: "fieldset",
                    itemId: "checksItems",
                    layout: {
                      type: "hbox",
                      pack: "center"
                    },
                    items: [
                      {
                        xtype: "checkboxfield",
                        itemId: "preliminaryManager",
                        style: "font-weight: bold;",
                        width: 200,
                        fieldLabel: "Borrador",
                        labelWidth: 100,
                        inputValue: "B",
                        uncheckedValue: "N",
                        listeners: {
                          change: function(chk) {
                            if (chk.getValue()) {
                              me.down("#stateTemp").setValue("B");
                              me.down("#rejectManager").setValue(false);
                              me.down("#approveManager").setValue(false);
                            } else {
                              setBackEventState(me);
                            }
                          }
                        }
                      },
                      {
                        xtype: "checkboxfield",
                        itemId: "rejectManager",
                        fieldLabel: "Rechazar",
                        width: 200,
                        labelWidth: 100,
                        style: "font-weight: bold;",
                        checked: false,
                        inputValue: "R",
                        uncheckedValue: "N",
                        listeners: {
                          change: function(chk) {
                            if (chk.getValue()) {
                              me.down("#motiveRefused").setVisible(true);
                              me.down("#descriptionRefused").setVisible(true);
                              me.down("#stateTemp").setValue("R");
                              me.down("#preliminaryManager").setValue(false);
                              me.down("#approveManager").setValue(false);
                              me.down("#motiveRefused").allowBlank = false;
                              me.down("#descriptionRefused").allowBlank = false;
                            } else {
                              setBackEventState(me);
                              me.down("#motiveRefused").allowBlank = true;
                              me.down("#descriptionRefused").allowBlank = true;
                              me.down("#motiveRefused").setVisible(false);
                              me.down("#descriptionRefused").setVisible(false);
                            }
                          }
                        }
                      },
                      {
                        xtype: "checkboxfield",
                        itemId: "approveManager",
                        fieldLabel: "Aprobar",
                        boxLabel: "(Env&iacute;o directo a Contabilidad)",
                        width: 400,
                        labelWidth: 100,
                        style: "font-weight: bold;",
                        checked: false,
                        inputValue: "AA",
                        uncheckedValue: "N",
                        listeners: {
                          change: function(chk) {
                            if (chk.getValue()) {
                              me.down("#stateTemp").setValue("AA");
                              me.down("#documentApproveManager").setVisible(
                                true
                              );
                              me.down("#descriptionRefused").setVisible(true);
                              me.down(
                                "#documentApproveManager"
                              ).allowBlank = false;
                              me.down("#descriptionRefused").allowBlank = false;
                              me.down("#preliminaryManager").setValue(false);
                              me.down("#rejectManager").setValue(false);
                            } else {
                              setBackEventState(me);
                              me.down(
                                "#documentApproveManager"
                              ).allowBlank = true;
                              me.down("#documentApproveManager").setVisible(
                                false
                              );
                              me.down("#descriptionRefused").allowBlank = true;
                              me.down("#descriptionRefused").setVisible(false);
                            }
                          }
                        }
                      }
                    ]
                  },
                  {
                    xtype: "combobox",
                    width: 500,
                    itemId: "motiveRefused",
                    name: "motiveRefused",
                    fieldLabel: "Motivo",
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    allowBlank: true,
                    hidden: true,
                    labelWidth: 150,
                    queryMode: "local",
                    displayField: "description",
                    valueField: "value",
                    store: {
                      fields: ["value", "description"],
                      pageSize: 9999,
                      autoLoad: true,
                      proxy: {
                        actionMethods: {
                          create: "POST",
                          read: "POST",
                          update: "POST"
                        },
                        type: "ajax",
                        url:
                          "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                        extraParams: {
                          propertyOrder: "description",
                          propertyFind: "identified",
                          valueFind: "REFUSEDEVENT"
                        },
                        reader: {
                          type: "json",
                          root: "data",
                          successProperty: "success"
                        }
                      }
                    },
                    listeners: {
                      select: function(cbo) {
                        me.down("#descriptionMotiveRefused").setValue(
                          cbo.getRawValue()
                        );
                      }
                    }
                  },
                  {
                    xtype: "textareafield",
                    anchor: "100%",
                    itemId: "descriptionRefused",
                    name: "descriptionRefused",
                    fieldLabel: "Comentario",
                    fieldCls: "obligatoryTextField",
                    msgTarget: "side",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    allowBlank: true,
                    hidden: true,
                    height: 50,
                    labelWidth: 150
                  },
                  {
                    xtype: "filefield",
                    width: 420,
                    hidden: true,
                    itemId: "documentApproveManager",
                    fieldLabel: "Adjuntar documento",
                    labelWidth: 150
                  },
                  {
                    xtype: "displayfield",
                    anchor: "100%",
                    fieldLabel: "Aprobadores"
                  },
                  {
                    xtype: "gridpanel",
                    itemId: "gridApprove",
                    store: storeManagerApprove,
                    titleAlign: "center",
                    columns: [
                      {
                        dataIndex: "approve",
                        align: "center",
                        text: "Estado",
                        width: 200
                      },
                      {
                        dataIndex: "fullName",
                        width: 300,
                        text: "Nombre"
                      },
                      {
                        dataIndex: "dateRegister",
                        width: 150,
                        align: "center",
                        text: "Fecha aprobaci&oacute;n"
                      },
                      {
                        dataIndex: "comments",
                        flex: 1,
                        text: "Comentario"
                      }
                    ],
                    bbar: {
                      xtype: "pagingtoolbar",
                      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                      store: storeManagerApprove,
                      displayInfo: true,
                      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                    }
                  }
                ]
              },
              {
                id: "card-2",
                xtype: "container",
                layout: "anchor",
                items: [
                  {
                    xtype: "fieldset",
                    title: "Informaci&oacute;n",
                    items: [
                      {
                        xtype: "container",
                        height: 27,
                        layout: "hbox",
                        items: [
                          {
                            xtype:"UpperCaseTextFieldReadOnly",
                            flex: 1,
                            fieldLabel: "T&iacute;tulo del evento",
                            itemId: "descriptionTittleEventTempOne",
                            name: "descriptionTittleEventTempOne",
                            fieldCls: "readOnlyText",
                            labelWidth: 150,
                            padding: "0 20 0 0"
                          },
                          {
                            xtype: "textfield",
                            fieldCls: "codeIncident",
                            itemId: "codeEventTempOne",
                            name: "codeEventTempOne",
                            width: 100
                          }
                        ]
                      },
                      {
                        xtype: "container",
                        height: 28,
                        layout: {
                          type: "hbox",
                          align: "top"
                        },
                        items: [
                          {
                            xtype:"UpperCaseTextFieldReadOnly",
                            width: 360,
                            fieldLabel: "Monto",
                            labelWidth: 150,
                            allowBlank: true,
                            name: "amountOriginTempOne",
                            itemId: "amountOriginTempOne",
                            fieldCls: "readOnlyText"
                          },
                          {
                            xtype:"UpperCaseTextFieldReadOnly",
                            width: 60,
                            padding: "0 0 0 4",
                            fieldCls: "readOnlyText",
                            name: "currencyTempOne",
                            itemId: "currencyTempOne"
                          }
                        ]
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        labelWidth: 150,
                        fieldLabel: "Organizaci&oacute;n interna",
                        itemId: "descriptionUnityTempOne",
                        name: "descriptionUnityTempOne"
                      },
                      {
                        xtype: 'UpperCaseTextArea',
                        anchor: "100%",
                        fieldLabel: "Descripci&oacute;n detallada",
                        fieldCls: "readOnlyText",
                        readOnly: true,
                        itemId: "descriptionLargeTempOne",
                        name: "descriptionLargeTempTwo",
                        height: 70,
                        labelWidth: 150
                      }
                    ]
                  },
                  {
                    xtype: "gridpanel",
                    itemId: "gridApprove",
                    store: storeManagerApprove,
                    titleAlign: "center",
                    columns: [
                      {
                        dataIndex: "approve",
                        align: "center",
                        text: "Estado",
                        width: 200
                      },
                      {
                        dataIndex: "fullName",
                        width: 300,
                        text: "Nombre"
                      },
                      {
                        dataIndex: "dateRegister",
                        width: 150,
                        align: "center",
                        text: "Fecha aprobaci&oacute;n"
                      },
                      {
                        dataIndex: "comments",
                        flex: 1,
                        text: "Comentario"
                      }
                    ],
                    bbar: {
                      xtype: "pagingtoolbar",
                      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                      store: storeManagerApprove,
                      displayInfo: true,
                      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                    }
                  }
                ]
              },
              {
                id: "card-3",
                xtype: "container",
                layout: "anchor",
                items: [
                  {
                    xtype: "fieldset",
                    title: "Informaci&oacute;n",
                    items: [
                      {
                        xtype: "container",
                        height: 27,
                        layout: "hbox",
                        items: [
                          {
                            xtype:"UpperCaseTextFieldReadOnly",
                            flex: 1,
                            fieldLabel: "T&iacute;tulo del evento",
                            itemId: "descriptionTittleEventTempTwo",
                            name: "descriptionTittleEventTempTwo",
                            fieldCls: "readOnlyText",
                            labelWidth: 150,
                            padding: "0 20 0 0"
                          },
                          {
                            xtype: "textfield",
                            fieldCls: "codeIncident",
                            itemId: "codeEventTempTwo",
                            name: "codeEventTempTwo",
                            width: 100
                          }
                        ]
                      },
                      {
                        xtype: "container",
                        height: 28,
                        layout: {
                          type: "hbox",
                          align: "top"
                        },
                        items: [
                          {
                            xtype:"UpperCaseTextFieldReadOnly",
                            width: 360,
                            fieldLabel: "Monto",
                            labelWidth: 150,
                            itemId: "amountOriginTempTwo",
                            name: "amountOriginTempTwo",
                            fieldCls: "readOnlyText"
                          },
                          {
                            xtype:"UpperCaseTextFieldReadOnly",
                            width: 60,
                            padding: "0 0 0 4",
                            fieldCls: "readOnlyText",
                            itemId: "currencyTempTwo",
                            name: "currencyTempTwo"
                          }
                        ]
                      },
                      {
                        xtype: "displayfield",
                        anchor: "100%",
                        labelWidth: 150,
                        fieldLabel: "Organizaci&oacute;n interna",
                        itemId: "descriptionUnityTempTwo",
                        name: "descriptionUnityTempTwo"
                      },
                      {
                        xtype: 'UpperCaseTextArea',
                        anchor: "100%",
                        fieldLabel: "Descripci&oacute;n detallada",
                        fieldCls: "readOnlyText",
                        readOnly: true,
                        height: 70,
                        labelWidth: 150,
                        itemId: "descriptionLargeTempTwo",
                        name: "descriptionLargeTempTwo"
                      }
                    ]
                  },
                  {
                    xtype: "fieldset",
                    title: "Detalle",
                    items: [
                      {
                        xtype:"UpperCaseTextFieldReadOnly",
                        width: 400,
                        fieldLabel: "Centro de costo",
                        itemId: "costCenterTempTwo",
                        name: "costCenterTempTwo",
                        fieldCls: "readOnlyText",
                        labelWidth: 150
                      },
                      {
                        xtype: "container",
                        height: 28,
                        layout: "hbox",
                        items: [
                          {
                            xtype: "textfield",
                            name: "idPlanAccountLoss",
                            itemId: "idPlanAccountLoss",
                            hidden: true
                          },
                          {
                            xtype: "TextFieldNumberFormatReadOnly",
                            width: 400,
                            fieldLabel: "Cuenta contable",
                            name: "planAccountLoss",
                            itemId: "planAccountLoss",
                            msgTarget: "side",
                            fieldCls: "obligatoryTextField",
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            allowBlank: true,
                            labelWidth: 150
                          },
                          {
                            xtype: "button",
                            iconCls: "search",
                            handler: function() {
                              var window = Ext.create(
                                "DukeSource.view.risk.parameter.windows.ViewWindowSearchPlanAccountCountable",
                                { modal: true }
                              ).show();
                              window
                                .down("grid")
                                .on("itemdblclick", function() {
                                  me.down("#planAccountLoss").setValue(
                                    window
                                      .down("grid")
                                      .getSelectionModel()
                                      .getSelection()[0]
                                      .get("codeAccount")
                                  );
                                  me.down("#idPlanAccountLoss").setValue(
                                    window
                                      .down("grid")
                                      .getSelectionModel()
                                      .getSelection()[0]
                                      .get("idAccountPlan")
                                  );
                                  window.close();
                                });
                            }
                          }
                        ]
                      },
                      {
                        xtype: 'UpperCaseTextArea',
                        anchor: "100%",
                        fieldLabel: "Glosa",
                        name: "comments",
                        msgTarget: "side",
                        fieldCls: "obligatoryTextField",
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        allowBlank: true,
                        height: 70,
                        labelWidth: 150
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ],
        tbar: [],
        buttons: [
          {
            text: "Salir",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          },
          {
            text: "Guardar",
            hidden: true,
            iconCls: "save",
            itemId: "saveEventOperational",
            scale: "medium",
            handler: function() {
              calculateNetLoss(me);
              var panel;

              if (category === DukeSource.global.GiroConstants.COLLABORATOR) {
                panel = Ext.ComponentQuery.query("ViewPanelEventsManager")[0];
                panel.down("#eventState").reset();
              } else if (category === DukeSource.global.GiroConstants.GESTOR) {
                panel = Ext.ComponentQuery.query("ViewPanelEventsManager")[0];
              } else {
                panel = Ext.ComponentQuery.query(
                  "ViewPanelEventsRiskOperational"
                )[0];
                if (me.down("#rejectManager").getValue()) {
                  me.down("#riskType").allowBlank = true;
                  me.down("#descriptionTittleEvent").allowBlank = true;
                  me.down("#typeEventLost").allowBlank = true;
                  me.down("#descriptionEffectEvent").allowBlank = true;
                  me.down("#cause").allowBlank = true;
                  me.down("#descriptionBusinessLineOne").setValue("");
                  me.down("#descriptionEventOne").setValue("");
                } else {
                  if (me.down("#descriptionBusinessLineOne") === "") {
                    me.down("#descriptionBusinessLineOne").setValue(
                      "(Seleccionar)"
                    );
                  }
                  if (me.down("#descriptionEventOne") === "") {
                    me.down("#descriptionEventOne").setValue("(Seleccionar)");
                  }
                  me.down("#riskType").allowBlank = false;
                  me.down("#descriptionTittleEvent").allowBlank = false;
                  me.down("#typeEventLost").allowBlank = false;
                  me.down("#descriptionEffectEvent").allowBlank = false;
                  me.down("#cause").allowBlank = false;
                }
              }
              var idDetailIncidents;
              if (
                Ext.ComponentQuery.query(
                  "ViewPanelIncidentsRiskOperational"
                )[0] !== undefined
              ) {
                idDetailIncidents = me
                  .down("textfield[name=idDetailIncidents]")
                  .getValue();
                me.down("textfield[name=idDetailIncidents]").getValue();
              } else {
                idDetailIncidents = "";
              }
              var form = me.down("form");
              if (form.getForm().isValid()) {
                if (category === DukeSource.global.GiroConstants.COLLABORATOR) {
                  if (
                    me.down("#descriptionUnity").getValue() ===
                      "(Seleccionar)" ||
                    me.down("#descriptionAgency").getValue() ===
                      "(Seleccionar)" ||
                    me.down("#descriptionProduct").getValue() ===
                      "(Seleccionar)" ||
                    me.down("#descriptionProcess").getValue() ===
                      "(Seleccionar)"
                  ) {
                    DukeSource.global.DirtyView.messageWarning(
                      "Debe ingresar al menos un nivel de:<br><br>1. Organizaci&oacute;n interna<br>2. Oficina (ub. geogr&aacute;fica)<br>3. Producto afectado<br>4. Proceso afectado"
                    );
                  } else {
                    saveEvent(me, form, panel, idDetailIncidents);
                  }
                } else if (category === DukeSource.global.GiroConstants.ANALYST) {
                  if (
                    me.down("#descriptionBusinessLineOne").getValue() ===
                      "(Seleccionar)" ||
                    me.down("#descriptionEventOne").getValue() ===
                      "(Seleccionar)"
                  ) {
                    DukeSource.global.DirtyView.messageWarning(
                      "Debe ingresar al menos un nivel de:<br><br>1. L&iacute;nea de negocio<br>2. Evento de riesgo"
                    );
                  } else {
                    saveEvent(me, form, panel, idDetailIncidents);
                  }
                }
              } else {
                form.query(".textfield").forEach(function(c) {
                  if (!c.allowBlank) {
                    if (!c.getValue()) console.log(c.getName());
                  }
                });

                DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
              }
            }
          }
        ],
        buttonAlign: "center",
        listeners: {
          afterrender: function(view) {
            calculateNetLoss(view);
          },
          show: function(view) {
            checkItems(view);
          }
        }
      });
      me.callParent(arguments);
    }
  }
);

function saveEvent(me, form, panel, idDetailIncidents) {
  form.getForm().submit({
    url:
      "http://localhost:9000/giro/saveEventMasterWizard.htm?nameView=ViewPanelEventsRiskOperational",
    waitMsg: "Saving...",
    method: "POST",
    success: function(form, action) {
      var valor = Ext.decode(action.response.responseText);
      if (valor.success) {
        DukeSource.global.DirtyView.messageNormal(valor.message);
        me.close();
        if (panel !== undefined) {
          var grid = panel.down("grid");
          grid.down("pagingtoolbar").doRefresh();
        }
      } else {
        DukeSource.global.DirtyView.messageWarning(valor.message);
      }
    },
    failure: function(form, action) {
      var valor = Ext.decode(action.response.responseText);
      if (!valor.success) {
        DukeSource.global.DirtyView.messageWarning(valor.message);
      }
    }
  });
}

function calculateNetLoss(me) {
  var typeChange = me.down("#typeChange").getValue();
  var amountOrigin = me.down("#amountOrigin").getValue();
  var grossLoss = me.down("#grossLoss");
  var spend =
    me.down("#totalAmountSpend") === undefined
      ? "0.00"
      : me.down("#totalAmountSpend").getValue();
  var subEvent =
    me.down("#amountSubEvent") === undefined
      ? "0.00"
      : me.down("#amountSubEvent").getValue();
  grossLoss.setValue(parseFloat(amountOrigin) * parseFloat(typeChange));
  var lossRecovery =
    me.down("#lossRecovery") === undefined
      ? "0.00"
      : me.down("#lossRecovery").getValue();
  var thirdRecovery =
    me.down("#amountThirdRecovery") === undefined
      ? "0.00"
      : me.down("#amountThirdRecovery").getValue();
  var sureAmountRecovery =
    me.down("#sureAmountRecovery") === undefined
      ? "0.00"
      : me.down("#sureAmountRecovery").getValue();
  var totalRecovery =
    parseFloat(lossRecovery) +
    parseFloat(sureAmountRecovery) +
    parseFloat(thirdRecovery);
  var result =
    parseFloat(grossLoss.getValue()) - totalRecovery + parseFloat(spend);

  console.log();

  me.down("#totalAmountSpend").setValue(spend);
  me.down("#amountSubEvent").setValue(subEvent);
  me.down("#lossRecovery").setValue(lossRecovery);
  me.down("#amountThirdRecovery").setValue(thirdRecovery);
  me.down("#sureAmountRecovery").setValue(sureAmountRecovery);
  me.down("#totalRecovery").setValue(totalRecovery);
  me.down("#netLoss").setValue(result);
}

function checkItems(me) {
  var toolbar = me.down("toolbar");
  var form = me.down("form");
  DukeSource.lib.Ajax.request({
    method: "POST",
    url: "http://localhost:9000/giro/findStateIncident.htm",
    params: {
      propertyFind: "si.typeIncident",
      valueFind: DukeSource.global.GiroConstants.EVENT,
      propertyOrder: "si.sequence",
      start: 0,
      limit: 100
    },
    success: function(response) {
      response = Ext.decode(response.responseText);
      if (response.success) {
        createToolBar(response, toolbar, me, form);
        inactivateComponents(me);
      } else {
        DukeSource.global.DirtyView.messageWarning(response.message);
      }
    },
    failure: function() {
      DukeSource.global.DirtyView.messageWarning(response.message);
    }
  });
}

function inactivateComponents(me, card) {
  if (card !== undefined) {
    var a = me.down("form").items.items[card];
    a.query(".textfield").forEach(function(c) {
      DukeSource.global.DirtyView.toReadOnly(c);
    });
    a.query(".button").forEach(function(c) {
      c.setDisabled(true);
    });
  }
}

function createToolBar(response, toolbar, me, form) {
  var items = response.data;
  for (var i = 1; i < items.length; i++) {
    toolbar.add({
      text: items[i].abbreviation,
      index: items[i].sequence,
      idState: items[i].id,
      hidden: true,
      action: "navigateEventState",
      listeners: {
        click: function(btn) {
          toolbar.query(".button").forEach(function(c) {
            c.getEl()
              .down(".x-btn-inner")
              .setStyle({ color: "black" });
          });
          btn
            .getEl()
            .down(".x-btn-inner")
            .setStyle({ color: "blue" });
        }
      }
    });
  }

  var stateTemp = me.down("#stateTemp").getValue();
  var record = me.record;
  var sequenceEvent =
    record === undefined ? -1 : record.get("sequenceEventState");

  toolbar.query(".button").forEach(function(c) {
    var index = parseInt(c.index);
    var idState = parseInt(c.idState);
    var card = index - 1;
    if (me.new) {
      if (index === 1) {
        c.setVisible(me.new);
        form.down("#eventState").setValue(idState);
      }
      me.down("#saveEventOperational").setVisible(true);
    } else {
      if (category === DukeSource.global.GiroConstants.COLLABORATOR) {
        if (index === 1 && sequenceEvent === 1) {
          c.setVisible(true);
          form.getLayout().setActiveItem(card);
          me.down("#saveEventOperational").setVisible(true);
        } else if (sequenceEvent > 1) {
          index === 1 ? c.setVisible(true) : c.setVisible(false);
          me.down("#saveEventOperational").setVisible(false);
          inactivateComponents(me, card);
        }
        stateTemp === "B"
          ? c.setIconCls("txt")
          : stateTemp === "R"
          ? c.setIconCls("spend")
          : c.setIconCls("evaluate");
      }
      if (category === DukeSource.global.GiroConstants.GESTOR) {
        if (index <= sequenceEvent) {
          c.setVisible(true);
          form.getLayout().setActiveItem(card);
          inactivateComponents(me, card);
          me.down("#attachDocument").setDisabled(false);
          stateTemp === "B"
            ? c.setIconCls("txt")
            : stateTemp === "R"
            ? c.setIconCls("spend")
            : c.setIconCls("evaluate");
        } else {
          c.setVisible(false);
        }
      }
      if (category === DukeSource.global.GiroConstants.ANALYST) {
        if (sequenceEvent <= 2) {
          if (index === 1 && sequenceEvent === 1) {
            form.getLayout().setActiveItem(card);
            me.down("#saveEventOperational").setVisible(true);
            c.setVisible(true);
            stateTemp === "A"
              ? c.setIconCls("evaluate")
              : stateTemp === "B"
              ? c.setIconCls("txt")
              : c.setIconCls("spend");
          }
          if (index === 2 && sequenceEvent === 1) {
            form.getLayout().setActiveItem(card);
            c.setVisible(true);
            me.down("#saveEventOperational").setVisible(true);
          }
          if (index === 1 && sequenceEvent === 2) {
            c.setIconCls("evaluate");
            c.setVisible(true);
            me.down("#saveEventOperational").setVisible(true);
          }
          if (index === 2 && sequenceEvent === 2) {
            form.getLayout().setActiveItem(card);
            c.setVisible(true);
            me.down("#saveEventOperational").setVisible(true);
            stateTemp === "A"
              ? c.setIconCls("evaluate")
              : stateTemp === "B"
              ? c.setIconCls("txt")
              : c.setIconCls("spend");
          }
        } else if (index <= sequenceEvent) {
          c.setVisible(true);
          form.getLayout().setActiveItem(card);
          c.setIconCls("evaluate");
          inactivateComponents(me, card);
        }
      }
      if (userRole === DukeSource.global.GiroConstants.ACCOUNTANT) {
        me.down("#saveEventOperational").setVisible(false);
        index <= sequenceEvent ? c.setVisible(true) : c.setVisible(false);
        index <= sequenceEvent ? c.setIconCls("evaluate") : "";
        inactivateComponents(me, card);
      }
    }
  });
}

function setBackEventState(me) {
  if (
    me.down("#rejectManager").getValue() === false &&
    me.down("#approveManager").getValue() === false &&
    me.down("#preliminaryManager").getValue() === false
  ) {
    me.down("#stateTemp").setValue("A");
  }
}
