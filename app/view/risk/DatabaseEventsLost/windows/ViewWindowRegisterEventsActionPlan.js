Ext.define("ModelGridPanelAssigned", {
  extend: "Ext.data.Model",
  fields: [
    "idEventActionPlan",
    "idEvent",
    "idActionPlan",
    "description",
    "state",
    "codeEvent",
    "descriptionShort",
    "descriptionLarge",
    "netLoss",
    "codePlan",
    "descriptionPlan",
    "percentMonitoring",
    "fullNameEmitted",
    "userEmitted",
    "emailEmitted",
    "fullNameReceptor",
    "userReceptor",
    "emailReceptor"
  ]
});
var StoreGridActionPlan = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelAssigned",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterEventsActionPlan",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowRegisterEventsActionPlan",
    height: 450,
    width: 900,
    layout: "border",
    border: false,
    title: "Relaci&oacute;n de eventos con planes de acci&oacute;n",
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      var idEvent = this.idEvent;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            region: "center",
            itemId: "gridActionPlanAssigned",
            width: 450,
            store: StoreGridActionPlan,
            tbar: [
              {
                xtypoe: "button",
                iconCls: "add",
                text: "Vincular",
                handler: function() {
                  var windowActionPlan = Ext.create(
                    "DukeSource.view.fulfillment.window.ViewWindowSearchActionPlan",
                    {
                      modal: false,
                      buttonAlign: "center",
                      buttons: [
                        {
                          text: "Vincular",
                          iconCls: "save",
                          scale: "medium",
                          handler: function() {
                            var recordActionPlan = windowActionPlan
                              .down("grid")
                              .getSelectionModel()
                              .getSelection()[0];
                            var gridAction = me.down("#gridActionPlanAssigned");
                            var records = gridAction.getStore().getRange();
                            var total = gridAction.getStore().getCount();

                            if (
                              windowActionPlan
                                .down("grid")
                                .getSelectionModel()
                                .getCount() === 0
                            ) {
                              DukeSource.global.DirtyView.messageWarning(
                                "Seleccione un plan de acci&oacute;n por favor"
                              );
                            } else {
                              var equals;
                              for (var int = 0; int < total; int++) {
                                if (
                                  records[int].get("idActionPlan") ===
                                  recordActionPlan.get("id")
                                ) {
                                  equals = true;
                                  break;
                                } else {
                                  equals = false;
                                }
                              }
                              if (equals) {
                                DukeSource.global.DirtyView.messageWarning(
                                  "Se encuentra asignada, seleccione otro plan de acción por favor"
                                );
                              } else {
                                DukeSource.lib.Ajax.request({
                                  method: "POST",
                                  url:
                                    "http://localhost:9000/giro/saveRelationActionPlanEvent.htm",
                                  params: {
                                    idActionPlan: recordActionPlan.get(
                                      "idDocument"
                                    ),
                                    idEvent: idEvent
                                  },
                                  success: function(response) {
                                    response = Ext.decode(
                                      response.responseText
                                    );
                                    if (response.success) {
                                      Ext.MessageBox.show({
                                        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                                        msg:
                                          response.message +
                                          ", desea vincular mas planes de acci&oacute;n ?",
                                        icon: Ext.Msg.QUESTION,
                                        buttonText: {
                                          yes: "Si"
                                        },
                                        buttons: Ext.MessageBox.YESNO,
                                        fn: function(btn) {
                                          if (btn !== "yes") {
                                            windowActionPlan.close();
                                          }
                                        }
                                      });
                                      me.down("#gridActionPlanAssigned")
                                        .down("pagingtoolbar")
                                        .moveFirst();
                                    } else {
                                      DukeSource.global.DirtyView.messageWarning(
                                        response.message
                                      );
                                    }
                                  }
                                });
                              }
                            }
                          }
                        },
                        {
                          text: "Salir",
                          scale: "medium",
                          iconCls: "logout",
                          handler: function() {
                            windowActionPlan.close();
                          }
                        }
                      ]
                    }
                  ).show();
                }
              },
              {
                xtype: "button",
                iconCls: "delete",
                text: "Eliminar",
                handler: function() {
                  var grid = me.down("#gridActionPlanAssigned");
                  var assigned = grid.getSelectionModel().getSelection()[0];
                  if (assigned === undefined) {
                    DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
                  } else {
                    Ext.MessageBox.show({
                      title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                      msg: "Esta seguro que desea eliminar el riesgo",
                      icon: Ext.Msg.QUESTION,
                      buttonText: {
                        yes: "Si"
                      },
                      buttons: Ext.MessageBox.YESNO,
                      fn: function(btn) {
                        if (btn === "yes") {
                          DukeSource.lib.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/deleteRelationActionPlanEvent.htm",
                            params: {
                              idActionPlanEvent: assigned.get(
                                "idEventActionPlan"
                              ),
                              idActionPlan: assigned.get("idActionPlan"),
                              idEvent: assigned.get("idEvent")
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              if (response.success) {
                                DukeSource.global.DirtyView.messageNormal(response.message);
                                grid.down("pagingtoolbar").doRefresh();
                              } else {
                                DukeSource.global.DirtyView.messageWarning(response.message);
                              }
                            },
                            failure: function() {}
                          });
                        }
                      }
                    });
                  }
                }
              }
            ],
            columns: [
              {
                header: "Seguimiento",
                dataIndex: "percentMonitoring",
                width: 120,
                align: "center",
                renderer: function(value, metaData, record) {
                  return changeMonitoring(value, record, metaData);
                }
              },
              {
                header: "C&oacute;digo",
                dataIndex: "codePlan",
                width: 60
              },
              {
                header: "Plan de acci&oacute;n",
                dataIndex: "descriptionPlan",
                flex: 1
              },
              {
                header: "Responsable",
                dataIndex: "fullNameReceptor",
                width: 150
              },
              {
                header: "Remitente",
                dataIndex: "fullNameEmitted",
                width: 150
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridActionPlan,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
              doRefresh: function() {
                var grid = this,
                  current = grid.store.currentPage;
                if (grid.fireEvent("beforechange", grid, current) !== false) {
                  grid.store.loadPage(current);
                }
              }
            },
            listeners: {
              render: function() {
                var grid = me.down("#gridActionPlanAssigned");
                grid.store.getProxy().extraParams = {
                  idEvent: idEvent
                };
                grid.store.getProxy().url =
                  "http://localhost:9000/giro/showRelationActionPlanEvent.htm";
                grid.down("pagingtoolbar").moveFirst();
              }
            }
          }
        ],
        buttons: [
          {
            text: "Salir",
            scale: "medium",
            iconCls: "logout",
            handler: function() {
              me.close();
            }
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);

function changeMonitoring(v) {
  var tmpValue = v / 100;
  var tmpText = v + "%";
  var progressRenderer = (function() {
    var b = new Ext.ProgressBar();
    if (tmpValue <= 0.3334) {
      b.baseCls = "x-taskBar";
    } else if (tmpValue <= 0.6667) {
      b.baseCls = "x-taskBar-medium";
    } else {
      b.baseCls = "x-taskBar-high";
    }
    return function(pValue, pText) {
      b.updateProgress(pValue, pText, true);
      return Ext.DomHelper.markup(b.getRenderTree());
    };
  })(tmpValue, tmpText);
  return progressRenderer(tmpValue, tmpText);
}
