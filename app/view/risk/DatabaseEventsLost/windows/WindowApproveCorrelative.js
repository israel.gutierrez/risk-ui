Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.WindowApproveCorrelative', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowApproveCorrelative',
    height: 250,
    width: 600,
    layout: {
        type: 'fit'
    },
    requires: [
        'Ext.ux.DateTimeField'
    ],
    title: 'Aprobación',
    titleAlign: 'center',
    border: false,
    initComponent: function () {
        var me = this;
        var actionExecute = this.actionExecute;
        var id = this.id;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'textfield',
                            hidden: true,
                            name: 'id',
                            value: id
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            name: 'module',
                            value: me.module,
                            itemId: 'module'
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            name: 'parentModule',
                            itemId: 'parentModule'
                        },
                        {
                            xtype: 'datetimefield',
                            value: new Date(),
                            fieldCls: 'readOnlyText',
                            format: 'd/m/Y H:i',
                            readOnly: true,
                            labelWidth: 150,
                            fieldLabel: 'Fecha de aprobación',
                            itemId: 'dateApprove',
                            name: 'dateApprove'
                        },
                        {
                            xtype: 'textfield',
                            value: userName,
                            fieldCls: 'readOnlyText',
                            readOnly: true,
                            labelWidth: 150,
                            fieldLabel: 'Usuario que aprueba',
                            itemId: 'userApprove',
                            name: 'userApprove'
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            maxLengthText: DukeSource.global.GiroMessages.MESSAGE_MAX_CHARACTER + 800,
                            maxLength: 800,
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            allowBlank: false,
                            fieldCls: 'obligatoryTextField',
                            labelWidth: 150,
                            height: 100,
                            fieldLabel: 'Comentarios de la aprobación',
                            name: 'commentApprove',
                            itemId: 'commentApprove'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    itemId: 'saveApprove',
                    scale: 'medium',
                    action: actionExecute,
                    iconCls: 'save'
                },
                {
                    text: 'Salir',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center',
            listeners: {
                render: function (f, o) {
                    f.down('#commentApprove').focus(false, 200);
                }
            }
        });

        me.callParent(arguments);
    }

});