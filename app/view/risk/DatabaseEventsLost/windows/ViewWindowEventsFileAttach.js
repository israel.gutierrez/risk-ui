Ext.define("ModelFileAttach", {
  extend: "Ext.data.Model",
  fields: ["correlative", "idEvent", "nameFile", "nameUser"]
});
var storeFileAttach = Ext.create("Ext.data.Store", {
  model: "ModelFileAttach",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListFileAttachmentsOfActivityAdvance.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowEventsFileAttach",
  {
    extend: "Ext.window.Window",
    border: false,
    alias: "widget.ViewWindowEventsFileAttach",
    height: 383,
    width: 589,

    layout: {
      align: "stretch",
      type: "vbox"
    },
    title: "Documentos adjuntos",
    initComponent: function() {
      var idEvent = this.idEvent;
      var buttonsDisabled = this.buttonsDisabled;
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            border: false,
            bodyStyle: {
              background: "#dfe8f6"
            },
            padding: 5,
            disabled: buttonsDisabled === undefined ? false : buttonsDisabled,
            items: [
              {
                xtype: "container",
                layout: {
                  type: "hbox"
                },
                height: 25,
                items: [
                  {
                    xtype: "textfield",
                    name: "correlative",
                    value: "id",
                    hidden: true
                  },
                  {
                    xtype: "filefield",
                    anchor: "100%",
                    allowBlank: false,
                    msgTarget: "side",
                    fieldCls: "obligatoryTextField",
                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                    itemId: "text",
                    flex: 1,
                    name: "text",
                    buttonConfig: {
                      iconCls: "tesla even-attachment"
                    },
                    buttonText: ""
                  }
                ]
              }
            ]
          },
          {
            xtype: "gridpanel",
            padding: "8 0 0 0",
            store: storeFileAttach,
            flex: 1,
            tbar: [
              "->",
              {
                text: "Eliminar",
                iconCls: "delete",
                disabled:
                  buttonsDisabled === undefined ? false : buttonsDisabled,
                handler: function() {
                  var grid = me.down("grid");
                  var record = grid.getSelectionModel().getSelection()[0];

                  if (record !== undefined) {
                    Ext.Ajax.request({
                      method: "POST",
                      url:
                        "http://localhost:9000/giro/deleteFileAttachmentsEvent.htm",
                      params: {
                        correlative: record.get("correlative"),
                        idEvent: record.get("idEvent")
                      },
                      success: function(response) {
                        response = Ext.decode(response.responseText);
                        if (response.success) {
                          grid.store.getProxy().url =
                            "http://localhost:9000/giro/showListFileAttachmentsEvent.htm";
                          grid.store.getProxy().extraParams = {
                            idEvent: idEvent
                          };
                          grid.down("pagingtoolbar").moveFirst();
                        } else {
                          DukeSource.global.DirtyView.messageWarning(response.message);
                        }
                      },
                      failure: function(form, action) {}
                    });
                  } else {
                    DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
                  }
                }
              },
              {
                xtype: "button",
                text: "Guardar",
                iconCls: "save",
                width: 90,
                disabled:
                  buttonsDisabled === undefined ? false : buttonsDisabled,
                handler: function() {
                  var form = me.down("form");
                  var grid = me.down("grid");

                  if (form.getForm().isValid()) {
                    form.getForm().submit({
                      url:
                        "http://localhost:9000/giro/saveFileAttachmentsEvent.htm?nameView=ViewPanelEventsRiskOperational" +
                        "&idEvent=" +
                        idEvent,
                      waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
                      method: "POST",
                      success: function(form, action) {
                        var valor = Ext.decode(action.response.responseText);
                        if (valor.success) {
                          DukeSource.global.DirtyView.messageAlert(valor.message);

                          grid.store.getProxy().extraParams = {
                            idEvent: idEvent
                          };

                          grid.store.getProxy().url =
                            "http://localhost:9000/giro/showListFileAttachmentsEvent.htm";
                          grid.down("pagingtoolbar").doRefresh();
                        } else {
                          DukeSource.global.DirtyView.messageWarning(valor.message);
                        }
                      },
                      failure: function(form, action) {
                        var valor = Ext.decode(action.response.responseText);
                        if (!valor.success) {
                          DukeSource.global.DirtyView.messageWarning(valor.message);
                        }
                      }
                    });
                  } else {
                    DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
                  }
                }
              }
            ],
            columns: [
              {
                xtype: "rownumberer",
                width: 25,
                sortable: false
              },
              {
                dataIndex: "nameUser",
                flex: 1,
                text: "Nombre"
              },
              {
                dataIndex: "nameFile",
                flex: 1,
                text: "Archivo"
              },
              {
                xtype: "actioncolumn",
                header: "Documento",
                align: "center",
                width: 80,
                items: [
                  {
                    iconCls: "documentDownload",
                    handler: function(grid, rowIndex) {
                      Ext.core.DomHelper.append(document.body, {
                        tag: "iframe",
                        id: "downloadIframe",
                        frameBorder: 0,
                        width: 0,
                        height: 0,
                        css: "display:none;visibility:hidden;height:0px;",
                        src:
                          "http://localhost:9000/giro/downloadFileAttachmentsEvent.htm?correlative=" +
                          grid.store.getAt(rowIndex).get("correlative") +
                          "&idEvent=" +
                          grid.store.getAt(rowIndex).get("idEvent") +
                          "&nameFile=" +
                          grid.store.getAt(rowIndex).get("nameFile")
                      });
                    }
                  }
                ]
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: 50,
              store: storeFileAttach,
              items: [
                {
                  xtype:"UpperCaseTrigger",
                  width: 120,
                  action: "searchGridAllAgency"
                }
              ]
            },
            listeners: {
              render: function() {
                var me = this;
                me.store.getProxy().extraParams = {
                  idEvent: idEvent
                };
                me.store.getProxy().url =
                  "http://localhost:9000/giro/showListFileAttachmentsEvent.htm";
                me.down("pagingtoolbar").moveFirst();
              }
            }
          }
        ],
        buttons: [
          {
            text: "Salir",
            scale: "medium",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);
