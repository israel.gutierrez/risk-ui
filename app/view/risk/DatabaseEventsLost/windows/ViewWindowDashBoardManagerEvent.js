Ext.define("ModelGridDashBoardManagerEvent", {
  extend: "Ext.data.Model",
  fields: [
    "idEvent",
    "codeEvent",
    "workArea",
    "isRiskEventIncidents",
    "idBusinessLineTwo",
    "businessLineOne",
    "factorRisk",
    "idEventThree",
    "eventTwo",
    "eventOne",
    "managerRisk",
    "agency",
    "currency",
    "riskType",
    "businessLineThree",
    "actionPlan",
    "process",
    "descriptionProcess",
    "idSubProcess",
    "descriptionSubProcess",
    "insuranceCompany",
    "eventState",
    "nameEventState",
    "colorEventState",
    "sequenceEventState",
    "eventType",
    "lossType",
    "totalLossRecovery",
    "totalLoss",
    "accountingEntryLoss",
    "accountingEntryRecoveryLoss",
    "descriptionLargeEvent",
    "descriptionShortEvent",
    "descriptionAgency",
    "descriptionRegion",
    "descriptionWorkArea",
    {
      name: "dateOccurrence",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateAcceptLossEvent",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateCloseEvent",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateDiscovery",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateAccountingEvent",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    "entityPenalizesLoss",
    "grossLoss",
    "lossRecovery",
    "sureAmountRecovery",
    "totalAmountSpend",
    "amountSubEvent",
    "sureAmount",
    "netLoss",
    "typeChange",
    "typeEvent",
    "nameEventThree",
    "nameEventTwo",
    "nameEventOne",
    "nameFactorRisk",
    "nameAction",
    "nameCurrency",
    "descriptionShort",
    "idPlanAccountSureAmountRecovery",
    "planAccountSureAmountRecovery",
    "planAccountLossRecovery",
    "idPlanAccountLossRecovery",
    "bookkeepingEntryLossRecovery",
    "bookkeepingEntrySureAmountRecovery",
    "amountOrigin",
    "codeGeneralIncidents",
    "idIncident",
    "cause",
    "fileAttachment",
    "codesRiskAssociated",
    "numberRiskAssociated",
    "indicatorAtRisk",
    "eventMain",
    "numberRelation",
    "numberActionPlan",
    "codesActionPlan"
  ]
});

var StoreGridDashBoardManagerEvent = Ext.create("Ext.data.Store", {
  model: "ModelGridDashBoardManagerEvent",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowDashBoardManagerEvent",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowDashBoardManagerEvent",
    layout: "fit",
    width: 1100,
    height: 500,
    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            title: "EVENTOS DE PÉRDIDA",
            titleAlign: "center",
            padding: "2 2 2 2",
            store: StoreGridDashBoardManagerEvent,
            loadMask: true,
            columnLines: true,
            columns: [
              { xtype: "rownumberer", width: 25, sortable: false },
              {
                header: "CODIGO",
                align: "left",
                dataIndex: "codeEvent",
                width: 110,
                renderer: function(value, metaData, record) {
                  var state;
                  var type = "";
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("colorEventState") +
                    ' !important; height:40px;"';
                  state =
                    '<br><span style="font-size:9px">' +
                    record.get("nameEventState") +
                    "</span>";
                  if (record.get("eventType") == "2") {
                    type =
                      '<i class="tesla even-stack2" style="margin-left:18px;line-height: 0.2 !important;"></i>';
                  } else if (record.get("eventState") == "X") {
                    metaData.tdAttr =
                      'style="background-color: #cccccc !important; height:40px;"';
                    type =
                      '<i class="tesla even-blocked-24" style="margin-left:18px;line-height: 0.2 !important;"></i>';
                  }

                  if (record.get("fileAttachment") == "S") {
                    return (
                      '<div style="display:inline-block;height:20px;width:20px;padding:2px;"><i class="tesla even-attachment"></i></div><div style="display:inline-block;position:absolute;">' +
                      '<span style="font-size: 14px;">' +
                      value +
                      type +
                      "</span> " +
                      state +
                      "</div>"
                    );
                  } else {
                    return (
                      '<div style="display:inline-block;height:20px;width:20px;padding:2px;"></div><div style="display:inline-block;position:absolute;">' +
                      '<span style="font-size: 14px;">' +
                      value +
                      type +
                      "</span> " +
                      state +
                      "</div>"
                    );
                  }
                }
              },
              {
                header: "DESCRIPCI&Oacute;N CORTA",
                align: "left",
                dataIndex: "descriptionShort",
                width: 270
              },
              {
                header: "SUCURSAL",
                dataIndex: "descriptionRegion",
                align: "center",
                width: 120
              },
              {
                header: "AGENCIA",
                dataIndex: "descriptionAgency",
                align: "center",
                width: 150
              },
              {
                header: "UNIDAD",
                align: "center",
                dataIndex: "descriptionWorkArea",
                width: 180
              },
              {
                header: "MONTO DE PÉRDIDA",
                dataIndex: "totalLoss",
                align: "center",
                tdCls: "custom-column",
                xtype: "numbercolumn",
                format: "0,0.00",
                width: 100
              },
              {
                header: "MONTO DE RECUPERO",
                dataIndex: "totalLossRecovery",
                align: "center",
                xtype: "numbercolumn",
                format: "0,0.00",
                width: 100
              },
              {
                header: "PÉRDIDA NETA",
                dataIndex: "netLoss",
                tdCls: "custom-column",
                align: "center",
                xtype: "numbercolumn",
                format: "0,0.00",
                width: 100
              },
              {
                header: "FECHA DE REPORTE",
                align: "center",
                dataIndex: "dateAcceptLossEvent",
                xtype: "datecolumn",
                format: "d/m/Y H:i",
                width: 90,
                renderer: function(a) {
                  return (
                    '<span style="color:green;">' +
                    Ext.util.Format.date(a, "d/m/Y H:i") +
                    "</span>"
                  );
                }
              },
              {
                header: "FECHA DE OCURRENCIA",
                align: "center",
                dataIndex: "dateOccurrence",
                xtype: "datecolumn",
                format: "d/m/Y H:i",
                width: 90,
                renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridDashBoardManagerEvent,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              items: [
                "-",
                {
                  text: "Exportar",
                  iconCls: "excel",
                  handler: function(b, e) {
                    b.up("grid").downloadExcelXml();
                  }
                }
              ],
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            },
            listeners: {
              itemcontextmenu: function(view, record, item, index, e, eOpts) {
                e.stopEvent();
                var addMenu = Ext.create(
                  "DukeSource.view.risk.DatabaseEventsLost.AddMenuEvent",
                  {}
                );
                addMenu.removeAll();
                addMenu.add({
                  text: "Ficha Evento",
                  iconCls: "pdf",
                  handler: function() {
                    var win = Ext.create("Ext.window.Window", {
                      height: 500,
                      layout: "fit",
                      width: 800,
                      modal: true
                    }).show();
                    var value = record.get("idEvent");
                    win.removeAll();
                    win.add({
                      xtype: "component",
                      autoEl: {
                        tag: "iframe",
                        src:
                          "http://localhost:9000/giro/pdfGeneralReport.htm?values=" +
                          value +
                          "&names=" +
                          "idEvent" +
                          "&types=" +
                          "Integer" +
                          "&nameReport=" +
                          nameReport("EventProfile") +
                          "&nameDownload= Ficha de Evento - " +
                          value
                      }
                    });
                  }
                });
                addMenu.showAt(e.getXY());
              }
            }
          }
        ],
        buttonAlign: "center",
        buttons: [
          {
            text: "SALIR",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
