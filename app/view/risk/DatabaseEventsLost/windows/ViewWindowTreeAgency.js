Ext.define('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeAgency', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowTreeAgency',
    requires: [
        'Ext.tree.Panel',
        'Ext.tree.View',
        'DukeSource.view.risk.DatabaseEventsLost.treepanel.TreePanelAgency'
    ],
    title: 'Ubicaci&oacute;n geogr&aacute;fica',
    border: false,
    titleAlign: 'center',
    layout: 'fit',
    modal: true,
    height: 570,
    width: 900,
    initComponent: function () {
        var me = this;
        var backWindow = this.backWindow;
        var valueNode;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'TreePanelAgency',
                    listeners: {
                        select: function (view, record) {
                            valueNode = record
                        },
                        itemdblclick: function (view, record) {
                            var depth = record.data['depth'];
                            if (depth === 3) {
                                var description = record.getPath('text', ' &#8702; ');
                                backWindow.down('#descriptionAgency').setValue(description.substring(39));
                                backWindow.down('#agency').setValue(record.data['idAgency']);
                                me.close();
                            } else {
                                DukeSource.global.DirtyView.messageWarning('Debe escoger una agencia');
                            }
                        }
                    }

                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    iconCls: 'save',
                    scale: 'medium',
                    handler: function () {
                        if (valueNode !== undefined) {
                            var depth = valueNode.data['depth'];
                            if (depth === 3) {
                                var description = valueNode.getPath('text', ' &#8702; ');
                                backWindow.down('#descriptionAgency').setValue(description.substring(39));
                                backWindow.down('#agency').setValue(valueNode.data['idAgency']);
                                me.close();
                            } else {
                                DukeSource.global.DirtyView.messageWarning('Debe escoger una agencia');
                            }
                        } else {
                            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM_CHECK);
                        }


                    }
                },
                {
                    text: 'Salir',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }


            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }
});