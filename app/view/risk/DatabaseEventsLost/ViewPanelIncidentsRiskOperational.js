Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.ViewPanelIncidentsRiskOperational",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelIncidentsRiskOperational",
    layout: "fit",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "panel",
            titleAlign: "center",
            bodyStyle: {
              background: "#D4E3F4"
            },
            border: false,
            layout: {
              type: "vbox",
              align: "stretch"
            },

            items: [
              {
                xtype: "container",
                height: 5
              },
              {
                xtype: "tabpanel",
                itemId: "panelView",
                flex: 1,
                plain: true,
                border: false,
                items: [],
                listeners: {
                  render: function() {
                    if (
                      category === DukeSource.global.GiroConstants.COLLABORATOR ||
                      category === DukeSource.global.GiroConstants.GESTOR
                    ) {
                      me.down("tabpanel").add(
                        Ext.create(
                          "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridIncidentsPendingRiskOperational",
                          { title: "Pendientes" }
                        )
                      );
                      me.down("tabpanel").add(
                        Ext.create(
                          "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridIncidentsRevisedRiskOperational",
                          { title: "Revisados" }
                        )
                      );
                      me.down("tabpanel").setActiveTab(0);
                    } else {
                      me.down("tabpanel").add(
                        Ext.create(
                          "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridIncidentsPendingRiskOperational",
                          { title: "Pendientes" }
                        )
                      );
                      me.down("tabpanel").add({
                        xtype: "panel",
                        title: "Revisados",
                        flex: 1,
                        layout: {
                          type: "border"
                        },
                        border: false,
                        items: [
                          {
                            xtype: Ext.create(
                              "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridIncidentsRevisedRiskOperational",
                              {
                                loadMask: true,
                                columnLines: true,
                                region: "center"
                              }
                            )
                          },
                          {
                            xtype: Ext.create(
                              "DukeSource.view.risk.util.search.AdvancedSearchIncident",
                              {
                                region: "west",
                                flex: 0.35,
                                collapsed: true,
                                padding: 2,
                                tbar: [
                                  {
                                    xtype: "button",
                                    scale: "medium",
                                    text: "Buscar",
                                    iconCls: "search",
                                    action: "searchAdvancedIncident",
                                    margin: "0 5 0 0"
                                  },
                                  {
                                    xtype: "button",
                                    scale: "medium",
                                    text: "Limpiar",
                                    iconCls: "clear",
                                    margin: "0 5 0 0",
                                    handler: function(btn) {
                                      btn
                                        .up("form")
                                        .getForm()
                                        .reset();
                                    }
                                  }
                                ]
                              }
                            )
                          }
                        ]
                      });

                      me.down("tabpanel").add(
                        Ext.create(
                          "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridIncidentsRefusedRiskOperational",
                          { title: "Invalidos" }
                        )
                      );
                      me.down("tabpanel").setActiveTab(0);
                    }
                  },
                  tabchange: function(tabPanel, newCard, oldCard) {
                    if (
                      category === DukeSource.global.GiroConstants.COLLABORATOR &&
                      newCard.xtype ===
                        "ViewGridIncidentsPendingRiskOperational"
                    ) {
                      me.down("#btnAddIncident").setVisible(true);
                      me.down("#buttonModify").setVisible(false);
                      me.down("#cbxFilterIncident").setVisible(false);
                    } else if (
                      category === DukeSource.global.GiroConstants.GESTOR &&
                      newCard.xtype ===
                        "ViewGridIncidentsPendingRiskOperational"
                    ) {
                      me.down("#btnAddIncident").setVisible(true);
                      me.down("#buttonModify").setVisible(false);
                      me.down("#cbxFilterIncident").setVisible(false);
                    } else if (
                      (category === DukeSource.global.GiroConstants.ANALYST ||
                        category === DukeSource.global.GiroConstants.AUDITOR) &&
                      newCard.xtype ===
                        "ViewGridIncidentsPendingRiskOperational"
                    ) {
                      me.down("#btnAddIncident").setVisible(false);
                      me.down("#buttonModify").setVisible(true);
                      me.down("#cbxFilterIncident").setVisible(false);
                    } else if (
                      (category === DukeSource.global.GiroConstants.ANALYST ||
                        category === DukeSource.global.GiroConstants.AUDITOR) &&
                      newCard.xtype === "panel"
                    ) {
                      me.down("#btnAddIncidentRevised").setVisible(true);
                      me.down("#buttonModify").setVisible(false);
                      me.down("#cbxFilterIncident").setVisible(true);
                    } else {
                      me.down("#btnAddIncident").setVisible(true);
                      me.down("#buttonModify").setVisible(false);
                      me.down("#cbxFilterIncident").setVisible(false);
                    }
                  }
                }
              }
            ]
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
