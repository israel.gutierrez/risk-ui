Ext.define('DukeSource.view.risk.DatabaseEventsLost.combos.ViewTypeLostIncident', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewTypeLostIncident',
    displayField: 'description',
    valueField: 'idTypeLostIncident',

    editable: true,
    forceSelection: true,
    fieldLabel: 'TIPO DE PÉRDIDA',
    msgTarget: 'side',
    store: {
        fields: ["idTypeLostIncident", "description"],
        data: [
            {
                idTypeLostIncident: "0",
                description: "SE DESCONOCE"
            },
            {
                idTypeLostIncident: "1",
                description: "ESTIMADO"
            },
            {
                idTypeLostIncident: "2",
                description: "EXACTO"
            }
        ]
    }
});


