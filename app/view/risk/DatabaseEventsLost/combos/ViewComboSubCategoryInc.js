Ext.define("ModelComboSubCategoryInc", {
  extend: "Ext.data.Model",
  fields: ["value", "description"]
});

var storeComboSubCategoryInc = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboSubCategoryInc",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm", //showListForeignKeysByTableNAme.htm
    //extraParams: {
    //    propertyFind: 'identified',
    //    valueFind:'SUBCATEGORYSI',
    //    propertyOrder: 'description'
    //},
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.combos.ViewComboSubCategoryInc",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboSubCategoryInc",
    queryMode: "local",
    displayField: "description",
    valueField: "value",

    editable: true,
    forceSelection: true,
    fieldLabel: "SUBCATEGORIA",
    msgTarget: "side",
    store: storeComboSubCategoryInc
  }
);
