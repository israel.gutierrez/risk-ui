Ext.define("ModelComboIncEvent", {
  extend: "Ext.data.Model",
  fields: ["value", "description"]
});

var storeComboIncEvent = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboIncEvent",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
    extraParams: {
      propertyFind: "identified",
      valueFind: "INCINDICATOR",
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.DatabaseEventsLost.combos.ViewComboIncEvent", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboIncEvent",
  queryMode: "remote",
  displayField: "description",
  valueField: "value",

  editable: true,
  forceSelection: true,
  fieldLabel: "PROCESAR COMO",
  msgTarget: "side",
  store: storeComboIncEvent
});
