Ext.define("ModelComboIndIncident", {
  extend: "Ext.data.Model",
  fields: ["value", "description"]
});

var storeComboIndIncident = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboIndIncident",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
    extraParams: {
      propertyFind: "identified",
      valueFind: "INDICATORINC",
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.combos.ViewComboIndIncident",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboIndIncident",
    queryMode: "remote",
    displayField: "description",
    valueField: "value",

    editable: true,
    forceSelection: true,
    fieldLabel: "INDICADOR INC",
    msgTarget: "side",
    store: storeComboIndIncident
  }
);
