Ext.define("ModelComboCategoryIncident", {
  extend: "Ext.data.Model",
  fields: ["value", "description"]
});

var storeComboCategoryIncident = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboCategoryIncident",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
    extraParams: {
      propertyFind: "identified",
      valueFind: "CATEGORYINC",
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.combos.ViewComboCategoryIncident",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboCategoryIncident",
    queryMode: "remote",
    displayField: "description",
    valueField: "value",

    editable: true,
    forceSelection: true,
    fieldLabel: "CATEGORIA",
    msgTarget: "side",
    store: storeComboCategoryIncident
  }
);
