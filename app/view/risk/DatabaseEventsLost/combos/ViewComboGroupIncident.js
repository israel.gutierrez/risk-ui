Ext.define("ModelComboGroupIncident", {
  extend: "Ext.data.Model",
  fields: ["idIncidentGroup", "description"]
});

var storeComboGroupIncident = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboGroupIncident",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListIncidentGroupActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.combos.ViewComboGroupIncident",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboGroupIncident",
    queryMode: "remote",
    displayField: "description",
    valueField: "idIncidentGroup",

    editable: true,
    forceSelection: true,
    fieldLabel: "GRUPO INC",
    msgTarget: "side",
    store: storeComboGroupIncident
  }
);
