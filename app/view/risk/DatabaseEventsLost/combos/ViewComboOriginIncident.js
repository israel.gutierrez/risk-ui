Ext.define("ModelComboOriginIncident", {
  extend: "Ext.data.Model",
  fields: ["value", "description"]
});

var storeComboOriginIncident = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboOriginIncident",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
    extraParams: {
      propertyFind: "identified",
      valueFind: "ORIGININC",
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.combos.ViewComboOriginIncident",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboOriginIncident",
    queryMode: "remote",
    displayField: "description",
    valueField: "value",

    editable: true,
    forceSelection: true,
    msgTarget: "side",
    fieldLabel: "PROCEDENCIA",
    store: storeComboOriginIncident
  }
);
