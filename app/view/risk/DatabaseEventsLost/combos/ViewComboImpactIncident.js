Ext.define("ModelComboImpactIncident", {
  extend: "Ext.data.Model",
  fields: ["value", "description"]
});

var storeComboImpactIncident = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboImpactIncident",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
    extraParams: {
      propertyFind: "identified",
      valueFind: "IMPACTINC",
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.combos.ViewComboImpactIncident",
  {
    extend: "Ext.form.ComboBox",
    alias: "widget.ViewComboImpactIncident",
    queryMode: "remote",
    displayField: "description",
    valueField: "value",

    editable: true,
    forceSelection: true,
    fieldLabel: "IMPACTO",
    msgTarget: "side",
    store: storeComboImpactIncident
  }
);
