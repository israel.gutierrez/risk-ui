Ext.define('ModelPanelEventsManager', {
    extend: 'Ext.data.Model',
    fields: [
        'idEvent',
        'codeEvent',
        'workArea',
        'isRiskEventIncidents',
        'businessLineOne',
        'businessLineTwo',
        'businessLineThree',
        'descriptionBusinessLineOne',
        'factorRisk',
        'idEventThree',
        'eventTwo',
        'eventOne',
        'managerRisk',
        'agency',
        'currency',
        'riskType',
        'actionPlan',
        'idProcessType',
        'idProcess',
        'idSubProcess',
        'idActivity',
        'insuranceCompany',
        'descriptionProcess',
        'descriptionSubProcess',
        'eventState',
        'nameEventState',
        'colorEventState',
        'sequenceEventState',
        'eventType',
        'lossType',
        'totalLossRecovery',
        'totalLoss',
        'accountingEntryLoss',
        'accountingEntryRecoveryLoss',
        'descriptionLarge',
        'descriptionAgency',
        'descriptionRegion',
        'descriptionWorkArea',
        'descriptionUnity',
        {
            name: 'dateOccurrence', type: 'date', format: 'd/m/Y H:i:s', convert: function (value) {
                return Ext.Date.parse(value, "d/m/Y H:i:s");
            }
        },
        {
            name: 'dateRegister', type: 'date', format: 'd/m/Y H:i:s', convert: function (value) {
                return Ext.Date.parse(value, "d/m/Y H:i:s");
            }
        },
        {
            name: 'dateCloseEvent', type: 'date', format: 'd/m/Y H:i:s', convert: function (value) {
                return Ext.Date.parse(value, "d/m/Y H:i:s");
            }
        },
        {
            name: 'dateDiscovery', type: 'date', format: 'd/m/Y H:i:s', convert: function (value) {
                return Ext.Date.parse(value, "d/m/Y H:i:s");
            }
        },
        {
            name: 'dateAccountingEvent', type: 'date', format: 'd/m/Y H:i:s', convert: function (value) {
                return Ext.Date.parse(value, "d/m/Y H:i:s");
            }
        },
        'entityPenalizesLoss',
        'grossLoss',
        'lossRecovery',
        'sureAmountRecovery',
        'totalAmountSpend',
        'amountSubEvent',
        'amountProvision',
        'sureAmount',
        'netLoss',
        'typeChange',
        'typeEvent',
        'descriptionEventThree',
        'descriptionEventTwo',
        'descriptionEventOne',
        'nameFactorRisk',
        'descriptionFactorRisk',
        'nameAction',
        'nameCurrency',
        'descriptionShort',
        'idPlanAccountSureAmountRecovery',
        'planAccountSureAmountRecovery',
        'planAccountLossRecovery',
        'idPlanAccountLossRecovery',
        'bookkeepingEntryLossRecovery',
        'bookkeepingEntrySureAmountRecovery',
        'amountOrigin',
        'codeGeneralIncidents',
        'idIncident',
        'cause',
        'fileAttachment',
        'codesRiskAssociated',
        'numberRiskAssociated',
        'indicatorAtRisk',
        'eventMain',
        'numberRelation',
        'numberActionPlan',
        'codesActionPlan',
        'descriptionTittleEvent',
        'costCenter',
        'codeMigration',
        'commentsEvent',
        'order',
        'stateTemp',
        'descriptionPlanAccountLoss',
        'codeAccountLoss',
        'codeCorrelative',
        'checkApprove',
        'userApprove',
        'commentApprove',
        'dateApprove',
        'processRecursive',
        'descriptionProcessRecursive',
        'operation',
        'descriptionOperation',
        'recoveryFixedAsset',
        'isCritic',
        'descriptionStateEvent',
        'descriptionCriticEvent',
        'descriptionTypeEvent',
        'product',
        'descriptionProduct',
        'processImpact',
        'subProcessImpact',
        'idEventApproved',
        'motiveReject',
        'approve',
        'order',
        'orderApproved',
        'stateApprove',
        'sequenceApprove',
        'comments',
        'userRegister',
        'fullNameUserRegister',
        'workAreaUserRegister'
    ]
});

var StoreEventsManager = Ext.create('Ext.data.Store', {
    model: 'ModelPanelEventsManager',
    autoLoad: true,
    proxy: {
        actionMethods: {
            create: 'POST',
            read: 'POST',
            update: 'POST'
        },
        type: 'ajax',
        url: codexCompany === 'es_cl_0001' && userRole === DukeSource.global.GiroConstants.GESTOR ?
            'http://localhost:9000/giro/getAllEventToApprove.htm' : codexCompany === 'es_cl_0001' ? 'http://localhost:9000/giro/getAllEventMaster.htm' : 'http://localhost:9000/giro/getAllEventForManager.htm',
        extraParams: {
            eventState: category === DukeSource.global.GiroConstants.ANALYST ? 1 : userRole === DukeSource.global.GiroConstants.ACCOUNTANT ? 3 : 1
        },
        reader: {
            totalProperty: 'totalCount',
            rootProperty: 'data',
            successProperty: 'success'
        }
    }
});

Ext.define('DukeSource.view.risk.DatabaseEventsLost.ViewPanelEventsManager', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelEventsManager',
    layout: 'fit',
    requires: [
        'DukeSource.view.risk.util.search.BasicSearchEvent'
    ],
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'panel',
                    titleAlign: 'center',
                    border: false,
                    layout: {
                        type: 'fit'
                    },
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'top',
                            items: [
                                {
                                    xtype: 'button',
                                    iconCls: 'add',
                                    text: 'Nuevo',
                                    hidden: codexCompany === 'es_cl_0001' && category !== DukeSource.global.GiroConstants.COLLABORATOR,
                                    scale: 'medium',
                                    cls: 'my-btn',
                                    overCls: 'my-over',
                                    handler: function () {
                                        var win;
                                        if (codexCompany === 'es_cl_0001') {
                                            win = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.WindowRegisterEventWizard', {
                                                new: true
                                            });
                                            win.show();
                                            win.down('#currency').getStore().load({
                                                callback: function () {
                                                    var typeChange = win.down('#typeChange');
                                                    win.down('#currency').setValue('1');
                                                    typeChange.setValue('1.00');
                                                    typeChange.setFieldStyle('background: #D8D8D8');
                                                    typeChange.setReadOnly(true);
                                                }
                                            })
                                        } else {
                                            win = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.WindowRegisterEventsManager', {
                                                actionType: 'new',
                                                modal: true
                                            });

                                            win.timer = setInterval(function () {
                                                win.down('#dateRegisterTemp').setValue(new Date());
                                            }, 1000);
                                            win.show();
                                            win.down('#dateOccurrence').focus(false, 200);
                                        }
                                    }
                                }, '-',
                                {
                                    xtype: 'button',
                                    scale: 'medium',
                                    hidden: codexCompany === 'es_cl_0001' && category !== DukeSource.global.GiroConstants.COLLABORATOR,
                                    cls: 'my-btn',
                                    overCls: 'my-over',
                                    iconCls: 'modify',
                                    text: 'Editar',
                                    handler: function () {
                                        var app = DukeSource.application;
                                        var grid = me.down('grid');
                                        var row = grid.getSelectionModel().getSelection()[0];
                                        if (row === undefined) {
                                            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
                                        } else {
                                            codexCompany === 'es_cl_0001' ?
                                                app.getController('DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelEventsRiskOperational')._onLoadEventsRisk(grid, row, grid.store.indexOf(row)) :
                                                app.getController('DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelEventManager')._onLoadEventsRiskManager(row);
                                        }
                                    }

                                }, '-',
                                {
                                    xtype: 'button',
                                    scale: 'medium',
                                    cls: 'my-btn',
                                    overCls: 'my-over',
                                    iconCls: 'evaluate',
                                    hidden: !(codexCompany === 'es_cl_0001' && userRole === DukeSource.global.GiroConstants.GESTOR),
                                    text: 'Aprobación',
                                    itemId: 'btnApprove',
                                    handler: function () {
                                        var record = me.down('grid').getSelectionModel().getSelection()[0];
                                        if (record === undefined) {
                                            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
                                        } else if (record.get('sequenceEventState') !== 2) {
                                            DukeSource.global.DirtyView.messageWarning('Estado no permitido para la aprobación');
                                        } else {
                                            var win = Ext.create('DukeSource.view.risk.parameter.windows.WindowApproveEvent', {
                                                modal: true
                                            });
                                            win.setTitle(win.title + ' - ' + record.get('descriptionShort'));
                                            win.down('form').getForm().loadRecord(record);
                                            win.show();
                                        }
                                    }
                                }, '-',
                                {
                                    xtype: 'combobox',
                                    width: 300,
                                    name: 'eventState',
                                    itemId: 'eventState',
                                    disabled: codexCompany === 'es_cl_0001' && userRole === DukeSource.global.GiroConstants.GESTOR,
                                    fieldLabel: 'Estado',
                                    labelWidth: 80,
                                    displayField: 'description',
                                    valueField: 'id',
                                    store: {
                                        autoLoad: true,
                                        fields: ['id', 'description', 'sequence'],
                                        proxy: {
                                            actionMethods: {
                                                create: 'POST',
                                                read: 'POST',
                                                update: 'POST'
                                            },
                                            type: 'ajax',
                                            url: 'http://localhost:9000/giro/findStateIncident.htm',
                                            extraParams: {
                                                propertyFind: 'si.typeIncident',
                                                valueFind: DukeSource.global.GiroConstants.EVENT,
                                                propertyOrder: 'si.sequence'
                                            },
                                            reader: {
                                                type: 'json',
                                                rootProperty: 'data',
                                                successProperty: 'success'
                                            }
                                        }
                                    },
                                    listeners: {
                                        select: function (cbo) {
                                            var grid = me.down('grid');
                                            grid.store.getProxy().extraParams = {
                                                eventState: cbo.findRecordByValue(cbo.getValue()).get('sequence')
                                            };
                                            grid.store.getProxy().url = 'http://localhost:9000/giro/getAllEventMaster.htm';
                                            grid.down('pagingtoolbar').moveFirst();
                                        }
                                    }
                                }, '-',
                                {
                                    xtype: 'checkboxfield',
                                    width: 200,
                                    hidden: !(codexCompany === 'es_cl_0001' && userRole === DukeSource.global.GiroConstants.GESTOR),
                                    fieldLabel: 'Ver pendientes de aprobación',
                                    checked: true,
                                    name: 'isSteal',
                                    itemId: 'isSteal',
                                    labelWidth: 200,
                                    inputValue: "S",
                                    uncheckedValue: "N",
                                    value: 'N',
                                    listeners: {
                                        change: function (chk, oldValue, newValue) {
                                            if (newValue) {
                                                chk.setFieldLabel('Ver todos');
                                                chk.setWidth(150);
                                                me.down('#eventState').setDisabled(false);
                                                me.down('#btnApprove').setDisabled(true);
                                                me.down('#eventState').reset()
                                            } else {
                                                chk.setFieldLabel('Ver pendientes de aprobación');
                                                chk.setWidth(250);
                                                me.down('grid').store.getProxy().url = 'http://localhost:9000/giro/getAllEventToApprove.htm';
                                                me.down('grid').down('pagingtoolbar').moveFirst();
                                                me.down('#eventState').setDisabled(true);
                                                me.down('#btnApprove').setDisabled(false);
                                                me.down('#eventState').reset()
                                            }

                                        }
                                    }
                                }
                            ]
                        }
                    ],
                    items: [
                        {
                            xtype: 'container',
                            border: false,
                            layout: {
                                type: 'border'
                            },
                            items: [
                                {
                                    xtype: 'gridpanel',
                                    name: 'gridEventsManager',
                                    itemId: 'gridEventsManager',
                                    store: StoreEventsManager,
                                    loadMask: true,
                                    columnLines: true,
                                    border: false,
                                    style: {
                                        borderLeft: '1px solid #99bce8'
                                    },
                                    region: 'center',
                                    columns: [
                                        {
                                            xtype: 'rownumberer',
                                            width: 45,
                                            resizable: true
                                        },
                                        {
                                            header: '',
                                            align: 'left',
                                            dataIndex: 'fileAttachment',
                                            hidden: codexCompany !== 'es_cl_0001',
                                            width: 35,
                                            renderer: function (value, metaData, record) {
                                                var reject = '';
                                                if (record.get('stateTemp') === 'R') {
                                                    reject = '<i class="tesla even-times-circle-o"></i>';
                                                }
                                                return reject;
                                            }
                                        },
                                        {
                                            header: 'Código',
                                            align: 'left',
                                            dataIndex: 'codeEvent',
                                            width: 160,
                                            renderer: function (value, metaData, record) {
                                                var state;
                                                var type = '';
                                                metaData.tdAttr = 'style="background-color: #' + record.get('colorEventState') + ' !important; height:40px;"';
                                                state = '<br><span style="font-size:7pt">' + record.get('nameEventState') + '</span>';
                                                if ((record.get('eventType') === '2')) {
                                                    type = '<i class="tesla even-stack2" style="margin-left:18px;line-height: 0.2 !important;"></i>';
                                                } else if ((record.get('eventState') === 'X')) {
                                                    metaData.tdAttr = 'style="background-color: #cccccc !important; height:40px;"';
                                                    type = '<i class="tesla even-blocked-24" style="margin-left:18px;line-height: 0.2 !important;"></i>';
                                                }

                                                if (record.get('fileAttachment') === 'S') {
                                                    return '<div style="display:inline-block;height:20px;width:20px;padding:2px;"><i class="tesla even-attachment"></i></div><div style="display:inline-block;position:absolute;">' +
                                                        '<span style="font-size: 12px;font-weight: bold">' + value + type + '</span> ' + state + '</div>';
                                                } else {
                                                    return '<div style="display:inline-block;height:20px;width:20px;padding:2px;"></div><div style="display:inline-block;position:absolute;">' +
                                                        '<span style="font-size: 12px;font-weight: bold">' + value + type + '</span> ' + state + '</div>';
                                                }
                                            }
                                        },
                                        {
                                            header: 'Descripción',
                                            align: 'left',
                                            dataIndex: 'descriptionLarge',
                                            width: 300
                                        },
                                        {
                                            header: 'Fecha de registro',
                                            align: 'center',
                                            dataIndex: 'dateRegister',
                                            xtype: 'datecolumn',
                                            format: 'd/m/Y H:i:s',
                                            width: 90,
                                            renderer: function (a) {
                                                return '<span style="color:green;">' + Ext.util.Format.date(a, 'd/m/Y H:i:s') + "</span>"
                                            }
                                        },
                                        {
                                            header: 'Fecha de ocurrencia',
                                            align: 'center',
                                            dataIndex: 'dateOccurrence',
                                            xtype: 'datecolumn',
                                            format: 'd/m/Y H:i',
                                            width: 90,
                                            renderer: Ext.util.Format.dateRenderer('d/m/Y H:i')
                                        }
                                    ],
                                    viewConfig: {
                                        listeners: {
                                            itemdblclick: function (grid, record, item, index) {
                                                if (record === undefined) {
                                                    DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
                                                } else {
                                                    var app = DukeSource.application;
                                                    codexCompany === 'es_cl_0001' ?
                                                        app.getController('DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelEventsRiskOperational')._onLoadEventsRisk(grid, record, index) :
                                                        app.getController('DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelEventManager')._onLoadEventsRiskManager(record);
                                                }
                                            }
                                        }
                                    },
                                    bbar: {
                                        xtype: 'pagingtoolbar',
                                        pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                                        store: StoreEventsManager,
                                        displayInfo: true,
                                        displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                                        emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                                    },
                                    listeners: {
                                        render: function () {
                                            var me = this;
                                            var columnAmount = Ext.create('Ext.grid.column.Column', {
                                                header: 'Montos',
                                                align: 'center',
                                                columns: [
                                                    {
                                                        header: 'Moneda',
                                                        align: 'center',
                                                        dataIndex: 'nameCurrency',
                                                        width: 90
                                                    },
                                                    {
                                                        header: 'Monto inicial',
                                                        dataIndex: 'amountOrigin',
                                                        align: 'center',
                                                        xtype: 'numbercolumn',
                                                        format: '0,0.00',
                                                        width: 100
                                                    },
                                                    {
                                                        header: 'Monto recupero',
                                                        dataIndex: 'totalLossRecovery',
                                                        align: 'center',
                                                        xtype: 'numbercolumn',
                                                        format: '0,0.00',
                                                        width: 100
                                                    },
                                                    {
                                                        header: 'Gastos',
                                                        dataIndex: 'totalAmountSpend',
                                                        align: 'center',
                                                        xtype: 'numbercolumn',
                                                        format: '0,0.00',
                                                        width: 60
                                                    },
                                                    {
                                                        header: 'Provisión',
                                                        dataIndex: 'amountProvision',
                                                        align: 'center',
                                                        hidden: hidden('WinGridProvisionHead'),
                                                        xtype: 'numbercolumn',
                                                        format: '0,0.00',
                                                        width: 80
                                                    },
                                                    {
                                                        header: 'Pérdida neta',
                                                        dataIndex: 'netLoss',
                                                        tdCls: 'custom-column',
                                                        align: 'center',
                                                        xtype: 'numbercolumn',
                                                        format: '0,0.00',
                                                        width: 100
                                                    }
                                                ]
                                            });
                                            var columnDateDiscovery = Ext.create('Ext.grid.column.Column', {
                                                text: 'Fecha de descubrimiento',
                                                align: 'center',
                                                hidden: hidden('EWMDateDiscovery'),
                                                dataIndex: 'dateDiscovery',
                                                xtype: 'datecolumn',
                                                format: 'd/m/Y H:i',
                                                width: 97,
                                                renderer: Ext.util.Format.dateRenderer('d/m/Y H:i')
                                            });

                                            me.headerCt.insert(6, columnAmount);
                                            me.headerCt.insert(7, columnDateDiscovery);

                                            me.getView().refresh();
                                        }
                                    }
                                },
                                {
                                    xtype: 'BasicSearchEvent',
                                    padding: 2,
                                    region: 'west',
                                    collapseDirection: 'left',
                                    collapsed: true,
                                    collapsible: true,
                                    split: true,
                                    flex: 0.4,
                                    title: 'Búsqueda',
                                    tbar: [
                                        {
                                            xtype: 'button',
                                            scale: 'medium',
                                            text: 'Buscar',
                                            iconCls: 'search',
                                            action: 'searchBasicEvent',
                                            margin: '0 5 0 0'
                                        },
                                        {
                                            xtype: 'button',
                                            scale: 'medium',
                                            text: 'Limpiar',
                                            iconCls: 'clear',
                                            margin: '0 5 0 0',
                                            handler: function () {
                                                me.down('BasicSearchEvent').down('form').getForm().reset();
                                            }
                                        },
                                        {
                                            xtype: 'button',
                                            scale: 'medium',
                                            text: 'Exportar',
                                            iconCls: 'excel',
                                            margin: '0 5 0 0',
                                            handler: function (btn) {
                                                var form = btn.up('form');
                                                if (form.getForm().isValid()) {

                                                    var codeEvent = form.down('#codeEvent').getValue() === '' ? 'T' : form.down('#codeEvent').getValue();
                                                    var dateInit = form.down('#dateRegisterInit').getRawValue() === '' ? '01/01/1900 00:00' : form.down('#dateRegisterInit').getRawValue();
                                                    var dateEnd = form.down('#dateRegisterEnd').getRawValue() === '' ? '31/12/2900 23:59' : form.down('#dateRegisterEnd').getRawValue();
                                                    var amount = form.down('#amount').getValue() === null ? 0 : form.down('#amount').getValue();
                                                    var idUser = userName;
                                                    var indicatorAmount = amount === 0 ? 'S' : 'N';

                                                    var value = idUser + ',' + codeEvent + ',' + dateInit + ',' + dateEnd + ',' + amount + ',' + indicatorAmount;
                                                    var panelReport = me.down('panel[name=panelReport]');
                                                    panelReport.removeAll();
                                                    panelReport.add(
                                                        {
                                                            xtype: 'component',
                                                            autoEl: {
                                                                tag: 'iframe',
                                                                hidden: true,
                                                                src: 'http://localhost:9000/giro/xlsGeneralReport.htm?values=' + value +
                                                                    '&names=' + 'userName,codeEvent,dateInit,dateEnd,amount,indicatorAmount' +
                                                                    '&types=' + 'String,String,Timestamp,Timestamp,BigDecimal,String' +
                                                                    '&nameReport=' + nameReport('ReportEvent_option_07') +
                                                                    '&nameDownload=report_event_user'
                                                            }
                                                        }
                                                    );
                                                } else {
                                                    DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    listeners: {
                        render: function () {
                            DukeSource.lib.Ajax.request({
                                method: 'POST',
                                url: 'http://localhost:9000/giro/findLevelAuthorizedEvent.htm',
                                params: {
                                    propertyFind: 'u.username',
                                    valueFind: userName,
                                    propertyFindTwo: 'ua.type',
                                    valueFindTwo: 'U'
                                },
                                success: function (response) {
                                    response = Ext.decode(response.responseText);
                                    if (response.success) {
                                        LEVEL_EVENT = response.data === undefined ? 1 : response.data.sequence;
                                    } else {
                                        DukeSource.global.DirtyView.messageWarning(response.message);
                                    }
                                },
                                failure: function () {
                                    DukeSource.global.DirtyView.messageWarning(response.message);
                                }
                            });
                        }
                    }
                }
            ]
        });
        me.callParent(arguments);
    }

});