Ext.define("ModelGridIncidentsHistoricalIncident", {
  extend: "Ext.data.Model",
  fields: [
    "idDetailIncidents",
    "idIncidents",
    "actionPlan",
    "nameActionPlan",
    "factorRisk",
    "nameFactorRisk",
    "lossType",
    "nameIdLostType",
    "eventOne",
    "nameEventOne",
    "eventTwo",
    "nameEventTwo",
    "eventThree",
    "nameEventThree",
    "currency",
    "nameCurrency",
    "descriptionLarge",
    "assignReport",
    {
      name: "dateOccurrence",
      type: "date",
      format: "d/m/Y",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y");
      }
    },
    {
      name: "dateDiscovery",
      type: "date",
      format: "d/m/Y",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y");
      }
    },
    {
      name: "dateIncidents",
      type: "date",
      format: "d/m/Y",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y");
      }
    },
    "netLoss",
    "indicatorIncidents",
    "nameUserReport",
    "userRegister",
    "fullNameUserRegister",
    "agency",
    "descriptionAgency",
    "codeGeneralIncidents",
    "causeIncidents",
    "originIncident",
    "comments",
    "idBusinessLineTwo",
    "idBusinessLineOne",
    "impactReport",
    "businessLineThree",
    "descriptionShort",
    "workArea",
    "descriptionWorkArea",
    "categoryIncidents",
    "subCategoryIncident",
    "nameIncidentsIndicator",
    "qualifyIncentive",
    "confidential"
  ]
});
var StoreGridIncidentsHistoricalIncident = Ext.create("Ext.data.Store", {
  model: "ModelGridIncidentsHistoricalIncident",
  autoLoad: false,
  groupField: "descriptionAgency",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridHistoricalIncident",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridHistoricalIncident",
    loadMask: true,
    columnLines: true,
    viewConfig: {
      stripeRows: true
    },
    store: StoreGridIncidentsHistoricalIncident,
    features: [
      {
        ftype: "grouping",
        id: "idGroupPendindInc",
        groupHeaderTpl:
          '{name} ({rows.length} total{[values.rows.length > 1 ? "s" : ""]})',
        hideGroupedHeader: true,
        startCollapsed: true
      }
    ],
    tbar: [],
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridIncidentsHistoricalIncident,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          iconCls: "print",
          handler: function() {
            DukeSource.global.DirtyView.printElementTogrid(
              Ext.ComponentQuery.query("ViewGridHistoricalIncident")[0]
            );
          }
        },
        "-",
        {
          xtype:"UpperCaseTrigger",
          listeners: {
            keyup: function(a) {
              DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(
                a,
                a.up("grid")
              );
            }
          },
          fieldLabel: "FILTRAR",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {},
    initComponent: function() {
      this.columns = [
        {
          xtype: "rownumberer",
          width: 25,
          sortable: false
        },
        {
          header: "ESTADO",
          align: "center",
          dataIndex: "indicatorIncidents",
          width: 50,
          renderer: function(c, b, a) {
            if (a.get("indicatorIncidents") == "X") {
              b.tdAttr = 'style="background-color: #A8A8A8 !important;"';
              return "<span>" + c + "</span>";
            }
            if (a.get("indicatorIncidents") == "I") {
              b.tdAttr = 'style="background-color: #0B9119 !important;"';
              return "<span>" + c + "</span>";
            }
            if (a.get("indicatorIncidents") == "A") {
              b.tdAttr = 'style="background-color: #4491B5 !important;"';
              return "<span>" + c + "</span>";
            } else {
              b.tdAttr = 'style="background-color: #DF0101 !important;"';
              return "<span>" + c + "</span>";
            }
          }
        },
        {
          header: "FECHA DE <br>REPORTE<br>",
          align: "center",
          dataIndex: "dateIncidents",
          format: "d/m/Y",
          xtype: "datecolumn",
          width: 80,
          renderer: function(a) {
            return (
              '<span style="color:green;">' +
              Ext.util.Format.date(a, "d/m/Y") +
              "</span>"
            );
          }
        },
        {
          header: "USUARIO QUE REGISTRO",
          dataIndex: "fullNameUserRegister",
          width: 150
        },
        {
          header: "CONFIDENCIALIDAD",
          dataIndex: "confidential",
          width: 150,
          renderer: function(c, b, a) {
            if (a.get("confidential") == "CONFIDENCIAL") {
              b.tdAttr = 'style="background-color: #FFDC9E !important;"';
              return "<span>" + c + "</span>";
            }
          }
        },
        {
          header: "DESCRIPCI&Oacute;N CORTA",
          dataIndex: "descriptionShort",
          width: 150
        },
        {
          header: "DESCRIPCI&Oacute;N LARGA",
          dataIndex: "descriptionLarge",
          width: 480
        },
        {
          header: "FECHA DE <br>OCURRENCIA<br>",
          align: "center",
          dataIndex: "dateOccurrence",
          xtype: "datecolumn",
          format: "d/m/Y",
          width: 90,
          renderer: Ext.util.Format.dateRenderer("d/m/Y")
        },
        {
          header: "FECHA DE <br>DESCUBRIMIENTO<br>",
          align: "center",
          dataIndex: "dateDiscovery",
          xtype: "datecolumn",
          format: "d/m/Y",
          width: 100,
          renderer: Ext.util.Format.dateRenderer("d/m/Y")
        },
        {
          header: "AREA",
          dataIndex: "descriptionWorkArea",
          width: 150
        },
        {
          header: "SUCURSAL",
          dataIndex: "descriptionAgency",
          width: 150
        },
        {
          header: "TIPO DE <br>P&Eacute;RDIDA<br>",
          dataIndex: "nameIdLostType",
          width: 100
        },
        {
          header: "MONEDA",
          dataIndex: "nameCurrency",
          width: 60
        },
        {
          header: "MONTO",
          dataIndex: "netLoss",
          align: "right",
          xtype: "numbercolumn",
          format: "0,0.00",
          width: 100
        },
        {
          header: "CAUSAS",
          dataIndex: "causeIncidents",
          width: 150
        },
        {
          header: "IMPACTO",
          dataIndex: "impactReport",
          width: 150
        },
        {
          header: "COMENTARIO",
          dataIndex: "comments",
          width: 150
        }
      ];
      this.callParent(arguments);
    }
  }
);
