Ext.define('DukeSource.view.risk.DatabaseEventsLost.grids.ViewTreeGridPanelIncidentsRiskOperational', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.ViewTreeGridPanelIncidentsRiskOperational',
    store: 'risk.DatabaseEventsLost.grids.StoreTreeGridPanelIncidentsRiskOperational',
//    title: 'The whole world in a Tree Panel with Column Headers',
//    height: 400,
//    width: 800,
//    padding: '5 5 5 5',
    useArrows: true,
    multiSelect: true,
    singleExpand: true,
    rootVisible: false,
    columns: [
        {
            //treecolumn xtype tells the Grid which column will show the tree
            xtype: 'treecolumn',
            text: 'HISTORICO',
            flex: 2,
            sortable: true,
            dataIndex: 'text'
        }

    ]
});
