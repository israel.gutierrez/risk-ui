Ext.define("ModelGridIncidentsPendingRiskOperational", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "idDetailIncidents",
    "descriptionLarge",
    "descriptionShort",
    {
      name: "dateOccurrence",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateFinal",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateDiscovery",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateRegister",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    "indicatorIncidents",
    "collaborator",
    "nameCollaborator",
    "agency",
    "descriptionAgency",
    "causeCollaborator",
    "workArea",
    "descriptionWorkArea",
    "nameIncidentsIndicator",
    "confidential",
    "typeIncident",
    "impacts",
    "hasTicket",
    "numberTicket",
    "fileAttachment",
    "lostType",
    "descriptionLostType",
    "factorRisk",
    "currency",
    "descriptionCurrency",
    {
      name: "amountLoss",
      type: "float",
      convert: function(value) {
        return parseFloat(value);
      }
    },
    "amountLoss",
    "userRegister",
    "nameUserRegister",
    "workAreaRegister",
    "descriptionFactorRisk",
    "commentManager"
  ]
});
var StoreGridIncidentsPendingRiskOperational = Ext.create("Ext.data.Store", {
  model: "ModelGridIncidentsPendingRiskOperational",
  autoLoad: false,
  groupField: "descriptionAgency",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    extraParams: {
      stateIncident: "P"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/getDetailIncidentsByState.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      rootProperty: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridIncidentsPendingRiskOperational",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridIncidentsPendingRiskOperational",
    loadMask: true,
    columnLines: true,
    border: true,
    padding: 1,
    viewConfig: {
      stripeRows: true
    },
    features: [
      {
        ftype: "grouping",
        id: "groupPending",
        groupHeaderTpl:
          '{name} ({rows.length} Pendiente{[values.rows.length > 1 ? "s" : ""]})',
        startCollapsed: true
      }
    ],

    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridIncidentsPendingRiskOperational,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          iconCls: "print",
          handler: function() {
            DukeSource.global.DirtyView.printElementTogrid(
              Ext.ComponentQuery.query(
                "ViewGridIncidentsPendingRiskOperational"
              )[0]
            );
          }
        },
        "-",
        {
          xtype:"UpperCaseTrigger",
          listeners: {
            keyup: function(a) {
              DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(
                a,
                a.up("grid")
              );
            }
          },
          fieldLabel: "Filtrar",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },

    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        columns: [
          {
            xtype: "rownumberer",
            width: 40,
            sortable: false
          },
          {
            header: "Estado",
            align: "center",
            dataIndex: "idDetailIncidents",
            width: 90,
            renderer: function(value, metaData, record) {
              var attachment = "";
              var secret = "";
              var state = '<div style="font-size: 9px">PENDIENTE</div>';
              if (record.get("fileAttachment") === "S") {
                attachment =
                  '<div style="display:inline-block;"><i class="tesla even-attachment"></i></div>';
              }
              if (record.get("confidential") === "S") {
                secret =
                  '<div style="display:inline-block;padding-left: 2px;"><i class="tesla even-user-secret"></i></div>';
              }
              if (record.get("indicatorIncidents") === "R") {
                metaData.tdAttr =
                  'style="background-color: #3ede52 !important;"';
              } else {
                metaData.tdAttr =
                  'style="background-color: #ff7676 !important;"';
              }
              return (
                '<div style="display:inline-block;font-size:12px;padding:5px;">' +
                attachment +
                secret +
                "" +
                '<span style="padding-left: 5px;font-weight: bold"></span></div>' +
                state
              );
            }
          },
          {
            header: "Fecha de reporte",
            align: "center",
            dataIndex: "dateRegister",
            format: "d/m/Y",
            xtype: "datecolumn",
            width: 80,
            renderer: function(a) {
              return (
                '<span style="color:red;">' +
                Ext.util.Format.date(a, "d/m/Y") +
                "</span>"
              );
            }
          },
          {
            header: "Descripci&oacute;n corta",
            dataIndex: "descriptionShort",
            width: 150
          },
          {
            header: "Descripci&oacute;n larga",
            dataIndex: "descriptionLarge",
            width: 480
          },
          {
            header: "Fecha de ocurrencia",
            align: "center",
            dataIndex: "dateOccurrence",
            xtype: "datecolumn",
            format: "d/m/Y H:i",
            width: 90,
            renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
          },
          {
            header: "Fecha fin",
            align: "center",
            dataIndex: "dateFinal",
            xtype: "datecolumn",
            format: "d/m/Y H:i",
            width: 90,
            renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
          },
          {
            header: "Fecha de descubrimiento",
            align: "center",
            dataIndex: "dateDiscovery",
            xtype: "datecolumn",
            format: "d/m/Y H:i",
            width: 97,
            renderer: function(a) {
              return (
                "<span>" + Ext.util.Format.date(a, "d/m/Y H:i") + "</span>"
              );
            }
          },
          {
            header: "Area",
            dataIndex: "descriptionWorkArea",
            width: 150
          }
        ],

        store: StoreGridIncidentsPendingRiskOperational,

        tbar: [
          {
            xtype: "button",
            cls: "my-btn",
            overCls: "my-over",
            itemId: "btnAddIncident",
            iconCls: "add",
            scale: "medium",
            hidden: true,
            text: "Reportar incidente",
            handler: function() {
              var c = Ext.create(
                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowIncidentManager",
                {
                  modal: true,
                  typeIncident: "RO"
                }
              );

              var dateReport = c.down("#dateRegister");
              setInterval(function() {
                dateReport.setValue(new Date());
              }, 1000);

              c.show();
              c.down("#descriptionShort").focus(false, 100);
            }
          },
          {
            xtype: "button",
            itemId: "buttonModify",
            iconCls: "modify",
            scale: "medium",
            cls: "my-btn",
            overCls: "my-over",
            hidden: true,
            text: "Evaluar incidente",
            handler: function() {
              var row = me.getSelectionModel().getSelection()[0];
              if (row === undefined) {
                DukeSource.global.DirtyView.messageWarning(
                  DukeSource.global.GiroMessages.MESSAGE_ITEM
                );
              } else {
                DukeSource.getApplication()
                  .getController(
                    "DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelIncidentsRiskOperational"
                  )
                  ._showModifyIncidentRiskOperational(me, row);
              }
            }
          }
        ],

        listeners: {
          render: function() {
            var a = this;

            if (category === DukeSource.global.GiroConstants.COLLABORATOR) {
              a.store.getProxy().extraParams = {
                stateIncident: "P"
              };
            } else {
              var userRegister = Ext.create("Ext.grid.column.Column", {
                text: "Usuario que reporta",
                dataIndex: "nameUserRegister",
                width: 150,
                align: "center"
              });
              a.headerCt.insert(4, userRegister);
              a.getView().refresh();

              if (category === DukeSource.global.GiroConstants.ANALYST) {
                a.store.getProxy().extraParams = {
                  stateIncident: "R",
                  typeIncident: DukeSource.global.GiroConstants.OPERATIONAL
                };
              }
            }
            a.store.getProxy().url =
              "http://localhost:9000/giro/getDetailIncidentsByState.htm";
            a.store.load();
          },
          afterrender: function(me) {
            me.getView()
              .getFeature("groupPending")
              .disable();
          }
        }
      });
      me.callParent(arguments);
    }
  }
);
