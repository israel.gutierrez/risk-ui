Ext.define("ModelGridIncidentsRevisedBusinessContinuity", {
  extend: "Ext.data.Model",
  fields: [
    "idIncident",
    "idDetailIncidents",
    "descriptionLarge",
    "descriptionShort",
    {
      name: "dateOccurrence",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateFinal",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateDiscovery",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateProcess",
      type: "date",
      format: "d/m/Y",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y");
      }
    },
    "indicatorIncidents",
    "stateIncident",
    "colorStateIncident",
    "descriptionStateIncident",
    "collaborator",
    "nameCollaborator",
    "agency",
    "descriptionAgency",
    "causeCollaborator",
    "workArea",
    "descriptionWorkArea",
    "nameIncidentsIndicator",
    "confidential",
    "criterionSafety",
    "subClassification",
    "typeIncident",
    "idFixedAssets",
    "descriptionFixedAssets",
    "reasonSafety",
    "descriptionReasonSafety",
    "subCategory",
    "hourEnd",
    "hourInit",
    "fileAttachment",
    "lostType",
    "descriptionLostType",
    "factorRisk",
    "descriptionFactorRisk",
    "currency",
    "descriptionCurrency",
    "amountLoss",
    "userRegister",
    "nameUserRegister",
    "commentManager",
    "descriptionIndicatorIncidents",
    "impactReport",
    "detailFactorRisk",
    "processOrigin",
    "subProcessOrigin",
    "processImpact",
    "subProcessImpact",
    "eventOne",
    "eventTwo",
    "eventThree",
    "businessLineOne",
    "businessLineTwo",
    "businessLineThree",
    "riskType",
    "originIncident",
    "incidentsGroup",
    "indicatorIncentive",
    "comments",
    "numberRisk",
    "codesRisk"
  ]
});
var StoreGridIncidentsRevisedBusinessContinuity = Ext.create("Ext.data.Store", {
  model: "ModelGridIncidentsRevisedBusinessContinuity",
  autoLoad: false,
  proxy: {
    actionMethods: { create: "POST", read: "POST", update: "POST" },
    extraParams: { stateIncident: "P" },
    type: "ajax",
    url: "http://localhost:9000/giro/getDetailIncidentsByState.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridIncidentsRevisedBusinessContinuity",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridIncidentsRevisedBusinessContinuity",
    loadMask: true,
    columnLines: true,
    border: false,
    viewConfig: { stripeRows: true },
    store: StoreGridIncidentsRevisedBusinessContinuity,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridIncidentsRevisedBusinessContinuity,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          iconCls: "print",
          handler: function() {
            DukeSource.global.DirtyView.printElementTogrid(
              Ext.ComponentQuery.query(
                "ViewGridIncidentsRevisedRiskOperational"
              )[0]
            );
          }
        },
        "-",
        {
          xtype:"UpperCaseTrigger",
          listeners: {
            keyup: function(a) {
              DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(
                a,
                a.up("grid")
              );
            }
          },
          fieldLabel: "FILTRAR",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var a = this;
        a.store.getProxy().extraParams = {
          typeIncident: DukeSource.global.GiroConstants.CONTINUITY
        };
        a.store.getProxy().url =
          "http://localhost:9000/giro/getIncidentsByState.htm";
        a.store.load();
      }
    },
    initComponent: function() {
      this.columns = [
        { xtype: "rownumberer", width: 40, sortable: false },
        {
          header: "CODIGO<br>INCIDENTE",
          align: "left",
          dataIndex: "idIncident",
          width: 90,
          renderer: function(value, metaData, record) {
            metaData.tdAttr = "";
            if (
              record.get("fileAttachment") == "S" &&
              record.get("confidential") == "N"
            ) {
              return (
                '<div style="display:inline-block;height:30px;width:30px;padding:5px;"><i class="tesla even-attachment"></i></div>' +
                '<div style="height:30px;width:30px;display:inline-block;position:absolute;padding: 5px 0px;">' +
                value +
                "</div>"
              );
            } else if (
              record.get("confidential") == "S" &&
              record.get("fileAttachment") == "N"
            ) {
              metaData.tdAttr = 'style="background-color: #FFDC9E !important;"';
              return (
                '<div style="display:inline-block;height:30px;width:30px;padding:5px;"><i class="tesla even-user-secret"></i></div>' +
                '<div style="height:30px;width:30px;display:inline-block;position:absolute;padding: 5px 0px;">' +
                value +
                "</div>"
              );
            } else if (
              record.get("fileAttachment") == "S" &&
              record.get("confidential") == "S"
            ) {
              metaData.tdAttr = 'style="background-color: #FFDC9E !important;"';
              return (
                '<div style="display:inline-block;height:30px;width:30px;padding:5px;"><i class="tesla even-attachment"></i></div>' +
                '<div style="height:30px;width:30px;display:inline-block;position:absolute;padding: 20px 0px;">' +
                value +
                "</div>" +
                '<div style="height:30px;width:30px;"><i class="tesla even-user-secret"></i></div>'
              );
            } else {
              return (
                '<div style="display:inline-block;height:30px;width:30px;padding:5px;"></div>' +
                '<div style="height:30px;width:30px;display:inline-block;position:absolute;padding: 5px 0px;">' +
                value +
                "</div>"
              );
            }
          }
        },
        {
          header: "ESTADO",
          align: "center",
          dataIndex: "indicatorIncidents",
          width: 130,
          renderer: function(c, b, a) {
            b.tdAttr =
              'style="background-color: #' +
              a.get("colorStateIncident") +
              ' !important;"';
            return "<span>" + a.get("descriptionStateIncident") + "</span>";
          }
        },
        {
          header: "FECHA DE <br>REPORTE<br>",
          align: "center",
          dataIndex: "dateProcess",
          format: "d/m/Y",
          xtype: "datecolumn",
          width: 80,
          renderer: function(a) {
            return (
              '<span style="color:red;">' +
              Ext.util.Format.date(a, "d/m/Y") +
              "</span>"
            );
          }
        },
        {
          text: "USUARIO QUE REPORTA",
          dataIndex: "nameUserRegister",
          width: 150,
          align: "center"
        },
        {
          header: "DESCRIPCI&Oacute;N CORTA",
          dataIndex: "descriptionShort",
          width: 150
        },
        {
          header: "DESCRIPCI&Oacute;N LARGA",
          dataIndex: "descriptionLarge",
          width: 480
        },
        {
          header: "FECHA DE <br>OCURRENCIA<br>",
          align: "center",
          dataIndex: "dateOccurrence",
          xtype: "datecolumn",
          format: "d/m/Y",
          width: 90,
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        {
          header: "FECHA  <br>FIN<br>",
          align: "center",
          dataIndex: "dateFinal",
          xtype: "datecolumn",
          format: "d/m/Y H:i",
          width: 90,
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        {
          header: "FECHA DE <br>DESCUBRIMIENTO<br>",
          align: "center",
          dataIndex: "dateDiscovery",
          xtype: "datecolumn",
          format: "d/m/Y",
          width: 97,
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        { header: "AREA", dataIndex: "descriptionWorkArea", width: 150 },
        {
          header: "AGENCIA",
          dataIndex: "descriptionAgency",
          width: 150
        },
        {
          header: "TIPO DE <br>P&Eacute;RDIDA<br>",
          dataIndex: "descriptionLostType",
          width: 100
        },
        {
          header: "MONEDA",
          dataIndex: "descriptionCurrency",
          width: 60
        },
        {
          header: "MONTO",
          dataIndex: "amountLoss",
          align: "right",
          xtype: "numbercolumn",
          format: "0,0.00",
          width: 100
        }
      ];
      this.callParent(arguments);
    }
  }
);
