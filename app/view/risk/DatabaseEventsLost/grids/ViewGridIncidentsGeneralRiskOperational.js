Ext.define("ModelGridIncidentsGeneralRiskOperational", {
  extend: "Ext.data.Model",
  fields: [
    "idDetailIncidents",
    "descriptionLarge",
    "descriptionShort",
    {
      name: "dateOccurrence",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateFinal",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateDiscovery",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateRegister",
      type: "date",
      format: "d/m/Y",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y");
      }
    },
    "indicatorIncidents",
    "collaborator",
    "nameCollaborator",
    "agency",
    "descriptionAgency",
    "causeCollaborator",
    "workArea",
    "descriptionWorkArea",
    "nameIncidentsIndicator",
    "confidential",
    "typeIncident",
    "impacts",
    "hasTicket",
    "numberTicket",
    "fileAttachment",
    "userRegister",
    "nameUserRegister"
  ]
});
var StoreGridIncidentsGeneralRiskOperational = Ext.create("Ext.data.Store", {
  model: "ModelGridIncidentsGeneralRiskOperational",
  autoLoad: false,
  groupField: "descriptionAgency",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    extraParams: {
      stateIncident: "P"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/getDetailIncidentsByState.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridIncidentsGeneralRiskOperational",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridIncidentsGeneralRiskOperational",
    loadMask: true,
    columnLines: true,
    border: false,
    viewConfig: {
      stripeRows: true
    },
    store: StoreGridIncidentsGeneralRiskOperational,
    features: [
      {
        ftype: "grouping",
        id: "idGroupPendindInc",
        groupHeaderTpl:
          '{name} ({rows.length} total{[values.rows.length > 1 ? "s" : ""]})',
        startCollapsed: true
      }
    ],
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridIncidentsGeneralRiskOperational,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          iconCls: "print",
          handler: function() {
            DukeSource.global.DirtyView.printElementTogrid(
              Ext.ComponentQuery.query(
                "ViewGridIncidentsGeneralRiskOperational"
              )[0]
            );
          }
        },
        "-",
        {
          xtype:"UpperCaseTrigger",
          listeners: {
            keyup: function(a) {
              DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(
                a,
                a.up("grid")
              );
            }
          },
          fieldLabel: "FILTRAR",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var a = this;
        a.store.getProxy().extraParams = {
          stateIncident: "R"
        };
        a.store.getProxy().url =
          "http://localhost:9000/giro/getDetailIncidentsByState.htm";
        a.store.load({
          callback: function() {
            a.getView()
              .getFeature("idGroupPendindInc")
              .disable();
          }
        });
      }
    },
    initComponent: function() {
      this.columns = [
        {
          xtype: "rownumberer",
          width: 25,
          sortable: false
        },
        {
          header: "CODIGO<br>INCIDENTE",
          align: "left",
          dataIndex: "idDetailIncidents",
          width: 90,
          renderer: function(value, metaData, record) {
            metaData.tdAttr = "";
            if (
              record.get("fileAttachment") == "S" &&
              record.get("confidential") == "N"
            ) {
              return (
                '<div style="display:inline-block;height:30px;width:30px;padding:5px;"><i class="tesla even-attachment"></i></div>' +
                '<div style="height:30px;width:30px;display:inline-block;position:absolute;padding: 5px 0px;">' +
                value +
                "</div>"
              );
            } else if (
              record.get("confidential") == "S" &&
              record.get("fileAttachment") == "N"
            ) {
              metaData.tdAttr = 'style="background-color: #FFDC9E !important;"';
              return (
                '<div style="display:inline-block;height:30px;width:30px;padding:5px;"><i class="tesla even-user-secret"></i></div>' +
                '<div style="height:30px;width:30px;display:inline-block;position:absolute;padding: 5px 0px;">' +
                value +
                "</div>"
              );
            } else if (
              record.get("fileAttachment") == "S" &&
              record.get("confidential") == "S"
            ) {
              metaData.tdAttr = 'style="background-color: #FFDC9E !important;"';
              return (
                '<div style="display:inline-block;height:30px;width:30px;padding:5px;"><i class="tesla even-attachment"></i></div>' +
                '<div style="height:30px;width:30px;display:inline-block;position:absolute;padding: 20px 0px;">' +
                value +
                "</div>" +
                '<div style="height:30px;width:30px;"><i class="tesla even-user-secret"></i></div>'
              );
            } else {
              return (
                '<div style="display:inline-block;height:30px;width:30px;padding:5px;"></div>' +
                '<div style="height:30px;width:30px;display:inline-block;position:absolute;padding: 5px 0px;">' +
                value +
                "</div>"
              );
            }
          }
        },
        {
          header: "ESTADO",
          align: "center",
          dataIndex: "indicatorIncidents",
          width: 120,
          renderer: function(c, b, a) {
            if (a.get("indicatorIncidents") == "P") {
              b.tdAttr = 'style="background-color: #DF5353 !important;"';
              return "<span>" + "PENDIENTE" + "</span>";
            } else if (a.get("indicatorIncidents") == "R") {
              b.tdAttr = 'style="background-color: #22E03A !important;"';
              return "<span>" + "PENDIENTE" + "</span>";
            } else {
              return c;
            }
          }
        },
        {
          header: "FECHA DE <br>REPORTE<br>",
          align: "center",
          dataIndex: "dateProcess",
          format: "d/m/Y",
          xtype: "datecolumn",
          width: 80,
          renderer: function(a) {
            return (
              '<span style="color:red;">' +
              Ext.util.Format.date(a, "d/m/Y") +
              "</span>"
            );
          }
        },
        {
          header: "USUARIO QUE REGISTRO",
          dataIndex: "nameUserRegister",
          width: 150
        },
        {
          header: "DESCRIPCI&Oacute;N CORTA",
          dataIndex: "descriptionShort",
          width: 150
        },
        {
          header: "DESCRIPCI&Oacute;N LARGA",
          dataIndex: "descriptionLarge",
          width: 480
        },
        {
          header: "FECHA DE <br>OCURRENCIA<br>",
          align: "center",
          dataIndex: "dateOccurrence",
          xtype: "datecolumn",
          format: "d/m/Y H:i",
          width: 90,
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        {
          header: "FECHA  <br>FIN<br>",
          align: "center",
          dataIndex: "dateFinal",
          xtype: "datecolumn",
          format: "d/m/Y H:i",
          width: 90,
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        {
          header: "FECHA DE <br>DESCUBRIMIENTO<br>",
          align: "center",
          dataIndex: "dateDiscovery",
          xtype: "datecolumn",
          format: "d/m/Y H:i",
          width: 97,
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        {
          header: "AREA",
          dataIndex: "descriptionWorkArea",
          width: 150
        },
        {
          header: "AGENCIA",
          dataIndex: "descriptionAgency",
          width: 150
        }
      ];
      this.callParent(arguments);
    }
  }
);
