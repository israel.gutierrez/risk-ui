Ext.define("ModelGridIncidentsRevisedRiskOperational", {
  extend: "Ext.data.Model",
  fields: [
    "idIncident",
    "idDetailIncidents",
    "descriptionLarge",
    "descriptionShort",
    {
      name: "dateOccurrence",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateFinal",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateDiscovery",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateRegister",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateReport",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    "indicatorIncidents",
    "stateIncident",
    "colorStateIncident",
    "descriptionStateIncident",
    "sequenceStateIncident",
    "collaborator",
    "nameCollaborator",
    "agency",
    "descriptionAgency",
    "causeCollaborator",
    "workArea",
    "descriptionWorkArea",
    "confidential",
    "typeIncident",
    "subCategory",
    "typeBreach",
    "nameBreach",
    "commentBreach",
    "hasTicket",
    "numberTicket",
    "fileAttachment",
    "lostType",
    "descriptionLostType",
    "factorRisk",
    "descriptionFactorRisk",
    "currency",
    "descriptionCurrency",
    "amountLoss",
    "dateRegisterPreIncident",
    "workAreaPreIncident",
    "userRegister",
    "fullNameUserRegister",
    "workAreaRegister",
    "userReport",
    "fullNameUserReport",
    "fullNameUserReport",
    "commentManager",
    "impact",
    "descriptionImpact",
    "idProcessType",
    "idProcess",
    "idSubProcess",
    "idActivity",
    "descriptionProcess",
    "descriptionIndicatorIncidents",
    "eventOne",
    "eventTwo",
    "eventThree",
    "businessLineOne",
    "businessLineTwo",
    "businessLineThree",
    "riskType",
    "originIncident",
    "incidentsGroup",
    "indicatorIncentive",
    "comments",
    "numberRisk",
    "codeIncident",
    "codesRisk",
    "codeIncident",
    "codeCorrelative",
    "checkApprove",
    "userApprove",
    "commentApprove",
    "dateApprove",
    "materialization",
    "typeVulnerability",
    "typeThreat",
    "assetInvolved",
    "product",
    "criticalLevel",
    {
      name: "numberCustomerAffect",
      type: "number",
      convert: function(value) {
        return parseInt(value);
      }
    },
    {
      name: "lossCustomer",
      type: "number",
      convert: function(value) {
        return parseFloat(value);
      }
    },
    {
      name: "costRecoveryMitigation",
      type: "number",
      convert: function(value) {
        return parseFloat(value);
      }
    },
    {
      name: "percentRecoveryMitigation",
      type: "number",
      convert: function(value) {
        return parseFloat(value);
      }
    },
    "nameSupplier",
    "rutSupplier"
  ]
});
var StoreGridIncidentsRevisedRiskOperational = Ext.create("Ext.data.Store", {
  model: "ModelGridIncidentsRevisedRiskOperational",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    extraParams: {
      stateIncident: "P"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/getDetailIncidentsByState.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridIncidentsRevisedRiskOperational",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridIncidentsRevisedRiskOperational",
    loadMask: true,
    columnLines: true,
    border: true,
    padding: 1,
    viewConfig: { stripeRows: true },
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridIncidentsRevisedRiskOperational,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          iconCls: "print",
          handler: function() {
            DukeSource.global.DirtyView.printElementTogrid(
              Ext.ComponentQuery.query(
                "ViewGridIncidentsRevisedRiskOperational"
              )[0]
            );
          }
        },
        "-",
        {
          xtype:"UpperCaseTrigger",
          listeners: {
            keyup: function(a) {
              DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(
                a,
                a.up("grid")
              );
            }
          },
          fieldLabel: "Filtrar",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
      doRefresh: function() {
        var grid = this.up("grid");
        var index = DukeSource.global.DirtyView.getSelectedRowIndex(grid);

        grid.store.load({
          callback: function() {
            if (index !== -1) {
              grid.getSelectionModel().select(index);
            }
          }
        });
      }
    },

    initComponent: function() {
      var me = this;

      Ext.applyIf(me, {
        tbar: [
          {
            xtype: "combobox",
            displayField: "description",
            valueField: "id",
            itemId: "cbxFilterIncident",
            fieldLabel: "Tipo de incidente",
            labelWidth: 120,
            value: "RO",
            hidden: hidden("INC_CBX_typeIncident"),
            store: Ext.create("Ext.data.Store", {
              fields: ["id", "description"],
              data: [
                { id: "RO", description: "Riesgo operacional" },
                { id: "CS", description: "Ciberseguridad" }
              ]
            }),
            listeners: {
              select: function(cbo) {
                var a = Ext.ComponentQuery.query(
                  "ViewGridIncidentsRevisedRiskOperational"
                )[0];
                a.store.getProxy().extraParams = {
                  typeIncident: cbo.getValue()
                };
                a.store.getProxy().url =
                  "http://localhost:9000/giro/getIncidentsByState.htm";
                a.store.load();
              }
            }
          },
          "-",
          {
            xtype: "button",
            cls: "my-btn",
            overCls: "my-over",
            itemId: "btnAddIncidentRevised",
            iconCls: "add",
            scale: "medium",
            hidden: true,
            text: "Reportar incidente",
            handler: function() {
              var typeInc = me.down("#cbxFilterIncident").getValue();

              if (typeInc === "RO") {
                if (category === DukeSource.global.GiroConstants.COLLABORATOR)
                  createWindow(
                    "ViewWindowIncidentCollaborator",
                    DukeSource.global.GiroConstants.OPERATIONAL
                  );
                else if (category === DukeSource.global.GiroConstants.GESTOR)
                  createWindow(
                    "ViewWindowIncidentManager",
                    DukeSource.global.GiroConstants.OPERATIONAL
                  );
                else if (category === DukeSource.global.GiroConstants.ANALYST) {
                  createWindow(
                    "ViewWindowRegisterIncidents",
                    DukeSource.global.GiroConstants.OPERATIONAL
                  );
                  var win = Ext.ComponentQuery.query(
                    "ViewWindowRegisterIncidents"
                  )[0];
                  win.down("#originIncident").store.load({
                    callback: function() {
                      win
                        .down("#originIncident")
                        .setValue(DukeSource.global.GiroConstants.GIRO);
                    }
                  });

                  win.down("#userReport").setValue(userName);
                  win.down("#fullNameUserReport").setValue(fullName);
                }
              } else if (typeInc === "CS") {
                createWindow(
                  "WindowRegisterIssueCiberSecurity",
                  DukeSource.global.GiroConstants.CYBER_SECURITY
                );
              }

              function createWindow(view, typeIncident) {
                var c = Ext.create(
                  "DukeSource.view.risk.DatabaseEventsLost.windows." + view,
                  {
                    modal: true,
                    typeIncident: typeIncident
                  }
                );

                var dateRegisterTemp = c.down("#dateRegisterTemp");
                var dateRegister = c.down("#dateRegister");
                setInterval(function() {
                  dateRegister.setValue(new Date());
                  dateRegisterTemp.setValue(new Date());
                }, 1000);

                c.show();
                c.down("#descriptionShort").focus(false, 100);
              }
            }
          },
          "->",
          {
            xtype: "button",
            scale: "medium",
            cls: "my-btn",
            overCls: "my-over",
            iconCls: "auditory",
            text: "Auditoría",
            handler: function(btn) {
              var exist = btn
                .up("panel")
                .down("#panelView")
                .getActiveTab()
                .down("grid");
              var grid;
              if (exist === null) {
                grid = btn
                  .up("panel")
                  .down("#panelView")
                  .getActiveTab();
                DukeSource.global.DirtyView.showAuditory(
                  grid,
                  "http://localhost:9000/giro/findAuditDetailIncidents.htm"
                );
              } else {
                grid = btn
                  .up("panel")
                  .down("#panelView")
                  .getActiveTab()
                  .down("grid");
                DukeSource.global.DirtyView.showWindowAuditory(
                  grid,
                  "http://localhost:9000/giro/findAuditIncident.htm"
                );
              }
            }
          }
        ],

        columns: [
          {
            header: "Fecha de reporte",
            align: "center",
            dataIndex: "dateReport",
            format: "d/m/Y H:i:s",
            xtype: "datecolumn",
            width: 80,
            renderer: function(a) {
              return (
                '<span style="color:red;">' +
                Ext.util.Format.date(a, "d/m/Y H:i:s") +
                "</span>"
              );
            }
          },
          {
            header: "Impacto",
            hidden: hidden("INC_GRD_impactReport"),
            dataIndex: "descriptionImpact",
            width: 90
          },
          {
            header: "Descripci&oacute;n corta",
            dataIndex: "descriptionShort",
            width: 150
          },
          {
            header: "Descripci&oacute;n larga",
            dataIndex: "descriptionLarge",
            width: 480
          },
          {
            header: "Fecha de ocurrencia",
            align: "center",
            dataIndex: "dateOccurrence",
            hidden: hidden("INC_WIR_DTF_DateDiscovery"),
            xtype: "datecolumn",
            format: "d/m/Y",
            width: 90,
            renderer: Ext.util.Format.dateRenderer("d/m/Y H:i:s")
          },
          {
            header: "Fecha de descubrimiento",
            align: "center",
            dataIndex: "dateDiscovery",
            xtype: "datecolumn",
            format: "d/m/Y",
            width: 97,
            renderer: Ext.util.Format.dateRenderer("d/m/Y H:i:s")
          },
          {
            header: "Area",
            dataIndex: "descriptionWorkArea",
            width: 150
          }
        ],

        store: StoreGridIncidentsRevisedRiskOperational,

        listeners: {
          render: function() {
            var a = this;
            var userRegister = Ext.create("Ext.grid.column.Column", {
              text: "Usuario que reporta",
              dataIndex: "fullNameUserReport",
              width: 150,
              renderer: function(value, metaData, record) {
                if (
                  record.get("typeIncident") ===
                  DukeSource.global.GiroConstants.OPERATIONAL
                ) {
                  return record.get("fullNameUserReport");
                } else {
                  return record.get("fullNameUserRegister");
                }
              }
            });
            if (
              category === DukeSource.global.GiroConstants.COLLABORATOR ||
              category === DukeSource.global.GiroConstants.GESTOR
            ) {
              var detailIncident = Ext.create("Ext.grid.column.Column", {
                header: "Código incidente",
                align: "left",
                dataIndex: "idDetailIncidents",
                width: 130,
                renderer: function(value, metaData, record) {
                  var attachment = "";
                  var secret = "";
                  var state =
                    '<div style="font-size:9px">' +
                    record.get("descriptionStateIncident") +
                    "</div>";
                  if (record.get("fileAttachment") === "S") {
                    attachment =
                      '<div style="display:inline-block;"><i class="tesla even-attachment"></i></div>';
                  }
                  if (record.get("confidential") === "S") {
                    secret =
                      '<div style="display:inline-block;padding-left:2px;"><i class="tesla even-user-secret"></i></div>';
                  }
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("colorStateIncident") +
                    ' !important;"';
                  return (
                    '<div style="display:inline-block;font-size:12px;padding:5px;">' +
                    attachment +
                    secret +
                    "" +
                    '<span style="padding-left:5px;font-weight:bold">' +
                    value +
                    "</span></div>" +
                    state
                  );
                }
              });
              if (category === DukeSource.global.GiroConstants.GESTOR) {
                a.headerCt.insert(3, userRegister);
                a.store.getProxy().extraParams = {
                  stateIncident: "A"
                };
              } else {
                a.store.getProxy().extraParams = {
                  stateIncident: "R"
                };
              }
              a.headerCt.insert(1, detailIncident);
              a.getView().refresh();
              a.store.getProxy().url =
                "http://localhost:9000/giro/getDetailIncidentsByState.htm";
              a.store.load();
            } else {
              var code = Ext.create("Ext.grid.column.Column", {
                header: "Código incidente",
                align: "center",
                dataIndex: "codeIncident",
                width: 110,
                renderer: function(value, metaData, record) {
                  var attachment = "";
                  var secret = "";
                  var state =
                    '<div style="font-size:9px">' +
                    record.get("descriptionStateIncident") +
                    "</div>";
                  if (record.get("fileAttachment") === "S") {
                    attachment =
                      '<div style="display:inline-block;"><i class="tesla even-attachment"></i></div>';
                  }
                  if (record.get("confidential") === "S") {
                    secret =
                      '<div style="display:inline-block;padding-left:2px;"><i class="tesla even-user-secret"></i></div>';
                  }
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("colorStateIncident") +
                    ' !important;"';
                  return (
                    '<div style="display:inline-block;font-size:12px;padding:5px;">' +
                    attachment +
                    secret +
                    "" +
                    '<span style="padding-left:5px;font-weight:bold">' +
                    value +
                    "</span></div>" +
                    state
                  );
                }
              });
              var riskAssociated = Ext.create("Ext.grid.column.Column", {
                header: "Riesgos",
                columns: [
                  {
                    header: "Nro",
                    align: "center",
                    dataIndex: "numberRisk",
                    width: 50
                  },
                  {
                    header: "Riesgos",
                    align: "center",
                    dataIndex: "codesRisk",
                    width: 120,
                    renderer: function(a) {
                      return '<span style="color:red;">' + a + "</span>";
                    }
                  }
                ]
              });
              var coin = Ext.create("Ext.grid.column.Column", {
                text: "Moneda",
                dataIndex: "descriptionCurrency",
                width: 60,
                align: "center"
              });
              var amount = Ext.create("Ext.grid.column.Column", {
                text: "Monto",
                dataIndex: "amountLoss",
                width: 100,
                align: "right",
                xtype: "numbercolumn",
                format: "0,0.00"
              });

              a.headerCt.insert(0, code);
              a.headerCt.insert(12, coin);
              a.headerCt.insert(13, amount);
              a.headerCt.insert(14, riskAssociated);
              a.headerCt.insert(4, userRegister);
              a.getView().refresh();
              a.store.getProxy().extraParams = {
                typeIncident: DukeSource.global.GiroConstants.OPERATIONAL
              };
              a.store.getProxy().url =
                "http://localhost:9000/giro/getIncidentsByState.htm";
              a.store.load();
            }
          }
        }
      });
      me.callParent(arguments);
    }
  }
);
