Ext.define("ModelGridIncidentsRefusedRiskOperational", {
  extend: "Ext.data.Model",
  fields: [
    "idIncident",
    "idDetailIncidents",
    "descriptionLarge",
    "descriptionShort",
    {
      name: "dateOccurrence",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateFinal",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateDiscovery",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateProcess",
      type: "date",
      format: "d/m/Y",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y");
      }
    },
    "indicatorIncidents",
    "stateIncident",
    "colorStateIncident",
    "descriptionStateIncident",
    "collaborator",
    "nameCollaborator",
    "agency",
    "descriptionAgency",
    "causeCollaborator",
    "workArea",
    "descriptionWorkArea",
    "nameIncidentsIndicator",
    "confidential",
    "criterionSafety",
    "subClassification",
    "typeIncident",
    "idFixedAssets",
    "descriptionFixedAssets",
    "hourEnd",
    "hourInit",
    "fileAttachment",
    "lostType",
    "descriptionLostType",
    "factorRisk",
    "descriptionFactorRisk",
    "currency",
    "descriptionCurrency",
    "amountLoss",
    "userRegister",
    "nameUserRegister",
    "commentManager",
    "descriptionIndicatorIncidents",
    "impactReport",
    "detailFactorRisk",
    "processOrigin",
    "subProcessOrigin",
    "processImpact",
    "subProcessImpact",
    "eventOne",
    "eventTwo",
    "eventThree",
    "businessLineOne",
    "businessLineTwo",
    "businessLineThree",
    "riskType",
    "originIncident",
    "incidentsGroup",
    "indicatorIncentive",
    "comments",
    "state"
  ]
});
var StoreGridIncidentsRefusedRiskOperational = Ext.create("Ext.data.Store", {
  model: "ModelGridIncidentsRefusedRiskOperational",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    extraParams: {
      stateIncident: "X"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/getDetailIncidentsByState.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridIncidentsRefusedRiskOperational",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridIncidentsRefusedRiskOperational",
    loadMask: true,
    columnLines: true,
    border: true,
    padding: 1,
    viewConfig: {
      stripeRows: true
    },
    store: StoreGridIncidentsRefusedRiskOperational,

    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridIncidentsRefusedRiskOperational,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          iconCls: "print",
          handler: function() {
            DukeSource.global.DirtyView.printElementTogrid(
              Ext.ComponentQuery.query(
                "ViewGridIncidentsRefusedRiskOperational"
              )[0]
            );
          }
        },
        "-",
        {
          xtype:"UpperCaseTrigger",
          listeners: {
            keyup: function(a) {
              DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(
                a,
                a.up("grid")
              );
            }
          },
          fieldLabel: "FILTRAR",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },

    listeners: {
      render: function() {
        var a = this;
        var typeIncident;
        if (category === DukeSource.global.GiroConstants.SECURITY_ANALYST) {
          typeIncident = DukeSource.global.GiroConstants.SECURITY;
        } else if (
          category === DukeSource.global.GiroConstants.BUSINESS_CONTINUITY
        ) {
          typeIncident = DukeSource.global.GiroConstants.CONTINUITY;
        } else {
          typeIncident = DukeSource.global.GiroConstants.OPERATIONAL;
        }
        a.store.getProxy().extraParams = {
          stateIncident: "X",
          typeIncident: typeIncident
        };
        a.store.getProxy().url =
          "http://localhost:9000/giro/getIncidentsByState.htm";
        a.store.load();
      },
      itemdblclick: function(grid, record) {
        var windows;
        if (
          Ext.ComponentQuery.query("ViewWindowRegisterIncidents")[0] ===
            undefined &&
          category === DukeSource.global.GiroConstants.ANALYST
        ) {
          windows = Ext.create(
            "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterIncidents",
            {
              typeIncident: record.get("indicatorIncidents")
            }
          );
        } else if (
          Ext.ComponentQuery.query("ViewWindowRegisterIncidents")[0] ===
            undefined &&
          category === DukeSource.global.GiroConstants.SECURITY_ANALYST
        ) {
          windows = Ext.create(
            "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterIncidentsSI",
            {
              typeIncident: record.get("indicatorIncidents")
            }
          );
        } else {
          windows = Ext.create(
            "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterIncidentsCN",
            {
              typeIncident: record.get("indicatorIncidents")
            }
          );
        }
        windows
          .down("#eventOne")
          .getStore()
          .load();
        windows
          .down("#eventTwo")
          .getStore()
          .load({
            url:
              "http://localhost:9000/giro/showListEventTwoActivesComboBox.htm",
            params: {
              valueFind: record.get("eventOne")
            }
          });
        windows.down("#eventTwo").setDisabled(false);
        windows
          .down("#eventThree")
          .getStore()
          .load({
            url:
              "http://localhost:9000/giro/showListEventThreeActivesComboBox.htm",
            params: {
              valueFind: record.get("eventOne"),
              valueFind2: record.get("eventTwo")
            }
          });
        windows.down("#eventThree").setDisabled(false);
        windows
          .down("#businessLineOne")
          .getStore()
          .load();
        windows
          .down("#businessLineTwo")
          .getStore()
          .load({
            url:
              "http://localhost:9000/giro/showListBusinessLineTwoActivesComboBox.htm",
            params: {
              valueFind: record.get("businessLineOne")
            }
          });
        windows.down("#businessLineTwo").setDisabled(false);
        windows
          .down("#businessLineThree")
          .getStore()
          .load({
            url:
              "http://localhost:9000/giro/showListBusinessLineThreeActivesComboBox.htm",
            params: {
              idBusinessLineOne: record.get("businessLineOne"),
              idBusinessLineTwo: record.get("businessLineTwo")
            }
          });
        windows.down("#businessLineThree").setDisabled(false);
        windows
          .down("#agency")
          .getStore()
          .load({
            callback: function() {
              if (
                !(
                  record.get("workArea") == "" ||
                  record.get("workArea") == undefined
                )
              ) {
                windows
                  .down("#workArea")
                  .getStore()
                  .load();
              }
              if (
                !(
                  record.get("factorRisk") == "" ||
                  record.get("factorRisk") == undefined
                )
              ) {
                windows
                  .down("#factorRisk")
                  .getStore()
                  .load();
              }
              if (
                !(
                  record.get("incidentsGroup") == "" ||
                  record.get("incidentsGroup") == undefined
                )
              ) {
                windows
                  .down("#incidentsGroup")
                  .getStore()
                  .load();
              }
              if (!(record.get("lostType") == "2")) {
                windows
                  .down("#currency")
                  .getStore()
                  .load();
                windows.down("#groupLostAmount").setVisible(true);
                windows.down("#amountLoss").allowBlank = false;
                windows
                  .down("#amountLoss")
                  .setFieldStyle("background: #d9ffdb");
                windows.down("#currency").allowBlank = false;
                windows.down("#currency").setFieldStyle("background: #d9ffdb");
              }

              if (record.get("originIncident") == "CO") {
                windows.down("#containerThreeColumn").add(2, {
                  xtype: "container",
                  itemId: "containerUserReport",
                  height: 26,
                  layout: "hbox",
                  items: [
                    {
                      xtype: "textfield",
                      name: "userReport",
                      itemId: "userReport",
                      hidden: true
                    },
                    {
                      xtype:
                       "UpperCaseTextFieldReadOnly",
                      flex: 1,
                      allowBlank: false,
                      value: record.get("nameUserRegister"),
                      name: "fullNameUserReport",
                      itemId: "fullNameUserReport",
                      fieldLabel: "QUIEN REPORTO"
                    },
                    {
                      xtype: "button",
                      iconCls: "search",
                      handler: function() {
                        var win = Ext.create(
                          "DukeSource.view.risk.util.search.SearchUser",
                          {
                            modal: true
                          }
                        ).show();
                        win.down("grid").on("itemdblclick", function() {
                          windows.down("textfield[name=userReport]").setValue(
                            win
                              .down("grid")
                              .getSelectionModel()
                              .getSelection()[0]
                              .get("userName")
                          );
                          windows
                            .down(
                              "UpperCaseTextFieldReadOnly[name=fullNameUserReport]"
                            )
                            .setValue(
                              win
                                .down("grid")
                                .getSelectionModel()
                                .getSelection()[0]
                                .get("fullName")
                            );
                          win.close();
                        });
                      }
                    }
                  ]
                });
                windows
                  .down("#userReport")
                  .setValue(record.get("userRegister"));
              }
              windows
                .down("#impactReport")
                .getStore()
                .load();
              windows
                .down("#processOrigin")
                .getStore()
                .load();
              windows
                .down("#subProcessOrigin")
                .getStore()
                .load({
                  url:
                    "http://localhost:9000/giro/showListSubProcessActives.htm",
                  params: {
                    valueFind: record.get("processOrigin")
                  }
                });
              windows.down("#subProcessOrigin").setDisabled(false);
              windows
                .down("#processImpact")
                .getStore()
                .load();
              windows
                .down("#subProcessImpact")
                .getStore()
                .load({
                  url:
                    "http://localhost:9000/giro/showListSubProcessActives.htm",
                  params: {
                    valueFind: record.get("processImpact")
                  }
                });
              windows.down("#subProcessImpact").setDisabled(false);

              windows
                .down("#originIncident")
                .getStore()
                .load();
              windows
                .down("#stateIncident")
                .getStore()
                .load();

              windows
                .down("form")
                .getForm()
                .setValues(record.data);
              if (record.get("state") == "N") {
                windows.query(".textfield,.numberfield").forEach(function(c) {
                  DukeSource.global.DirtyView.toReadOnly(c);
                });
                windows.down("#confidential").setDisabled(true);
                windows.down("#document").setDisabled(true);
                var buttons = windows.query("button");
                for (var i = 0; i < buttons.length - 1; i++) {
                  buttons[i].setDisabled(true);
                }
              }
              windows.show();
            }
          });
      }
    },

    initComponent: function() {
      this.columns = [
        {
          xtype: "rownumberer",
          width: 40,
          sortable: false
        },
        {
          header: "ESTADO",
          align: "center",
          dataIndex: "indicatorIncidents",
          width: 130,
          renderer: function(value, metaData, record) {
            var attachment = "";
            if (record.get("fileAttachment") === "S") {
              attachment = '<div><i class="tesla even-attachment"></i></div>';
            }
            metaData.tdAttr =
              'style="background-color: #' +
              record.get("colorStateIncident") +
              ' !important;"';
            return (
              attachment +
              "<div style='font-size: 9px'>" +
              record.get("descriptionStateIncident") +
              "</div>"
            );
          }
        },
        {
          header: "FECHA DE <br>REPORTE<br>",
          align: "center",
          dataIndex: "dateProcess",
          format: "d/m/Y",
          xtype: "datecolumn",
          width: 80,
          renderer: function(value, metaData, record) {
            return (
              '<span style="color:green;">' +
              Ext.util.Format.date(value, "d/m/Y") +
              "</span>"
            );
          }
        },
        {
          header: "USUARIO QUE REGISTRO",
          dataIndex: "nameUserRegister",
          width: 150
        },
        {
          header: "DESCRIPCI&Oacute;N CORTA",
          dataIndex: "descriptionShort",
          width: 150
        },
        {
          header: "DESCRIPCI&Oacute;N LARGA",
          dataIndex: "descriptionLarge",
          width: 480
        },
        {
          header: "FECHA DE <br>OCURRENCIA<br>",
          align: "center",
          dataIndex: "dateOccurrence",
          xtype: "datecolumn",
          format: "d/m/Y H:i",
          width: 90,
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        {
          header: "FECHA  <br>FIN<br>",
          align: "center",
          dataIndex: "dateFinal",
          xtype: "datecolumn",
          format: "d/m/Y H:i",
          width: 90,
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        {
          header: "FECHA DE <br>DESCUBRIMIENTO<br>",
          align: "center",
          dataIndex: "dateDiscovery",
          xtype: "datecolumn",
          format: "d/m/Y H:i",
          width: 97,
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        {
          header: "AREA",
          dataIndex: "descriptionWorkArea",
          width: 150
        },
        {
          header: "AGENCIA",
          dataIndex: "descriptionAgency",
          width: 150
        }
      ];
      this.callParent(arguments);
    }
  }
);
