Ext.define("ModelGridIncidentsGestorRiskOperational", {
  extend: "Ext.data.Model",
  fields: [
    "idDetailIncidents",
    "descriptionLarge",
    "descriptionShort",
    {
      name: "dateOccurrence",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateFinal",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateDiscovery",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateRegister",
      type: "date",
      format: "d/m/Y",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y");
      }
    },
    "indicatorIncidents",
    "collaborator",
    "nameCollaborator",
    "agency",
    "descriptionAgency",
    "causeCollaborator",
    "workArea",
    "descriptionWorkArea",
    "nameIncidentsIndicator",
    "confidential",
    "typeIncident",
    "impacts",
    "hasTicket",
    "numberTicket",
    "fileAttachment",
    "lostType",
    "descriptionLostType",
    "factorRisk",
    "descriptionFactorRisk",
    "currency",
    "descriptionCurrency",
    "amountLoss",
    "userRegister",
    "nameUserRegister",
    "commentManager"
  ]
});
var StoreGridIncidentsGestorRiskOperational = Ext.create("Ext.data.Store", {
  model: "ModelGridIncidentsGestorRiskOperational",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    extraParams: {
      stateIncident: "A"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/getDetailIncidentsByState.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridIncidentsGestorRiskOperational",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridIncidentsGestorRiskOperational",
    loadMask: true,
    border: false,
    columnLines: true,
    viewConfig: {
      stripeRows: true
    },
    store: StoreGridIncidentsGestorRiskOperational,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridIncidentsGestorRiskOperational,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          iconCls: "print",
          handler: function() {
            DukeSource.global.DirtyView.printElementTogrid(
              Ext.ComponentQuery.query(
                "ViewGridIncidentsGestorRiskOperational"
              )[0]
            );
          }
        },
        "-",
        {
          xtype:"UpperCaseTrigger",
          listeners: {
            keyup: function(a) {
              DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(
                a,
                a.up("grid")
              );
            }
          },
          fieldLabel: "FILTRAR",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var a = this;
        a.store.getProxy().extraParams = {
          stateIncident: "A"
        };
        a.store.getProxy().url =
          "http://localhost:9000/giro/getDetailIncidentsByState.htm";
        a.store.load({});
      }
    },
    initComponent: function() {
      this.columns = [
        { xtype: "rownumberer", width: 40, sortable: false },
        {
          header: "CODIGO",
          align: "left",
          dataIndex: "idDetailIncidents",
          width: 70,
          renderer: function(value, metaData, record) {
            metaData.tdAttr = "";
            if (record.get("fileAttachment") == "S") {
              return (
                '<div style="display:inline-block;height:30px;width:30px;padding:5px;"><i class="tesla even-attachment"></i></div>' +
                '<div style="height:30px;width:30px;display:inline-block;position:absolute;padding: 5px 0px;">' +
                value +
                "</div>"
              );
            } else {
              return (
                '<div style="display:inline-block;height:30px;width:30px;padding:5px;"></div>' +
                '<div style="height:30px;width:30px;display:inline-block;position:absolute;padding: 5px 0px;">' +
                value +
                "</div>"
              );
            }
          }
        },
        {
          header: "ESTADO",
          align: "center",
          dataIndex: "indicatorIncidents",
          width: 120,
          renderer: function(c, b, a) {
            if (a.get("indicatorIncidents") == "A") {
              b.tdAttr = 'style="background-color: #4491B5 !important;"';
              return "<span>" + "REVISADO" + "</span>";
            }
          }
        },
        {
          header: "FECHA DE <br>REPORTE<br>",
          align: "center",
          dataIndex: "dateProcess",
          format: "d/m/Y",
          xtype: "datecolumn",
          width: 80,
          renderer: function(a) {
            return (
              '<span style="color:red;">' +
              Ext.util.Format.date(a, "d/m/Y") +
              "</span>"
            );
          }
        },
        {
          header: "USUARIO QUE REGISTRO",
          dataIndex: "nameUserRegister",
          align: "center",
          width: 150
        },
        {
          header: "DESCRIPCI&Oacute;N CORTA",
          dataIndex: "descriptionShort",
          width: 150
        },
        {
          header: "DESCRIPCI&Oacute;N LARGA",
          dataIndex: "descriptionLarge",
          width: 480
        },
        {
          header: "FECHA DE <br>OCURRENCIA<br>",
          align: "center",
          dataIndex: "dateOccurrence",
          xtype: "datecolumn",
          format: "d/m/Y H:i",
          width: 90,
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        {
          header: "FECHA  <br>FIN<br>",
          align: "center",
          dataIndex: "dateFinal",
          xtype: "datecolumn",
          format: "d/m/Y H:i",
          width: 90,
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        {
          header: "FECHA DE <br>DESCUBRIMIENTO<br>",
          align: "center",
          dataIndex: "dateDiscovery",
          xtype: "datecolumn",
          format: "d/m/Y H:i",
          width: 97,
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        { header: "AREA", dataIndex: "descriptionWorkArea", width: 150 },
        {
          header: "AGENCIA",
          dataIndex: "descriptionAgency",
          width: 150
        }
      ];
      this.callParent(arguments);
    }
  }
);
