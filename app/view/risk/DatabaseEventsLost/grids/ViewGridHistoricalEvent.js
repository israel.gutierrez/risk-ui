Ext.define("ModelGridHistoricalEvent", {
  extend: "Ext.data.Model",
  fields: [
    "idEvent",
    "codeEvent",
    "workArea",
    "isRiskEventIncidents",
    "idBusinessLineTwo",
    "businessLineOne",
    "factorRisk",
    "idEventThree",
    "eventTwo",
    "eventOne",
    "managerRisk",
    "agency",
    "currency",
    "riskType",
    "businessLineThree",
    "actionPlan",
    "process",
    "descriptionProcess",
    "idSubProcess",
    "descriptionSubProcess",
    "insuranceCompany",
    "eventState",
    "nameEventState",
    "colorEventState",
    "sequenceEventState",
    "eventType",
    "lossType",
    "accountingEntryLoss",
    "accountingEntryRecoveryLoss",
    "descriptionLargeEvent",
    "descriptionShortEvent",
    "descriptionAgency",
    "descriptionRegion",
    "descriptionWorkArea",
    {
      name: "dateOccurrence",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateAcceptLossEvent",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateCloseEvent",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateDiscovery",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    {
      name: "dateAccountingEvent",
      type: "date",
      format: "d/m/Y H:i:s",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y H:i:s");
      }
    },
    "entityPenalizesLoss",
    "grossLoss",
    "lossRecovery",
    "sureAmountRecovery",
    "totalAmountSpend",
    "amountSubEvent",
    "sureAmount",
    "netLoss",
    "typeChange",
    "typeEvent",
    "nameEventThree",
    "nameEventTwo",
    "nameEventOne",
    "nameFactorRisk",
    "nameAction",
    "nameCurrency",
    "descriptionShort",
    "idPlanAccountSureAmountRecovery",
    "planAccountSureAmountRecovery",
    "planAccountLossRecovery",
    "idPlanAccountLossRecovery",
    "bookkeepingEntryLossRecovery",
    "bookkeepingEntrySureAmountRecovery",
    "amountOrigin",
    "codeGeneralIncidents",
    "idIncident",
    "cause",
    "fileAttachment",
    "codesRiskAssociated",
    "numberRiskAssociated",
    "indicatorAtRisk",
    "eventMain",
    "numberRelation",
    "numberActionPlan",
    "codesActionPlan"
  ]
});
var StoreGridHistoricalEvent = Ext.create("Ext.data.Store", {
  model: "ModelGridHistoricalEvent",
  autoLoad: false,
  groupField: "descriptionAgency",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridHistoricalEvent",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridHistoricalEvent",
    loadMask: true,
    columnLines: true,
    cls: "incident-grid",
    viewConfig: {
      stripeRows: true
    },
    store: StoreGridHistoricalEvent,
    features: [
      {
        ftype: "grouping",
        id: "idGroupPendindInc",
        groupHeaderTpl:
          '{name} ({rows.length} total{[values.rows.length > 1 ? "s" : ""]})',
        hideGroupedHeader: true,
        startCollapsed: false
      }
    ],
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: StoreGridHistoricalEvent,
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          iconCls: "print",
          handler: function() {
            DukeSource.global.DirtyView.printElementTogrid(
              Ext.ComponentQuery.query("ViewGridHistoricalEvent")[0]
            );
          }
        },
        "-",
        {
          xtype:"UpperCaseTrigger",
          listeners: {
            keyup: function(text) {
              DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(
                text,
                text.up("grid")
              );
            }
          },
          fieldLabel: "FILTRAR",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {},
    initComponent: function() {
      this.columns = [
        { xtype: "rownumberer", width: 25, sortable: false },
        {
          header: "CODIGO",
          align: "left",
          dataIndex: "codeEvent",
          width: 110,
          renderer: function(value, metaData, record) {
            var state;
            var type = "";
            metaData.tdAttr =
              'style="background-color: #' +
              record.get("colorEventState") +
              ' !important; height:40px;"';
            state =
              '<br><span style="font-size:9px">' +
              record.get("nameEventState") +
              "</span>";
            if (record.get("eventType") == "2") {
              type =
                '<i class="tesla even-stack2" style="margin-left:18px;line-height: 0.2 !important;"></i>';
            } else if (record.get("eventState") == "X") {
              metaData.tdAttr =
                'style="background-color: #cccccc !important; height:40px;"';
              type =
                '<i class="tesla even-blocked-24" style="margin-left:18px;line-height: 0.2 !important;"></i>';
            }

            if (record.get("fileAttachment") == "S") {
              return (
                '<div style="display:inline-block;height:20px;width:20px;padding:2px;"><i class="tesla even-attachment"></i></div><div style="display:inline-block;position:absolute;">' +
                '<span style="font-size: 14px;">' +
                value +
                type +
                "</span> " +
                state +
                "</div>"
              );
            } else {
              return (
                '<div style="display:inline-block;height:20px;width:20px;padding:2px;"></div><div style="display:inline-block;position:absolute;">' +
                '<span style="font-size: 14px;">' +
                value +
                type +
                "</span> " +
                state +
                "</div>"
              );
            }
          }
        },
        {
          header: "DESCRIPCI&Oacute;N CORTA",
          align: "left",
          dataIndex: "descriptionShort",
          width: 270
        },

        {
          header: "FECHA DE<br>REPORTE<br>",
          align: "center",
          dataIndex: "dateAcceptLossEvent",
          xtype: "datecolumn",
          format: "d/m/Y H:i",
          width: 90,
          renderer: function(a) {
            return (
              '<span style="color:green;">' +
              Ext.util.Format.date(a, "d/m/Y H:i") +
              "</span>"
            );
          }
        },
        {
          header: "FECHA DE<br>OCURRENCIA<br>",
          align: "center",
          dataIndex: "dateOccurrence",
          xtype: "datecolumn",
          format: "d/m/Y H:i",
          width: 90,
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        {
          header: "FECHA DE<br>DESCUBRIMIENTO<br>",
          align: "center",
          dataIndex: "dateDiscovery",
          xtype: "datecolumn",
          format: "d/m/Y H:i",
          width: 97,
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        {
          header: "FECHA DE<br>CONTABILIZACI&Oacute;N<br>",
          align: "center",
          dataIndex: "dateAccountingEvent",
          xtype: "datecolumn",
          format: "d/m/Y H:i",
          width: 97,
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        {
          header: "FECHA DE<br>CIERRE<br>",
          dataIndex: "dateCloseEvent",
          width: 90,
          align: "center",
          xtype: "datecolumn",
          format: "d/m/Y H:i",
          renderer: Ext.util.Format.dateRenderer("d/m/Y H:i")
        },
        {
          header: "MONEDA",
          align: "center",
          dataIndex: "nameCurrency",
          width: 90
        },
        {
          header: "TIPO<br>CAMBIO",
          align: "center",
          dataIndex: "typeChange",
          width: 60
        },
        {
          header: "MONTO<br>INICIAL",
          dataIndex: "amountOrigin",
          align: "center",
          xtype: "numbercolumn",
          format: "0,0.00",
          width: 100
        },
        {
          header: "MONTO<br>SUB-EVENTOS",
          dataIndex: "amountSubEvent",
          align: "center",
          xtype: "numbercolumn",
          format: "0,0.00",
          width: 100
        },
        {
          header: "MONTO<br>PÉRDIDA",
          dataIndex: "grossLoss",
          align: "center",
          tdCls: "custom-column",
          xtype: "numbercolumn",
          format: "0,0.00",
          width: 100
        },
        {
          header: "MONTO REC.<br>SEGURO",
          dataIndex: "sureAmountRecovery",
          align: "center",
          xtype: "numbercolumn",
          format: "0,0.00",
          width: 100
        },
        {
          header: "MONTO REC<br>SIN SEGURO",
          dataIndex: "lossRecovery",
          align: "center",
          xtype: "numbercolumn",
          format: "0,0.00",
          width: 100
        },
        {
          header: "GASTOS",
          dataIndex: "totalAmountSpend",
          align: "center",
          xtype: "numbercolumn",
          format: "0,0.00",
          width: 60
        },
        {
          header: "PÉRDIDA<br>NETA",
          dataIndex: "netLoss",
          tdCls: "custom-column",
          align: "center",
          xtype: "numbercolumn",
          format: "0,0.00",
          width: 100
        }
      ];
      this.callParent(arguments);
    }
  }
);
