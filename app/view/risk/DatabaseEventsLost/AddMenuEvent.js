Ext.define('DukeSource.view.risk.DatabaseEventsLost.AddMenuEvent', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.AddMenuEvent',
    width: 180,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: []
        });

        me.callParent(arguments);
    }
});
