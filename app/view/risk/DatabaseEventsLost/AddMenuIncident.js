Ext.define("DukeSource.view.risk.DatabaseEventsLost.AddMenuIncident", {
  extend: "Ext.menu.Menu",
  alias: "widget.AddMenuIncident",
  width: 150,
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: []
    });

    me.callParent(arguments);
  }
});
