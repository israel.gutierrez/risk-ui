Ext.define("ModelTreeWindowFactorRisk", {
  extend: "Ext.data.Model",
  fields: [
    { name: "id", type: "string" },
    { name: "text", type: "string" },
    { name: "idFactorRisk", type: "string" },
    { name: "description", type: "string" },
    { name: "parent", type: "string" },
    { name: "parentId", type: "string" },
    { name: "state", type: "string" }
  ]
});

var StoreTreeWindowFactorRisk = Ext.create("Ext.data.TreeStore", {
  extend: "Ext.data.Store",
  model: "ModelTreeWindowFactorRisk",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListFactorRiskActives.htm",
    extraParams: {
      node: "root",
      depth: 0
    },
    reader: {
      type: "json",
      totalProperty: "totalCount",
      successProperty: "success"
    }
  },
  root: {
    id: "root",
    expanded: true
  },
  folderSort: true,
  sorters: [
    {
      property: "text",
      direction: "ASC"
    }
  ]
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.treepanel.TreePanelFactorRisk",
  {
    extend: "Ext.tree.Panel",
    alias: "widget.TreePanelFactorRisk",
    useArrows: true,
    multiSelect: true,
    mixins: {
      treeFilter: "DukeSource.global.TreeFilter"
    },
    singleExpand: false,
    rootVisible: false,
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        tbar: [
          {
            xtype: "textfield",
            itemId: "searchOption",
            name: "searchOption",
            width: 200,
            emptyText: "Buscar",
            enableKeyEvents: true,
            listeners: {
              afterrender: function(e) {
                e.focus(false, 200);
              },
              keyup: function(e, t) {
                me.expandAll(this);
                me.filterByText(e.getValue());
                if (e.getValue() === "") {
                  me.collapseAll(this);
                }
              }
            }
          }
        ],
        store: StoreTreeWindowFactorRisk
      });
      me.callParent(arguments);
    }
  }
);
