Ext.define("ModelTreeWindowProduct", {
  extend: "Ext.data.Model",
  fields: [
    { name: "id", type: "string" },
    { name: "text", type: "string" },
    { name: "idProduct", type: "string" },
    { name: "description", type: "string" },
    { name: "parent", type: "string" },
    { name: "factor", type: "string" },
    { name: "path", type: "string" },
    { name: "parentId", type: "string" },
    { name: "state", type: "string" }
  ]
});

var StoreTreeWindowProduct = Ext.create("Ext.data.TreeStore", {
  extend: "Ext.data.Store",
  model: "ModelTreeWindowProduct",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListProductActives.htm",
    extraParams: {
      node: "root",
      depth: 0
    },
    reader: {
      type: "json",
      totalProperty: "totalCount",
      successProperty: "success"
    }
  },
  root: {
    id: "root",
    expanded: true
  },
  folderSort: true,
  sorters: [
    {
      property: "text",
      direction: "ASC"
    }
  ]
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.treepanel.TreePanelProduct",
  {
    extend: "Ext.tree.Panel",
    alias: "widget.TreePanelProduct",
    useArrows: true,
    multiSelect: true,
    mixins: {
      treeFilter: "DukeSource.global.TreeFilter"
    },
    singleExpand: false,
    rootVisible: false,
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        tbar: [
          {
            xtype: "textfield",
            itemId: "searchOption",
            name: "searchOption",
            width: 200,
            emptyText: "Buscar",
            enableKeyEvents: true,
            listeners: {
              render: function(e) {
                e.focus(false, 1000);
              },
              keyup: function(e, t) {
                me.expandAll(this);
                me.filterByText(e.getValue());
                if (e.getValue() === "") {
                  me.collapseAll(this);
                }
              }
            }
          }
        ],
        store: StoreTreeWindowProduct
      });
      me.callParent(arguments);
    }
  }
);
