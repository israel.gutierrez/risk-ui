Ext.define("ModelTreeWindowProcess", {
  extend: "Ext.data.Model",
  fields: [
    { name: "id", type: "string" },
    { name: "idProcessType", type: "string" },
    { name: "idProcess", type: "string" },
    { name: "idSubProcess", type: "string" },
    { name: "codeProcess", type: "string" },
    { name: "idActivity", type: "string" },
    { name: "text", type: "string" },
    { name: "description", type: "string" },
    { name: "abbreviation", type: "string" },
    { name: "parent", type: "string" },
    { name: "parentId", type: "string" },
    { name: "state", type: "string" }
  ]
});

var StoreTreeWindowProcess = Ext.create("Ext.data.TreeStore", {
  extend: "Ext.data.Store",
  model: "ModelTreeWindowProcess",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      category === DukeSource.global.GiroConstants.GESTOR
        ? "http://localhost:9000/giro/showListProcessAssigned.htm"
        : "http://localhost:9000/giro/showListProcessTypeActives.htm",
    extraParams: {
      node: "root",
      depth: 0,
      check: false,
      username: userName
    },
    reader: {
      type: "json",
      totalProperty: "totalCount",
      successProperty: "success"
    }
  },
  root: {
    id: "root",
    expanded: true
  },
  folderSort: true,
  sorters: [
    {
      property: "text",
      direction: "ASC"
    }
  ]
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.treepanel.TreePanelProcess",
  {
    extend: "Ext.tree.Panel",
    alias: "widget.TreePanelProcess",
    useArrows: true,
    multiSelect: true,
    mixins: {
      treeFilter: "DukeSource.global.TreeFilter"
    },
    singleExpand: false,
    rootVisible: false,
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        tbar: [
          {
            xtype: "textfield",
            itemId: "searchOption",
            name: "searchOption",
            width: 200,
            emptyText: "Buscar",
            enableKeyEvents: true,
            listeners: {
              afterrender: function(e) {
                e.focus(false, 200);
              },
              specialkey: function(e, t) {
                if (t.getKey() === t.ENTER) {
                  me.expandAll(this);
                  me.filterByText(e.getValue());
                  if (e.getValue() === "") {
                    me.collapseAll(this);
                  }
                }
              }
            }
          }
        ],
        store: StoreTreeWindowProcess
      });
      me.callParent(arguments);
    }
  }
);
