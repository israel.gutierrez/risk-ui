Ext.define("ModelTreeWindowAgency", {
  extend: "Ext.data.Model",
  fields: [
    { name: "id", type: "string" },
    { name: "idCompany", type: "string" },
    { name: "idRegion", type: "string" },
    { name: "idAgency", type: "string" },
    { name: "text", type: "string" },
    { name: "description", type: "string" },
    { name: "parent", type: "string" },
    { name: "parentId", type: "string" },
    { name: "location", type: "string" },
    { name: "state", type: "string" }
  ]
});

var StoreTreeWindowAgency = Ext.create("Ext.data.TreeStore", {
  extend: "Ext.data.Store",
  model: "ModelTreeWindowAgency",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListCompanyActives.htm",
    extraParams: {
      node: "root",
      depth: 0
    },
    reader: {
      type: "json",
      totalProperty: "totalCount",
      successProperty: "success"
    }
  },
  root: {
    id: "root",
    expanded: true
  },
  folderSort: true,
  sorters: [
    {
      property: "text",
      direction: "ASC"
    }
  ]
});

Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.treepanel.TreePanelAgency",
  {
    extend: "Ext.tree.Panel",
    alias: "widget.TreePanelAgency",
    useArrows: true,
    multiSelect: true,
    mixins: {
      treeFilter: "DukeSource.global.TreeFilter"
    },
    singleExpand: false,
    rootVisible: false,
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        tbar: [
          {
            xtype: "textfield",
            itemId: "searchOption",
            name: "searchOption",
            width: 200,
            emptyText: "Buscar",
            enableKeyEvents: true,
            listeners: {
              afterrender: function(e) {
                e.focus(false, 200);
              },
              keyup: function(e, t) {
                me.expandAll(this);
                me.filterByText(e.getValue());
                if (e.getValue() === "") {
                  me.collapseAll(this);
                }
              }
            }
          }
        ],
        store: StoreTreeWindowAgency
      });
      me.callParent(arguments);
    }
  }
);
