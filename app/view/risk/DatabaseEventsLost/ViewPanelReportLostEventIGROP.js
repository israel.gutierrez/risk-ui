Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.ViewPanelReportLostEventIGROP",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelReportLostEventIGROP",
    border: false,
    layout: "fit",

    initComponent: function() {
      var me = this;
      this.items = [
        {
          xtype: "container",
          name: "generalContainer",
          border: false,
          layout: {
            align: "stretch",
            type: "border"
          },
          items: [
            {
              xtype: "panel",
              padding: "1",
              flex: 3,
              name: "panelReport",
              region: "center",
              layout: "fit"
            },
            {
              xtype: "form",
              padding: 2,
              region: "west",
              collapseDirection: "right",
              collapsed: false,
              collapsible: true,
              split: true,
              flex: 1,
              title: "BÚSQUEDA",
              fieldDefaults: {
                labelCls: "changeSizeFontToEightPt",
                fieldCls: "changeSizeFontToEightPt"
              },
              items: [
                {
                  xtype: "container",
                  layout: {
                    type: "anchor"
                  },
                  padding: "5",
                  items: [
                    {
                      xtype: "datefield",
                      fieldLabel: "FECHA REPORTE DE",
                      allowBlank: false,
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      labelWidth: 120,
                      format: "d/m/Y",
                      anchor: "100%",
                      name: "dateInit"
                    },
                    {
                      xtype: "datefield",
                      fieldLabel: "FECHA REPORTE A",
                      allowBlank: false,
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      labelWidth: 120,
                      format: "d/m/Y",
                      anchor: "100%",
                      name: "dateEnd"
                    },
                    {
                      xtype: "ViewComboBusinessLineOne",
                      fieldLabel: "LINEA NEGOCIO 1",
                      listeners: {
                        select: function(cbo) {
                          me.down("ViewComboBusinessLineTwo")
                            .getStore()
                            .load({
                              url:
                                "http://localhost:9000/giro/showListBusinessLineTwoActivesComboBox.htm",
                              params: {
                                valueFind: cbo.getValue()
                              }
                            });
                        }
                      },
                      labelWidth: 120,
                      emptyText: "Seleccionar",
                      forceSelection: false,
                      anchor: "100%",
                      name: "businessLineOne"
                    },
                    {
                      xtype: "ViewComboEventOne",
                      fieldLabel: "EVENTO NIVEL 1",
                      listeners: {
                        select: function(cbo) {
                          me.down("ViewComboEventTwo")
                            .getStore()
                            .load({
                              url:
                                "http://localhost:9000/giro/showListEventTwoActivesComboBox.htm",
                              params: {
                                valueFind: cbo.getValue()
                              }
                            });
                        }
                      },
                      labelWidth: 120,
                      emptyText: "Seleccionar",
                      forceSelection: false,
                      anchor: "100%",
                      name: "eventOne"
                    },
                    {
                      xtype: "ViewComboEventTwo",
                      fieldLabel: "EVENTO NIVEL 2",
                      labelWidth: 120,
                      emptyText: "Seleccionar",
                      forceSelection: false,
                      anchor: "100%",
                      name: "eventTwo"
                    },
                    {
                      xtype: "NumberDecimalNumber",
                      fieldLabel: "PÉRDIDA MAYOR A",
                      labelWidth: 120,
                      emptyText: "0.00",
                      anchor: "100%",
                      name: "amountSearch",
                      itemId: "amountSearch"
                    },
                    {
                      xtype: "container",
                      layout: {
                        align: "stretch",
                        pack: "end",
                        // padding: '10 40 10 10',
                        type: "hbox"
                      },
                      items: [
                        {
                          xtype: "button",
                          scale: "medium",
                          text: "GENERAR",
                          iconCls: "pdf",
                          action: "generateReportEventsLostIGROPPdf"
                        },
                        {
                          xtype: "button",
                          scale: "medium",
                          text: "GENERAR",
                          iconCls: "excel",
                          action: "generateReportEventsLostIGROPXls"
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ];
      this.callParent();
    }
  }
);
