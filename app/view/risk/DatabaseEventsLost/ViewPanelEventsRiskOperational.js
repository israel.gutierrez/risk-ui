Ext.define('ModelPanelEventsRiskOperational', {
    extend: 'Ext.data.Model',
    fields: [
        'idEvent',
        'codeEvent',
        'workArea',
        'isRiskEventIncidents',
        'dateRegister',
        'userRegister',
        'fullNameUserRegister',
        'workAreaUserRegister',
        'businessLineOne',
        'businessLineTwo',
        'businessLineThree',
        'descriptionBusinessLineOne',
        'factorRisk',
        'idEventThree',
        'eventTwo',
        'eventOne',
        'managerRisk',
        'agency',
        'currency',
        'riskType',
        'actionPlan',
        'idProcessType',
        'idProcess',
        'idSubProcess',
        'idActivity',
        'insuranceCompany',
        'descriptionProcess',
        'descriptionSubProcess',
        'eventState',
        'nameEventState',
        'colorEventState',
        'sequenceEventState',
        'eventType',
        'lossType',
        'totalLossRecovery',
        'totalLoss',
        'accountingEntryLoss',
        'accountingEntryRecoveryLoss',
        'descriptionLarge',
        'descriptionAgency',
        'descriptionRegion',
        'descriptionWorkArea',
        'descriptionUnity',
        'descriptionUnity2',
        {
            name: 'dateOccurrence', type: 'date', format: 'd/m/Y H:i:s', convert: function (value) {
                return Ext.Date.parse(value, "d/m/Y H:i:s");
            }
        },
        {
            name: 'dateAcceptLossEvent', type: 'date', format: 'd/m/Y H:i:s', convert: function (value) {
                return Ext.Date.parse(value, "d/m/Y H:i:s");
            }
        },
        {
            name: 'dateCloseEvent', type: 'date', format: 'd/m/Y H:i:s', convert: function (value) {
                return Ext.Date.parse(value, "d/m/Y H:i:s");
            }
        },
        {
            name: 'dateDiscovery', type: 'date', format: 'd/m/Y H:i:s', convert: function (value) {
                return Ext.Date.parse(value, "d/m/Y H:i:s");
            }
        },
        {
            name: 'dateAccountingEvent', type: 'date', format: 'd/m/Y H:i:s', convert: function (value) {
                return Ext.Date.parse(value, "d/m/Y H:i:s");
            }
        },
        'entityPenalizesLoss',
        'grossLoss',
        'lossRecovery',
        'sureAmountRecovery',
        'totalAmountSpend',
        'amountSubEvent',
        'amountProvision',
        'sureAmount',
        'netLoss',
        'typeChange',
        'typeEvent',
        'descriptionEventThree',
        'descriptionEventTwo',
        'descriptionEventOne',
        'nameFactorRisk',
        'descriptionFactorRisk',
        'nameAction',
        'nameCurrency',
        'descriptionShort',
        'idPlanAccountSureAmountRecovery',
        'planAccountSureAmountRecovery',
        'planAccountLossRecovery',
        'idPlanAccountLossRecovery',
        'bookkeepingEntryLossRecovery',
        'bookkeepingEntrySureAmountRecovery',
        'amountOrigin',
        'codeGeneralIncidents',
        'idIncident',
        'cause',
        'fileAttachment',
        'codesRiskAssociated',
        'numberRiskAssociated',
        'indicatorAtRisk',
        'eventMain',
        'numberRelation',
        'numberActionPlan',
        'codesActionPlan',
        'descriptionTittleEvent',
        'costCenter',
        'codeMigration',
        'comments',
        'order',
        'stateTemp',
        'descriptionPlanAccountLoss',
        'codeAccountLoss',
        'codeCorrelative',
        'checkApprove',
        'userApprove',
        'commentApprove',
        'dateApprove',
        'processRecursive',
        'descriptionProcessRecursive',
        'operation',
        'descriptionOperation',
        'recoveryFixedAsset',
        'isCritic',
        'descriptionStateEvent',
        'descriptionCriticEvent',
        'descriptionTypeEvent',
        'product',
        'descriptionProduct',
        'processImpact',
        'subProcessImpact'
    ]
});

var StorePanelEventsRiskOperational = Ext.create('Ext.data.Store', {
    model: 'ModelPanelEventsRiskOperational',
    autoLoad: true,
    proxy: {
        actionMethods: {
            create: 'POST',
            read: 'POST',
            update: 'POST'
        },
        type: 'ajax',
        url: 'http://localhost:9000/giro/getAllEventMaster.htm',
        /* url: 'app/allEventMaster.json', */
        extraParams: {
            eventState: category === DukeSource.global.GiroConstants.ANALYST ? 1 : userRole === DukeSource.global.GiroConstants.ACCOUNTANT ? 3 : 1,
            start:0,limit:25
        },
        reader: {
            totalProperty: 'totalCount',
            root: 'data',
            successProperty: 'success'
        }
    }
});
Ext.define('DukeSource.view.risk.DatabaseEventsLost.ViewPanelEventsRiskOperational', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelEventsRiskOperational',
    requires: ['DukeSource.view.risk.util.search.AdvancedSearchEvent'],
    layout: 'fit',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'panel',
                    titleAlign: 'center',
                    border: false,
                    layout: {
                        type: 'fit'
                    },
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'top',
                            items: [
                                {
                                    xtype: 'button',
                                    iconCls: 'add',
                                    text: 'Nuevo',
                                    scale: 'medium',
                                    cls: 'my-btn',
                                    hidden: (category === DukeSource.global.GiroConstants.ANALYST && codexCompany === 'es_cl_0001'),
                                    overCls: 'my-over',
                                    handler: function () {
                                        var win;
                                        if (codexCompany === 'es_cl_0001') {
                                            win = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.WindowRegisterEventWizard', {
                                                new: true
                                            });
                                            win.show();
                                            win.down('#currency').getStore().load({
                                                callback: function () {
                                                    var typeChange = win.down('#typeChange');
                                                    win.down('#currency').setValue('1');
                                                    typeChange.setValue('1.00');
                                                    typeChange.setFieldStyle('background: #D8D8D8');
                                                    typeChange.setReadOnly(true);
                                                }
                                            })
                                        } else {
                                            win = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterEvents', {
                                                width: 800,
                                                actionType: 'new',
                                                modal: true,
                                                gridParent: me.down('#gridEvents'),
                                                indexRecordParent: -1
                                            }).show();
                                            win.down('#descriptionShort').focus(false, 400);
                                            win.down('#eventMain').setValue('S');
                                            win.down('#numberRelation').setValue(0);

                                            win.timer = setInterval(function () {
                                                win.down('#dateRegisterTemp').setValue(new Date());
                                            }, 1000);

                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    scale: 'medium',
                                    cls: 'my-btn',
                                    overCls: 'my-over',
                                    iconCls: 'detail',
                                    action: 'accountingEvent',
                                    hidden: userRole !== DukeSource.global.GiroConstants.ACCOUNTANT,
                                    text: 'Contabilizar',
                                    handler: function () {
                                        var record = me.down('grid').getSelectionModel().getSelection()[0];
                                        if (record === undefined) {
                                            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
                                        } else if (record.get('sequenceEventState') < 3) {
                                            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.EVENT_NOT_APPROVED);
                                        } else {
                                            var win = Ext.create('DukeSource.view.risk.parameter.windows.WindowAccountingEvent', {
                                                modal: true
                                            });
                                            win.setTitle(win.title + ' - ' + record.get('descriptionShort'));
                                            win.down('form').getForm().loadRecord(record);
                                            win.show();
                                        }
                                    }
                                }, '-',
                                {
                                    xtype: 'combobox',
                                    width: 300,
                                    name: 'eventState',
                                    itemId: 'eventState',
                                    fieldLabel: 'Estado',
                                    hidden: codexCompany !== 'es_cl_0001',
                                    labelWidth: 80,
                                    displayField: 'description',
                                    valueField: 'id',
                                    store: {
                                        autoLoad: true,
                                        fields: ['id', 'description', 'sequence'],
                                        proxy: {
                                            actionMethods: {
                                                create: 'POST',
                                                read: 'POST',
                                                update: 'POST'
                                            },
                                            type: 'ajax',
                                            url: 'http://localhost:9000/giro/findStateIncident.htm',
                                            extraParams: {
                                                propertyFind: 'si.typeIncident',
                                                valueFind: DukeSource.global.GiroConstants.EVENT,
                                                propertyOrder: 'si.sequence'
                                            },
                                            reader: {
                                                type: 'json',
                                                rootProperty: 'data',
                                                successProperty: 'success'
                                            }
                                        }
                                    },
                                    listeners: {
                                        select: function (cbo) {
                                            var grid = me.down('grid');
                                            grid.store.getProxy().extraParams = {
                                                eventState: cbo.findRecordByValue(cbo.getValue()).get('sequence')
                                            };
                                            grid.store.getProxy().url = 'http://localhost:9000/giro/getAllEventMaster.htm';
                                            grid.down('pagingtoolbar').moveFirst();
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    scale: 'medium',
                                    cls: 'my-btn',
                                    overCls: 'my-over',
                                    iconCls: 'sub_event',
                                    action: 'addSubEvents',
                                    text: 'Sub eventos',
                                    hidden: hidden('VPER_DBCLK_SubEvents')
                                },
                                {
                                    xtype: 'button',
                                    scale: 'medium',
                                    cls: 'my-btn',
                                    overCls: 'my-over',
                                    iconCls: 'risk',
                                    action: 'relationToRisk',
                                    text: 'Riesgos',
                                    hidden: category === DukeSource.global.GiroConstants.GESTOR || codexCompany === 'es_cl_0001'
                                },
                                {
                                    xtype: "checkbox",
                                    fieldLabel: "Aprobados",
                                    labelWidth: 80,
                                    name: "state",
                                    itemId: 'state',
                                    inputValue: "S",
                                    uncheckedValue: "N",
                                    hidden: hidden('EVN_WRE_CHK_Approve'),
                                    checked: true,
                                    listeners: {
                                        change: function (c) {
                                            var grid = me.down('grid');
                                            var value;
                                            if (c.checked === true) {
                                                c.setFieldLabel("Aprobados");
                                                value = 'S';

                                            } else {
                                                c.setFieldLabel("Sin Aprobar");
                                                value = 'N';
                                            }
                                            grid.store.proxy.url = 'http://localhost:9000/giro/advancedSearchEvent.htm';
                                            grid.store.proxy.extraParams = {
                                                fields: 'ev.checkApprove',
                                                values: value,
                                                types: 'String',
                                                operators: 'equal',
                                                searchIn: 'Event'
                                            };
                                            grid.store.load();
                                        }
                                    }
                                },
                                '->',
                                {
                                    xtype: 'button',
                                    scale: 'medium',
                                    cls: 'my-btn',
                                    overCls: 'my-over',
                                    iconCls: 'auditory',
                                    text: 'Auditoría',
                                    handler: function () {
                                        var grid = me.down('grid');
                                        DukeSource.global.DirtyView.showAuditory(grid, 'http://localhost:9000/giro/findAuditEventMaster.htm');
                                    }
                                }
                            ]
                        }
                    ],
                    items: [
                        {
                            xtype: 'container',
                            border: false,
                            layout: {
                                type: 'border'
                            },
                            items: [
                                {
                                    xtype: 'gridpanel',
                                    name: 'gridEvents',
                                    itemId: 'gridEvents',
                                    store: StorePanelEventsRiskOperational,
                                    loadMask: true,
                                    columnLines: true,
                                    border: false,
                                    style: {
                                        borderLeft: '1px solid #99bce8'
                                    },
                                    enableLocking: true,
                                    region: 'center',
                                    columns: [
                                        {
                                            header: 'ID',
                                            align: 'left',
                                            dataIndex: 'idEvent',
                                            hidden: hidden('EVN_VPE_GRID_IdEvent'),
                                            locked: true,
                                            width: 50
                                        },
                                        {
                                            header: '',
                                            align: 'left',
                                            dataIndex: 'fileAttachment',
                                            locked: true,
                                            hidden: codexCompany !== 'es_cl_0001',
                                            width: 35,
                                            renderer: function (value, metaData, record) {
                                                var reject = '';
                                                if (record.get('stateTemp') === 'R') {
                                                    reject = '<i class="tesla even-times-circle-o"></i>';
                                                }
                                                return reject;
                                            }
                                        },
                                        {
                                            header: 'Código',
                                            align: 'left',
                                            dataIndex: 'codeEvent',
                                            locked: true,
                                            width: 150,
                                            renderer: function (value, metaData, record) {
                                                var state;
                                                var type = '';
                                                metaData.tdAttr = 'style="background-color: #' + record.get('colorEventState') + ' !important; height:40px;"';
                                                state = '<br><span style="font-size:7pt">' + record.get('nameEventState') + '</span>';
                                                if ((record.get('eventType') == '2')) {
                                                    type = '<i class="tesla even-stack2" style="margin-left:18px;line-height: 0.2 !important;"></i>';
                                                } else if ((record.get('eventState') === 'X')) {
                                                    metaData.tdAttr = 'style="background-color: #cccccc !important; height:40px;"';
                                                    type = '<i class="tesla even-blocked-24" style="margin-left:18px;line-height: 0.2 !important;"></i>';
                                                }

                                                if (record.get('fileAttachment') === 'S') {
                                                    return '<div style="display:inline-block;height:20px;width:20px;padding:2px;"><i class="tesla even-attachment"></i></div><div style="display:inline-block;position:absolute;">' +
                                                        '<span style="font-size: 12px;font-weight: bold">' + value + type + '</span> ' + state + '</div>';
                                                } else {
                                                    return '<div style="display:inline-block;height:20px;width:20px;padding:2px;"></div><div style="display:inline-block;position:absolute;">' +
                                                        '<span style="font-size: 12px;font-weight: bold">' + value + type + '</span> ' + state + '</div>';
                                                }
                                            }
                                        },
                                        {
                                            header: 'Descripción corta',
                                            align: 'left',
                                            dataIndex: 'descriptionShort',
                                            hidden: hidden('EVN_WER_TXF_DescriptionShort'),
                                            width: 300
                                        },
                                        {
                                            header: getName('EVN_WER_TXF_DescriptionLarge'),
                                            align: 'left',
                                            dataIndex: 'descriptionLarge',
                                            width: 300
                                        },

                                        {
                                            header: 'Fecha de reporte',
                                            align: 'center',
                                            dataIndex: 'dateAcceptLossEvent',
                                            xtype: 'datecolumn',
                                            format: 'd/m/Y H:i',
                                            width: 90,
                                            renderer: function (a) {
                                                return '<span style="color:green;">' + Ext.util.Format.date(a, 'd/m/Y H:i') + "</span>"
                                            }
                                        },
                                        {
                                            header: 'Fecha de ocurrencia',
                                            align: 'center',
                                            dataIndex: 'dateOccurrence',
                                            xtype: 'datecolumn',
                                            format: 'd/m/Y H:i',
                                            width: 90,
                                            renderer: Ext.util.Format.dateRenderer('d/m/Y H:i')
                                        }
                                    ],
                                    viewConfig: {
                                        plugins: {
                                            ptype: 'gridviewdragdrop'
                                        },
                                        listeners: {
                                            itemdblclick: function (grid, record, item, index, e) {
                                                var app = DukeSource.app;
                                                app.getController('DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelEventsRiskOperational')._onLoadEventsRisk(grid, record, index);
                                            }
                                        }
                                    },
                                    bbar: {
                                        xtype: 'pagingtoolbar',
                                        pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                                        store: StorePanelEventsRiskOperational,
                                        displayInfo: true,
                                        displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                                        emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
                                        doRefresh: function () {
                                            var grid = this.up('grid');
                                            var index = DukeSource.global.DirtyView.getSelectedRowIndex(grid);

                                            grid.store.load({
                                                callback: function () {
                                                    if (index !== -1) {
                                                        grid.getSelectionModel().select(index);
                                                    }
                                                }
                                            });
                                        }
                                    },
                                    listeners: {
                                        render: function () {
                                            var me = this;
                                            if (category === DukeSource.global.GiroConstants.ANALYST) {
                                                var columnAmount = Ext.create('Ext.grid.column.Column', {
                                                    header: 'Montos',
                                                    align: 'center',
                                                    columns: [
                                                        {
                                                            header: 'Moneda',
                                                            align: 'center',
                                                            dataIndex: 'nameCurrency',
                                                            width: 90
                                                        },
                                                        {
                                                            header: 'Tipo cambio',
                                                            align: 'center',
                                                            dataIndex: 'typeChange',
                                                            width: 60
                                                        },
                                                        {
                                                            header: 'Monto inicial',
                                                            dataIndex: 'amountOrigin',
                                                            align: 'center',
                                                            xtype: 'numbercolumn',
                                                            format: '0,0.00',
                                                            width: 100
                                                        },
                                                        {
                                                            header: 'Monto sub-eventos',
                                                            dataIndex: 'amountSubEvent',
                                                            align: 'center',
                                                            xtype: 'numbercolumn',
                                                            format: '0,0.00',
                                                            width: 100
                                                        },
                                                        {
                                                            header: getName('EVN_CPE_RCLK_Spends'),
                                                            dataIndex: 'totalAmountSpend',
                                                            align: 'center',
                                                            xtype: 'numbercolumn',
                                                            format: '0,0.00',
                                                            width: 60
                                                        },
                                                        {
                                                            header: 'Provisión',
                                                            dataIndex: 'amountProvision',
                                                            align: 'center',
                                                            hidden: hidden('WinGridProvisionHead'),
                                                            xtype: 'numbercolumn',
                                                            format: '0,0.00',
                                                            width: 80
                                                        },
                                                        {
                                                            header: 'Monto total pérdida',
                                                            dataIndex: 'totalLoss',
                                                            align: 'center',
                                                            tdCls: 'custom-column',
                                                            xtype: 'numbercolumn',
                                                            format: '0,0.00',
                                                            width: 100
                                                        },
                                                        {
                                                            header: 'Monto total recupero',
                                                            dataIndex: 'totalLossRecovery',
                                                            align: 'center',
                                                            xtype: 'numbercolumn',
                                                            format: '0,0.00',
                                                            width: 100
                                                        },
                                                        {
                                                            header: 'Pérdida neta',
                                                            dataIndex: 'netLoss',
                                                            tdCls: 'custom-column',
                                                            align: 'center',
                                                            xtype: 'numbercolumn',
                                                            format: '0,0.00',
                                                            width: 100
                                                        }
                                                    ]
                                                });
                                                var columnDateDiscovery = Ext.create('Ext.grid.column.Column', {
                                                    text: 'Fecha de descubrimiento',
                                                    align: 'center',
                                                    dataIndex: 'dateDiscovery',
                                                    xtype: 'datecolumn',
                                                    format: 'd/m/Y H:i',
                                                    width: 97,
                                                    renderer: Ext.util.Format.dateRenderer('d/m/Y H:i')
                                                });
                                                var columnDateAccounting = Ext.create('Ext.grid.column.Column', {
                                                    text: 'Fecha de contabilización',
                                                    align: 'center',
                                                    dataIndex: 'dateAccountingEvent',
                                                    xtype: 'datecolumn',
                                                    format: 'd/m/Y H:i',
                                                    width: 97,
                                                    renderer: Ext.util.Format.dateRenderer('d/m/Y H:i')
                                                });
                                                var columnDateClose = Ext.create('Ext.grid.column.Column', {
                                                    text: 'Fecha de cierre',
                                                    dataIndex: 'dateCloseEvent',
                                                    width: 90,
                                                    align: 'center',
                                                    xtype: 'datecolumn',
                                                    format: 'd/m/Y H:i',
                                                    renderer: Ext.util.Format.dateRenderer('d/m/Y H:i')
                                                });
                                                var columnFactorRisk = Ext.create('Ext.grid.column.Column', {
                                                    header: 'Factor de riesgo',
                                                    dataIndex: 'descriptionFactorRisk',
                                                    width: 200,
                                                    tdCls: 'process-columnFree',
                                                    renderer: function (value) {
                                                        var s = value.split(' &#8702; ');
                                                        var path = "";
                                                        for (var i = 0; i < s.length; i++) {
                                                            var text = s[i];
                                                            text = s[i] + '<br>';
                                                            path = path + ' &#8702; ' + text;
                                                        }
                                                        return path;
                                                    }
                                                });
                                                var columnRisk = Ext.create('Ext.grid.column.Column', {
                                                    header: 'Riesgos',
                                                    align: 'center',
                                                    columns: [
                                                        {
                                                            header: 'Cantidad',
                                                            align: 'center',
                                                            dataIndex: 'numberRiskAssociated',
                                                            width: 70
                                                        },
                                                        {
                                                            header: 'Riesgos asociados',
                                                            dataIndex: 'codesRiskAssociated',
                                                            width: 150,
                                                            renderer: function (a) {
                                                                return '<span style="color:red;">' + a + "</span>"
                                                            }
                                                        }
                                                        // ,
                                                        // {
                                                        //     xtype: 'actioncolumn',
                                                        //     align: 'center',
                                                        //     width: 45,
                                                        //     items: [
                                                        //         {
                                                        //             icon: 'images/ir.png',
                                                        //             handler: function (grid, rowIndex) {
                                                        //                 var rowEvent = grid.store.getAt(rowIndex);
                                                        //                 if (Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0] == undefined) {
                                                        //                     DukeSource.global.DirtyView.verifyLoadController('DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelIdentifyRiskOperational');
                                                        //                     var k = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelIdentifyRiskOperational', {
                                                        //                         title: 'EVRO',
                                                        //                         origin: 'events',
                                                        //                         closable: true,
                                                        //                         border: false
                                                        //                     });
                                                        //                     DukeSource.getApplication().centerPanel.addPanel(k);
                                                        //                     locateRisk(k, rowEvent);
                                                        //                 } else {
                                                        //                     var k1 = Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0];
                                                        //                     DukeSource.getApplication().centerPanel.addPanel(k1);
                                                        //                     locateRisk(k1, rowEvent);
                                                        //                 }
                                                        //             }
                                                        //         }
                                                        //     ]
                                                        // }
                                                    ]
                                                });

                                                var columnEvent2 = Ext.create('Ext.grid.column.Column', {
                                                    text: getName('EWCBEventTwo'),
                                                    dataIndex: 'descriptionEventOne',
                                                    width: 270,
                                                    tdCls: 'process-columnFree',
                                                    renderer: function (value) {
                                                        var s = value.split(' &#8702; ');
                                                        var path = "";
                                                        for (var i = 0; i < s.length; i++) {
                                                            var text = s[i];
                                                            text = s[i] + '<br>';
                                                            path = path + ' &#8702; ' + text;
                                                        }
                                                        return path;
                                                    }
                                                });

                                                var columnActionPlan = Ext.create('Ext.grid.column.Column', {
                                                    header: 'Planes de acción', columns: [
                                                        {
                                                            header: 'Nro',
                                                            align: 'center',
                                                            dataIndex: 'numberActionPlan',
                                                            width: 50
                                                        },
                                                        {
                                                            header: 'Planes',
                                                            align: 'center',
                                                            dataIndex: 'codesActionPlan',
                                                            width: 120,
                                                            renderer: function (a) {
                                                                return '<span style="color:red;">' + a + "</span>"
                                                            }
                                                        }
                                                    ]
                                                });
                                                var columnGeneralIncident = Ext.create('Ext.grid.column.Column', {
                                                    text: 'Código incidente',
                                                    dataIndex: 'idIncident',
                                                    width: 70,
                                                    align: 'center'
                                                });
                                                me.normalGrid.headerCt.insert(2, columnAmount);
                                                me.normalGrid.headerCt.insert(3, columnRisk);
                                                me.normalGrid.headerCt.insert(6, columnDateDiscovery);
                                                me.normalGrid.headerCt.insert(7, columnDateAccounting);
                                                me.normalGrid.headerCt.insert(8, columnDateClose);
                                                me.normalGrid.headerCt.insert(9, columnFactorRisk);
                                                me.normalGrid.headerCt.insert(10, columnEvent2);
                                                // me.normalGrid.headerCt.insert(12, columnActionPlan);
                                                // me.normalGrid.headerCt.insert(13, columnGeneralIncident);
                                                me.getView().refresh();
                                            } else {
                                                var columnSucursal = Ext.create('Ext.grid.column.Column', {
                                                    text: 'Sucursal',
                                                    dataIndex: 'descriptionAgency',
                                                    width: 150,
                                                    tdCls: 'process-columnFree',
                                                    renderer: function (value) {
                                                        var s = value.split(' &#8702; ');
                                                        var path = "";
                                                        for (var i = 0; i < s.length; i++) {
                                                            if (s[i]) {
                                                                var text = s[i];
                                                                text = s[i] + '<br>';
                                                                path = path + ' &#8702; ' + text;
                                                            }
                                                        }
                                                        return path;
                                                    }
                                                });
                                                var columnUnity = Ext.create('Ext.grid.column.Column', {
                                                    text: 'Unidad',
                                                    dataIndex: 'descriptionWorkArea',
                                                    width: 230,
                                                    tdCls: 'process-columnFree',
                                                    renderer: function (value) {
                                                        var s = value.split(' &#8702; ');
                                                        var path = "";
                                                        for (var i = 0; i < s.length; i++) {
                                                            var text = s[i];
                                                            text = s[i] + '<br>';
                                                            path = path + ' &#8702; ' + text;
                                                        }
                                                        return path;
                                                    }
                                                });
                                                var columnAmounts = Ext.create('Ext.grid.column.Column', {
                                                    header: 'MONTOS',
                                                    align: 'center',
                                                    columns: [
                                                        {
                                                            header: 'Monto total de pérdida',
                                                            dataIndex: 'totalLoss',
                                                            align: 'center',
                                                            tdCls: 'custom-column',
                                                            xtype: 'numbercolumn',
                                                            format: '0,0.00',
                                                            width: 100
                                                        },
                                                        {
                                                            header: 'Monto total de recupero',
                                                            dataIndex: 'totalLossRecovery',
                                                            align: 'center',
                                                            xtype: 'numbercolumn',
                                                            format: '0,0.00',
                                                            width: 100
                                                        },
                                                        {
                                                            header: 'Pérdida neta',
                                                            dataIndex: 'netLoss',
                                                            tdCls: 'custom-column',
                                                            align: 'center',
                                                            xtype: 'numbercolumn',
                                                            format: '0,0.00',
                                                            width: 100
                                                        }
                                                    ]
                                                });
                                                var columnActionPlans = Ext.create('Ext.grid.column.Column', {
                                                    header: 'Planes de acción', columns: [
                                                        {
                                                            header: 'Nro',
                                                            align: 'center',
                                                            dataIndex: 'numberActionPlan',
                                                            width: 50
                                                        },
                                                        {
                                                            header: 'Planes',
                                                            align: 'center',
                                                            dataIndex: 'codesActionPlan',
                                                            width: 120,
                                                            renderer: function (a) {
                                                                return '<span style="color:red;">' + a + "</span>"
                                                            }
                                                        }
                                                    ]
                                                });
                                                me.normalGrid.headerCt.insert(2, columnSucursal);
                                                me.normalGrid.headerCt.insert(3, columnUnity);
                                                me.normalGrid.headerCt.insert(4, columnAmounts);
                                                me.normalGrid.headerCt.insert(7, columnActionPlans);
                                                me.getView().refresh();
                                            }
                                        }
                                    }

                                },
                                {
                                    xtype: 'AdvancedSearchEvent',
                                    padding: 2,
                                    region: 'west',
                                    collapseDirection: 'left',
                                    collapsed: true,
                                    collapsible: true,
                                    split: true,
                                    flex: 0.35,
                                    title: 'Búsqueda avanzada',
                                    tbar: [
                                        {
                                            xtype: 'button',
                                            scale: 'medium',
                                            text: 'Buscar',
                                            iconCls: 'search',
                                            action: 'searchAdvancedEvent',
                                            margin: '0 5 0 0'
                                        },
                                        {
                                            xtype: 'button',
                                            scale: 'medium',
                                            text: 'Limpiar',
                                            iconCls: 'clear',
                                            margin: '0 5 0 0',
                                            handler: function () {
                                                me.down('AdvancedSearchEvent').down('form').getForm().reset();
                                            }
                                        },
                                        {
                                            xtype: 'button',
                                            scale: 'medium',
                                            text: 'Exportar',
                                            hidden: hidden('EVN_VPE_BTN_ExportExcel'),
                                            iconCls: 'excel',
                                            action: 'exportSearchAdvancedEventXls',
                                            margin: '0 5 0 0'
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    listeners: {
                        render: function () {
                            DukeSource.lib.Ajax.request({
                                method: 'POST',
                                url: 'http://localhost:9000/giro/findLevelAuthorizedEvent.htm',
                                params: {
                                    propertyFind: 'u.username',
                                    valueFind: userName,
                                    propertyFindTwo: 'ua.type',
                                    valueFindTwo: 'U'
                                },
                                success: function (response) {
                                    response = Ext.decode(response.responseText);
                                    if (response.success) {
                                        LEVEL_EVENT = response.data === undefined ? 1 : response.data.sequence;
                                    } else {
                                        DukeSource.global.DirtyView.messageAlert(response.message);
                                    }
                                },
                                failure: function (response) {
                                    DukeSource.global.DirtyView.messageAlert(response.message);
                                }
                            });
                        }
                    }
                }
            ]
        });
        me.callParent(arguments);
    }

});

function locateRisk(k, rowRisk) {
    DukeSource.getApplication().centerPanel.addPanel(k);
    var fields = 'ev.idEvent';
    var values = rowRisk.get('idEvent');
    var types = 'Long';
    var operator = 'equal';
    var gridMasterPlan = k.down('grid');
    gridMasterPlan.store.getProxy().extraParams = {
        fields: fields,
        values: values,
        types: types,
        operators: operator,
        search: 'EventRisk',
        isLast: 'true'
    };
    gridMasterPlan.store.getProxy().url = 'http://localhost:9000/giro/findMatchRisk.htm';
    gridMasterPlan.down('pagingtoolbar').moveFirst();
}