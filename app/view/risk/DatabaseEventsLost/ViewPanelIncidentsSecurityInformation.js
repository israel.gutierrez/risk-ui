Ext.define(
  "DukeSource.view.risk.DatabaseEventsLost.ViewPanelIncidentsSecurityInformation",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelIncidentsSecurityInformation",
    layout: "fit",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "panel",
            title: "GESTI&Oacute;N DE INCIDENTES",
            titleAlign: "center",
            bodyStyle: {
              background: "#D4E3F4"
            },
            border: false,
            layout: {
              type: "vbox",
              align: "stretch"
            },
            dockedItems: [
              {
                xtype: "toolbar",
                dock: "top",
                height: 40,
                items: [
                  {
                    xtype: "button",
                    itemId: "buttonModify",
                    iconCls: "modify",
                    scale: "medium",
                    cls: "my-btn",
                    overCls: "my-over",
                    hidden: true,
                    text: "EVALUAR INCIDENTE",
                    handler: function() {
                      if (
                        me
                          .down("ViewGridIncidentsPendingRiskOperational")
                          .getSelectionModel()
                          .getSelection()[0] == undefined
                      ) {
                        Ext.Msg.show({
                          title: DukeSource.global.GiroMessages.TITLE_WARNING,
                          msg: DukeSource.global.GiroMessages.MESSAGE_ITEM,
                          buttons: Ext.Msg.OK,
                          icon: Ext.Msg.WARNING,
                          fn: function() {
                            me.down("#panelView")
                              .getActiveTab()
                              .getView()
                              .features[0].expandAll();
                          }
                        });
                      } else {
                        DukeSource.getApplication()
                          .getController(
                            "DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelIncidentsSecurityInformation"
                          )
                          ._showModifyIncidentSecurityInformation(
                            me.down("ViewGridIncidentsPendingRiskOperational"),
                            me
                              .down("ViewGridIncidentsPendingRiskOperational")
                              .getSelectionModel()
                              .getSelection()[0]
                          );
                      }
                    }
                  },
                  {
                    xtype: "button",
                    cls: "my-btn",
                    overCls: "my-over",
                    itemId: "btnAddIncident",
                    iconCls: "add",
                    scale: "medium",
                    hidden: true,
                    text: "AGREGAR INCIDENTE",
                    handler: function() {
                      if (
                        Ext.ComponentQuery.query(
                          "ViewWindowRegisterIncidentsSI"
                        )[0] == undefined
                      ) {
                        var c = Ext.create(
                          "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterIncidentsSI",
                          {}
                        );
                        c.show();
                        var b = c
                          .down("form")
                          .down("UpperCaseTextField[name=descriptionShort]");
                        b.focus(false, 100);
                      }
                    }
                  },
                  "-",
                  {
                    xtype: "datefield",
                    format: "d/m/Y",
                    itemId: "dateInitial",
                    width: 210,
                    fieldLabel: "FECHA REPORTE",
                    listeners: {
                      select: function(c, b) {
                        me.down("datefield[itemId=dateFinal]").setMinValue(b);
                      },
                      expand: function(c) {
                        c.setMaxValue(new Date());
                      }
                    }
                  },
                  {
                    xtype: "datefield",
                    format: "d/m/Y",
                    itemId: "dateFinal",
                    width: 110,
                    labelSeparator: "",
                    fieldLabel: "-",
                    labelWidth: 10,
                    listeners: {
                      select: function(c, b) {
                        me.down("datefield[itemId=dateInitial]").setMaxValue(b);
                        c.setMaxValue(new Date());
                      },
                      expand: function(c) {
                        c.setMaxValue(new Date());
                      }
                    }
                  },
                  {
                    xtype: "button",
                    iconCls: "search",
                    scale: "medium",
                    cls: "my-btn",
                    overCls: "my-over",
                    text: "BUSCAR",
                    handler: function() {
                      var c = me
                        .down("datefield[itemId=dateInitial]")
                        .getRawValue();
                      var b = me
                        .down("datefield[itemId=dateFinal]")
                        .getRawValue();
                      var a = me.down("tabpanel").getActiveTab();
                      if (c == "" || b == "") {
                        DukeSource.global.DirtyView.messageAlert(
                          DukeSource.global.GiroMessages.TITLE_WARNING,
                          "Ingrese la FECHA INICIO y FECHA FIN",
                          Ext.Msg.WARNING
                        );
                      } else {
                        if (
                          category == DukeSource.global.GiroConstants.ANALYST ||
                          category == DukeSource.global.GiroConstants.SECURITY_ANALYST
                        ) {
                          if (a.xtype == "panel") {
                            var grid = a.down("grid");
                            grid.store.getProxy().extraParams = {
                              stateIncident: "I",
                              dateInit: c,
                              dateEnd: b
                            };
                            grid.store.getProxy().url =
                              "http://localhost:9000/giro/getIncidentsByDate.htm";
                            grid.down("pagingtoolbar").moveFirst();
                          } else {
                            a.store.getProxy().extraParams = {
                              stateIncident:
                                a.xtype ==
                                "ViewGridIncidentsGeneralRiskOperational"
                                  ? "P"
                                  : a.xtype ==
                                    "ViewGridIncidentsPendingRiskOperational"
                                  ? "R"
                                  : "X",
                              dateInit: c,
                              dateEnd: b
                            };
                            if (
                              a.xtype ==
                              "ViewGridIncidentsRefusedRiskOperational"
                            )
                              a.store.getProxy().url =
                                "http://localhost:9000/giro/getIncidentsByDate.htm";
                            else
                              a.store.getProxy().url =
                                "http://localhost:9000/giro/getDetailIncidentsByDate.htm";
                            a.down("pagingtoolbar").moveFirst();
                          }
                        }
                        if (category == DukeSource.global.GiroConstants.GESTOR) {
                          a.store.getProxy().extraParams = {
                            stateIncident:
                              a.xtype ==
                              "ViewGridIncidentsRevisedRiskOperational"
                                ? "R"
                                : a.xtype ==
                                  "ViewGridIncidentsGestorRiskOperational"
                                ? "A"
                                : "P",
                            dateInit: c,
                            dateEnd: b
                          };
                          a.store.getProxy().url =
                            "http://localhost:9000/giro/getDetailIncidentsByDate.htm";
                          a.down("pagingtoolbar").moveFirst();
                        }
                        if (category == DukeSource.global.GiroConstants.COLLABORATOR) {
                          a.store.getProxy().extraParams = {
                            stateIncident:
                              a.xtype ==
                              "ViewGridIncidentsRevisedRiskOperational"
                                ? "R"
                                : "P",
                            dateInit: c,
                            dateEnd: b
                          };
                          a.store.getProxy().url =
                            "http://localhost:9000/giro/getDetailIncidentsByDate.htm";
                          a.down("pagingtoolbar").moveFirst();
                        }
                      }
                    }
                  }
                ]
              }
            ],
            items: [
              {
                xtype: "container",
                height: 5
              },
              {
                xtype: "tabpanel",
                itemId: "panelView",
                flex: 1,
                plain: true,
                border: false,
                items: [],
                listeners: {
                  render: function() {
                    me.down("tabpanel").add(
                      Ext.create(
                        "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridIncidentsPendingRiskOperational",
                        { title: "INCIDENTES PENDIENTES" }
                      )
                    );
                    me.down("tabpanel").add({
                      xtype: "panel",
                      title: "INCIDENTES REVISADOS",
                      flex: 1,
                      layout: {
                        type: "border"
                      },
                      border: false,
                      items: [
                        {
                          xtype: Ext.create(
                            "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridIncidentsRevisedSecurityInformation",
                            {
                              loadMask: true,
                              columnLines: true,
                              border: false,
                              style: {
                                borderLeft: "1px solid #99bce8"
                              },
                              region: "center"
                            }
                          )
                        },
                        {
                          xtype: Ext.create(
                            "DukeSource.view.risk.util.search.AdvancedSearchIncident",
                            {
                              region: "west",
                              flex: 0.35,
                              collapsed: true,
                              padding: 2,
                              tbar: [
                                {
                                  xtype: "button",
                                  scale: "medium",
                                  text: "BUSCAR",
                                  iconCls: "search",
                                  action: "searchAdvancedSecurityIncident",
                                  margin: "0 5 0 0"
                                },
                                {
                                  xtype: "button",
                                  scale: "medium",
                                  text: "LIMPIAR",
                                  iconCls: "clear",
                                  margin: "0 5 0 0",
                                  handler: function(btn) {
                                    btn
                                      .up("form")
                                      .getForm()
                                      .reset();
                                  }
                                }
                              ]
                            }
                          )
                        }
                      ]
                    });
                    me.down("tabpanel").add(
                      Ext.create(
                        "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridIncidentsRefusedRiskOperational",
                        {
                          title: "INVALIDOS",
                          region: "center"
                        }
                      )
                    );
                    Ext.create(
                      "DukeSource.view.risk.DatabaseEventsLost.grids.ViewGridHistoricalIncident"
                    );
                    me.down("tabpanel").setActiveTab(0);
                  },
                  tabchange: function(tabPanel, newCard, oldCard) {
                    if (
                      newCard.xtype == "ViewGridIncidentsPendingRiskOperational"
                    ) {
                      me.down("#btnAddIncident").setVisible(false);
                      me.down("#buttonModify").setVisible(true);
                    } else if (newCard.xtype == "panel") {
                      me.down("#btnAddIncident").setVisible(true);
                      me.down("#buttonModify").setVisible(false);
                    } else {
                      me.down("#btnAddIncident").setVisible(false);
                      me.down("#buttonModify").setVisible(false);
                    }
                  }
                }
              }
            ]
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
