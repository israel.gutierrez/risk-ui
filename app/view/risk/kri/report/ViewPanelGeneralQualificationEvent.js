Ext.define("GeneralQualificationEventOne", {
  extend: "Ext.data.Model",
  fields: [
    "description",
    "score",
    "rangeSup1",
    "rangeSup2",
    "rangeSup3",
    "rangeSup4",
    "rangeSup5",
    "rangeSup6",
    "rangeSup7",
    "rangeSup8",
    "rangeSup9",
    "rangeSup10"
  ]
});

var store = Ext.create("Ext.data.Store", {
  model: "GeneralQualificationEventOne",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("ModelGridPanelGeneralQualificationEventOne", {
  extend: "Ext.data.Model",
  fields: [
    "idEvent",
    "score",
    "evaluationResult",
    "idQualificationKri",
    "levelColour",
    "descriptionQualificationEventOne",
    "year",
    "month",
    "descriptionMonth"
  ]
});

var StoreGridPanelGeneralQualificationEventOne = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelGeneralQualificationEventOne",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.risk.kri.report.ViewPanelGeneralQualificationEvent",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelGeneralQualificationEvent",
    border: false,
    layout: "border",

    initComponent: function() {
      var me = this;

      this.items = [
        {
          xtype: "form",
          anchor: "100%",
          region: "north",
          margin: "2 2 0 2",
          bodyPadding: "5",

          layout: {
            type: "hbox"
          },
          items: [
            //
            {
              xtype: "textfield",
              hidden: true,
              name: "idEventOne"
            },

            {
              xtype: "ViewComboYearKri",
              allowBlank: false,
              //                        readOnly:true,
              //                        editable:false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              padding: "0 10 0 80",
              labelWidth: 50,
              width: 120,
              fieldLabel: "ANIO",
              name: "year",
              listeners: {
                select: function(cbo, record) {
                  var panel = cbo.up("ViewPanelRegisterEvaluationKri");
                  var keyRisk = panel
                    .down("textfield[name=idKeyRiskIndicator]")
                    .getValue();
                  me.down("ViewComboMonthKri").reset();
                  me.down("ViewComboMonthKri")
                    .getStore()
                    .load({
                      url: "http://localhost:9000/giro/getMonthComboBox.htm",
                      params: {
                        idKeyRiskIndicator: keyRisk,
                        year: cbo.getValue()
                      }
                    });
                }
              }
            },
            {
              xtype: "ViewComboMonthKri",
              allowBlank: false,
              //                        readOnly:true,
              //                        editable:false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              padding: "0 10 0 0",
              labelWidth: 50,
              width: 150,
              fieldLabel: "MES",
              name: "month"
            },

            //                    {
            //                        xtype: 'datefield',
            //                        allowBlank:false,
            //                        format:'d/m/Y',
            //                        name:'dateStart',
            //                        msgTarget: 'side',
            //                        fieldCls: 'obligatoryTextField',
            //                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
            //                        padding:'0 10 0 0',
            //                        labelWidth: 50,
            //                        width:120
            //                    },
            //                    {
            //                        xtype: 'datefield',
            //                        allowBlank:false,
            //                        format:'d/m/Y',
            //                        name:'dateLimit',
            //                        msgTarget: 'side',
            //                        fieldCls: 'obligatoryTextField',
            //                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
            //                        padding:'0 10 0 0',
            //                        labelWidth: 50,
            //                        width:150
            //                    },
            {
              xtype: "button",
              iconCls: "save",
              text: "CONSULTAR",
              action: "reSearchMasterEventOneQualification"
            }
          ]
        },
        {
          xtype: "gridpanel",
          cls: "custom-grid",
          store: StoreGridPanelGeneralQualificationEventOne,
          loadMask: true,
          columnLines: true,
          region: "center",
          flex: 2,
          padding: "2 2 2 2",
          columns: [
            { xtype: "rownumberer", width: 25, sortable: false },
            {
              header: "MES A&Ntilde;O",
              align: "center",
              dataIndex: "year",
              width: 100
            },
            {
              header: "EVENTO",
              align: "center",
              dataIndex: "descriptionMonth",
              width: 100
            },
            {
              header: "PESO",
              align: "center",
              dataIndex: "descriptionMonth",
              width: 100
            },
            {
              header: "CALIFICACI&Oacute;N",
              dataIndex: "descriptionQualificationEventOne",
              flex: 5
            },
            {
              header: "EVALUACI&Oacute;N",
              align: "center",
              dataIndex: "score",
              width: 100,
              renderer: function(value, metaData, record) {
                metaData.tdAttr =
                  'style="background-color: #' +
                  record.get("levelColour") +
                  ' !important;"';
                return '<span style="color:#949898;">' + value + "</span>";
              }
            },
            {
              header: "PONDERADO",
              dataIndex: "descriptionQualificationEventOne",
              flex: 5
            }
          ],
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: StoreGridPanelGeneralQualificationEventOne,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            items: [
              "-",
              {
                action: "exportFulFillmentPdf",
                iconCls: "pdf"
              },
              "-",
              { action: "exportFulFillmentExcel", iconCls: "excel" },
              "-"
            ],
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          },
          listeners: {
            render: function() {
              var me = this;
              me.getStore().load({
                url: "http://localhost:9000/giro/loadGridDefault.htm"
              });
            }
          }
        },
        ,
        {
          xtype: "form",
          anchor: "100%",
          region: "south",
          margin: "0 2 2 2",
          bodyPadding: "5",
          layout: {
            type: "hbox"
          },
          items: [
            //
            {
              xtype: "UpperCaseTextFieldReadOnly",
              padding: "0 10 0 0",
              flex: 1,
              labelWidth: 160,
              fieldLabel: "RESULTADO NORMALIZADO",
              name: "resultNormalized"
            },
            {
              xtype: "UpperCaseTextFieldReadOnly",
              padding: "0 10 0 0",
              flex: 1,
              fieldLabel: "DESCRIPCION",
              name: "descriptionResult"
            }
          ]
        },
        {
          xtype: "chart",
          store: Ext.create("Ext.data.Store", {
            model: "GeneralQualificationEventOne",
            autoLoad: true,
            proxy: {
              actionMethods: {
                create: "POST",
                read: "POST",
                update: "POST"
              },
              type: "ajax",
              url: "http://localhost:9000/giro/loadGridDefault.htm",
              reader: {
                totalProperty: "totalCount",
                root: "data",
                successProperty: "success"
              }
            }
          }),
          region: "east",
          width: 600,
          animate: true,
          insetPadding: 20,
          axes: [
            {
              type: "Numeric",
              position: "left",
              minimum: 0,
              adjustMinimumByMajorUnit: 0,
              fields: [
                "score",
                "rangeSup1",
                "rangeSup2",
                "rangeSup3",
                "rangeSup4",
                "rangeSup5",
                "rangeSup6",
                "rangeSup7",
                "rangeSup8",
                "rangeSup9",
                "rangeSup10"
              ],
              title: "RATIO",
              grid: {
                odd: {
                  opacity: 1,
                  fill: "#ddd",
                  stroke: "#bbb",
                  "stroke-width": 0.5
                }
              }
            },
            {
              type: "Category",
              position: "bottom",
              fields: ["description"],
              label: {
                rotate: {
                  degrees: 270
                }
              },
              title: "TIEMPO"
            }
          ],
          series: [
            {
              type: "area",
              highlight: true,
              axis: "left",
              xField: "description",
              yField: [
                "rangeSup1",
                "rangeSup2",
                "rangeSup3",
                "rangeSup4",
                "rangeSup5",
                "rangeSup6",
                "rangeSup7",
                "rangeSup8",
                "rangeSup9",
                "rangeSup10"
              ],
              style: {
                opacity: 0.93
              }
            },
            {
              type: "line",
              axis: "left",
              fillOpacity: 0.5,
              xField: "description",
              tips: {
                trackMouse: true,
                width: 150,
                height: 38,
                renderer: function(storeItem, item) {
                  this.setTitle(
                    storeItem.get("description") +
                      "<br />" +
                      storeItem.get("score")
                  );
                }
              },
              style: {
                "stroke-width": 0
              },
              yField: "score"
            },

            {
              type: "line",
              axis: "left",
              fillOpacity: 0.5,
              xField: "description",
              tips: {
                trackMouse: true,
                width: 150,
                height: 38,
                renderer: function(storeItem, item) {
                  this.setTitle(
                    storeItem.get("description") +
                      "<br />" +
                      storeItem.get("score2")
                  );
                }
              },
              style: {
                "stroke-width": 0
              },
              yField: "score"
            }
          ]
        }

        //
      ];
      this.callParent();
    }
  }
);
