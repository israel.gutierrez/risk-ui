Ext.define("SummaryEventOne", {
  extend: "Ext.data.Model",
  fields: []
});

var StoreSummaryEventOne = Ext.create("Ext.data.Store", {
  model: "SummaryEventOne",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("AverageQualification", {
  extend: "Ext.data.Model",
  fields: ["description", "resFin", "niv1", "niv2", "niv3", "niv4", "niv5"]
});

var storeAverageQualification = Ext.create("Ext.data.Store", {
  model: "AverageQualification",
  autoLoad: true,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("SummaryQualificationEventOne", {
  extend: "Ext.data.Model",
  fields: [
    "description",
    "ev1",
    "ev2",
    "ev3",
    "ev4",
    "ev5",
    "ev6",
    "ev7",
    "niv1",
    "niv2",
    "niv3",
    "niv4",
    "niv5"
  ]
});

var storeSummaryQualificationEventOneExt = Ext.create("Ext.data.Store", {
  model: "SummaryQualificationEventOne",
  autoLoad: true,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

//var colors = ['url(#v-1)',
//    'url(#v-2)',
//    'url(#v-3)',
//    'url(#v-4)',
//    'url(#v-5)'];

var colorAverage =
  '[{"color1":"3366FF","color2":"99CC00","color3":"FFFF00","color4":"FF6600","color5":"FF0000"}]';

//var baseColor = '#eee';
//Ext.define('Ext.chart.theme.Fancy', {
//    extend: 'Ext.chart.theme.Base',
//
//    constructor: function(config) {
//        this.callParent([Ext.apply({
//            axis: {
//                fill: baseColor,
//                stroke: baseColor
//            },
//            axisLabelLeft: {
//                fill: baseColor
//            },
//            axisLabelBottom: {
//                fill: baseColor
//            },
//            axisTitleLeft: {
//                fill: baseColor
//            },
//            axisTitleBottom: {
//                fill: baseColor
//            },
//            colors: colors
//        }, config)]);
//    }
//});

Ext.define(
  "DukeSource.view.risk.kri.report.ViewPanelSummaryQualificationEvent",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelSummaryQualificationEvent",
    border: false,
    layout: "border",

    initComponent: function() {
      var me = this;

      this.items = [
        {
          xtype: "panel",
          region: "west",
          activeTab: 0,
          items: [
            {
              xtype: "form",
              anchor: "100%",

              width: 300,
              margin: "2 2 0 2",
              bodyPadding: "5",
              layout: {
                type: "vbox"
              },
              items: [
                {
                  xtype: "textfield",
                  hidden: true,
                  name: "idEventOne"
                },
                {
                  xtype: "datefield",
                  fieldLabel: "BUSCA DE",
                  allowBlank: false,
                  format: "d/m/Y",
                  name: "dateStart",
                  msgTarget: "side",
                  fieldCls: "obligatoryTextField",
                  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  padding: "0 10 0 0"
                },
                {
                  xtype: "datefield",
                  fieldLabel: "HASTA",
                  allowBlank: false,
                  format: "d/m/Y",
                  name: "dateLimit",
                  msgTarget: "side",
                  fieldCls: "obligatoryTextField",
                  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  padding: "0 10 0 0"
                },
                {
                  xtype: "button",
                  iconCls: "save",
                  text: "BUSCAR",
                  action: "reSearchMasterEventOneQualification"
                }
              ]
            },
            {
              xtype: "ViewGridPanelShortQualificationEventOne",
              padding: "2 2 2 2"
            },
            { xtype: "ViewGridPanelShortEventOne", padding: "2 2 2 2" }
          ]
        },
        {
          xtype: "tabpanel",
          region: "center",
          activeTab: 0,
          items: [
            {
              xtype: "gridpanel",
              itemId: "gridEvolutionRisk",
              title: "Evolucion de Riesgo",
              cls: "custom-grid",
              store: StoreSummaryEventOne,
              loadMask: true,
              columnLines: true,
              region: "center",
              flex: 20,
              padding: "2 2 2 2",
              selType: "cellmodel",
              plugins: [
                Ext.create("Ext.grid.plugin.CellEditing", {
                  clicksToEdit: 1,
                  listeners: {
                    edit: function(editor, e, eOpts) {
                      me.down("grid").store.data.items[e.rowIdx].set(
                        "text",
                        editor.editors.items[0].field.rawValue
                      );
                    }
                  }
                })
              ],
              columns: [],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: StoreSummaryEventOne,
                displayInfo: true,
                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                items: [
                  "-",
                  {
                    action: "exportFulFillmentPdf",
                    iconCls: "pdf"
                  },
                  "-",
                  {
                    action: "exportFulFillmentExcel",
                    iconCls: "excel"
                  },
                  "-",
                  {
                    xtype:"UpperCaseTrigger",
                    fieldLabel: "FILTRAR",
                    action: "searchTriggerGridFulFillment",
                    labelWidth: 60,
                    width: 300
                  }
                ],
                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              }
            },
            {
              xtype: "chart",
              itemId: "chartSummary",
              store: storeSummaryQualificationEventOneExt,
              region: "east",
              width: 980,
              animate: true,
              title: "GRAFICO EVOLUCION DE RIESGO OPERACIONAL",
              insetPadding: 20,
              legend: {
                position: "left"
              },
              axes: [
                {
                  type: "Numeric",
                  position: "left",
                  minimum: 0,
                  adjustMinimumByMajorUnit: 0,
                  fields: [
                    "ev1",
                    "ev2",
                    "ev3",
                    "ev4",
                    "ev5",
                    "ev6",
                    "ev7",
                    "niv1",
                    "niv2",
                    "niv3",
                    "niv4",
                    "niv5"
                  ],
                  title: "NIVEL DE EVALUACIÓN",
                  grid: {
                    odd: {
                      opacity: 1,
                      fill: "#ddd",
                      stroke: "#bbb",
                      "stroke-width": 0.5
                    }
                  }
                },
                {
                  type: "Category",
                  position: "bottom",
                  fields: ["description"],
                  label: {
                    rotate: {
                      degrees: 270
                    }
                  },
                  title: "PERIODO"
                }
              ],
              series: [
                {
                  type: "area",
                  highlight: true,
                  axis: "left",
                  xField: "description",
                  yField: ["niv1", "niv2", "niv3", "niv4", "niv5"],
                  style: {
                    opacity: 0.93
                  }
                },
                {
                  type: "line",
                  axis: "left",
                  //                        title:' score 1',
                  fillOpacity: 0.5,
                  xField: "description",
                  yField: "ev1",
                  tips: {
                    trackMouse: true,
                    width: 150,
                    height: 38,
                    renderer: function(storeItem, item) {
                      this.setTitle(
                        storeItem.get("description") +
                          "<br />" +
                          storeItem.get("ev1")
                      );
                    }
                  },
                  style: {
                    "stroke-width": 0
                  }
                },
                {
                  type: "line",
                  axis: "left",
                  //                        title:' score 2',
                  fillOpacity: 0.5,
                  xField: "description",
                  yField: "ev2",
                  tips: {
                    trackMouse: true,
                    width: 150,
                    height: 38,
                    renderer: function(storeItem, item) {
                      this.setTitle(
                        storeItem.get("description") +
                          "<br />" +
                          storeItem.get("ev2")
                      );
                    }
                  },
                  style: {
                    "stroke-width": 0
                  }
                },
                {
                  type: "line",
                  axis: "left",
                  fillOpacity: 0.5,
                  xField: "description",
                  yField: "ev3",
                  tips: {
                    trackMouse: true,
                    width: 150,
                    height: 38,
                    renderer: function(storeItem, item) {
                      this.setTitle(
                        storeItem.get("description") +
                          "<br />" +
                          storeItem.get("ev3")
                      );
                    }
                  },
                  style: {
                    "stroke-width": 0
                  }
                },
                {
                  type: "line",
                  axis: "left",
                  fillOpacity: 0.5,
                  xField: "description",
                  yField: "ev4",
                  tips: {
                    trackMouse: true,
                    width: 150,
                    height: 38,
                    renderer: function(storeItem, item) {
                      this.setTitle(
                        storeItem.get("description") +
                          "<br />" +
                          storeItem.get("ev4")
                      );
                    }
                  },
                  style: {
                    "stroke-width": 0
                  }
                },
                {
                  type: "line",
                  axis: "left",
                  fillOpacity: 0.5,
                  xField: "description",
                  yField: "ev5",
                  tips: {
                    trackMouse: true,
                    width: 150,
                    height: 38,
                    renderer: function(storeItem, item) {
                      this.setTitle(
                        storeItem.get("description") +
                          "<br />" +
                          storeItem.get("ev5")
                      );
                    }
                  },
                  style: {
                    "stroke-width": 0
                  }
                },
                {
                  type: "line",
                  axis: "left",
                  fillOpacity: 0.5,
                  xField: "description",
                  yField: "ev6",
                  tips: {
                    trackMouse: true,
                    width: 150,
                    height: 38,
                    renderer: function(storeItem, item) {
                      this.setTitle(
                        storeItem.get("description") +
                          "<br />" +
                          storeItem.get("ev6")
                      );
                    }
                  },
                  style: {
                    "stroke-width": 0
                  }
                },
                {
                  type: "line",
                  axis: "left",
                  fillOpacity: 0.5,
                  xField: "description",
                  yField: "ev7",
                  tips: {
                    trackMouse: true,
                    width: 150,
                    height: 38,
                    renderer: function(storeItem, item) {
                      this.setTitle(
                        storeItem.get("description") +
                          "<br />" +
                          storeItem.get("ev7")
                      );
                    }
                  },
                  style: {
                    "stroke-width": 0
                  }
                }
              ]
            },
            {
              xtype: "chart",
              //                        theme: 'Fancy',
              itemId: "chartAverage",
              store: storeAverageQualification,
              region: "east",
              width: 980,
              animate: true,
              title: "RESULTADO RIESGO OPERACIONAL",
              legend: {
                position: "left"
              },
              //                        background: {
              //                            fill: 'rgb(17, 17, 17)'
              //                        },
              axes: [
                {
                  type: "Numeric",
                  position: "left",
                  minimum: 0,
                  adjustMinimumByMajorUnit: 0,
                  fields: ["resFin", "niv1", "niv2", "niv3", "niv4", "niv5"],
                  title: "NIVEL DE EVALUACIÓN"
                  //                                ,grid: {
                  //                                    odd: {
                  //                                        stroke: '#555'
                  //                                    },
                  //                                    even: {
                  //                                        stroke: '#555'
                  //                                    }
                  //                                }
                },

                {
                  type: "Category",
                  position: "bottom",
                  fields: ["description"],
                  label: {
                    rotate: {
                      degrees: 270
                    }
                  },
                  title: "PERIODO"
                }
              ],
              series: [
                {
                  type: "area",
                  highlight: true,
                  axis: "left",
                  xField: "description",
                  yField: ["niv1", "niv2", "niv3", "niv4", "niv5"],
                  style: {
                    opacity: 0.93
                  }
                },
                {
                  type: "line",
                  axis: "left",
                  title: "Evol. ROp",
                  fillOpacity: 2.5,
                  xField: "description",
                  yField: "resFin",
                  tips: {
                    trackMouse: true,
                    width: 150,
                    height: 38,
                    renderer: function(storeItem, item) {
                      this.setTitle(
                        storeItem.get("description") +
                          "<br />" +
                          storeItem.get("resFin")
                      );
                    }
                  },
                  style: {
                    "stroke-width": 0
                  }
                },

                {
                  type: "column",
                  axis: "left",
                  title: "Evol. ROp",
                  highlight: true,
                  tips: {
                    trackMouse: true,
                    width: 140,
                    height: 28,
                    renderer: function(storeItem, item) {
                      this.setTitle(
                        storeItem.get("description") +
                          ": " +
                          storeItem.get("resFin") +
                          ""
                      );
                    }
                  },
                  label: {
                    display: "insideEnd",
                    "text-anchor": "middle",
                    field: "resFin",
                    renderer: Ext.util.Format.numberRenderer("0"),
                    orientation: "vertical",
                    color: "#000"
                  },
                  //                                renderer: function(sprite, storeItem, barAttr, i, store) {
                  //                                    barAttr.fill = colorAverage[i];
                  //                                    return barAttr;
                  //                                },
                  style: {
                    opacity: 0.91
                  },
                  xField: "description",
                  yField: "resFin"
                }
              ]
            }
          ]
        }
      ];
      this.callParent();
    }
  }
);
