Ext.define("ModelGridPanelRegisterEvaluationKri", {
  extend: "Ext.data.Model",
  fields: [
    "january",
    "february",
    "march",
    "april",
    "may",
    "june",
    "july",
    "august",
    "september",
    "october",
    "november",
    "december",
    "averge",
    "idKeyRiskIndicator",
    "idProcess",
    "idSubProcess",
    "nameKri",
    "numberRiskAssociated",
    "codesRisk",
    "nameProcess",
    "nameSubProcess",
    "colorjanuary",
    "colorjanuary",
    "colorfebruary",
    "colormarch",
    "colorapril",
    "colormay",
    "colorjune",
    "colorjuly",
    "coloraugust",
    "colorseptember",
    "coloroctober",
    "colornovember",
    "colordecember",
    "idWorkArea",
    "nameWorkArea"
  ]
});

var StoreGridPanelRegisterEvaluationKri = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelRegisterEvaluationKri",
  groupField: "nameProcess",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
var groupingFeature = Ext.create("Ext.grid.feature.Grouping", {
  groupHeaderTpl:
    'PROCESO : {name} ({rows.length} Kri{[values.rows.length > 1 ? "s" : ""]})',
  hideGroupedHeader: true,
  startCollapsed: true
});
Ext.define("DukeSource.view.risk.kri.report.ViewPanelAreaQualificationKri", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelAreaQualificationKri",
  border: false,
  layout: "card",

  initComponent: function() {
    var me = this;
    this.items = [
      {
        xtype: "gridpanel",
        region: "center",
        store: StoreGridPanelRegisterEvaluationKri,
        loadMask: true,
        features: [groupingFeature],
        columnLines: true,
        padding: "2 2 2 2",
        columns: [
          { xtype: "rownumberer", width: 25, sortable: false },
          { header: "KRI", dataIndex: "nameKri", width: 350 },
          { header: "RIESGOS", dataIndex: "codesRisk", width: 150 },
          {
            xtype: "actioncolumn",
            header: "REL.",
            align: "center",
            width: 60,
            items: [
              {
                icon: "images/risk.png",
                handler: function(grid, rowIndex) {
                  var row = grid.store.getAt(rowIndex);
                  var windows = Ext.create(
                    "DukeSource.view.risk.kri.windows.ViewWindowSearchRiskByKri",
                    {
                      riskIndicator: grid.store
                        .getAt(rowIndex)
                        .get("idKeyRiskIndicator"),
                      title:
                        "ASIGNAR RIESGO A : " +
                        grid.store.getAt(rowIndex).get("nameKri"),
                      modal: true
                    }
                  );
                  windows.show();
                  windows.down("#codeRisk").focus(false, 100);
                  var grid1 = Ext.ComponentQuery.query(
                    "ViewWindowSearchRiskByKri grid[title=DISPONIBLES]"
                  )[0];
                  var process =
                    row.get("idProcess") == "" ? "" : row.get("idProcess");
                  var fields = "p.idProcess";
                  var values = process;
                  var types = "Long";
                  var operator = "equal";
                  grid1.store.getProxy().extraParams = {
                    fields: fields,
                    values: values,
                    types: types,
                    operators: operator,
                    search: "full",
                    isLast: "true"
                  };
                  grid1.store.getProxy().url =
                    "http://localhost:9000/giro/findMatchRisk.htm";
                  grid1.down("pagingtoolbar").moveFirst();

                  var grid2 = Ext.ComponentQuery.query(
                    "ViewWindowSearchRiskByKri grid[title=ASIGNADOS]"
                  )[0];
                  grid2.store.getProxy().extraParams = {
                    keyRiskIndicator: grid.store
                      .getAt(rowIndex)
                      .get("idKeyRiskIndicator")
                  };
                  grid2.store.getProxy().url =
                    "http://localhost:9000/giro/findListRelationWithRisk.htm";
                  grid2.down("pagingtoolbar").moveFirst();
                }
              }
            ]
          },
          {
            header: "ENE.",
            columns: [
              {
                header: "NOR.",
                width: 50,
                align: "center",
                dataIndex: "january",
                renderer: function(value, metaData, record) {
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("colorjanuary") +
                    ' !important;"';
                  return '<span style="color:#000;">' + value + "</span>";
                }
              }
            ]
          },
          {
            header: "FEB.",
            columns: [
              {
                header: "NOR.",
                width: 50,
                align: "center",
                dataIndex: "february",
                flex: 1,
                renderer: function(value, metaData, record) {
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("colorfebruary") +
                    ' !important;"';
                  return '<span style="color:#000;">' + value + "</span>";
                }
              }
            ]
          },
          {
            header: "MAR.",
            columns: [
              //{header:'UMB.',width:50},
              {
                header: "NOR.",
                width: 50,
                align: "center",
                dataIndex: "march",
                flex: 1,
                renderer: function(value, metaData, record) {
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("colormarch") +
                    ' !important;"';
                  return '<span style="color:#000;">' + value + "</span>";
                }
              }
            ]
          },
          {
            header: "ABR.",
            columns: [
              //{header:'UMB.',width:50},
              {
                header: "NOR.",
                width: 50,
                align: "center",
                dataIndex: "april",
                flex: 1,
                renderer: function(value, metaData, record) {
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("colorapril") +
                    ' !important;"';
                  return '<span style="color:#000;">' + value + "</span>";
                }
              }
            ]
          },
          {
            header: "MAY.",
            columns: [
              //{header:'UMB.',width:50},
              {
                header: "NOR.",
                width: 50,
                align: "center",
                dataIndex: "may",
                flex: 1,
                renderer: function(value, metaData, record) {
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("colormay") +
                    ' !important;"';
                  return '<span style="color:#000;">' + value + "</span>";
                }
              }
            ]
          },
          {
            header: "JUN.",
            columns: [
              {
                header: "NOR.",
                width: 50,
                align: "center",
                dataIndex: "june",
                flex: 1,
                renderer: function(value, metaData, record) {
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("colorjune") +
                    ' !important;"';
                  return '<span style="color:#000;">' + value + "</span>";
                }
              }
            ]
          },
          {
            header: "JUL.",
            columns: [
              {
                header: "NOR.",
                width: 50,
                align: "center",
                dataIndex: "july",
                flex: 1,
                renderer: function(value, metaData, record) {
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("colorjuly") +
                    ' !important;"';
                  return '<span style="color:#000;">' + value + "</span>";
                }
              }
            ]
          },
          {
            header: "AGO.",
            columns: [
              {
                header: "NOR.",
                width: 50,
                align: "center",
                dataIndex: "august",
                flex: 1,
                renderer: function(value, metaData, record) {
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("coloraugust") +
                    ' !important;"';
                  return '<span style="color:#000;">' + value + "</span>";
                }
              }
            ]
          },
          {
            header: "SEP.",
            columns: [
              {
                header: "NOR.",
                width: 50,
                align: "center",
                dataIndex: "september",
                flex: 1,
                renderer: function(value, metaData, record) {
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("colorseptember") +
                    ' !important;"';
                  return '<span style="color:#000;">' + value + "</span>";
                }
              }
            ]
          },
          {
            header: "OCT.",
            columns: [
              {
                header: "NOR.",
                width: 50,
                align: "center",
                dataIndex: "october",
                flex: 1,
                renderer: function(value, metaData, record) {
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("coloroctober") +
                    ' !important;"';
                  return '<span style="color:#000;">' + value + "</span>";
                }
              }
            ]
          },
          {
            header: "NOV.",
            columns: [
              {
                header: "NOR.",
                width: 50,
                align: "center",
                dataIndex: "november",
                flex: 1,
                renderer: function(value, metaData, record) {
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("colornovember") +
                    ' !important;"';
                  return '<span style="color:#000;">' + value + "</span>";
                }
              }
            ]
          },
          {
            header: "DIC.",
            columns: [
              {
                header: "NOR.",
                width: 50,
                align: "center",
                dataIndex: "december",
                flex: 1,
                renderer: function(value, metaData, record) {
                  metaData.tdAttr =
                    'style="background-color: #' +
                    record.get("colordecember") +
                    ' !important;"';
                  return '<span style="color:#000;">' + value + "</span>";
                }
              }
            ]
          },
          {
            header: "PROM.",
            align: "center",
            dataIndex: "averge",
            flex: 1,
            renderer: function(value, metaData, record) {
              metaData.tdAttr = 'style="background-color: #cdcfcf !important;"';
              return '<span style="color:#000;">' + value + "</span>";
            }
          }
          /*{
                        xtype: 'actioncolumn',
                        align: 'center',
                        header:'DETALLE',
                        width: 50,
                        items: [
                            {
                                icon: 'images/kri.png',
                                handler: function (grid, rowIndex) {
                                    me.getLayout().setActiveItem(1);
                                }
                            }
                        ]
                     }*/
        ],
        bbar: {
          xtype: "pagingtoolbar",
          pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
          store: StoreGridPanelRegisterEvaluationKri,
          displayInfo: true,
          displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
          items: [
            {
              xtype:"UpperCaseTrigger",
              fieldLabel: "FILTRAR",
              action: "searchTriggerGridAreaQualification",
              labelWidth: 60,
              width: 300
            }
          ],
          emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
        },
        tbar: [
          "-",
          {
            xtype: "ViewComboYear",
            fieldLabel: "Buscar por año",
            listeners: {
              select: function(cbo) {
                var panel = cbo.up("ViewPanelAreaQualificationKri");
                var grid = panel.down("grid");
                (grid.store.getProxy().extraParams = { year: cbo.getValue() }),
                  (grid.store.getProxy().url =
                    "http://localhost:9000/giro/getIndicatorRiskOperationalByMount.htm"),
                  grid.down("pagingtoolbar").moveFirst();
              }
            }
          },
          "-",
          {
            itemId: "collapse",
            xtype: "button",
            text: "Reducir Todo",
            pressed: true,
            enableToggle: true,
            handler: function() {
              var view = me.down("grid").getView();
              var groupFeature = view.features[0];
              var button = me.down("#collapse");
              if (me.down("#collapse").pressed) {
                button.setText("Reducir Todo");
                groupFeature.expandAll();
              } else {
                button.setText("Expandir Todo");
                groupFeature.collapseAll();
              }
            }
          }
        ]
      },
      {
        xtype: "panel",
        padding: "2 2 2 2",
        layout: {
          type: "vbox",
          align: "stretch"
        },
        tbar: [
          "-",
          {
            xtype: "button",
            text: "ATRAS",
            handler: function() {
              me.getLayout().setActiveItem(0);
            }
          }
        ],
        items: [
          {
            xtype: "container",
            flex: 1,
            layout: {
              type: "hbox",
              align: "stretch"
            },
            items: [
              {
                xtype: "form",
                flex: 1,
                bodyPadding: 10,
                title: "My Form",
                items: [
                  {
                    xtype: "textfield",
                    anchor: "100%",
                    fieldLabel: "INDICADOR"
                  },
                  {
                    xtype: "textfield",
                    anchor: "100%",
                    fieldLabel: "PROCESO N1"
                  },
                  {
                    xtype: "textfield",
                    anchor: "100%",
                    fieldLabel: "PROCESO N2"
                  },
                  {
                    xtype: "textfield",
                    anchor: "100%",
                    fieldLabel: "PROCESO N3"
                  },
                  {
                    xtype: "textfield",
                    anchor: "100%",
                    fieldLabel: "UMBRALES"
                  },
                  {
                    xtype: "textfield",
                    anchor: "100%",
                    fieldLabel: "NIVELES"
                  }
                ]
              },
              {
                xtype: "form",
                flex: 1,
                bodyPadding: 10,
                title: "My Form",
                items: [
                  {
                    xtype: "textareafield",
                    anchor: "100%",
                    fieldLabel: "FORMULA"
                  }
                ]
              }
            ]
          },
          {
            xtype: "container",
            flex: 1,
            layout: {
              type: "hbox",
              align: "stretch"
            },
            items: [
              {
                xtype: "form",
                flex: 1,
                bodyPadding: 10,
                title: "My Form"
              },
              {
                xtype: "form",
                flex: 1,
                bodyPadding: 10,
                title: "My Form"
              }
            ]
          }
        ]
      }
    ];
    this.callParent();
  }
});
