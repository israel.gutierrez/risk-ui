Ext.draw.engine.ImageExporter.defaultUrl = "ImageExportService";
Ext.define("detailQualification", {
  extend: "Ext.data.Model",
  fields: ["user", "taskWeight", "percentage"]
});
Ext.define("chartDetailReportKri", {
  extend: "Ext.data.Model",
  fields: [
    "defaultWeighing0245",
    "defaultWeighing0246",
    "defaultWeighing0247",
    "dateReport"
  ]
});

var store = Ext.create("Ext.data.Store", {
  model: "chartDetailReportKri",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("ModelGridDetailReportKri", {
  extend: "Ext.data.Model",
  fields: []
});

var StoreGridDetailReportKri = Ext.create("Ext.data.Store", {
  model: "ModelGridDetailReportKri",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.kri.report.ViewPanelDetailReportKri", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelDetailReportKri",
  border: false,
  layout: "border",
  tbar: [
    "-",
    {
      text: "BUSCAR KRI",
      iconCls: "search",
      action: "searchDetailReportKri"
    },
    "-"
  ],
  initComponent: function() {
    var me = this;

    this.items = [
      {
        xtype: "form",
        anchor: "100%",
        region: "north",
        margin: "2 2 0 2",
        bodyPadding: "5",

        layout: {
          type: "hbox"
        },
        items: [
          {
            xtype: "textfield",
            hidden: true,
            name: "idKeyRiskIndicator"
          },
          {
            xtype: "UpperCaseTextFieldReadOnly",
            padding: "0 10 0 0",
            width: 100,
            name: "codeKri"
          },
          {
            xtype: "UpperCaseTextFieldReadOnly",
            padding: "0 10 0 0",
            flex: 3,
            fieldLabel: "KRI",
            name: "nameKri"
          },
          {
            xtype: "UpperCaseTextFieldReadOnly",
            width: 100,
            padding: "0 10 0 0",
            name: "abbreviation"
          },
          {
            xtype: "datefield",
            allowBlank: false,
            format: "d/m/Y",
            name: "dateStart",
            msgTarget: "side",
            fieldCls: "obligatoryTextField",
            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
            padding: "0 10 0 0",
            labelWidth: 50,
            width: 120
          },
          {
            xtype: "datefield",
            allowBlank: false,
            format: "d/m/Y",
            name: "dateLimit",
            msgTarget: "side",
            fieldCls: "obligatoryTextField",
            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
            padding: "0 10 0 0",
            labelWidth: 50,
            width: 150
          },
          {
            xtype: "button",
            iconCls: "save",
            text: "CONSULTAR",
            action: "searchResumeDetailKri"
          }
        ]
      },
      {
        xtype: "tabpanel",
        region: "center",
        padding: "2 2 2 2",
        items: [
          {
            xtype: "gridpanel",
            itemId: "gridReportDetail",
            cls: "custom-grid",
            title: "LISTADO KRI",
            store: StoreGridDetailReportKri,
            loadMask: true,
            columnLines: true,
            region: "center",
            columns: [],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridDetailReportKri,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
            },
            listeners: {
              render: function() {
                var me = this;
                me.getStore().load({
                  url: "http://localhost:9000/giro/loadGridDefault.htm"
                });
              }
            }
          },
          {
            xtype: "panel",
            title: "GRAFICA KRI",
            itemId: "chartDetailKri",
            layout: "fit",
            tbar: [
              {
                text: "DISEÑAR REPORTE",
                iconCls: "maintainance",
                handler: function() {
                  if (
                    me
                      .down("UpperCaseTextFieldReadOnly[name=nameKri]")
                      .getValue() == ""
                  ) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      "SELECCIONE UN KRI",
                      Ext.Msg.WARNING
                    );
                  } else {
                    if (
                      Ext.ComponentQuery.query("ViewWindowDesignReport")[0] ==
                      undefined
                    ) {
                      var win = Ext.create(
                        "DukeSource.view.risk.kri.report.ViewWindowDesignReport",
                        {}
                      ).show();
                      win
                        .down("ViewComboDesignReportKri")
                        .getStore()
                        .load({
                          url:
                            "http://localhost:9000/giro/listDescriptionWeighingKri.htm",
                          params: {
                            idKeyRiskIndicator: me
                              .down("textfield[name=idKeyRiskIndicator]")
                              .getValue()
                          }
                        });
                    }
                  }
                }
              },
              {
                text: "CARGAR DATOS",
                iconCls: "operations",
                action: "loadDataChartKri"
              },
              {
                text: "GUARDAR REPORTE",
                iconCls: "save",
                action: "saveChartReportDetailKri"
              }
            ],
            items: []
          }
        ]
      }
    ];
    this.callParent();
  }
});
