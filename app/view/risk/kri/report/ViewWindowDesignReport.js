Ext.define('DukeSource.view.risk.kri.report.ViewWindowDesignReport', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowDesignReport',
    width: 479,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'PARAMETROS GRAFICA KRI',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'combobox',
                            anchor: '100%',
                            itemId: 'typeChart',
                            store: {
                                fields: ['id', 'description'],
                                data: [
                                    {id: '1', description: 'APILADO'},
                                    {id: '2', description: 'COLUMNA'},
                                    {id: '3', description: 'COLUMNA-LINEA'}
                                ]
                            },
                            displayField: 'description',
                            valueField: 'id',
                            fieldLabel: 'TIPO GRAFICA'
                        },
                        {
                            xtype: 'ViewComboDesignReportKri',
                            anchor: '100%',
                            multiSelect: true,
                            delimiter: ' | ',
                            itemId: 'columnReportKri',
                            name: 'columnReportKri',
                            fieldLabel: 'COLUMNAS'
                        },
                        {
                            xtype: 'ViewComboDesignReportKri',
                            anchor: '100%',
                            itemId: 'lineReportKri',
                            name: 'lineReportKri',
                            fieldLabel: 'LINEA'
                        }

                    ]
                }
            ],
            buttons: [
                {
                    text: 'GENERAR',
                    action: 'designReportDetailKri',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ]
        });

        me.callParent(arguments);
    }

});
