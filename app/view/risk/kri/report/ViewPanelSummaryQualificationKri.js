Ext.define("detailQualification", {
  extend: "Ext.data.Model",
  fields: ["user", "taskWeight", "percentage"]
});
Ext.define("SummaryQualification", {
  extend: "Ext.data.Model",
  fields: [
    "description",
    "evaluationResult",
    "rangeSup1",
    "rangeSup2",
    "rangeSup3",
    "rangeSup4",
    "rangeSup5",
    "rangeSup6",
    "rangeSup7",
    "rangeSup8",
    "rangeSup9",
    "rangeSup10"
  ]
});

var store = Ext.create("Ext.data.Store", {
  model: "SummaryQualification",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("ModelGridPanelSummaryQualificationKri", {
  extend: "Ext.data.Model",
  fields: [
    "evaluationResult",
    "idQualificationKri",
    "levelColour",
    "descriptionQualificationKri",
    "descriptionQualificationEachKri",
    "levelColourQualificationEachKri",
    "year",
    "month",
    "averageIndicatorValue",
    "descriptionMonth"
  ]
});

var StoreGridPanelSummaryQualificationKri = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelSummaryQualificationKri",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.kri.report.ViewPanelSummaryQualificationKri", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelSummaryQualificationKri",
  border: false,
  layout: "border",
  tbar: [
    "-",
    {
      text: "CONSULTAR KRI",
      iconCls: "search",
      action: "searchMasterKriQualification"
    },
    "-"
  ],
  initComponent: function() {
    var me = this;

    this.items = [
      {
        xtype: "form",
        anchor: "100%",
        region: "north",
        margin: "2 2 0 2",
        bodyPadding: "5",

        layout: {
          type: "hbox"
        },
        items: [
          {
            xtype: "textfield",
            hidden: true,
            name: "idKeyRiskIndicator"
          },
          {
            xtype: "UpperCaseTextFieldReadOnly",
            padding: "0 10 0 0",
            width: 100,
            name: "codeKri"
          },
          {
            xtype: "UpperCaseTextFieldReadOnly",
            padding: "0 10 0 0",
            flex: 3,
            fieldLabel: "KRI",
            name: "nameKri"
          },
          {
            xtype: "UpperCaseTextFieldReadOnly",
            width: 100,
            padding: "0 4 0 0",
            name: "abbreviation"
          },
          {
            xtype: "datefield",
            allowBlank: false,
            format: "d/m/Y",
            name: "dateStart",
            msgTarget: "side",
            fieldCls: "obligatoryTextField",
            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
            padding: "0 4 0 0",
            labelWidth: 50,
            width: 120
          },
          {
            xtype: "datefield",
            allowBlank: false,
            format: "d/m/Y",
            name: "dateLimit",
            msgTarget: "side",
            fieldCls: "obligatoryTextField",
            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
            padding: "0 4 0 0",
            labelWidth: 50,
            width: 120
          },
          {
            xtype: "checkbox",
            fieldLabel: "NORMALIZADO",
            name: "normalized",
            labelAlign: "left",
            boxLabelAlign: "left",
            itemId: "normalized",
            inputValue: "S",
            flex: 1.2,
            checked: true,
            uncheckedValue: "N",
            listeners: {
              change: function(cb) {
                cb.setFieldLabel(
                  cb.getValue() == true ? "NORMALIZADO" : "SIN NORMALIZAR"
                );
              }
            }
          },
          {
            xtype: "button",
            iconCls: "save",
            text: "CONSULTAR",
            action: "reSearchMasterKriQualification"
          }
        ]
      },
      {
        xtype: "gridpanel",
        cls: "custom-grid",
        store: StoreGridPanelSummaryQualificationKri,
        loadMask: true,
        columnLines: true,
        region: "center",
        flex: 2,
        padding: "2 0 2 2",
        columns: [
          { xtype: "rownumberer", width: 25, sortable: false },
          {
            header: "A&Ntilde;O",
            align: "center",
            dataIndex: "year",
            width: 70
          },
          {
            header: "MES",
            align: "center",
            dataIndex: "descriptionMonth",
            width: 80
          },
          {
            header: "CALIFICACI&Oacute;N",
            dataIndex: "descriptionQualificationKri",
            flex: 5
          },
          {
            header: "INDICADOR",
            dataIndex: "averageIndicatorValue",
            xtype: "numbercolumn",
            format: "0,0.00",
            width: 90,
            renderer: function(value, metaData, record) {
              metaData.tdAttr =
                'style="background-color: #' +
                record.get("levelColourQualificationEachKri") +
                ' !important;"';
              return '<span style="color:#949898;">' + value + "</span>";
            }
          },
          {
            header: "EVALUACI&Oacute;N",
            align: "center",
            dataIndex: "evaluationResult",
            width: 90,
            renderer: function(value, metaData, record) {
              metaData.tdAttr =
                'style="background-color: #' +
                record.get("levelColour") +
                ' !important;"';
              return '<span style="color:#949898;">' + value + "</span>";
            }
          }
        ],
        bbar: {
          xtype: "pagingtoolbar",
          pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
          store: StoreGridPanelSummaryQualificationKri,
          displayInfo: true,
          displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
          items: [
            "-",
            {
              action: "exportFulFillmentPdf",
              iconCls: "pdf"
            },
            "-",
            { action: "exportFulFillmentExcel", iconCls: "excel" },
            "-"
          ],
          emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
        },
        listeners: {
          render: function() {
            var me = this;
            me.getStore().load({
              url: "http://localhost:9000/giro/loadGridDefault.htm"
            });
          }
        }
      },
      {
        xtype: "chart",
        title: "NORMALIZADO",
        store: Ext.create("Ext.data.Store", {
          model: "SummaryQualification",
          autoLoad: true,
          proxy: {
            actionMethods: {
              create: "POST",
              read: "POST",
              update: "POST"
            },
            type: "ajax",
            url: "http://localhost:9000/giro/loadGridDefault.htm",
            reader: {
              totalProperty: "totalCount",
              root: "data",
              successProperty: "success"
            }
          }
        }),
        region: "east",
        width: 900,
        split: true,
        animate: true,
        insetPadding: 20,
        axes: [
          {
            type: "Numeric",
            position: "left",
            minimum: 0,
            adjustMinimumByMajorUnit: 0,
            fields: [
              "evaluationResult",
              "rangeSup1",
              "rangeSup2",
              "rangeSup3",
              "rangeSup4",
              "rangeSup5",
              "rangeSup6",
              "rangeSup7",
              "rangeSup8",
              "rangeSup9",
              "rangeSup10"
            ],
            title: "RATIO",
            grid: {
              odd: {
                opacity: 1,
                fill: "#ddd",
                stroke: "#bbb",
                "stroke-width": 0.5
              }
            }
          },
          {
            type: "Category",
            position: "bottom",
            fields: ["description"],
            label: {
              rotate: {
                degrees: 270
              }
            },
            title: "TIEMPO"
          }
        ],
        series: [
          {
            type: "area",
            highlight: true,
            axis: "left",
            xField: "description",
            yField: [
              "rangeSup1",
              "rangeSup2",
              "rangeSup3",
              "rangeSup4",
              "rangeSup5",
              "rangeSup6",
              "rangeSup7",
              "rangeSup8",
              "rangeSup9",
              "rangeSup10"
            ],
            style: {
              opacity: 0.93
            }
          },
          {
            type: "line",
            axis: "left",
            fillOpacity: 0.5,
            xField: "description",
            tips: {
              trackMouse: true,
              width: 150,
              height: 38,
              renderer: function(storeItem, item) {
                this.setTitle(
                  storeItem.get("description") +
                    "<br />" +
                    storeItem.get("evaluationResult")
                );
              }
            },
            style: {
              "stroke-width": 0
            },
            yField: "evaluationResult"
          }
        ]
      }
    ];
    this.callParent();
  }
});
