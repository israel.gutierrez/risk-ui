Ext.define("ModelGridPanelConfigMasterKri", {
  extend: "Ext.data.Model",
  fields: [
    "idKeyRiskIndicator",
    "idProcessType",
    "idProcess",
    "idSubProcess",
    "idActivity",
    "descriptionSubProcess",
    "descriptionProcess",
    "descriptionProcessType",
    "workArea",
    "descriptionWorkArea",
    "jobPlace",
    "descriptionJobPlace",
    "factorRisk",
    "managerRisk",
    "calculatingFrequency",
    "descriptionFrequency",
    "descriptionFactorRisk",
    "abbreviationFrequency",
    "codeKri",
    "nameKri",
    "description",
    "abbreviation",
    "remarks",
    "comments",
    "tacticalTarget",
    "formCalculating",
    "indicatorKri",
    "coverage",
    "modeEvaluation",
    "unitMeasure",
    "state",
    "fullName",
    "category",
    "indicatorCriteria",
    "indicatorQualification",
    "indicatorConfiguration",
    "indicatorEvaluation",
    "indicatorNormalizer",
    "initPeriod",
    "initYear",
    "dateInit",
    "numberRiskAssociated",
    "codesRisk",
    "initMonth",
    "year",
    "month"
  ]
});

var StoreGridPanelConfigMasterKri = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelConfigMasterKri",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.kri.ViewPanelConfigMasterKri", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelConfigMasterKri",
  border: false,
  layout: "fit",
  tbar: [
    {
      text: "Nuevo",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      iconCls: "add",
      handler: function() {
        var win = Ext.create(
          "DukeSource.view.risk.kri.windows.ViewWindowEntryKri",
          {
            actionType: "new"
          }
        ).show();

        var gridUser = win.down("grid");

        gridUser.store.getProxy().extraParams = {
          propertyOrder: "idQualificationKri"
        };
        gridUser.store.getProxy().url =
          "http://localhost:9000/giro/showListQualificationKriActives.htm";
        gridUser.store.load();
        win.down("#nameKri").focus(false, 200);
      }
    },
    "-",
    {
      text: "Modificar",
      iconCls: "modify",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      handler: function() {
        var app = DukeSource.app;
        app
          .getController(
            "DukeSource.controller.risk.kri.ControllerPanelConfigMasterKri"
          )
          ._onModifyEntryKri();
      }
    },
    "-",
    {
      text: "Eliminar",
      iconCls: "delete",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      action: "deleteEntryKri"
    },
    "-",
    {
      text: "Configurar",
      iconCls: "cog_edit",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      menu: {
        items: [
          {
            text: "Variables del kri",
            action: "configDetailKri",
            iconCls: "replicate"
          },
          {
            text: "Usuarios responsables",
            iconCls: "users",
            hidden: hidden("VPK_MNU_OwnerKri"),
            handler: function(btn) {
              var grid = Ext.ComponentQuery.query(
                "ViewPanelConfigMasterKri grid"
              )[0];
              var win = DukeSource.global.DirtyView.createWindowSearch(
                grid,
                "deleteKriUserAssign",
                "auditKriUserAssign"
              );
              win.down("#headerWindow").setTitle("<b>INFORMACION DEL KRI</b>");
              win.down("#name").setValue(win.record.get("nameKri"));
              win.down("#code").setValue(win.record.get("codeKri"));

              var gridUser = win.down("grid");
              gridUser.store.getProxy().extraParams = {
                idKri: win.record.get("idKeyRiskIndicator")
              };
              gridUser.store.getProxy().url =
                "http://localhost:9000/giro/showKriUsersAssign.htm";
              gridUser.down("pagingtoolbar").moveFirst();
            }
          }
        ]
      }
    },
    {
      text: "Ir evaluación",
      tooltip: "Registrar evaluación del KRI",
      iconCls: "ir",
      scale: "medium",
      cls: "my-btn",
      action: "goEvaluationKri",
      overCls: "my-over"
    },
    "->",
    {
      text: "Ficha kri",
      hidden: hidden("VPK_BN_ProfileKri"),
      action: "showProfileKri",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      iconCls: "pdf"
    },
    {
      text: "Auditoría",
      action: "auditoryEntryKri",
      iconCls: "auditory"
    }
  ],
  initComponent: function() {
    this.items = [
      {
        xtype: "container",
        layout: {
          align: "stretch",
          type: "hbox"
        },
        items: [
          {
            xtype: "gridpanel",
            name: "masterKri",
            border: false,
            columnLines: true,
            itemId: "masterGridKri",
            store: StoreGridPanelConfigMasterKri,
            flex: 1,
            columns: [
              {
                dataIndex: "descriptionProcess",
                flex: 5,
                tdCls: "process-columnFree",
                header: "Proceso",
                renderer: function(value) {
                  var s = value.split(" &#8702; ");
                  var path = "";
                  for (var i = 0; i < s.length; i++) {
                    var text = s[i];
                    text = s[i] + "<br>";
                    path = path + " &#8702; " + text;
                  }
                  return path;
                }
              },
              {
                dataIndex: "codeKri",
                width: 60,
                header: "Código"
              },
              {
                dataIndex: "nameKri",
                flex: 5,
                header: "Descripción"
              },
              {
                dataIndex: "indicatorConfiguration",
                width: 100,
                align: "center",
                header: "Configuración",
                renderer: function(v, metaData, record) {
                  if (v === "N") {
                    return '<div style="color:#f38c1f; font-size: 10px"><b>INCOMPLETA</b></div>';
                  } else {
                    return '<div style="color:#2d9231; font-size: 10px"><b>COMPLETA</b></div>';
                  }
                }
              },
              {
                dataIndex: "descriptionWorkArea",
                width: 200,
                header: "Área responsable"
              },
              {
                dataIndex: "descriptionFrequency",
                width: 110,
                align: "center",
                header: "Frecuencia de reporte"
              },
              {
                header: "Riesgos vinculados",
                align: "center",
                columns: [
                  {
                    header: "NRO",
                    align: "center",
                    dataIndex: "numberRiskAssociated",
                    width: 30
                  },
                  {
                    header: "Riesgos",
                    dataIndex: "codesRisk",
                    width: 100,
                    renderer: function(a) {
                      return '<span style="color:red;">' + a + "</span>";
                    }
                  },
                  {
                    xtype: "actioncolumn",
                    align: "center",
                    width: 40,
                    items: [
                      {
                        icon: "images/risk.png",
                        handler: function(grid, rowIndex) {
                          var row = grid.store.getAt(rowIndex);
                          var win = Ext.create(
                            "DukeSource.view.risk.kri.windows.ViewWindowSearchRiskByKri",
                            {
                              riskIndicator: row.get("idKeyRiskIndicator"),
                              process: row.get("process"),
                              title: "Asignar riesgo a : " + row.get("nameKri"),
                              modal: true
                            }
                          ).show();

                          var grid2 = win.down("#gridRiskSelected");
                          grid2.store.getProxy().extraParams = {
                            keyRiskIndicator: grid.store
                              .getAt(rowIndex)
                              .get("idKeyRiskIndicator")
                          };
                          grid2.store.getProxy().url =
                            "http://localhost:9000/giro/findListRelationWithRisk.htm";
                          grid2.down("pagingtoolbar").moveFirst();
                        }
                      }
                    ]
                  }
                ]
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridPanelConfigMasterKri,
              displayInfo: true,
              displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
              items: [
                {
                  xtype:"UpperCaseTrigger",
                  fieldLabel: "FILTRAR",
                  action: "searchTriggerConfigMasterKri",
                  labelWidth: 60,
                  width: 250
                }
              ]
            },
            listeners: {
              render: function() {
                var grid = this;
                DukeSource.global.DirtyView.searchPaginationGridNormal(
                  "",
                  grid,
                  grid.down("pagingtoolbar"),
                  "http://localhost:9000/giro/showListKeyRiskIndicatorActives.htm",
                  "",
                  "idKeyRiskIndicator"
                );
              },
              itemdblclick: function(grid, record, item, index, e) {
                var app = DukeSource.application;
                app
                  .getController(
                    "DukeSource.controller.risk.kri.ControllerPanelConfigMasterKri"
                  )
                  ._onModifyEntryKri();
              }
            }
          }
        ]
      }
    ];
    this.callParent();
  }
});
