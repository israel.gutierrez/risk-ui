Ext.define('DukeSource.view.risk.kri.ViewPanelReportsKri', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelReportsKri',
    requires: [
        'DukeSource.view.risk.parameter.combos.ViewComboYear'
    ],
    border: false,
    layout: 'fit',
    initComponent: function () {
        var me = this;
        this.items = [
            {
                xtype: 'container',
                name: 'generalContainer',
                border: false,
                layout: {
                    align: 'stretch',
                    type: 'border'
                },
                items: [
                    {
                        xtype: 'panel',
                        padding: '2 2 2 0',
                        flex: 3,
                        border: false,
                        itemId: 'panelReport',
                        titleAlign: 'center',
                        region: 'center',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'panel',
                                flex: 1,
                                title: 'Filtros',
                                items: [
                                    {
                                        xtype: 'container',
                                        hidden: true,
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                name: 'nameReport',
                                                itemId: 'nameReport',
                                                hidden: true
                                            },
                                            {
                                                xtype: 'textfield',
                                                name: 'nameFile',
                                                itemId: 'nameFile',
                                                hidden: true
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'form',
                                        border: false,
                                        hidden: true,
                                        bodyPadding: 5,
                                        fieldDefaults: {
                                            labelCls: 'changeSizeFontToEightPt',
                                            fieldCls: 'changeSizeFontToEightPt'
                                        },
                                        items: [
                                            {
                                                xtype: 'ViewComboYear',
                                                msgTarget: 'side',
                                                hidden: true,
                                                fieldCls: 'obligatoryTextField',
                                                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                fieldLabel: 'Año',
                                                name: 'year',
                                                itemId: 'year'
                                            },
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'F.reporte desde',
                                                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                format: 'd/m/Y',
                                                hidden: true,
                                                itemId: 'dateInit',
                                                name: 'dateInit'
                                            },
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'F.reporte hasta',
                                                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                format: 'd/m/Y',
                                                hidden: true,
                                                name: 'dateEnd',
                                                itemId: 'dateEnd'
                                            }

                                        ],
                                        buttons: [
                                            {
                                                text: 'Limpiar',
                                                scale: 'medium',
                                                iconCls: 'clear',
                                                handler: function () {
                                                    me.down('form').getForm().reset();
                                                }
                                            },
                                            {
                                                text: 'Generar',
                                                scale: 'medium',
                                                iconCls: 'excel',
                                                action: 'generateReportsKri'
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                xtype: 'container',
                                itemId: 'containerReport',
                                flex: 3,
                                margins: '0 0 0 2',
                                style: {
                                    background: '#dde8f4',
                                    border: '#99bce8 solid 1px !important'
                                },
                                layout: 'fit',
                                items: []
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        title: 'Reportes',
                        region: 'west',
                        width: 250,
                        height: 300,
                        collapsible: true,
                        layout: 'accordion',
                        margins: '2',
                        items: [
                            {
                                xtype: 'menu',
                                floating: false,
                                bodyStyle: 'background-color:#f3f7fb !important;',
                                items: [
                                    {
                                        text: "Consolidado de Kris",
                                        leaf: true,
                                        nameDownload: 'Consolidado_kri',
                                        nameReport: nameReport('ConsolidateKRI'),
                                        dateInit: false,
                                        year: true,
                                        dateEnd: false
                                    },
                                    {
                                        text: "Detalle de Kris",
                                        leaf: true,
                                        hidden: hidden('DetailAllKRI'),
                                        nameDownload: 'Detalle_kris',
                                        nameReport: nameReport('DetailAllKRI'),
                                        dateInit: true,
                                        year: false,
                                        dateEnd: true
                                    }
                                ],
                                listeners: {
                                    click: function (menu, item, e) {
                                        me.down('form').getForm().reset();
                                        me.down('form').setVisible(true);
                                        me.down('#nameFile').setValue(item.nameDownload);
                                        me.down('#nameReport').setValue(item.nameReport);
                                        me.down('#dateInit').setVisible(item.dateInit);
                                        me.down('#dateEnd').setVisible(item.dateEnd);
                                        me.down('#year').setVisible(item.year);
                                    }
                                }
                            }
                        ]
                    }
                ]

            }
        ];
        this.callParent();
    }
});

