Ext.define("ModelGriEvKri", {
  extend: "Ext.data.Model",
  fields: [
    "yearMonth",
    "year",
    "month",
    "idFrequency",
    "idKeyRiskIndicator",
    "indicatorKri",
    "descriptionFrequency",
    "indicatorNormalizer",
    "userRegister",
    "description"
  ]
});

Ext.define("DukeSource.view.risk.kri.ViewPanelRegisterEvaluationKri", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelRegisterEvaluationKri",
  border: false,
  layout: "border",
  initComponent: function() {
    var me = this;
    var rowMaster = this.rowMaster;
    var gRow = -1;
    var storeEvKri = Ext.create("Ext.data.Store", {
      model: "ModelGriEvKri",
      autoLoad: true,
      proxy: {
        actionMethods: {
          create: "POST",
          read: "POST",
          update: "POST"
        },
        type: "ajax",
        url: "http://localhost:9000/giro/loadGridDefault.htm",
        reader: {
          type: "json",
          root: "data",
          successProperty: "success",
          totalProperty: "totalCount"
        }
      }
    });
    this.items = [
      {
        xtype: "form",
        anchor: "100%",
        region: "north",
        border: false,
        bodyPadding: "5",
        layout: {
          type: "hbox",
          defaultMargins: {
            right: 5
          }
        },
        items: [
          {
            xtype: "textfield",
            hidden: true,
            itemId: "idKri",
            name: "idKri"
          },
          {
            xtype: "textfield",
            hidden: true,
            name: "indicatorNormalizer",
            itemId: "indicatorNormalizer"
          },
          {
            xtype: "textfield",
            hidden: true,
            name: "indicatorKri",
            itemId: "indicatorKri"
          },
          {
            xtype: "fieldset",
            flex: 2,
            title: "<b>DATOS DEL KRI Y LA EVALUACIÓN</b>",
            layout: {
              type: "hbox",
              align: "stretch"
            },
            items: [
              {
                xtype: "textfield",
                padding: "0 5 0 0",
                width: 100,
                name: "codeKri",
                fieldCls: "codeIncident",
                itemId: "codeKri"
              },
              {
                xtype: "button",
                margin: "0 5 0 0",
                width: 80,
                text: "VER KRI",
                handler: function() {
                  var app = DukeSource.app;
                  app
                    .getController(
                      "DukeSource.controller.risk.kri.ControllerPanelConfigMasterKri"
                    )
                    ._onModifyEntryKri(rowMaster);
                }
              },
              {
                xtype: "container",
                flex: 1,
                layout: {
                  type: "vbox",
                  align: "stretch"
                },
                items: [
                  {
                    xtype: "UpperCaseTextFieldReadOnly",
                    labelWidth: 30,
                    flex: 5,
                    fieldLabel: "KRI",
                    name: "nameKri",
                    itemId: "nameKri",
                    fieldStyle: {
                      textAlign: "center"
                    }
                  },
                  {
                    xtype: "container",
                    flex: 1,
                    layout: {
                      type: "hbox"
                    },
                    items: [
                      {
                        xtype: "UpperCaseTextFieldReadOnly",
                        width: 200,
                        padding: "0 1 0 0",
                        hidden: true,
                        name: "abbreviation",
                        itemId: "abbreviation"
                      },
                      {
                        xtype: "textfield",
                        readOnly: true,
                        msgTarget: "side",
                        flex: 1,
                        fieldCls: "readOnlyText",
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        labelWidth: 30,
                        fieldLabel: "A&Ntilde;O",
                        name: "year",
                        itemId: "year",
                        fieldStyle: {
                          textAlign: "center"
                        }
                      },
                      {
                        xtype: "textfield",
                        readOnly: true,
                        msgTarget: "side",
                        flex: 1,
                        fieldCls: "readOnlyText",
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        padding: "0 0 0 5",
                        name: "abbreviationFrequency",
                        itemId: "abbreviationFrequency",
                        fieldStyle: {
                          textAlign: "center"
                        }
                      },
                      {
                        xtype: "textfield",
                        readOnly: true,
                        msgTarget: "side",
                        fieldCls: "readOnlyText",
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        padding: "0 0 0 5",
                        flex: 1,
                        fieldStyle: {
                          textAlign: "center"
                        },
                        align: "center",
                        name: "month",
                        itemId: "month"
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        xtype: "grid",
        id: "grid-pnl",
        store: storeEvKri,
        loadMask: true,
        columnLines: true,
        region: "center",
        flex: 2,
        multiSelect: true,
        forceFit: true,
        padding: "0 2 2 2",
        tbar: [
          {
            text: "REGISTRAR",
            cls: "my-btn",
            overCls: "my-over",
            iconCls: "add",
            scale: "medium",
            action: "entryEvaluationKri"
          },
          "-",
          {
            text: "ELIMINAR",
            cls: "my-btn",
            overCls: "my-over",
            scale: "medium",
            iconCls: "delete",
            action: "deleteEvaluationKri"
          },
          "-",
          {
            text: "DETALLE",
            cls: "my-btn",
            overCls: "my-over",
            scale: "medium",
            iconCls: "detail",
            action: "viewDetailEvaluationKri"
          },
          "-",
          {
            text: "SEGUIMIENTO",
            cls: "my-btn",
            overCls: "my-over",
            scale: "medium",
            iconCls: "tracing",
            action: "addTraceEvaluationKri"
          },
          "->",
          {
            text: "DESCARGAR FORMATO",
            tooltip: "Descargar formato de evaluación",
            iconCls: "documentDownload",
            scale: "medium",
            cls: "my-btn",
            action: "downloadFormatEvaluation",
            overCls: "my-over"
          },
          {
            text: "PARAMETROS",
            tooltip: "Descargar parámetros de KRI",
            iconCls: "excel",
            scale: "medium",
            cls: "my-btn",
            action: "downloadKriParameter",
            overCls: "my-over"
          }
        ],
        columns: [
          {
            dataIndex: "description",
            flex: 3,
            header: "EVALUACIÓN"
          },
          {
            dataIndex: "descriptionFrequency",
            flex: 1,
            align: "center",
            header: "FRECUENCIA"
          },
          {
            dataIndex: "year",
            flex: 1,
            align: "center",
            header: "AÑO"
          },
          {
            dataIndex: "month",
            flex: 1,
            align: "center",
            header: "MES"
          },
          {
            dataIndex: "userRegister",
            flex: 3,
            header: "USUARIO QUE REGISTRO"
          }
        ],
        bbar: {
          xtype: "pagingtoolbar",
          pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
          store: storeEvKri,
          displayInfo: true,
          displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
          emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
        },
        listeners: {
          itemclick: function(grid, record, item, index) {
            Ext.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/getDetailEvaluationForKri.htm",
              params: {
                idKri: record.get("idKeyRiskIndicator"),
                normalizer: record.get("indicatorNormalizer"),
                yearMonth: record.get("yearMonth")
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  var result = new Ext.Component({
                    autoEl: "ul",
                    data: response.data,
                    listeners: {
                      el: {
                        delegate: ".list-row",
                        click: function(ev, li) {
                          Ext.fly(li).toggleCls("list-row-selected");
                        }
                      }
                    },
                    tpl: [
                      '<tpl for=".">',
                      '<li class="list-row" style="background-color:{color};">{descriptionQualification} <div>{value}</div></li>',
                      "</tpl>"
                    ],
                    cls: "green-box",
                    renderTo: Ext.getBody()
                  });
                  me.down("#containerResult").removeAll();
                  me.down("#containerResult").add(result);
                  me.down("#month").setValue(record.get("month"));
                  me.down("#year").setValue(record.get("year"));
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_WARNING,
                    response.message,
                    Ext.Msg.WARNING
                  );
                }
              },
              failure: function() {}
            });
          }
        }
      },
      {
        xtype: "container",
        region: "east",
        itemId: "containerResult",
        flex: 0.5
      }
    ];
    this.callParent();
  }
});
