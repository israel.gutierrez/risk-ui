Ext.define("ModelGridPanelRegisterEvaluationKri", {
  extend: "Ext.data.Model",
  fields: []
});

var StoreGridPanelRegisterEvaluationKri = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelRegisterEvaluationKri",
  autoLoad: false,

  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDetailScoreCard.htm",
    params: {},
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.kri.ViewPanelRegisterScorecardConfig", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelRegisterScorecardConfig",
  border: false,
  layout: "border",
  //  text: 'NUEVO',   action: 'entryScoreCardConfig'//,
  initComponent: function() {
    var me = this;

    this.items = [
      {
        xtype: "gridpanel",
        cls: "custom-grid",
        store: StoreGridPanelRegisterEvaluationKri,
        loadMask: true,
        columnLines: true,
        region: "center",
        flex: 2,
        padding: "2 2 2 2",
        selType: "cellmodel",
        features: [
          {
            ftype: "summary"
          }
        ],
        listeners: {
          render: function() {
            var me = this;
            me.store.getProxy().extraParams = {};
            me.store.getProxy().url =
              "http://localhost:9000/giro/loadGridDetailScoreCard.htm";
          }
        },
        plugins: [
          Ext.create("Ext.grid.plugin.CellEditing", {
            clicksToEdit: 1,
            listeners: {
              edit: function(editor, e, eOpts) {
                me.down("grid").store.data.items[e.rowIdx].set(
                  "text",
                  editor.editors.items[0].field.rawValue
                );
              }
            }
          })
        ],
        columns: []
      }
    ];

    this.callParent();
  }
});

function change(val, record) {
  if (record.get("colorByTime") == "") {
    return val;
  } else {
    return (
      '<div  style="background-color:#' +
      record.get("colorByTime") +
      ';"><span style="color:white;">' +
      val +
      " / " +
      record.get("percentage") +
      "%</span></div>"
    );
  }
}
