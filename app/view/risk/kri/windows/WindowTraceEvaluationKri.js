Ext.define("ModelTraceEvaluationKri", {
  extend: "Ext.data.Model",
  fields: [
    "idTracing",
    "yearMonth",
    "idKri",
    {
      name: "dateTracing",
      type: "date",
      format: "d/m/Y",
      convert: function(value) {
        return Ext.Date.parse(value, "d/m/Y");
      }
    },
    "description",
    "nameFile",
    "state",
    "indicatorAttachment"
  ]
});
var storeTraceEvaluation = Ext.create("Ext.data.Store", {
  model: "ModelTraceEvaluationKri",
  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("DukeSource.view.risk.kri.windows.WindowTraceEvaluationKri", {
  extend: "Ext.window.Window",
  alias: "widget.WindowTraceEvaluationKri",
  width: 900,
  title: "SEGUIMIENTO DE EVALUACIÓN",
  titleAlign: "center",
  layout: {
    type: "hbox",
    align: "stretch"
  },
  border: false,
  initComponent: function() {
    var me = this;
    var idKri = me.idKri;
    var yearMonth = me.yearMonth;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "panel",
          flex: 1,
          items: [
            {
              xtype: "form",
              itemId: "formRegisterTrace",
              border: false,
              layout: "anchor",
              bodyPadding: 5,
              items: [
                {
                  xtype: "textfield",
                  name: "idTracing",
                  itemId: "idTracing",
                  value: "id",
                  hidden: true
                },
                {
                  xtype: "textfield",
                  name: "idKri",
                  itemId: "idKri",
                  value: idKri,
                  hidden: true
                },
                {
                  xtype: "textfield",
                  name: "indicatorAttachment",
                  itemId: "indicatorAttachment",
                  value: "N",
                  hidden: true
                },
                {
                  xtype: "textfield",
                  name: "yearMonth",
                  itemId: "yearMonth",
                  value: yearMonth,
                  hidden: true
                },
                {
                  xtype: "datefield",
                  name: "dateTracing",
                  itemId: "dateTracing",
                  anchor: "100%",
                  labelAlign: "top",
                  allowBlank: false,
                  value: new Date(),
                  maxValue: new Date(),
                  format: "d/m/Y",
                  msgTarget: "side",
                  fieldCls: "obligatoryTextField",
                  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  fieldLabel: "FECHA"
                },
                {
                  xtype: "filefield",
                  anchor: "100%",
                  labelAlign: "top",
                  fieldLabel: "ADJUNTO",
                  name: "document",
                  itemId: "document",
                  buttonConfig: {
                    iconCls: "tesla even-attachment"
                  },
                  buttonText: ""
                },
                {
                  xtype: "UpperCaseTextArea",
                  anchor: "100%",
                  name: "description",
                  labelAlign: "top",
                  itemId: "description",
                  allowBlank: false,
                  maxLength: 1000,
                  height: 200,
                  msgTarget: "side",
                  fieldCls: "obligatoryTextField",
                  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  fieldLabel: "DESCRIPCION",
                  listeners: {
                    expand: function(field, value) {
                      field.setMaxValue(new Date());
                    }
                  }
                }
              ],
              buttons: [
                {
                  xtype: "button",
                  iconCls: "save",
                  scale: "medium",
                  cls: "my-btn",
                  overCls: "my-over",
                  text: "GUARDAR",
                  handler: function() {
                    var form = me.down("form");
                    if (form.getForm().isValid()) {
                      form.getForm().submit({
                        url: "http://localhost:9000/giro/saveTracingKri.htm",
                        waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                        method: "POST",
                        success: function(form, action) {
                          var valor = Ext.decode(action.response.responseText);
                          if (valor.success) {
                            me
                              .down("#gridTrackEvaluationKri")
                              .store.getProxy().extraParams = {
                              idKri: me.idKri,
                              yearMonth: me.yearMonth
                            };
                            me
                              .down("#gridTrackEvaluationKri")
                              .store.getProxy().url =
                              "http://localhost:9000/giro/showTracingEvaluationKri.htm";
                            me.down("#gridTrackEvaluationKri")
                              .down("pagingtoolbar")
                              .moveFirst();
                            me.down("form")
                              .getForm()
                              .reset();
                            me.down("#yearMonth").setValue(me.yearMonth);
                            me.down("#idKri").setValue(me.idKri);
                            DukeSource.global.DirtyView.messageAlert(
                              DukeSource.global.GiroMessages.TITLE_MESSAGE,
                              valor.message,
                              Ext.Msg.INFO
                            );
                          }
                        },
                        failure: function(form, action) {
                          var valor = Ext.decode(action.response.responseText);
                          if (!valor.success) {
                            DukeSource.global.DirtyView.messageAlert(
                              DukeSource.global.GiroMessages.TITLE_ERROR,
                              valor.message,
                              Ext.Msg.ERROR
                            );
                          }
                        }
                      });
                    } else {
                      DukeSource.global.DirtyView.messageAlert(
                        DukeSource.global.GiroMessages.TITLE_WARNING,
                        DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
                        Ext.Msg.ERROR
                      );
                    }
                  }
                }
              ],
              buttonAlign: "center"
            }
          ]
        },
        {
          xtype: "gridpanel",
          itemId: "gridTrackEvaluationKri",
          height: 350,
          columnLines: true,
          flex: 2,
          padding: "0 0 0 2",
          tbar: [
            {
              xtype: "button",
              iconCls: "add",
              cls: "my-btn",
              scale: "medium",
              overCls: "my-over",
              text: "NUEVO",
              handler: function() {
                me.down("form")
                  .getForm()
                  .reset();
                me.down("#idKri").setValue(idKri);
                me.down("#yearMonth").setValue(yearMonth);
              }
            },
            "-",
            {
              xtype: "button",
              cls: "my-btn",
              scale: "medium",
              overCls: "my-over",
              iconCls: "delete",
              text: "ELIMINAR",
              handler: function(btn) {
                var grid = me.down("#gridTrackEvaluationKri");
                var row = grid.getSelectionModel().getSelection()[0];
                if (row === undefined) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_WARNING,
                    DukeSource.global.GiroMessages.MESSAGE_ITEM,
                    Ext.Msg.WARNING
                  );
                } else {
                  Ext.MessageBox.show({
                    title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                    msg:
                      "Esta seguro que desea continuar?" +
                      " tenga en cuenta que se eliminara tambien,<br> " +
                      "el adjunto del seguimiento",
                    icon: Ext.Msg.WARNING,
                    buttonText: {
                      yes: "Si"
                    },
                    buttons: Ext.MessageBox.YESNO,
                    fn: function(btn) {
                      if (btn == "yes") {
                        DukeSource.lib.Ajax.request({
                          method: "POST",
                          url:
                            "http://localhost:9000/giro/deleteTracingEvaluationKri.htm?nameView=ViewPanelConfigMasterKri",
                          params: {
                            idTracing: row.get("idTracing")
                          },
                          success: function(response) {
                            response = Ext.decode(response.responseText);
                            if (response.success) {
                              DukeSource.global.DirtyView.messageAlert(
                                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                                response.message,
                                Ext.Msg.INFO
                              );
                              grid.down("pagingtoolbar").moveFirst();
                            } else {
                              DukeSource.global.DirtyView.messageAlert(
                                DukeSource.global.GiroMessages.TITLE_WARNING,
                                response.message,
                                Ext.Msg.ERROR
                              );
                            }
                          },
                          failure: function() {
                            DukeSource.global.DirtyView.messageAlert(
                              DukeSource.global.GiroMessages.TITLE_WARNING,
                              response.message,
                              Ext.Msg.ERROR
                            );
                          }
                        });
                      }
                    }
                  });
                }
              }
            }
          ],
          columns: [
            {
              xtype: "rownumberer",
              width: 25,
              sortable: true
            },
            {
              xtype: "actioncolumn",
              header: "ADJUNTO",
              align: "center",
              width: 80,
              dataIndex: "indicatorAttachment",
              items: [
                {
                  handler: function(grid, rowIndex) {
                    DukeSource.global.DirtyView.downloadFileAttachment(
                      grid,
                      rowIndex,
                      "http://localhost:9000/giro/downloadFileTracingKri.htm",
                      ["idTracing"]
                    );
                  }
                }
              ],
              renderer: function(value, meta, record) {
                if (value === "S") {
                  meta.tdCls = "documentDownload_grid";
                } else {
                  meta.tdCls = "";
                }
              }
            },
            {
              header: "ARCHIVO",
              width: 150,
              dataIndex: "nameFile",
              align: "center"
            },
            {
              header: "FECHA",
              width: 90,
              dataIndex: "dateTracing",
              format: "d/m/Y",
              renderer: Ext.util.Format.dateRenderer("d/m/Y")
            },
            {
              header: "DESCRIPCION SEGUIMIENTO",
              width: 600,
              dataIndex: "description"
            }
          ],
          viewConfig: {
            loadingText: false
          },
          store: storeTraceEvaluation,
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: storeTraceEvaluation,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            items: [
              "-",
              {
                text: "Exportar",
                iconCls: "excel",
                handler: function(b, e) {
                  b.up("grid").downloadExcelXml();
                }
              }
            ]
          },
          listeners: {
            itemdblclick: function(dv, record, item, index, e) {
              me.down("form")
                .getForm()
                .setValues(record.raw);
            }
          }
        }
      ],
      buttons: [
        {
          text: "SALIR",
          scope: this,
          scale: "medium",
          handler: this.close,
          iconCls: "logout"
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});
