Ext.define('ModelGridQualificationKRI', {
    extend: 'Ext.data.Model',
    fields: [
        'idQualificationEachKri'
        , 'description'
        , 'rangeInf'
        , 'rangeSup'
        , 'levelColour'
        , 'addRangeInf'
        , 'addRangeSup'
    ]
});

var StoreGridQualificationKRI = Ext.create('Ext.data.Store', {
    model: 'ModelGridQualificationKRI',
    autoLoad: false,
    proxy: {
        actionMethods: {
            create: 'POST',
            read: 'POST',
            update: 'POST'
        },
        type: 'ajax',
        url: 'http://localhost:9000/giro/loadGridDefault.htm',
        reader: {
            totalProperty: 'totalCount',
            root: 'data',
            successProperty: 'success'
        }
    }
});

var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
    clicksToEdit: 1
});

Ext.define('DukeSource.view.risk.kri.windows.ViewWindowEntryKri', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowEntryKri',
    border: false,
    modal: true,
    width: 670,
    layout: {
        type: 'fit'
    },
    title: 'Registro de KRI',

    setPeriod: function (me, f) {
        if (f.isValid()) {
            var numberDate;
            var cbo = me.down('#calculatingFrequency');
            var record = cbo.findRecord(cbo.valueField, cbo.getValue());

            var v = f.getValue();
            var month = v.getMonth() + 1;

            if (record.data['equivalentFrequency'] === 1) {
                numberDate = Ext.Date.getDayOfYear(v);
            }
            else if (record.data['equivalentFrequency'] === 7) {
                numberDate = Ext.Date.getWeekOfYear(v);
            }
            else {
                numberDate = Math.ceil(month / (record.data['equivalentFrequency'] / 30));
            }
            me.down('#initPeriod').setValue(numberDate);
            me.down('#initMonth').setValue(month);
            me.down('#initYear').setValue(v.getFullYear());
        }
    },
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    frame: true,
                    padding: 2,
                    bodyPadding: '0 5 0 5',
                    fieldDefaults: {
                        labelCls: 'changeSizeFontToEightPt',
                        fieldCls: 'changeSizeFontToEightPt'
                    },
                    items: [
                        {
                            xtype: 'container',
                            height: 26,
                            layout: {
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    name: 'idKeyRiskIndicator',
                                    value: 'id',
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'isModifiedQualification',
                                    itemId: 'isModifiedQualification',
                                    value: 'N',
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'indicatorEvaluation',
                                    itemId: 'indicatorEvaluation',
                                    value: 'N',
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'indicatorCriteria',
                                    itemId: 'indicatorCriteria',
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'idProcessType',
                                    name: 'idProcessType'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'idProcess',
                                    name: 'idProcess'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'idSubProcess',
                                    name: 'idSubProcess'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'idActivity',
                                    name: 'idActivity'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'codeProcess',
                                    name: 'codeProcess'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    itemId: 'factorRisk',
                                    name: 'factorRisk'
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'newLimit',
                                    itemId: 'newLimit',
                                    value: 'N',
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'jobPlace',
                                    itemId: 'jobPlace',
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'workArea',
                                    itemId: 'workArea',
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'initMonth',
                                    itemId: 'initMonth',
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'codeKri',
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'numberRiskAssociated',
                                    value: 0,
                                    hidden: true
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'codesRisk',
                                    value: '',
                                    hidden: true
                                },
                                {
                                    xtype: 'UpperCaseTextFieldObligatory',
                                    flex: 1,
                                    name: 'nameKri',
                                    itemId: 'nameKri',

                                    fieldLabel: 'Nombre KRI',
                                    fieldCls: 'obligatoryTextField',
                                    allowBlank: false,
                                    enforceMaxLength: true,
                                    maxLength: 500
                                },
                                {
                                    xtype: 'textfield',
                                    width: 200,
                                    padding: '0 0 0 10',
                                    name: 'abbreviation',
                                    fieldCls: 'obligatoryTextField',
                                    allowBlank: blank('WRI_TBX_Abbreviation'),
                                    hidden: hidden('WRI_TBX_Abbreviation'),
                                    fieldLabel: 'Abreviatura',
                                    enforceMaxLength: true,
                                    maxLength: 200
                                }
                            ]
                        },
                        {
                            xtype: 'textarea',
                            allowBlank: false,
                            fieldCls: 'obligatoryTextField',
                            anchor: '100%',
                            name: 'description',
                            fieldLabel: 'Definición del kri',
                            enforceMaxLength: true,
                            maxLength: 2000
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            name: 'remarks',
                            itemId: 'remarks',
                            allowBlank: blank('WRKRemarks'),
                            hidden: hidden('WRKRemarks'),
                            fieldLabel: getName('WRKRemarks'),
                            enforceMaxLength: true,
                            maxLength: 2000
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            name: 'tacticalTarget',
                            itemId: 'tacticalTarget',
                            allowBlank: blank('WRKTacticalTarget'),
                            hidden: hidden('WRKTacticalTarget'),
                            fieldLabel: getName('WRKTacticalTarget'),
                            enforceMaxLength: true,
                            maxLength: 2000
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            name: 'comments',
                            itemId: 'comments',
                            allowBlank: blank('IND_WRK_TXA_Comments'),
                            hidden: hidden('IND_WRK_TXA_Comments'),
                            fieldLabel: getName('IND_WRK_TXA_Comments'),
                            enforceMaxLength: true,
                            maxLength: 2000
                        },
                        {
                            xtype: 'fieldset',
                            title: '<b>Responsable</b>',
                            margin: 0,
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            padding: 5,
                            items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    height: 26,
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            name: 'managerRisk',
                                            hidden: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            fieldCls: 'obligatoryTextField',
                                            editable: false,
                                            flex: 1,
                                            padding: '0 1 0 0',
                                            allowBlank: false,
                                            name: 'fullName',
                                            fieldLabel: 'Asignado a'
                                        },
                                        {
                                            xtype: 'button',
                                            width: 25,
                                            name: 'buttonSearch',
                                            iconCls: 'search',
                                            handler: function () {
                                                var managerKriUser = Ext.create('DukeSource.view.risk.util.search.SearchUser', {
                                                    modal: true
                                                }).show();
                                                managerKriUser.down('grid').on('itemdblclick', function (view, record) {
                                                    me.down('textfield[name=managerRisk]').setValue(record.get('userName'));
                                                    me.down('textfield[name=fullName]').setValue(record.get('fullName'));
                                                    me.down('#jobPlace').setValue(record.get('idJobPlace'));
                                                    me.down('#descriptionJobPlace').setValue(record.get('descriptionJobPlace'));
                                                    me.down('#workArea').setValue(record.get('workArea'));
                                                    me.down('#descriptionWorkArea').setValue(record.get('descriptionWorkArea'));
                                                    me.down('#category').setValue(record.get('category'));
                                                    managerKriUser.close();
                                                });
                                            }
                                        },
                                        {
                                            xtype: 'textfield',
                                            padding: '0 0 0 5',
                                            fieldLabel: 'Categoría',
                                            flex: 1,
                                            name: 'category',
                                            itemId: 'category',
                                            allowBlank: false,
                                            readOnly: true,
                                            msgTarget: 'side',
                                            fieldCls: 'readOnlyText'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            name: 'descriptionJobPlace',
                                            itemId: 'descriptionJobPlace',
                                            flex: 1.1,
                                            allowBlank: false,
                                            readOnly: true,
                                            msgTarget: 'side',
                                            fieldCls: 'readOnlyText',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            fieldLabel: 'Cargo responsable'
                                        },
                                        {
                                            xtype: 'textfield',
                                            flex: 1,
                                            allowBlank: false,
                                            readOnly: true,
                                            padding: '0 0 0 5',
                                            msgTarget: 'side',
                                            queryMode: 'remote',
                                            fieldCls: 'readOnlyText',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'descriptionWorkArea',
                                            itemId: 'descriptionWorkArea',
                                            fieldLabel: 'Area responsable'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            title: '<b>Periodo</b>',
                            margin: 0,
                            layout: {
                                type: 'vbox',
                                align: 'stretch'
                            },
                            padding: 5,
                            items: [
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    height: 26,
                                    items: [
                                        {
                                            xtype: 'ViewComboCalculationFrequency',
                                            flex: 1.1,
                                            allowBlank: false,
                                            editable: false,
                                            msgTarget: 'side',
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'calculatingFrequency',
                                            itemId: 'calculatingFrequency',
                                            fieldLabel: 'Periodicidad',
                                            listeners: {
                                                select: function (cbo) {
                                                    var record = cbo.findRecord(cbo.valueField, cbo.getValue());
                                                    me.down('#abbreviationFrequency').setValue(record.raw.abbreviation);
                                                    me.down('#initPeriod').reset();
                                                    me.down('#initYear').reset();
                                                }
                                            }

                                        },
                                        {
                                            xtype: 'datefield',
                                            fieldLabel: 'Fecha de inicio',
                                            padding: '0 0 0 5',
                                            flex: 1,
                                            format: 'd/m/Y',
                                            name: 'dateInit',
                                            enableKeyEvents: true,
                                            plugins: [{
                                                ptype: 'afterlabelinfo',
                                                qtip: 'La fecha de inicio debe ser un periodo anterior al inicio' + '<br>' +
                                                    'de actividades del indicador'
                                            }],
                                            itemId: 'dateInit',
                                            listeners: {
                                                select: function (f, v) {
                                                    me.setPeriod(me, f);
                                                },
                                                keyup: function (f, e) {
                                                    me.setPeriod(me, f);
                                                }
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    layout: 'hbox',
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            fieldLabel: 'Reportar desde',
                                            maxValue: 12,
                                            allowBlank: false,
                                            readOnly: true,
                                            fieldCls: 'readOnlyText',
                                            name: 'abbreviationFrequency',
                                            itemId: 'abbreviationFrequency',
                                            flex: 0.4
                                        },
                                        {
                                            xtype: 'numberfield',
                                            maxLength: 4,
                                            fieldCls: 'readOnlyText',
                                            allowBlank: false,
                                            readOnly: true,
                                            fieldStyle: {
                                                textAlign: 'center'
                                            },
                                            name: 'initPeriod',
                                            itemId: 'initPeriod',
                                            flex: 0.2
                                        }, {
                                            xtype: 'numberfield',
                                            maxLength: 4,
                                            fieldCls: 'readOnlyText',
                                            readOnly: true,
                                            fieldStyle: {
                                                textAlign: 'center'
                                            },
                                            allowBlank: false,
                                            name: 'initYear',
                                            itemId: 'initYear',
                                            flex: 0.2
                                        }
                                    ]
                                }

                            ]
                        },
                        {
                            xtype: 'fieldset',
                            title: '<b>Proceso y otros</b>',
                            margin: 0,
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            padding: 5,
                            items: [
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            height: 18,
                                            fieldLabel: 'Factor de riesgo',
                                            hidden: hidden('IND_WRK_DescriptionFactorRisk'),
                                            itemId: 'descriptionFactorRisk',
                                            name: 'descriptionFactorRisk',
                                            value: '(Seleccionar)',
                                            fieldCls: 'style-for-url',
                                            listeners: {
                                                afterrender: function (view) {
                                                    view.getEl().on('click', function () {
                                                        Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeFactorRisk', {
                                                            winParent: me
                                                        }).show();
                                                    })
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'displayfield',
                                            fieldLabel: 'Proceso',
                                            height: 18,
                                            itemId: 'descriptionProcess',
                                            name: 'descriptionProcess',
                                            allowBlank: false,
                                            value: '(Seleccionar)',
                                            fieldCls: 'style-for-url',
                                            listeners: {
                                                afterrender: function (view) {
                                                    view.getEl().on('click', function () {
                                                        var window = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeProcess', {
                                                            backWindow: me
                                                        });
                                                        window.show();
                                                    })
                                                }
                                            }
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: {
                                        type: 'vbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'ViewComboKriMeasureUnit',
                                            flex: 1,
                                            allowBlank: false,
                                            msgTarget: 'side',
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'unitMeasure',
                                            fieldLabel: 'Un. medida',
                                            listeners: {
                                                specialkey: function (f, e) {
                                                    DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('combobox[itemId=formCalculating]'))

                                                }
                                            }
                                        },
                                        {
                                            xtype: "combobox",
                                            allowBlank: false,
                                            fieldCls: "obligatoryTextField",
                                            flex: 1,
                                            msgTarget: 'side',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            displayField: 'description',
                                            valueField: 'id',
                                            editable: false,
                                            name: 'formCalculating',
                                            itemId: 'formCalculating',
                                            fieldLabel: 'Fórmula',
                                            store: {
                                                fields: ['id', 'description'],
                                                data: [
                                                    // {"id": "1", "description": "Suma producto"},
                                                    {"id": "2", "description": "División"},
                                                    {"id": "3", "description": "Constante"}
                                                ]
                                            },
                                            listeners: {
                                                specialkey: function (c, d) {
                                                    DukeSource.global.DirtyView.focusEventEnterObligatory(c, d, me.down("ViewComboCalculationFrequency"))
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'ViewComboKriIndicator',
                                            flex: 1,
                                            allowBlank: false,
                                            hidden: true,
                                            msgTarget: 'side',
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            fieldLabel: 'Indicador',
                                            name: 'indicatorKri',
                                            listeners: {
                                                afterrender: function (cbo) {
                                                    cbo.setValue('RIESGO');
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'ViewComboKriCoverage',
                                            flex: 1,
                                            allowBlank: true,
                                            hidden: hidden('WRK_CB_TypeKri'),
                                            msgTarget: 'side',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'coverage',
                                            fieldLabel: 'Tipo',
                                            listeners: {
                                                afterrender: function (cbo) {
                                                    cbo.setValue('KRI');
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'ViewComboKriEvaluationMode',
                                            flex: 1,
                                            allowBlank: false,
                                            hidden: true,
                                            msgTarget: 'side',
                                            fieldCls: 'obligatoryTextField',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            name: 'modeEvaluation',
                                            fieldLabel: 'Modo ev.',
                                            listeners: {
                                                afterrender: function (cbo) {
                                                    cbo.setValue('CUANTITATIVO');
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'checkboxfield',
                                            flex: 1,
                                            allowBlank: true,
                                            hidden: true,
                                            fieldLabel: 'Normalizador',
                                            boxLabel: 'Aplica',
                                            name: 'indicatorNormalizer',
                                            inputValue: 'S',
                                            uncheckedValue: 'N',
                                            listeners: {
                                                specialkey: function (f, e) {
                                                    DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('button[action=saveConfigurationMasterKri]'))

                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            title: '<b>UMBRALES DE CALIFICACIÓN</b>',
                            margin: 0,
                            padding: 0,
                            border: false,
                            items: [
                                {

                                    xtype: 'gridpanel',
                                    height: 150,
                                    store: StoreGridQualificationKRI,
                                    columns: [
                                        {
                                            header: 'Nivel',
                                            sortable: false,
                                            dataIndex: 'description',
                                            align: 'center',
                                            flex: 1
                                        },
                                        {
                                            header: 'Límite inferior',
                                            dataIndex: 'rangeInf',
                                            sortable: false,
                                            align: 'center',
                                            flex: 1,
                                            editor: {
                                                xtype: 'numberfield',
                                                decimalPrecision: 4,
                                                allowBlank: true
                                            },
                                            tdCls: 'column-obligatory'
                                        },
                                        {
                                            header: '',
                                            dataIndex: 'addRangeInf',
                                            align: 'center',
                                            width: 50,
                                            sortable: false,
                                            editor: new Ext.form.field.ComboBox({
                                                name: 'addRangeInf',
                                                displayField: 'description',
                                                valueField: 'id',
                                                store: {
                                                    fields: ['id', 'description'],
                                                    data: [
                                                        {"id": ">", "description": ">"},
                                                        {"id": ">=", "description": ">="}
                                                    ]
                                                },
                                                lazyRender: true,
                                                listClass: 'x-combo-list-small'
                                            }),
                                            format: '0,0.00',
                                            tdCls: 'column-obligatory'
                                        },
                                        {
                                            header: '',
                                            dataIndex: 'addRangeSup',
                                            align: 'center',
                                            sortable: false,
                                            width: 50,
                                            editor: new Ext.form.field.ComboBox({
                                                name: 'addRangeSup',
                                                displayField: 'description',
                                                valueField: 'id',
                                                store: {
                                                    fields: ['id', 'description'],
                                                    data: [
                                                        {"id": "<", "description": "<"},
                                                        {"id": "<=", "description": "<="}
                                                    ]
                                                },
                                                lazyRender: true,
                                                listClass: 'x-combo-list-small'
                                            }),
                                            tdCls: 'column-obligatory',
                                            format: '0,0.00'
                                        },
                                        {
                                            header: 'Límite superior',
                                            dataIndex: 'rangeSup',
                                            sortable: false,
                                            align: 'center',
                                            flex: 1,
                                            editor: {
                                                xtype: 'numberfield',
                                                decimalPrecision: 4,
                                                allowBlank: true
                                            },
                                            tdCls: 'column-obligatory'
                                        },
                                        {
                                            dataIndex: 'levelColour',
                                            sortable: false,
                                            flex: 1,
                                            text: 'Color - Semáforo',
                                            renderer: function (value) {
                                                return '<div  style="background-color:#' + value + '; color:#' + value + ';"> ' + value + '</div>';
                                            }
                                        }
                                    ],
                                    plugins: [cellEditing],
                                    dockedItems: [
                                        {
                                            xtype: 'toolbar',
                                            dock: 'top',
                                            items: [
                                                {
                                                    xtype: 'button',
                                                    text: 'Nuevos umbrales',
                                                    iconCls: 'chart_line_edit',
                                                    handler: function (btn) {
                                                        Ext.MessageBox.show({
                                                            title: DukeSource.global.GiroMessages.TITLE_WARNING,
                                                            msg: DukeSource.global.GiroMessages.MESSAGE_KRI_NEW_LIMIT,
                                                            icon: Ext.Msg.WARNING,
                                                            buttonText: {
                                                                yes: "Si"
                                                            },
                                                            buttons: Ext.MessageBox.YESNO,
                                                            fn: function (btn) {
                                                                if (btn === 'yes') {
                                                                    me.down('#newLimit').setValue(DukeSource.global.GiroConstants.YES);
                                                                    var grid = me.down('grid');

                                                                    grid.store.getProxy().extraParams = {
                                                                        propertyOrder: 'idQualificationKri'
                                                                    };
                                                                    grid.store.getProxy().url = 'http://localhost:9000/giro/showListQualificationKriActives.htm';
                                                                    grid.store.load();
                                                                }
                                                            }
                                                        });

                                                    }
                                                }
                                            ]

                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    iconCls: 'save',
                    scale: 'medium',
                    action: 'saveConfigurationMasterKri'
                },
                {
                    text: 'Salir',
                    iconCls: 'logout',
                    scale: 'medium',
                    scope: this,
                    handler: this.close
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    },
    close: function () {
        if (this.fireEvent('beforeclose', this) !== false) {
            var me = this;
            if (me.actionType === 'new') {
                DukeSource.global.DirtyView.messageOut(me);
            }
            else {
                me.doClose();
            }
        }
    }
});