Ext.define('DukeSource.view.risk.kri.windows.ViewWindowNewRatingPeriodEvent', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowNewRatingPeriodEvent',
    layout: {
        align: 'stretch',
        type: 'vbox'
    },
    anchorSize: 100,
    title: 'SELECCIONAR PERIODO',
    titleAlign: 'center',
    width: 300,
    height: 130,
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    anchor: '100%',
                    margin: '2 2 0 2',
                    bodyPadding: '5',
                    layout: {
                        type: 'vbox'
                    },
                    items: [
                        {
                            xtype: 'ViewComboYear',
                            msgTarget: 'side',
                            fieldCls: 'obligatoryTextField',
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            fieldLabel: 'A&Ntilde;O:',
                            name: 'year'
                        },
                        {
                            xtype: 'ViewComboMonth',
                            allowBlank: false,
                            msgTarget: 'side',
                            fieldCls: 'obligatoryTextField',
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            fieldLabel: 'MES',
                            name: 'month'
                        }
                    ]
                }

            ],
            buttons: [
                {
                    text: 'ACEPTAR',
                    tooltip: 'ACEPTAR',
                    action: 'newRantingPeriod',
                    scope: this,
                    handler: this.close
                },
                {
                    text: 'CANCELAR',
                    iconCls: '',
                    scope: this,
                    handler: this.close
                }
            ]
        });

        me.callParent(arguments);
    }

});
