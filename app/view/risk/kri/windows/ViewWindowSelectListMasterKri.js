var sm = new Ext.selection.CheckboxModel({
  checkOnly: true
});

Ext.define("ModelSelectListMasterKri", {
  extend: "Ext.data.Model",
  fields: [
    "idKeyRiskIndicator",
    "codeKri",
    "nameKri",
    "description",
    "abbreviation"
  ]
});

var storeSelectListMasterKri = Ext.create("Ext.data.Store", {
  model: "ModelSelectListMasterKri",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.kri.windows.ViewWindowSelectListMasterKri", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowSelectListMasterKri",
  layout: {
    align: "stretch",
    type: "vbox"
  },
  anchorSize: 100,
  title: "SELECCIONAR KRIS",
  titleAlign: "center",
  width: 900,
  height: 600,
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "panel",
          flex: 3,
          padding: "2 2 2 2",
          layout: {
            align: "stretch",
            type: "vbox"
          },
          items: [
            {
              xtype: "form",
              anchor: "100%",
              margin: "2 2 0 2",
              bodyPadding: "5",
              layout: {
                type: "vbox"
              },
              items: [
                {
                  xtype: "container",
                  flex: 3,
                  padding: "2 2 2 2",
                  layout: {
                    align: "stretch",
                    type: "vbox"
                  },
                  items: [
                    {
                      xtype: "UpperCaseTextFieldObligatory",
                      padding: "0 2 0 0",
                      labelWidth: 180,
                      fieldLabel: "NOMBRE CUADRO DE MANDO:",
                      name: "newDescriptionScorecard",
                      width: 730,
                      flex: 6
                    }
                  ]
                },
                {
                  xtype: "container",
                  flex: 3,
                  padding: "2 2 2 2",
                  layout: {
                    align: "stretch",
                    type: "hbox"
                  },
                  items: [
                    {
                      xtype: "datefield",
                      format: "d/m/Y",
                      name: "dateInitialWindow",
                      labelWidth: 104,
                      padding: "0 2 0 76",
                      fieldLabel: "FECHA VIGENCIA:",
                      //                                            displayField:validityPeriod,
                      allowBlank: false,
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      fieldCls: "obligatoryTextField",
                      listeners: {
                        select: function(field, value) {
                          me.down(
                            "datefield[name=dateFinalWindow]"
                          ).setMinValue(value);
                        }
                      }
                    },
                    {
                      xtype: "datefield",
                      format: "d/m/Y",
                      //                                            itemId:'dateFinalWindow',
                      name: "dateFinalWindow",
                      labelWidth: 120,
                      padding: "0 2 0 50",
                      fieldLabel: "FECHA CADUCIDAD:",
                      allowBlank: false,
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      fieldCls: "obligatoryTextField",
                      listeners: {
                        select: function(field, value) {
                          me.down(
                            "datefield[name=dateInitialWindow]"
                          ).setMaxValue(value);
                        }
                      }
                    }
                  ]
                }
              ]
            },

            {
              xtype: "gridpanel",
              padding: "0 1 0 0",
              store: storeSelectListMasterKri,
              flex: 1,
              titleAlign: "center",
              selModel: sm,
              loadMask: true,
              columnLines: true,
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false
                },
                {
                  dataIndex: "idKeyRiskIndicator",
                  hidden: true
                },
                {
                  dataIndex: "codeKri",
                  width: 100,
                  align: "center",
                  text: "CODIGO"
                },
                {
                  dataIndex: "abbreviation",
                  flex: 2,
                  text: "ABREVIATURA"
                },
                {
                  dataIndex: "nameKri",
                  flex: 5,
                  text: "KRI"
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: storeSelectListMasterKri,
                displayInfo: true,
                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              },

              listeners: {
                render: function() {
                  var me = this;
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    me,
                    me.down("pagingtoolbar"),
                    "http://localhost:9000/giro/showListKeyRiskIndicatorActives.htm",
                    "",
                    "idKeyRiskIndicator"
                  );
                }
              }
            }
          ],
          buttons: [
            {
              //                        xtype: 'button',
              text: "ACEPTAR",
              iconCls: "save",
              action: "selectListKris"
            },
            {
              //                        xtype: 'button',
              text: "SALIR",
              scope: this,
              handler: this.close,
              iconCls: "logout"
            }
          ]
        }
      ]
    });

    me.callParent(arguments);
  }
});
