Ext.define("ModelGridDetailWeighingMasterKri", {
  extend: "Ext.data.Model",
  fields: [
    "idQuantityValuesWeights",
    "idWeighingKri",
    "description",
    "state",
    "equivalentValue"
  ]
});

var StoreGridDetailWeighingMasterKri = Ext.create("Ext.data.Store", {
  model: "ModelGridDetailWeighingMasterKri",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.risk.kri.windows.ViewWindowDetailWeighingMasterKri",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowDetailWeighingMasterKri",
    height: 400,
    width: 479,
    border: false,
    layout: {
      type: "fit"
    },
    title: "PONDERADOR",
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "container",
            layout: {
              align: "stretch",
              type: "vbox"
            },
            items: [
              {
                xtype: "form",
                bodyPadding: 10,
                items: [
                  {
                    xtype: "textfield",
                    name: "idKeyRiskIndicator",
                    hidden: true
                  },
                  {
                    xtype: "textfield",
                    value: "id",
                    name: "idWeighingKri",
                    hidden: true
                  },
                  {
                    xtype: "UpperCaseTextFieldObligatory",
                    anchor: "100%",
                    fieldCls: "obligatoryTextField",
                    name: "description",
                    itemId: "description",
                    fieldLabel: "PONDERADOR"
                  },
                  {
                    xtype: "NumberDecimalNumberObligatory",
                    anchor: "100%",
                    fieldCls: "obligatoryTextField",
                    name: "weighingMatrix",
                    fieldLabel: "VALOR"
                  }
                ]
              },
              {
                xtype: "gridpanel",
                flex: 1,
                padding: "2 0 0 0",
                store: StoreGridDetailWeighingMasterKri,
                title: "DETALLE PONDERADOR",
                titleAlign: "center",
                plugins: [
                  Ext.create("Ext.grid.plugin.RowEditing", {
                    clicksToMoveEditor: 1,
                    saveBtnText: "GUARDAR",
                    cancelBtnText: "CANCELAR",
                    autoCancel: false
                  })
                ],

                columns: [
                  { xtype: "rownumberer", width: 25, sortable: false },
                  {
                    header: "ESCALA",
                    dataIndex: "idQuantityValuesWeights",
                    flex: 1,
                    editor: {
                      xtype: "TextFieldNumberFormatObligatory",
                      allowBlank: false
                    }
                  },
                  {
                    header: "DESCRIPCION",
                    dataIndex: "description",
                    flex: 1,
                    editor: {
                      xtype: "UpperCaseTextFieldObligatory",
                      allowBlank: false
                    }
                  }
                ],
                tbar: [
                  {
                    text: "NUEVO",
                    iconCls: "new",
                    handler: function(button) {
                      var modelAgency = Ext.create(
                        "ModelGridDetailWeighingMasterKri",
                        {
                          idQuantityValuesWeights: "",
                          description: "",
                          idWeighingKri: "id",
                          state: "S"
                        }
                      );
                      var window = Ext.ComponentQuery.query(
                        "ViewWindowDetailWeighingMasterKri"
                      )[0];
                      var panel = window.down("grid");
                      var grid = panel;
                      var editor = panel.editingPlugin;
                      editor.cancelEdit();
                      grid.getStore().insert(0, modelAgency);
                      editor.startEdit(0, 0);
                    }
                  }
                ],
                listeners: {
                  render: function() {
                    var me = this;
                    DukeSource.global.DirtyView.loadGridDefault(me);
                  }
                }
              }
            ]
          }
        ],
        buttons: [
          {
            text: "GUARDAR",
            scale: "medium",
            action: "saveDetailWeighingKri",
            iconCls: "save"
          },
          {
            text: "SALIR",
            scale: "medium",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      });

      me.callParent(arguments);
    }
  }
);
