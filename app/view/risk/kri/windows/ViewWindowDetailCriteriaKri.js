Ext.define("DukeSource.view.risk.kri.windows.ViewWindowDetailCriteriaKri", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowDetailCriteriaKri",
  width: 479,
  border: false,
  layout: {
    type: "fit"
  },
  title: "Detalle criterio KRI",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          items: [
            {
              xtype: "textfield",
              itemId: "id",
              name: "id",
              hidden: true
            },
            {
              xtype: "UpperCaseTextFieldObligatory",
              anchor: "100%",
              itemId: "text",
              name: "text",
              fieldLabel: "Descripción"
            },
            {
              xtype: "textfield",
              anchor: "100%",
              itemId: "fieldName",
              name: "fieldName",
              fieldLabel: "Identificador"
            },
            {
              xtype: "combobox",
              anchor: "100%",
              fieldLabel: "Tipo",
              editable: false,
              name: "typeCriteria",
              itemId: "typeCriteria",
              fieldCls: "obligatoryTextField",
              queryMode: "local",
              displayField: "description",
              valueField: "value",
              store: {
                fields: ["value", "description"],
                pageSize: 9999,
                autoLoad: true,
                proxy: {
                  actionMethods: {
                    create: "POST",
                    read: "POST",
                    update: "POST"
                  },
                  type: "ajax",
                  url:
                    "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
                  extraParams: {
                    propertyOrder: "description",
                    propertyFind: "identified",
                    valueFind: "TYPE_CRITERIA_KRI"
                  },
                  reader: {
                    type: "json",
                    root: "data",
                    successProperty: "success"
                  }
                }
              },
              listeners: {
                render: function(cbo) {
                  cbo.setValue("G");
                }
              }
            }
          ]
        }
      ],
      buttons: [
        {
          text: "Guardar",
          scale: "medium",
          action: "saveCriteriaKri",
          iconCls: "save"
        },
        {
          text: "Salir",
          scale: "medium",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});
