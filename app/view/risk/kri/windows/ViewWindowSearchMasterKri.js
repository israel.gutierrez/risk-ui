Ext.define("ModelSearchMasterKri", {
  extend: "Ext.data.Model",
  fields: [
    "idKeyRiskIndicator",
    "process",
    "subProcess",
    "workArea",
    "factorRisk",
    "managerRisk",
    "calculatingFrequency",
    "abbreviationFrequency",
    "codeKri",
    "nameKri",
    "fullName",
    "description",
    "abbreviation",
    "remarks",
    "formCalculating",
    "indicatorKri",
    "coverage",
    "modeEvaluation",
    "unitMeasure",
    "state",
    "indicatorCriteria",
    "year",
    "month",
    "yearMonth",
    "indicatorConfiguration",
    "indicatorNormalizer",
    "indicatorQualification"
  ]
});

var storeSearchMasterKri = Ext.create("Ext.data.Store", {
  model: "ModelSearchMasterKri",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.kri.windows.ViewWindowSearchMasterKri", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowSearchMasterKri",
  layout: {
    align: "stretch",
    type: "vbox"
  },
  title: "Buscar KRI",
  width: 800,
  border: false,
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          height: 45,
          bodyPadding: 10,
          items: [
            {
              xtype: "container",
              anchor: "100%",
              height: 26,
              layout: {
                type: "hbox"
              },
              items: [
                {
                  xtype: "combobox",
                  value: "2",
                  labelWidth: 40,

                  width: 190,
                  fieldLabel: "Tipo",
                  store: [
                    ["1", "Código"],
                    ["2", "Descripción"]
                  ]
                },
                {
                  xtype: "UpperCaseTextField",
                  flex: 2,
                  listeners: {
                    specialkey: function(field, e) {
                      var property = "";
                      if (
                        Ext.ComponentQuery.query(
                          "ViewWindowSearchMasterKri combobox"
                        )[0].getValue() == "1"
                      ) {
                        property = "abbreviation";
                      } else {
                        property = "nameKri";
                      }
                      if (e.getKey() === e.ENTER) {
                        var grid = me.down("grid");
                        var toolbar = grid.down("pagingtoolbar");
                        DukeSource.global.DirtyView.searchPaginationGridToEnter(
                          field,
                          grid,
                          grid.down("pagingtoolbar"),
                          "http://localhost:9000/giro/findKeyRiskIndicator.htm",
                          property,
                          "description"
                        );
                      }
                    }
                  }
                },
                {
                  xtype: "datefield",
                  name: "dateStart",
                  format: "d/m/Y",
                  allowBlank: false,
                  disabled: true,
                  hidden: true,
                  msgTarget: "side",
                  fieldCls: "obligatoryTextField",
                  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  padding: "0 10 0 10",
                  labelWidth: 50,
                  fieldLabel: "INICIO",
                  width: 160
                },
                {
                  xtype: "datefield",
                  name: "dateLimit",
                  format: "d/m/Y",
                  allowBlank: false,
                  disabled: true,
                  hidden: true,
                  msgTarget: "side",
                  fieldCls: "obligatoryTextField",
                  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  padding: "0 10 0 0",
                  labelWidth: 50,
                  fieldLabel: "FIN",
                  width: 160
                },
                {
                  xtype: "checkbox",
                  fieldLabel: "Normalizado",
                  hidden: true,
                  name: "normalized",
                  labelAlign: "left",
                  boxLabelAlign: "left",
                  itemId: "normalized",
                  inputValue: "S",
                  flex: 1.2,
                  checked: true,
                  uncheckedValue: "N",
                  listeners: {
                    change: function(cb) {
                      cb.setFieldLabel(
                        cb.getValue() == true ? "NORMALIZADO" : "SIN NORMALIZAR"
                      );
                    }
                  }
                }
              ]
            }
          ]
        },
        {
          xtype: "container",
          flex: 3,
          height: 400,
          padding: "2 2 2 2",
          layout: {
            align: "stretch",
            type: "hbox"
          },
          items: [
            {
              xtype: "gridpanel",
              padding: "0 1 0 0",
              loadMask: true,
              columnLines: true,
              store: storeSearchMasterKri,
              flex: 1,
              titleAlign: "center",
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false
                },
                {
                  dataIndex: "codeKri",
                  width: 100,
                  align: "center",
                  text: "Código"
                },
                {
                  dataIndex: "nameKri",
                  flex: 5,
                  text: "KRI"
                },
                {
                  dataIndex: "fullName",
                  flex: 3,
                  text: "Responsable"
                },
                {
                  dataIndex: "indicatorConfiguration",
                  width: 90,
                  align: "center",
                  text: "Configuración terminada?",
                  renderer: function(value, metaData) {
                    if (value === "S") {
                      metaData.tdCls = "finish-cell";
                      return value === "N" ? "No" : "Si";
                    }

                    metaData.tdCls = "pending-cell";
                    return value === "N" ? "No" : "Si";
                  }
                },
                {
                  dataIndex: "indicatorQualification",
                  width: 70,
                  hidden: true,
                  align: "center",
                  text: "Tiene evaluaciones?",
                  renderer: function(value, metaData) {
                    if (value === "S") {
                      metaData.tdCls = "finish-cell";
                      return value === "N" ? "No" : "Si";
                    }

                    metaData.tdCls = "pending-cell";
                    return value === "N" ? "No" : "Si";
                  }
                },
                {
                  dataIndex: "indicatorNormalizer",
                  width: 70,
                  hidden: true,
                  align: "center",
                  text: "ACEPTA<br>NORMALIZ.",
                  renderer: function(value, metaData) {
                    if (value === "S") {
                      metaData.tdCls = "finish-cell";
                      return value === "N" ? "No" : "Si";
                    }

                    metaData.tdCls = "pending-cell";
                    return value === "N" ? "No" : "Si";
                  }
                },
                {
                  dataIndex: "yearMonth",
                  width: 70,
                  align: "center",
                  text: "Fecha de última evaluación",
                  renderer: function(value, metaData) {
                    if (value.length == undefined) {
                      var month = value.toString().substring(4);
                      var year = value.toString().substring(4, 0);
                      value = month + "-" + year;
                    }
                    return value;
                  }
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: storeSearchMasterKri,
                displayInfo: true,
                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              },
              listeners: {
                render: function() {
                  var me = this;
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    me,
                    me.down("pagingtoolbar"),
                    "http://localhost:9000/giro/showListKeyRiskIndicatorActives.htm",
                    "",
                    "idKeyRiskIndicator"
                  );
                }
              }
            }
          ]
        }
      ]
    });

    me.callParent(arguments);
  }
});
