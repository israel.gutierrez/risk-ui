Ext.define("DukeSource.view.risk.kri.windows.WindowDetailEvaluationKri", {
  extend: "Ext.window.Window",
  alias: "widget.WindowDetailEvaluationKri",
  height: 500,
  width: 1100,
  border: false,
  layout: {
    type: "fit"
  },
  requires: [
    "Ext.ux.grid.feature.MultiGroupingSummary",
    "Ext.ux.grid.feature.MultiGrouping"
  ],
  title: "DETALLE KRI",
  titleAlign: "center",
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [],
      buttons: [
        {
          text: "SALIR",
          scope: this,
          scale: "medium",
          handler: this.close,
          iconCls: "logout"
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});
