Ext.require(["Ext.ux.CheckColumn"]);
Ext.require(["Ext.ux.CheckCombo"]);
Ext.define("ModelGridPanelCriteriaKri", {
  extend: "Ext.data.Model",
  fields: [
    "idDetailCriteriaKri",
    "weighingNormalizer",
    "idNormalizerKri",
    "normalizerKri",
    { name: "bestValue", type: "number" },
    { name: "worstValue", type: "number" },
    "criteriaEvaluationKri",
    "descriptionDetailCriteria",
    "descriptionCriteria",
    "mainFilter"
  ]
});

var StoreGridPanelCriteriaKri = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelCriteriaKri",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("ModelGridPanelWeighingKri", {
  extend: "Ext.data.Model",
  fields: [
    "idWeighingKri",
    "idKeyRiskIndicator",
    "description",
    "fieldName",
    "weighingMatrix",
    "typeColumn"
  ]
});

var StoreGridPanelWeighingKri = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelWeighingKri",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.kri.windows.WindowDetailConfigKri", {
  extend: "Ext.window.Window",
  alias: "widget.WindowDetailConfigKri",
  width: 550,
  height: 600,
  border: false,
  layout: {
    type: "fit"
  },
  title: "Ponderadores",
  modal: true,
  initComponent: function() {
    var me = this;
    var record = this.record;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          layout: {
            align: "stretch",
            type: "vbox"
          },
          border: false,
          items: [
            {
              xtype: "fieldset",
              padding: 5,
              margin: 5,
              title: "Información del KRI",
              layout: "hbox",
              border: 2,
              style: {
                borderColor: "#78abf5"
              },
              height: 60,
              items: [
                {
                  xtype: "textfield",
                  flex: 3,
                  fieldLabel: "Nombre",
                  name: "nameKri",
                  itemId: "nameKri",
                  fieldCls: "readOnlyText",
                  readOnly: true
                },
                {
                  xtype: "textfield",
                  fieldLabel: "Código",
                  padding: "0 0 0 5",
                  readOnly: true,
                  labelWidth: 60,
                  flex: 1,
                  fieldCls: "codeIncident",
                  name: "codeKri",
                  itemId: "codeKri"
                }
              ]
            },
            {
              xtype: "gridpanel",
              name: "detailCriteriaKri",
              store: StoreGridPanelCriteriaKri,
              flex: 1,
              title: "Criterio KRI",
              titleAlign: "center",
              selType: "cellmodel",
              selModel: Ext.create("Ext.selection.CheckboxModel"),
              plugins: [
                Ext.create("Ext.grid.plugin.CellEditing", {
                  clicksToEdit: 1
                })
              ],
              columns: [],
              tbar: [
                {
                  text: "Nuevo",
                  name: "newDetailCriteria",
                  iconCls: "add",
                  handler: function(button) {
                    var win = Ext.create(
                      "DukeSource.view.risk.kri.windows.ViewWindowCriteriaMasterKri",
                      {
                        modal: true
                      }
                    ).show();
                    win.down("#text").focus(false, 300);
                  }
                },
                {
                  text: "Agregar criterio",
                  name: "addDetailCriteria",
                  hidden: true,
                  iconCls: "import",
                  handler: function(button) {
                    var grid1 = me.down("grid[name=detailCriteriaKri]");
                    var records = grid1.getStore().getRange();
                    if (
                      Ext.ComponentQuery.query(
                        "ViewWindowAddCriteriaMasterKri"
                      )[0] === undefined
                    ) {
                      var win = Ext.create(
                        "DukeSource.view.risk.kri.windows.ViewWindowAddCriteriaMasterKri",
                        {}
                      ).show();
                      win.down("grid").store.getProxy().extraParams = {
                        idKeyRiskIndicator: record.get("idKeyRiskIndicator"),
                        criteriaEvaluationKri: records[0].get(
                          "criteriaEvaluationKri"
                        )
                      };
                      win.down("grid").store.getProxy().url =
                        "http://localhost:9000/giro/getListNormalizerKriNewCriteria.htm";
                      win
                        .down("grid")
                        .down("pagingtoolbar")
                        .moveFirst();
                    }
                  }
                },
                {
                  text: "Eliminar",
                  action: "deleteDetailCriteriaKri",
                  iconCls: "delete"
                },
                {
                  text: "Guardar",
                  action: "saveDetailCriteriaKri",
                  iconCls: "save"
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: StoreGridPanelCriteriaKri,
                displayInfo: true,
                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              },
              listeners: {
                render: function(win) {
                  if (record.get("indicatorNormalizer") === "N") {
                    var description = Ext.create("Ext.grid.column.Column", {
                      dataIndex: "descriptionDetailCriteria",
                      flex: 2.2,
                      text: "Criterio"
                    });
                    var weight = Ext.create("Ext.grid.column.Column", {
                      text: "Peso",
                      dataIndex: "weighingNormalizer",
                      align: "center",
                      flex: 0.6,
                      tdCls: "custom-column",
                      editor: {
                        xtype: "textfield",
                        allowBlank: false
                      }
                    });
                    var filter = Ext.create("Ext.grid.column.Column", {
                      text: "Filtro",
                      dataIndex: "mainFilter",
                      align: "center",
                      flex: 0.6,
                      hidden: true,
                      tdCls: "custom-column",
                      editor: new Ext.form.field.ComboBox({
                        typeAhead: true,
                        triggerAction: "all",
                        readOnly: false,
                        selectOnTab: true,
                        allowBlank: true,
                        store: [
                          ["S", "SI"],
                          ["N", "NO"]
                        ],
                        lazyRender: true,
                        listClass: "x-combo-list-small"
                      })
                    });
                    win.headerCt.removeAll();
                    win.headerCt.insert(1, description);
                    win.headerCt.insert(2, filter);
                    win.headerCt.insert(3, weight);
                    win.getView().refresh();
                  } else if (record.get("indicatorNormalizer") === "S") {
                    description = Ext.create("Ext.grid.column.Column", {
                      dataIndex: "descriptionDetailCriteria",
                      flex: 2.2,
                      text: "Criterio"
                    });
                    weight = Ext.create("Ext.grid.column.Column", {
                      text: "PESO",
                      dataIndex: "weighingNormalizer",
                      align: "center",
                      flex: 0.6,
                      tdCls: "custom-column",
                      editor: {
                        xtype: "textfield",
                        allowBlank: false
                      }
                    });
                    var normalizer = Ext.create("Ext.grid.column.Column", {
                      text: "Normalizador",
                      dataIndex: "normalizerKri",
                      align: "center",
                      flex: 1
                    });
                    var valueMin = Ext.create("Ext.grid.column.Column", {
                      text: "VAL.MIN.",
                      dataIndex: "bestValue",
                      align: "center",
                      flex: 0.6,
                      tdCls: "custom-column",
                      editor: {
                        xtype: "textfield",
                        allowBlank: false
                      }
                    });
                    var valueMax = Ext.create("Ext.grid.column.Column", {
                      text: "VAL.MAX.",
                      dataIndex: "worstValue",
                      align: "center",
                      flex: 0.6,
                      tdCls: "custom-column",
                      editor: {
                        xtype: "textfield",
                        allowBlank: false
                      }
                    });
                    win.headerCt.removeAll();
                    win.headerCt.insert(1, description);
                    win.headerCt.insert(2, weight);
                    win.headerCt.insert(3, normalizer);
                    win.headerCt.insert(4, valueMin);
                    win.headerCt.insert(5, valueMax);
                    win.getView().refresh();
                  }
                }
              }
            },
            {
              xtype: "gridpanel",
              name: "detailWeighingKri",
              store: StoreGridPanelWeighingKri,
              flex: 1,
              padding: "2 0 0 0",
              title: "Ponderador kri",
              titleAlign: "center",
              columns: [
                {
                  dataIndex: "idWeighingKri",
                  flex: 1,
                  align: "center",
                  text: "Código"
                },
                {
                  dataIndex: "description",
                  flex: 5,
                  text: "Ponderador"
                },
                {
                  dataIndex: "typeColumn",
                  flex: 2,
                  text: "Columna",
                  renderer: function(value) {
                    if (value === "N") {
                      return "NUMERADOR";
                    } else if (value === "D") {
                      return "DENOMINADOR";
                    } else if (value === "I" || value === "S") {
                      return "INDICADOR";
                    } else if (value === "C") {
                      return "CONSTANTE";
                    }
                  }
                },
                {
                  dataIndex: "weighingMatrix",
                  align: "center",
                  flex: 1,
                  text: "PESO"
                }
              ],
              tbar: [
                {
                  text: "Nuevo",
                  iconCls: "add",
                  handler: function(button) {
                    var idKri = record.get("idKeyRiskIndicator");
                    var win;

                    var gridDetailKri = me.down("grid[name=detailWeighingKri]");
                    if (record.get("modeEvaluation") === "CUANTITATIVO") {
                      if (record.get("formCalculating") === "1") {
                        win = Ext.create(
                          "DukeSource.view.risk.kri.windows.ViewWindowWeighingMasterKri",
                          {}
                        );
                        win.down("#idKeyRiskIndicator").setValue(idKri);
                        win.show();
                        win.down("#description").focus(false, 300);
                      } else if (record.get("formCalculating") === "2") {
                        if (gridDetailKri.getStore().getTotalCount() === 0) {
                          win = Ext.create(
                            "DukeSource.view.risk.kri.windows.ViewWindowWeighingMasterKri",
                            {}
                          );
                          win.down("#idKeyRiskIndicator").setValue(idKri);
                          win.down("form").add({
                            xtype: "combobox",
                            fieldCls: "readOnlyText",
                            displayField: "description",
                            valueField: "id",
                            name: "typeColumn",
                            value: "N",
                            readOnly: true,
                            fieldLabel: "Posición",
                            store: {
                              fields: ["id", "description"],
                              data: [{ description: "NUMERADOR", id: "N" }]
                            }
                          });
                          win.show();
                          win.down("#description").focus(false, 300);
                        } else if (
                          gridDetailKri.getStore().getTotalCount() === 1
                        ) {
                          win = Ext.create(
                            "DukeSource.view.risk.kri.windows.ViewWindowWeighingMasterKri",
                            {}
                          );
                          win.down("#idKeyRiskIndicator").setValue(idKri);
                          win.down("form").add({
                            xtype: "combobox",
                            fieldCls: "readOnlyText",
                            displayField: "description",
                            valueField: "id",
                            name: "typeColumn",
                            value: "D",
                            readOnly: true,
                            fieldLabel: "Posición",
                            store: {
                              fields: ["id", "description"],
                              data: [{ description: "DENOMINADOR", id: "D" }]
                            }
                          });
                          win.show();
                          win.down("#description").focus(false, 300);
                        } else {
                          Ext.MessageBox.show({
                            title: DukeSource.global.GiroMessages.TITLE_WARNING,
                            msg:
                              "El numerador y denominador están completos, desea ingresar alguna constante?",
                            icon: Ext.Msg.QUESTION,
                            buttonText: {
                              yes: "Si"
                            },
                            buttons: Ext.MessageBox.YESNO,
                            fn: function(t) {
                              if (t === "yes") {
                                win = Ext.create(
                                  "DukeSource.view.risk.kri.windows.ViewWindowWeighingMasterKri",
                                  {}
                                );
                                win.down("#idKeyRiskIndicator").setValue(idKri);
                                win.down("form").add({
                                  xtype: "combobox",
                                  anchor: "100%",
                                  readOnly: true,
                                  fieldCls: "readOnlyText",
                                  displayField: "description",
                                  valueField: "id",
                                  name: "typeColumn",
                                  value: "C",
                                  fieldLabel: "Constante",
                                  store: {
                                    fields: ["id", "description"],
                                    data: [
                                      { description: "CONSTANTE", id: "C" }
                                    ]
                                  }
                                });
                                win.show();
                                win.down("#description").focus(false, 300);
                              }
                              if (t === "no") {
                                e.close();
                              }
                            }
                          });
                        }
                      } else {
                        if (gridDetailKri.getStore().getTotalCount() === 0) {
                          win = Ext.create(
                            "DukeSource.view.risk.kri.windows.ViewWindowWeighingMasterKri",
                            {}
                          );
                          win.down("#idKeyRiskIndicator").setValue(idKri);
                          win.down("form").add({
                            xtype: "combobox",
                            readOnly: true,
                            fieldCls: "readOnlyText",
                            displayField: "description",
                            valueField: "id",
                            name: "typeColumn",
                            value: "I",
                            fieldLabel: "Ponderador",
                            store: {
                              fields: ["id", "description"],
                              data: [{ description: "INDICADOR", id: "I" }]
                            }
                          });
                          win.show();
                          win.down("#description").focus(false, 300);
                        } else {
                          /*win = Ext.create('DukeSource.view.risk.kri.windows.ViewWindowWeighingMasterKri',
                                                     {});
                                                     win.down('#idKeyRiskIndicator').setValue(idKri);
                                                     win.down('form').add({
                                                     xtype: 'combobox',
                                                     anchor: '100%',
                                                     readOnly: true,
                                                     fieldCls: 'readOnlyText',
                                                     displayField: 'description',
                                                     valueField: 'id',
                                                     name: 'typeColumn',
                                                     value: 'C',
                                                     fieldLabel: 'CONSTANTE',
                                                     store: {
                                                     fields: ['id', 'description'],
                                                     data: [
                                                     {description: 'CONSTANTE', id: 'C'}
                                                     ]
                                                     }
                                                     });
                                                     win.show();
                                                     in.down('#description').focus(false, 300);*/
                          DukeSource.global.DirtyView.messageWarning(
                            "El indicador ya fue agregado"
                          );
                        }
                      }
                    } else if (record.get("modeEvaluation") === "CUALITATIVO") {
                      win = Ext.create(
                        "DukeSource.view.risk.kri.windows.ViewWindowDetailWeighingMasterKri",
                        {}
                      );
                      win.down("#idKeyRiskIndicator").setValue(idKri);
                      win.show();
                      win.down("#description").focus(false, 300);
                    }
                  }
                },
                {
                  text: "Eliminar",
                  action: "deleteWeighingKri",
                  iconCls: "delete"
                },
                {
                  text: "Modificar",
                  action: "modifyWeighingKri",
                  iconCls: "modify"
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: StoreGridPanelWeighingKri,
                displayInfo: true,
                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              }
            }
          ]
        }
      ],
      buttons: [
        {
          text: "Guardar",
          hidden: true,
          scale: "medium",
          action: "saveWeighingKri",
          iconCls: "save"
        },
        {
          text: "Salir",
          scope: this,
          scale: "medium",
          handler: this.close,
          iconCls: "logout"
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});
