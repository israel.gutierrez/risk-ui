Ext.require(['Ext.ux.ColorField']);
Ext.define('DukeSource.view.risk.kri.windows.ViewWindowEntryQualificationKri', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowEntryQualificationKri',
    requires: [
        'Ext.form.Panel'
    ],
    width: 400,
    layout: 'fit',
    title: 'INGRESO CALIFICACI&Oacute;N DE KRI',
    border: false,
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    layout: 'anchor',
                    items: [
                        {
                            xtype: 'textfield',
                            hidden: true,
                            anchor: '100%',
                            name: 'idQualificationEachKri'
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            name: 'description',
                            fieldLabel: 'DESCRIPCI&Oacute;N'
                        },
                        {
                            xtype: 'container',
                            height: 26,
                            anchor: '100%',
                            layout: {
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    flex: 1,
                                    allowBlank: false,
                                    msgTarget: 'side',
                                    fieldCls: 'obligatoryTextField',
                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                    name: 'addRangeInf',
                                    displayField: 'description',
                                    valueField: 'id',
                                    padding: '0 10 0 0',
                                    fieldLabel: 'L&Iacute;MITE INFERIOR',
                                    store: {
                                        fields: ['id', 'description'],
                                        data: [
                                            {"id": ">", "description": ">"},
                                            {"id": ">=", "description": ">="}
                                        ]
                                    }
                                },
                                {
                                    xtype: 'NumberDecimalNumberObligatory',
                                    flex: 0.8,
                                    name: 'rangeInf'
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            height: 26,
                            anchor: '100%',
                            layout: {
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'combobox',
                                    flex: 1,
                                    allowBlank: false,
                                    msgTarget: 'side',
                                    fieldCls: 'obligatoryTextField',
                                    blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                    name: 'addRangeSup',
                                    displayField: 'description',
                                    valueField: 'id',
                                    padding: '0 10 0 0',
                                    fieldLabel: 'L&Iacute;MITE SUPERIOR',
                                    store: {
                                        fields: ['id', 'description'],
                                        data: [
                                            {"id": "<", "description": "<"},
                                            {"id": "<=", "description": "<="}
                                        ]
                                    }
                                },
                                {
                                    xtype: 'NumberDecimalNumber',
                                    flex: 0.8,
                                    name: 'rangeSup'
                                }
                            ]
                        },
                        {
                            xtype: 'colorfield',
                            anchor: '100%',
                            allowBlank: false,
                            msgTarget: 'side',
                            fieldCls: 'obligatoryTextField',
                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: 'levelColour',
                            fieldLabel: 'COLOR'
                        }

                    ]
                }
            ], buttons: [
                {
                    text: 'GUARDAR',
                    iconCls: 'save',
                    handler: function () {
                        var win = Ext.ComponentQuery.query('ViewWindowEntryKri')[0];
                        var form = me.down('form');
                        var values = form.getValues();
                        win.down('grid').getStore().add(values);
                        me.close();

                    }
                },
                {
                    text: 'SALIR',
                    iconCls: '',
                    scope: this,
                    handler: this.close
                }
            ]
        });

        me.callParent(arguments);
    }

});