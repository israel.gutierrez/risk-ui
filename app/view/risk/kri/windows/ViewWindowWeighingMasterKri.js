Ext.define('DukeSource.view.risk.kri.windows.ViewWindowWeighingMasterKri', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowWeighingMasterKri',
    width: 350,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'Ponderador',
    titleAlign: 'center',
    modal: true,
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'idKeyRiskIndicator',
                            itemId: 'idKeyRiskIndicator',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            value: 'id',
                            name: 'idWeighingKri',
                            hidden: true
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            anchor: '100%',
                            fieldCls: 'obligatoryTextField',
                            name: 'description',
                            itemId: 'description',
                            fieldLabel: 'Ponderador'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            fieldCls: 'obligatoryTextFieldNoUpperCase',
                            hidden: true,
                            name: 'fieldName',
                            fieldLabel: 'Identificador'
                        },
                        {
                            xtype: 'NumberDecimalNumberObligatory',
                            anchor: '100%',
                            hidden: true,
                            fieldCls: 'obligatoryTextField',
                            name: 'weighingMatrix',
                            fieldLabel: 'Peso',
                            value: 1
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    scale: 'medium',
                    action: 'saveWeighingKri',
                    iconCls: 'save'
                },
                {
                    text: 'Salir',
                    scale: 'medium',
                    scope: this,
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});
