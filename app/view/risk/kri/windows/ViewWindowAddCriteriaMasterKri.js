Ext.require(["Ext.ux.CheckColumn"]);
Ext.define("ModelGridPanelCriteriaKri", {
  extend: "Ext.data.Model",
  fields: [
    "idDetailCriteriaKri",
    "weighingNormalizer",
    "normalizerKri",
    "bestValue",
    "worstValue",
    "criteriaEvaluationKri",
    "descriptionDetailCriteria",
    "descriptionCriteria"
  ]
});

var StoreGridPanelCriteriaKri = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelCriteriaKri",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("DukeSource.view.risk.kri.windows.ViewWindowAddCriteriaMasterKri", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowAddCriteriaMasterKri",
  height: 300,
  width: 679,
  border: false,
  layout: {
    type: "fit"
  },
  title: "Criterio kri",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "gridpanel",
          name: "detailWindowCriteriaKri",
          store: StoreGridPanelCriteriaKri,
          selType: "cellmodel",
          selModel: Ext.create("Ext.selection.CheckboxModel"),
          plugins: [
            Ext.create("Ext.grid.plugin.CellEditing", {
              clicksToEdit: 1
            })
          ],
          columns: [
            {
              dataIndex: "descriptionDetailCriteria",
              flex: 2.2,
              text: "Criterio"
            },
            {
              dataIndex: "weighingNormalizer",
              align: "center",
              flex: 0.6,
              text: "PESO",
              editor: {
                xtype: "textfield",
                allowBlank: false
              }
            },
            {
              dataIndex: "normalizerKri",
              align: "center",
              flex: 1,
              text: "Normalizador",
              hidden: true,
              editor: {
                xtype: "textfield",
                allowBlank: false
              }
            },
            {
              dataIndex: "bestValue",
              align: "center",
              hidden: true,
              flex: 0.6,
              text: "Mejor",
              editor: {
                xtype: "textfield",
                allowBlank: false
              }
            },
            {
              dataIndex: "worstValue",
              align: "center",
              hidden: true,
              flex: 0.6,
              text: "Peor",
              editor: {
                xtype: "textfield",
                allowBlank: false
              }
            }
          ],
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: StoreGridPanelCriteriaKri,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          }
        }
      ],
      buttons: [
        {
          text: "GUARDAR",
          scale: "medium",
          action: "addCriteriaKri",
          iconCls: "save",
          handler: function() {
            var panel = Ext.ComponentQuery.query("ViewPanelConfigMasterKri")[0];
            var grid = panel.down("grid[name=masterKri]");
            var grid1 = panel.down("grid[name=detailCriteriaKri]");
            var arrayDataToSave = new Array();
            var recordss = me
              .down("grid")
              .getSelectionModel()
              .getSelection();
            Ext.Array.each(recordss, function(record, index, countriesItSelf) {
              arrayDataToSave.push(record.data);
            });
            var records = grid1.getStore().getRange();
            if (panel == undefined) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                "EL PANEL DE CONFIGURACION NO EXISTE",
                Ext.Msg.WARNING
              );
              me.close();
            } else {
              Ext.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/addNormalizerKri.htm?nameView=ViewPanelConfigMasterKri",
                params: {
                  jsonData: Ext.JSON.encode(arrayDataToSave),
                  idKeyRiskIndicator: grid
                    .getSelectionModel()
                    .getSelection()[0]
                    .get("idKeyRiskIndicator"),
                  criteriaEvaluationKri: records[0].get("criteriaEvaluationKri")
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      response.mensaje,
                      Ext.Msg.INFO
                    );
                    grid1.getStore().load();
                    me.close();
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_ERROR,
                      response.mensaje,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function() {}
              });
            }
          }
        },
        {
          text: "Salir",
          scale: "medium",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});
