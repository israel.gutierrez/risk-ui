Ext.define('DukeSource.view.risk.kri.windows.WindowRegisterEvaluationKri', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowRegisterEvaluationKri',
    width: 1100,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'REGISTRAR EVALUACIÓN',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    bodyStyle: {
                        background: '#dfe8f6'
                    },
                    items: [
                        {
                            xtype: 'fieldset',
                            itemId: 'headerWindow',
                            padding: 5,
                            margin: 5,
                            layout: 'hbox',
                            border: false,
                            style: {
                                borderColor: '#78abf5'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    name: 'idKeyRiskIndicator'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    name: 'indicatorNormalizer'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    name: 'indicatorKri'
                                },
                                {
                                    xtype: 'textfield',
                                    hidden: true,
                                    name: 'timeKri'
                                },
                                {
                                    xtype: 'textfield',
                                    flex: 3,
                                    fieldLabel: 'NOMBRE',
                                    name: 'nameKri',
                                    itemId: 'nameKri',
                                    fieldCls: "readOnlyText",
                                    readOnly: true
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'CODIGO',
                                    padding: '0 5 0 5',
                                    readOnly: true,
                                    labelWidth: 60,
                                    flex: 1,
                                    fieldCls: 'codeIncident',
                                    name: 'codeKri',
                                    itemId: 'codeKri'
                                },
                                {
                                    xtype: 'container',
                                    flex: 2,
                                    layout: {
                                        type: 'hbox'
                                    },
                                    items: [
                                        {
                                            xtype: 'UpperCaseTextFieldReadOnly',
                                            width: 200,
                                            padding: '0 1 0 0',
                                            hidden: true,
                                            name: 'abbreviation',
                                            itemId: 'abbreviation'
                                        },
                                        {
                                            xtype: 'textfield',
                                            readOnly: true,
                                            msgTarget: 'side',
                                            flex: 1,
                                            fieldCls: 'readOnlyText',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            labelWidth: 30,
                                            fieldLabel: 'A&Ntilde;O',
                                            name: 'year',
                                            itemId: 'year',
                                            fieldStyle: {
                                                textAlign: 'center'
                                            }
                                        },
                                        {
                                            xtype: 'textfield',
                                            readOnly: true,
                                            msgTarget: 'side',
                                            flex: 1,
                                            fieldCls: 'readOnlyText',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            padding: '0 0 0 5',
                                            name: 'abbreviationFrequency',
                                            itemId: 'abbreviationFrequency',
                                            fieldStyle: {
                                                textAlign: 'center'
                                            }
                                        },
                                        {
                                            xtype: 'textfield',
                                            readOnly: true,
                                            msgTarget: 'side',
                                            fieldCls: 'readOnlyText',
                                            blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                            padding: '0 0 0 5',
                                            flex: 1,
                                            fieldStyle: {
                                                textAlign: 'center'
                                            },
                                            align: 'center',
                                            name: 'month',
                                            itemId: 'month'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            title: '<b>CARGAR ARCHIVO DE EVALUACIÓN</b>',
                            layout: {
                                type: 'hbox',
                                align: 'stretch',
                                defaultMargins: {
                                    right: 5
                                }
                            },
                            items: [
                                {
                                    xtype: 'fileuploadfield',
                                    flex: 3,
                                    fieldLabel: 'CARGAR ARCHIVO',
                                    buttonConfig: {
                                        iconCls: 'excel'
                                    },
                                    buttonText: "seleccionar"
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    iconCls: 'save',
                    scale: 'medium',
                    text: 'GUARDAR',
                    itemId: 'btnSaveEvaluation',
                    action: 'saveEvaluationKri'
                },
                {
                    text: 'SALIR',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });

        me.callParent(arguments);
    }

});