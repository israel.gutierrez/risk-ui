Ext.define("ModelSearchEventOne", {
  extend: "Ext.data.Model",
  fields: [
    "yearMonth",
    "idEventOne",
    "description",
    "month",
    "year",
    "score",
    "state",
    "idQualificationEventOne",
    "levelColour",
    "descriptionQualificationEventOne",
    "descriptionMonth"
  ]
});

var storeSearchEventOne = Ext.create("Ext.data.Store", {
  model: "ModelSearchEventOne",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
//storeUserActives.load();

Ext.define("DukeSource.view.risk.kri.windows.ViewWindowSearchEventOne", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowSearchEventOne",
  layout: {
    align: "stretch",
    type: "vbox"
  },
  anchorSize: 100,
  title: "BUSCAR Evento",
  titleAlign: "center",
  width: 900,
  height: 500,
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          height: 45,
          padding: "2 2 2 2",
          bodyPadding: 10,
          items: [
            {
              xtype: "container",
              anchor: "100%",
              height: 26,
              layout: {
                type: "hbox"
              },
              items: [
                {
                  xtype: "combobox",
                  value: "2",
                  labelWidth: 40,

                  width: 190,
                  //                                    flex:1,
                  fieldLabel: "TIPO",
                  store: [
                    ["1", "CODIGO"],
                    ["2", "DESCRIPCION"]
                  ]
                },
                {
                  xtype: "UpperCaseTextField",
                  //    action:'searchUserActives',
                  flex: 2,

                  //                                    fieldLabel: 'BUSCAR PER',
                  listeners: {
                    afterrender: function(field) {
                      field.focus(false, 200);
                    },
                    specialkey: function(field, e) {
                      var property = "";
                      if (
                        Ext.ComponentQuery.query(
                          "ViewWindowSearchEventOne combobox"
                        )[0].getValue() == "1"
                      ) {
                        property = "abbreviation";
                      } else {
                        property = "nameKri";
                      }
                      if (e.getKey() === e.ENTER) {
                        var grid = me.down("grid");
                        var toolbar = grid.down("pagingtoolbar");
                        DukeSource.global.DirtyView.searchPaginationGridToEnter(
                          field,
                          grid,
                          grid.down("pagingtoolbar"),
                          "http://localhost:9000/giro/showListEventOneActivesComboBox.htm",
                          property,
                          "description"
                        );
                      }
                    }
                  }
                },
                //                                {
                //                                    xtype: 'ViewComboYear',
                //                                    flex: 0.7,
                //                                    disabled:true,
                //                                    hidden:true,
                //                                    labelWidth: 50,
                //                                    padding:'0 10 0 10',
                //                                    fieldLabel: 'ANIO'
                //                                },
                //                                {
                //                                    xtype: 'ViewComboMonth',
                //                                    flex: 1,
                //                                    disabled:true,
                //                                    hidden:true,
                //                                    labelWidth: 50,
                //                                    fieldLabel: 'MES'
                //                                },
                {
                  xtype: "datefield",
                  name: "dateStart",
                  format: "d/m/Y",
                  allowBlank: false,
                  disabled: true,
                  hidden: true,
                  msgTarget: "side",
                  fieldCls: "obligatoryTextField",
                  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  padding: "0 10 0 10",
                  labelWidth: 50,
                  fieldLabel: "INICIO",
                  width: 150
                },
                {
                  xtype: "datefield",
                  name: "dateLimit",
                  format: "d/m/Y",
                  allowBlank: false,
                  disabled: true,
                  hidden: true,
                  msgTarget: "side",
                  fieldCls: "obligatoryTextField",
                  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  padding: "0 10 0 0",
                  labelWidth: 50,
                  fieldLabel: "FIN",
                  width: 150
                }
              ]
            }
          ]
        },
        {
          xtype: "container",
          flex: 3,
          padding: "2 2 2 2",
          layout: {
            align: "stretch",
            type: "hbox"
          },
          items: [
            {
              xtype: "gridpanel",
              padding: "0 1 0 0",
              store: storeSearchEventOne,
              flex: 1,
              titleAlign: "center",
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false
                },
                {
                  dataIndex: "idEventOne",
                  width: 100,
                  align: "center",
                  text: "CODIGO"
                },

                //                                'yearMonth'
                //                                ,'idEventOne'
                //                                ,'description'
                //                                ,'month'
                //                                ,'year'
                //                                ,'score'
                //                                ,'state'
                //                                ,'idQualificationEventOne'
                //                                ,'levelColour'
                //                                ,'descriptionQualificationEventOne'
                //                                ,'descriptionMonth'

                {
                  dataIndex: "description",
                  flex: 5,
                  text: "KRI"
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: storeSearchEventOne,
                displayInfo: true,
                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              },
              listeners: {
                render: function() {
                  var me = this;
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    me,
                    me.down("pagingtoolbar"),
                    "http://localhost:9000/giro/showListEventOneActivesComboBox.htm",
                    "",
                    "idEventOne"
                  );
                }
              }
            }
          ]
        }
      ]
    });

    me.callParent(arguments);
  }
});
