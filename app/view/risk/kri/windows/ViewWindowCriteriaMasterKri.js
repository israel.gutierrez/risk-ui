Ext.require(["Ext.ux.CheckColumn"]);
Ext.define("ModelGridPanelCriteriaKri", {
  extend: "Ext.data.Model",
  fields: [
    "idDetailCriteriaKri",
    "weighingNormalizer",
    "idNormalizerKri",
    "normalizerKri",
    "bestValue",
    "worstValue",
    "criteriaEvaluationKri",
    "descriptionDetailCriteria",
    "descriptionCriteria"
  ]
});

var StoreGridPanelCriteriaKri = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelCriteriaKri",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("DukeSource.view.risk.kri.windows.ViewWindowCriteriaMasterKri", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowCriteriaMasterKri",
  height: 400,
  width: 479,
  border: false,
  layout: {
    align: "stretch",
    type: "vbox"
  },
  title: "Criterio",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          height: 45,
          padding: "2 2 2 2",
          bodyPadding: 10,
          items: [
            {
              xtype: "textfield",
              itemId: "id",
              name: "id",
              hidden: true
            },
            {
              xtype: "ViewComboCriteriaEvaluationKri",
              anchor: "100%",
              fieldCls: "obligatoryTextField",
              itemId: "text",
              name: "text",
              labelWidth: 150,
              fieldLabel: "Seleccionar criterio",
              listeners: {
                select: function(cbo) {
                  var grid = me.down("grid");
                  grid.store.getProxy().extraParams = {
                    criteriaEvaluationKri: cbo.getValue()
                  };
                  grid.store.getProxy().url =
                    "http://localhost:9000/giro/getListNormalizerKriDefault.htm";
                  grid.down("pagingtoolbar").moveFirst();
                }
              }
            }
          ]
        },
        {
          xtype: "container",
          flex: 3,
          padding: "2 2 2 2",
          layout: {
            align: "stretch",
            type: "hbox"
          },
          items: [
            {
              xtype: "gridpanel",
              name: "detailCriteriaKri",
              padding: "0 1 0 0",
              store: StoreGridPanelCriteriaKri,
              flex: 1,
              title: "Criterio kri",
              selType: "cellmodel",
              selModel: Ext.create("Ext.selection.CheckboxModel"),
              plugins: [
                Ext.create("Ext.grid.plugin.CellEditing", {
                  clicksToEdit: 1
                })
              ],
              columns: [
                {
                  dataIndex: "descriptionDetailCriteria",
                  flex: 2.2,
                  text: "Criterio"
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: StoreGridPanelCriteriaKri,
                displayInfo: true,
                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              },
              listeners: {
                render: function() {
                  var me = this;
                  me.store.getProxy().url =
                    "http://localhost:9000/giro/loadGridDefault.htm";
                  me.down("pagingtoolbar").moveFirst();
                }
              }
            }
          ]
        }
      ],
      buttons: [
        {
          text: "Continuar",
          scale: "medium",
          iconCls: "save",
          handler: function() {
            var win = Ext.ComponentQuery.query("WindowDetailConfigKri")[0];
            var grid = win.down("grid[name=detailCriteriaKri]");
            var recordKri = win.record;

            var arrayDataToSave = [];
            var records = me
              .down("grid")
              .getSelectionModel()
              .getSelection();
            Ext.Array.each(records, function(record, index, countriesItSelf) {
              arrayDataToSave.push(record.data);
            });

            if (arrayDataToSave.length === 0) {
              DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
            } else {
              DukeSource.lib.Ajax.request({
                waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                method: "POST",
                url:
                  "http://localhost:9000/giro/saveNormalizerKri.htm?nameView=ViewPanelConfigMasterKri",
                params: {
                  jsonData: Ext.JSON.encode(arrayDataToSave),
                  idKeyRiskIndicator: recordKri.get("idKeyRiskIndicator")
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    grid.store.getProxy().extraParams = {
                      idKeyRiskIndicator: recordKri.get("idKeyRiskIndicator")
                    };
                    grid.store.getProxy().url =
                      "http://localhost:9000/giro/getListNormalizerKri.htm";
                    grid.down("pagingtoolbar").doRefresh();
                    me.close();
                  } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                  }
                },
                failure: function() {}
              });
            }
          }
        },
        {
          text: "Salir",
          scale: "medium",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});
