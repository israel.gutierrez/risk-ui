Ext.define(
  "DukeSource.view.risk.kri.grids.ViewGridPanelRegisterEvaluationKri",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterEvaluationKri",
    store: "risk.kri.grids.StoreGridPanelRegisterEvaluationKri",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.kri.grids.StoreGridPanelRegisterEvaluationKri",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridEvaluationKri",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findEvaluationKri.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveEvaluationKri.htm?nameView=ViewPanelConfigMasterKri",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findEvaluationKri.htm",
                    "description",
                    "description"
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        {
          header: "CODIGO",
          dataIndex: "idEvaluationKri",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "COD.DETALLE PONDERADO",
          dataIndex: "nameDetailWeighingKri",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "CODIGO PONDERADOR",
          dataIndex: "nameWeighingKri",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "KRI",
          dataIndex: "nameKeyRiskIndicator",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "NORMALIZADOR KRI",
          dataIndex: "nameNormalizerKri",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "DESCRIPCION",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "RESULTADO EVAL.",
          dataIndex: "evaluationResul",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        },
        {
          header: "ESTADO",
          dataIndex: "state",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextField",
            allowBlank: false
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);
