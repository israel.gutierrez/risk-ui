Ext.define("DukeSource.view.risk.kri.grids.ViewGridPanelRegisterScorecard", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridPanelRegisterScorecard",
  store: "risk.kri.grids.StoreGridPanelRegisterScorecard",
  loadMask: true,
  columnLines: true,
  bbar: {
    xtype: "pagingtoolbar",
    pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
    store: "risk.kri.grids.StoreGridPanelRegisterScorecard",
    displayInfo: true,
    displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
    items: [
      "-",
      {
        xtype:"UpperCaseTrigger",
        fieldLabel: "FILTRAR",
        action: "searchTriggerGridScorecard",
        labelWidth: 60,
        width: 300
      }
    ],
    emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
  },
  listeners: {
    render: function() {
      var me = this;
      DukeSource.global.DirtyView.searchPaginationGridNormal(
        "",
        me,
        me.down("pagingtoolbar"),
        "http://localhost:9000/giro/findScorecardMaster.htm",
        "description",
        "description"
      );
    }
  },
  initComponent: function() {
    this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
      clicksToMoveEditor: 1,
      saveBtnText: "GUARDAR",
      cancelBtnText: "CANCELAR",
      autoCancel: false,
      completeEdit: function() {
        var me = this;
        var grid = me.grid;
        var selModel = grid.getSelectionModel();
        var record = selModel.getLastSelected();
        if (me.editing && me.validateEdit()) {
          me.editing = false;

          Ext.Ajax.request({
            method: "POST",
            url:
              "http://localhost:9000/giro/saveScorecard.htm?nameView=ViewPanelRegisterScorecard",
            params: {
              jsonData: Ext.JSON.encode(record.data)
            },
            success: function(response) {
              response = Ext.decode(response.responseText);
              if (response.success) {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_MESSAGE,
                  response.mensaje,
                  Ext.Msg.INFO
                );
                DukeSource.global.DirtyView.searchPaginationGridNormal(
                  "",
                  grid,
                  grid.down("pagingtoolbar"),
                  "http://localhost:9000/giro/findScorecardMaster.htm",
                  "description",
                  "description"
                );
              } else {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_ERROR,
                  response.mensaje,
                  Ext.Msg.ERROR
                );
              }
            },
            failure: function() {}
          });
          me.fireEvent("edit", me, me.context);
        }
      }
    });
    this.columns = [
      { xtype: "rownumberer", width: 50, sortable: false },
      {
        header: "Codigo Kri",
        dataIndex: "idKeyRiskIndicator",
        flex: 1,
        editor: {
          xtype: "UpperCaseTextField",
          allowBlank: false
        }
      },
      {
        header: "codigo Evento",
        dataIndex: "idEventOne",
        flex: 1,
        editor: {
          xtype: "UpperCaseTextField",
          allowBlank: false
        }
      },
      {
        header: "PESO",
        dataIndex: "weight",
        flex: 1,
        editor: {
          xtype: "UpperCaseTextField",
          allowBlank: false
        }
      },
      {
        header: "RELACION",
        dataIndex: "relationshipKRI",
        width: 200,
        editor: new Ext.form.field.ComboBox({
          triggerAction: "all",
          //                editable:true,
          //                readOnly:true,
          //                selectOnTab: true,
          allowBlank: false,
          store: [
            ["0", "NO TIENE RELACIóN"],
            ["1", "TIENE POCA REALACIÓN"],
            ["2", "TIENE BASTANTE REALACIÓN"],
            ["3", "TIENE MUCHA RELACIÓN"]
          ],
          lazyRender: true,
          listClass: "x-combo-list-small"
        })
      },
      {
        header: "ESTADO",
        dataIndex: "state",
        width: 60,
        editor: new Ext.form.field.ComboBox({
          triggerAction: "all",
          editable: false,
          readOnly: true,
          selectOnTab: true,
          allowBlank: false,
          store: [
            ["S", "ACTIVO"],
            ["N", "INACTIVO"]
          ],
          lazyRender: true,
          listClass: "x-combo-list-small"
        })
      }
    ];
    this.callParent(arguments);
  }
});
