Ext.require(["Ext.ux.ColorField"]);
Ext.define(
  "DukeSource.view.risk.kri.grids.ViewGridPanelShortQualificationEventOne",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelShortQualificationEventOne",
    store: "risk.kri.grids.StoreGridPanelRegisterQualificationEventOne",
    loadMask: true,
    columnLines: true,
    title: "NIVELES DE CALIFICACION",
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.kri.grids.StoreGridPanelRegisterQualificationEventOne",
      displayInfo: false
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findAllQualificationEventOne.htm"
        );
      }
    },
    initComponent: function() {
      this.columns = [
        {
          header: "Cod",
          align: "center",
          dataIndex: "idQualificationEventOne",
          width: 25,
          sortable: false
        },
        {
          header: "DESCRIPCION",
          dataIndex: "description",
          sortable: false,
          flex: 2
        },
        {
          header: "R.I.",
          align: "center",
          dataIndex: "rangeInf",
          sortable: false,
          width: 25
        },
        {
          header: "R.S.",
          align: "center",
          dataIndex: "rangeSup",
          sortable: false,
          width: 25
        },
        {
          header: "Niv.",
          align: "center",
          dataIndex: "legend",
          sortable: false,
          width: 35
        },
        {
          header: "COLOR",
          align: "center",
          dataIndex: "levelColour",
          sortable: false,
          flex: 1,
          renderer: function(value) {
            return (
              '<div  style="background-color:#' +
              value +
              "; color:#" +
              value +
              ';"> ' +
              value +
              "</div>"
            );
          }
        }
      ];
      this.callParent(arguments);
    }
  }
);
