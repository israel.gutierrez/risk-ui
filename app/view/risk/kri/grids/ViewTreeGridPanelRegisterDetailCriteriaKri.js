Ext.define('DukeSource.view.risk.kri.grids.ViewTreeGridPanelRegisterDetailCriteriaKri', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.ViewTreeGridPanelRegisterDetailCriteriaKri',
    store: 'risk.kri.grids.StoreTreeGridPanelRegisterDetailCriteriaKri',
    useArrows: true,
    multiSelect: true,
    singleExpand: true,
    rootVisible: false,
    columns: [
        {
            xtype: 'treecolumn',
            text: 'CRITERIO KRI -> DETALLE DE CRITERIO DE KRI',
            flex: 2,
            sortable: true,
            dataIndex: 'text'
        }
    ]
});
