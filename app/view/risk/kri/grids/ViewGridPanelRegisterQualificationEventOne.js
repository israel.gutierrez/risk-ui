Ext.require(["Ext.ux.ColorField"]);
Ext.define(
  "DukeSource.view.risk.kri.grids.ViewGridPanelRegisterQualificationEventOne",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterQualificationEventOne",
    store: "risk.kri.grids.StoreGridPanelRegisterQualificationEventOne",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.kri.grids.StoreGridPanelRegisterQualificationEventOne",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridQualificationEventOne",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findQualificationEventOne.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveQualificationEventOne.htm?nameView=ViewPanelRegisterQualificationEventOne",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findQualificationEventOne.htm",
                    "description",
                    "description"
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 25, sortable: false },
        {
          header: "CODIGO",
          align: "center",
          dataIndex: "idQualificationEventOne",
          flex: 1
        },
        {
          header: "DESCRIPCION",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextFieldObligatory",
            allowBlank: false
          }
        },
        {
          header: "RANGO INFERIOR",
          align: "center",
          dataIndex: "rangeInf",
          flex: 1,
          editor: {
            xtype: "NumberDecimalNumberObligatory",
            allowBlank: false
          }
        },
        {
          header: "RANGO SUPERIOR",
          align: "center",
          dataIndex: "rangeSup",
          flex: 1,
          editor: {
            xtype: "NumberDecimalNumberObligatory",
            allowBlank: false
          }
        },
        {
          header: "COLOR",
          align: "center",
          dataIndex: "levelColour",
          flex: 1,
          editor: {
            xtype: "colorfield",
            allowBlank: false
          },
          renderer: function(value) {
            return (
              '<div  style="background-color:#' +
              value +
              "; color:#" +
              value +
              ';"> ' +
              value +
              "</div>"
            );
          }
        },
        { header: "ESTADO", align: "center", dataIndex: "state", flex: 1 }
      ];
      this.callParent(arguments);
    }
  }
);
