Ext.define('DukeSource.view.risk.kri.combos.ViewComboEvaluationKri', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboEvaluationKri',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idEvaluationKri',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.kri.combos.StoreComboEvaluationKri'
});
