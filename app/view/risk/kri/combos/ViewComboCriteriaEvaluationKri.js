Ext.define("ModelComboCriteriaEvaluationKri", {
  extend: "Ext.data.Model",
  fields: ["idCriteriaEvaluationKri", "description"]
});

var StoreComboCriteriaEvaluationKri = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboCriteriaEvaluationKri",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListCriteriaEvaluationKriActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.kri.combos.ViewComboCriteriaEvaluationKri", {
  alias: "widget.ViewComboCriteriaEvaluationKri",
  queryMode: "local",
  displayField: "description",
  valueField: "idCriteriaEvaluationKri",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: StoreComboCriteriaEvaluationKri
});
