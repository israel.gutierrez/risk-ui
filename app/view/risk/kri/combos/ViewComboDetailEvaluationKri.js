Ext.define('DukeSource.view.risk.kri.combos.ViewComboDetailEvaluationKri', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboDetailEvaluationKri',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idDetailEvaluationKri',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.kri.combos.StoreComboDetailEvaluationKri'
});
