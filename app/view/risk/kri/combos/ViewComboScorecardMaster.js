Ext.define("ModelComboScorecardMaster", {
  extend: "Ext.data.Model",
  fields: ["idScorecardMaster", "description", "state"]
});

var StoreComboScorecardMaster = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboScorecardMaster",
  pageSize: 9999,
  autoLoad: false,

  listeners: {},

  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListScorecardMasterActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.kri.combos.ViewComboScorecardMaster", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboScorecardMaster",
  queryMode: "local",
  displayField: "description",
  valueField: "idScorecardMaster",

  editable: false,
  forceSelection: true,
  //    tpl: '<tpl for="."><div class="x-boundlist-item">{description}({state})</div><tpl if="xindex == 1"><hr /></tpl></tpl>',
  tpl: Ext.create(
    "Ext.XTemplate",
    '<tpl for=".">',
    '<div class="x-boundlist-item"><span style={[this.getColorItem(values)]}>{[this.getValueItem(values)]}</span></div>',
    "</tpl>",
    {
      getColorItem: function(rec) {
        return rec.state == "S" ? "color:blue" : "color:red";
      }
    },
    {
      getValueItem: function(rec) {
        return rec.state == "S"
          ? rec.description + "(" + rec.state + ")"
          : rec.description;
      }
    }
  ),

  displayTpl: Ext.create(
    "Ext.XTemplate",
    '<tpl for="."> {description}({state})  </tpl>'
  ),

  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: StoreComboScorecardMaster
});
