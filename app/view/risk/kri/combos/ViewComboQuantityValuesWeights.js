Ext.define('DukeSource.view.risk.kri.combos.ViewComboQuantityValuesWeights', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboQuantityValuesWeights',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idQuantityValuesWeights',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.kri.combos.StoreComboQuantityValuesWeights'
});
