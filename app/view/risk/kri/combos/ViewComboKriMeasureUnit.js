Ext.define("ModelComboKriMeasureUnit", {
  extend: "Ext.data.Model",
  fields: ["value", "description"]
});

var storeComboKriMeasureUnit = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboKriMeasureUnit",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
    extraParams: {
      propertyFind: "identified",
      valueFind: "UNIDMED",
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.kri.combos.ViewComboKriMeasureUnit", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboKriMeasureUnit",
  queryMode: "local",
  displayField: "description",
  valueField: "value",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: storeComboKriMeasureUnit
});
