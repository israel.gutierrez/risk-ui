Ext.define("ModelComboCalculationFrequency", {
  extend: "Ext.data.Model",
  fields: [
    "idCalculatingFrequency",
    "description",
    "abbreviation",
    "weighing",
    "equivalentFrequency"
  ]
});

var storeComboCalculationFrequency = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboCalculationFrequency",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListCalculatingFrequencyActivesComboBox.htm",
    extraParams: {
      propertyOrder: "idCalculatingFrequency"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.kri.combos.ViewComboCalculationFrequency", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboCalculationFrequency",
  queryMode: "local",
  displayField: "description",
  valueField: "idCalculatingFrequency",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: storeComboCalculationFrequency
});
