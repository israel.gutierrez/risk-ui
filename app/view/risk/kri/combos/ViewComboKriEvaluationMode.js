Ext.define("ModelComboKriEvaluationMode", {
  extend: "Ext.data.Model",
  fields: ["value", "description"]
});

var storeComboKriEvaluationMode = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboKriEvaluationMode",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
    extraParams: {
      propertyFind: "identified",
      valueFind: "EVALKRI",
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.kri.combos.ViewComboKriEvaluationMode", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboKriEvaluationMode",
  queryMode: "local",
  displayField: "description",
  valueField: "value",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: storeComboKriEvaluationMode
});
