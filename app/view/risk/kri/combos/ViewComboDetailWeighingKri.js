Ext.define('DukeSource.view.risk.kri.combos.ViewComboDetailWeighingKri', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboDetailWeighingKri',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idDetailWeighingKri',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.kri.combos.StoreComboDetailWeighingKri'
});
