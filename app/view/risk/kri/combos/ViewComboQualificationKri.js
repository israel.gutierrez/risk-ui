Ext.define('DukeSource.view.risk.kri.combos.ViewComboQualificationKri', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboQualificationKri',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idQualificationKri',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.kri.combos.StoreComboQualificationKri'
});
