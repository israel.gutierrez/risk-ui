Ext.define('DukeSource.view.risk.kri.combos.ViewComboNormalizerKri', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboNormalizerKri',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idNormalizerKri',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.kri.combos.StoreComboNormalizerKri'
});
