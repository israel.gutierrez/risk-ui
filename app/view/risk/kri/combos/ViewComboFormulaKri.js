var formKri = Ext.create('Ext.data.Store', {
    fields: ['id', 'description'],
    data: [
        {"id": "1", "description": "SUMA PRODUCTO"},
        {"id": "2", "description": "DIVISOR"},
        {"id": "3", "description": "CONSTANTE"}
    ]
});

Ext.define('DukeSource.view.risk.kri.combos.ViewComboFormulaKri', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboFormulaKri',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'id',
    value: 'S',
    editable: false,
    forceSelection: true,
//    listeners:{
//        render:function(){
//            var me = this;
//            me.store.load();
//        }
//    },
    store: formKri
});