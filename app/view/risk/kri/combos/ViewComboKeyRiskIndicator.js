Ext.define('DukeSource.view.risk.kri.combos.ViewComboKeyRiskIndicator', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboKeyRiskIndicator',
    queryMode: 'local',
    displayField: 'nameKri',
    valueField: 'idKeyRiskIndicator',

    editable: false,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        //me.store.load();
    },
    store: 'risk.kri.combos.StoreComboKeyRiskIndicator'
});
