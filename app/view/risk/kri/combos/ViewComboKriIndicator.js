Ext.define("ModelComboKriIndicator", {
  extend: "Ext.data.Model",
  fields: ["value", "description"]
});

var storeComboKriIndicator = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboKriIndicator",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
    extraParams: {
      propertyFind: "identified",
      valueFind: "INDKRI",
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.kri.combos.ViewComboKriIndicator", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboKriIndicator",
  queryMode: "local",
  displayField: "description",
  valueField: "value",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: storeComboKriIndicator
});
