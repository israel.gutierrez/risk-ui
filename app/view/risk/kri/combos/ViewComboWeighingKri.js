Ext.define("DukeSource.view.risk.kri.combos.ViewComboWeighingKri", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboWeighingKri",
  queryMode: "local",
  displayField: "description",
  valueField: "idWeighingKri",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: "risk.kri.combos.StoreComboWeighingKri"
});
