Ext.define('DukeSource.view.risk.kri.combos.ViewComboCalculatingFrequency', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboCalculatingFrequency',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idCalculatingFrequency',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.kri.combos.StoreComboCalculatingFrequency'
});
