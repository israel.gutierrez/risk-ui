Ext.define("ModelComboDesignReportKri", {
  extend: "Ext.data.Model",
  fields: ["codeWeighingKri", "description"]
});

var storeComboDesignReportKri = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboDesignReportKri",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListForeignKeysByTableNAme.htm",
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.kri.combos.ViewComboDesignReportKri", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboDesignReportKri",
  queryMode: "local",
  displayField: "description",
  valueField: "codeWeighingKri",

  editable: true,
  forceSelection: true,
  store: storeComboDesignReportKri
});
