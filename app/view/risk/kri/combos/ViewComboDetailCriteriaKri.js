Ext.define('DukeSource.view.risk.kri.combos.ViewComboDetailCriteriaKri', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboDetailCriteriaKri',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idDetailCriteriaKri',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'risk.kri.combos.StoreComboDetailCriteriaKri'
});
