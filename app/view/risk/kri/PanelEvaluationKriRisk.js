Ext.define("ModelGriEvKri", {
  extend: "Ext.data.Model",
  fields: []
});

Ext.define("DukeSource.view.risk.kri.PanelEvaluationKriRisk", {
  extend: "Ext.panel.Panel",
  alias: "widget.PanelEvaluationKriRisk",
  border: false,
  layout: "border",
  initComponent: function() {
    var me = this;
    var rowMaster = this.rowMaster;
    var storeEvKri = Ext.create("Ext.data.Store", {
      model: "ModelGriEvKri",
      autoLoad: true,
      proxy: {
        actionMethods: {
          create: "POST",
          read: "POST",
          update: "POST"
        },
        type: "ajax",
        url: "http://localhost:9000/giro/loadGridDefault.htm",
        reader: {
          type: "json",
          root: "data",
          successProperty: "success",
          totalProperty: "totalCount"
        }
      }
    });
    this.items = [
      {
        xtype: "form",
        anchor: "100%",
        region: "north",
        margin: "2 2 0 2",
        bodyPadding: "5",
        layout: {
          type: "hbox",
          align: "middle"
        },
        items: [
          {
            xtype: "textfield",
            hidden: true,
            itemId: "idKeyRiskIndicator",
            name: "idKeyRiskIndicator"
          },
          {
            xtype: "textfield",
            hidden: true,
            itemId: "dateEvaluation",
            name: "dateEvaluation"
          },
          {
            xtype: "textfield",
            hidden: true,
            name: "indicatorNormalizer",
            itemId: "indicatorNormalizer"
          },
          {
            xtype: "textfield",
            hidden: true,
            name: "indicatorKri",
            itemId: "indicatorKri"
          },
          {
            xtype: "fieldset",
            flex: 2,
            title: "Datos del kri y  de la evaluación",
            layout: {
              type: "hbox",
              align: "stretch"
            },
            items: [
              {
                xtype: "button",
                margin: "0 5 0 0",
                width: 80,
                iconCls: "logout",
                text: "Salir",
                handler: function() {
                  me.close();
                }
              },
              {
                xtype: "button",
                margin: "0 5 0 0",
                width: 80,
                text: "Detalle KRI",
                handler: function() {
                  var app = DukeSource.application;
                  var gridBack = Ext.ComponentQuery.query(
                    "ViewPanelConfigMasterKri grid"
                  )[0];

                  rowMaster = gridBack.store.getAt(
                    gridBack.store.find(
                      "idKeyRiskIndicator",
                      me.down("#idKeyRiskIndicator").getValue()
                    )
                  );

                  app
                    .getController(
                      "DukeSource.controller.risk.kri.ControllerPanelConfigMasterKri"
                    )
                    ._onModifyEntryKri(rowMaster);
                }
              },
              {
                xtype: "textfield",
                padding: "0 5 0 0",
                width: 100,
                name: "codeKri",
                fieldCls: "readOnlyTextCenter",
                itemId: "codeKri"
              },
              {
                xtype: "container",
                flex: 1,
                layout: {
                  type: "vbox",
                  align: "stretch"
                },
                items: [
                  {
                    xtype: "UpperCaseTextFieldReadOnly",
                    labelWidth: 30,
                    flex: 5,
                    fieldLabel: "KRI",
                    name: "nameKri",
                    itemId: "nameKri",
                    fieldStyle: {
                      textAlign: "center"
                    }
                  },
                  {
                    xtype: "container",
                    flex: 1,
                    layout: {
                      type: "hbox"
                    },
                    items: [
                      {
                        xtype: "UpperCaseTextFieldReadOnly",
                        width: 200,
                        padding: "0 1 0 0",
                        hidden: true,
                        name: "abbreviation",
                        itemId: "abbreviation"
                      },
                      {
                        xtype: "ViewComboYearKri",
                        allowBlank: false,
                        readOnly: true,
                        editable: false,
                        msgTarget: "side",
                        fieldCls: "obligatoryTextField",
                        fieldStyle: {
                          textAlign: "center"
                        },
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        labelWidth: 30,
                        flex: 1,
                        fieldLabel: "Año",
                        name: "year",
                        itemId: "year",
                        listeners: {
                          select: function(cbo, record) {
                            var panel = cbo.up("PanelEvaluationKriRisk");
                            var keyRisk = panel
                              .down("textfield[name=idKeyRiskIndicator]")
                              .getValue();
                            me.down("ViewComboMonthKri").reset();
                            me.down("ViewComboMonthKri")
                              .getStore()
                              .load({
                                url:
                                  "http://localhost:9000/giro/getMonthComboBox.htm",
                                params: {
                                  idKeyRiskIndicator: keyRisk,
                                  year: cbo.getValue()
                                }
                              });
                          }
                        }
                      },
                      {
                        xtype: "textfield",
                        readOnly: true,
                        msgTarget: "side",
                        flex: 1,
                        fieldCls: "readOnlyText",
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        padding: "0 0 0 5",
                        name: "abbreviationFrequency",
                        itemId: "abbreviationFrequency",
                        fieldStyle: {
                          textAlign: "center"
                        }
                      },
                      {
                        xtype: "ViewComboMonthKri",
                        allowBlank: false,
                        readOnly: true,
                        editable: false,
                        msgTarget: "side",
                        fieldCls: "obligatoryTextField",
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        padding: "0 0 0 5",
                        flex: 1,
                        fieldStyle: {
                          textAlign: "center"
                        },
                        align: "center",
                        labelWidth: 50,
                        fieldLabel: "Mes",
                        name: "month",
                        itemId: "month",
                        action: "consultEvaluationKri"
                      }
                    ]
                  }
                ]
              },
              {
                xtype: "button",
                iconCls: "save",
                margin: "0 0 0 10",
                flex: 0.3,
                cls: "my-btn",
                overCls: "my-over",
                scale: "medium",
                hidden: true,
                text: "Guardar",
                itemId: "btnSaveEvaluation",
                action: "saveEvaluationKri"
              },
              {
                xtype: "filefield",
                anchor: "100%",
                labelWidth: 100,
                padding: "0 0 0 5",
                hidden: true,
                fieldLabel: "Documento adjunto",
                name: "document",
                itemId: "document",
                buttonConfig: {
                  iconCls: "tesla even-attachment"
                },
                buttonText: ""
              }
            ]
          }
        ]
      },
      {
        xtype: "grid",
        id: "grid-pnl",
        store: storeEvKri,
        loadMask: true,
        columnLines: true,
        region: "center",
        flex: 20,
        multiSelect: true,
        forceFit: true,
        padding: "2 2 2 2",
        selType: "cellmodel",
        plugins: [
          Ext.create("Ext.grid.plugin.CellEditing", {
            clicksToEdit: 2,
            listeners: {
              edit: function(editor, e, eOpts) {
                me.down("grid").store.data.items[e.rowIdx].set(
                  "text",
                  editor.editors.items[0].field.rawValue
                );
              }
            }
          })
        ],
        tbar: [
          {
            text: "Registrar",
            cls: "my-btn",
            overCls: "my-over",
            iconCls: "add",
            scale: "medium",
            action: "entryEvaluationKri"
          },
          "-",
          {
            text: "Consultar",
            cls: "my-btn",
            overCls: "my-over",
            scale: "medium",
            iconCls: "search",
            action: "searchEvaluationKri"
          },
          "-",
          {
            text: "Eliminar",
            cls: "my-btn",
            overCls: "my-over",
            scale: "medium",
            iconCls: "delete",
            action: "deleteEvaluationKri"
          },
          {
            text: "Seguimiento",
            cls: "my-btn",
            hidden: true,
            overCls: "my-over",
            scale: "medium",
            iconCls: "tracing",
            action: "trackingKri"
          },
          "->",
          {
            text: "Auditoría",
            cls: "my-btn",
            overCls: "my-over",
            scale: "medium",
            iconCls: "auditory",
            handler: function(btn) {
              var grid = me.down("grid");
              DukeSource.global.DirtyView.showWindowAuditory(
                grid,
                "http://localhost:9000/giro/findAuditEvaluationKri.htm"
              );
            }
          }
        ],
        columns: [],
        bbar: {
          xtype: "pagingtoolbar",
          pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
          store: storeEvKri,
          displayInfo: true,
          displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
          emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
        }
      },
      {
        xtype: "form",
        itemId: "containerResult",
        flex: 5,
        anchor: "100%",
        region: "south",
        bodyStyle: "background:#d5e2f2;",
        margin: "0 2 2 2",
        bodyPadding: "5",
        layout: {
          type: "hbox",
          align: "middle",
          pack: "end"
        },
        items: [
          {
            xtype: "UpperCaseTextFieldReadOnly",
            padding: "0 10 0 0",
            flex: 1,
            fieldStyle: {
              textAlign: "center"
            },
            fieldLabel: "Resultado kri",
            itemId: "resultQualification",
            name: "resultQualification"
          },
          {
            xtype: "UpperCaseTextFieldReadOnly",
            fieldStyle: {
              textAlign: "center"
            },
            padding: "0 10 0 0",
            flex: 1,
            fieldLabel: "Calificación",
            itemId: "descriptionQualification",
            name: "descriptionQualification"
          },
          {
            xtype: "UpperCaseTextFieldReadOnly",
            fieldStyle: {
              textAlign: "center"
            },
            padding: "0 10 0 0",
            flex: 1,
            labelWidth: 130,
            fieldLabel: "Resultado normalizado",
            hidden: true,
            itemId: "resultNormalized",
            name: "resultNormalized"
          },
          {
            xtype: "UpperCaseTextFieldReadOnly",
            flex: 1,
            fieldStyle: {
              textAlign: "center"
            },
            hidden: true,
            fieldLabel: "Calificación",
            itemId: "descriptionResult",
            name: "descriptionResult"
          }
        ]
      }
    ];
    this.callParent();
  }
});
