Ext.define('DukeSource.view.risk.kri.parameter.ViewPanelRegisterDetailCriteriaKri', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterDetailCriteriaKri',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newDetailCriteriaKri'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDetailCriteriaKri',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'detailCriteriaKriAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewTreeGridPanelRegisterDetailCriteriaKri', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
