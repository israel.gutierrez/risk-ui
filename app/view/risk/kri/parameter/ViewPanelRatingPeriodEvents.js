Ext.define("DukeSource.view.risk.kri.parameter.ViewPanelRatingPeriodEvents", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelRatingPeriodEvents",
  border: false,
  layout: "border",
  tbar: [
    {
      text: "NUEVO",
      iconCls: "add",
      action: "newWindowSelectRatingPeriod"
    }
  ],
  listeners: {
    render: function() {
      var me = this;
    }
  },

  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          anchor: "100%",
          region: "north",
          margin: "2 2 0 2",
          bodyPadding: "5",
          layout: {
            type: "hbox"
          },
          items: [
            {
              xtype: "ViewComboYear",
              allowBlank: false,
              //                            readOnly: true,
              //                            editable: false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              padding: "0 10 0 80",
              labelWidth: 100,
              width: 250,
              fieldLabel: "PERIODO A&Ntilde;O:",
              name: "year",
              listeners: {
                select: function(cbo, record) {
                  me.down("ViewComboMonth").reset();
                  me.down("ViewComboMonth")
                    .getStore()
                    .load({
                      url:
                        "http://localhost:9000/giro/listMonthRatingPeriodEvent.htm",
                      params: {
                        year: cbo.getValue()
                      }
                    });
                }
              }
            },
            {
              xtype: "ViewComboMonth",
              allowBlank: false,
              //                            readOnly:true,
              //                            editable:false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              padding: "0 10 0 0",
              labelWidth: 50,
              width: 180,
              fieldLabel: "MES",
              name: "month"
            },
            {
              xtype: "button",
              itemId: "btnSearchRantingPeriod",
              text: "",
              iconCls: "search",
              tooltip: "BUSCAR",
              action: "findRantingPeriod"
            },
            {
              xtype: "UpperCaseTextFieldReadOnly",
              text: "",
              hidden: true,
              itemId: "idScorecardMasterItemId"
            }
          ]
        },
        {
          xtype: "tabpanel",
          activeTab: 0,
          region: "center",
          items: [
            {
              xtype: "ViewTabRatingPeriodEvents",
              title: "PERIODO NORMALIZADO"
            },
            {
              xtype: "ViewGridPanelRankingWeightEvent",
              title: "EVENTO DE RIESGO OPERACIONAL",
              padding: "2 2 2 2"
            }
          ]
        },
        {
          xtype: "form",
          itemId: "formResultNormalized",
          anchor: "20%",
          margin: "2 2 0 2",
          bodyPadding: "5",
          region: "south",
          layout: {
            type: "hbox"
          },
          items: [
            {
              xtype: "UpperCaseTextFieldReadOnly",
              padding: "0 10 0 0",
              flex: 1,
              labelWidth: 160,
              fieldLabel: "RESULTADO NORMALIZADO",
              itemId: "resultWeighting"
            },
            {
              xtype: "UpperCaseTextFieldReadOnly",
              padding: "0 10 0 0",
              flex: 1,
              fieldLabel: "DESCRIPCION",
              itemId: "descriptionWeighting"
            }
          ]
        }
      ],
      buttons: [
        {
          xtype: "button",
          text: "CALCULAR",
          overCls: "customOverStyleAll",
          iconCls: "save",
          hidden: true,
          itemId: "btnCalculateRantingPeriod",
          action: "calculateRantingPeriod"
        },
        {
          xtype: "button",
          text: "GUARDAR",
          iconCls: "save",
          hidden: true,
          action: "saveRanting"
        }
      ]
    });
    this.callParent();
  }
});
