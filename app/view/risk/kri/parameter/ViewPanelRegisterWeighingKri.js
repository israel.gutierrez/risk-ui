Ext.define('DukeSource.view.risk.kri.parameter.ViewPanelRegisterWeighingKri', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterWeighingKri',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newWeighingKri'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteWeighingKri'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchWeighingKri',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'weighingKriAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterWeighingKri', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
