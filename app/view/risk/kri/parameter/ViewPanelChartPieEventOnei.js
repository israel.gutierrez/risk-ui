//var store = Ext.create('Ext.data.JsonStore', {
//    fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5'],
//    data: [
//        {'name':'metric one', 'data1':10, 'data2':12, 'data3':14, 'data4':8, 'data5':13},
//        {'name':'metric two', 'data1':7, 'data2':8, 'data3':16, 'data4':10, 'data5':3},
//        {'name':'metric three', 'data1':5, 'data2':2, 'data3':14, 'data4':12, 'data5':7},
//        {'name':'metric four', 'data1':2, 'data2':14, 'data3':6, 'data4':1, 'data5':23},
//        {'name':'metric five', 'data1':27, 'data2':38, 'data3':36, 'data4':13, 'data5':33}
//    ]
//});


Ext.define('DukeSource.view.risk.kri.parameter.ViewPanelChartPieEventOne', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelChartPieEventOne',
    border: false,
    layout: 'border',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newQuantityValuesWeights'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteQuantityValuesWeights'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchQuantityValuesWeights',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'quantityValuesWeightsAuditory',
            iconCls: 'auditory'
        }
    ]
//    ,initComponent:function(){
//        var me = this;
//        this.items=[
//            {
//                xtype: 'chart',
////                renderTo: Ext.getBody(),
//                width: 500,
//                height: 300,
//                animate: true,
//                store: store,
//                theme: 'Base:gradients',
//                series: [{
//                    type: 'pie',
//                    field: 'data1',
//                    showInLegend: true,
//                    tips: {
//                        trackMouse: true,
//                        width: 140,
//                        height: 28,
//                        renderer: function(storeItem, item) {
//                            //calculate and display percentage on hover
//                            var total = 0;
//                            store.each(function(rec) {
//                                total += rec.get('data1');
//                            });
//                            this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('data1') / total * 100) + '%');
//                        }
//                    },
//                    highlight: {
//                        segment: {
//                            margin: 20
//                        }
//                    },
//                    label: {
//                        field: 'name',
//                        display: 'rotate',
//                        contrast: true,
//                        font: '18px Arial'
//                    }
//                }]
//            }
//
//
//        ];
//        this.callParent();
//    }
});


//Ext.define('DukeSource.view.risk.kri.report.ViewPanelSummaryQualificationKri',{
//    extend:'Ext.panel.Panel',
//    alias:'widget.ViewPanelSummaryQualificationKri',
//    border:false,
//    layout:'border',
//    tbar: [
//        '-',
//        {
//            text:'CONSULTAR KRI',
//            iconCls:'search',
//            action:'searchMasterKriQualification'
//
//        },'-'
//    ],
//    initComponent:function(){
//        var me = this;
//
//        this.items=[
//            {
//                xtype: 'chart',
//                store:Ext.create('Ext.data.Store', {
//                    model: 'SummaryQualification',
//                    autoLoad:true,
//                    proxy: {
//                        actionMethods: {
//                            create: 'POST',
//                            read: 'POST',
//                            update: 'POST'
//                        },
//                        type: 'ajax',
//                        url: 'loadGridDefault.htm',
//                        reader: {
//                            totalProperty: 'totalCount',
//                            root: 'data',
//                            successProperty: 'success'
//                        }
//                    }
//
//                }),
//                region:'east',
//                width: 900,
//                animate: true,
//                insetPadding: 20,
//                axes: [
//
//                    {
//                        type: 'Numeric',
//                        position: 'left',
//                        minimum: 0,
//                        adjustMinimumByMajorUnit: 0,
//                        fields: ['evaluationResult','rangeSup1','rangeSup2','rangeSup3','rangeSup4','rangeSup5','rangeSup6','rangeSup7','rangeSup8','rangeSup9','rangeSup10'],
//                        title: 'RATIO',
//                        grid: {
//                            odd: {
//                                opacity: 1,
//                                fill: '#ddd',
//                                stroke: '#bbb',
//                                'stroke-width': 0.5
//                            }
//                        }
//                    },
//                    {
//                        type: 'Category',
//                        position: 'bottom',
//                        fields: ['description'],
//                        label: {
//                            rotate: {
//                                degrees: 270
//                            }
//                        },
//                        title: 'TIEMPO'
//                    }],
//                series: [
//                    {
//                        type: 'area',
//                        highlight: true,
//                        axis: 'left',
//                        xField: 'description',
//                        yField: ['rangeSup1','rangeSup2','rangeSup3','rangeSup4','rangeSup5','rangeSup6','rangeSup7','rangeSup8','rangeSup9','rangeSup10'],
//                        style: {
//                            opacity: 0.93
//                        }
//                    },
//                    {
//                        type: 'line',
//                        axis: 'left',
//                        fillOpacity: 0.5,
//                        xField: 'description',
//                        tips: {
//                            trackMouse: true,
//                            width: 150,
//                            height: 38,
//                            renderer: function(storeItem, item) {
//                                this.setTitle(storeItem.get('description') + '<br />' + storeItem.get('evaluationResult'));
//                            }
//                        },
//                        style: {
//                            'stroke-width': 0
//                        },
//                        yField: 'evaluationResult'
//                    }
//                ]
//            }
//
//
//        ];
//        this.callParent();
//    }
//});
