Ext.define(
  "DukeSource.view.risk.kri.parameter.ViewPanelRegisterCalculatingFrequency",
  {
    extend: "Ext.panel.Panel",
    alias: "widget.ViewPanelRegisterCalculatingFrequency",
    border: false,
    layout: "fit",
    tbar: [
      {
        text: "Nuevo",
        iconCls: "add",
        action: "newCalculatingFrequency"
      },
      "-",
      {
        text: "Eliminar",
        iconCls: "delete",
        action: "deleteCalculatingFrequency"
      },
      "-",
      {
        xtype:"UpperCaseTrigger",
        action: "searchTriggerGridCalculatingFrequency",
        fieldLabel: "Buscar",
        labelWidth: 60,
        width: 300
      },
      "-",
      "->",
      {
        text: "Auditoria",
        action: "calculatingFrequencyAuditory",
        iconCls: "auditory"
      }
    ],
    initComponent: function() {
      this.items = [
        {
          xtype: "ViewGridPanelRegisterCalculatingFrequency",
          padding: 2
        }
      ];
      this.callParent();
    }
  }
);
