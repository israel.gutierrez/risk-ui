Ext.define('DukeSource.view.risk.kri.parameter.ViewPanelRegisterCriteriaEvaluationKri', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterCriteriaEvaluationKri',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newCriteriaEvaluationKri'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteCriteriaEvaluationKri'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchCriteriaEvaluationKri',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'criteriaEvaluationKriAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterCriteriaEvaluationKri', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
