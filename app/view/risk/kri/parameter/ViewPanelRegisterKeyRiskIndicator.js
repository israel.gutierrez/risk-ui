Ext.define('DukeSource.view.risk.kri.parameter.ViewPanelRegisterKeyRiskIndicator', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterKeyRiskIndicator',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newKeyRiskIndicator'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteKeyRiskIndicator'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchKeyRiskIndicator',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'keyRiskIndicatorAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterKeyRiskIndicator', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
