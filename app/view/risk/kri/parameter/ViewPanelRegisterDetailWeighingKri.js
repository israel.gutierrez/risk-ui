Ext.define('DukeSource.view.risk.kri.parameter.ViewPanelRegisterDetailWeighingKri', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterDetailWeighingKri',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newDetailWeighingKri'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteDetailWeighingKri'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDetailWeighingKri',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'detailWeighingKriAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterDetailWeighingKri', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
