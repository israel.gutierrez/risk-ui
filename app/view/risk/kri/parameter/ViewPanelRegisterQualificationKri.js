Ext.define('DukeSource.view.risk.kri.parameter.ViewPanelRegisterQualificationKri', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterQualificationKri',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newQualificationKri'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteQualificationKri'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchQualificationKri',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'qualificationKriAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterQualificationKri', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
