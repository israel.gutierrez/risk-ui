Ext.define('DukeSource.view.risk.kri.parameter.ViewPanelRegisterQualificationEventOne', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterQualificationEventOne',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newQualificationEventOne'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteQualificationEventOne'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchQualificationEventOne',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'qualificationEventOneAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterQualificationEventOne', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
