Ext.define('DukeSource.view.risk.kri.parameter.ViewPanelRegisterDetailEvaluationKri', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterDetailEvaluationKri',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newDetailEvaluationKri'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteDetailEvaluationKri'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDetailEvaluationKri',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'detailEvaluationKriAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterDetailEvaluationKri', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
