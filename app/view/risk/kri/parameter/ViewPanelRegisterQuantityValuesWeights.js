Ext.define('DukeSource.view.risk.kri.parameter.ViewPanelRegisterQuantityValuesWeights', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterQuantityValuesWeights',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newQuantityValuesWeights'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteQuantityValuesWeights'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchQuantityValuesWeights',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'quantityValuesWeightsAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterQuantityValuesWeights', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
