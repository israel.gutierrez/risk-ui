Ext.define('DukeSource.view.risk.kri.parameter.ViewPanelRegisterScorecard', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterScorecard',
    border: false,
    layout: 'border',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newScorecard'
        }
    ],
    listeners: {
        render: function () {
            var me = this;
        }
    },

    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                anchor: '100%',
                region: 'north',
                margin: '2 2 0 2',
                bodyPadding: '5',
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'UpperCaseTextFieldObligatory',
                        padding: '0 2 0 0',
                        labelWidth: 180,
                        flex: 3,
                        fieldCls: 'obligatoryTextField',
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        fieldLabel: 'NOMBRE CUADRO DE MANDO:',
                        name: 'descriptionScorecard',
                        hidden: true
                    },

                    {
                        xtype: 'ViewComboScorecardMaster',
                        action: 'searchConfigScorecard',
                        itemId: 'cboScorecardMaster',
                        msgTarget: 'side',
                        padding: '0 2 0 0',
                        labelWidth: 180,
                        flex: 3,
                        fieldCls: 'obligatoryTextField',
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        fieldLabel: 'NOMBRE CUADRO DE MANDO:'
                    },
                    {
                        xtype: 'datefield',
                        format: 'd/m/Y',
                        itemId: 'dateInitial',
                        labelWidth: 104,
                        padding: '0 2 0 100',
                        fieldLabel: 'FECHA VIGENCIA:',
                        allowBlank: false,
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        fieldCls: 'obligatoryTextField',
                        flex: 1,
                        listeners: {
                            select: function (field, value) {
                                me.down('datefield[itemId=dateFinal]').setMinValue(value);
                            }
                        }
                    },
                    {
                        xtype: 'datefield',
                        format: 'd/m/Y',
                        itemId: 'dateFinal',
                        labelWidth: 120,
                        padding: '0 2 0 10',
                        fieldLabel: 'FECHA CADUCIDAD:',
                        allowBlank: false,
                        blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        fieldCls: 'obligatoryTextField',
                        flex: 1,
                        listeners: {
                            select: function (field, value) {
                                me.down('datefield[itemId=dateInitial]').setMaxValue(value);
                            }
                        }
                    }
                ]
            },
            {
                "xtype": "tabpanel",
                "activeTab": 0,
                region: 'center',
                "items": [
                    {
                        "xtype": "ViewPanelRegisterScorecardConfig",
                        "title": "MATRIZ CAUSA-EFECTO"
                    },
//                    {
//                        xtype: 'container',
//                        "title": "PONDERACION POR EVENTO",
//                        anchor: '100%',
//
//                        layout: {
//                            type: 'hbox'
//                        },
//                        items: [
                    {
                        xtype: 'ViewGridPanelRegisterWeightEvent',
                        "title": "PONDERACION POR EVENTO",
                        padding: '2 2 2 2',
                        height: 480,
                        flex: 1
                    }
//                            ,{
//                                xtype: 'ViewPanelChartPieEventOne',
//                                padding: '2 2 2 2',
//                                flex:1
//                            }
//                        ]
//                    }
                ]
            },
            {
                xtype: 'form',
                anchor: '20%',
                margin: '2 2 0 2',
                bodyPadding: '5',
                region: 'south',
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'button',
                        text: 'CANCELAR',
                        overCls: 'customOverStyleAll',
                        iconCls: 'delete',
                        hidden: true,
                        action: 'cancelScoreCardConfig'
                    },
                    {
                        xtype: 'button',
                        text: 'CONTINUAR',
                        iconCls: 'save',
                        hidden: true,
                        action: 'continueScoreCardConfig'
                    },
                    {
                        xtype: 'button',
                        text: 'GUARDAR',
                        iconCls: 'save',
                        hidden: true,
                        action: 'saveScoreCardConfig'
                    }
                ]
            }
        ];
        this.callParent();
    }
});
