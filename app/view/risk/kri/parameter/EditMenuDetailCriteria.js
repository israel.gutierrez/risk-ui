Ext.define('DukeSource.view.risk.kri.parameter.EditMenuDetailCriteria', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.EditMenuDetailCriteria',
    width: 120,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [

                {
                    xtype: 'menuitem',
                    text: 'Editar',
                    iconCls: 'modify'
                },
                {
                    xtype: 'menuitem',
                    text: 'Eliminar',
                    iconCls: 'delete'
                }
            ]
        });

        me.callParent(arguments);
    }
});