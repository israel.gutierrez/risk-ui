Ext.define('DukeSource.view.risk.kri.parameter.ViewPanelRegisterNormalizerKri', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterNormalizerKri',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newNormalizerKri'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteNormalizerKri'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchNormalizerKri',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'normalizerKriAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterNormalizerKri', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
