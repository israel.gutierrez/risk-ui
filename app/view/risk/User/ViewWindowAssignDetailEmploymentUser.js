Ext.define("DukeSource.view.risk.User.ViewWindowAssignDetailEmploymentUser", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowAssignDetailEmploymentUser",
  layout: {
    type: "fit"
  },
  closable: false,
  width: 160,
  height: 420,
  modal: true,
  border: false,
  title: "DETALLE PUESTO",
  titleAlign: "center",
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          title: "",
          items: [
            {
              xtype: "numberfield",
              anchor: "100%",
              fieldLabel: "Nro EMPLEADO EN EL CARGO",
              fieldCls: "obligatoryTextField",
              msgTarget: "side",
              blankText:
                DukeSource.view.finance.util.Messages.MESSAGE_OBLIGATORIO,
              name: "correlativeUserEmployment",
              allowBlank: false,
              labelWidth: 175,
              listeners: {
                afterrender: function(field) {
                  field.focus(false, 200);
                },
                specialkey: function(f, e) {
                  DukeSource.global.DirtyView.focusEventEnterObligatory(
                    f,
                    e,
                    Ext.ComponentQuery.query(
                      "ViewWindowAssignDetailEmploymentUser datefield[name=dateStartUserEmployment]"
                    )[0]
                  );
                }
                //
              }
            },
            {
              xtype: "datefield",
              anchor: "100%",
              value: new Date(),
              format: "d/m/Y",
              fieldLabel: "FECHA DE INICIO",
              allowBlank: false,
              msgTarget: "side",
              blankText:
                DukeSource.view.finance.util.Messages.MESSAGE_OBLIGATORIO,
              name: "dateStartUserEmployment",
              labelWidth: 175,
              listeners: {
                specialkey: function(f, e) {
                  DukeSource.global.DirtyView.focusEventEnterObligatory(
                    f,
                    e,
                    Ext.ComponentQuery.query(
                      "ViewWindowAssignDetailEmploymentUser combobox[name=state]"
                    )[0]
                  );
                }
                //
              }
            },
            {
              xtype: "combobox",
              anchor: "100%",
              fieldLabel: "ESTADO",
              allowBlank: false,
              msgTarget: "side",
              blankText:
                DukeSource.view.finance.util.Messages.MESSAGE_OBLIGATORIO,
              name: "state",
              labelWidth: 175,
              value: "S",
              selectOnTab: true,
              store: [
                ["S", "SI"],
                ["N", "NO"]
              ],
              listeners: {
                specialkey: function(f, e) {
                  DukeSource.global.DirtyView.focusEventEnterObligatory(
                    f,
                    e,
                    Ext.ComponentQuery.query(
                      "ViewWindowAssignDetailEmploymentUser button"
                    )[0]
                  );
                }
                //
              }
            },
            {
              xtype: "textfield",
              hidden: true,
              fieldLabel: "CODIGO",
              name: "idUserEmployment",
              hideLabel: true
            },
            {
              xtype: "textfield",
              hidden: true,
              fieldLabel: "DESCRIPCION",
              name: "descriptionEmployment",
              hideLabel: true
            },
            {
              xtype: "textfield",
              hidden: true,
              fieldLabel: "EMPLEADO",
              name: "employment",
              hideLabel: true
            },
            {
              xtype: "textfield",
              hidden: true,
              fieldLabel: "USER",
              name: "user",
              hideLabel: true
            }
          ]
        }
      ]
    });

    me.callParent(arguments);
  }
});
