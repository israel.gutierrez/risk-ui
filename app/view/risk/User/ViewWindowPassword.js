Ext.define("DukeSource.view.risk.User.ViewWindowPassword", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowPassword",
  border: false,
  layout: {
    type: "fit"
  },
  anchorSize: 100,
  title: "CAMBIO CONTRASE&Ntilde;A",
  titleAlign: "center",
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          title: "",
          items: [
            {
              xtype: "textfield",
              anchor: "100%",
              name: "userName",
              fieldLabel: "USUARIO",
              readOnly: true,
              fieldCls: "readOnlyText",
              labelWidth: 170
            },
            {
              xtype: "textfield",
              anchor: "100%",
              name: "password",
              inputType: "password",
              fieldLabel: "CONTRASENIA ACTUAL",
              msgTarget: "side",
              allowBlank: false,
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              fieldCls: "obligatoryTextField",
              labelWidth: 170,
              listeners: {
                afterrender: function(field) {
                  field.focus(false, 200);
                },
                specialkey: function(f, e) {
                  DukeSource.global.DirtyView.focusEventEnterObligatory(
                    f,
                    e,
                    Ext.ComponentQuery.query(
                      "ViewWindowPassword textfield[name=newPassword]"
                    )[0]
                  );
                }
              }
            },
            {
              xtype: "textfield",
              anchor: "100%",
              name: "newPassword",
              fieldLabel: "CONTRASE&Ntilde;A NUEVA",
              inputType: "password",
              msgTarget: "side",
              allowBlank: false,
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              fieldCls: "obligatoryTextField",
              labelWidth: 170,
              listeners: {
                specialkey: function(f, e) {
                  DukeSource.global.DirtyView.focusEventEnterObligatory(
                    f,
                    e,
                    Ext.ComponentQuery.query(
                      "ViewWindowPassword textfield[fieldLabel=CONFIRMAR CONTRASENIA]"
                    )[0]
                  );
                }
              }
            },
            {
              xtype: "textfield",
              anchor: "100%",
              fieldLabel: "CONFIRMAR CONTRASE&Ntilde;A",
              msgTarget: "side",
              action: "confirmNewPass",
              inputType: "password",
              allowBlank: false,
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              fieldCls: "obligatoryTextField",
              labelWidth: 170,
              listeners: {
                specialkey: function(f, e) {
                  var win = Ext.ComponentQuery.query("ViewWindowPassword")[0];
                  if (
                    win.down("textfield[name=newPassword]").getValue() ==
                    win.down("textfield[action=confirmNewPass]").getValue()
                  ) {
                    DukeSource.global.DirtyView.focusEventEnterObligatory(
                      f,
                      e,
                      win.down("button[action=saveChangePasswordWindow]")
                    );
                  } else {
                    Ext.MessageBox.show({
                      title: DukeSource.global.GiroMessages.TITLE_WARNING,
                      msg: "Los password NO COINCIDEN, intentelo nuevamente",
                      icon: Ext.Msg.WARNING,
                      buttons: Ext.MessageBox.OK,
                      closable: false,
                      fn: function(btn) {
                        if (btn == "ok") {
                          var pass = win.down(
                            "textfield[action=confirmNewPass]"
                          );
                          pass.reset();
                          pass.focus(false, 100);
                        }
                      }
                    });
                  }
                }
              }
            }
          ]
        }
      ],
      buttons: [
        {
          text: "CONFIRMAR",
          action: "saveChangePasswordWindow",
          iconCls: "save"
        },
        {
          text: "SALIR",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ]
    });

    me.callParent(arguments);
  }
});
