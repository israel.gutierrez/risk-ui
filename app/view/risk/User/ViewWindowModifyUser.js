Ext.define("DukeSource.view.risk.User.ViewWindowModifyUser", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowModifyUser",
  layout: {
    type: "fit"
  },
  width: 272,
  height: 133,
  modal: true,
  border: false,
  title: "MODIFICAR USUARIO",
  titleAlign: "center",
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          title: "",
          items: [
            {
              xtype: "timefield",
              anchor: "100%",
              fieldLabel: "HORA INICIO",
              name: "hourStart",
              fieldCls: "obligatoryTextField",
              allowBlank: false,
              msgTarget: "side",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              listeners: {
                afterrender: function(field) {
                  field.focus(false, 200);
                },
                specialkey: function(f, e) {
                  DukeSource.global.DirtyView.focusEventEnterObligatory(
                    f,
                    e,
                    Ext.ComponentQuery.query(
                      "ViewWindowModifyUser timefield[name=hourEnd]"
                    )[0]
                  );
                }
                //
              }
            },
            {
              xtype: "timefield",
              anchor: "100%",
              fieldLabel: "FECHA FIN",
              fieldCls: "obligatoryTextField",
              name: "hourEnd",
              allowBlank: false,
              msgTarget: "side",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              listeners: {
                specialkey: function(f, e) {
                  DukeSource.global.DirtyView.focusEventEnterObligatory(
                    f,
                    e,
                    Ext.ComponentQuery.query(
                      "ViewWindowModifyUser button[action=confirmModifyUser]"
                    )[0]
                  );
                }
              }
            },
            {
              xtype: "textfield",
              hidden: true,
              name: "userName",
              hideLabel: true
            }
          ]
        }
      ],
      buttons: [
        {
          text: "CONFIRMAR",
          action: "confirmModifyUser",
          iconCls: "save"
        },
        {
          text: "SALIR",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ]
    });

    me.callParent(arguments);
  }
});
