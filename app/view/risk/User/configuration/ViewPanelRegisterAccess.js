Ext.define('DukeSource.view.risk.User.configuration.ViewPanelRegisterAccess', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterAccess',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newAccess'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteAccess'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchAccess',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-'
        , '->',
        {
            text: 'AUDITORIA',
            action: 'accessAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterAccess', padding: '2 2 2 2'}];
        this.callParent();
    }
});
