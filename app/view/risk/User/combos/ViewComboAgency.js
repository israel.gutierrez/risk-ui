Ext.define("ModelComboAgency", {
  extend: "Ext.data.Model",
  fields: ["idAgency", "description"]
});

var StoreComboAgency = Ext.create("Ext.data.Store", {
  extend: "Ext.data.Store",
  model: "ModelComboAgency",
  pageSize: 9999,
  autoLoad: true,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListAgencyActivesComboBox.htm",
    extraParams: {
      propertyOrder: "a.description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.User.combos.ViewComboAgency", {
  extend: "Ext.form.ComboBox",
  plugins: ["ComboSelectCount"],
  alias: "widget.ViewComboAgency",
  displayField: "description",
  valueField: "idAgency",

  editable: true,
  typeAhead: true,
  forceSelection: true,
  store: StoreComboAgency
});
