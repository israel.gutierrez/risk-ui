Ext.define("DukeSource.view.risk.User.combos.ViewComboEmployment", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboEmployment",
  queryMode: "local",
  displayField: "description",
  valueField: "idEmployment",

  editable: true,
  forceSelection: true,
  listeners: {
    render: function() {
      var me = this;
      me.store.load();
    }
  },
  store: "risk.User.combos.StoreComboEmployment"
});
