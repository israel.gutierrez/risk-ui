Ext.define("DukeSource.view.risk.User.combos.ViewComboRole", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboRole",
  queryMode: "local",
  displayField: "name",
  valueField: "id",

  editable: true,
  typeAhead: true,
  autoSync: true,
  forceSelection: false,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: "risk.User.combos.StoreComboRole"
});
