Ext.define("DukeSource.view.risk.User.combos.ViewComboAuthorization", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboAuthorization",
  queryMode: "local",
  displayField: "description",
  valueField: "idSpecialProcess",

  editable: true,
  forceSelection: true,
  listeners: {
    render: function() {
      var me = this;
      me.store.load();
    }
  },
  store: "risk.User.combos.StoreComboAuthorization"
});
