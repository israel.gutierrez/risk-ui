Ext.define("DukeSource.view.risk.User.combos.ViewComboPermission", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboPermission",
  queryMode: "local",
  displayField: "description",
  valueField: "value",
  editable: false,
  forceSelection: true,
  listeners: {
    render: function() {
      var me = this;
      me.store.load();
    }
  },
  store: "risk.User.combos.StoreComboPermission"
});
