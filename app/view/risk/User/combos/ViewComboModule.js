Ext.define("DukeSource.view.risk.User.combos.ViewComboModule", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboModule",
  queryMode: "local",
  displayField: "description",
  valueField: "idModule",
  editable: true,
  typeAhead: true,
  forceSelection: true,
  listeners: {
    render: function() {
      var me = this;
      me.store.load();
    }
  },
  store: "risk.User.combos.StoreComboModule"
});
