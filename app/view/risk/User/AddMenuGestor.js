Ext.define('DukeSource.view.risk.User.AddMenuGestor', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.AddMenuGestor',
    width: 160,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: []
        });

        me.callParent(arguments);
    }
});