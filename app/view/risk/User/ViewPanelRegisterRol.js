Ext.define('DukeSource.view.risk.User.ViewPanelRegisterRol', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterRol',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'Nuevo',
            iconCls: 'add',
            scale: 'medium',
            cls: 'my-btn',
            overCls: 'my-over',
            action: 'newRole'
        },
        '-',
        {
            text: 'Eliminar',
            iconCls: 'delete',
            scale: 'medium',
            cls: 'my-btn',
            overCls: 'my-over',
            action: 'deleteRole'
        },
        '-',
        {
            text: 'Permisos',
            iconCls: 'key_go',
            scale: 'medium',
            cls: 'my-btn',
            overCls: 'my-over',
            action: 'assignPermission'
        },
        '-'
        ,
        {
            xtype: 'UpperCaseTextField',
            action: 'searchRole',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        },
        '->',
        '-',
        {
            text: 'AUDITORIA',
            scale: 'medium',
            cls: 'my-btn',
            overCls: 'my-over',
            hidden: true,
            iconCls: 'auditory'
        }

    ],
    initComponent: function () {
        this.items = [
            {
                xtype: 'ViewGridPanelRegisterRol',
                padding: 2
            }
        ];
        this.callParent();
    }

});