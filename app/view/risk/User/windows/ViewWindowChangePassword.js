Ext.define("DukeSource.view.risk.User.windows.ViewWindowChangePassword", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowChangePassword",
  border: false,
  layout: {
    type: "fit"
  },
  anchorSize: 100,
  title: "CAMBIO CONTRASE&Ntilde;A",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          title: "",
          items: [
            {
              xtype: "textfield",
              anchor: "100%",
              name: "userName",
              itemId: "userName",
              fieldLabel: "USUARIO",
              // value: user,
              readOnly: true,
              fieldCls: "readOnlyText",
              labelWidth: 170
            },
            {
              xtype: "textfield",
              anchor: "100%",
              name: "newPassword",
              itemId: "newPassword",
              fieldLabel: "CONTRASE&Ntilde;A NUEVA",
              inputType: "password",
              msgTarget: "side",
              allowBlank: false,
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              fieldCls: "obligatoryTextField",
              labelWidth: 170,
              listeners: {
                blur: function(f) {
                  var strongRegex = new RegExp(
                    "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"
                  );
                  if (!strongRegex.test(f.getValue())) {
                    Ext.MessageBox.show({
                      title: DukeSource.global.GiroMessages.TITLE_WARNING,
                      msg:
                        "La Contraseña no cumple los requisitos mínimos necesarios",
                      icon: Ext.Msg.WARNING,
                      buttons: Ext.MessageBox.OK,
                      fn: function(t) {
                        if (t == "ok") {
                          f.focus(false, 100);
                        }
                      }
                    });
                    f.reset();
                  }
                }
              }
            },
            {
              xtype: "textfield",
              anchor: "100%",
              fieldLabel: "CONFIRMAR CONTRASE&Ntilde;A",
              msgTarget: "side",
              action: "confirmNewPass",
              inputType: "password",
              allowBlank: false,
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              fieldCls: "obligatoryTextField",
              labelWidth: 170
              // listeners: {
              //     blur: function (f, e) {
              //         if (me.down('textfield[name=newPassword]').getValue() == me.down('textfield[action=confirmNewPass]').getValue()) {
              //             DukeSource.global.DirtyView.focusEventEnterObligatory(f, e, me.down('button[text=CONFIRMAR]'))
              //         } else {
              //             Ext.MessageBox.show({
              //                 title: DukeSource.global.GiroMessages.TITLE_WARNING,
              //                 msg: 'Los password NO COINCIDEN, intentelo nuevamente',
              //                 icon: Ext.Msg.WARNING,
              //                 buttons: Ext.MessageBox.OK,
              //                 closable: false,
              //                 fn: function (btn) {
              //                     if (btn == 'ok') {
              //                         var pass = me.down('textfield[action=confirmNewPass]');
              //                         pass.reset();
              //                         pass.focus(false, 100);
              //                     }
              //                 }
              //             });
              //
              //         }
              //     }
              // }
            }
          ]
        }
      ],
      buttons: [
        {
          text: "CONFIRMAR",
          iconCls: "save",
          handler: function() {
            if (
              me
                .down("form")
                .getForm()
                .isValid()
            ) {
              if (
                me.down("textfield[name=newPassword]").getValue() ==
                me.down("textfield[action=confirmNewPass]").getValue()
              ) {
                Ext.MessageBox.show({
                  title: DukeSource.global.GiroMessages.TITLE_WARNING,
                  msg: "Esta Seguro de Cambiar el PASSWORD?",
                  icon: Ext.Msg.QUESTION,
                  buttons: Ext.MessageBox.YESNO,
                  closable: false,
                  fn: function(btn) {
                    if (btn == "yes") {
                      Ext.Ajax.request({
                        method: "POST",
                        url: "http://localhost:9000/giro/changePassword.htm",
                        params: {
                          userName: me
                            .down("textfield[name=userName]")
                            .getValue(),
                          newPassword: me
                            .down("textfield[name=newPassword]")
                            .getValue()
                        },
                        success: function(response) {
                          response = Ext.decode(response.responseText);
                          if (response.success) {
                            DukeSource.global.DirtyView.messageAlert(
                              DukeSource.global.GiroMessages.TITLE_MESSAGE,
                              response.mensaje,
                              Ext.Msg.INFO
                            );
                            me.close();
                          } else {
                            DukeSource.global.DirtyView.messageAlert(
                              DukeSource.global.GiroMessages.TITLE_WARNING,
                              response.mensaje,
                              Ext.Msg.ERROR
                            );
                          }
                        },
                        failure: function() {}
                      });
                    }
                  }
                });
              } else {
                Ext.MessageBox.show({
                  title: DukeSource.global.GiroMessages.TITLE_WARNING,
                  msg: "Los password NO COINCIDEN, intentelo nuevamente",
                  icon: Ext.Msg.WARNING,
                  buttons: Ext.MessageBox.OK,
                  closable: false,
                  fn: function(btn) {
                    if (btn == "ok") {
                      var pass = me.down("textfield[action=confirmNewPass]");
                      pass.reset();
                      pass.focus(false, 100);
                    }
                  }
                });
              }
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
                Ext.Msg.WARNING
              );
            }
          }
        },
        {
          text: "SALIR",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ]
    });

    me.callParent(arguments);
  }
});
