Ext.define("DukeSource.view.risk.User.windows.ViewWindowConstantApplication", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowConstantApplication",
  height: 280,
  width: 479,
  border: false,
  layout: {
    type: "fit"
  },
  title: "Constante de la aplicaci&oacute;n",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          title: "",
          items: [
            {
              xtype: "UpperCaseTextField",
              anchor: "60%",
              name: "idConstantApplication",
              value: "id",
              fieldLabel: "C&acute;odigo",
              fieldCls: "readOnlyText",
              readOnly: true
            },
            {
              xtype: "ViewComboModule",
              anchor: "100%",
              allowBlank: false,
              msgTarget: "side",
              fieldCls: "UpperCaseTextFieldObligatory",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              name: "module",
              fieldLabel: "M&oacute;dulo"
            },
            {
              xtype: "UpperCaseTextFieldObligatory",
              anchor: "100%",
              name: "codeConstant",
              fieldCls: "obligatoryTextField",
              fieldLabel: "C&oacute;digo de constante"
            },
            {
              xtype: "UpperCaseTextFieldObligatory",
              anchor: "100%",
              name: "description",
              fieldCls: "obligatoryTextField",
              fieldLabel: "Descripci&oacute;n"
            },
            {
              xtype: "textfield",
              anchor: "100%",
              readOnly: false,
              allowBlank: true,
              name: "valueConstant",
              fieldCls: "obligatoryTextFieldNoUpperCase",
              fieldLabel: "Valor"
            },
            {
              xtype: "combobox",
              allowBlank: false,
              editable: false,
              fieldCls: "obligatoryTextField",
              anchor: "100%",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              name: "typeConstants",
              itemId: "typeConstants",
              fieldLabel: "Tipo",
              forceSelection: true,
              displayField: "description",
              valueField: "typeConstants",
              fieldStyle: "text-transform:uppercase",
              store: {
                fields: ["typeConstants", "description", "state"],
                data: [
                  {
                    typeConstants: "S",
                    description: "SISTEMAS",
                    state: "S"
                  },
                  {
                    typeConstants: "U",
                    description: "USUARIO",
                    state: "S"
                  }
                ]
              }
            },
            {
              xtype: "UpperCaseTextFieldObligatory",
              anchor: "100%",
              name: "state",
              fieldCls: "obligatoryTextField",
              fieldLabel: "Estado",
              value: "S",
              hidden: true
            }
          ]
        }
      ],
      buttons: [
        {
          text: "Guardar",
          scale: "medium",
          action: "saveConstantApplication",
          iconCls: "save"
        },
        {
          text: "Salir",
          scale: "medium",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});
