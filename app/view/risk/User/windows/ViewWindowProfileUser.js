Ext.define("DukeSource.view.risk.User.windows.ViewWindowProfileUser", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowProfileUser",
  border: false,
  layout: {
    type: "fit"
  },
  anchorSize: 200,
  title: "PERFIL DEL USUARIO",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          listeners: {
            beforerender: function(field) {
              Ext.Ajax.request({
                method: "POST",
                url: "http://localhost:9000/giro/getAttributesToSession.htm",
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    Ext.ComponentQuery.query(
                      "ViewWindowProfileUser UpperCaseTextFieldReadOnly"
                    )[0].setValue(response.username);
                    Ext.ComponentQuery.query(
                      "ViewWindowProfileUser UpperCaseTextFieldReadOnly"
                    )[1].setValue(response.fullName);
                    Ext.ComponentQuery.query(
                      "ViewWindowProfileUser UpperCaseTextFieldReadOnly"
                    )[2].setValue(response.category);
                    Ext.ComponentQuery.query(
                      "ViewWindowProfileUser UpperCaseTextFieldReadOnly"
                    )[3].setValue(response.descriptionAgency);
                    Ext.ComponentQuery.query(
                      "ViewWindowProfileUser UpperCaseTextFieldReadOnly"
                    )[4].setValue(response.workAreaDescription);
                    Ext.ComponentQuery.query(
                      "ViewWindowProfileUser UpperCaseTextFieldReadOnly"
                    )[5].setValue(response.jobPlaceDescription);
                    Ext.ComponentQuery.query(
                      "ViewWindowProfileUser UpperCaseTextFieldReadOnly"
                    )[6].setValue(response.descriptionRole);
                  } else {
                    this.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_ERROR,
                      response.mensaje,
                      Ext.Msg.ERROR
                    );
                  }
                }
              });
            }
          },
          items: [
            {
              xtype: "UpperCaseTextFieldReadOnly",
              anchor: "100%",
              name: "userId",
              fieldLabel: "USUARIO",
              readOnly: true,
              fieldCls: "readOnlyText",
              labelWidth: 100
            },
            {
              xtype: "UpperCaseTextFieldReadOnly",
              anchor: "100%",
              name: "userName",
              fieldLabel: "NOMBRES",
              readOnly: true,
              fieldCls: "readOnlyText",
              labelWidth: 100
            },
            {
              xtype: "UpperCaseTextFieldReadOnly",
              anchor: "100%",
              name: "userCategory",
              fieldLabel: "CATEGORIA",
              readOnly: true,
              fieldCls: "readOnlyText",
              labelWidth: 100
            },
            {
              xtype: "UpperCaseTextFieldReadOnly",
              anchor: "100%",
              name: "userAgency",
              fieldLabel: "AGENCIA",
              readOnly: true,
              fieldCls: "readOnlyText",
              labelWidth: 100
            },
            {
              xtype: "UpperCaseTextFieldReadOnly",
              anchor: "100%",
              name: "userWorkArea",
              fieldLabel: "AREA",
              readOnly: true,
              fieldCls: "readOnlyText",
              labelWidth: 100
            },
            {
              xtype: "UpperCaseTextFieldReadOnly",
              anchor: "100%",
              name: "userJobPlace",
              fieldLabel: "CARGO",
              readOnly: true,
              fieldCls: "readOnlyText",
              labelWidth: 100
            },
            {
              xtype: "UpperCaseTextFieldReadOnly",
              anchor: "100%",
              name: "userRole",
              fieldLabel: "ROL",
              readOnly: true,
              fieldCls: "readOnlyText",
              labelWidth: 100
            }
          ],
          buttons: [
            {
              text: "Salir",
              scope: this,
              handler: this.close,
              iconCls: "logout"
            }
          ]
        }
      ]
    });

    me.callParent(arguments);
  }
});
