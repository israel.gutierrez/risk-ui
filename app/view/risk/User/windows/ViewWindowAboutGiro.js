Ext.define('DukeSource.view.risk.User.windows.ViewWindowAboutGiro', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowAboutGiro',
    border: false,
    layout: {
        type: 'fit'
    },
    anchorSize: 160,
    title: 'A CERCA DE GIRO',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'panel',
                    //bodyPadding: 10,
                    //layout:{
                    //    //align:'center',
                    //  type:'hbox'
                    //},
                    bodyStyle: 'background: url(images/aboutGiro.png) no-repeat; background-size: cover',

                    items: [
                        {
                            xtype: 'label',
                            anchor: '100%',
                            name: 'userId',
                            text: 'My awesome field',
                            padding: '20 0 0 5'
                        },
                        //{
                        //    xtype: 'label',
                        //    anchor: '100%',
                        //    name:'userId',
                        //    text: '',
                        //    padding: '22 0 0 10'
                        //},
                        {
                            xtype: 'label',
                            anchor: '100%',
                            name: 'userId',
                            text: 'Version: 2.6 \n Fecha de lanzamiento: 20/06/2016 \n Fecha de actualización: 22/06/2016 \n Tesla Technologies S.A.C.',
                            padding: '24 0 0 20'
                        }
                        //,
                        //{
                        //    xtype: 'label',
                        //    anchor: '100%',
                        //    name:'userId',
                        //    text: '',
                        //    padding: '28 0 0 30'
                        //}
                    ],
                    buttons: [
                        {
                            text: 'Salir',
                            scope: this,
                            handler: this.close,
                            iconCls: 'logout'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }
});
