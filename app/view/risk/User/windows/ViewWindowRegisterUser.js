Ext.define("DukeSource.view.risk.User.windows.ViewWindowRegisterUser", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowRegisterUser",
  width: 700,
  layout: "fit",
  title: "Registro de Usuario",
  border: false,
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          flex: 1,
          layout: {
            type: "vbox",
            align: "stretch"
          },
          fieldDefaults: {
            labelCls: "changeSizeFontToEightPt",
            fieldCls: "changeSizeFontToEightPt"
          },
          bodyPadding: 10,
          title: "",
          items: [
            {
              xtype: "fieldset",
              flex: 0.2,
              title: "Informaci&oacute;n adicional",
              padding: 5,
              collapsible: true,
              layout: {
                type: "hbox"
              },
              items: [
                {
                  xtype: "ViewComboCategory",
                  flex: 1,
                  fieldLabel: "Categor&iacute;a",
                  readOnly: true,
                  labelWidth: 100,
                  fieldCls: "readOnlyText",
                  allowBlank: false,
                  msgTarget: "side",
                  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  name: "category",
                  itemId: "category"
                },
                {
                  xtype: "textfield",
                  flex: 1.1,
                  padding: "0 0 0 10",
                  labelWidth: 100,
                  fieldLabel: "Region",
                  readOnly: true,
                  fieldCls: "readOnlyText",
                  msgTarget: "side",
                  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  itemId: "descriptionRegion",
                  name: "descriptionRegion"
                }
              ]
            },
            {
              xtype: "fieldset",
              layout: {
                type: "hbox",
                align: "stretch"
              },
              title: "Registro",
              padding: 5,
              items: [
                {
                  xtype: "textfield",
                  hidden: true,
                  name: "userName",
                  itemId: "userName"
                },
                {
                  xtype: "textfield",
                  hidden: true,
                  name: "username",
                  itemId: "username",
                  value: "Id"
                },
                {
                  xtype: "textfield",
                  hidden: true,
                  name: "idUserRole",
                  itemId: "idUserRole"
                },
                {
                  xtype: "textfield",
                  hidden: true,
                  name: "salted",
                  itemId: "salted"
                },
                {
                  xtype: "container",
                  flex: 1,
                  layout: {
                    type: "anchor"
                  },
                  items: [
                    {
                      xtype: "UpperCaseTextFieldObligatory",
                      anchor: "100%",
                      fieldLabel: "Nombres",
                      fieldCls: "obligatoryTextField",
                      name: "firstName",
                      itemId: "firstName",
                      labelWidth: 100
                    },
                    {
                      xtype: "UpperCaseTextFieldObligatory",
                      fieldCls: "obligatoryTextField",
                      anchor: "100%",
                      fieldLabel: "Ap.paterno",
                      name: "surnamePat",
                      itemId: "surnamePat",
                      labelWidth: 100
                    },
                    {
                      xtype: "UpperCaseTextFieldObligatory",
                      fieldCls: "obligatoryTextField",
                      anchor: "100%",
                      fieldLabel: "Ap. materno",
                      name: "surnameMat",
                      labelWidth: 100
                    },
                    {
                      xtype: "UpperCaseTextFieldObligatory",
                      fieldLabel: "Login",
                      anchor: "100%",
                      name: "login",
                      itemId: "login",
                      labelWidth: 100,
                      listeners: {
                        blur: function(d) {
                          if (d.getValue().length === 0) return false;
                          DukeSource.global.DirtyView.existUserByUserName(
                            d,
                            "http://localhost:9000/giro/verificationUserExist.htm",
                            d,
                            me.down("#numberDocument")
                          );
                        }
                      }
                    },
                    {
                      xtype: "textfield",
                      action: "searchDataUser",
                      anchor: "100%",
                      fieldLabel: "Nro.documento",
                      name: "numberDocument",
                      itemId: "numberDocument",
                      allowBlank: true,
                      msgTarget: "side",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      labelWidth: 100
                    },
                    {
                      xtype: "UpperCaseTextFieldObligatory",
                      anchor: "100%",
                      labelWidth: 100,
                      fieldLabel: "Email",
                      name: "email",
                      fieldCls: "obligatoryTextField",
                      itemId: "email",
                      vtype: "email",
                      listeners: {
                        blur: function(d) {
                          var field = me.down("#username");
                          if (
                            me.down("button[action=saveUserRisk]").getText() ==
                            "Modificar"
                          )
                            return false;
                          var position = d.getValue().indexOf("@");
                          if (position == -1) return false;
                          if (position > 15) position = 15;
                          field.setValue(d.getValue().substr(0, position));
                          me.down("#userName").setValue(
                            d.getValue().substr(0, position)
                          );
                          DukeSource.global.DirtyView.existUserByUserName(
                            field,
                            "http://localhost:9000/giro/verificationUserExist.htm",
                            d,
                            me.down("ViewComboAgency")
                          );
                        }
                      }
                    }
                  ]
                },
                {
                  xtype: "container",
                  flex: 1.1,
                  padding: "0 0 0 10",
                  layout: {
                    type: "anchor"
                  },
                  items: [
                    {
                      xtype: "ViewComboAgency",
                      store: Ext.create(
                        "DukeSource.store.risk.User.combos.StoreComboAgency"
                      ),
                      anchor: "100%",
                      fieldLabel: "Agencia",
                      fieldCls: "obligatoryTextField",
                      allowBlank: false,
                      msgTarget: "side",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      name: "agency",
                      labelWidth: 100,
                      listeners: {
                        select: function(cbo) {
                          var item = cbo.findRecord(
                            cbo.valueField,
                            cbo.getValue()
                          );
                          me.down("#descriptionRegion").setValue(
                            item.data["descriptionRegion"]
                          );
                        }
                      }
                    },
                    {
                      xtype: "ViewComboWorkArea",
                      plugins: ["ComboSelectCount"],
                      labelWidth: 100,
                      queryMode: "remote",
                      name: "workArea",
                      anchor: "100%",
                      fieldCls: "obligatoryTextField",
                      allowBlank: false,
                      msgTarget: "side",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      fieldLabel: "Area"
                    },
                    {
                      xtype: "ViewComboJobPlace",
                      anchor: "100%",
                      labelWidth: 100,
                      fieldLabel: "Cargo",
                      plugins: ["ComboSelectCount"],
                      queryMode: "remote",
                      allowBlank: false,
                      msgTarget: "side",
                      fieldCls: "obligatoryTextField",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      name: "idJobPlace"
                    },
                    {
                      xtype: "combobox",
                      anchor: "100%",
                      fieldLabel: "Rol",
                      name: "idRole",
                      itemId: "idRole",
                      fieldCls: "obligatoryTextField",
                      labelWidth: 100,
                      editable: false,
                      forceSelection: true,
                      displayField: "description",
                      valueField: "idRole",
                      fieldStyle: "text-transform:uppercase",
                      store: {
                        fields: ["idRole", "description"],
                        pageSize: 999,
                        proxy: {
                          actionMethods: {
                            create: "POST",
                            read: "POST",
                            update: "POST"
                          },
                          type: "ajax",
                          url:
                            "http://localhost:9000/giro/showListRolesActivesComboBox.htm",
                          extraParams: {
                            propertyOrder: "description"
                          },
                          reader: {
                            type: "json",
                            root: "data",
                            successProperty: "success"
                          }
                        }
                      },
                      listeners: {
                        select: function(cbo) {
                          var mod = cbo.findRecord(
                            cbo.valueField,
                            cbo.getValue()
                          );
                          me.down("ViewComboCategory").setValue(
                            mod.raw.category
                          );
                        }
                      }
                    },
                    {
                      xtype: "container",
                      anchor: "100%",
                      hidden: true,
                      height: 26,
                      layout: {
                        type: "hbox"
                      },
                      items: [
                        {
                          xtype: "timefield",
                          flex: 2,
                          fieldLabel: "Hora de inicio",
                          name: "hourStart",
                          labelWidth: 100
                        },
                        {
                          xtype: "timefield",
                          fieldLabel: "Hora fin",
                          name: "hourEnd",
                          padding: "0 0 0 10",
                          flex: 1.5,
                          labelWidth: 60
                        }
                      ]
                    },
                    {
                      xtype: "UpperCaseTextField",
                      fieldLabel: "Contraseña",
                      inputType: "password",
                      allowBlank: true,
                      msgTarget: "side",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      name: "password",
                      itemId: "password",
                      anchor: "100%",
                      labelWidth: 100
                    },
                    {
                      xtype: "UpperCaseTextField",
                      fieldLabel: "Repita contraseña",
                      inputType: "password",
                      allowBlank: true,
                      msgTarget: "side",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      name: "rePassword",
                      itemId: "rePassword",
                      anchor: "100%",
                      labelWidth: 100,
                      listeners: {
                        specialkey: function(f, e) {
                          if (
                            Ext.ComponentQuery.query(
                              "ViewPanelUsers UpperCaseTextField[name=password]"
                            )[0].getValue() ==
                            Ext.ComponentQuery.query(
                              "ViewPanelUsers UpperCaseTextField[fieldLabel=REINGRESE PASSWORD]"
                            )[0].getValue()
                          ) {
                            DukeSource.global.DirtyView.focusEventEnterObligatory(
                              f,
                              e,
                              Ext.ComponentQuery.query(
                                "ViewPanelUsers button[action=saveUserRisk]"
                              )[0]
                            );
                          } else {
                            Ext.MessageBox.show({
                              title:
                                DukeSource.global.GiroMessages.TITLE_WARNING,
                              msg:
                                "Los password no coinciden, intentelo nuevamente",
                              icon: Ext.Msg.WARNING,
                              buttons: Ext.MessageBox.OK,
                              closable: false,
                              fn: function(btn) {
                                if (btn == "ok") {
                                  var pass = Ext.ComponentQuery.query(
                                    "ViewPanelUsers UpperCaseTextField[fieldLabel=REINGRESE PASSWORD]"
                                  )[0];
                                  pass.reset();
                                  pass.focus(false, 100);
                                }
                              }
                            });
                          }
                        }
                      }
                    },
                    {
                      xtype: "combobox",
                      anchor: "100%",
                      labelWidth: 100,
                      name: "state",
                      hidden: true,
                      itemId: "state",
                      fieldLabel: "Estado",
                      displayField: "description",
                      valueField: "state",
                      value: "S",
                      store: {
                        fields: ["state", "description"],
                        data: [
                          {
                            state: "S",
                            description: "Activo"
                          },
                          {
                            state: "N",
                            description: "Inactivo"
                          }
                        ]
                      }
                    },
                    {
                      xtype: "UpperCaseTextField",
                      fieldLabel: "Password email",
                      inputType: "password",
                      hidden: true,
                      allowBlank: true,
                      msgTarget: "side",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      name: "passwordEmail",
                      itemId: "passwordEmail",
                      anchor: "100%",
                      labelWidth: 100
                    }
                  ]
                }
              ]
            }
          ]
        }
      ],
      buttonAlign: "center",
      buttons: [
        {
          xtype: "button",
          iconCls: "save",
          scale: "medium",
          text: "Guardar",
          action: "saveUserRisk"
        },
        {
          xtype: "button",
          iconCls: "logout",
          text: "Salir",
          scale: "medium",
          scope: this,
          handler: this.close
        }
      ]
    });

    me.callParent(arguments);
  }
});
