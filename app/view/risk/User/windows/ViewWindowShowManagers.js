Ext.require(["Ext.ux.CheckColumn"]);
Ext.define("ModelShowManagers", {
  extend: "Ext.data.Model",
  fields: [
    "userName",
    "fullName",
    "category",
    "descriptionCategory",
    "descriptionJobPlace"
  ]
});
var storeShowManagers = Ext.create("Ext.data.Store", {
  model: "ModelShowManagers",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
storeShowManagers.load();

Ext.define("DukeSource.view.risk.User.windows.ViewWindowShowManagers", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowShowManagers",
  height: 500,
  width: 800,
  border: false,
  layout: {
    type: "fit"
  },
  title: "Gestores",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          hidden: true,
          bodyPadding: 10,
          items: [
            {
              xtype: "textfield",
              itemId: "id",
              name: "id",
              hidden: true
            }
          ]
        },
        {
          xtype: "grid",
          name: "gridShowManager",
          itemId: "gridShowManager",
          store: storeShowManagers,
          flex: 1,
          titleAlign: "center",
          columns: [
            {
              xtype: "rownumberer",
              width: 50,
              sortable: false
            },
            {
              dataIndex: "fullName",
              flex: 280,
              text: "Nombre"
            },
            {
              dataIndex: "descriptionCategory",
              flex: 130,
              text: "Categoria",
              align: "center"
            },
            {
              dataIndex: "descriptionJobPlace",
              flex: 250,
              text: "Cargo"
            }
          ],
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: storeShowManagers,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            items: [
              "-",
              {
                xtype:"UpperCaseTrigger",
                fieldLabel: "FILTRAR",
                action: "filterGridShowManager",
                labelWidth: 60,
                width: 300
              }
            ],
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          },
          padding: "2 2 2 2",
          tbar: [
            {
              xtype: "button",
              iconCls: "add",
              action: "addManagerAssign"
            },
            {
              xtype: "button",
              iconCls: "collaborator",
              handler: function(button) {
                var window = button.up("window");
                var records = window
                  .down("#gridShowManager")
                  .getSelectionModel()
                  .getSelection()[0];
                if (records !== undefined) {
                  var countryWindow = Ext.create(
                    "DukeSource.view.risk.User.windows.ViewWindowShowCollaborators",
                    {
                      modal: true
                    }
                  );
                  countryWindow.show();
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_WARNING,
                    "Selecione por favor un REGISTRO",
                    Ext.Msg.ERROR
                  );
                }
              }
            },
            {
              xtype: "button",
              iconCls: "delete",
              action: "deleteManagerAssign"
            },
            {
              xtype: "UpperCaseTextField",
              action: "searchCollaboratorOfBoss",
              name: "textSearchShowManager",
              itemId: "textSearchShowManager",
              fieldLabel: "BUSCAR",
              labelWidth: 60,
              width: 200
            }
          ],
          listeners: {
            render: function() {
              var viewPanel = Ext.ComponentQuery.query(
                "ViewPanelAssignCollaborator"
              )[0];
              var mainGrid = viewPanel
                .down("#gridManagerAssign")
                .getSelectionModel()
                .getSelection()[0];
              var userName = mainGrid.get("userName");
              var me = this;
              me.store.getProxy().extraParams = {
                userName: userName,
                category: "GESTOR"
              };
              me.store.getProxy().url =
                "http://localhost:9000/giro/showListCollaboratorActives.htm";
              me.down("pagingtoolbar").moveFirst();
            }
          }
        }
      ],
      buttons: [
        {
          text: "Salir",
          scope: this,
          scale: "medium",
          handler: this.close,
          iconCls: "logout"
        }
      ],
      buttonAlign: "center"
    });
    me.callParent(arguments);
  }
});
