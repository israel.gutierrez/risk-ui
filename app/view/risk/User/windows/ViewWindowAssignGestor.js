Ext.require(["Ext.ux.CheckColumn"]);
Ext.define("ModelGestor", {
  extend: "Ext.data.Model",
  fields: [
    "userName",
    "fullName",
    "category",
    "descriptionCategory",
    "descriptionJobPlace"
  ]
});
var sm = Ext.create("Ext.selection.CheckboxModel");
var storeGestor = Ext.create("Ext.data.Store", {
  model: "ModelGestor",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListGestorAvailableActives.htm",
    extraParams: {
      category: "GESTOR"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
storeGestor.load();

Ext.define("DukeSource.view.risk.User.windows.ViewWindowAssignGestor", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowAssignGestor",
  height: 500,
  width: 800,
  border: false,
  layout: {
    type: "fit"
  },
  title: "Asignar",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          hidden: true,
          bodyPadding: 10,
          items: [
            {
              xtype: "textfield",
              itemId: "id",
              name: "id",
              hidden: true
            }
          ]
        },
        {
          xtype: "gridpanel",
          store: storeGestor,
          flex: 1,
          titleAlign: "center",
          columns: [
            {
              xtype: "rownumberer",
              width: 50,
              sortable: false
            },
            {
              dataIndex: "userName",
              width: 90,
              text: "Usuario"
            },
            {
              dataIndex: "fullName",
              flex: 280,
              text: "Nombre"
            },
            {
              dataIndex: "descriptionCategory",
              flex: 130,
              text: "Categoria",
              align: "center"
            },
            {
              dataIndex: "descriptionJobPlace",
              flex: 250,
              text: "Cargo"
            }
          ],
          selModel: sm,
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: storeGestor,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            items: [
              "-",
              {
                xtype:"UpperCaseTrigger",
                fieldLabel: "FILTRAR",
                action: "filterGridManagerAvailable",
                labelWidth: 60,
                width: 300
              }
            ],
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          },
          listeners: {
            render: function() {
              var me = this;
              me.store.getProxy().extraParams = { category: "GESTOR" };
              me.store.getProxy().url =
                "http://localhost:9000/giro/showListGestorAvailableActives.htm";
              me.down("pagingtoolbar").moveFirst();
            }
          }
        }
      ],
      buttons: [
        {
          text: "Guardar",
          scale: "medium",
          action: "saveManagerToAnalyst",
          iconCls: "save"
        },
        {
          text: "Salir",
          scale: "medium",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ],
      buttonAlign: "center"
    });
    me.callParent(arguments);
  }
});
