Ext.require(["Ext.ux.CheckColumn"]);
Ext.define("ModelShowCollaborators", {
  extend: "Ext.data.Model",
  fields: [
    "userName",
    "fullName",
    "category",
    "descriptionCategory",
    "descriptionJobPlace"
  ]
});
var sm = Ext.create("Ext.selection.CheckboxModel");
var storeShowCollaborators = Ext.create("Ext.data.Store", {
  model: "ModelShowCollaborators",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
storeShowCollaborators.load();

Ext.define("DukeSource.view.risk.User.windows.ViewWindowShowCollaborators", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowShowCollaborators",
  height: 500,
  width: 800,
  border: false,
  layout: {
    type: "fit"
  },
  title: "COLABORADORES ASIGNADOS",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          hidden: true,
          bodyPadding: 10,
          items: [
            {
              xtype: "textfield",
              itemId: "id",
              name: "id",
              hidden: true
            }
          ]
        },
        {
          xtype: "gridpanel",
          name: "gridShowCollaborator",
          itemId: "gridShowCollaborator",
          store: storeShowCollaborators,
          flex: 1,
          titleAlign: "center",
          columns: [
            {
              xtype: "rownumberer",
              width: 50,
              sortable: false
            },
            //{
            //    dataIndex: 'userName',
            //    width: 90,
            //    text: 'USUARIO'
            //},
            {
              dataIndex: "fullName",
              flex: 280,
              text: "NOMBRE"
            },
            {
              dataIndex: "descriptionCategory",
              flex: 130,
              text: "CATEGORIA",
              align: "center"
            },
            {
              dataIndex: "descriptionJobPlace",
              flex: 250,
              text: "CARGO"
            }
          ],
          selModel: sm,
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: storeShowCollaborators,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            items: [
              "-",
              {
                xtype:"UpperCaseTrigger",
                fieldLabel: "FILTRAR",
                action: "filterGridShowCollaborator",
                labelWidth: 60,
                width: 300
              }
            ],
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          },
          padding: "2 2 2 2",
          tbar: [
            {
              xtype: "button",
              iconCls: "add",
              action: "addCollaboratorAssign"
            },
            {
              xtype: "button",
              iconCls: "delete",
              action: "deleteCollaboratorAssign"
            },
            {
              xtype: "UpperCaseTextField",
              action: "searchCollaboratorOfManager",
              name: "textSearchShowCollaborator",
              itemId: "textSearchShowCollaborator",
              fieldLabel: "BUSCAR",
              labelWidth: 60,
              width: 200
            }
          ],
          listeners: {
            render: function() {
              var viewPanel = Ext.ComponentQuery.query(
                "ViewPanelAssignCollaborator"
              )[0];
              var mainGrid = viewPanel
                .down("#gridManagerAssign")
                .getSelectionModel()
                .getSelection()[0];
              var me = this;
              var userName;
              if (mainGrid.get("category") == "GESTOR") {
                userName = mainGrid.get("userName");
              } else {
                var viewManager = Ext.ComponentQuery.query(
                  "ViewWindowShowManagers"
                )[0];
                var managerGrid = viewManager
                  .down("#gridShowManager")
                  .getSelectionModel()
                  .getSelection()[0];
                userName = managerGrid.get("userName");
              }
              me.store.getProxy().extraParams = {
                userName: userName,
                category: "COLABORADOR"
              };
              me.store.getProxy().url =
                "http://localhost:9000/giro/showListCollaboratorActives.htm";
              me.down("pagingtoolbar").moveFirst();
            }
          }
        }
      ],
      buttons: [
        //{
        //    text: 'GUARDAR',
        //    action: 'saveManagerToAnalyst',
        //    iconCls: 'save'
        //},
        {
          text: "SALIR",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ]
    });
    me.callParent(arguments);
  }
});
