Ext.define("ModelWindowAccess", {
  extend: "Ext.data.Model",
  fields: [
    { name: "idAccess", type: "string" },
    { name: "valueAccess", type: "string" },
    { name: "typeAccess", type: "string" },
    { name: "module", type: "string" },
    { name: "codeAccess", type: "string" },
    { name: "icon", type: "string" },
    { name: "nameClass", type: "string" },
    { name: "nameTab", type: "string" },
    { name: "closable", type: "string" },
    { name: "permitSubTabs", type: "string" },
    { name: "typeItem", type: "string" },
    { name: "nameView", type: "string" },
    { name: "state", type: "string" }
  ]
});
Ext.define("DukeSource.view.risk.User.windows.ViewWindowAccess", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowAccess",
  height: 208,
  width: 479,
  border: false,
  layout: {
    type: "fit"
  },
  title: "TITLE WINDOW",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          model: "ModelWindowAccess",
          bodyPadding: 10,
          title: "",
          items: [
            {
              xtype: "UpperCaseTextFieldReadOnly",
              anchor: "60%",
              name: "idAccess",
              value: "id",
              fieldLabel: "CODIGO"
            },
            {
              xtype: "UpperCaseTextFieldObligatory",
              anchor: "100%",
              name: "valueAccess",
              fieldLabel: "VALOR ACCESO"
            },
            {
              xtype: "UpperCaseTextFieldObligatory",
              anchor: "100%",
              name: "typeAccess",
              fieldLabel: "TIPO ACCESO"
            },
            {
              xtype: "ViewComboTypeAccess",
              anchor: "100%",
              allowBlank: false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              name: "module",
              fieldLabel: "MODULO"
            },
            {
              xtype: "UpperCaseTextFieldObligatory",
              anchor: "100%",
              name: "codeAccess",
              fieldLabel: "CODIGO ACCESO"
            },
            {
              xtype: "UpperCaseTextFieldObligatory",
              anchor: "100%",
              name: "icon",
              fieldLabel: "ICONO"
            },
            {
              xtype: "UpperCaseTextFieldObligatory",
              anchor: "100%",
              name: "nameClass",
              fieldLabel: "NOMBRE CLASE"
            },
            {
              xtype: "UpperCaseTextFieldObligatory",
              anchor: "100%",
              name: "nameTab",
              fieldLabel: "NOMBRE TAB"
            },
            {
              xtype: "UpperCaseTextFieldObligatory",
              anchor: "100%",
              name: "closable",
              fieldLabel: "CLOSABLE"
            },
            {
              xtype: "UpperCaseTextFieldObligatory",
              anchor: "100%",
              name: "permitSubTabs",
              fieldLabel: "PERMITSUBTABS"
            },
            {
              xtype: "UpperCaseTextFieldObligatory",
              anchor: "100%",
              name: "typeItem",
              fieldLabel: "TIPO ITEM"
            },
            {
              xtype: "UpperCaseTextFieldObligatory",
              anchor: "100%",
              name: "nameView",
              fieldLabel: "NAME VISTA"
            },
            {
              xtype: "ViewComboYesNo",
              anchor: "60%",
              name: "state",
              allowBlank: false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              fieldLabel: "ESTADO"
            }
          ]
        }
      ],
      buttons: [
        {
          text: "GUARDAR",
          action: "saveAccess",
          iconCls: "save"
        },
        {
          text: "SALIR",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ]
    });

    me.callParent(arguments);
  }
});
