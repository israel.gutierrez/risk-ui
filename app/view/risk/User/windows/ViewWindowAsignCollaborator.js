Ext.require(["Ext.ux.CheckColumn"]);
Ext.define("ModelCollaborator", {
  extend: "Ext.data.Model",
  fields: ["userName", "fullName", "category"]
});
var sm = Ext.create("Ext.selection.CheckboxModel");
var storeCollaborator = Ext.create("Ext.data.Store", {
  model: "ModelCollaborator",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListGestorAvailableActives.htm",
    extraParams: {
      category: "COLABORADOR"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
storeCollaborator.load();
Ext.define("DukeSource.view.risk.User.windows.ViewWindowAsignCollaborator", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowAsignCollaborator",
  height: 400,
  width: 700,
  border: false,
  layout: {
    type: "fit"
  },
  title: "ASIGNAR COLABORADOR",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          hidden: true,
          bodyPadding: 10,
          items: [
            {
              xtype: "textfield",
              itemId: "id",
              name: "id",
              hidden: true
            }
          ]
        },
        {
          xtype: "gridpanel",
          store: storeCollaborator,
          flex: 1,
          //                    title: 'GESTORES NO ASIGNADOS',
          titleAlign: "center",
          columns: [
            {
              xtype: "rownumberer",
              width: 50,
              sortable: false
            },
            {
              dataIndex: "userName",
              width: 90,
              text: "USUARIO"
            },
            {
              dataIndex: "fullName",
              flex: 1,
              text: "NOMBRE"
            },
            {
              dataIndex: "category",
              flex: 1,
              text: "CATEGORIA"
            }
          ],
          selModel: sm,
          //                    bbar:{
          //                        xtype: 'pagingtoolbar',
          //                        pageSize: 50,
          //                        store: storeGestor
          //                    },
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: storeCollaborator,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          },
          listeners: {
            render: function() {
              var me = this;
              me.store.getProxy().extraParams = { category: "COLABORADOR" };
              me.store.getProxy().url =
                "http://localhost:9000/giro/showListGestorAvailableActives.htm";
              me.down("pagingtoolbar").moveFirst();
            }
          }
        }
      ],
      buttons: [
        {
          text: "GUARDAR",
          action: "saveGestorToAnalist",
          iconCls: "save"
        },
        {
          text: "SALIR",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ]
    });

    me.callParent(arguments);
  }
});
