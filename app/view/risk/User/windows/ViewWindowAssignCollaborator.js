Ext.require(["Ext.ux.CheckColumn"]);
Ext.define("ModelCollaborator", {
  extend: "Ext.data.Model",
  fields: [
    "userName",
    "fullName",
    "category",
    "descriptionCategory",
    "descriptionJobPlace"
  ]
});
var sm = Ext.create("Ext.selection.CheckboxModel");
var storeCollaborator = Ext.create("Ext.data.Store", {
  model: "ModelCollaborator",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListGestorAvailableActives.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
storeCollaborator.load();
Ext.define("DukeSource.view.risk.User.windows.ViewWindowAssignCollaborator", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowAssignCollaborator",
  height: 500,
  width: 800,
  border: false,
  layout: {
    type: "fit"
  },
  title: "ASIGNAR COLABORADOR",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          hidden: true,
          bodyPadding: 10,
          items: [
            {
              xtype: "textfield",
              itemId: "id",
              name: "id",
              hidden: true
            }
          ]
        },
        {
          xtype: "gridpanel",
          store: storeCollaborator,
          flex: 1,
          titleAlign: "center",
          columns: [
            {
              xtype: "rownumberer",
              width: 50,
              sortable: false
            },
            {
              dataIndex: "userName",
              width: 90,
              text: "USUARIO"
            },
            {
              dataIndex: "fullName",
              flex: 350,
              text: "NOMBRE"
            },
            {
              dataIndex: "descriptionCategory",
              flex: 150,
              text: "CATEGORIA",
              align: "center"
            },
            {
              dataIndex: "descriptionJobPlace",
              flex: 350,
              text: "CARGO"
            }
          ],
          selModel: sm,
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: storeCollaborator,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            items: [
              "-",
              {
                xtype:"UpperCaseTrigger",
                fieldLabel: "FILTRAR",
                action: "filterGridCollaboratorAvailable",
                labelWidth: 60,
                width: 300
              }
            ],
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          },
          listeners: {
            render: function() {
              var me = this;
              me.store.getProxy().extraParams = { category: "COLABORADOR" };
              me.store.getProxy().url =
                "http://localhost:9000/giro/showListGestorAvailableActives.htm";
              me.down("pagingtoolbar").moveFirst();
            }
          }
        }
      ],
      buttons: [
        {
          text: "GUARDAR",
          action: "saveCollaboratorToManager",
          iconCls: "save"
        },
        {
          text: "SALIR",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ]
    });

    me.callParent(arguments);
  }
});
