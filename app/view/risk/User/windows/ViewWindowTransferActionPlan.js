Ext.define("DukeSource.view.risk.User.windows.ViewWindowTransferActionPlan", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowTransferActionPlan",
  height: 150,
  width: 550,
  border: false,
  layout: {
    type: "fit"
  },
  title: "TRANSFERENCIA DE PLANES DE ACCION",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          items: [
            {
              xtype: "container",
              flex: 1,
              margin: "0 5 0 5",
              padding: 3,
              layout: {
                align: "stretch",
                type: "hbox"
              },
              items: [
                {
                  xtype: "textfield",
                  hidden: true,
                  name: "idUserOld",
                  itemId: "idUserOld"
                },
                {
                  xtype: "UpperCaseTextFieldReadOnly",
                  flex: 1,
                  allowBlank: false,
                  fieldLabel: "USUARIO ORIGEN",
                  labelWidth: 130,
                  name: "fullNameUserOld"
                },
                {
                  xtype: "button",
                  width: 26,
                  iconCls: "search",
                  itemId: "searchUserOld",
                  handler: function(button) {
                    var panel = button.up("panel");
                    var window = Ext.create(
                      "DukeSource.view.risk.util.search.SearchUser",
                      {
                        category: "ANALISTA",
                        modal: true
                      }
                    ).show();
                    window.down("grid").on("itemdblclick", function() {
                      panel.down("textfield[name=idUserOld]").setValue(
                        window
                          .down("grid")
                          .getSelectionModel()
                          .getSelection()[0]
                          .get("userName")
                      );
                      panel
                        .down(
                          "UpperCaseTextFieldReadOnly[name=fullNameUserOld]"
                        )
                        .setValue(
                          window
                            .down("grid")
                            .getSelectionModel()
                            .getSelection()[0]
                            .get("fullName")
                        );
                      me.down("button[itemId=searchUserOld]").focus(false, 100);
                      window.close();
                    });
                  }
                }
              ]
            },
            {
              xtype: "container",
              flex: 1,
              margin: "0 5 0 5",
              padding: 3,
              layout: {
                align: "stretch",
                type: "hbox"
              },
              items: [
                {
                  xtype: "textfield",
                  hidden: true,
                  name: "idUserNew",
                  itemId: "idUserNew"
                },
                {
                  xtype: "UpperCaseTextFieldReadOnly",
                  flex: 1,
                  allowBlank: false,
                  labelWidth: 130,
                  fieldLabel: "USUARIO DESTINO",
                  name: "fullNameUserNew"
                },
                {
                  xtype: "button",
                  width: 26,
                  iconCls: "search",
                  itemId: "searchUserNew",
                  handler: function(button) {
                    var panel = button.up("panel");
                    var window = Ext.create(
                      "DukeSource.view.risk.util.search.SearchUser",
                      {
                        category: "ANALISTA",
                        modal: true
                      }
                    ).show();
                    window.down("grid").on("itemdblclick", function() {
                      panel.down("textfield[name=idUserNew]").setValue(
                        window
                          .down("grid")
                          .getSelectionModel()
                          .getSelection()[0]
                          .get("userName")
                      );
                      panel
                        .down(
                          "UpperCaseTextFieldReadOnly[name=fullNameUserNew]"
                        )
                        .setValue(
                          window
                            .down("grid")
                            .getSelectionModel()
                            .getSelection()[0]
                            .get("fullName")
                        );
                      me.down("button[itemId=searchUserNew]").focus(false, 100);
                      window.close();
                    });
                  }
                }
              ]
            }
          ]
        }
      ],
      buttons: [
        {
          text: "GUARDAR",
          scale: "medium",
          action: "saveTransferActivities",
          iconCls: "save",
          handler: function(btn) {
            var windows = btn.up("window");
            var form = windows.down("form");
            if (form.getForm().isValid()) {
              DukeSource.lib.Ajax.request({
                method: "POST",
                url: "http://localhost:9000/giro/processTransferActionPlan.htm",
                params: {
                  oldUser: form.down("#idUserOld").getValue(),
                  newUser: form.down("#idUserNew").getValue()
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      response.mensaje,
                      Ext.Msg.INFO
                    );
                    windows.close();
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      response.mensaje,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function() {}
              });
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
                Ext.Msg.ERROR
              );
            }
          }
        },
        {
          text: "SALIR",
          scope: this,
          scale: "medium",
          handler: this.close,
          iconCls: "logout"
        }
      ]
    });
    me.callParent(arguments);
  }
});
