Ext.define('DukeSource.view.risk.User.windows.ViewWindowManagerOfCollaborator', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowManagerOfCollaborator',
    height: 200,
    width: 400,
    border: false,
    layout: {
        type: 'fit'
    },
    title: 'ANALISTA/GESTOR/COORDINADOR DEL COLABORADOR',
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    padding: '10 5 5 5',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'UpperCaseTextFieldReadOnly',
                            anchor: '100%',
                            name: 'nameManagerUser',
                            itemId: 'nameManagerUser',
                            fieldLabel: 'NOMBRES',
                            fieldCls: 'readOnlyText',
                            labelWidth: 100
                        },
                        {
                            xtype: 'UpperCaseTextFieldReadOnly',
                            anchor: '100%',
                            name: 'nameAgencyUser',
                            itemId: 'nameAgencyUser',
                            fieldLabel: 'AGENCIA',
                            fieldCls: 'readOnlyText',
                            labelWidth: 100
                        },
                        {
                            xtype: 'UpperCaseTextFieldReadOnly',
                            anchor: '100%',
                            name: 'nameWorkAreaUser',
                            itemId: 'nameWorkAreaUser',
                            fieldLabel: 'AREA',
                            fieldCls: 'readOnlyText',
                            labelWidth: 100
                        },
                        {
                            xtype: 'UpperCaseTextFieldReadOnly',
                            anchor: '100%',
                            name: 'nameJobPlaceUser',
                            itemId: 'nameJobPlaceUser',
                            fieldLabel: 'CARGO',
                            fieldCls: 'readOnlyText',
                            labelWidth: 100
                        },
                        {
                            xtype: 'UpperCaseTextFieldReadOnly',
                            anchor: '100%',
                            name: 'nameCategoryUser',
                            itemId: 'nameCategoryUser',
                            fieldLabel: 'CATEGORIA',
                            fieldCls: 'readOnlyText',
                            labelWidth: 100
                        }
                    ],
                    buttons: [
                        {
                            text: 'Salir',
                            scope: this,
                            handler: this.close,
                            iconCls: 'logout'
                        }
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
});