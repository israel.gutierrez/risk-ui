Ext.define("ModelSearchUserAssigned", {
  extend: "Ext.data.Model",
  fields: [
    "category",
    "checked",
    "jobPlace",
    "descriptionJobPlace",
    "workArea",
    "descriptionWorkArea",
    "email",
    "fullName",
    "leaf",
    "level",
    "username",
    "user"
  ]
});

var StoreSearchUserAssigned = Ext.create("Ext.data.Store", {
  model: "ModelSearchUserAssigned",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.User.windows.WindowUserAssigned", {
  extend: "Ext.window.Window",
  alias: "widget.WindowUserAssigned",
  layout: {
    align: "stretch",
    type: "vbox"
  },
  anchorSize: 100,
  title: "USUARIOS ASIGNADOS",
  titleAlign: "center",
  width: 900,
  height: 500,
  border: false,
  initComponent: function() {
    var me = this;
    var deleteAction = me.deleteAction;
    var auditAction = me.auditAction;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "fieldset",
          itemId: "headerWindow",
          padding: 5,
          margin: 5,
          layout: "hbox",
          border: false,
          style: {
            borderColor: "#78abf5"
          },
          height: 60,
          items: [
            {
              xtype: "textfield",
              flex: 3,
              fieldLabel: "NOMBRE",
              name: "name",
              itemId: "name",
              fieldCls: "readOnlyText",
              readOnly: true
            },
            {
              xtype: "textfield",
              fieldLabel: "CODIGO",
              padding: "0 0 0 5",
              readOnly: true,
              labelWidth: 60,
              flex: 1,
              fieldCls: "codeIncident",
              name: "code",
              itemId: "code"
            }
          ]
        },
        {
          xtype: "gridpanel",
          columnLines: true,
          store: StoreSearchUserAssigned,
          flex: 1,
          titleAlign: "center",
          columns: [
            {
              xtype: "rownumberer",
              width: 25,
              sortable: false
            },
            {
              header: "USUARIO",
              dataIndex: "username",
              align: "center",
              width: 120
            },
            {
              dataIndex: "fullName",
              align: "center",
              width: 300,
              text: "NOMBRE COMPLETO"
            },
            {
              dataIndex: "descriptionJobPlace",
              width: 320,
              text: "CARGO"
            },
            {
              dataIndex: "category",
              align: "center",
              width: 100,
              text: "CATEGORIA"
            }
          ],
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: StoreSearchUserAssigned,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          },
          tbar: [
            {
              text: "AGREGAR",
              iconCls: "import",
              scale: "medium",
              cls: "my-btn",
              handler: function() {
                var view = Ext.create(
                  "DukeSource.view.risk.parameter.process.ViewWindowAddOwnerToProcess",
                  {
                    modal: true,
                    actionButton: "saveKriUserAssign",
                    parentView: me
                  }
                );
                view.show();
              }
            },
            {
              text: "QUITAR",
              iconCls: "export",
              scale: "medium",
              cls: "my-btn",
              action: deleteAction
            },
            "->",
            {
              text: "AUDITORIA",
              action: auditAction,
              iconCls: "auditory"
            }
          ]
        }
      ],
      buttons: [
        {
          text: "SALIR",
          scale: "medium",
          iconCls: "logout",
          handler: function(btn) {
            me.close();
          }
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});
