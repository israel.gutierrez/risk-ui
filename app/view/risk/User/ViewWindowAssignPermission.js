Ext.define("DukeSource.view.risk.User.ViewWindowAssignPermission", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowAssignPermission",
  layout: {
    align: "stretch",
    type: "vbox"
  },
  title: "ASIGNAR PERMISOS",
  titleAlign: "center",
  initComponent: function() {
    this.items = [
      {
        xtype: "container",
        flex: 1,
        layout: {
          align: "stretch",
          type: "vbox"
        },
        items: [
          {
            xtype: "label",
            text:
              "Observaciones: El permiso minimo para tener acceso a un recurso es el de 'CONSULTAR'",
            style:
              "color:#D5512C;font-family: tahoma, arial, verdana, sans-serif;font-size: 12px;text-align:right"
          }
        ],
        buttonAlign: "center"
      },
      {
        xtype: "ViewGridPermissionCheck",
        padding: "2 2 2 2",
        title: "",
        flex: 15,
        dockedItems: [
          {
            xtype: "toolbar",
            dock: "top",
            items: [
              {
                xtype: "combobox",
                allowBlank: false,
                msgTarget: "side",
                anchor: "100%",
                fieldLabel: "AGENCIA",
                name: "agency",
                itemId: "agency",
                editable: false,
                hidden: true,
                plugins: ["ComboSelectCount"],
                displayField: "description",
                valueField: "idAgency",
                fieldStyle: "text-transform:uppercase",
                store: {
                  fields: ["idAgency", "description"],
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url:
                      "http://localhost:9000/giro/showListAgencyActivesComboBox.htm",
                    extraParams: {
                      propertyOrder: "a.description",
                      start: 0,
                      limit: 9999
                    },
                    reader: {
                      type: "json",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                },
                listeners: {
                  select: function() {
                    Ext.ComponentQuery.query("ViewWindowAssignPermission")[0]
                      .down("ViewComboModule")
                      .reset();
                    Ext.ComponentQuery.query("ViewWindowAssignPermission")[0]
                      .down("ViewComboPermission")
                      .reset();
                  }
                }
              },
              {
                xtype: "ViewComboModule",
                width: 350,
                fieldLabel: "MODULO A",
                action: "searchAccessByModule"
              },
              {
                xtype: "textfield",
                name: "idRole",
                hidden: true,
                anchor: "100%",
                readOnly: true
              }
              /*{
                             xtype: 'ViewComboPermission',
                             width: 200,
                             fieldLabel: 'PERMISO A',
                             action: 'searchAccessByModuleType'
                             }*/
            ]
          }
        ]
      }
    ];
    this.buttons = [
      {
        text: "Salir",
        scope: this,
        scale: "medium",
        handler: this.close,
        iconCls: "logout"
      },
      {
        text: "Guardar",
        action: "assignPermissionToRol",
        scale: "medium",
        iconCls: "save"
      }
    ];
    this.buttonAlign = "center";

    this.callParent(arguments);
  }
});
