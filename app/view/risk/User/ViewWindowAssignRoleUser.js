Ext.define("ModelRoleAll", {
  extend: "Ext.data.Model",
  fields: ["id", "name"]
});

Ext.define("ModelRoleSelected", {
  extend: "Ext.data.Model",
  fields: ["idUserRole", "user", "role", "state", "descriptionRole"]
});
var storeRolAll = Ext.create("Ext.data.Store", {
  model: "ModelRoleAll",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListRolesActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
storeRolAll.load();

var storeRolSelected = Ext.create("Ext.data.Store", {
  model: "ModelRoleSelected",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListUserRolesActives.htm",
    extraParams: {
      propertyFind: "description",
      valueFind: "",
      propertyOrder: "description"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
storeRolSelected.load();

Ext.define("DukeSource.view.risk.User.ViewWindowAssignRoleUser", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowAssignRoleUser",
  layout: {
    align: "stretch",
    type: "vbox"
  },
  anchorSize: 100,
  title: "ASIGNAR ROL",
  titleAlign: "center",

  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          height: 45,
          padding: "2 2 2 2",
          bodyPadding: 10,
          items: [
            {
              xtype: "UpperCaseTextField",
              action: "searchRoleAll",
              anchor: "100%",
              fieldLabel: "BUSCAR ROL"
            },
            {
              xtype: "textfield",
              name: "ocultar",
              hidden: true,
              fieldLabel: "ROLE",
              hideLabel: true
            }
          ]
        },
        {
          xtype: "container",
          flex: 3,
          padding: "2 2 2 2",
          layout: {
            align: "stretch",
            type: "hbox"
          },
          items: [
            {
              xtype: "gridpanel",
              padding: "0 1 0 0",
              store: storeRolAll,
              loadMask: true,
              columnLines: true,
              flex: 1,
              title: "DISPONIBLES",
              titleAlign: "center",
              multiSelect: true,
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false
                },
                {
                  dataIndex: "id",
                  width: 60,
                  text: "CODIGO"
                },
                {
                  dataIndex: "name",
                  flex: 1,
                  text: "ROL"
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: 50,
                store: storeRolAll,
                items: [
                  {
                    xtype:"UpperCaseTrigger",
                    width: 120,
                    action: "searchGridAllRole"
                  }
                ]
              }
            },
            {
              xtype: "gridpanel",
              padding: "0 0 0 1",
              store: storeRolSelected,
              loadMask: true,
              columnLines: true,
              multiSelect: true,
              flex: 1,
              title: "ASIGNADOS",
              titleAlign: "center",
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false
                },
                {
                  dataIndex: "role",
                  width: 60,
                  text: "CODIGO"
                },
                {
                  dataIndex: "descriptionRole",
                  flex: 1,
                  text: "ROL"
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: 50,
                store: storeRolSelected
              }
            }
          ]
        }
      ],
      buttons: [
        {
          text: "SALIR",
          iconCls: "logout",
          scope: this,
          handler: this.close
        }
      ]
    });

    me.callParent(arguments);
  }
});
