Ext.define('DukeSource.view.risk.User.ViewPanelRegisterConstant', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterConstant',
    border: false,
    layout: 'fit',
    tbar: [
        {
            xtype: 'ViewComboModule',
            action: 'constantModule',
            width: 350,
            fieldLabel: 'MODULO'
        }, '-'
        ,
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDataGridConstant',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'constantAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterConstant', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
