Ext.define("ModelAgencyAll", {
  extend: "Ext.data.Model",
  fields: ["idAgency", "description"]
});
Ext.define("ModelAgencySelected", {
  extend: "Ext.data.Model",
  fields: [
    "agency",
    "descriptionAgency",
    "idUserAgency",
    "user",
    "defaultAgency",
    "state"
  ]
});
var storeAgencyAll = Ext.create("Ext.data.Store", {
  model: "ModelAgencyAll",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListUserAgenciesActives.htm",
    extraParams: {
      propertyFind: "user.username",
      valueFind: "",
      propertyOrder: "idUserAgency"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
storeAgencyAll.load();

var storeAgencySelected = Ext.create("Ext.data.Store", {
  model: "ModelAgencySelected",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListUserAgenciesActives.htm",
    extraParams: {
      propertyFind: "user.username",
      valueFind: "",
      propertyOrder: "idUserAgency"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
storeAgencySelected.load();

Ext.define("DukeSource.view.risk.User.ViewWindowAssignAgencyUser", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowAssignAgencyUser",
  layout: {
    align: "stretch",
    type: "vbox"
  },
  anchorSize: 100,
  title: "ASIGNAR AGENCIA",
  titleAlign: "center",

  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          height: 45,
          padding: "2 2 2 2",
          bodyPadding: 10,
          items: [
            {
              xtype: "UpperCaseTextField",
              action: "searchAgencyAll",
              anchor: "100%",
              labelWidth: 120,
              fieldLabel: "BUSCAR AGENCIA"
            },
            {
              xtype: "textfield",
              name: "ocultar",
              hidden: true,
              fieldLabel: "AGENCIA",
              hideLabel: true
            }
          ]
        },
        {
          xtype: "container",
          flex: 3,
          padding: "2 2 2 2",
          layout: {
            align: "stretch",
            type: "hbox"
          },
          items: [
            {
              xtype: "gridpanel",
              padding: "0 1 0 0",
              store: storeAgencyAll,
              flex: 1,
              title: "DISPONIBLES",
              titleAlign: "center",
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false
                },
                {
                  dataIndex: "idAgency",
                  width: 60,
                  text: "CODIGO"
                },
                {
                  dataIndex: "description",
                  flex: 1,
                  text: "NOMBRE"
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: 50,
                store: storeAgencyAll,
                items: [
                  {
                    xtype:"UpperCaseTrigger",
                    width: 120,
                    action: "searchGridAllAgency"
                  }
                ]
              }
            },
            {
              xtype: "gridpanel",
              padding: "0 0 0 1",
              store: storeAgencySelected,
              flex: 1,
              title: "ASIGNADOS",
              titleAlign: "center",
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false
                },
                {
                  dataIndex: "agency",
                  width: 60,
                  text: "CODIGO"
                },
                {
                  dataIndex: "descriptionAgency",
                  flex: 1,
                  text: "NOMBRE"
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: 50,
                store: storeAgencySelected
              }
            }
          ]
        }
      ],
      buttons: [
        {
          text: "SALIR",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ]
    });

    me.callParent(arguments);
  }
});
