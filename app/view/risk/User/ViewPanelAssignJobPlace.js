Ext.define('DukeSource.view.risk.User.ViewPanelAssignJobPlace', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelAssignJobPlace',
    border: false,
    layout: 'fit',
    tbar: [
        '->',
        {
            text: 'AUDITORIA',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewTreeGridPanelAssignJobPlace', padding: '2 2 2 2'}];
        this.callParent();
    }
});
