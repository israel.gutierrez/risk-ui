Ext.define("FormPanelUser", {
  extend: "Ext.data.Model",
  fields: ["name", "surnamePat", "surnameMat", "idPerson"]
});

Ext.define("DukeSource.view.risk.User.ViewPanelUsers", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelUsers",
  border: false,
  layout: "fit",
  tbar: [
    {
      text: "Nuevo",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      action: "registerUser",
      iconCls: "add"
    },
    "-",
    {
      text: "Modificar",
      iconCls: "modify",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      action: "modifyUser"
    },
    "-",
    {
      text: "Eliminar",
      iconCls: "delete",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      action: "deleteUserRisk"
    },
    {
      text: "Cambio password",
      iconCls: "finish",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      action: "password"
    },
    {
      text: "Reportes",
      iconCls: "excel",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      menu: {
        items: [
          {
            xtype: "menuitem",
            text: "Consolidado de Usuarios",
            handler: function() {
              Ext.core.DomHelper.append(document.body, {
                tag: "iframe",
                id: "downloadIframe",
                frameBorder: 0,
                width: 0,
                height: 0,
                css: "display:none;visibility:hidden;height:0px;",
                src:
                  "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
                  "&names=" +
                  "&types=" +
                  "&nameReport=" +
                  nameReport("UserConsolidate") +
                  "&nameDownload=Consolidado_usuarios"
              });
            }
          },
          {
            xtype: "menuitem",
            text: "Histórico de Cambios",
            handler: function() {
              Ext.core.DomHelper.append(document.body, {
                tag: "iframe",
                id: "downloadIframe",
                frameBorder: 0,
                width: 0,
                height: 0,
                css: "display:none;visibility:hidden;height:0px;",
                src:
                  "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
                  "&names=" +
                  "&types=" +
                  "&nameReport=" +
                  nameReport("UserHistorical") +
                  "&nameDownload=Historial_Cambios_usuarios"
              });
            }
          }
        ]
      }
    },
    "->",
    {
      text: "Auditoría",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      action: "userAuditory",
      iconCls: "auditory"
    }
  ],
  initComponent: function() {
    var me = this;
    this.items = [
      {
        xtype: "ViewGridPanelUsers",
        flex: 2,
        padding: "1 2 2 2"
      }
    ];
    this.callParent();
  }
});
