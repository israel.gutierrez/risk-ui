Ext.define("DukeSource.view.risk.User.ViewWindowPermission", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowPermission",
  layout: "fit",
  title: "VER PERMISOS",
  titleAlign: "center",
  tbar: [
    "->",
    "-",
    {
      text: "AUDITORIA",
      iconCls: "auditory"
    },
    "-"
  ],
  initComponent: function() {
    this.items = [
      {
        xtype: "container",
        layout: {
          align: "stretch",
          type: "vbox"
        },
        items: [
          {
            xtype: "form",
            height: 110,
            padding: "2 2 1 2",
            width: 681,
            layout: {
              align: "stretch",
              type: "hbox"
            },
            bodyPadding: 10,
            title: "",
            items: [
              {
                xtype: "container",
                flex: 1,
                height: 10,
                padding: "0 0 0 20",
                width: 333,
                layout: {
                  type: "anchor"
                },
                items: [
                  {
                    xtype: "textfield",
                    anchor: "100%",
                    fieldLabel: "USUARIO",
                    readOnly: true,
                    fieldCls: "readOnlyText",
                    labelWidth: 150
                  },
                  {
                    xtype: "textfield",
                    anchor: "100%",
                    fieldLabel: "NOMBRE COMPLETO",
                    readOnly: true,
                    fieldCls: "readOnlyText",
                    labelWidth: 150
                  },
                  {
                    xtype: "textfield",
                    anchor: "100%",
                    fieldLabel: "DNI",
                    readOnly: true,
                    fieldCls: "readOnlyText",
                    labelWidth: 150
                  }
                ]
              },
              {
                xtype: "container",
                flex: 0.5,
                height: 28,
                width: 412
              },
              {
                xtype: "container",
                flex: 1,
                height: 27,
                padding: "0 20 0 0 ",
                width: 371,
                layout: {
                  type: "anchor"
                },
                items: [
                  {
                    xtype: "textfield",
                    fieldLabel: "PUESTO",
                    readOnly: true,
                    fieldCls: "readOnlyText",
                    anchor: "100%",
                    labelWidth: 150
                  },
                  {
                    xtype: "textfield",
                    fieldLabel: "AGENCIA",
                    readOnly: true,
                    fieldCls: "readOnlyText",
                    anchor: "100%",
                    labelWidth: 150
                  },
                  {
                    xtype: "textfield",
                    fieldLabel: "RUC",
                    readOnly: true,
                    fieldCls: "readOnlyText",
                    anchor: "100%",
                    labelWidth: 150
                  }
                ]
              }
            ]
          },
          {
            xtype: "ViewGridPermission",
            flex: 2,
            padding: "1 2 2 2"
          }
        ]
      }
    ];
    this.buttons = [
      {
        text: "SALIR",
        scope: this,
        handler: this.close,
        iconCls: "logout"
      }
    ];
    this.callParent();
  }
});
