Ext.define('DukeSource.view.risk.User.ViewWindowAgreeDataUser', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowAgreeDataUser',
    border: false,
    layout: {
        type: 'fit'
    },
    anchorSize: 100,
    title: 'AGREGAR DATOS',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'numberfield',
                            anchor: '100%',
                            maxLength: 8,
                            minLength: 8,
                            action: 'actionSearhByDni',
                            fieldLabel: 'INGRESER DNI'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            fieldLabel: 'PERSONA'
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'CONFIRMAR',
                    iconCls: 'save'
                },

                {
                    text: 'CANCELAR',
                    iconCls: 'logout',
                    scope: this,
                    handler: this.close
                }
            ]
        });

        me.callParent(arguments);
    }

});