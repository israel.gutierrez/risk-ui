Ext.define("DukeSource.view.risk.User.grids.ViewGridPanelUsers", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridPanelUsers",
  store: "risk.User.grids.StoreGridPanelUsers",
  loadMask: true,
  columnLines: true,
  tbar: [
    {
      xtype: "UpperCaseTextField",
      fieldLabel: "BUSCAR",
      action: "searchRoleGeneral",
      labelWidth: 60,
      width: 300
    },
    "-"
  ],
  bbar: {
    xtype: "pagingtoolbar",
    pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
    store: "risk.User.grids.StoreGridPanelUsers",
    displayInfo: true,
    displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
    items: [
      "-",
      {
        xtype:"UpperCaseTrigger",
        action: "searchGridRoleGeneral",
        fieldLabel: "FILTRO",
        labelWidth: 60,
        width: 300
      }
    ],
    emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
  },
  listeners: {
    render: function() {
      var me = this;
      me.store.getProxy().extraParams = { propertyOrder: "u.username" };
      me.store.getProxy().url =
        "http://localhost:9000/giro/showListUserActives.htm";
      me.down("pagingtoolbar").moveFirst();
    }
  },
  initComponent: function() {
    this.columns = [
      { xtype: "rownumberer", width: 25, sortable: false },
      { header: "CODIGO", dataIndex: "userName", flex: 1.5 },
      { header: "LOGIN", dataIndex: "login", flex: 1.5 },
      { header: "NOMBRE", dataIndex: "fullName", flex: 4 },
      { header: "CATEGORIA", dataIndex: "descriptionCategory", flex: 2 },
      {
        header: "ESTADO",
        align: "center",
        dataIndex: "state",
        flex: 1,
        renderer: function(value, metaData, record) {
          if (record.get("state") == "S") {
            return '<i class="tesla even-circle-active"></i>' + " ACTIVO";
          } else {
            return '<i class="tesla even-circle-inactive"></i>' + " INACTIVO";
          }
        }
      },
      { header: "CORREO", dataIndex: "email", flex: 2 },
      /*{
                xtype: 'actioncolumn',
                header: 'ROL',
                align: 'center',
                width: 90,
                items: [{
                    icon: 'images/role.png',
                    handler: function (grid, rowIndex) {
                        var window = Ext.create('DukeSource.view.risk.User.ViewWindowAssignRoleUser', {
                            height: 400,
                            width: 750,
                            modal: true
                        }).show();
                        window.query('textfield[name=ocultar]')[0].setValue(grid.store.getAt(rowIndex).get('userName'));
                        var grid1 = Ext.ComponentQuery.query('ViewWindowAssignRoleUser grid[title=DISPONIBLES]')[0];
                        var toolbar = grid1.query('pagingtoolbar')[0];
                        DukeSource.global.DirtyView.searchPaginationGridNormal(grid.store.getAt(rowIndex).get('userName'), grid1, toolbar, 'showListRoleAvailableByUser.htm', 'user.username', 'description');
                        var grid2 = Ext.ComponentQuery.query('ViewWindowAssignRoleUser grid[title=ASIGNADOS]')[0];
                        var toolbar2 = grid2.query('pagingtoolbar')[0];
                        DukeSource.global.DirtyView.searchPaginationGridNormal(grid.store.getAt(rowIndex).get('userName'), grid2, toolbar2, 'showListUserRolesActives.htm', 'user.username', 'description');
                    }
                }]
            },*/
      /*{
                xtype: 'actioncolumn',
                header: 'AGENCIA',
                align: 'center',
                width: 90,
                items: [{
                    icon: 'images/agency.png',
                    handler: function (grid, rowIndex) {
                        var window = Ext.create('DukeSource.view.risk.User.ViewWindowAssignAgencyUser', {
                            height: 400,
                            width: 750,
                            modal: true
                        }).show();
                        window.query('textfield[name=ocultar]')[0].setValue(grid.store.getAt(rowIndex).get('userName'));

                        var grid1 = Ext.ComponentQuery.query('ViewWindowAssignAgencyUser grid[title=DISPONIBLES]')[0];
                        var toolbar = grid1.query('pagingtoolbar')[0];
                        DukeSource.global.DirtyView.searchPaginationGridNormal(grid.store.getAt(rowIndex).get('userName'), grid1, toolbar, 'showListAgenciesAvailableByUser.htm', 'user.username', 'description');

                        var grid2 = Ext.ComponentQuery.query('ViewWindowAssignAgencyUser grid[title=ASIGNADOS]')[0];
                        var toolbar2 = grid2.query('pagingtoolbar')[0];
                        DukeSource.global.DirtyView.searchPaginationGridNormal(grid.store.getAt(rowIndex).get('userName'), grid2, toolbar2, 'showListUserAgenciesActives.htm', 'user.username', 'idUserAgency');

                    }
                }]
            },*/
      { header: "HORA INICIO", dataIndex: "hourStart", flex: 1 },
      { header: "HORA FIN", dataIndex: "hourEnd", flex: 1 }
    ];
    this.callParent(arguments);
  }
});
