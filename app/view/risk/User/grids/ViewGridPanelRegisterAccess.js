Ext.define("DukeSource.view.risk.User.grids.ViewGridPanelRegisterAccess", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridPanelRegisterAccess",
  store: "risk.User.grids.StoreGridPanelRegisterAccess",
  loadMask: true,
  columnLines: true,
  bbar: {
    xtype: "pagingtoolbar",
    pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
    store: "risk.User.grids.StoreGridPanelRegisterAccess",
    displayInfo: true,
    displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
    items: [
      "-",
      {
        xtype:"UpperCaseTrigger",
        fieldLabel: "FILTRAR",
        action: "searchTriggerGridAccess",
        labelWidth: 60,
        width: 300
      }
    ],
    emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
  },
  listeners: {
    render: function() {
      var me = this;
      DukeSource.global.DirtyView.searchPaginationGridNormal(
        "",
        me,
        me.down("pagingtoolbar"),
        "http://localhost:9000/giro/findAccess.htm",
        "nameView",
        "nameView"
      );
    }
  },
  initComponent: function() {
    this.columns = [
      { xtype: "rownumberer", width: 50, sortable: false },
      { header: "CODIGO", dataIndex: "idAccess", flex: 1 },
      { header: "VALOR ACCESO", dataIndex: "valueAccess", flex: 1 },
      { header: "TIPO ACCESO", dataIndex: "typeAccess", flex: 1 },
      { header: "MODULO", dataIndex: "nameModule", flex: 1 },
      { header: "CODIGO ACCESO", dataIndex: "codeAccess", flex: 1 },
      { header: "ICONO", dataIndex: "icon", flex: 1 },
      { header: "NOMBRE CLASE", dataIndex: "nameClass", flex: 1 },
      { header: "NOMBRE TAB", dataIndex: "nameTab", flex: 1 },
      { header: "CCLOSAB", dataIndex: "cClosab", flex: 1 },
      { header: "CLOSABLE", dataIndex: "closable", flex: 1 },
      { header: "PERMITSUBTABS", dataIndex: "permitSubTabs", flex: 1 },
      { header: "TIPO ITEM", dataIndex: "typeItem", flex: 1 },
      { header: "NAME VISTA", dataIndex: "nameView", flex: 1 },
      { header: "ESTADO", dataIndex: "state", width: 60 }
    ];
    this.callParent(arguments);
  }
});
