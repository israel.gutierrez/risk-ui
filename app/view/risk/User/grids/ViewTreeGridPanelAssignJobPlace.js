Ext.define('DukeSource.view.risk.User.grids.ViewTreeGridPanelAssignJobPlace', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.ViewTreeGridPanelAssignJobPlace',
    store: 'risk.User.grids.StoreTreeGridPanelAssignJobPlace',
    useArrows: true,
    multiSelect: true,
    singleExpand: true,
    rootVisible: false,
    columns: [
        {
            xtype: 'treecolumn',
            text: 'ANALISTAS -> CARGOS',
            width: 520,
            sortable: true,
            dataIndex: 'text'
        },
        {
            text: 'DESCRIPCION',
            flex: 1,
            sortable: true,
            dataIndex: 'fieldTwo',
            align: 'left'
        },
        {
            text: 'ABREVIATURA',
            width: 100,
            sortable: true,
            dataIndex: 'fieldOne',
            align: 'left'
        }
    ]
});
