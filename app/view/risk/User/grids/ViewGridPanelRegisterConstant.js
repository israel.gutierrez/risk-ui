Ext.define("DukeSource.view.risk.User.grids.ViewGridPanelRegisterConstant", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridPanelRegisterConstant",
  store: "risk.User.grids.StoreGridPanelRegisterConstant",
  loadMask: true,
  columnLines: true,
  bbar: {
    xtype: "pagingtoolbar",
    pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
    store: "risk.User.grids.StoreGridPanelRegisterConstant",
    displayInfo: true,
    displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
    items: [
      "-",
      {
        xtype:"UpperCaseTrigger",
        fieldLabel: "FILTRAR",
        action: "searchTriggerGridConstant",
        labelWidth: 60,
        width: 300
      }
    ],
    emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
  },
  listeners: {
    render: function() {
      var me = this;
      DukeSource.global.DirtyView.loadGridDefault(me);
    }
  },
  initComponent: function() {
    this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
      clicksToMoveEditor: 1,
      saveBtnText: "GUARDAR",
      cancelBtnText: "CANCELAR",
      autoCancel: false,
      completeEdit: function() {
        var me = this;
        var grid = me.grid;
        var selModel = grid.getSelectionModel();
        var record = selModel.getLastSelected();
        if (me.editing && me.validateEdit()) {
          me.editing = false;
          Ext.Ajax.request({
            method: "POST",
            url: "http://localhost:9000/giro/saveConstant.htm",
            params: {
              jsonData: Ext.JSON.encode(record.data)
            },
            success: function(response) {
              response = Ext.decode(response.responseText);
              if (response.success) {
                Ext.MessageBox.alert(
                  DukeSource.global.GiroMessages.TITLE_MESSAGE,
                  response.mensaje
                );
              } else {
                Ext.MessageBox.alert(
                  DukeSource.global.GiroMessages.TITLE_MESSAGE,
                  response.mensaje
                );
              }
            },
            failure: function() {}
          });
          me.fireEvent("edit", me, me.context);
        }
      }
    });
    this.columns = [
      { xtype: "rownumberer", width: 25, sortable: false },
      {
        header: "Módulo",
        dataIndex: "descriptionModule",
        width: 150,
        renderer: function(val) {
          return '<span style="color:green;">' + val + "</span>";
        }
      },
      {
        header: "Descripción",
        dataIndex: "description",
        flex: 2,
        editor: {
          xtype: "UpperCaseTextField",
          allowBlank: false
        }
      },
      {
        header: "Valor",
        dataIndex: "valor",
        flex: 1,
        editor: {
          allowBlank: false,
          xtype: "UpperCaseTextField"
        }
      }
    ];
    this.callParent(arguments);
  }
});
