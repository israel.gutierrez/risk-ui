Ext.define('DukeSource.view.risk.User.grids.ViewTreeGridPanelAssignCollaborator', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.ViewTreeGridPanelAssignCollaborator',
    store: 'risk.User.grids.StoreTreeGridPanelAssignCollaborator',
    useArrows: true,
    multiSelect: true,
    singleExpand: true,
    rootVisible: false,
    columns: [
        {
            xtype: 'treecolumn',
            text: 'CATEGORIA DE USUARIOS',
            flex: 2,
            sortable: true,
            dataIndex: 'text'
        },
        {
            text: 'TIPO',
            flex: 1,
            sortable: true,
            dataIndex: 'fieldOne',
            align: 'left'
        }
    ]
});
