Ext.require(["Ext.ux.CheckColumn"]);
Ext.require(["Ext.ux.CheckedColumn"]);

Ext.define("DukeSource.view.risk.User.grids.ViewGridPermissionCheck", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridPermissionCheck",
  store: "risk.User.grids.StoreGridPermissionCheck",
  loadMask: true,
  columnLines: true,
  cls: "custom-grid",
  renderTo: Ext.getBody(),
  viewConfig: {
    stripeRows: true
  },
  bbar: {
    xtype: "pagingtoolbar",
    pageSize: 999,
    store: "risk.User.grids.StoreGridPermissionCheck",
    displayInfo: true,
    displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
    emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
  },
  listeners: {
    render: function() {
      var me = this;
      DukeSource.global.DirtyView.loadGridDefault(me);
    }
  },
  initComponent: function() {
    this.columns = [
      { xtype: "rownumberer", width: 30 },
      { header: "Acceso", dataIndex: "valueAccess", flex: 5 },
      { header: "Modulo", dataIndex: "descriptionModule", flex: 2 },
      { header: "Tipo", dataIndex: "typeItem", flex: 1.5 },
      {
        text: "Consultar",

        dataIndex: "indicatorOptionConsult",
        flex: 1.5,
        xtype: "checkedcolumn",
        columnHeaderCheckbox: true,
        store: Ext.create(
          "DukeSource.store.risk.User.grids.StoreGridPermissionCheck"
        ),
        sortable: false,
        hideable: false,
        menuDisabled: true,
        stopSelection: false
      },
      {
        text: "Guardar",
        dataIndex: "indicatorOptionSave",
        flex: 1.5,
        xtype: "checkedcolumn",
        columnHeaderCheckbox: true,
        store: Ext.create(
          "DukeSource.store.risk.User.grids.StoreGridPermissionCheck"
        ),
        sortable: false,
        hideable: false,
        menuDisabled: true,
        stopSelection: false,
        draggable: false,
        resizable: false
      },
      {
        text: "Modificar",
        dataIndex: "indicatorOptionUpdate",
        flex: 1.5,
        xtype: "checkedcolumn",
        columnHeaderCheckbox: true,
        store: Ext.create(
          "DukeSource.store.risk.User.grids.StoreGridPermissionCheck"
        ),
        sortable: false,
        hideable: false,
        menuDisabled: true,
        stopSelection: false
      },
      {
        text: "Eliminar",
        dataIndex: "indicatorOptionDelete",
        flex: 1.5,
        xtype: "checkedcolumn",
        columnHeaderCheckbox: true,
        store: Ext.create(
          "DukeSource.store.risk.User.grids.StoreGridPermissionCheck"
        ),
        sortable: false,
        hideable: false,
        menuDisabled: true,
        stopSelection: false
      }
    ];

    this.callParent(arguments);
  }
});
