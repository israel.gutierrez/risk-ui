Ext.define("DukeSource.view.risk.User.grids.ViewGridPanelRegisterCompany", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridPanelRegisterCompany",
  store: "risk.User.grids.StoreGridPanelRegisterCompany",
  loadMask: true,
  columnLines: true,
  bbar: {
    xtype: "pagingtoolbar",
    pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
    store: "risk.User.grids.StoreGridPanelRegisterCompany",
    displayInfo: true,
    displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
    emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
  },
  listeners: {
    render: function() {
      var me = this;
      me.store.load();
    }
  },
  initComponent: function() {
    this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
      clicksToMoveEditor: 1,
      saveBtnText: "GUARDAR",
      cancelBtnText: "CANCELAR",
      autoCancel: false,
      completeEdit: function() {
        var me = this;
        var grid = me.grid;
        var selModel = grid.getSelectionModel();
        var record = selModel.getLastSelected();
        if (me.editing && me.validateEdit()) {
          me.editing = false;
          Ext.Ajax.request({
            method: "POST",
            url: "http://localhost:9000/giro/saveCompany.htm",
            params: {
              jsonData: Ext.JSON.encode(record.data)
            },
            success: function(response) {
              response = Ext.decode(response.responseText);
              if (response.success) {
                grid.getStore().load();
                Ext.MessageBox.alert("CONFIRMACION", response.mensaje);
              } else {
                Ext.MessageBox.alert("CONFIRMACION", response.mensaje);
              }
            },
            failure: function() {}
          });
          me.fireEvent("edit", me, me.context);
        }
      }
    });

    this.columns = [
      { xtype: "rownumberer", width: 25, sortable: false },
      {
        header: "EMPRESA",
        dataIndex: "description",
        flex: 1,
        editor: {
          xtype: "UpperCaseTextField",
          allowBlank: false
        }
      },
      {
        header: "DIRECCION",
        dataIndex: "address",
        flex: 1,
        editor: {
          xtype: "UpperCaseTextField",
          allowBlank: false
        }
      },
      {
        header: "RUC",
        dataIndex: "ruc",
        width: 100,
        editor: {
          xtype: "numberfield",
          maxLength: 11,
          allowBlank: false
        }
      },
      {
        header: "TITULO REPORTE",
        dataIndex: "titleReport",
        flex: 1,
        editor: {
          xtype: "UpperCaseTextField",
          allowBlank: false
        }
      }
    ];
    this.callParent(arguments);
  }
});
