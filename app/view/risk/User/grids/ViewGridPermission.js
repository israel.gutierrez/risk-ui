//Ext.require(['Ext.ux.CheckColumn']);
Ext.define("DukeSource.view.risk.User.grids.ViewGridPermission", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridPermission",
  store: "risk.User.grids.StoreGridPermission",
  loadMask: true,
  columnLines: true,
  viewConfig: {
    stripeRows: true
  },
  bbar: {
    xtype: "pagingtoolbar",
    pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
    store: "risk.User.grids.StoreGridPermission",
    displayInfo: true,
    displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
    items: ["-"],
    emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
  },

  initComponent: function() {
    //        this.selModel = Ext.create('Ext.selection.CheckboxModel');
    this.columns = [
      { xtype: "rownumberer", width: 25, sortable: false },
      { header: "ACCESO", dataIndex: "descriptionAccess", flex: 1 },
      { header: "MODULO", dataIndex: "descriptionModule", flex: 3 },
      { header: "TIPO", dataIndex: "typeAccess", flex: 3 },
      { header: "CONSULTAR", dataIndex: "indicatorOptionConsult", flex: 3 },
      { header: "GUARDAR", dataIndex: "indicatorOptionSave", flex: 3 },
      { header: "MODIFICAR", dataIndex: "indicatorOptionUpdate", flex: 3 },
      { header: "ELIMINAR", dataIndex: "indicatorOptionDelete", flex: 3 }
      //            ,{ header: 'active', dataIndex: 'active', xtype: 'checkcolumn', processEvent: function () { return false; } }
    ];

    this.callParent(arguments);
  }
});
