Ext.define(
  "DukeSource.view.risk.User.grids.ViewGridPanelRegisterConstantApplication",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterConstantApplication",
    store: "risk.User.grids.StoreGridPanelRegisterConstantApplication",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "risk.User.grids.StoreGridPanelRegisterConstantApplication",
      displayInfo: true,
      displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "Filtrar",
          action: "searchTriggerGridConstantApplication",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/showListConstantApplicationActives.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        { header: "Módulo", dataIndex: "nameModule", width: 150 },
        { header: "Código de constante", dataIndex: "codeConstant", flex: 1.5 },
        { header: "Descripción", dataIndex: "description", flex: 4 },
        { header: "Valor constante", dataIndex: "valueConstant", width: 180 }
      ];
      this.callParent(arguments);
    }
  }
);
