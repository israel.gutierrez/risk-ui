Ext.define("DukeSource.view.risk.User.grids.ViewGridPanelRegisterRol", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridPanelRegisterRol",
  store: "risk.User.grids.StoreGridPanelRegisterRol",
  loadMask: true,
  columnLines: true,
  bbar: {
    xtype: "pagingtoolbar",
    pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
    store: "risk.User.grids.StoreGridPanelRegisterRol",
    displayInfo: true,
    displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
    emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
  },
  listeners: {
    render: function() {
      var me = this;
      me.store.load();
    }
  },

  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      plugins: [
        Ext.create("Ext.grid.plugin.RowEditing", {
          clicksToMoveEditor: 1,
          pluginId: "roweditor",
          saveBtnText: "GUARDAR",
          cancelBtnText: "CANCELAR",
          autoCancel: false,
          completeEdit: function() {
            var me = this;
            var grid = me.grid;
            var selModel = grid.getSelectionModel();
            var record = selModel.getLastSelected();
            if (me.editing && me.validateEdit()) {
              me.editing = false;
              Ext.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/saveRole.htm?nameView=ViewPanelRegisterRol",
                params: {
                  jsonData: Ext.JSON.encode(record.data)
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    DukeSource.global.DirtyView.messageNormal(response.message);
                    grid.getStore().load();
                  } else {
                    DukeSource.global.DirtyView.messageWarning(
                      response.message
                    );
                  }
                },
                failure: function() {}
              });

              me.fireEvent("edit", me, me.context);
            }
          }
        })
      ],
      columns: [
        {
          xtype: "rownumberer",
          width: 25,
          sortable: false
        },
        {
          header: "",
          hidden: true,
          dataIndex: "id",
          width: 25
        },
        {
          header: "Rol",
          dataIndex: "name",
          flex: 1,
          editor: {
            allowBlank: false
          }
        },
        {
          header: "Descripción",
          dataIndex: "description",
          flex: 2,
          editor: {
            allowBlank: false
          }
        },
        {
          header: "Categoría",
          dataIndex: "category",
          flex: 1,
          editor: new Ext.form.field.ComboBox({
            typeAhead: true,
            triggerAction: "all",
            selectOnTab: true,
            allowBlank: false,
            store: [
              ["AUDITOR", "AUDITOR"],
              ["ANALISTA", "ANALISTA"],
              ["GESTOR", "GESTOR"],
              ["COLABORADOR", "COLABORADOR"]
            ],
            listeners: {
              select: function(cbo, records) {
                var grid = cbo.up("panel").context.grid;
                var a = grid.getPlugin("roweditor").editor.down("#lineDefense");
                var b = grid.getPlugin("roweditor").editor.down("#module");
                var c = grid.getPlugin("roweditor").editor.down("#upModule");

                if (
                  cbo.getValue() === DukeSource.global.GiroConstants.AUDITOR
                ) {
                  a.setValue("3");
                  b.setValue("AU");
                  c.setValue("AU");
                } else if (
                  cbo.getValue() === DukeSource.global.GiroConstants.ANALYST
                ) {
                  a.setValue("2");
                  b.setValue("RO");
                  c.setValue("RI");
                } else {
                  a.setValue("1");
                  b.setValue("");
                  c.setValue("");
                }
              }
            }
          })
        },
        {
          header: "L&iacute;nea de Defensa",
          dataIndex: "lineDefense",
          flex: 1,
          readOnly: true,
          editor: new Ext.form.field.ComboBox({
            triggerAction: "all",
            fieldCls: "readOnlyText",
            itemId: "lineDefense",
            selectOnTab: true,
            displayField: "name",
            valueField: "id",
            store: {
              fields: ["id", "name"],
              data: [
                { id: "1", name: "1ra Linea de defensa" },
                { id: "2", name: "2da Linea de defensa" },
                { id: "3", name: "3ra Linea de defensa" }
              ]
            }
          }),
          renderer: function(value) {
            var a = me.getPlugin("roweditor").editor.down("#lineDefense");

            if (value !== undefined && value !== "") {
              var idx = a.store.find("id", value);
              var rec = a.store.getAt(idx);
              return rec.get("name");
            }
          }
        },
        {
          header: "M&oacute;dulo",
          dataIndex: "module",
          flex: 1,
          readOnly: true,
          editor: new Ext.form.field.ComboBox({
            triggerAction: "all",
            fieldCls: "readOnlyText",
            itemId: "module",
            displayField: "name",
            valueField: "id",
            store: {
              fields: ["id", "name"],
              data: [
                { id: "RO", name: "Riesgo operacional" },
                { id: "SI", name: "Seguridad de la informacion" },
                { id: "CN", name: "Continuidad del negocio" },
                { id: "CO", name: "Compliance" },
                { id: "AU", name: "Auditoria" }
              ]
            }
          }),
          renderer: function(value) {
            var a = me.getPlugin("roweditor").editor.down("#module");

            if (value !== undefined && value !== "") {
              var idx = a.store.find("id", value);
              var rec = a.store.getAt(idx);
              return rec.get("name");
            }
          }
        },
        {
          header: "M&oacute;dulo superior",
          dataIndex: "upModule",
          flex: 1,
          readOnly: true,
          editor: new Ext.form.field.ComboBox({
            triggerAction: "all",
            fieldCls: "readOnlyText",
            itemId: "upModule",
            displayField: "name",
            valueField: "id",
            store: {
              fields: ["id", "name"],
              data: [
                { id: "CO", name: "Compliance" },
                { id: "RI", name: "Riesgos" },
                { id: "AU", name: "Auditoria" }
              ]
            }
          }),
          renderer: function(value) {
            var a = me.getPlugin("roweditor").editor.down("#upModule");

            if (value !== undefined && value !== "") {
              var idx = a.store.find("id", value);
              var rec = a.store.getAt(idx);
              return rec.get("name");
            }
          }
        },
        {
          header: "Estado",
          dataIndex: "state",
          hidden: true,
          flex: 1,
          editor: new Ext.form.field.ComboBox({
            typeAhead: true,
            triggerAction: "all",
            selectOnTab: true,
            allowBlank: false,
            store: [
              ["S", "Activo"],
              ["N", "Inactivo"]
            ]
          })
        }
      ]
    });

    me.callParent(arguments);
  }
});
