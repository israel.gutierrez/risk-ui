Ext.define("ModelGridManagerAssign", {
  extend: "Ext.data.Model",
  fields: [
    "userName",
    "fullName",
    "descriptionAgency",
    "descriptionWorkArea",
    "descriptionJobPlace",
    "descriptionCategory",
    "category"
  ]
});

var StoreGridManagerAssign = Ext.create("Ext.data.Store", {
  model: "ModelGridManagerAssign",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.risk.User.ViewPanelAssignCollaborator", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelAssignCollaborator",
  border: false,
  layout: "fit",
  tbar: [
    {
      xtype: "UpperCaseTextField",
      action: "searchCollaboratorAssign",
      name: "textSearchCollaboratorAssign",
      itemId: "textSearchCollaboratorAssign",
      fieldLabel: "Nombre o apellido",
      labelWidth: 120,
      width: 300
    },
    {
      disabled: true,
      iconCls: "search"
    },
    "->",
    {
      text: "Auditoria",
      action: "registerEventAuditory",
      iconCls: "auditory"
    }
  ],
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "panel",
          flex: 1,
          padding: "0 2 2 2",
          border: false,
          layout: {
            type: "border"
          },
          items: [
            {
              xtype: "gridpanel",
              name: "gridManagerAssign",
              itemId: "gridManagerAssign",
              region: "center",
              title:
                "Lista de analistas &#8702; gestores &#8702; colaboradores",
              store: StoreGridManagerAssign,
              flex: 1,
              titleAlign: "center",
              bbar: {
                xtype: "pagingtoolbar",
                itemId: "ptManagerAssign",
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: StoreGridManagerAssign,
                displayInfo: true,
                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                items: [
                  "-",
                  {
                    xtype:"UpperCaseTrigger",
                    fieldLabel: "Filtrar",
                    action: "filterGridManagerAssign",
                    labelWidth: 60,
                    width: 300
                  }
                ],
                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              },
              padding: "2 2 2 0",
              listeners: {
                render: function() {
                  var me = Ext.ComponentQuery.query(
                    "ViewPanelAssignCollaborator"
                  )[0];
                  var mainGrid = me.down("#gridManagerAssign");
                  mainGrid.store.getProxy().extraParams = {};
                  mainGrid.store.getProxy().url =
                    "http://localhost:9000/giro/showListAnalystActives.htm";
                  mainGrid.down("pagingtoolbar").moveFirst();
                }
              },
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false
                },
                {
                  header: "CATEGORIA",
                  align: "center",
                  width: 100,
                  sortable: true,
                  dataIndex: "category",
                  renderer: function(c, b, a) {
                    c.tdAttr = "";
                    if (
                      a.get("category") === "ANALISTA" ||
                      a.get("category") === "ANALISTASI"
                    ) {
                      b.tdAttr =
                        'style="background-color: #94BF6E !important;"';
                      return "<span>" + c + "</span>";
                    } else if (a.get("category") === "GESTOR") {
                      b.tdAttr =
                        'style="background-color: #0092B2 !important;"';
                      return "<span>" + c + "</span>";
                    } else {
                      b.tdAttr =
                        'style="background-color: #FFCD55 !important;"';
                      return "<span>" + c + "</span>";
                    }
                  }
                },
                {
                  header: "Nombre",
                  align: "left",
                  width: 250,
                  sortable: true,
                  dataIndex: "fullName"
                },
                {
                  header: "Agencia",
                  align: "left",
                  width: 180,
                  sortable: true,
                  dataIndex: "descriptionAgency"
                },
                {
                  header: "Area",
                  align: "left",
                  flex: 1,
                  sortable: true,
                  dataIndex: "descriptionWorkArea"
                },
                {
                  header: "Cargo",
                  align: "left",
                  flex: 1,
                  sortable: false,
                  dataIndex: "descriptionJobPlace"
                }
              ]
            },
            {
              xtype: "AdvancedSearchUser",
              padding: "2 0 2 0",
              region: "west",
              collapseDirection: "left",
              collapsed: true,
              background: "#F5D705 !important;",
              collapsible: true,
              split: true,
              flex: 0.3,
              title: "Busqueda avanzada",
              tbar: [
                {
                  text: "Buscar",
                  scale: "medium",
                  iconCls: "search",
                  action: "searchAdvancedUser",
                  margin: "0 5 0 0"
                },
                {
                  text: "Limpiar",
                  scale: "medium",
                  iconCls: "clear",
                  margin: "0 5 0 0",
                  handler: function() {
                    me.down("form")
                      .getForm()
                      .reset();
                  }
                }
              ]
            }
          ]
        }
      ]
    });
    me.callParent(arguments);
  }
});
