Ext.define("DukeSource.view.risk.User.ViewPanelRegisterConstantApplication", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelRegisterConstantApplication",
  border: false,
  layout: "fit",
  tbar: [
    {
      text: "Nuevo",
      iconCls: "add",
      action: "newConstantApplication"
    },
    "-",
    {
      text: "Eliminar",
      iconCls: "delete",
      action: "deleteConstantApplication"
    },
    "-",
    {
      xtype: "ViewComboModule",
      action: "constantModule",
      labelWidth: 60,
      width: 200,
      fieldLabel: "Módulo"
    },
    "-",
    {
      xtype: "UpperCaseTextField",
      action: "searchConstantApplication",
      fieldLabel: "Buscar",
      labelWidth: 60,
      width: 300
    },
    "-",
    "->",
    {
      text: "Auditoría",
      action: "constantApplicationAuditory",
      iconCls: "auditory"
    }
  ],
  initComponent: function() {
    this.items = [
      { xtype: "ViewGridPanelRegisterConstantApplication", padding: 2 }
    ];
    this.callParent();
  }
});
