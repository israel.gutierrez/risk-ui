Ext.define("ModelEmploymentAll", {
  extend: "Ext.data.Model",
  fields: ["idEmployment", "description"]
});
Ext.define("ModelEmploymentSelected", {
  extend: "Ext.data.Model",
  fields: [
    "idUserEmployment",
    "user",
    "employment",
    "correlativeUserEmployment",
    "dateStartUserEmployment",
    "descriptionEmployment",
    "state"
  ]
});
var storeEmploymentAll = Ext.create("Ext.data.Store", {
  model: "ModelEmploymentAll",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListEmploymentsActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
storeEmploymentAll.load();

var storeEmploymentSelected = Ext.create("Ext.data.Store", {
  model: "ModelEmploymentSelected",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListUserEmploymentActives.htm",
    extraParams: {
      propertyFind: "user.username",
      valueFind: "",
      propertyOrder: "idUserEmployment"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
storeEmploymentSelected.load();

Ext.define("DukeSource.view.risk.User.ViewWindowAssignEmploymentUser", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowAssignEmploymentUser",
  layout: {
    align: "stretch",
    type: "vbox"
  },
  anchorSize: 100,
  title: "ASIGNAR PUESTO",
  titleAlign: "center",

  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          height: 45,
          padding: "2 2 2 2",
          bodyPadding: 10,
          items: [
            {
              xtype: "UpperCaseTextField",
              action: "searchEmploymentAll",
              anchor: "100%",
              labelWidth: 120,
              fieldLabel: "BUSCAR PUESTO"
            },
            {
              xtype: "textfield",
              name: "ocultar",
              hidden: true,
              fieldLabel: "PUESTO",
              hideLabel: true
            }
          ]
        },
        {
          xtype: "container",
          flex: 3,
          padding: "2 2 2 2",
          layout: {
            align: "stretch",
            type: "hbox"
          },
          items: [
            {
              xtype: "gridpanel",
              padding: "0 1 0 0",
              store: storeEmploymentAll,
              flex: 1,
              title: "DISPONIBLES",
              titleAlign: "center",
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false
                },
                {
                  dataIndex: "idEmployment",
                  width: 60,
                  text: "CODIGO"
                },
                {
                  dataIndex: "description",
                  flex: 1,
                  text: "NOMBRE"
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: 50,
                store: storeEmploymentAll,
                items: [
                  {
                    xtype:"UpperCaseTrigger",
                    width: 120,
                    action: "searchGridAllEmployment"
                  }
                ]
              }
            },
            {
              xtype: "gridpanel",
              padding: "0 0 0 1",
              store: storeEmploymentSelected,
              flex: 1,
              title: "ASIGNADOS",
              titleAlign: "center",
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false
                },
                {
                  dataIndex: "idUserEmployment",
                  width: 60,
                  text: "CODIGO"
                },
                {
                  dataIndex: "descriptionEmployment",
                  flex: 1,
                  text: "NOMBRE"
                },
                {
                  dataIndex: "correlativeUserEmployment",
                  flex: 1,
                  text: "NUMERO"
                },
                {
                  dataIndex: "dateStartUserEmployment",
                  flex: 1,
                  text: "FECHA"
                },
                {
                  dataIndex: "state",
                  flex: 1,
                  text: "ESTADO"
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: 50,
                store: storeEmploymentSelected
              }
            }
          ]
        }
      ],
      buttons: [
        {
          text: "CONFIRMAR",
          scope: this,
          handler: this.close,
          iconCls: "save"
        }
      ]
    });

    me.callParent(arguments);
  }
});
