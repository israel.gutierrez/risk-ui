Ext.define("DukeSource.view.risk.User.ViewPanelPermission", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelPermission",
  border: false,
  layout: "fit",
  tbar: [
    {
      xtype: "UpperCaseTextField",
      action: "searchRole",
      fieldLabel: "BUSCAR",
      labelWidth: 60,
      width: 300
    },
    "-",
    "->",
    "-",
    {
      text: "AUDITORIA",
      iconCls: "auditory"
    },
    "-"
  ],
  initComponent: function() {
    this.items = [
      {
        xtype: "container",
        layout: {
          align: "stretch",
          type: "vbox"
        },
        items: [
          {
            xtype: "form",
            height: 130,
            padding: "2 2 1 2",
            width: 681,
            layout: {
              align: "stretch",
              type: "hbox"
            },
            bodyPadding: 10,
            title: "",
            items: [
              {
                xtype: "container",
                flex: 1,
                height: 10,
                padding: "0 0 0 20",
                width: 333,
                layout: {
                  type: "anchor"
                },
                items: [
                  {
                    xtype: "container",
                    anchor: "100%",
                    height: 26,
                    width: 236,
                    layout: {
                      type: "hbox"
                    },
                    items: [
                      {
                        xtype: "UpperCaseTextField",
                        flex: 4,
                        fieldLabel: "DATOS DE USUARIO",
                        readOnly: true,
                        fieldCls: "readOnlyText",
                        msgTarget: "side",
                        blankText:
                          DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                        name: "user",
                        labelWidth: 150,
                        allowBlank: false
                      },
                      {
                        xtype: "button",
                        text: "BUSCAR",
                        iconCls: "search",
                        action: "userNameLimitPermissionPanel",
                        flex: 1
                      }
                    ]
                  },
                  {
                    xtype: "UpperCaseTextField",
                    anchor: "100%",
                    fieldLabel: "NOMBRE COMPLETO",
                    labelWidth: 150,
                    name: "fullName"
                  },
                  {
                    xtype: "UpperCaseTextField",
                    anchor: "100%",
                    maskRe: /[0-9]/,
                    fieldLabel: "DNI",
                    labelWidth: 150,
                    name: "document"
                  }
                ]
              },
              {
                xtype: "container",
                flex: 0.5,
                height: 28,
                width: 412
              },
              {
                xtype: "container",
                flex: 1,
                height: 27,
                padding: "0 20 0 0 ",
                width: 371,
                layout: {
                  type: "anchor"
                },
                items: [
                  {
                    xtype: "ViewComboRole",
                    fieldLabel: "ROL",
                    anchor: "100%",
                    labelWidth: 150,
                    typeAhead: true
                  },
                  {
                    xtype: "ViewComboAgency",
                    fieldLabel: "AGENCIA",
                    anchor: "100%",
                    labelWidth: 150
                  }
                ]
              }
            ],
            dockedItems: [
              {
                xtype: "toolbar",
                flex: 1,
                dock: "bottom",
                width: 718,
                items: [
                  "->",
                  "-",
                  {
                    xtype: "button",
                    iconCls: "save",
                    text: "BUSCAR",
                    action: "searchPermissionByRoleAgency"
                  },
                  "-",
                  {
                    xtype: "button",
                    iconCls: "logout",
                    text: "SALIR",
                    action: "goOutPanelPermission"
                  },
                  "-"
                ]
              }
            ]
          },
          {
            xtype: "ViewGridPermissionCheck",
            flex: 2,
            padding: "1 2 2 2"
          }
        ]
      }
    ];
    this.callParent();
  }
});
