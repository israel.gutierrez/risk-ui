Ext.define('DukeSource.view.risk.User.EditMenuCollaborator', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.EditMenuCollaborator',
    width: 120,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [

                {
                    xtype: 'menuitem',
                    text: 'Eliminar',
                    iconCls: 'delete'
                }
            ]
        });

        me.callParent(arguments);
    }
});