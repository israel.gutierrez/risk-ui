Ext.define("DukeSource.view.dashboard.HeatMapRisk", {
  extend: "Ext.Component",
  xtype: "heat-map-risk",

  cls: "heat-map-cp",
  nameHeatMap: "Mapa de riesgos",

  initEvents: function() {
    this.callParent();
  },

  graphicHeatMap: function(data, xAxis, yAxis) {
    $(this.el.dom).highcharts({
      chart: {
        type: "heatmap",
        marginTop: 40,
        marginBottom: 80
      },
      title: {
        text: ""
      },
      subtitle: {
        text: "<b>Mapa " + this.nameHeatMap + "</b>"
      },
      xAxis: {
        categories: xAxis.split(",")
      },
      yAxis: {
        categories: yAxis.split(","),
        title: null
      },
      colorAxis: {
        min: 0,
        minColor: "#FFFFFF",
        maxColor: "#ea3323"
      },
      legend: {
        enabled: true,
        align: "right",
        verticalAlign: "top",
        layout: "vertical"
      },
      tooltip: {
        formatter: function() {
          return (
            "Frecuencia: <b>" +
            this.series.yAxis.categories[this.point.y] +
            "</b><br>Impacto: <b>" +
            this.series.xAxis.categories[this.point.x] +
            "</b> <br>Riesgos: <b>" +
            this.point.value +
            "</b>"
          );
        }
      },
      series: [
        {
          name: "Riesgos",
          borderWidth: 0.2,
          data: data,
          dataLabels: {
            enabled: true,
            color: "#000000"
          },
          point: {
            events: {
              click: function(heap) {
                var win = Ext.create(
                  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDashboardManagerRisk",
                  {
                    modal: true
                  }
                );
                win.show(undefined, function() {
                  var grid = win.down("grid");
                  var fields = "madet.idMatrix,str.id";
                  var values =
                    heap.point.options["idMatrix"] +
                    ";" +
                    GiroConstants.ID_RISK_ACCEPT;
                  var types = "Long,Integer";
                  var operator = "equal,equal";
                  grid.store.proxy.url =
                    "http://localhost:9000/giro/findMatchRisk.htm";
                  grid.store.proxy.extraParams = {
                    fields: fields,
                    values: values,
                    types: types,
                    operators: operator,
                    search: "full",
                    isLast: "false",
                    condition: ""
                  };
                  grid.down("pagingtoolbar").moveFirst();
                });
              }
            }
          },
          cursor: "pointer"
        }
      ]
    });
  }
});
