Ext.define('DukeSource.view.dashboard.BoxScaleRisk',{
    extend: 'Ext.Component',
    xtype: 'box-scale-risk',

    baseCls: 'box-scale-risk-cp',
    childEls: ['body'],
    border: 'none',


    renderTpl: [
        '<div id="{id}-scale-risk-cp" style="border: {this.border}">{% this.renderContent(out, values) %}</div>'
    ],

    getTargetEl: function() {
        return this.body;
    },

    setColor: function(border) {
        this.border = border;
        this.body.update(border);
    }
});