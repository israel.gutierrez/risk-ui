Ext.define("ModelTreeMain", {
  extend: "Ext.data.Model",
  fields: ["text"]
});

var StoreTreeMain = Ext.create("Ext.data.TreeStore", {
  extend: "Ext.data.TreeStore",
  model: "ModelTreeMain",
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListTreePanel.htm",
    reader: {
      type: "json",
      root: "instanceList",
      successProperty: "success"
    }
  },
  root: {
    expanded: true
  }
});
Ext.define("DukeSource.view.main.ViewTreeMain", {
  extend: "Ext.tree.Panel",
  cls: "custom-grid",
  alias: "widget.ViewTreeMain",
  store: StoreTreeMain,
  rootVisible: false,
  mixins: {
    treeFilter: "TreeFilter"
  },
  dockedItems: [
    {
      xtype: "toolbar",
      items: [
        {
          tooltip: "Expandir",
          icon: "images/expand-all.gif",
          action: "expand"
        },
        {
          tooltip: "Colapsar",
          icon: "images/expand-all.gif",
          action: "colapse"
        },
        "->",
        {
          xtype: "textfield",
          name: "textFilter",
          width: 200,
          emptyText: "Busque la opcion a Trabajar",
          enableKeyEvents: true,
          listeners: {
            afterrender: function(field) {
              field.focus(false, 200);
            },
            keyup: function(f) {
              var me = Ext.ComponentQuery.query("ViewTreeMain")[0];
              me.expandAll();
              me.filterByText(f.getValue());
              if (f.getValue() === "") {
                me.collapseAll();
              }
            }
          }
        }
      ]
    }
  ]
});
