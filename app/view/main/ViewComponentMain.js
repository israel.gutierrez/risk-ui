Ext.define('DukeSource.view.main.ViewComponentMain', {
    extend: 'DukeSource.view.main.ViewPanelMain',
    region: 'center',
    navigationClass: 'DukeSource.view.main.ViewTreeMain',
    newItemText: "New User",
    deleteMessage: "Do you really wish to delete the user",
    deleteTitle: "Delete User"
});