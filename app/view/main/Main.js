getName = function(textId) {
  var store = Ext.getStore("DukeSource.store.Names"),
    rec = store.findRecord("id", textId);
  return rec.get(codexCompany);
};
hidden = function(textId) {
  var store = Ext.getStore("DukeSource.store.Hidden"),
    rec = store.findRecord("id", textId);
  return rec.get(codexCompany);
};
blank = function(textId) {
  var store = Ext.getStore("DukeSource.store.AllowBlank"),
    rec = store.findRecord("id", textId);

  return rec.get(codexCompany);
};
styleField = function(textId) {
  var store = Ext.getStore("DukeSource.store.StyleFields"),
    rec = store.findRecord("id", textId);

  return rec.get(codexCompany);
};
nameReport = function(textId) {
  var store = Ext.getStore("DukeSource.store.Reports"),
    rec = store.findRecord("id", textId);

  return rec.get(codexCompany);
};

Ext.define("ViewTreeMain", {
  extend: "Ext.tree.Panel",
  cls: "custom-grid",
  alias: "widget.ViewTreeMain",
  store: "DukeSource.store.TreeStore",
  rootVisible: false,
  mixins: {
    treeFilter: "DukeSource.global.TreeFilter"
  },
  dockedItems: [
    {
      xtype: "toolbar",
      items: [
        {
          xtype: "textfield",
          padding: "0 0 0 0",
          itemId: "searchOption",
          name: "searchOption",
          width: "100%",
          emptyText: "Buscar",
          enableKeyEvents: true,
          listeners: {
            afterrender: function(e) {
              e.focus(false, 200);
            },
            keyup: function(e) {
              var treeMain = Ext.ComponentQuery.query("ViewTreeMain")[0];
              treeMain
                .getStore()
                .filter("text", new RegExp("^" + e.getValue() + "$"));
              /* var treeMain = Ext.ComponentQuery.query("ViewTreeMain")[0];
              treeMain.expandAll(treeMain);
              treeMain.filterByText(e.getValue());
              if (e.getValue() === "") {
                treeMain.collapseAll(treeMain);
              } */
            }
          }
        }
      ]
    }
  ],
  listeners: {
    expand: function(tr) {
      tr.down("#searchOption").focus(false, 200);
    }
  }
});
Ext.define("DukeSource.view.main.Main", {
  alias: "Main",
  extend: "Ext.panel.Panel",
  requires: ["DukeSource.view.risk.util.ComboSelectCount"],
  layout: {
    type: "border"
  },
  initComponent: function() {
    var me = this;
    this.navigationMain = Ext.create("ViewTreeMain", {
      title: "GIRO - TOPRISK 2.7.0",
      itemId: "menuTreeMain",
      region: "west",
      collapsible: true,
      collapsed: true,
      split: true,
      margins: 2,
      width: 282
    });
    function addInitialPanel(nameClass, tittle) {
      me.addPanel(
        Ext.create(nameClass, {
          title: tittle,
          itemId: tittle,
          closeAction: "destroy",
          closable: true
        })
      );
    }
    this.editorTabPanelMain = Ext.create("Ext.tab.Panel", {
      tabPosition: "top",
      itemId: "principalTab",
      action: "principalTab",
      region: "center",
      margins: 2,
      layout: "fit",
      listeners: {
        afterrender: function() {
          if (category === undefined) {
            DukeSource.global.DirtyView.messageWarning(
              "La configuración del rol esta incompleta, comuníquese con el Administrador"
            );
          }

          if (
            category === DukeSource.global.GiroConstants.COLLABORATOR ||
            category === DukeSource.global.GiroConstants.GESTOR
          ) {
            addInitialPanel(
              getName("firstPanelManager"),
              getName("tittleFirstPanelManager")
            );
          } else if (category === DukeSource.global.GiroConstants.ANALYST) {
            addInitialPanel(
              getName("firstPanelAnalyst"),
              getName("tittleFirstPanelAnalyst")
            );
          } else if (category === DukeSource.global.GiroConstants.AUDITOR) {
            addInitialPanel(
              getName("firstPanelAuditor"),
              getName("tittleFirstPanelAuditor")
            );
          } else if (userRole === DukeSource.global.GiroConstants.ACCOUNTANT) {
            addInitialPanel(
              "DukeSource.view.risk.DatabaseEventsLost.ViewPanelEventsRiskOperational",
              "Eventos de p&eacute;rdida"
            );
          }
        }
      },
      items: []
    });
    this.items = [this.navigationMain, this.editorTabPanelMain];
    this.callParent();
  },
  addPanel: function(e) {
    var t = this.editorTabPanelMain.down("[title=" + e.title + "]");
    if (!t) {
      this.editorTabPanelMain.add(e);
      this.editorTabPanelMain.setActiveTab(e);
    } else {
      this.editorTabPanelMain.setActiveTab(t);
    }
  }
});
