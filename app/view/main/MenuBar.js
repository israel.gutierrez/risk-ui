Ext.define("DukeSource.view.main.MenuBar", {
  extend: "Ext.toolbar.Toolbar",
  padding: 0,
  layout: {
    align: "middle",
    pack: "center",
    type: "hbox"
  },
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "tbtext",
          text: 'GIRO<span style="font-size:12px;">Tesla Technologies</span>',
          baseCls: "companySignature",
          width: 285,
          padding: "5 5 5 40",
          height: 50,
          margin: 0
        },
        {
          scale: "large",
          iconCls: "tesla even-archive",
          name: "incidents",
          tooltip: "Incidentes",
          height: 40,
          handler: function() {
            loadPanel(
              "DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelIncidentsRiskOperational",
              "DukeSource.view.risk.DatabaseEventsLost.ViewPanelIncidentsRiskOperational",
              "Incidentes"
            );
          }
        },
        {
          scale: "large",
          iconCls: "tesla even-coins",
          tooltip: "Eventos pérdida",
          name: "events",
          itemId: "events",
          height: 40,
          handler: function() {
            if (
              category === DukeSource.global.GiroConstants.COLLABORATOR ||
              category === DukeSource.global.GiroConstants.GESTOR
            ) {
              loadPanel(
                "DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelEventManager",
                "DukeSource.view.risk.DatabaseEventsLost.ViewPanelEventsManager",
                "Eventos de pérdida"
              );
            } else {
              loadPanel(
                "DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelEventsRiskOperational",
                "DukeSource.view.risk.DatabaseEventsLost.ViewPanelEventsRiskOperational",
                "Eventos de pérdida"
              );
            }
          }
        },
        {
          scale: "large",
          iconCls: "tesla even-alert",
          tooltip: "Riesgos",
          name: "risk",
          itemId: "risk",
          style: {
            color: "black"
          },
          height: 40,
          handler: function() {
            loadPanel(
              "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelPreviousIdentifyRiskOperational",
              "DukeSource.view.risk.EvaluatorRiskOperational.PanelProcessRiskEvaluation",
              "EVRO"
            );

            // loadPanel( //view resume for manager no actions
            //     'DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelIdentifyRiskOperational',
            //     'DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelIdentifyManagerRiskOperational',
            //     'EVRO');

            // loadPanel( // view for identification
            //     'DukeSource.controller.risk.IdentificationRiskOperational.ControllerPanelGenericIdentifyRiskOperational',
            //     'DukeSource.view.risk.IdentificationRiskOperational.ViewPanelGenericIdentifyRiskOperational',
            //     'Identificación (IRO)');
          }
        },
        {
          name: "process",
          itemId: "process",
          scale: "large",
          iconCls: "tesla even-clipboard3",
          tooltip: "Planes de acción",
          height: 40,
          handler: function() {
            if (
              category === DukeSource.global.GiroConstants.GESTOR ||
              category === DukeSource.global.GiroConstants.COLLABORATOR
            ) {
              loadPanel(
                "DukeSource.controller.fulfillment.ControllerPanelDocumentPending",
                "DukeSource.view.fulfillment.ViewPanelDocumentPending",
                "Planes de acción"
              );

              var grid = Ext.ComponentQuery.query(
                "ViewPanelDocumentPending"
              )[0].down("grid");
              var fields = "";
              var values = "";
              var types = "";
              var operator = "";

              grid.store.getProxy().url =
                "http://localhost:9000/giro/findDetailDocument.htm";
              grid.store.getProxy().extraParams = {
                fields: fields,
                values: values,
                types: types,
                operator: operator,
                searchIn: "ActionPlanProcess",
                stateDocument: "PR"
              };
              grid.down("pagingtoolbar").doRefresh();
            } else {
              loadPanel(
                "DukeSource.controller.fulfillment.ControllerPanelDocumentPending",
                "DukeSource.view.fulfillment.ViewPanelDocumentPending",
                "Planes de acción"
              );
            }
          }
        },
        {
          name: "task",
          itemId: "task",
          scale: "large",
          iconCls: "tesla even-stack3",
          tooltip: "Tareas",
          hidden: true,
          height: 40,
          handler: function(btn) {
            loadPanel(
              "DukeSource.controller.fulfillment.ControllerTreeGridPanelCollaborator",
              "DukeSource.view.fulfillment.ViewTreeGridPanelCollaborator",
              "Tareas"
            );
          }
        },
        {
          name: "kri",
          itemId: "kri",
          scale: "large",
          iconCls: "tesla even-chart",
          tooltip: "Indicadores claves de riesgo",
          height: 40,
          handler: function(btn) {
            loadPanel(
              "DukeSource.controller.risk.kri.ControllerPanelConfigMasterKri",
              "DukeSource.view.risk.kri.ViewPanelConfigMasterKri",
              "Indicadores"
            );
          }
        },
        {
          iconCls: "tesla even-cube",
          scale: "large",
          /* hidden:
            hidden("MB_BTN_CubeReport") || category !== DukeSource.global.GiroConstants.ANALYST, */
          tooltip: "Cubo de Reportes",
          name: "cubeReport",
          itemId: "cubeReport",
          height: 40,
          handler: function() {
            if (!window.location.origin) {
              window.location.origin =
                window.location.protocol +
                "" +
                window.location.hostname +
                (window.location.port ? ":" + window.location.port : "");
            }
            window.location.href = window.location.origin + "/bi-ui";
          }
        },
        "->",
        {
          xtype: "tbtext",
          itemId: "userInfo",
          text:
            '<b><span style="color: #797979">' +
            descriptionCategory +
            '</span><br><span style="color: #0078d7">' +
            userName +
            "</span></b>"
        },
        {
          text: "",
          scale: "large",
          style: "border-radius:5px;",
          height: 40,
          iconCls: "tesla even-user",
          menu: {
            items: [
              {
                xtype: "menuitem",
                text: "Perfil",
                hrefTarget: "_self",
                iconCls: "manager",
                handler: function() {
                  Ext.create(
                    "DukeSource.view.risk.User.windows.ViewWindowProfileUser",
                    {
                      width: 447
                    }
                  ).show();
                }
              },
              {
                xtype: "menuitem",
                text: "Password",
                hidden: true,
                itemId: "passwordMain",
                iconCls: "key",
                handler: function() {
                  var view = Ext.create(
                    "DukeSource.view.risk.User.windows.ViewWindowChangePassword",
                    {
                      width: 447
                    }
                  ).show();
                  view.down("#userName").setValue(userName);
                }
              },
              {
                xtype: "menuitem",
                text: "Cerrar sesión",
                iconCls: "logout16",
                hrefTarget: "_self",
                handler: function() {
                  window.location.href = "logout";
                }
              }
            ]
          }
        }
      ]
    });

    me.callParent(arguments);
  }
});

function loadPanel(controller, panelUrl, title) {
  DukeSource.getApplication().loadController(controller);
  DukeSource.getApplication().centerPanel.addPanel(
    Ext.create(panelUrl, {
      title: title,
      closeAction: "destroy",
      closable: true
    })
  );
  Ext.ComponentQuery.query("ViewTreeMain")[0].collapse();
}
