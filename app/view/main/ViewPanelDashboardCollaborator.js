Ext.require("js.countries." + langLocale);

Ext.define("DukeSource.view.main.ViewPanelDashboardCollaborator", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelDashboardCollaborator",
  id: "card-wizard-panel",
  layout: "fit",
  requires: ["Ext.form.field.ComboBox"],
  activeItem: 0,

  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "container",
          layout: {
            type: "hbox",
            align: "stretch"
          },
          padding: 2,
          items: [
            {
              xtype: "container",
              itemId: "incidents",
              flex: 1
            },
            {
              xtype: "container",
              flex: 2,
              layout: {
                type: "vbox",
                align: "stretch"
              },
              items: [
                {
                  xtype: "container",
                  flex: 1.2,
                  layout: {
                    type: "vbox",
                    align: "stretch"
                  },
                  items: [
                    {
                      xtype: "container",
                      height: 26,
                      layout: {
                        type: "hbox",
                        align: "stretch"
                      },
                      items: [
                        {
                          xtype: "combobox",
                          anchor: "100%",
                          labelWidth: 30,
                          flex: 1.5,
                          fieldLabel: "KRI",
                          msgTarget: "side",
                          fieldCls: "obligatoryTextField",
                          displayField: "description",
                          valueField: "idKeyRiskIndicator",

                          forceSelection: true,
                          editable: false,
                          name: "keyRiskIndicator",
                          itemId: "keyRiskIndicator",
                          store: {
                            fields: ["idKeyRiskIndicator", "description"],
                            pageSize:
                              DukeSource.global.GiroConstants.ITEMS_PAGE,
                            proxy: {
                              actionMethods: {
                                create: "POST",
                                read: "POST",
                                update: "POST"
                              },
                              type: "ajax",
                              url:
                                "http://localhost:9000/giro/showListKeyRiskIndicatorActives.htm",
                              extraParams: {
                                propertyOrder: "description"
                              },
                              reader: {
                                type: "json",
                                root: "data",
                                successProperty: "success"
                              }
                            }
                          },
                          listeners: {
                            select: function(cbo) {
                              me.down("#eachQualification").reset();
                              me.down("#eachQualification").setDisabled(false);

                              me.down("#eachQualification").store.load({
                                url:
                                  "http://localhost:9000/giro/showVersionQualificationEachKriActives.htm",
                                params: {
                                  idKri: cbo.getValue()
                                }
                              });
                            }
                          }
                        },
                        {
                          xtype: "combobox",
                          labelWidth: 60,
                          flex: 1,
                          padding: "0 0 0 5",
                          fieldLabel: "UMBRAL",
                          msgTarget: "side",
                          disabled: true,
                          queryMode: "local",
                          fieldCls: "obligatoryTextField",
                          displayField: "description",
                          valueField: "version",

                          editable: false,
                          name: "eachQualification",
                          itemId: "eachQualification",
                          store: {
                            autoLoad: false,
                            fields: [
                              "idKeyRiskIndicator",
                              "description",
                              "version"
                            ],
                            proxy: {
                              actionMethods: {
                                create: "POST",
                                read: "POST",
                                update: "POST"
                              },
                              type: "ajax",
                              url:
                                "http://localhost:9000/giro/loadGridDefault.htm",
                              reader: {
                                type: "json",
                                root: "data",
                                successProperty: "success"
                              }
                            }
                          },
                          listeners: {
                            select: function(cbo) {
                              var detailKri = me.down("#detailKri").el.dom;
                              Ext.Ajax.request({
                                method: "POST",
                                url:
                                  "http://localhost:9000/giro/showKriLastEvaluation.htm",
                                params: {
                                  idKri: me
                                    .down("#keyRiskIndicator")
                                    .getValue(),
                                  version: cbo.getValue()
                                },
                                success: function(response) {
                                  response = Ext.decode(response.responseText);
                                  var months = JSON.parse(response.months);
                                  var dataOne = JSON.parse(response.dataOne);

                                  if (response.success) {
                                    var chart = $(detailKri).highcharts();
                                    if (chart !== undefined) {
                                      chart.destroy();
                                    }

                                    for (var k = 0; k < dataOne.length; k++) {
                                      dataOne[k]["point"] = {
                                        events: {
                                          click: function(bar) {
                                            detailEvaluationKri(bar);
                                          }
                                        }
                                      };
                                    }
                                    $(detailKri).highcharts({
                                      chart: {
                                        type: "column"
                                      },
                                      title: {
                                        text: ""
                                      },
                                      subtitle: {
                                        text: "EVOLUTIVO KRIs"
                                      },
                                      xAxis: {
                                        categories: months.split(",")
                                      },
                                      yAxis: {
                                        min: 0,
                                        title: {
                                          text: ""
                                        },
                                        stackLabels: {
                                          enabled: true,
                                          style: {
                                            fontWeight: "bold"
                                          }
                                        }
                                      },
                                      legend: {
                                        reserved: true
                                      },
                                      tooltip: {
                                        headerFormat: "<b>{point.x}</b><br/>",
                                        pointFormat:
                                          "{series.name}: {point.y}<br/>Total: {point.stackTotal}"
                                      },
                                      plotOptions: {
                                        column: {
                                          stacking: "normal",
                                          dataLabels: {
                                            enabled: true
                                          }
                                        }
                                      },
                                      series: dataOne
                                    });
                                  } else {
                                    DukeSource.global.DirtyView.messageAlert(
                                      DukeSource.global.GiroMessages
                                        .TITLE_WARNING,
                                      response.mensaje,
                                      Ext.Msg.ERROR
                                    );
                                  }
                                },
                                failure: function() {}
                              });
                            }
                          }
                        }
                      ]
                    },
                    {
                      xtype: "container",
                      itemId: "detailKri",
                      flex: 1
                    }
                  ]
                }
              ]
            }
          ],
          listeners: {
            afterrender: function(c) {
              var detailKri = c.down("#detailKri").el.dom;
              var pieIncidents = c.down("#incidents").el.dom;

              DukeSource.lib.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/showDataReportCollaborator.htm",
                params: {},
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  var months =
                    response.months === undefined
                      ? ""
                      : JSON.parse(response.months);
                  var dataOne =
                    response.dataOne === undefined
                      ? []
                      : JSON.parse(response.dataOne);
                  var dataTwo =
                    response.dataTwo === undefined
                      ? []
                      : JSON.parse(response.dataTwo);

                  if (response.success) {
                    for (var k = 0; k < dataOne.length; k++) {
                      dataOne[k]["point"] = {
                        events: {
                          click: function(bar) {
                            detailEvaluationKri(bar);
                          }
                        }
                      };
                    }
                    $(detailKri).highcharts({
                      chart: {
                        type: "column"
                      },
                      title: {
                        text: ""
                      },
                      subtitle: {
                        text: "EVOLUTIVO KRIs"
                      },
                      xAxis: {
                        categories:
                          months === undefined ? [] : months.split(",")
                      },
                      yAxis: {
                        min: 0,
                        title: {
                          text: ""
                        },
                        stackLabels: {
                          enabled: true,
                          style: {
                            fontWeight: "bold"
                          }
                        }
                      },
                      legend: {
                        reversed: true
                      },
                      tooltip: {
                        headerFormat: "<b>{point.x}</b><br/>",
                        pointFormat:
                          "{series.name}: {point.y}<br/>Total: {point.stackTotal}"
                      },
                      plotOptions: {
                        column: {
                          stacking: "normal",
                          dataLabels: {
                            enabled: true
                          }
                        }
                      },
                      series: dataOne === undefined ? [] : dataOne
                    });
                    $(pieIncidents).highcharts({
                      chart: {
                        type: "pie",
                        options3d: {
                          enabled: true,
                          alpha: 45,
                          beta: 0
                        }
                      },
                      title: {
                        text: ""
                      },
                      subtitle: {
                        text: "INCIDENTES REPORTADOS"
                      },
                      tooltip: {
                        pointFormat:
                          "Porcentaje: <b>{point.percentage:.1f}%</b><br>Nro Incidentes: <b>{point.y}</b>"
                      },
                      plotOptions: {
                        pie: {
                          allowPointSelect: true,
                          cursor: "pointer",
                          depth: 35,
                          dataLabels: {
                            enabled: true,
                            format: "{point.percentage:.1f}%"
                          },
                          showInLegend: true
                        }
                      },
                      series: [
                        {
                          name: "Estado Del Incidente",
                          data: dataTwo,
                          cursor: "pointer"
                        }
                      ]
                    });
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      response.message,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function() {}
              });
            }
          }
        }
      ]
    });
    me.callParent(arguments);
  }
});

function detailEvaluationKri(bar) {
  var win = Ext.create(
    "DukeSource.view.risk.kri.windows.WindowDetailEvaluationKri",
    {
      modal: true
    }
  );
  win.show();
  var grid = win.down("grid");
  var store = grid.getStore();
  DukeSource.lib.Ajax.request({
    method: "POST",
    url: "http://localhost:9000/giro/buildJsonByIdKriAndYearMonth.htm",
    params: {
      idKeyRiskIndicator: bar.point.options["codeKri"],
      indicatorNormalizer: bar.point.options["indicatorNormalizer"],
      indicatorKri: bar.point.options["indicatorKri"],
      idQualification: bar.point.options["idQualification"],
      year: bar.point.options["yearMonth"].substr(0, 4),
      month: bar.point.options["yearMonth"].substr(4, 5)
    },
    success: function(response) {
      response = Ext.decode(response.responseText);
      if (response.success) {
        var fields = JSON.parse(response.fields);
        var columns = JSON.parse(response.columns);
        var data = JSON.parse(response.data);
        store.model.setFields(fields);
        grid.reconfigure(store, columns);
        store.removeAll();
        store.loadRawData(data, true);
        // store.group(['descriptionRegion', 'descriptionAgency']);
        grid.getView().refresh();
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_ERROR,
          response.mensaje,
          Ext.Msg.ERROR
        );
      }
    },
    failure: function() {}
  });
}
