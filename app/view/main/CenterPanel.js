Ext.define('DukeSource.view.main.CenterPanel', {
    extend: 'Ext.tab.Panel',
    tabPosition: 'bottom',
    border: false,
    region: 'center',
    bodyStyle: 'background:#DBDBDB'
});