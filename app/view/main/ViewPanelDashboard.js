Ext.require("countries." + langLocale);

var cardNav = function(incr) {
  var l = Ext.getCmp("card-wizard-panel").getLayout();
  var i = l.activeItem.id.split("card-")[1];
  var next = parseInt(i, 10) + incr;
  l.setActiveItem(next);
  Ext.getCmp("card-prev").setDisabled(next === 0);
  Ext.getCmp("card-next").setDisabled(next === 2);

  Ext.getCmp("card-prev-first").setDisabled(next === 0);
  Ext.getCmp("card-next-first").setDisabled(next === 2);
};

Ext.define("DukeSource.view.main.ViewPanelDashboard", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelDashboard",
  id: "card-wizard-panel",
  layout: "card",
  requires: [
    "DukeSource.view.risk.parameter.combos.ViewComboProcess",
    "DukeSource.view.risk.parameter.combos.ViewComboTypeMatrix",
    "Ext.form.field.ComboBox",
    "DukeSource.view.risk.parameter.combos.ViewComboOperationalRiskExposition"
  ],
  activeItem: 0,
  bbar: [
    {
      id: "card-prev-first",
      cls: "my-btn",
      overCls: "my-over",
      text: "&laquo; Anterior",
      hidden: GIRO_VERSION !== "3",
      handler: Ext.Function.bind(cardNav, this, [-1]),
      disabled: true
    },
    {
      id: "card-next-first",
      cls: "my-btn",
      overCls: "my-over",
      hidden: GIRO_VERSION !== "3",
      text: "Siguiente &raquo;",
      handler: Ext.Function.bind(cardNav, this, [1])
    },
    "->",
    {
      id: "card-prev",
      cls: "my-btn",
      overCls: "my-over",
      hidden: GIRO_VERSION !== "3",
      text: "&laquo; Anterior",
      handler: Ext.Function.bind(cardNav, this, [-1]),
      disabled: true
    },
    {
      id: "card-next",
      cls: "my-btn",
      hidden: GIRO_VERSION !== "3",
      overCls: "my-over",
      text: "Siguiente &raquo;",
      handler: Ext.Function.bind(cardNav, this, [1])
    }
  ],
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "container",
          id: "card-0",
          layout: {
            type: "hbox",
            align: "stretch"
          },
          padding: 2,
          items: [
            {
              xtype: "container",
              layout: {
                type: "vbox",
                align: "stretch"
              },
              flex: 1,
              items: [
                {
                  xtype: "container",
                  itemId: "mapLostEvent",
                  flex: 1
                }
              ]
            },
            {
              xtype: "container",
              layout: {
                type: "vbox",
                align: "stretch"
              },
              flex: 1,
              items: [
                {
                  xtype: "container",
                  layout: {
                    type: "hbox",
                    align: "center"
                  },
                  items: [
                    {
                      xtype: "ViewComboOperationalRiskExposition",
                      queryMode: "remote",
                      flex: 1,
                      name: "maxExpositionProcess",
                      itemId: "maxExpositionProcess",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      fieldLabel: "Máxima exposición",
                      listeners: {
                        select: function(cbo) {
                          if (TYPE_MATRIX_SIZE === DukeSource.global.GiroConstants.ONE_S) {
                            Ext.Ajax.request({
                              method: "POST",
                              url:
                                "http://localhost:9000/giro/showEvaluationCriticRisk.htm",
                              params: {
                                maxExposition: me
                                  .down("#maxExpositionProcess")
                                  .getValue(),
                                typeMatrix: TYPE_MATRIX_DEFAULT
                              },
                              success: function(response) {
                                response = Ext.decode(response.responseText);
                                var process = JSON.parse(response.process);
                                var data = JSON.parse(response.data);

                                if (response.success) {
                                  var processCritic = me.down("#processCritic")
                                    .el.dom;
                                  var chart = $(processCritic).highcharts();
                                  chart.destroy();
                                  for (var k = 0; k < data.length; k++) {
                                    data[k]["point"] = {
                                      events: {
                                        click: function(bar) {
                                          detailProcessCritic(bar);
                                        }
                                      }
                                    };
                                  }
                                  $(processCritic).highcharts({
                                    chart: {
                                      type: "bar"
                                    },
                                    credits: false,
                                    title: {
                                      text: ""
                                    },
                                    subtitle: {
                                      text: "Autoevaluación de procesos"
                                    },

                                    xAxis: {
                                      categories: process.split(","),
                                      labels: {
                                        style: {
                                          fontSize: "10px"
                                        }
                                      }
                                    },
                                    yAxis: {
                                      min: 0,
                                      enabled: false,
                                      title: {
                                        text: ""
                                      }
                                    },
                                    legend: {
                                      reversed: true
                                    },
                                    plotOptions: {
                                      series: {
                                        stacking: "normal"
                                      },
                                      bar: {
                                        stacking: "normal"
                                      }
                                    },
                                    series: data
                                  });
                                } else {
                                  DukeSource.global.DirtyView.messageWarning(response.message);
                                }
                              },
                              failure: function() {}
                            });
                            Ext.Ajax.request({
                              method: "POST",
                              url: "http://localhost:9000/giro/showMapRisk.htm",
                              params: {
                                maxExposition: me
                                  .down("#maxExpositionProcess")
                                  .getValue(),
                                typeMatrix: TYPE_MATRIX_DEFAULT
                              },
                              success: function(response) {
                                response = Ext.decode(response.responseText);
                                var data = JSON.parse(response.data);
                                var impacts = Ext.JSON.decode(response.impact);
                                var frequencies = Ext.JSON.decode(
                                  response.frequency
                                );

                                if (response.success) {
                                  var mapRisk = me.down("#mapRisk").el.dom;
                                  var chart = $(mapRisk).highcharts();
                                  chart.destroy();
                                  for (var i = 0; i < data.length; i++) {
                                    if (data[i]["value"] === 0) {
                                      data[i]["value"] = "";
                                    }
                                  }

                                  $(mapRisk).highcharts({
                                    chart: {
                                      type: "heatmap",
                                      marginTop: 40,
                                      marginBottom: 80,
                                      plotBorderWidth: 1
                                    },
                                    credits: false,
                                    title: {
                                      text: ""
                                    },
                                    subtitle: {
                                      text: "Mapa de riesgos"
                                    },
                                    xAxis: {
                                      categories: impacts.split(",")
                                    },
                                    yAxis: {
                                      categories: frequencies.split(","),
                                      title: null
                                    },
                                    colorAxis: {
                                      min: 0,
                                      minColor: "#FFFFFF",
                                      maxColor: Highcharts.getOptions()
                                        .colors[0]
                                    },
                                    legend: {
                                      enabled: false
                                    },
                                    tooltip: {
                                      formatter: function() {
                                        return (
                                          "Frecuencia: <b>" +
                                          this.series.yAxis.categories[
                                            this.point.y
                                          ] +
                                          "</b><br>Impacto: <b>" +
                                          this.series.xAxis.categories[
                                            this.point.x
                                          ] +
                                          "</b> <br>Riesgos: <b>" +
                                          this.point.value +
                                          "</b>"
                                        );
                                      }
                                    },
                                    series: [
                                      {
                                        name: "Riesgos",
                                        borderWidth: 0.2,
                                        data: data,
                                        dataLabels: {
                                          enabled: true,
                                          color: "#000000"
                                        },
                                        point: {
                                          events: {
                                            click: function(heap) {
                                              var win = Ext.create(
                                                "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDashboardManagerRisk",
                                                {
                                                  modal: true
                                                }
                                              );
                                              win.show();
                                              var grid = win.down("grid");
                                              var fields = "madet.idMatrix";
                                              var values =
                                                heap.point.options["idMatrix"];
                                              var types = "Long,Integer";
                                              var operator = "equal,equal";
                                              grid.store.proxy.url =
                                                "http://localhost:9000/giro/findMatchRisk.htm";
                                              grid.store.proxy.extraParams = {
                                                fields: fields,
                                                values: values,
                                                types: types,
                                                operators: operator,
                                                search: "full",
                                                isLast: "false",
                                                condition: ""
                                              };
                                              grid
                                                .down("pagingtoolbar")
                                                .moveFirst();
                                            }
                                          }
                                        },
                                        cursor: "pointer"
                                      }
                                    ]
                                  });
                                } else {
                                  DukeSource.global.DirtyView.messageWarning(response.message);
                                }
                              },
                              failure: function() {}
                            });
                          }
                        }
                      }
                    },
                    {
                      xtype: "ViewComboTypeMatrix",
                      queryMode: "remote",
                      padding: "0 0 0 5",
                      fieldLabel: "Tipo de matriz",
                      flex: 1,
                      hidden: TYPE_MATRIX_SIZE === DukeSource.global.GiroConstants.ONE_S,
                      msgTarget: "side",
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      name: "typeMatrix",
                      itemId: "typeMatrix",
                      tpl: Ext.create(
                        "Ext.XTemplate",
                        '<tpl for=".">',
                        '<div class="x-boundlist-item">{description} | {stateApproved}</div>',
                        "</tpl>"
                      ),
                      displayTpl: Ext.create(
                        "Ext.XTemplate",
                        '<tpl for=".">',
                        "{description} | {stateAssigned}",
                        "</tpl>"
                      ),
                      listeners: {
                        select: function(cbo) {
                          Ext.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/showEvaluationCriticRisk.htm",
                            params: {
                              maxExposition: me
                                .down("#maxExpositionProcess")
                                .getValue(),
                              typeMatrix: cbo.getValue()
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              var process = JSON.parse(response.process);
                              var data = JSON.parse(response.data);

                              if (response.success) {
                                var processCritic = me.down("#processCritic").el
                                  .dom;
                                var chart = $(processCritic).highcharts();
                                chart.destroy();
                                for (var k = 0; k < data.length; k++) {
                                  data[k]["point"] = {
                                    events: {
                                      click: function(bar) {
                                        detailProcessCritic(bar);
                                      }
                                    }
                                  };
                                }
                                $(processCritic).highcharts({
                                  chart: {
                                    type: "bar"
                                  },
                                  credits: false,
                                  title: {
                                    text: ""
                                  },
                                  subtitle: {
                                    text: "Autoevaluación de procesos"
                                  },

                                  xAxis: {
                                    min: 0,
                                    max: 25,
                                    scrollbar: {
                                      enabled: true
                                    },
                                    categories: process.split(","),
                                    labels: {
                                      style: {
                                        fontSize: "10px"
                                      }
                                    }
                                  },
                                  yAxis: {
                                    min: 0,
                                    enabled: false,
                                    title: {
                                      text: ""
                                    }
                                  },
                                  legend: {
                                    reversed: true
                                  },
                                  plotOptions: {
                                    series: {
                                      stacking: "normal"
                                    },
                                    bar: {
                                      stacking: "normal"
                                    }
                                  },
                                  series: data
                                });
                              } else {
                                DukeSource.global.DirtyView.messageWarning(response.message);
                              }
                            },
                            failure: function() {}
                          });
                          Ext.Ajax.request({
                            method: "POST",
                            url: "http://localhost:9000/giro/showMapRisk.htm",
                            params: {
                              maxExposition: me
                                .down("#maxExpositionProcess")
                                .getValue(),
                              typeMatrix: cbo.getValue()
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              var data = JSON.parse(response.data);
                              var impacts = Ext.JSON.decode(response.impact);
                              var frequencies = Ext.JSON.decode(
                                response.frequency
                              );

                              if (response.success) {
                                var mapRisk = me.down("#mapRisk").el.dom;
                                var chart = $(mapRisk).highcharts();
                                chart.destroy();
                                for (var i = 0; i < data.length; i++) {
                                  if (data[i]["value"] === 0) {
                                    data[i]["value"] = "";
                                  }
                                }

                                $(mapRisk).highcharts({
                                  chart: {
                                    type: "heatmap",
                                    marginTop: 40,
                                    marginBottom: 80,
                                    plotBorderWidth: 1
                                  },
                                  credits: false,
                                  title: {
                                    text: ""
                                  },
                                  subtitle: {
                                    text: "Mapa de riesgos"
                                  },
                                  xAxis: {
                                    categories: impacts.split(",")
                                  },
                                  yAxis: {
                                    categories: frequencies.split(","),
                                    title: null
                                  },
                                  colorAxis: {
                                    min: 0,
                                    minColor: "#FFFFFF",
                                    maxColor: Highcharts.getOptions().colors[0]
                                  },
                                  legend: {
                                    enabled: false
                                  },
                                  tooltip: {
                                    formatter: function() {
                                      return (
                                        "Frecuencia: <b>" +
                                        this.series.yAxis.categories[
                                          this.point.y
                                        ] +
                                        "</b><br>Impacto: <b>" +
                                        this.series.xAxis.categories[
                                          this.point.x
                                        ] +
                                        "</b> <br>Riesgos: <b>" +
                                        this.point.value +
                                        "</b>"
                                      );
                                    }
                                  },
                                  series: [
                                    {
                                      name: "Riesgos",
                                      borderWidth: 0.2,
                                      data: data,
                                      dataLabels: {
                                        enabled: true,
                                        color: "#000000"
                                      },
                                      point: {
                                        events: {
                                          click: function(heap) {
                                            var win = Ext.create(
                                              "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDashboardManagerRisk",
                                              {
                                                modal: true
                                              }
                                            );
                                            win.show();
                                            var grid = win.down("grid");
                                            var fields = "madet.idMatrix";
                                            var values =
                                              heap.point.options["idMatrix"];
                                            var types = "Long,Integer";
                                            var operator = "equal,equal";
                                            grid.store.proxy.url =
                                              "http://localhost:9000/giro/findMatchRisk.htm";
                                            grid.store.proxy.extraParams = {
                                              fields: fields,
                                              values: values,
                                              types: types,
                                              operators: operator,
                                              search: "full",
                                              isLast: "false",
                                              condition: ""
                                            };
                                            grid
                                              .down("pagingtoolbar")
                                              .moveFirst();
                                          }
                                        }
                                      },
                                      cursor: "pointer"
                                    }
                                  ]
                                });
                              } else {
                                DukeSource.global.DirtyView.messageWarning(response.message);
                              }
                            },
                            failure: function() {}
                          });
                        }
                      }
                    }
                  ]
                },
                {
                  xtype: "component",
                  itemId: "processCritic",
                  flex: 1
                }
              ]
            },
            {
              xtype: "container",
              flex: 1,
              layout: {
                type: "vbox",
                align: "stretch"
              },
              items: [
                {
                  xtype: "container",
                  itemId: "mapRisk",
                  flex: 1
                },
                {
                  xtype: "container",
                  itemId: "actionPlanByProcess",
                  flex: 1
                }
              ]
            }
          ],
          listeners: {
            activate: function(c) {
              var mapLostEvent = c.down("#mapLostEvent").el.dom;
              var processCritic = c.down("#processCritic").el.dom;
              var mapRisk = c.down("#mapRisk").el.dom;
              var actionPlanByProcess = c.down("#actionPlanByProcess").el.dom;

              DukeSource.lib.Ajax.request({
                method: "POST",
                url: "http://localhost:9000/giro/showDataReport.htm",
                params: {},
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    var data = Ext.JSON.decode(response.data1);
                    var valueMaxMap = 0;
                    var location = "";
                    var value = 0;
                    var numberEvents = 0;
                    var idRegion = "";

                    for (var i = 0; i < data.length; i++) {
                      if (data[i].hasOwnProperty("location")) {
                        data[i]["hc-key"] = data[i]["location"];
                      }
                      if (data[i].hasOwnProperty("netLoss")) {
                        data[i]["value"] = parseFloat(data[i]["netLoss"]);
                      }
                      if (valueMaxMap < data[i]["value"]) {
                        valueMaxMap = parseFloat(data[i]["value"]);
                      }
                      if (data[i]["location"] === "bo-lp") {
                        value = (
                          parseFloat(value) + parseFloat(data[i]["value"])
                        ).toFixed(2);
                        numberEvents =
                          parseInt(numberEvents) +
                          parseInt(data[i]["numberEvents"]);
                        location = data[i]["hc-key"];
                        idRegion = data[i]["idRegion"];
                      }
                    }
                    data.push({
                      "hc-key": location,
                      idRegion: idRegion,
                      numberEvents: numberEvents,
                      value: parseFloat(value)
                    });
                    $(mapLostEvent).highcharts("Map", {
                      title: {
                        text: ""
                      },
                      credits: false,
                      subtitle: {
                        text: "Eventos de pérdida por Rop (pérdida neta)"
                      },
                      mapNavigation: {
                        enabled: true,
                        buttonOptions: {
                          verticalAlign: "bottom"
                        }
                      },
                      colorAxis: {
                        min: 0,
                        max: valueMaxMap,
                        minColor: "#FFFFFF",
                        maxColor: "#FF0000"
                      },
                      series: [
                        {
                          data: data,
                          mapData: Highcharts.maps["" + langLocale],
                          joinBy: "hc-key",
                          name: "Detalle eventos",
                          states: {
                            hover: {
                              color: "#BADA55"
                            }
                          },
                          dataLabels: {
                            enabled: true,
                            format: "{point.name}"
                          },
                          tooltip: {
                            pointFormat:
                              "Pérdida Neta (" +
                              symbol +
                              "): <b>{point.value}</b><br>Numero de Eventos: <b>{point.numberEvents}</b>"
                          },
                          point: {
                            events: {
                              click: function(map) {
                                var win = Ext.create(
                                  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowDashBoardManagerEvent",
                                  {
                                    modal: true
                                  }
                                );
                                win.show();
                                var grid = win.down("grid");
                                var fields = "r.location";
                                var values = map.point.options["hc-key"];
                                var types = "String";
                                var operator = "equal";
                                grid.store.proxy.url =
                                  "http://localhost:9000/giro/advancedSearchEvent.htm";
                                grid.store.proxy.extraParams = {
                                  fields: fields,
                                  values: values,
                                  types: types,
                                  operators: operator,
                                  searchIn: "Event"
                                };
                                grid.down("pagingtoolbar").moveFirst();
                              }
                            }
                          },
                          cursor: "pointer"
                        }
                      ]
                    });

                    var data3 = Ext.JSON.decode(response.data3);
                    var statesActionPlan = Ext.JSON.decode(
                      response.statesActionPlan
                    );
                    var data4 = JSON.parse(response.data4);
                    var process = JSON.parse(response.process);

                    for (var k = 0; k < data4.length; k++) {
                      data4[k]["point"] = {
                        events: {
                          click: function(bar) {
                            detailProcessCritic(bar);
                          }
                        }
                      };
                    }
                    $(processCritic).highcharts({
                      chart: {
                        type: "bar"
                      },
                      credits: false,
                      title: {
                        text: ""
                      },
                      subtitle: {
                        text: "Autoevaluación de procesos"
                      },
                      xAxis: {
                        min: 0,
                        max: 25,
                        scrollbar: {
                          enabled: true
                        },
                        categories: process.split(","),
                        labels: {
                          style: {
                            fontSize: "10px"
                          }
                        }
                      },
                      yAxis: {
                        min: 0,
                        enabled: false,
                        title: {
                          text: ""
                        }
                      },
                      legend: {
                        reversed: true
                      },
                      plotOptions: {
                        series: {
                          stacking: "normal"
                        },
                        bar: {
                          stacking: "normal"
                        }
                      },
                      series: data4
                    });

                    var data2 = JSON.parse(response.data2);

                    var impacts = Ext.JSON.decode(response.impacts);
                    var frequencies = Ext.JSON.decode(response.frequencies);

                    for (var i = 0; i < data2.length; i++) {
                      if (data2[i]["value"] === 0) {
                        data2[i]["value"] = "";
                      }
                    }
                    $(mapRisk).highcharts({
                      chart: {
                        type: "heatmap",
                        marginTop: 40,
                        marginBottom: 80,
                        plotBorderWidth: 1
                      },
                      title: {
                        text: ""
                      },
                      credits: false,
                      subtitle: {
                        text: "Mapa de riesgos"
                      },
                      xAxis: {
                        categories:
                          RISK_AXIS_Y === "impact"
                            ? frequencies.split(",")
                            : impacts.split(",")
                      },
                      yAxis: {
                        categories:
                          RISK_AXIS_Y === "impact"
                            ? impacts.split(",")
                            : frequencies.split(","),
                        title: null
                      },
                      colorAxis: {
                        min: 0,
                        minColor: "#FFFFFF",
                        maxColor: Highcharts.getOptions().colors[0]
                      },
                      legend: {
                        enabled: false
                      },
                      tooltip: {
                        formatter: function() {
                          return (
                            "Frecuencia: <b>" +
                            this.series.yAxis.categories[this.point.y] +
                            "</b><br>Impacto: <b>" +
                            this.series.xAxis.categories[this.point.x] +
                            "</b> <br>Riesgos: <b>" +
                            this.point.value +
                            "</b>"
                          );
                        }
                      },
                      series: [
                        {
                          name: "Riesgos",
                          borderWidth: 0.2,
                          data: data2,
                          dataLabels: {
                            enabled: true,
                            color: "#000000"
                          },
                          point: {
                            events: {
                              click: function(heap) {
                                var win = Ext.create(
                                  "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDashboardManagerRisk",
                                  {
                                    modal: true
                                  }
                                );
                                win.show(undefined, function() {
                                  var grid = win.down("grid");
                                  var fields = "madet.idMatrix";
                                  var values = heap.point.options["idMatrix"];
                                  var types = "Long,Integer";
                                  var operator = "equal,equal";
                                  grid.store.proxy.url =
                                    "http://localhost:9000/giro/findMatchRisk.htm";
                                  grid.store.proxy.extraParams = {
                                    fields: fields,
                                    values: values,
                                    types: types,
                                    operators: operator,
                                    isLast: "false",
                                    search: "full",
                                    condition: ""
                                  };
                                  grid.down("pagingtoolbar").doRefresh();
                                });
                              }
                            }
                          },
                          cursor: "pointer"
                        }
                      ]
                    });

                    for (var k = 0; k < data3.length; k++) {
                      data3[k]["point"] = {
                        events: {
                          click: function(bar) {
                            detailActionPlan(bar);
                          }
                        }
                      };
                    }
                    $(actionPlanByProcess).highcharts({
                      chart: {
                        type: "bar"
                      },
                      credits: false,
                      title: {
                        text: ""
                      },
                      subtitle: {
                        text: "Planes de acción"
                      },
                      xAxis: {
                        categories: statesActionPlan.split(",")
                      },
                      yAxis: {
                        min: 0,
                        enabled: false,
                        title: {
                          text: ""
                        }
                      },
                      legend: {
                        reversed: true
                      },
                      tooltip: {
                        headerFormat: "<b>{point.x}</b><br/>",
                        pointFormat:
                          "{series.name}: {point.y}<br/>Total: {point.stackTotal}"
                      },
                      plotOptions: {
                        series: {
                          stacking: "normal"
                        },
                        bar: {
                          stacking: "normal"
                        }
                      },
                      series: data3
                    });
                  } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                  }
                },
                failure: function() {}
              });
            }
          }
        },
        {
          xtype: "container",
          id: "card-1",
          layout: {
            type: "hbox",
            align: "stretch"
          },
          padding: 2,
          items: [
            {
              xtype: "container",
              itemId: "kriGeneral",
              flex: 1
            },
            {
              xtype: "container",
              flex: 1,
              layout: {
                type: "vbox",
                align: "stretch"
              },
              items: [
                {
                  xtype: "container",
                  flex: 1.2,
                  layout: {
                    type: "vbox",
                    align: "stretch"
                  },
                  items: [
                    {
                      xtype: "container",
                      height: 26,
                      layout: {
                        type: "hbox",
                        align: "stretch"
                      },
                      items: [
                        {
                          xtype: "combobox",
                          anchor: "100%",
                          labelWidth: 30,
                          flex: 1.5,
                          fieldLabel: "KRI",
                          msgTarget: "side",
                          fieldCls: "obligatoryTextField",
                          displayField: "description",
                          valueField: "idKeyRiskIndicator",
                          forceSelection: true,
                          editable: false,
                          name: "keyRiskIndicator",
                          itemId: "keyRiskIndicator",
                          store: {
                            fields: ["idKeyRiskIndicator", "description"],
                            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                            proxy: {
                              actionMethods: {
                                create: "POST",
                                read: "POST",
                                update: "POST"
                              },
                              type: "ajax",
                              url:
                                "http://localhost:9000/giro/showListKeyRiskIndicatorActives.htm",
                              extraParams: {
                                propertyOrder: "description"
                              },
                              reader: {
                                type: "json",
                                root: "data",
                                successProperty: "success"
                              }
                            }
                          },
                          listeners: {
                            select: function(cbo) {
                              me.down("#eachQualification").reset();
                              me.down("#eachQualification").setDisabled(false);

                              me.down("#eachQualification").store.load({
                                url:
                                  "http://localhost:9000/giro/showVersionQualificationEachKriActives.htm",
                                params: {
                                  idKri: cbo.getValue()
                                }
                              });
                            }
                          }
                        },
                        {
                          xtype: "combobox",
                          labelWidth: 60,
                          flex: 1,
                          padding: "0 0 0 5",
                          fieldLabel: "Umbral",
                          msgTarget: "side",
                          disabled: true,
                          queryMode: "local",
                          fieldCls: "obligatoryTextField",
                          displayField: "description",
                          valueField: "version",

                          editable: false,
                          name: "eachQualification",
                          itemId: "eachQualification",
                          store: {
                            autoLoad: false,
                            fields: [
                              "idKeyRiskIndicator",
                              "description",
                              "version"
                            ],
                            proxy: {
                              actionMethods: {
                                create: "POST",
                                read: "POST",
                                update: "POST"
                              },
                              type: "ajax",
                              url:
                                "http://localhost:9000/giro/loadGridDefault.htm",
                              reader: {
                                type: "json",
                                root: "data",
                                successProperty: "success"
                              }
                            }
                          },
                          listeners: {
                            select: function(cbo) {
                              var detailKri = me.down("#detailKri").el.dom;
                              Ext.Ajax.request({
                                method: "POST",
                                url:
                                  "http://localhost:9000/giro/showKriLastEvaluation.htm",
                                params: {
                                  idKri: me
                                    .down("#keyRiskIndicator")
                                    .getValue(),
                                  version: cbo.getValue()
                                },
                                success: function(response) {
                                  response = Ext.decode(response.responseText);
                                  var months = JSON.parse(response.months);
                                  var dataOne = JSON.parse(response.dataOne);

                                  if (response.success) {
                                    var chart = $(detailKri).highcharts();
                                    chart.destroy();
                                    for (var k = 0; k < dataOne.length; k++) {
                                      dataOne[k]["point"] = {
                                        events: {
                                          click: function(bar) {
                                            detailEvaluationKri(bar);
                                          }
                                        }
                                      };
                                    }
                                    $(detailKri).highcharts({
                                      chart: {
                                        type: "column"
                                      },
                                      credits: false,
                                      title: {
                                        text: ""
                                      },
                                      subtitle: {
                                        text: "Evolutivo de indicadores"
                                      },
                                      xAxis: {
                                        categories: months.split(",")
                                      },
                                      yAxis: {
                                        min: 0,
                                        title: {
                                          text: ""
                                        },
                                        stackLabels: {
                                          enabled: true,
                                          style: {
                                            fontWeight: "bold"
                                          }
                                        }
                                      },
                                      legend: {
                                        reserved: true
                                      },
                                      tooltip: {
                                        headerFormat: "<b>{point.x}</b><br/>",
                                        pointFormat:
                                          "{series.name}: {point.value}<br/>Total: {point.stackTotal}"
                                      },
                                      plotOptions: {
                                        column: {
                                          stacking: "normal",
                                          dataLabels: {
                                            enabled: true
                                          }
                                        }
                                      },
                                      series: dataOne
                                    });
                                  } else {
                                    DukeSource.global.DirtyView.messageAlert(
                                      DukeSource.global.GiroMessages
                                        .TITLE_WARNING,
                                      response.mensaje,
                                      Ext.Msg.ERROR
                                    );
                                  }
                                },
                                failure: function() {}
                              });
                            }
                          }
                        }
                      ]
                    },
                    {
                      xtype: "container",
                      itemId: "detailKri",
                      flex: 1.1
                    }
                  ]
                },
                {
                  xtype: "container",
                  itemId: "pieStateEvent",
                  flex: 1
                }
              ]
            },
            {
              xtype: "container",
              flex: 1,
              layout: {
                type: "vbox",
                align: "stretch"
              },
              items: [
                {
                  xtype: "container",
                  itemId: "actionPlanByDate",
                  flex: 1
                },
                {
                  xtype: "container",
                  itemId: "actionPlanByWorkArea",
                  flex: 1.2
                }
              ]
            }
          ],
          listeners: {
            activate: function(c) {
              var kriGeneral = c.down("#kriGeneral").el.dom;
              var detailKri = c.down("#detailKri").el.dom;
              var actionPlanByWorkArea = c.down("#actionPlanByWorkArea").el.dom;
              var pieStateEvent = c.down("#pieStateEvent").el.dom;
              var actionPlanByDate = c.down("#actionPlanByDate").el.dom;

              DukeSource.lib.Ajax.request({
                method: "POST",
                url: "http://localhost:9000/giro/showDataReportOne.htm",
                params: {},
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  var data = JSON.parse(response.data);
                  var kris = JSON.parse(response.kris);
                  var months =
                    response.months === undefined
                      ? ""
                      : JSON.parse(response.months);
                  var dataOne =
                    response.dataOne === undefined
                      ? []
                      : JSON.parse(response.dataOne);
                  var validity = JSON.parse(response.validityStates);
                  var dataTwo = JSON.parse(response.dataTwo);
                  var dataThree = JSON.parse(response.dataThree);
                  var workAreas = JSON.parse(response.workAreas);
                  var dataFour = JSON.parse(response.dataFour);

                  if (response.success) {
                    for (var k = 0; k < data.length; k++) {
                      data[k]["point"] = {
                        events: {
                          click: function(bar) {
                            detailEvaluationKri(bar);
                          }
                        }
                      };
                    }
                    $(kriGeneral).highcharts({
                      chart: {
                        type: "bar",
                        marginLeft: 150
                      },
                      credits: false,
                      title: {
                        text: ""
                      },
                      subtitle: {
                        text: "KRIs"
                      },
                      credits: {
                        enabled: false
                      },
                      xAxis: {
                        categories: kris === undefined ? [] : kris.split(","),
                        min: 0,
                        max: 20,
                        scrollbar: {
                          enabled: true
                        },
                        labels: {
                          style: {
                            fontSize: "10px"
                          }
                        }
                      },
                      yAxis: {
                        min: 0,
                        title: {
                          text: ""
                        },
                        stackLabels: {
                          enabled: true,
                          style: {
                            fontWeight: "bold",
                            fontSize: "10px"
                          }
                        }
                      },
                      legend: {
                        reversed: true
                      },
                      plotOptions: {
                        series: {
                          stacking: "normal"
                        },
                        bar: {
                          stacking: "normal"
                        },
                        column: {
                          stacking: "normal",
                          dataLabels: {
                            enabled: true
                          }
                        }
                      },
                      series: data === undefined ? [] : data
                    });

                    for (k = 0; k < dataOne.length; k++) {
                      dataOne[k]["point"] = {
                        events: {
                          click: function(bar) {
                            detailEvaluationKri(bar);
                          }
                        }
                      };
                    }
                    $(detailKri).highcharts({
                      chart: {
                        type: "column"
                      },
                      credits: false,
                      title: {
                        text: ""
                      },
                      subtitle: {
                        text: "Evolutivo de indicadores"
                      },
                      xAxis: {
                        categories:
                          months === undefined ? [] : months.split(",")
                      },
                      yAxis: {
                        min: 0,
                        title: {
                          text: ""
                        },
                        stackLabels: {
                          enabled: true,
                          style: {
                            fontWeight: "bold"
                          }
                        }
                      },
                      legend: {
                        reversed: true
                      },
                      tooltip: {
                        headerFormat: "<b>{point.x}</b><br/>",
                        pointFormat:
                          "{series.name}: {point.value}<br/>Total: {point.stackTotal}"
                      },
                      plotOptions: {
                        column: {
                          stacking: "normal",
                          dataLabels: {
                            enabled: true
                          }
                        }
                      },
                      series: dataOne === undefined ? [] : dataOne
                    });

                    $(pieStateEvent).highcharts({
                      chart: {
                        type: "pie",
                        options3d: {
                          enabled: true,
                          alpha: 45,
                          beta: 0
                        }
                      },
                      credits: false,
                      title: {
                        text: ""
                      },
                      subtitle: {
                        text: "Estado de los eventos de pérdida"
                      },
                      tooltip: {
                        pointFormat:
                          "Porcentaje: <b>{point.percentage:.1f}%</b><br>Nro Eventos: <b>{point.y}</b>"
                      },
                      plotOptions: {
                        pie: {
                          allowPointSelect: true,
                          cursor: "pointer",
                          depth: 35,
                          dataLabels: {
                            enabled: true,
                            format: "{point.percentage:.1f}%"
                          },
                          showInLegend: true
                        }
                      },
                      series: [
                        {
                          name: "Estado Evento",
                          data: dataThree,
                          point: {
                            events: {
                              click: function(pie) {
                                var win = Ext.create(
                                  "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowDashBoardManagerEvent",
                                  {
                                    modal: true
                                  }
                                );
                                win.show();
                                var grid = win.down("grid");
                                var fields = "es.id";
                                var values = pie.point.options["stateEvent"];
                                var types = "Integer";
                                var operator = "equal";
                                grid.store.proxy.url =
                                  "http://localhost:9000/giro/advancedSearchEvent.htm";
                                grid.store.proxy.extraParams = {
                                  fields: fields,
                                  values: values,
                                  types: types,
                                  operators: operator,
                                  searchIn: "Event"
                                };
                                grid.down("pagingtoolbar").moveFirst();
                              }
                            }
                          },
                          cursor: "pointer"
                        }
                      ]
                    });

                    for (k = 0; k < dataTwo.length; k++) {
                      dataTwo[k]["point"] = {
                        events: {
                          click: function(bar) {
                            var win = Ext.create(
                              "DukeSource.view.fulfillment.window.WindowDetailActionPlan",
                              {
                                modal: true
                              }
                            );
                            win.show();
                            var grid = win.down("grid");
                            var joke = bar.point.options["type"];
                            var fields =
                              joke === "REQUERIDOS"
                                ? "td.id,wa.id,sd.id"
                                : "td.id,wa.id,sd.id";
                            var values =
                              bar.point.options["validity"] +
                              "," +
                              workArea +
                              ",PR";
                            var types = "Integer,Integer,String";
                            var operator =
                              joke === "REQUERIDOS"
                                ? "equal,equal,equal"
                                : "equal,distinct,equal";
                            grid.store.proxy.url =
                              "http://localhost:9000/giro/findDetailDocument.htm";
                            grid.store.proxy.extraParams = {
                              fields: fields,
                              values: values,
                              types: types,
                              operator: operator,
                              searchIn: "ActionPlan",
                              condition: ""
                            };
                            grid.down("pagingtoolbar").moveFirst();
                          }
                        }
                      };
                    }
                    $(actionPlanByDate).highcharts({
                      chart: {
                        type: "bar"
                      },
                      credits: false,
                      title: {
                        text: ""
                      },
                      subtitle: {
                        text: "Planes de acción en proceso"
                      },
                      xAxis: {
                        categories: validity.split(",")
                      },
                      yAxis: {
                        min: 0,
                        enabled: false,
                        title: {
                          text: ""
                        }
                      },
                      legend: {
                        reversed: true
                      },
                      tooltip: {
                        headerFormat: "<b>{point.x}</b><br/>",
                        pointFormat:
                          "{series.name}: {point.y}<br/>Total: {point.stackTotal}"
                      },
                      plotOptions: {
                        series: {
                          stacking: "normal"
                        },
                        bar: {
                          stacking: "normal"
                        }
                      },
                      series: dataTwo
                    });

                    for (k = 0; k < dataFour.length; k++) {
                      dataFour[k]["point"] = {
                        events: {
                          click: function(bar) {
                            var win = Ext.create(
                              "DukeSource.view.fulfillment.window.WindowDetailActionPlan",
                              {
                                modal: true
                              }
                            );
                            win.show();
                            var grid = win.down("grid");
                            var fields = "sd.id,e.id";
                            var values =
                              bar.point.options["stateActionPlan"] +
                              "," +
                              bar.point.options["workArea"];
                            var types = "String,Integer";
                            var operator = "equal,equal";
                            grid.store.proxy.url =
                              "http://localhost:9000/giro/findDetailDocument.htm";
                            grid.store.proxy.extraParams = {
                              fields: fields,
                              values: values,
                              types: types,
                              operator: operator,
                              searchIn: "ActionPlan",
                              condition: ""
                            };
                            grid.down("pagingtoolbar").moveFirst();
                          }
                        }
                      };
                    }
                    $(actionPlanByWorkArea).highcharts({
                      chart: {
                        type: "bar"
                      },
                      credits: false,
                      title: {
                        text: ""
                      },
                      subtitle: {
                        text: "Planes de acción por áreas"
                      },
                      xAxis: {
                        categories: workAreas.split(","),
                        labels: {
                          style: {
                            fontSize: "9px"
                          }
                        }
                      },
                      yAxis: {
                        min: 0,
                        enabled: false,
                        title: {
                          text: ""
                        }
                      },
                      legend: {
                        reversed: true
                      },
                      tooltip: {
                        headerFormat: "<b>{point.x}</b><br/>",
                        pointFormat:
                          "{series.name}: {point.y}<br/>Total: {point.stackTotal}"
                      },
                      plotOptions: {
                        series: {
                          stacking: "normal"
                        },
                        bar: {
                          stacking: "normal"
                        }
                      },
                      series: dataFour
                    });
                  } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                  }
                },
                failure: function() {}
              });
            }
          }
        },
        {
          xtype: "container",
          id: "card-2",
          layout: {
            type: "hbox",
            align: "stretch"
          },
          padding: "2 2 2 2",
          items: [
            {
              xtype: "container",
              layout: {
                type: "vbox",
                align: "stretch"
              },
              flex: 1,
              items: [
                {
                  xtype: "container",
                  itemId: "lostEventByTypeEvent",
                  flex: 1
                },
                {
                  xtype: "container",
                  itemId: "lostEventByFactorRisk",
                  flex: 1
                }
              ]
            },
            {
              xtype: "container",
              flex: 1,
              layout: {
                type: "vbox",
                align: "stretch"
              },
              items: [
                {
                  xtype: "container",
                  flex: 1,
                  layout: {
                    type: "vbox",
                    align: "stretch"
                  },
                  items: [
                    {
                      xtype: "container",
                      width: "100%",
                      height: 26,
                      layout: {
                        type: "hbox",
                        align: "stretch"
                      },
                      items: [
                        {
                          xtype: "datefield",
                          format: "d/m/Y",
                          anchor: "100%",
                          padding: "0 0 0 5",
                          fieldLabel: "Desde",
                          name: "dateInit",
                          itemId: "dateInit",
                          fieldCls: "obligatoryTextField"
                        },
                        {
                          xtype: "datefield",
                          format: "d/m/Y",
                          anchor: "100%",
                          padding: "0 0 0 5",
                          fieldLabel: "Hasta",
                          name: "dateEnd",
                          itemId: "dateEnd",
                          fieldCls: "obligatoryTextField",
                          listeners: {
                            select: function(field, e) {
                              Ext.Ajax.request({
                                method: "POST",
                                url:
                                  "http://localhost:9000/giro/showEventMaterializedByDate.htm",
                                params: {
                                  dateInit: me.down("#dateInit").getRawValue(),
                                  dateEnd: field.getRawValue()
                                },
                                success: function(response) {
                                  response = Ext.decode(response.responseText);
                                  var risks = JSON.parse(response.risks);
                                  var dataOne = JSON.parse(response.dataOne);

                                  if (response.success) {
                                    var lostEventMaterialized = me.down(
                                      "#lostEventMaterialized"
                                    ).el.dom;
                                    var chart = $(
                                      lostEventMaterialized
                                    ).highcharts();
                                    chart.destroy();
                                    for (var k = 0; k < dataOne.length; k++) {
                                      dataOne[k]["point"] = {
                                        events: {
                                          click: function(bar) {
                                            var win = Ext.create(
                                              "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowDashBoardManagerEvent",
                                              {
                                                modal: true
                                              }
                                            );
                                            win.show();
                                            var grid = win.down("grid");
                                            var fields = "ri.id";
                                            var values =
                                              bar.point.options["idRisk"];
                                            var types = "Long";
                                            var operator = "equal";
                                            grid.store.proxy.url =
                                              "http://localhost:9000/giro/advancedSearchEvent.htm";
                                            grid.store.proxy.extraParams = {
                                              fields: fields,
                                              values: values,
                                              types: types,
                                              operators: operator,
                                              searchIn: "EventByRisk"
                                            };
                                            grid
                                              .down("pagingtoolbar")
                                              .moveFirst();
                                          }
                                        }
                                      };
                                    }

                                    $(lostEventMaterialized).highcharts({
                                      chart: {
                                        type: "column"
                                      },
                                      credits: false,
                                      title: {
                                        text: ""
                                      },
                                      subtitle: {
                                        text:
                                          "Riesgos con mayores pérdidas materializadas"
                                      },
                                      xAxis: {
                                        categories: risks.split(",")
                                      },
                                      yAxis: {
                                        min: 0,
                                        enabled: false,
                                        title: {
                                          text: ""
                                        }
                                      },
                                      legend: {
                                        reversed: true
                                      },
                                      tooltip: {
                                        headerFormat: "<b>{point.x}</b><br/>",
                                        pointFormat:
                                          "{series.name}: {point.y}<br/>Total: {point.stackTotal}"
                                      },
                                      plotOptions: {
                                        series: {
                                          stacking: "normal"
                                        },
                                        bar: {
                                          stacking: "normal"
                                        }
                                      },
                                      series: dataOne
                                    });
                                  } else {
                                    DukeSource.global.DirtyView.messageWarning(response.message);
                                  }
                                },
                                failure: function() {}
                              });
                            }
                          }
                        }
                      ]
                    },
                    {
                      xtype: "container",
                      itemId: "lostEventMaterialized",
                      flex: 1
                    }
                  ]
                },
                {
                  xtype: "container",
                  flex: 1,
                  layout: {
                    type: "vbox",
                    align: "stretch"
                  },
                  items: [
                    {
                      xtype: "ViewComboProcess",
                      labelWidth: 100,
                      height: 20,
                      width: "100%",
                      queryMode: "remote",
                      name: "process",
                      itemId: "process",
                      fieldCls: "obligatoryTextField",
                      plugins: ["ComboSelectCount"],
                      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                      fieldLabel: "PROCESO",
                      listeners: {
                        beforerender: function(cbo) {
                          if (category !== DukeSource.global.GiroConstants.ANALYST) {
                            cbo.store.proxy.url =
                              "http://localhost:9000/giro/showListProcessAssigned.htm";
                            cbo.store.proxy.extraParams = {
                              username: userName
                            };
                          }
                        },
                        select: function(cbo) {
                          Ext.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/showEventScaleRiskByProcess.htm",
                            params: {
                              idProcess: cbo.getValue()
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              var scaleRisk = JSON.parse(response.scaleRisk);
                              var dataOne = JSON.parse(response.dataOne);

                              if (response.success) {
                                var lostEventLevelRisk = me.down(
                                  "#lostEventLevelRisk"
                                ).el.dom;
                                var chart = $(lostEventLevelRisk).highcharts();
                                chart.destroy();
                                for (var k = 0; k < dataOne.length; k++) {
                                  dataOne[k]["point"] = {
                                    events: {
                                      click: function(bar) {
                                        var win = Ext.create(
                                          "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowDashBoardManagerEvent",
                                          {
                                            modal: true
                                          }
                                        );
                                        win.show();
                                        var grid = win.down("grid");
                                        var fields =
                                          "srr.id.idScaleRisk,ver.process.id";
                                        var values =
                                          bar.point.options["idScaleRisk"] +
                                          "," +
                                          cbo.getValue();
                                        var types = "Integer,Integer";
                                        var operator = "equal,equal";
                                        grid.store.proxy.url =
                                          "http://localhost:9000/giro/advancedSearchEvent.htm";
                                        grid.store.proxy.extraParams = {
                                          fields: fields,
                                          values: values,
                                          types: types,
                                          operators: operator,
                                          searchIn: "EventByRisk"
                                        };
                                        grid.down("pagingtoolbar").moveFirst();
                                      }
                                    }
                                  };
                                }
                                $(lostEventLevelRisk).highcharts({
                                  chart: {
                                    type: "column"
                                  },
                                  credits: false,
                                  title: {
                                    text: ""
                                  },
                                  subtitle: {
                                    text: "Pérdidas por nivel de riesgo"
                                  },
                                  xAxis: {
                                    categories: scaleRisk.split(",")
                                  },
                                  yAxis: {
                                    min: 0,
                                    enabled: false,
                                    title: {
                                      text: ""
                                    }
                                  },
                                  legend: {
                                    reversed: true
                                  },
                                  tooltip: {
                                    headerFormat: "<b>{point.x}</b><br/>",
                                    pointFormat:
                                      "{series.name}: {point.y}<br/>Nro Eventos: {point.numberEvents}<br/>Total: {point.stackTotal}"
                                  },
                                  plotOptions: {
                                    series: {
                                      stacking: "normal"
                                    },
                                    bar: {
                                      stacking: "normal"
                                    }
                                  },
                                  series: dataOne
                                });
                              } else {
                                DukeSource.global.DirtyView.messageWarning(response.message);
                              }
                            },
                            failure: function() {}
                          });
                        }
                      }
                    },
                    {
                      xtype: "container",
                      flex: 1,
                      itemId: "lostEventLevelRisk"
                    }
                  ]
                }
              ]
            }
          ],
          listeners: {
            activate: function(c) {
              var lostEventByTypeEvent = c.down("#lostEventByTypeEvent").el.dom;
              var lostEventByFactorRisk = c.down("#lostEventByFactorRisk").el
                .dom;
              var lostEventMaterialized = c.down("#lostEventMaterialized").el
                .dom;
              var lostEventLevelRisk = c.down("#lostEventLevelRisk").el.dom;

              DukeSource.lib.Ajax.request({
                method: "POST",
                url: "http://localhost:9000/giro/showDataReportTwo.htm",
                params: {},
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  var typeEvents = JSON.parse(response.typeEvents);
                  var data = JSON.parse(response.data);
                  var factors = JSON.parse(response.factors);
                  var dataOne = JSON.parse(response.dataOne);
                  var risks = JSON.parse(response.risks);
                  var dataTwo = JSON.parse(response.dataTwo);
                  var scaleRisk = JSON.parse(response.scaleRisk);
                  var dataThree = JSON.parse(response.dataThree);

                  if (response.success) {
                    for (var k = 0; k < data.length; k++) {
                      data[k]["point"] = {
                        events: {
                          click: function(bar) {
                            var win = Ext.create(
                              "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowDashBoardManagerEvent",
                              {
                                modal: true
                              }
                            );
                            win.show();
                            var grid = win.down("grid");
                            var fields = "eo.id";
                            var values = bar.point.options["eventOne"];
                            var types = "Integer";
                            var operator = "equal";
                            grid.store.proxy.url =
                              "http://localhost:9000/giro/advancedSearchEvent.htm";
                            grid.store.proxy.extraParams = {
                              fields: fields,
                              values: values,
                              types: types,
                              operators: operator,
                              searchIn: "Event"
                            };
                            grid.down("pagingtoolbar").moveFirst();
                          }
                        }
                      };
                    }
                    $(lostEventByTypeEvent).highcharts({
                      chart: {
                        type: "bar"
                      },
                      credits: false,
                      title: {
                        text: ""
                      },
                      subtitle: {
                        text: "Pérdidas por tipo de evento"
                      },
                      xAxis: {
                        categories: typeEvents.split(","),
                        labels: {
                          style: {
                            fontSize: "9px"
                          }
                        }
                      },
                      yAxis: {
                        min: 0,
                        enabled: false,
                        title: {
                          text: ""
                        }
                      },
                      legend: {
                        reversed: true
                      },
                      tooltip: {
                        headerFormat: "<b>{point.x}</b><br/>",
                        pointFormat:
                          "{series.name}: {point.y}<br/>Nro Eventos: {point.numberEvents}"
                      },
                      plotOptions: {
                        series: {
                          stacking: "normal"
                        },
                        bar: {
                          stacking: "normal"
                        }
                      },
                      series: data
                    });

                    for (var k = 0; k < dataOne.length; k++) {
                      dataOne[k]["point"] = {
                        events: {
                          click: function(bar) {
                            var win = Ext.create(
                              "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowDashBoardManagerEvent",
                              {
                                modal: true
                              }
                            );
                            win.show();
                            var grid = win.down("grid");
                            var fields = "fr.id";
                            var values = bar.point.options["factorRisk"];
                            var types = "Integer";
                            var operator = "equal";
                            grid.store.proxy.url =
                              "http://localhost:9000/giro/advancedSearchEvent.htm";
                            grid.store.proxy.extraParams = {
                              fields: fields,
                              values: values,
                              types: types,
                              operators: operator,
                              searchIn: "Event"
                            };
                            grid.down("pagingtoolbar").moveFirst();
                          }
                        }
                      };
                    }
                    $(lostEventByFactorRisk).highcharts({
                      chart: {
                        type: "bar"
                      },
                      credits: false,
                      title: {
                        text: ""
                      },
                      subtitle: {
                        text: "Pérdidas por factor de riesgo"
                      },
                      xAxis: {
                        categories: factors.split(","),
                        labels: {
                          style: {
                            fontSize: "9px"
                          }
                        }
                      },
                      yAxis: {
                        min: 0,
                        enabled: false,
                        title: {
                          text: ""
                        }
                      },
                      legend: {
                        reversed: true
                      },
                      tooltip: {
                        headerFormat: "<b>{point.x}</b><br/>",
                        pointFormat:
                          "{series.name}: {point.y}<br/>Nro Eventos: {point.numberEvents}"
                      },
                      plotOptions: {
                        series: {
                          stacking: "normal"
                        },
                        bar: {
                          stacking: "normal"
                        }
                      },
                      series: dataOne
                    });

                    for (var k = 0; k < dataTwo.length; k++) {
                      dataTwo[k]["point"] = {
                        events: {
                          click: function(bar) {
                            var win = Ext.create(
                              "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowDashBoardManagerEvent",
                              {
                                modal: true
                              }
                            );
                            win.show();
                            var grid = win.down("grid");
                            var fields = "ri.id";
                            var values = bar.point.options["idRisk"];
                            var types = "Long";
                            var operator = "equal";
                            grid.store.proxy.url =
                              "http://localhost:9000/giro/advancedSearchEvent.htm";
                            grid.store.proxy.extraParams = {
                              fields: fields,
                              values: values,
                              types: types,
                              operators: operator,
                              searchIn: "EventByRisk"
                            };
                            grid.down("pagingtoolbar").moveFirst();
                          }
                        }
                      };
                    }
                    $(lostEventMaterialized).highcharts({
                      chart: {
                        type: "column"
                      },
                      credits: false,
                      title: {
                        text: ""
                      },
                      subtitle: {
                        text: "Riesgos con mayores pérdidas materializadas"
                      },
                      xAxis: {
                        categories: risks.split(",")
                      },
                      yAxis: {
                        min: 0,
                        enabled: false,
                        title: {
                          text: ""
                        }
                      },
                      legend: {
                        reversed: true
                      },
                      tooltip: {
                        headerFormat: "<b>{point.x}</b><br/>",
                        pointFormat: "{series.name}: {point.y}"
                      },
                      plotOptions: {
                        series: {
                          stacking: "normal"
                        },
                        bar: {
                          stacking: "normal"
                        }
                      },
                      series: dataTwo
                    });

                    for (var k = 0; k < dataThree.length; k++) {
                      dataThree[k]["point"] = {
                        events: {
                          click: function(bar) {
                            var win = Ext.create(
                              "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowDashBoardManagerEvent",
                              {
                                modal: true
                              }
                            );
                            win.show();
                            var grid = win.down("grid");
                            var fields = "srr.id.idScaleRisk";
                            var values = bar.point.options["idScaleRisk"];
                            var types = "Integer";
                            var operator = "equal";
                            grid.store.proxy.url =
                              "http://localhost:9000/giro/advancedSearchEvent.htm";
                            grid.store.proxy.extraParams = {
                              fields: fields,
                              values: values,
                              types: types,
                              operators: operator,
                              searchIn: "EventByRisk"
                            };
                            grid.down("pagingtoolbar").moveFirst();
                          }
                        }
                      };
                    }
                    $(lostEventLevelRisk).highcharts({
                      chart: {
                        type: "column"
                      },
                      credits: false,
                      title: {
                        text: ""
                      },
                      subtitle: {
                        text: "Pérdidas por nivel de riesgo"
                      },
                      xAxis: {
                        categories: scaleRisk.split(",")
                      },
                      yAxis: {
                        min: 0,
                        enabled: false,
                        title: {
                          text: ""
                        }
                      },
                      legend: {
                        reversed: true
                      },
                      tooltip: {
                        headerFormat: "<b>{point.x}</b><br/>",
                        pointFormat:
                          "{series.name}: {point.y}<br/>Nro Eventos: {point.numberEvents}"
                      },
                      plotOptions: {
                        series: {
                          stacking: "normal"
                        },
                        bar: {
                          stacking: "normal"
                        }
                      },
                      series: dataThree
                    });
                  } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                  }
                },
                failure: function() {}
              });
            }
          }
        }
      ]
    });
    me.callParent(arguments);
  }
});

function detailProcessCritic(bar) {
  var win = Ext.create(
    "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDashboardManagerRisk",
    {
      modal: true
    }
  );
  win.show();
  var grid = win.down("grid");
  var fields = "p.idProcess,madet.idScaleRisk.id.idScaleRisk";
  var values =
    bar.point.options["idProcess"] + ";" + bar.point.options["idScaleRisk"];
  var types = "Integer,Integer,Integer";
  var operator = "equal,equal,equal";
  grid.store.proxy.url = "http://localhost:9000/giro/findMatchRisk.htm";
  grid.store.proxy.extraParams = {
    fields: fields,
    values: values,
    types: types,
    operators: operator,
    search: "full",
    isLast: "false",
    condition: ""
  };
  grid.down("pagingtoolbar").doRefresh();
}

function detailActionPlan(bar) {
  var win = Ext.create(
    "DukeSource.view.fulfillment.window.WindowDetailActionPlan",
    {
      modal: true
    }
  );
  win.show();
  var grid = win.down("grid");
  var joke = bar.point.options["type"];
  var fields = joke === "REQUERIDOS" ? "sd.id,wa.id" : "sd.id,wa.id";
  var values = bar.point.options["stateActionPlan"] + "," + workArea;
  var types = "String,Integer";
  var operator = joke === "REQUERIDOS" ? "equal,equal" : "equal,distinct";
  grid.store.proxy.url = "http://localhost:9000/giro/findDetailDocument.htm";
  grid.store.proxy.extraParams = {
    fields: fields,
    values: values,
    types: types,
    operator: operator,
    searchIn: "ActionPlan",
    condition: ""
  };
  grid.down("pagingtoolbar").doRefresh();
}

function detailEvaluationKri(bar) {
  var win = Ext.create(
    "DukeSource.view.risk.kri.windows.WindowDetailEvaluationKri",
    {
      modal: true
    }
  );
  win.show();
  var idKeyRiskIndicator = bar.point.options["codeKri"];
  var indicatorNormalizer = bar.point.options["indicatorNormalizer"];
  var indicatorKri = bar.point.options["indicatorKri"];
  var idQualification = bar.point.options["idQualification"];
  var year = bar.point.options["yearMonth"].substr(0, 4);
  var month = bar.point.options["yearMonth"].substr(4, 5);

  giro.lib.Ajax.request({
    method: "POST",
    url: "http://localhost:9000/giro/buildJsonByIdKriAndYearMonth.htm",
    params: {
      idKeyRiskIndicator: idKeyRiskIndicator,
      indicatorNormalizer: indicatorNormalizer,
      indicatorKri: indicatorKri,
      idQualification: idQualification,
      year: year,
      month: month
    },
    success: function(response) {
      response = Ext.decode(response.responseText);
      if (response.success) {
        Ext.define("ModelGriEvalKri", {
          extend: "Ext.data.Model",
          fields: JSON.parse(response.fields)
        });

        var storeDetailEval = Ext.create("Ext.data.Store", {
          model: "ModelGriEvalKri",
          pageSize: DukeSource.global.GiroConstants.SUPER_ITEMS_PAGE,
          proxy: {
            actionMethods: {
              create: "POST",
              read: "POST",
              update: "POST"
            },
            type: "ajax",
            url: "http://localhost:9000/giro/buildJsonByIdKriAndYearMonth.htm",
            reader: {
              type: "json",
              root: "data",
              successProperty: "success",
              totalProperty: "totalCount"
            }
          }
        });
        storeDetailEval.loadData(JSON.parse(response.data));
        storeDetailEval.totalCount = response.totalCount;

        var pagingBar = Ext.create("Ext.toolbar.Paging", {
          pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
          store: storeDetailEval,
          displayInfo: true,
          displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
          items: [
            "-",
            {
              text: "Exportar",
              iconCls: "excel",
              handler: function(b, e) {
                b.up("grid").downloadExcelXml();
              }
            }
          ],
          emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
        });

        pagingBar.onLoad();
        var grid = Ext.create("Ext.grid.Panel", {
          id: "gridDetailEvaluationKri",
          store: storeDetailEval,
          loadMask: true,
          columnLines: true,
          region: "center",
          flex: 20,
          multiSelect: true,
          title: "Detalle de evaluación",
          titleAlign: "center",
          forceFit: true,
          columns: JSON.parse(response.columns),
          stripeRows: true,
          bbar: pagingBar
        });

        win.add(grid);
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_ERROR,
          response.mensaje,
          Ext.Msg.ERROR
        );
      }
    },
    failure: function() {}
  });
}
