Ext.define('DukeSource.view.main.ViewPanelMain', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelMain',
    layout: 'border',
    initComponent: function () {
        this.navigationMain = Ext.create('DukeSource.view.main.ViewTreeMain', {
            title: 'Riesgo Operacional',
            region: 'west',
            collapsible: true,
            hidden: true,
            split: true,
            margins: '2 0 2 2',
            width: 280
        });
        this.editorTabPanelMain = Ext.create('Ext.tab.Panel', {
            tabPosition: 'bottom',
            itemId: 'principalTab',
            action: 'principalTab',
            bodyStyle: 'background:#D5E2F2',
            region: 'center',
            margins: '2 2 2 0',
            layout: 'fit'
        });


        this.items = [this.navigationMain, this.editorTabPanelMain];
        this.callParent();

    },
    addPanel: function (item) {
        var cmp = this.editorTabPanelMain.down("[title=" + item.title + "]");
        if (!cmp) {
            this.editorTabPanelMain.add(item);
            this.editorTabPanelMain.setActiveTab(item);
        } else {
            this.editorTabPanelMain.setActiveTab(cmp);
        }
    }

});
DukeSource.getComponent = function () {
};
