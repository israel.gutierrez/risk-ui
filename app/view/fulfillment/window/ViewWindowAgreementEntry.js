Ext.define('DukeSource.view.fulfillment.window.ViewWindowAgreementEntry', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowAgreementEntry',
//    height: 240,
    width: 500,
    layout: {
        type: 'fit'
    },
    title: 'INGRESAR ACUERDO',
    titleAlign: 'center',
    border: false,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    layout: 'anchor',
                    items: [
                        {
                            xtype: 'textfield',
                            hidden: true,
                            value: 'id',
                            name: 'idAgreement'
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            fieldLabel: 'CODIGO ACUERDO',
                            anchor: '100%',
                            name: 'codeAgreement'
                        },
                        {
                            xtype: 'UpperCaseTextFieldObligatory',
                            fieldLabel: 'CODIGO COMITE',
                            anchor: '100%',
                            name: 'codeCommittee'
                        },
                        {
                            xtype: 'datefield',
                            allowBlank: false,
                            msgTarget: 'side',
                            fieldCls: 'obligatoryTextField',
                            blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            fieldLabel: 'FECHA ACUERDO',
                            anchor: '100%',
                            name: 'dateAgreement'
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            height: 75,
                            anchor: '100%',
                            maxLengthText:DukeSource.global.GiroMessages.MESSAGE_MAX_CHARACTER + 300,
                            maxLength: 300,
                            allowBlank: false,
                            fieldLabel: 'DESCRIPCION',
                            name: 'description',
                            listeners: {
                                specialkey: function (f, e) {
                                   DukeSource.global.DirtyView.focusEventEnter(f, e, me.down('button[action=saveNewDebility]'))
                                }
                            }
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'GUARDAR',
                    action: 'saveAgreementOfActionPlan',
                    iconCls: 'save'
                },
                {
                    text: 'SALIR',
                    iconCls: 'logout',
                    handler: function () {
                        me.close();
                    }
                }
            ]
        });

        me.callParent(arguments);
    }

});