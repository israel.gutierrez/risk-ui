Ext.define("ModelIncidentAssignedPlan", {
  extend: "Ext.data.Model",
  fields: [
    "idActionPlanIncident",
    "idIndicator",
    "idActionPlan",
    "description",
    "state",
    "codeKri",
    "indicatorConfiguration",
    "descriptionIndicator",
    "abbreviationIndicator",
    "descriptionWorkArea"
  ]
});
var StoreGridActionPlan = Ext.create("Ext.data.Store", {
  model: "ModelIncidentAssignedPlan",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.fulfillment.window.WindowActionPlanRelationIndicator",
  {
    extend: "Ext.window.Window",
    alias: "widget.WindowActionPlanRelationIndicator",
    height: 450,
    width: 900,
    layout: "border",
    border: false,
    title: "Indicadores vinculados al plan de acci&oacute;n",
    titleAlign: "center",
    initComponent: function() {
      var me = this;
      var idActionPlan = this.idActionPlan;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            region: "center",
            width: 450,
            store: StoreGridActionPlan,
            tbar: [
              {
                xtypoe: "button",
                iconCls: "add",
                text: "Vincular",
                handler: function() {
                  var windowActionPlan = Ext.create(
                    "DukeSource.view.fulfillment.window.ViewWindowSearchActionPlan",
                    {
                      modal: false,
                      buttonAlign: "center",
                      buttons: [
                        {
                          text: "Vincular",
                          iconCls: "save",
                          scale: "medium",
                          handler: function() {
                            var recordActionPlan = windowActionPlan
                              .down("grid")
                              .getSelectionModel()
                              .getSelection()[0];
                            var gridAction = me.down("#gridActionPlanAssigned");
                            var records = gridAction.getStore().getRange();
                            var total = gridAction.getStore().getCount();

                            if (
                              windowActionPlan
                                .down("grid")
                                .getSelectionModel()
                                .getCount() === 0
                            ) {
                             DukeSource.global.DirtyView.messageWarning(
                                "Seleccione un plan de acci&oacute;n por favor"
                              );
                            } else {
                              var equals;
                              for (var int = 0; int < total; int++) {
                                if (
                                  records[int].get("idActionPlan") ===
                                  recordActionPlan.get("id")
                                ) {
                                  equals = true;
                                  break;
                                } else {
                                  equals = false;
                                }
                              }
                              if (equals) {
                               DukeSource.global.DirtyView.messageWarning(
                                  "Se encuentra asignada, seleccione otro plan de acción por favor"
                                );
                              } else {
                                DukeSource.lib.Ajax.request({
                                  method: "POST",
                                  url:
                                    "http://localhost:9000/giro/saveRelationActionPlanEvent.htm",
                                  params: {
                                    idActionPlan: idActionPlan,
                                    idIndicator: recordActionPlan.get(
                                      "idIndicator"
                                    )
                                  },
                                  success: function(response) {
                                    response = Ext.decode(
                                      response.responseText
                                    );
                                    if (response.success) {
                                      Ext.MessageBox.show({
                                        title:DukeSource.global.GiroMessages.TITLE_CONFIRM,
                                        msg:
                                          response.message +
                                          ", desea vincular mas planes de acci&oacute;n ?",
                                        icon: Ext.Msg.QUESTION,
                                        buttonText: {
                                          yes: "Si"
                                        },
                                        buttons: Ext.MessageBox.YESNO,
                                        fn: function(btn) {
                                          if (btn !== "yes") {
                                            windowActionPlan.close();
                                          }
                                        }
                                      });
                                      me.down("#gridActionPlanAssigned")
                                        .down("pagingtoolbar")
                                        .moveFirst();
                                    } else {
                                     DukeSource.global.DirtyView.messageWarning(
                                        response.message
                                      );
                                    }
                                  }
                                });
                              }
                            }
                          }
                        },
                        {
                          text: "Salir",
                          scale: "medium",
                          iconCls: "logout",
                          handler: function() {
                            windowActionPlan.close();
                          }
                        }
                      ]
                    }
                  ).show();
                }
              },
              {
                xtype: "button",
                iconCls: "delete",
                text: "Eliminar",
                handler: function() {
                  var grid = me.down("#gridActionPlanAssigned");

                  var assigned = grid.getSelectionModel().getSelection()[0];

                  if (assigned === undefined) {
                   DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
                  } else {
                    Ext.MessageBox.show({
                      title:DukeSource.global.GiroMessages.TITLE_CONFIRM,
                      msg: "Esta seguro que desea eliminar el riesgo",
                      icon: Ext.Msg.QUESTION,
                      buttonText: {
                        yes: "Si"
                      },
                      buttons: Ext.MessageBox.YESNO,
                      fn: function(btn) {
                        if (btn === "yes") {
                          DukeSource.lib.Ajax.request({
                            method: "POST",
                            url:
                              "http://localhost:9000/giro/deleteRelationActionPlanEvent.htm",
                            params: {
                              idActionPlanEvent: assigned.get(
                                "idEventActionPlan"
                              ),
                              idActionPlan: assigned.get("idActionPlan"),
                              idEvent: assigned.get("idEvent")
                            },
                            success: function(response) {
                              response = Ext.decode(response.responseText);
                              if (response.success) {
                               DukeSource.global.DirtyView.messageNormal(response.message);
                                grid.down("pagingtoolbar").doRefresh();
                              } else {
                               DukeSource.global.DirtyView.messageWarning(response.message);
                              }
                            },
                            failure: function() {}
                          });
                        }
                      }
                    });
                  }
                }
              }
            ],
            columns: [
              {
                dataIndex: "codeKri",
                width: 60,
                header: "Código"
              },
              {
                dataIndex: "nameKri",
                flex: 5,
                header: "Descripción"
              },
              {
                dataIndex: "indicatorConfiguration",
                width: 100,
                align: "center",
                header: "Configuración",
                renderer: function(v, metaData, record) {
                  if (v === "N") {
                    return '<div style="color:#f38c1f; font-size: 10px"><b>INCOMPLETA</b></div>';
                  } else {
                    return '<div style="color:#2d9231; font-size: 10px"><b>COMPLETA</b></div>';
                  }
                }
              },
              {
                dataIndex: "descriptionWorkArea",
                width: 200,
                header: "Área responsable"
              },
              {
                dataIndex: "descriptionFrequency",
                width: 110,
                align: "center",
                header: "Frecuencia de reporte"
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
              store: StoreGridActionPlan,
              displayInfo: true,
              displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
              doRefresh: function() {
                var grid = this,
                  current = grid.store.currentPage;
                if (grid.fireEvent("beforechange", grid, current) !== false) {
                  grid.store.loadPage(current);
                }
              }
            },
            listeners: {
              render: function() {
                var grid = me.down("grid");
                grid.store.getProxy().extraParams = {
                  idActionPlan: idActionPlan
                };
                grid.store.getProxy().url =
                  "http://localhost:9000/giro/loadGridDefault.htm";
                grid.down("pagingtoolbar").moveFirst();
              }
            }
          }
        ],
        buttons: [
          {
            text: "Salir",
            scale: "medium",
            iconCls: "logout",
            handler: function() {
              me.close();
            }
          }
        ]
      });
      me.callParent(arguments);
    }
  }
);
