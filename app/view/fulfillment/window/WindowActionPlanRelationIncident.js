Ext.define('ModelIncidentAssignedPlan', {
    extend: 'Ext.data.Model',
    fields: [
        'idActionPlanIncident',
        'idIncident',
        'idActionPlan',
        'description',
        'state',
        'codeIssue',
        'stateIncident',
        'dateRegister',
        'userReport',
        'descriptionShort',
        'descriptionLarge',
        'fullNameUserReport',
        'descriptionStateIncident',
        'colorStateIncident',
        {
            name: 'dateReport', type: 'date'
        }
    ]
});
var StoreGridActionPlan = Ext.create('Ext.data.Store', {
    model: 'ModelIncidentAssignedPlan',
    autoLoad: false,
    proxy: {
        actionMethods: {
            create: 'POST',
            read: 'GET',
            update: 'UPDATE',
            delete: 'DELETE'
        },
        type: 'ajax',
        url: 'plan/relation/incident/',
        reader: {
            totalProperty: 'totalCount',
            root: 'data',
            successProperty: 'success'
        }
    }
});

Ext.define('DukeSource.view.fulfillment.window.WindowActionPlanRelationIncident', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowActionPlanRelationIncident',
    height: 450,
    width: 900,
    layout: 'border',
    border: false,
    title: 'Incidentes vinculados al plan de acci&oacute;n',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        var idActionPlan = this.idActionPlan;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'gridpanel',
                    region: 'center',
                    width: 450,
                    store: StoreGridActionPlan,
                    tbar: [
                        {
                            xtypoe: 'button',
                            iconCls: 'add',
                            text: 'Vincular',
                            handler: function () {

                                var win = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.WindowSearchIncident', {
                                    modal: false,
                                    buttonAlign: 'center',
                                    buttons: [
                                        {
                                            text: 'Vincular',
                                            iconCls: 'save',
                                            scale: 'medium',
                                            handler: function () {
                                                var rowIssue = win.down('grid').getSelectionModel().getSelection()[0];

                                                DukeSource.lib.Ajax.request({
                                                    method: 'POST',
                                                    url: 'plan/relation/incident/register',
                                                    params: {
                                                        idActionPlan: idActionPlan,
                                                        idIssue: rowIssue.get('idIncident')
                                                    },
                                                    success: function (response) {
                                                        response = Ext.decode(response.responseText);
                                                        if (response.success) {

                                                            Ext.MessageBox.show({
                                                                title:DukeSource.global.GiroMessages.TITLE_CONFIRM,
                                                                msg: response.message + ', desea vincular m&aacute;s incidentes?',
                                                                icon: Ext.Msg.QUESTION,
                                                                buttonText: {
                                                                    yes: "Si"
                                                                },
                                                                buttons: Ext.MessageBox.YESNO,
                                                                fn: function (btn) {
                                                                    if (btn !== 'yes') {
                                                                        win.close();
                                                                    }
                                                                }
                                                            });

                                                            me.down('grid').down('pagingtoolbar').doRefresh();
                                                        } else {
                                                           DukeSource.global.DirtyView.messageWarning(response.message);
                                                        }
                                                    }
                                                });
                                            }
                                        },
                                        {
                                            text: 'Salir',
                                            scale: 'medium',
                                            iconCls: 'logout',
                                            handler: function () {
                                                win.close();
                                            }
                                        }
                                    ]
                                }).show();
                            }
                        },
                        {
                            xtype: 'button',
                            iconCls: 'delete',
                            text: 'Eliminar',
                            handler: function () {
                                var grid = me.down('grid');
                                var rowIssue = grid.getSelectionModel().getSelection()[0];

                                if (rowIssue === undefined) {
                                   DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
                                } else {
                                    Ext.MessageBox.show({
                                        title:DukeSource.global.GiroMessages.TITLE_CONFIRM,
                                        msg: 'Esta seguro que desea eliminar el riesgo',
                                        icon: Ext.Msg.QUESTION,
                                        buttonText: {
                                            yes: "Si"
                                        },
                                        buttons: Ext.MessageBox.YESNO,
                                        fn: function (btn) {
                                            if (btn === 'yes') {

                                                DukeSource.lib.Ajax.request({
                                                    method: 'DELETE',
                                                    url: 'plan/relation/incident/' + rowIssue.get('id'),

                                                    success: function (response) {
                                                        response = Ext.decode(response.responseText);

                                                        if (response.success) {
                                                           DukeSource.global.DirtyView.messageNormal(response.message);
                                                            grid.down('pagingtoolbar').doRefresh();
                                                        } else {
                                                           DukeSource.global.DirtyView.messageWarning(response.message);
                                                        }
                                                    },
                                                    failure: function () {
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    ],
                    columns: [
                        {
                            header: 'Código incidente', align: 'left', dataIndex: 'codeIssue', width: 130,
                            renderer: function (value, metaData, record) {
                                var attachment = '';
                                var secret = '';
                                var state = '<div style="font-size:9px">' + record.get('descriptionStateIncident') + '</div>';
                                if (record.get('fileAttachment') === 'S') {
                                    attachment = '<div style="display:inline-block;"><i class="tesla even-attachment"></i></div>';

                                }
                                if (record.get("confidential") === "S") {
                                    secret = '<div style="display:inline-block;padding-left:2px;"><i class="tesla even-user-secret"></i></div>';
                                }
                                metaData.tdAttr = 'style="background-color: #' + record.get('colorStateIncident') + ' !important;"';
                                return '<div style="display:inline-block;font-size:12px;padding:5px;">' + attachment + secret + '' +
                                    '<span style="padding-left:5px;font-weight:bold">' + value + '</span></div>' + state;
                            }
                        },
                        {
                            header: "Fecha de reporte",
                            align: "center",
                            dataIndex: "dateReport",
                            format: "d/m/Y H:i:s",
                            xtype: 'datecolumn',
                            width: 80
                        },
                        {
                            header: "Usuario que reporta",
                            dataIndex: "fullNameUserReport",
                            width: 150
                        },
                        {
                            header: "Descripci&oacute;n corta",
                            dataIndex: "descriptionShort",
                            width: 150
                        },
                        {
                            header: "Descripci&oacute;n larga",
                            dataIndex: "descriptionLarge",
                            width: 480
                        }
                    ],
                    bbar: {
                        xtype: 'pagingtoolbar',
                        pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
                        store: StoreGridActionPlan,
                        displayInfo: true,
                        displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                        emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER,
                        doRefresh: function () {
                            var grid = this,
                                current = grid.store.currentPage;
                            if (grid.fireEvent('beforechange', grid, current) !== false) {
                                grid.store.loadPage(current);
                            }
                        }
                    },
                    listeners: {
                        render: function () {
                            var grid = me.down('grid');
                            grid.store.getProxy().url = 'plan/relation/incident/' + idActionPlan;
                            grid.down('pagingtoolbar').doRefresh();
                        }
                    }
                }
            ],
            buttons: [
                {
                    text: 'Salir',
                    scale: 'medium',
                    iconCls: 'logout',
                    handler: function () {
                        me.close();
                    }
                }
            ]
        });
        me.callParent(arguments);
    }
});