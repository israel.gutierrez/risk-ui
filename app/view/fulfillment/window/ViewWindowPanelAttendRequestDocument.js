Ext.define("ModelAttendRequestReprogram", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "idDocument",
    "stateQualification",
    "descriptionStateQualification",
    "dateRevision",
    "dateReProgram",
    "observation",
    "idFileAttachment",
    "nameFile",
    "userName",
    "description",
    "state"
  ]
});

var storeAttendRequestReprogram = Ext.create("Ext.data.Store", {
  model: "ModelAttendRequestReprogram",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    extraParams: {},
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define(
  "DukeSource.view.fulfillment.window.ViewWindowPanelAttendRequestDocument",
  {
    extend: "Ext.window.Window",
    alias: "widget.ViewWindowPanelAttendRequestDocument",
    height: 400,
    width: 750,
    layout: {
      type: "fit"
    },
    border: true,
    title: "Solicitudes de reprogramaci&oacute;n",
    titleAlign: "center",
    tbar: [
      "-",
      {
        text: "Atender",
        iconCls: "add",
        action: "attendRequestDocument"
      }
    ],
    initComponent: function() {
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "gridpanel",
            border: false,
            store: storeAttendRequestReprogram,
            columns: [
              {
                dataIndex: "stateQualification",
                width: 90,
                text: "Estado",
                align: "center",
                renderer: function(val, record, metadata) {
                  if (val) {
                    return '<div style="color:#74CC70;"><b>Atendido</b></div>';
                  } else {
                    return '<div style="color:crimson;"><b>Pendiente</b></div>';
                  }
                }
              },
              {
                dataIndex: "dateReProgram",
                width: 150,
                text: "Nueva fecha l&iacute;mite",
                align: "center"
              },
              {
                dataIndex: "descriptionStateQualification",
                width: 120,
                text: "Atención",
                align: "center"
              },
              {
                dataIndex: "description",
                width: 300,
                text: "Descripci&oacute;n",
                align: "center"
              },
              {
                dataIndex: "observation",
                width: 200,
                text: "Observaci&oacute;n",
                align: "center"
              },
              {
                dataIndex: "dateRevision",
                width: 100,
                text: "Fecha atenci&oacute;n",
                align: "center"
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
              displayInfo: true,
              displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
              store: storeAttendRequestReprogram,
              doRefresh: function() {
                var me = this,
                  current = me.store.currentPage;
                var grid = me.up("grid");
                if (me.fireEvent("beforechange", me, current) !== false) {
                  me.store.loadPage(current);
                }
              },
              items: [
                {
                  xtype:"UpperCaseTrigger",
                  width: 120,
                  action: "searchGridMonitoringDocument"
                }
              ]
            }
          }
        ],
        buttons: [
          {
            text: "Salir",
            scope: this,
            scale: "medium",
            handler: this.close,
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      });
      me.callParent(arguments);
    }
  }
);
