Ext.define('DukeSource.view.fulfillment.window.ViewWindowHistoricalDocument', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowHistoricalDocument',
    layout: {
        type: 'fit'
    },
    title: 'HISTORICAL DEL PLAN DE ACCION',
    border: false,
    width: 800,
    height: 600,

    titleAlign: 'center',
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'ViewGridHistoricalDocument'
                }
            ]
        });

        me.callParent(arguments);
    }

});

