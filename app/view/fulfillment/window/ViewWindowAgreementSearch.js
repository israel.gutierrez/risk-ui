Ext.define("ModelAgreement", {
  extend: "Ext.data.Model",
  fields: [
    "codeAgreement",
    "codeCommittee",
    "dateAgreement",
    "description",
    "idAgreement"
  ]
});

var storeAgreement = Ext.create("Ext.data.Store", {
  model: "ModelAgreement",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.fulfillment.window.ViewWindowAgreementSearch", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowAgreementSearch",
  layout: {
    align: "stretch",
    type: "vbox"
  },
  anchorSize: 100,
  title: "BUSCAR ACUERDO",
  titleAlign: "center",
  width: 700,
  height: 500,
  tbar: [
    {
      xtype: "button",
      text: "NUEVO",
      iconCls: "new",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      handler: function() {
        var win = Ext.create(
          "DukeSource.view.fulfillment.window.ViewWindowAgreement",
          {
            originAgreement: "searchAgreement",
            modal: true
          }
        ).show();
        win.down("#codeAgreement").focus(false, 200);
      }
    }
  ],
  initComponent: function() {
    var me = this;
    var category = this.category;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          height: 45,
          padding: 2,
          bodyPadding: 5,
          items: [
            {
              xtype: "container",
              anchor: "100%",
              height: 26,
              layout: {
                type: "hbox"
              },
              items: [
                {
                  xtype: "combobox",
                  value: "2",
                  labelWidth: 40,
                  width: 190,
                  fieldLabel: "TIPO",
                  store: [
                    ["1", "CODIGO"],
                    ["2", "ACUERDO"]
                  ]
                },
                {
                  xtype: "UpperCaseTextField",
                  flex: 2,
                  listeners: {
                    specialkey: function(field, e) {
                      var property = "";
                      if (
                        Ext.ComponentQuery.query(
                          "ViewWindowAgreementSearch combobox"
                        )[0].getValue() == "1"
                      ) {
                        property = "codeAgreement";
                      } else {
                        property = "description";
                      }
                      if (e.getKey() === e.ENTER) {
                        var grid = me.down("grid");
                        grid.store.getProxy().extraParams = {
                          propertyFind: property,
                          valueFind: field.getValue(),
                          propertyOrder: "idAgreement"
                        };
                        grid.store.getProxy().url =
                          "http://localhost:9000/giro/findAgreement.htm";
                        grid.down("pagingtoolbar").moveFirst();
                      }
                    },
                    afterrender: function(f) {
                      f.focus(false, 100);
                    }
                  }
                }
              ]
            }
          ]
        },
        {
          xtype: "gridpanel",
          store: storeAgreement,
          flex: 3,
          padding: 2,
          titleAlign: "center",
          columns: [
            {
              xtype: "rownumberer",
              width: 25,
              sortable: false
            },
            {
              dataIndex: "codeAgreement",
              width: 200,
              text: "CODIGO"
            },
            {
              dataIndex: "description",
              flex: 1,
              text: "ACUERDO"
            }
          ],
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: storeAgreement,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          },
          listeners: {
            render: function() {
              var me = this;
              me.store.getProxy().extraParams = {
                propertyFind: "description",
                valueFind: "",
                propertyOrder: "idAgreement"
              };
              me.store.getProxy().url =
                "http://localhost:9000/giro/findAgreement.htm";
              me.down("pagingtoolbar").moveFirst();
            }
          }
        }
      ],
      buttonAlign: "center",
      buttons: [
        {
          text: "SALIR",
          scale: "medium",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ]
    });

    me.callParent(arguments);
  }
});
