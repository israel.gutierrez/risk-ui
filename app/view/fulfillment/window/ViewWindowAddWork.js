Ext.define('DukeSource.view.fulfillment.window.ViewWindowAddWork', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowAddWork',
    width: 650,
    layout: {
        type: 'fit'
    },
    border: false,
    title: 'INGRESO TAREA',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'idScheduleDocument',
                            value: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'container',
                            layout: 'hbox',
                            height: 26,
                            items: [
                                {
                                    xtype: 'datefield',
                                    name: 'dateInitial',
                                    itemId: 'dateInitial',
                                    format: 'd/m/Y',
                                    flex: 1,
                                    value: new Date(),
                                    allowBlank: false,
                                    msgTarget: 'side',
                                    fieldCls: 'obligatoryTextField',
                                    blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                    fieldLabel: 'FECHA INICIO',
                                    listeners: {
                                        specialkey: function (f, e) {
                                           DukeSource.global.DirtyView.focusEventEnterObligatory(f, e, me.down('#dateExpire'));
                                        }
                                    }
                                },
                                {
                                    xtype: 'datefield',
                                    name: 'dateExpire',
                                    itemId: 'dateExpire',
                                    format: 'd/m/Y',
                                    flex: 1,
                                    padding: '0 0 0 5',
                                    allowBlank: false,
                                    msgTarget: 'side',
                                    fieldCls: 'obligatoryTextField',
                                    blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                    fieldLabel: 'FECHA FIN',
                                    listeners: {
                                        specialkey: function (f, e) {
                                           DukeSource.global.DirtyView.focusEventEnterObligatory(f, e, me.down('#taskWeight'));
                                        },
                                        expand: function (f, e) {
                                            f.setMinValue(me.down('datefield[name=dateInitial]').getRawValue());
                                        }
                                    }
                                },
                                {
                                    xtype: 'NumberDecimalNumberObligatory',
                                    maxValue: 100,
                                    padding: '0 0 0 5',
                                    flex: 1,
                                    fieldLabel: 'PESO(%)',
                                    name: 'taskWeight',
                                    itemId: 'taskWeight',
                                    listeners: {
                                        specialkey: function (f, e) {
                                           DukeSource.global.DirtyView.focusEventEnterObligatory(f, e, me.down('#descriptionTask'));
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            height: 50,
                            anchor: '100%',
                            fieldCls: 'obligatoryTextField',
                            blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            allowBlank: false,
                            name: 'descriptionTask',
                            itemId: 'descriptionTask',
                            maxLength: 600,
                            fieldLabel: 'DESCRIPCION',
                            listeners: {
                                specialkey: function (f, e) {
                                   DukeSource.global.DirtyView.focusEventEnterObligatory(f, e, me.down('#searchUser'));
                                }
                            }
                        },
                        {
                            xtype: 'container',
                            height: 26,
                            layout: {
                                type: 'hbox'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    name: 'collaborator',
                                    itemId: 'collaborator',
                                    hidden: true
                                },
                                {
                                    xtype: 'UpperCaseTextFieldReadOnly',
                                    fieldLabel: 'RESPONSABLE',
                                    allowBlank: false,
                                    msgTarget: 'side',
                                    blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                    flex: 1,
                                    name: 'fullNameCollaborator',
                                    itemId: 'fullNameCollaborator'
                                },
                                {
                                    xtype: 'button',
                                    width: 25,
                                    name: 'searchUser',
                                    itemId: 'searchUser',
                                    iconCls: 'search',
                                    handler: function (button) {
                                        var windowMain = button.up('window');
                                        var windows = Ext.create('DukeSource.view.risk.util.search.SearchUser', {
                                            modal: true
                                        }).show();
                                        windows.down('UpperCaseTextField').focus(false, 100);
                                        windows.down('grid').on('itemdblclick', function (view, row) {
                                            windowMain.down('#collaborator').setValue(row.get('userName'));
                                            windowMain.down('#fullNameCollaborator').setValue(row.get('fullName'));
                                            windowMain.down('#emailCollaborator').setValue(row.get('email') + ';');
                                            windows.close();
                                        });
                                    },
                                    listeners: {
                                        specialkey: function (f, e) {
                                           DukeSource.global.DirtyView.focusEventEnterObligatory(f, e, me.down('#sendEmail'));
                                        }
                                    }
                                },
                                {
                                    xtype: 'fileuploadfield',
                                    anchor: '100%',
                                    flex: 1,
                                    padding: '0 0 0 5',
                                    fieldLabel: 'ARCHIVO',
                                    buttonConfig: {
                                        iconCls: 'tesla even-attachment'
                                    },
                                    buttonText: ""
                                }

                            ]
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            allowBlank: true,
                            hidden: true,
                            height: 50,
                            name: 'observationTask',
                            maxLength: 600,
                            fieldLabel: 'OBSERVACION',
                            listeners: {
                                specialkey: function (f, e) {
                                   DukeSource.global.DirtyView.focusEventEnterObligatory(f, e, me.down('button[text=GUARDAR]'));
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            name: 'indicatorAttachment',
                            value: 'N',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'idDocument',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'stateDocument',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'percentageCollaborator',
                            itemId: 'percentageCollaborator',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'percentage',
                            itemId: 'percentage',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            flex: 1,
                            name: 'emailCollaborator',
                            itemId: 'emailCollaborator',
                            fieldLabel: 'ENVIAR CORREO'
                        }
                    ]
                }
            ]

        });

        me.callParent(arguments);
    }

});
