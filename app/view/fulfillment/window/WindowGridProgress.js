Ext.define('ModelGridProgress', {
    extend: 'Ext.data.Model',
    fields: [
        'checked',
        'dateRegister',
        'description',
        'emailManager',
        'fullName',
        'idActionPlan',
        'idAdvanceActivity',
        'indicatorAttachment',
        'leaf',
        'percentage',
        'username'
    ]
});

var StoreGridProgress = Ext.create('Ext.data.Store', {
    model: 'ModelGridProgress',
    pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'http://localhost:9000/giro/loadGridDefault.htm',
        reader: {
            type: 'json',
            root: 'data',
            successProperty: 'success',
            totalProperty: 'totalCount'
        }
    }
});

var createTooltip = function (idElementToShow, text) {
    Ext.create('Ext.tip.ToolTip', {
        target: idElementToShow,
        html: text,
        showDelay: 0,
        hideDelay: 0
    });
};

function settingStyle(val) {
    if (0 <= val && val <= 33.34) {
        return '#f34b4b';
    } else if (33.34 < val && val <= 66.6) {
        return '#ffab23';
    } else {
        return '#3ea55a';
    }
}

Ext.define('DukeSource.view.fulfillment.window.WindowGridProgress', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.button.Button',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date',
        'Ext.grid.View',
        'Ext.toolbar.Paging'
    ],

    autoShow: true,
    width: 800,
    layout: 'fit',
    id: 'WindowGridProgress',

    initComponent: function () {
        var me = this;
        var recordActionPlan = me.recordActionPlan;
        var isOwner = recordActionPlan.get('userReceptor') === userName;

        Ext.applyIf(me, {
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    border: false,
                    layout: {
                        type: 'hbox',
                        pack: 'center'
                    },
                    items: [
                        {
                            xtype: 'button',
                            cls: 'x-btn-default-medium bo-gray',
                            padding: '2 8',
                            iconCls: 'logout',
                            text: 'Salir',
                            scale: 'medium',
                            handler: function () {
                                me.close();
                            }
                        }
                    ]
                }
            ],
            items: [
                {
                    xtype: 'panel',
                    border: false,
                    id: 'gridProgress',
                    layout: 'fit',
                    dockedItems: [
                        {
                            xtype: 'container',
                            dock: 'top',
                            cls: 'main-bg p-1rem',
                            items: [
                                {
                                    xtype: 'component',
                                    autoEl: {
                                        tag: 'h2',
                                        html: 'Plan de acción'
                                    },
                                    cls: 'm-0'
                                },
                                {
                                    xtype: 'component',
                                    id: 'progressDescription',
                                    autoEl: {
                                        tag: 'p',
                                        html: recordActionPlan.get('description')
                                    },
                                    cls: 'p-05 m-0'
                                },
                                {
                                    xtype: 'container',
                                    id: 'progressBtnAddAndEdit',
                                    items: [
                                        {
                                            xtype: 'button',
                                            iconCls: 'add',
                                            scale: 'medium',
                                            text: 'Agregar',
                                            handler: function () {
                                                var winAddProgressOrTask = Ext.create('DukeSource.view.fulfillment.window.WindowTabRegisterTask', {
                                                    modal: true,
                                                    title: me.title,
                                                    activeTab: 1,
                                                    closable: false,
                                                    recordActionPlan: me.recordActionPlan,
                                                    buttonBack: {
                                                        xtype: 'button',
                                                        cls: 'm-1',
                                                        iconCls: 'arrow_left_16',
                                                        handler: function () {
                                                            winAddProgressOrTask.close();
                                                            me.show();
                                                        }
                                                    },
                                                    alertMessage: {
                                                        xtype: 'component',
                                                        autoEl: {
                                                            tag: 'b',
                                                            html: 'Después de crear la tarea se moverán todos los avances dentro de esta tarea.'
                                                        },
                                                        cls: 'c-danger p-0-1'
                                                    },
                                                    buttons: [
                                                        {
                                                            text: 'Guardar',
                                                            iconCls: 'save',
                                                            scale: 'medium',
                                                            handler: function (btn) {
                                                                if (winAddProgressOrTask.down('tabpanel').getActiveTab().title === winAddProgressOrTask.down('panel[title="Tareas"]').title) {
                                                                    winAddProgressOrTask.down('FormTask').postTaskForm(function () {
                                                                        var winGridTask = Ext.create('DukeSource.view.fulfillment.window.WindowTabGridTask', {
                                                                            modal: true,
                                                                            recordActionPlan: me.recordActionPlan,
                                                                            title: me.title
                                                                        });
                                                                        var gridTask = btn.up('window').down('gridpanel');
                                                                        gridTask.store.getProxy().url = 'http://localhost:9000/giro/showTaskByActionPlan.htm';
                                                                        gridTask.store.getProxy().extraParams = {
                                                                            idActionPlan: me.recordActionPlan.get('idDocument')
                                                                        };
                                                                        gridTask.down('pagingtoolbar').doRefresh();
                                                                        winAddProgressOrTask.close();
                                                                        winGridTask.show();

                                                                    });
                                                                } else if (winAddProgressOrTask.down('tabpanel').getActiveTab().title === winAddProgressOrTask.down('panel[title="Avances"]').title) {
                                                                    winAddProgressOrTask.down('FormProgress').postProgressForm(function () {
                                                                        var gridProgress = me.down('gridpanel');
                                                                        gridProgress.store.getProxy().url = 'http://localhost:9000/giro/showListAdvanceActivityTask.htm';
                                                                        gridProgress.store.getProxy().extraParams = {
                                                                            idActionPlan: me.recordActionPlan.get('idDocument')
                                                                        };
                                                                        gridProgress.down('pagingtoolbar').doRefresh();
                                                                        me.show();
                                                                        winAddProgressOrTask.close();
                                                                    });
                                                                }
                                                            }
                                                        },
                                                        {
                                                            text: 'Cancelar',
                                                            iconCls: 'cancel',
                                                            scale: 'medium',
                                                            handler: function () {
                                                                me.returnPrevWindow(winAddProgressOrTask, me, 'btnModifyProgress');
                                                            }
                                                        }
                                                    ],
                                                    listeners: {
                                                        render: function () {
                                                            winAddProgressOrTask.down('panel[title="Tareas"]').tab.hide();
                                                        }
                                                    }
                                                });

                                                winAddProgressOrTask.down('FormTask').validateDates(me.recordActionPlan.get('dateInitPlan'), me.recordActionPlan.get('dateEnd'));
                                                winAddProgressOrTask.down('FormProgress').validateDates(me.recordActionPlan.get('dateInitPlan'));
                                                me.hide();
                                                winAddProgressOrTask.show();
                                            }

                                        },
                                        {
                                            xtype: 'button',
                                            id: 'btnModifyProgress',
                                            iconCls: 'modify',
                                            cls: 'm-lr-05rem',
                                            text: 'Editar',
                                            scale: 'medium',
                                            disabled: true,
                                            listeners: {
                                                click: function (btn) {
                                                    btn.up('window').formEditProgress(me.recordActionPlan, me);
                                                },
                                                render: function () {
                                                    createTooltip('btnModifyProgress', 'Selecciona el primer avance para editarlo.')
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'button',
                                            id: 'btnDeleteProgress',
                                            iconCls: 'delete',
                                            text: 'Eliminar',
                                            scale: 'medium',
                                            disabled: true,
                                            listeners: {
                                                click: function (btn) {
                                                    var grid = btn.up('window').down('#gridPanelProgress2');
                                                    var row = grid.getSelectionModel().getSelection()[0];

                                                    Ext.MessageBox.show({
                                                        title:DukeSource.global.GiroMessages.TITLE_WARNING,
                                                        msg: 'Estas seguro que desea eliminar este avance?',
                                                        icon: Ext.Msg.WARNING,
                                                        buttonText: {
                                                            yes: "Si", no: "No"
                                                        },
                                                        buttons: Ext.MessageBox.YESNO,
                                                        fn: function (btn) {
                                                            if (btn === 'yes') {
                                                                DukeSource.lib.Ajax.request({
                                                                    method: 'POST',
                                                                    url: 'http://localhost:9000/giro/deleteAdvanceActivity.htm',
                                                                    params: {
                                                                        jsonData: Ext.JSON.encode(row.raw)
                                                                    },
                                                                    success: function (response) {
                                                                        response = Ext.decode(response.responseText);
                                                                        if (response.success) {
                                                                           DukeSource.global.DirtyView.messageNormal(response.message);
                                                                            grid.getStore().load();
                                                                        } else {
                                                                           DukeSource.global.DirtyView.messageWarning(response.message);
                                                                        }
                                                                    },
                                                                    failure: function () {
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });
                                                },
                                                render: function () {
                                                    createTooltip('btnDeleteProgress', 'Selecciona el primer avance para eliminarlo.')
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'button',
                                            id: 'btnAttachmentAdvance',
                                            cls: 'm-lr-05rem',
                                            iconCls: 'attach',
                                            text: 'Adjuntos',
                                            scale: 'medium',
                                            hidden: true,
                                            listeners: {
                                                click: function (btn) {
                                                    var grid = btn.up('window').down('#gridPanelProgress2');
                                                    var row = grid.getSelectionModel().getSelection()[0];

                                                    var k = Ext.create('DukeSource.view.fulfillment.window.ViewWindowViewAttachDocument', {
                                                        modal: true,
                                                        row: row,
                                                        saveFile: 'http://localhost:9000/giro/saveFileAttachAdvance.htm',
                                                        deleteFile: 'http://localhost:9000/giro/deleteFileAttachAdvance.htm',
                                                        src: 'http://localhost:9000/giro/downloadFileAttachAdvance.htm',
                                                        params: ["idFileAttachment", "idAdvanceActivity", "nameFile"]
                                                    });

                                                    var gridFileAttach = k.down('grid');

                                                    gridFileAttach.store.getProxy().extraParams = {
                                                        idAdvanceActivity: row.get('idAdvanceActivity')
                                                    };
                                                    gridFileAttach.store.getProxy().url = 'http://localhost:9000/giro/showFilesAttachAdvance.htm';
                                                    gridFileAttach.down('pagingtoolbar').moveFirst();

                                                    k.idAdvanceActivity = row.get('idAdvanceActivity');
                                                    k.show();
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    items: [
                        {
                            xtype: 'container',
                            layout: 'fit',
                            items: [
                                {
                                    xtype: 'gridpanel',
                                    itemId: 'gridPanelProgress2',
                                    title: 'Avances',
                                    store: StoreGridProgress,
                                    border: false,
                                    layout: 'fit',
                                    height: 400,
                                    columns: [
                                        {
                                            xtype: 'numbercolumn',
                                            sortable: false,
                                            dataIndex: 'percentage',
                                            text: 'Nivel de avance',
                                            format: '.00',
                                            locked: true,
                                            renderer: function (value, metaData, record) {
                                                var style = settingStyle(record.get('percentage'));
                                                var icon;
                                                if (record.get('indicatorAttachment') === 'S') {
                                                    icon = '<i class="tesla even-attachment"></i>'
                                                } else {
                                                    icon = ''
                                                }
                                                return '<div style="background-color: ' + style + '; padding: 8px; width: ' + value + 'px">' + icon + '' + value + '%</div>';
                                            }
                                        },
                                        {
                                            text: '',
                                            hidden: true,
                                            align: 'center',
                                            locked: true,
                                            width: 35,
                                            renderer: function (value, meta, record) {
                                                var id = Ext.id();
                                                Ext.defer(function () {
                                                    new Ext.container.Container({
                                                        layout: 'hbox',
                                                        idRowContainer: id,
                                                        itemId: id,
                                                        id: id,
                                                        hidden: true,
                                                        items: [
                                                            {
                                                                xtype: 'button',
                                                                iconCls: 'modify',
                                                                handler: function () {
                                                                    me.formEditProgress(me.recordActionPlan, me);
                                                                }
                                                            }
                                                        ]
                                                    }).render(document.body, id);
                                                }, 50);
                                                record.idRowContainer = id;
                                                return Ext.String.format('<div id="{0}"></div>', id);
                                            }
                                        },
                                        {
                                            xtype: 'gridcolumn',
                                            sortable: false,
                                            header: 'Descripción',
                                            dataIndex: 'description',
                                            headerAlign: 'center',
                                            width: 500
                                        },
                                        {
                                            header: 'Fecha',
                                            sortable: false,
                                            dataIndex: 'dateRegister',
                                            type: 'date',
                                            dateFormat: 'd/m/Y',
                                            align: 'center',
                                            width: 100
                                        },
                                        {
                                            xtype: 'gridcolumn',
                                            sortable: false,
                                            dataIndex: 'fullName',
                                            text: 'Usuario',
                                            align: 'center',
                                            width: 150
                                        }
                                    ],
                                    dockedItems: [
                                        {
                                            xtype: 'pagingtoolbar',
                                            store: StoreGridProgress,
                                            dock: 'bottom',
                                            width: 360,
                                            afterPageText: 'de {0}',
                                            beforePageText: 'Página',
                                            displayInfo: true,
                                            border: false,
                                            pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
                                            displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                                            emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                                        }
                                    ],
                                    listeners: {
                                        select: function (thisGrid, record) {
                                            if ((record.index === 0 && isOwner) ||DukeSource.global.GiroConstants.ANALYST === category) {
                                                me.down('button[id="btnModifyProgress"]').enable();
                                                me.down('button[id="btnDeleteProgress"]').enable();
                                            } else {
                                                me.down('button[id="btnModifyProgress"]').disable();
                                                me.down('button[id="btnDeleteProgress"]').disable();
                                            }

                                            me.down('#btnAttachmentAdvance').setVisible(true);
                                        }
                                    }
                                }
                            ]
                        }
                    ],
                    listeners: {
                        afterrender: function () {
                            if (category ===DukeSource.global.GiroConstants.GESTOR) {
                                me.down('#progressBtnAddAndEdit').setDisabled(!isOwner);
                            }
                        }
                    }
                }
            ]
        });
        me.callParent(arguments);
    },

    returnPrevWindow: function (windowToClose, windowToShow, btnToDisable) {
        windowToClose.close();
        windowToShow.show();
        Ext.getCmp(btnToDisable).disable();
    },

    formEditProgress: function (recordActionPlan, me) {
        var win = Ext.create('DukeSource.view.fulfillment.window.WindowFormAddOrEditProgress', {
            modal: true,
            recordActionPlan: recordActionPlan,
            title: me.title,
            buttonBack: {
                xtype: 'button',
                cls: 'm-1',
                iconCls: 'arrow_left_16',
                handler: function () {
                    me.returnPrevWindow(win, me, 'btnModifyProgress');
                }
            },
            buttons: [
                {
                    text: 'Guardar',
                    iconCls: 'save',
                    scale: 'medium',
                    handler: function () {
                        win.down('FormProgress').postProgressForm(function () {
                            win.close();
                            me.show();
                            var gridProgress = me.down('gridpanel');
                            gridProgress.store.getProxy().url = 'http://localhost:9000/giro/showListAdvanceActivityTask.htm';
                            gridProgress.store.getProxy().extraParams = {
                                idActionPlan: me.recordActionPlan.get('idDocument')
                            };
                            gridProgress.down('pagingtoolbar').doRefresh();
                        });
                    }
                },
                {
                    text: 'Cancelar',
                    iconCls: 'cancel',
                    scale: 'medium',
                    handler: function () {
                        me.returnPrevWindow(win, me, 'btnModifyProgress');
                    }
                }
            ],
            buttonAlign: 'center'
        });
        var grid = me.down('grid');
        var rowSelected = grid.getSelectionModel().getSelection()[0];
        win.down('FormProgress').validateDates(recordActionPlan.get('dateInitPlan'));
        win.show();
        me.hide();
        win.down('form').getForm().setValues(rowSelected.data);
    }
});