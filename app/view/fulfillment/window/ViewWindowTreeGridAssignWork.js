Ext.define("ModelWindowTreeGridAssignWork", {
  extend: "Ext.data.Model",
  fields: [
    "idDocument",
    "idDetailDocument",
    "idScheduleDocument",
    "dateInitial",
    "dateExpire",
    "percentage",
    "percentageCollaborator",
    "taskWeight",
    "descriptionTask",
    "observationTask",
    "user",
    "nameUser",
    "emailCollaborator",
    "detailDocumentObservation",
    "detailDocumentPercentage",
    "colorByTime",
    "userIssuing",
    "userEndAssignment",
    "userLastAssignment",
    "fullNameCollaborator",
    "descriptionTypeDocument",
    "typeDocument",
    "descriptionActionPlan",
    "idActionPlan",
    "codeActionPlan",
    "nameStateDocument",
    "stateDocument",
    "descriptionPriority",
    "priority",
    "category",
    "dateReception",
    "dateEnd",
    "dateAssign",
    "idRisk",
    "jobPlace",
    "descriptionJobPlace",
    "workArea",
    "state",
    "sendEmail",
    "fileAttachment",
    "nameFile",
    "nameUserInsert",
    "indicatorAttachment"
  ]
});

Ext.define("ModelGridAdvanceActivity", {
  extend: "Ext.data.Model",
  fields: [
    "idDocument",
    "idDetailDocument",
    "idScheduleDocument",
    "idAdvanceActivity",
    "nameDetailDocument",
    "nameScheduleDocument",
    "nameDocument",
    "user",
    "nameUser",
    "descriptionTask",
    "observationTask",
    "percentage",
    "state",
    "sendEmail",
    "emailManager",
    "dateInitial",
    "stateRevision",
    "indicatorAttachment"
  ]
});

var StoreDocumentSchedule = Ext.create("Ext.data.Store", {
  model: "ModelWindowTreeGridAssignWork",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

var StoreGridAdvanceActivity = Ext.create("Ext.data.Store", {
  model: "ModelGridAdvanceActivity",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.fulfillment.window.ViewWindowTreeGridAssignWork", {
  extend: "Ext.window.Window",
  width: 1024,
  height: 660,
  maximized: true,
  alias: "widget.ViewWindowTreeGridAssignWork",
  border: false,
  title: "GESTI&Oacute;N DE TAREAS",
  layout: {
    type: "vbox",
    align: "stretch"
  },
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "gridpanel",
          title: "TAREAS PENDIENTES",
          itemId: "gridTask",
          name: "gridTask",
          flex: 1,
          titleAlign: "center",
          cls: "x-grid-taskBar",
          store: StoreDocumentSchedule,
          plugins: [Ext.create("Ext.grid.plugin.CellEditing")],
          tbar: [
            {
              xtype: "button",
              iconCls: "attachFile",
              text: "ARCHIVOS ADJUNTOS",
              action: "attachFileTaskManager",
              itemId: "attachFile"
            }
          ],
          columns: [
            { xtype: "rownumberer", width: 25, sortable: false },
            {
              header: "AD",
              align: "center",
              width: 25,
              sortable: true,
              dataIndex: "indicatorAttachment",
              renderer: function(value, metaData, record) {
                if (record.get("indicatorAttachment") == "S") {
                  return '<div><i class="tesla even-attachment"></i></div>';
                } else {
                  return "";
                }
              }
            },
            {
              header: "% AVANCE",
              width: 100,
              dataIndex: "percentageCollaborator",
              align: "center",
              renderer: function(v, m, r) {
                var tmpValue = v / 100;
                var tmpText = v + "%";
                var progressRenderer = (function(pValue, pText) {
                  var b = new Ext.ProgressBar();
                  if (tmpValue <= 0.3334) {
                    b.baseCls = "x-taskBar";
                  } else if (tmpValue <= 0.6667) {
                    b.baseCls = "x-taskBar-medium";
                  } else {
                    b.baseCls = "x-taskBar-high";
                  }
                  return function(pValue, pText) {
                    b.updateProgress(pValue, pText, true);
                    return Ext.DomHelper.markup(b.getRenderTree());
                  };
                })(tmpValue, tmpText);
                return progressRenderer(tmpValue, tmpText);
              }
            },
            {
              header: "PESO<br>TAREA",
              width: 60,
              dataIndex: "taskWeight",
              align: "center"
            },
            {
              header: "TAREAS",
              align: "left",
              width: 500,
              sortable: true,
              dataIndex: "descriptionTask"
            },
            {
              header: "USUARIO",
              align: "left",
              width: 250,
              sortable: true,
              dataIndex: "fullNameCollaborator"
            },
            {
              header: "FECHA LIMITE",
              width: 100,
              format: "d/m/Y H:i:s",
              dataIndex: "dateExpire",
              align: "center"
            }
          ],
          bbar: {
            xtype: "pagingtoolbar",
            pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: StoreDocumentSchedule,
            displayInfo: true,
            displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          },
          padding: "2 2 2 2"
        },
        {
          xtype: "grid",
          name: "gridAdvanceActivity",
          itemId: "gridAdvanceActivity",
          title: "NIVEL DE AVANCE DE LAS TAREAS",
          flex: 1,
          store: StoreGridAdvanceActivity,
          titleAlign: "center",
          bbar: {
            xtype: "pagingtoolbar",
            itemId: "ptAdvanceActivity",
            pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: StoreGridAdvanceActivity,
            displayInfo: true,
            displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          },
          listeners: {
            render: function() {
              var me = this;
              me.store.getProxy().url =
                "http://localhost:9000/giro/loadGridDefault.htm";
              me.down("pagingtoolbar").moveFirst();
            }
          },
          tbar: [
            {
              xtype: "button",
              iconCls: "modify",
              text: "VERIFICAR AVANCE",
              action: "modifyAdvanceActivityManager"
            },
            {
              xtype: "button",
              iconCls: "attachFile",
              text: "ARCHIVOS ADJUNTOS",
              action: "attachFileAdvanceActivityManager"
            }
          ],
          columns: [
            { xtype: "rownumberer", width: 25, sortable: false },
            {
              header: "AD",
              align: "center",
              width: 25,
              sortable: true,
              dataIndex: "indicatorAttachment",
              renderer: function(value, metaData, record) {
                if (record.get("indicatorAttachment") == "S") {
                  return '<div><i class="tesla even-attachment"></i></div>';
                } else {
                  return "";
                }
              }
            },
            {
              header: "NIVEL AVANCE",
              align: "center",
              width: 100,
              sortable: true,
              dataIndex: "percentage",
              renderer: function(v, m, r) {
                var tmpValue = v / 100;
                var tmpText = v + "%";
                var progressRenderer = (function(pValue, pText) {
                  var b = new Ext.ProgressBar();
                  if (tmpValue <= 0.3334) {
                    b.baseCls = "x-taskBar";
                  } else if (tmpValue <= 0.6667) {
                    b.baseCls = "x-taskBar-medium";
                  } else {
                    b.baseCls = "x-taskBar-high";
                  }
                  return function(pValue, pText) {
                    b.updateProgress(pValue, pText, true);
                    return Ext.DomHelper.markup(b.getRenderTree());
                  };
                })(tmpValue, tmpText);
                return progressRenderer(tmpValue, tmpText);
              }
            },
            {
              header: "AVANCE",
              align: "left",
              width: 500,
              sortable: true,
              dataIndex: "descriptionTask"
            },
            {
              header: "FECHA",
              align: "center",
              width: 150,
              sortable: true,
              dataIndex: "dateInitial"
            },
            {
              header: "USUARIO",
              align: "left",
              width: 250,
              sortable: true,
              dataIndex: "nameUser"
            }
          ]
        }
      ],
      listeners: {
        blur: function() {
          alert("lost focus");
        }
      },
      buttons: [
        {
          text: "SALIR",
          scale: "medium",
          iconCls: "logout",
          handler: function() {
            me.close();
          }
        }
      ],
      buttonAlign: "center"
    });
    me.callParent(arguments);
  }
});

function change(val, record, metaData) {
  if (val == 0) {
    metaData.tdAttr = 'style="border:1px solid #C7CCC0 !important;"';
    return '<div><span style="color:crimson;">' + val + "%</span></div>";
  } else if (1 <= val && val <= 33.34) {
    metaData.tdAttr =
      'style="border:1px solid #C7CCC0 !important;background-color:#FFE7E9;"';
    return '<div><span style="color:crimson;">' + val + "%</span></div>"; //style="background-color:crimson;width:' + val + '%"
  } else if (33.34 < val && val <= 66.6) {
    metaData.tdAttr =
      'style="border:1px solid #C7CCC0 !important;background-color:#F2CE3E;"';
    return (
      '<div><div style="background-color:#FFA700;width:' +
      val +
      '%"><span style="color:white;">' +
      val +
      "%</span></div></div>"
    );
  } else {
    metaData.tdAttr =
      'style="border:1px solid #C7CCC0 !important;background-color:#5BABE1;"';
    return (
      '<div><div style="background-color:#3172D7;width:' +
      val +
      '%"><span style="color:white;">' +
      val +
      "%</span></div></div>"
    );
  }
}
