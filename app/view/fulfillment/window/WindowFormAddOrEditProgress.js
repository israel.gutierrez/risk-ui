Ext.define("DukeSource.view.fulfillment.window.WindowFormAddOrEditProgress", {
  extend: "Ext.window.Window",
  requires: ["DukeSource.view.fulfillment.form.FormProgress"],

  width: 700,
  layout: "fit",
  closable: false,

  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "panel",
          items: [
            me.buttonBack || {
              xtype: "button",
              cls: "m-1",
              iconCls: "arrow_left_16",
              handler: function() {
                me.close();
                Ext.getCmp("WindowTabGridTask").show();
              }
            },
            {
              xtype: "FormProgress",
              recordActionPlan: me.recordActionPlan,
              idTaskActionPlan: me.idTaskActionPlanField,
              idActionPlanField: {
                xtype: "textfield",
                name: "idActionPlan",
                itemId: "idActionPlan",
                value: me.recordActionPlan.get("idDocument"),
                hidden: true
              }
            }
          ]
        }
      ],
      buttons: [
        {
          text: "Guardar",
          iconCls: "save",
          scale: "medium",
          handler: function() {
            me.down("FormProgress").postProgressForm(function() {
              me.close();
              Ext.getCmp("WindowTabGridTask").show();
              var gridProgress = Ext.getCmp("WindowTabGridTask").down(
                'gridpanel[name="gridPanelProgress"]'
              );
              gridProgress.store.getProxy().url =
                "http://localhost:9000/giro/showListAdvanceActivityTask.htm";
              gridProgress.store.getProxy().extraParams = {
                idActionPlan: me.recordActionPlan.get("idDocument"),
                idTaskActionPlan: me.rowSelected.get("idTask")
              };
              gridProgress.down("pagingtoolbar").doRefresh();
              Ext.getCmp("WindowTabGridTask")
                .down('panel[title="Tareas"]')
                .down("grid")
                .down("pagingtoolbar")
                .doRefresh();
            });
          }
        },
        {
          text: "Cancelar",
          iconCls: "cancel",
          scale: "medium",
          handler: function() {
            me.close();
            Ext.getCmp("WindowTabGridTask").show();
          }
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});
