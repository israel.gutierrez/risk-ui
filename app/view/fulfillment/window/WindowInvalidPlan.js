Ext.define("DukeSource.view.fulfillment.window.WindowInvalidPlan", {
  extend: "Ext.window.Window",
  alias: "widget.WindowInvalidPlan",
  height: 250,
  width: 550,
  layout: {
    type: "fit"
  },
  border: false,
  title: "SUSTENTO DE INVALIDACION",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    var idDocument = this.idDocument;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          title: "",
          items: [
            {
              xtype: "textfield",
              name: "idDocument",
              itemId: "idDocument",
              value: idDocument,
              hidden: true
            },
            {
              xtype: "datefield",
              anchor: "100%",
              name: "dateInvalid",
              itemId: "dateInvalid",
              labelWidth: 120,
              format: "d/m/Y",
              readOnly: true,
              editable: false,
              allowBlank: false,
              value: new Date(),
              msgTarget: "side",
              fieldCls: "readOnlyText",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              fieldLabel: "FECHA DE REGISTRO"
            },
            {
              xtype: "UpperCaseTextArea",
              anchor: "100%",
              labelWidth: 120,
              name: "descriptionInvalid",
              itemId: "descriptionInvalid",
              allowBlank: false,
              height: 80,
              msgTarget: "side",
              maxLength: 600,
              fieldCls: "obligatoryTextField",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              fieldLabel: "SUSTENTO",
              listeners: {
                specialkey: function(f, e) {
                  DukeSource.global.DirtyView.focusEventEnterObligatory(
                    f,
                    e,
                    me.down("button[text=GUARDAR]")
                  );
                }
              }
            },
            {
              xtype: "UpperCaseTextArea",
              anchor: "100%",
              labelWidth: 120,
              name: "descriptionInvalid",
              itemId: "descriptionInvalid",
              allowBlank: false,
              height: 80,
              msgTarget: "side",
              maxLength: 600,
              fieldCls: "obligatoryTextField",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              fieldLabel: "SUSTENTO",
              listeners: {
                specialkey: function(f, e) {
                  DukeSource.global.DirtyView.focusEventEnterObligatory(
                    f,
                    e,
                    me.down("button[text=GUARDAR]")
                  );
                }
              }
            },
            {
              xtype: "fileuploadfield",
              anchor: "100%",
              labelWidth: 120,
              fieldLabel: "ADJUNTO",
              buttonConfig: {
                iconCls: "tesla even-attachment"
              },
              buttonText: ""
            }
          ]
        }
      ],
      buttonAlign: "center"
    });
    me.callParent(arguments);
  }
});
