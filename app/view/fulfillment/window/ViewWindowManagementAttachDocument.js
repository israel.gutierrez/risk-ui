Ext.define("ModelFileAttach", {
  extend: "Ext.data.Model",
  fields: [
    "correlative",
    "idDocument",
    "idDetailDocument",
    "idScheduleDocument",
    "idAdvanceActivity",
    "nameUserInsert",
    "nameFile"
  ]
});

var storeFileAttach = Ext.create("Ext.data.Store", {
  model: "ModelFileAttach",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListFileAttachmentsOfActivityAdvance.htm",
    extraParams: {},
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define(
  "DukeSource.view.fulfillment.window.ViewWindowManagementAttachDocument",
  {
    extend: "Ext.window.Window",
    border: false,
    alias: "widget.ViewWindowManagementAttachDocument",
    height: 383,
    width: 589,
    layout: {
      align: "stretch",
      type: "vbox"
    },
    title: "DOCUMENTOS ADJUNTOS",
    initComponent: function() {
      var idTree = this.idTree;
      var me = this;
      Ext.applyIf(me, {
        items: [
          {
            xtype: "form",
            padding: "0 0 2 0",
            bodyPadding: "10",
            items: [
              {
                xtype: "textfield",
                name: "correlative",
                value: "id",
                hidden: true
              },
              {
                xtype: "filefield",
                anchor: "100%",
                allowBlank: false,
                msgTarget: "side",
                fieldCls: "obligatoryTextField",
                blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                itemId: "text",
                name: "text",
                fieldLabel: "DESCRIPCION"
              }
            ],
            bbar: [
              "->",
              "-",
              {
                text: "ADJUNTAR",
                iconCls: "save",
                handler: function() {
                  var form = me.down("form");
                  var grid = me.down("grid");
                  if (form.getForm().isValid()) {
                    form.getForm().submit({
                      url:
                        "http://localhost:9000/giro/saveFileAttachmentsOfActivityAdvance.htm?nameView=ViewPanelDocumentPending" +
                        "&id=" +
                        idTree,
                      waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
                      method: "POST",
                      success: function(form, action) {
                        var valor = Ext.decode(action.response.responseText);
                        if (valor.success) {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_MESSAGE,
                            valor.mensaje,
                            Ext.Msg.INFO
                          );
                          grid.store.getProxy().extraParams = {
                            id: idTree
                          };
                          grid.store.getProxy().url =
                            "http://localhost:9000/giro/showListFileAttachmentsOfActivityAdvance.htm";
                          grid.down("pagingtoolbar").moveFirst();
                        } else {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_ERROR,
                            valor.mensaje,
                            Ext.Msg.ERROR
                          );
                        }
                      },
                      failure: function(form, action) {
                        var valor = Ext.decode(action.response.responseText);
                        if (!valor.success) {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_ERROR,
                            valor.mensaje,
                            Ext.Msg.ERROR
                          );
                        }
                      }
                    });
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
                      Ext.Msg.ERROR
                    );
                  }
                }
              },
              "-"
            ]
          },
          {
            xtype: "gridpanel",
            store: storeFileAttach,
            flex: 1,
            columns: [
              {
                xtype: "rownumberer",
                width: 25,
                sortable: false
              },
              {
                dataIndex: "correlative",
                width: 60,
                text: "CODIGO"
              },
              {
                dataIndex: "nameUserInsert",
                flex: 1,
                text: "NOMBRE"
              },
              {
                dataIndex: "nameFile",
                flex: 1,
                text: "ARCHIVO"
              },
              {
                xtype: "actioncolumn",
                header: "DOCUMENTO",
                align: "center",
                width: 80,
                items: [
                  {
                    icon: "images/page_white_put.png",
                    handler: function(grid, rowIndex) {
                      Ext.core.DomHelper.append(document.body, {
                        tag: "iframe",
                        id: "downloadIframe",
                        frameBorder: 0,
                        width: 0,
                        height: 0,
                        css: "display:none;visibility:hidden;height:0px;",
                        src:
                          "http://localhost:9000/giro/downloadFileAttachmentsOfActivityAdvance.htm?correlative=" +
                          grid.store.getAt(rowIndex).get("correlative") +
                          "&idDocument=" +
                          grid.store.getAt(rowIndex).get("idDocument") +
                          "&idDetailDocument=" +
                          grid.store.getAt(rowIndex).get("idDetailDocument") +
                          "&idScheduleDocument=" +
                          grid.store.getAt(rowIndex).get("idScheduleDocument") +
                          "&idAdvanceActivity=" +
                          grid.store.getAt(rowIndex).get("idAdvanceActivity") +
                          "&nameFile=" +
                          grid.store.getAt(rowIndex).get("nameFile")
                      });
                    }
                  }
                ]
              }
            ],
            bbar: {
              xtype: "pagingtoolbar",
              pageSize: 50,
              store: storeFileAttach,
              items: [
                {
                  xtype:"UpperCaseTrigger",
                  width: 120,
                  action: "searchGridAllAgency"
                }
              ]
            }
          }
        ],
        buttons: [
          {
            text: "SALIR",
            scope: this,
            handler: this.close,
            iconCls: "logout"
          }
        ]
      });

      me.callParent(arguments);
    }
  }
);
