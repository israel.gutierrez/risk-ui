var statesPending = Ext.create('Ext.data.Store', {
    fields: ['id', 'name'],
    data: [
        {
            "id": "PE",
            "name": "PENDIENTE"
        },
        {
            "id": "PR",
            "name": "PROCESO"
        },
        {
            "id": "PZ",
            "name": "RECHAZADO"
        },
        {
            "id": "CU",
            "name": "CULMINADO"
        }
    ]
});

Ext.define('DukeSource.view.fulfillment.window.ViewWindowRegisterMonitoring', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowRegisterMonitoring',
    height: 300,
    width: 550,
    layout: {
        type: 'fit'
    },
    border: false,
    title: 'Registro de monitoreo',
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 5,
                    title: '',
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'id',
                            value: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'idDocument',
                            hidden: true
                        },
                        {
                            xtype: 'NumberDecimalNumberObligatory',
                            maxValue: 100,
                            fieldLabel: '% real del avance',
                            fieldStyle: 'text-align: left;',
                            allowBlank: false,
                            fieldCls: 'obligatoryTextField',
                            blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: 'percentage',
                            labelWidth: 120,
                            listeners: {
                                specialkey: function (f, e) {
                                   DukeSource.global.DirtyView.focusEventEnterObligatory(f, e, me.down('combobox'));
                                }
                            }
                        },
                        {
                            xtype: 'ViewComboForeignKey',
                            fieldLabel: 'Estado',
                            labelWidth: 120,
                            allowBlank: false,
                            fieldCls: 'obligatoryTextField',
                            blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: 'stateDocument',
                            listeners: {
                                specialkey: function (f, e) {
                                   DukeSource.global.DirtyView.focusEventEnterObligatory(f, e,
                                        me.down('UpperCaseTextArea[name=description]'));
                                }
                            }
                        },
                        {
                            xtype: 'datefield',
                            name: 'dateMonitoring',
                            labelWidth: 120,
                            format: 'd/m/Y H:i',
                            readOnly: true,
                            editable: false,
                            allowBlank: false,
                            value: new Date(),
                            msgTarget: 'side',
                            fieldCls: 'readOnlyText',
                            blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            fieldLabel: 'Fecha de registro'
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            labelWidth: 120,
                            name: 'description',
                            allowBlank: false,
                            height: 80,
                            msgTarget: 'side',
                            maxLength: 500,
                            fieldCls: 'obligatoryTextField',
                            blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            fieldLabel: 'Descripción del monitoreo',
                            listeners: {
                                specialkey: function (f, e) {
                                   DukeSource.global.DirtyView.focusEventEnterObligatory(f, e, me.down('button[text=GUARDAR]'));
                                }
                            }
                        }
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
});
