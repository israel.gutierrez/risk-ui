Ext.define("ModelSearchRiskIdentify", {
  extend: "Ext.data.Model",
  fields: [
    "idRisk",
    "descriptionRisk",
    "codeRisk",
    "versionCorrelative",
    "descriptionCorrelative"
  ]
});

var storeSearchRiskIdentify = Ext.create("Ext.data.Store", {
  model: "ModelSearchRiskIdentify",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.fulfillment.window.ViewWindowSearchRiskIdentify", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowSearchRiskIdentify",
  layout: {
    align: "stretch",
    type: "vbox"
  },
  anchorSize: 100,
  title: "RIESGOS IDENTIFICADOS",
  titleAlign: "center",
  width: 900,
  height: 500,
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          height: 45,
          padding: "2 2 2 2",
          bodyPadding: 10,
          items: [
            {
              xtype: "container",
              anchor: "100%",
              height: 26,
              layout: {
                type: "hbox"
              },
              items: [
                {
                  xtype: "combobox",
                  value: "2",
                  labelWidth: 40,
                  width: 190,
                  padding: "0 10 0 0",
                  fieldLabel: "TIPO",
                  store: [
                    ["1", "CODIGO"],
                    ["2", "DESCRIPCION"]
                  ]
                },
                {
                  xtype: "UpperCaseTextField",
                  flex: 2,
                  listeners: {
                    afterrender: function(field) {
                      field.focus(false, 200);
                    },
                    specialkey: function(field, e) {
                      var property = "";
                      if (
                        Ext.ComponentQuery.query(
                          "ViewWindowSearchRiskIdentify combobox"
                        )[0].getValue() == "1"
                      ) {
                        property = "codeRisk";
                      } else {
                        property = "descriptionRisk";
                      }
                      if (e.getKey() === e.ENTER) {
                        var grid = me.down("grid");
                        DukeSource.global.DirtyView.searchPaginationGridToEnter(
                          field,
                          grid,
                          grid.down("pagingtoolbar"),
                          "http://localhost:9000/giro/findMatchRiskToMonitoring.htm",
                          property,
                          "descriptionRisk"
                        );
                      }
                    }
                  }
                }
              ]
            }
          ]
        },
        {
          xtype: "container",
          flex: 3,
          padding: "2 2 2 2",
          layout: {
            align: "stretch",
            type: "hbox"
          },
          items: [
            {
              xtype: "gridpanel",
              padding: "0 1 0 0",
              store: storeSearchRiskIdentify,
              flex: 1,
              titleAlign: "center",
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false
                },
                {
                  dataIndex: "codeRisk",
                  width: 100,
                  align: "center",
                  text: "CODIGO"
                },
                {
                  dataIndex: "descriptionRisk",
                  flex: 5,
                  text: "DESCRIPCION"
                },
                {
                  dataIndex: "descriptionCorrelative",
                  flex: 2,
                  text: "VERSION"
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: storeSearchRiskIdentify,
                displayInfo: true,
                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              },
              listeners: {
                render: function() {
                  var me = this;
                  DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    me,
                    me.down("pagingtoolbar"),
                    "http://localhost:9000/giro/showListRiskActivesToMonitoring.htm",
                    "description",
                    "description"
                  );
                }
              }
            }
          ]
        }
      ]
    });
    me.callParent(arguments);
  }
});
