Ext.define('DukeSource.view.fulfillment.window.WindowRegisterActionPlan', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowRegisterActionPlan',
    requires: [
        'DukeSource.view.risk.parameter.combos.ViewComboIndicatorType'
        , 'DukeSource.view.fulfillment.combos.ViewComboTypeDocument'
        , 'Ext.ux.DateTimeField'

    ],
    width: 820,
    layout: {
        type: 'fit'
    },
    title: 'Ingresar plan de acci&oacute;n',
    titleAlign: 'center',
    border: false,
    initComponent: function () {
        var me = this;
        var rowActionPlan = this.rowActionPlan;
        var isOwner = this.isOwner === undefined ? true : this.isOwner;
        var isInMyModule = this.isInMyModule === undefined ? true : this.isInMyModule;
        var isAvailable = this.isAvailable;

        var isEditable = isAvailable && isOwner;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    fileUpload: true,
                    isUpload: true,
                    method: 'POST',
                    fieldDefaults: {
                        labelCls: 'changeSizeFontToEightPt',
                        fieldCls: 'changeSizeFontToEightPt'
                    },
                    enctype: 'multipart/form-data',
                    bodyPadding: 5,
                    items: [
                        {
                            xtype: 'textfield',
                            itemId: 'stateDocument',
                            name: 'stateDocument',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            itemId: 'idDocument',
                            name: 'idDocument',
                            value: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'dateInitTemp',
                            itemId: 'dateInitTemp',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'dateEndTemp',
                            itemId: 'dateEndTemp',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'typeDocumentTemp',
                            itemId: 'typeDocumentTemp',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            itemId: 'codeCorrelative',
                            name: 'codeCorrelative'
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            itemId: 'checkApprove',
                            name: 'checkApprove'
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            itemId: 'userApprove',
                            name: 'userApprove'
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            itemId: 'commentApprove',
                            name: 'commentApprove'
                        },
                        {
                            xtype: 'datetimefield',
                            format: 'd/m/Y H:i',
                            hidden: true,
                            itemId: 'dateApprove',
                            name: 'dateApprove'
                        },
                        {
                            xtype: 'textfield',
                            name: 'numberRiskAssociated',
                            value: '0',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'codesRisk',
                            value: '',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'indicatorAttachment',
                            value: 'N',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'indicatorManagement',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'percentage',
                            value: 0,
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'percentageMonitoring',
                            value: 0,
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'numberReprogramming',
                            value: 0,
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            hidden: true,
                            name: 'userEmitted'
                        },
                        {
                            xtype: 'fieldset',
                            title: '<b>DATOS DEL PLAN DE ACCION</b>',
                            items: [
                                {
                                    xtype: 'container',
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'container',
                                            layout: 'anchor',
                                            flex: 1,
                                            items: [
                                                {
                                                    xtype: 'combobox',
                                                    anchor: '100%',
                                                    fieldLabel: getName('PLAN_WRAP_CB_OriginPlan'),
                                                    hidden:  hidden('PLAN_WRAP_CB_OriginPlan'),
                                                    editable: false,
                                                    allowBlank: true,
                                                    forceSelection: true,
                                                    name: 'originPlan',
                                                    itemId: 'originPlan',
                                                    queryMode: 'local',
                                                    displayField: 'description',
                                                    valueField: 'value',
                                                    store: {
                                                        fields: ['value', 'description'],
                                                        pageSize: 9999,
                                                        autoLoad: true,
                                                        proxy: {
                                                            actionMethods: {
                                                                create: 'POST',
                                                                read: 'POST',
                                                                update: 'POST'
                                                            },
                                                            type: 'ajax',
                                                            url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                                                            extraParams: {
                                                                propertyOrder: 'description',
                                                                propertyFind: 'identified',
                                                                valueFind: 'ACTION_PLAN_ORIGIN'
                                                            },
                                                            reader: {
                                                                type: 'json',
                                                                root: 'data',
                                                                successProperty: 'success'
                                                            }
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype: 'ViewComboIndicatorType',
                                                    anchor: '100%',
                                                    allowBlank: false,
                                                    name: 'indicatorPlan',
                                                    queryMode: 'remote',
                                                    itemId: 'indicatorPlan',
                                                    fieldLabel: getName('APW_CBX_IndicatorPlan'),
                                                    fieldCls: 'obligatoryTextField'
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    name: 'observation',
                                                    fieldLabel: 'Detalle de fuente',
                                                    hidden: hidden('WAP_TXT_DetailIndicatorPlan')
                                                },
                                                {
                                                    xtype: 'numberfield',
                                                    maxValue: 999999999,
                                                    minValue: 0,
                                                    decimalPrecision: DECIMAL_PRECISION,
                                                    hidden: hidden('WAP_TXT_CostPlan'),
                                                    value: 0.00,
                                                    fieldCls: 'obligatoryTextField',
                                                    fieldLabel: 'Costo estimado',
                                                    name: 'costPlan',
                                                    itemId: 'costPlan',
                                                    anchor: '100%'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            layout: {
                                                type: 'vbox',
                                                align: 'stretch'
                                            },
                                            flex: 1,
                                            padding: '0 10 0 10',
                                            items: [
                                                {
                                                    xtype: 'datefield',
                                                    labelWidth: 110,
                                                    name: 'dateAssign',
                                                    format: 'd/m/Y',
                                                    value: new Date(),
                                                    allowBlank: false,
                                                    msgTarget: 'side',
                                                    fieldCls: 'obligatoryTextField',
                                                    blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                    fieldLabel: getName('WRAP_DF_DateAssign'),
                                                    listeners: {
                                                        specialkey: function (f, e) {
                                                           DukeSource.global.DirtyView.focusEventEnterObligatory(f, e,
                                                                me.down('#dateInitPlan'));

                                                        },
                                                        expand: function (f) {
                                                            if (me.down("#dateInitPlan").getValue() !== undefined) {
                                                                f.setMaxValue(me.down("#dateInitPlan").getValue())
                                                            }
                                                        }

                                                    }
                                                },
                                                {
                                                    xtype: 'datefield',
                                                    labelWidth: 110,
                                                    name: 'dateInitPlan',
                                                    itemId: 'dateInitPlan',
                                                    format: 'd/m/Y',
                                                    allowBlank: false,
                                                    msgTarget: 'side',
                                                    fieldCls: 'obligatoryTextField',
                                                    blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                    fieldLabel: getName('WRAP_DF_DateInitPlan'),
                                                    listeners: {
                                                        expand: function (f) {
                                                            if (me.down("#dateEnd").getValue() !== undefined) {
                                                                f.setMaxValue(me.down("#dateEnd").getValue())
                                                            }
                                                            f.setMinValue(me.down('datefield[name=dateAssign]').getRawValue());
                                                        },
                                                        select: function (cbo) {
                                                            var dateEnd = me.down('#dateEnd').getRawValue();
                                                            var dateEndTemp = me.down('textfield[name=dateEndTemp]').getValue();
                                                            var dateInitTemp = me.down('textfield[name=dateInitTemp]').getValue();
                                                            if (rowActionPlan !== undefined) {
                                                                if (rowActionPlan.get('checkApprove') ===DukeSource.global.GiroConstants.YES) {
                                                                    if (dateInitTemp !== undefined && dateInitTemp !== "") {
                                                                        var dateInit = cbo.getRawValue();
                                                                        if (dateInit !== dateInitTemp) {
                                                                           DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_PLAN_REPROGRAM);
                                                                            if (me.down('ViewComboTypeDocument').getValue() === '3') {
                                                                                me.down('ViewComboTypeDocument').setValue("4");
                                                                            } else {
                                                                                me.down('ViewComboTypeDocument').setValue("2");
                                                                            }
                                                                        } else if (dateEnd === dateEndTemp && dateInit === dateInitTemp) {
                                                                            me.down('ViewComboTypeDocument').setValue(me.down('textfield[name=typeDocumentTemp]').getValue());
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype: 'datefield',
                                                    labelWidth: 110,
                                                    name: 'dateEnd',
                                                    itemId: 'dateEnd',
                                                    format: 'd/m/Y',
                                                    allowBlank: false,
                                                    msgTarget: 'side',
                                                    fieldCls: 'obligatoryTextField',
                                                    blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                    fieldLabel: 'Fecha límite',
                                                    listeners: {
                                                        expand: function (f) {
                                                            f.setMinValue(me.down('#dateInitPlan').getRawValue());
                                                        },
                                                        select: function (cbo) {
                                                            var dateInit = me.down('#dateInitPlan').getRawValue();
                                                            var dateInitTemp = me.down('textfield[name=dateInitTemp]').getValue();
                                                            var dateEndTemp = me.down('textfield[name=dateEndTemp]').getValue();
                                                            if (rowActionPlan !== undefined) {
                                                                if (rowActionPlan.get('checkApprove') ===DukeSource.global.GiroConstants.YES) {
                                                                    if (dateEndTemp !== undefined && dateEndTemp !== "") {
                                                                        var dateEnd = cbo.getRawValue();
                                                                        if (dateEnd !== dateEndTemp) {
                                                                           DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_PLAN_REPROGRAM);
                                                                            if (me.down('ViewComboTypeDocument').getValue() === '3') {
                                                                                me.down('ViewComboTypeDocument').setValue("4");
                                                                            } else {
                                                                                me.down('ViewComboTypeDocument').setValue("2");
                                                                            }
                                                                        } else if (dateEnd === dateEndTemp && dateInit === dateInitTemp) {
                                                                            me.down('ViewComboTypeDocument').setValue(me.down('textfield[name=typeDocumentTemp]').getValue());
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'container',
                                            layout: 'anchor',
                                            flex: 1,
                                            padding: '0 5 0 0',
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    height: 35,
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'stretch'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'UpperCaseTextFieldReadOnly',
                                                            anchor: '100%',
                                                            name: 'codePlan',
                                                            itemId: 'codePlan',
                                                            flex: 1,
                                                            value:DukeSource.global.GiroConstants.DRAFT,
                                                            fieldCls: 'codeIncident',
                                                            fieldLabel: 'Código'
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            text: 'Aprobar',
                                                            flex: 0.5,
                                                            hidden: true,
                                                            margin: '0 0 0 2',
                                                            iconCls: 'evaluate',
                                                            scale: 'medium',
                                                            name: 'btnApprovePlan',
                                                            itemId: 'btnApprovePlan',
                                                            cls: 'my-btn',
                                                            overCls: 'my-over',
                                                            handler: function () {
                                                                var win = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.WindowApproveCorrelative', {
                                                                    modal: true,
                                                                    id: me.down('#idDocument').getValue(),
                                                                    actionExecute: 'approveActionPlan'
                                                                });
                                                                if (me.down('#checkApprove').getValue() ===DukeSource.global.GiroConstants.YES) {
                                                                    win.down('#dateApprove').setValue(me.down('#dateApprove').getValue());
                                                                    win.down('#userApprove').setValue(me.down('#userApprove').getValue());
                                                                    win.down('#commentApprove').setValue(me.down('#commentApprove').getValue());
                                                                    win.down('#saveApprove').setDisabled(true);
                                                                    win.down('#saveApprove').setVisible(false);
                                                                }
                                                                win.show();
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'ViewComboTypeDocument',
                                                    anchor: '100%',
                                                    padding: '5 0 0 0',
                                                    flex: 1,
                                                    name: 'typeDocument',
                                                    itemId: 'typeDocument',
                                                    allowBlank: false,
                                                    readOnly: true,
                                                    editable: false,
                                                    msgTarget: 'side',
                                                    fieldCls: 'readOnlyText',
                                                    fieldStyle: "text-align: center;",
                                                    blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                    fieldLabel: 'Vigencia',
                                                    listeners: {
                                                        afterrender: function (cbo) {
                                                            cbo.getStore().autoLoad = true;
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype: 'container',
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'stretch'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'checkboxfield',
                                                            hidden: true,
                                                            checked: true,
                                                            name: 'approved',
                                                            itemId: 'approved',
                                                            inputValue: 'S',
                                                            uncheckedValue: 'N',
                                                            fieldLabel: 'Proceso',
                                                            readOnly: false,
                                                            listeners: {
                                                                change: function (chk) {
                                                                    if (!chk.getValue()) {
                                                                        chk.setFieldLabel('PENDIENTE');
                                                                    } else {
                                                                        chk.setFieldLabel('PROCESO');
                                                                    }
                                                                },
                                                                afterrender: function () {

                                                                }
                                                            }
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            fieldLabel: 'Reprogramaciones',
                                                            name: 'numberReprogram',
                                                            itemId: 'numberReprogram',
                                                            value: 0,
                                                            flex: 1.2,
                                                            readOnly: true,
                                                            editable: false,
                                                            msgTarget: 'side',
                                                            fieldCls: 'readOnlyText',
                                                            fieldStyle: "text-align: center;"
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'UpperCaseTextArea',
                                    itemId: 'description',
                                    padding: '15 0 0 0',
                                    name: 'description',
                                    anchor: '100%',
                                    height: 75,
                                    maxLength: 2000,
                                    fieldCls: "obligatoryTextField",
                                    fieldLabel: 'Descripción',
                                    editable: false,
                                    allowBlank: false
                                }
                            ]
                        },
                        {
                            xtype: 'container',
                            anchor: '100%',
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: {
                                        type: 'anchor'
                                    },
                                    items: [
                                        {
                                            xtype: 'fieldset',
                                            title: '<b>RESPONSABLE</b>',
                                            border: false,
                                            items: [
                                                {
                                                    xtype: 'container',
                                                    anchor: '100%',
                                                    height: 26,
                                                    hidden: hidden('APW_CTN_Agreement'),
                                                    layout: {
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'textfield',
                                                            name: 'idAgreement',
                                                            hidden: true
                                                        },
                                                        {
                                                            xtype: 'UpperCaseTextFieldReadOnly',
                                                            allowBlank: true,
                                                            fieldLabel: getName('APWTXAgreement'),
                                                            flex: 1,
                                                            name: 'codeAgreement'
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            name: 'searchAgreement',
                                                            width: 25,
                                                            iconCls: 'search',
                                                            handler: function () {
                                                                var win = Ext.create('DukeSource.view.fulfillment.window.ViewWindowAgreementSearch', {
                                                                    modal: true,
                                                                    height: 430,
                                                                    width: 830
                                                                }).show();
                                                                win.down('grid').on('itemdblclick', function () {
                                                                    var row = win.down('grid').getSelectionModel().getSelection()[0];
                                                                    me.down('textfield[name=idAgreement]').setValue(row.get('idAgreement'));
                                                                    me.down('UpperCaseTextFieldReadOnly[name=codeAgreement]').setValue(row.get('codeAgreement'));
                                                                    win.close();
                                                                });
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'container',
                                                    anchor: '100%',
                                                    height: 26,
                                                    layout: {
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'textfield',
                                                            name: 'userReceptor',
                                                            itemId: 'userReceptor',
                                                            hidden: true
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            name: 'idOwnerProcess',
                                                            itemId: 'idOwnerProcess',
                                                            hidden: true
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            name: 'idProcessType',
                                                            itemId: 'idProcessType',
                                                            hidden: true
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            name: 'idProcess',
                                                            itemId: 'idProcess',
                                                            hidden: true
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            name: 'idSubProcess',
                                                            itemId: 'idSubProcess',
                                                            hidden: true
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            name: 'idActivity',
                                                            itemId: 'idActivity',
                                                            hidden: true
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            name: 'descriptionEntity',
                                                            hidden: true
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            name: 'emailReceptor',
                                                            itemId: 'emailReceptor',
                                                            hidden: true
                                                        },
                                                        {
                                                            xtype: 'UpperCaseTextFieldReadOnly',
                                                            blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                                                            allowBlank: false,
                                                            fieldLabel: 'Usuario',
                                                            flex: 1,
                                                            name: 'fullNameReceptor',
                                                            itemId: 'fullNameReceptor'
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            name: 'searchResponsible',
                                                            width: 25,
                                                            iconCls: 'search',
                                                            handler: function () {
                                                                var win = Ext.create('DukeSource.view.fulfillment.window.ViewWindowSearchUserProcess', {
                                                                    modal: true
                                                                }).show();
                                                                win.down('grid').on('itemdblclick', function () {
                                                                    var row = win.down('grid').getSelectionModel().getSelection()[0];
                                                                    if (row.get('category') !==DukeSource.global.GiroConstants.ANALYST) {
                                                                        me.down('#userReceptor').setValue(row.get('username'));
                                                                        me.down('#idOwnerProcess').setValue(row.get('id'));
                                                                        me.down('#idProcessType').setValue(row.get('idProcessType'));
                                                                        me.down('#idProcess').setValue(row.get('idProcess'));
                                                                        me.down('#idSubProcess').setValue(row.get('idSubProcess'));
                                                                        me.down('#idActivity').setValue(row.get('idActivity'));
                                                                        me.down('#fullNameReceptor').setValue(row.get('fullName'));
                                                                        me.down('#sendEmail').setValue(row.get('email') + ';');
                                                                        win.close();
                                                                    } else {
                                                                       DukeSource.global.DirtyView.messageWarning("La categoría no esta permitida");
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    name: 'unity',
                                                    itemId: 'unity',
                                                    hidden: true
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    name: 'costCenter',
                                                    itemId: 'costCenter',
                                                    hidden: true
                                                },
                                                {
                                                    xtype: 'displayfield',
                                                    anchor: '100%',
                                                    fieldLabel: 'Gerencia',
                                                    itemId: 'descriptionUnity',
                                                    name: 'descriptionUnity',
                                                    padding: '2 0 2 0',
                                                    value: '(Seleccionar)',
                                                    fieldCls: 'style-for-url',
                                                    listeners: {
                                                        afterrender: function (view) {
                                                            view.getEl().on('click', function () {
                                                                Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeOrganization', {
                                                                    backWindow: me,
                                                                    descriptionUnity: 'descriptionUnity',
                                                                    unity: 'unity',
                                                                    costCenter: 'costCenter'
                                                                }).show();
                                                            })
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    anchor: '100%',
                                                    name: 'sendEmail',
                                                    itemId: 'sendEmail',
                                                    fieldLabel: 'Enviar correo'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    flex: 1,
                                    border: false,
                                    title: '<b>SOLICITANTE Y ADJUNTOS</b>',
                                    layout: {
                                        type: 'anchor'
                                    },
                                    margin: '0 0 0 5',
                                    items: [
                                        {
                                            xtype: 'textfield',
                                            name: 'unityClaimant',
                                            itemId: 'unityClaimant',
                                            hidden: true
                                        },
                                        {
                                            xtype: 'textfield',
                                            name: 'costCenterClaimant',
                                            itemId: 'costCenterClaimant',
                                            hidden: true
                                        },
                                        {
                                            xtype: 'displayfield',
                                            anchor: '100%',
                                            fieldLabel: 'Area solicitante',
                                            hidden: hidden('PLAN_WRAP_DSF_DescriptionUnityClaimant'),
                                            itemId: 'descriptionUnityClaimant',
                                            name: 'descriptionUnityClaimant',
                                            padding: '2 0 2 0',
                                            height: 22,
                                            value: '(Seleccionar)',
                                            fieldCls: 'style-for-url',
                                            listeners: {
                                                afterrender: function (view) {
                                                    view.getEl().on('click', function () {
                                                        Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowTreeOrganization', {
                                                            backWindow: me,
                                                            descriptionUnity: 'descriptionUnityClaimant',
                                                            unity: 'unityClaimant',
                                                            costCenter: 'costCenterClaimant'
                                                        }).show();
                                                    })
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'fileuploadfield',
                                            anchor: '100%',
                                            fieldLabel: 'Adjunto',
                                            buttonConfig: {
                                                iconCls: 'tesla even-attachment'
                                            },
                                            buttonText: ""
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            buttons: [
                {
                    text: 'Guardar',
                    hidden: !isEditable,
                    iconCls: 'save',
                    scale: 'medium',
                    action: 'saveDocumentFulfillment'
                },
                {
                    text: 'Salir',
                    iconCls: 'logout',
                    scale: 'medium',
                    handler: function () {
                        me.close();
                    }
                }
            ],
            buttonAlign: 'center',
            listeners: {
                beforeshow: function () {
                    me.setEditableWin(isEditable, isInMyModule);
                }
            }
        });
        me.callParent(arguments);
    },

    close: function () {
        if (this.fireEvent('beforeclose', this) !== false) {
            var me = this;
            if (me.actionType === 'new') {
               DukeSource.global.DirtyView.messageOut(me);
            } else {
                me.doClose();
            }
        }
    },

    setEditableWin: function (isEditable, isInMyModule) {

        var me = this;

        if (LINE_DEFENSE > 1) {

            if (!isInMyModule) {
                me.down('form').query('.textfield, .displayfield').forEach(function (c) {
                   DukeSource.global.DirtyView.toReadOnly(c);
                });
                me.down('form').query('.button').forEach(function (c) {
                    c.setDisabled(true)
                });
            }

        } else {
            if (!isEditable) {
                me.down('form').query('.textfield, .displayfield').forEach(function (c) {
                   DukeSource.global.DirtyView.toReadOnly(c);
                });
                me.down('form').query('.button').forEach(function (c) {
                    c.setDisabled(true)
                });
            }
        }

    }
});