Ext.define("ActionPlanManager", {
  extend: "Ext.data.Model",
  fields: [
    "idDocument",
    "idDetailDocument",
    "descriptionTypeDocument",
    "descriptionEntity",
    "description",
    "dateReception",
    "stateDocument",
    "nameStateDocument",
    "nameStateProcess",
    "descriptionPriority",
    "userEmitted",
    "fullNameEmitted",
    "emailEmitted",
    "fullNameReceptor",
    "userReceptor",
    "emailReceptor",
    "dateEnd",
    "dateAssign",
    "priority",
    "typeDocument",
    "entitySender",
    "observation",
    "category",
    "categoryManager",
    "colorByTime",
    "colorByPerformance",
    "percentage",
    "percentageMonitoring",
    "idActionPlan",
    "descriptionActionPlan",
    "idRisk",
    "codeRisk",
    "versionCorrelative",
    "descriptionRisk",
    "descriptionVersion",
    "idWeakness",
    "codeWeakness",
    "descriptionWeakness",
    "idRiskWeaknessDetail",
    "jobPlace",
    "descriptionJobPlace",
    "employment",
    "descriptionUnityClaimant",
    "workArea",
    "descriptionWorkArea",
    "dateInitPlan",
    "idProcess",
    "descriptionProcess",
    "numberAgree",
    "idAgreement",
    "codeAgreement",
    "codePlan",
    "numberRiskAssociated",
    "codesRisk",
    "numberEvent",
    "codesEvent",
    "indicatorAttachment",
    "indicatorReprogram",
    "indicatorPlan",
    "descriptionIndicatorPlan",
    "originPlan",
    "dateInvalid",
    "descriptionInvalid"
  ]
});

var storeActionPlanManager = Ext.create("Ext.data.Store", {
  model: "ActionPlanManager",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.fulfillment.window.WindowDetailActionPlan", {
  extend: "Ext.window.Window",
  alias: "widget.WindowDetailActionPlan",
  width: 1100,
  height: 500,
  layout: {
    type: "fit"
  },
  title: "",
  titleAlign: "center",
  border: true,
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "gridpanel",
          store: storeActionPlanManager,
          loadMask: true,
          columnLines: true,
          title: "Planes de accion",
          region: "center",
          titleAlign: "center",
          flex: 2,
          padding: 2,
          border: true,
          columns: [
            { xtype: "rownumberer", width: 25, sortable: false },
            {
              header: "C&oacute;digo ",
              align: "left",
              dataIndex: "codePlan",
              width: 150,
              renderer: function(value, metaData, record) {
                var reprogram;
                var style = settingStyle(record.get("percentageMonitoring"));
                var state;
                if (record.get("stateDocument") == "PR") {
                  metaData.tdAttr =
                    'style="background-color:#d4ecff !important;"';
                  state =
                    '<div class="' +
                    style +
                    '">' +
                    record.get("percentageMonitoring") +
                    "</div>";
                } else if (
                  record.get("stateDocument") == "PE" ||
                  record.get("stateDocument") == "PZ"
                ) {
                  metaData.tdAttr =
                    'style="background-color:#ffdfdf !important;"';
                  state =
                    '<div class="' +
                    style +
                    '">' +
                    record.get("percentageMonitoring") +
                    "</div>";
                } else if (record.get("stateDocument") == "CU") {
                  metaData.tdAttr =
                    'style="background-color:#d7ffe5 !important;"';
                  state =
                    '<div class="' +
                    style +
                    '">' +
                    record.get("percentageMonitoring") +
                    "</div>";
                } else {
                  metaData.tdAttr =
                    'style="background-color:#d8d8d8 !important;"';
                  state =
                    '<div class="' +
                    style +
                    '">' +
                    record.get("percentageMonitoring") +
                    "</div>";
                }

                if (record.get("indicatorReprogram") == "S") {
                  reprogram =
                    '<div class="tesla even-retweet" style="display: inline-block"></div>';
                } else {
                  reprogram = "<div></div>";
                }

                if (record.get("indicatorAttachment") == "S") {
                  return (
                    '<div class="icon-attachment"><i class="tesla even-attachment"></i></div><div style="display:inline-block;position:absolute;">' +
                    '<span class="text-main-icon">' +
                    value +
                    "</span>" +
                    state +
                    reprogram +
                    "</div>"
                  );
                } else {
                  return (
                    '<div class="icon-attachment"></div><div style="display:inline-block;position:absolute;">' +
                    '<span class="text-main-icon">' +
                    value +
                    "</span>" +
                    state +
                    reprogram +
                    "</div>"
                  );
                }
              }
            },
            {
              header: "Avance",
              align: "center",
              dataIndex: "percentage",
              width: 110,
              renderer: function(
                value,
                metaData,
                record,
                row,
                col,
                store,
                gridView
              ) {
                return changeMonitoring(value, record, metaData);
              },
              hidden: category != DukeSource.global.GiroConstants.ANALYST
            },
            {
              header: "Plan de acci&oacute;n",
              headerAlign: "center",
              dataIndex: "description",
              width: 500
            },
            {
              header: "Fecha inicio",
              align: "center",
              dataIndex: "dateInitPlan",
              type: "date",
              dateFormat: "d/m/Y",
              width: 100
            },
            {
              header: "Fecha l&iacute;mite",
              align: "center",
              dataIndex: "dateEnd",
              type: "date",
              dateFormat: "d/m/Y",
              width: 100
            },
            {
              header: "Responsable",
              dataIndex: "fullNameReceptor",
              width: 150
            },
            {
              header: "Remitente",
              dataIndex: "fullNameEmitted",
              width: 150
            },
            {
              header: "Area solicitante",
              dataIndex: "descriptionUnityClaimant",
              width: 180,
              renderer: function(value, metaData, record) {
                var s = value.split(" &#8702; ");
                var path = "";
                for (var i = 0; i < s.length; i++) {
                  var text = s[i];
                  text = s[i] + "<br>";
                  path = path + " &#8702; " + text;
                }
                return path;
              }
            },
            {
              header: "Eventos de pérdida",
              dataIndex: "codesEvent",
              hidden: true,
              width: 120,
              align: "center",
              renderer: function(a) {
                return '<span style="color:red;">' + a + "</span>";
              }
            }
          ],
          viewConfig: {},
          listeners: {
            render: function(grid) {
              if (category === DukeSource.global.GiroConstants.GESTOR) {
                var risk = Ext.create("Ext.grid.column.Column", {
                  text: "Riesgos",
                  align: "center",
                  dataIndex: "codesRisk",
                  width: 120,
                  renderer: function(a) {
                    return '<span style="color:#00AA00;">' + a + "</span>";
                  }
                });
                grid.headerCt.insert(5, risk);
                grid.getView().refresh();
              } else {
                var validity = Ext.create("Ext.grid.column.Column", {
                  text: "Vigencia",
                  align: "center",
                  dataIndex: "descriptionTypeDocument",
                  width: 110
                });
                var dateAssign = Ext.create("Ext.grid.column.Column", {
                  text: "Fecha asignaci&oacute;n",
                  align: "center",
                  dataIndex: "dateAssign",
                  type: "date",
                  dateFormat: "d/m/Y",
                  width: 80
                });
                var risk = Ext.create(
                  "Ext.grid.column.Column",
                  {
                    header: "Riesgos",
                    align: "center",
                    dataIndex: "codesRisk",
                    width: 120,
                    renderer: function(a) {
                      return '<span style="color:#00AA00;">' + a + "</span>";
                    }
                  },
                  {
                    xtype: "actioncolumn",
                    align: "center",
                    width: 45,
                    header: "IR",
                    items: [
                      {
                        icon: "images/ir.png",
                        handler: function(grid, rowIndex) {
                          var rowRisk = grid.store.getAt(rowIndex);
                          var panelEvaluation = Ext.ComponentQuery.query(
                            "ViewPanelEvaluationRiskOperational"
                          )[0];
                          var panelPreviousEvaluation = Ext.ComponentQuery.query(
                            "ViewPanelPreviousIdentifyRiskOperational"
                          )[0];
                          if (panelEvaluation != undefined) {
                            panelEvaluation.destroy();
                          }
                          if (panelPreviousEvaluation != undefined) {
                            panelPreviousEvaluation.destroy();
                          }
                          if (
                            Ext.ComponentQuery.query(
                              "ViewPanelIdentifyRiskOperational"
                            )[0] == undefined
                          ) {
                           DukeSource.global.DirtyView.verifyLoadController(
                              "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelPreviousIdentifyRiskOperational"
                            );
                           DukeSource.global.DirtyView.verifyLoadController(
                              "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelIdentifyRiskOperational"
                            );
                            var k = Ext.create(
                              "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelIdentifyRiskOperational",
                              {
                                title: "EVRO",
                                origin: "actionPlan",
                                closable: true,
                                border: false
                              }
                            );
                            DukeSource.getApplication().centerPanel.addPanel(k);
                            locateRiskByActionPlan(k, rowRisk);
                          } else {
                            var k = Ext.ComponentQuery.query(
                              "ViewPanelIdentifyRiskOperational"
                            )[0];
                            DukeSource.getApplication().centerPanel.addPanel(k);
                            locateRiskByActionPlan(k, rowRisk);
                          }
                        }
                      }
                    ]
                  }
                );
                var type = Ext.create("Ext.grid.column.Column", {
                  text: "Frecuencia",
                  align: "center",
                  dataIndex: "descriptionIndicatorPlan",
                  width: 90
                });
                grid.headerCt.insert(4, validity);
                grid.headerCt.insert(5, dateAssign);
                grid.headerCt.insert(10, risk);
                grid.headerCt.insert(11, type);
                grid.getView().refresh();
              }
            },
            itemdblclick: function(view, record, item, index, e) {
              var app = DukeSource.application;
              app
                .getController(
                  "DukeSource.controller.fulfillment.ControllerPanelDocumentPending"
                )
                ._onModifyDocument(view);
            }
          },
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: storeActionPlanManager,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            items: [
              "-",
              {
                text: "Exportar",
                iconCls: "excel",
                handler: function(b, e) {
                  b.up("grid").downloadExcelXml();
                }
              }
            ],
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          }
        }
      ],
      buttons: [
        {
          text: "Salir",
          iconCls: "logout",
          scale: "medium",
          handler: function() {
            me.close();
          }
        }
      ],
      buttonAlign: "center"
    });
    me.callParent(arguments);
  }
});

function changeMonitoring(v, m, r) {
  var tmpValue = v / 100;
  var tmpText = v + "%";
  var progressRenderer = (function(pValue, pText) {
    var b = new Ext.ProgressBar();
    if (tmpValue <= 0.3334) {
      b.baseCls = "x-taskBar";
    } else if (tmpValue <= 0.6667) {
      b.baseCls = "x-taskBar-medium";
    } else {
      b.baseCls = "x-taskBar-high";
    }
    return function(pValue, pText) {
      b.updateProgress(pValue, pText, true);
      return Ext.DomHelper.markup(b.getRenderTree());
    };
  })(tmpValue, tmpText);
  return progressRenderer(tmpValue, tmpText);
}

function settingStyle(val) {
  if (0 <= val && val <= 33.34) {
    return "buttonActionPlanRed";
  } else if (33.34 < val && val <= 66.6) {
    return "buttonActionPlanYellow";
  } else {
    return "buttonActionPlanGreen";
  }
}
