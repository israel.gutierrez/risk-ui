Ext.define("PopulationPoint", {
  extend: "Ext.data.Model",
  fields: [
    "fullNameUserEmitted",
    "idDocument",
    "dateReceptor",
    "description",
    "descriptionWorkArea"
  ]
});

Ext.define("DukeSource.view.fulfillment.window.ViewWindowChartTracingGestor", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowChartTracingGestor",
  height: 386,
  width: 800,
  layout: {
    type: "fit"
  },
  title: "SEGUMIENTO DEL DOCUMENTO - RUTA",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "chart",
          store: Ext.create("Ext.data.Store", {
            model: "PopulationPoint",
            autoLoad: true,
            proxy: {
              actionMethods: {
                create: "POST",
                read: "POST",
                update: "POST"
              },
              type: "ajax",
              url: "http://localhost:9000/giro/showTracingDocument.htm",
              extraParams: {
                idDocument: Ext.ComponentQuery.query(
                  "ViewPanelDocumentPending grid"
                )[0]
                  .getSelectionModel()
                  .getSelection()[0]
                  .get("idDocument")
              },
              reader: {
                totalProperty: "totalCount",
                root: "data",
                successProperty: "success"
              }
            }
          }),
          width: 400,
          insetPadding: 20,
          style: "background:#fff",
          animate: true,
          theme: "Category1",
          axes: [
            {
              type: "Time",
              minimum: 0,
              position: "left",
              fields: ["dateReceptor"],
              title: false,
              grid: true,
              label: {
                renderer: Ext.util.Format.numberRenderer("0,0"),
                font: "10px Arial"
              }
            },
            {
              type: "Category",
              position: "bottom",
              fields: ["fullNameUserEmitted"],
              title: false
            }
          ],
          series: [
            {
              type: "line",
              axis: "left",
              xField: "fullNameUserEmitted",
              yField: "idDocument",
              listeners: {
                itemmouseup: function(item) {
                  Ext.example.msg(
                    "Item Selected",
                    item.value[1] +
                      " visits on " +
                      Ext.Date.monthNames[item.value[0]]
                  );
                }
              },
              tips: {
                trackMouse: true,
                width: 300,
                height: 70,
                renderer: function(storeItem, item) {
                  this.setTitle("ASUNTO : " + storeItem.get("description"));
                  this.update(
                    "FECHA RECEPCION : " +
                      storeItem.get("dateReceptor") +
                      "<br>" +
                      "AREA : " +
                      storeItem.get("descriptionWorkArea")
                  );
                }
              },
              style: {
                fill: "#38B8BF",
                stroke: "#38B8BF",
                "stroke-width": 3
              },
              markerConfig: {
                type: "circle",
                size: 4,
                radius: 4,
                "stroke-width": 0,
                fill: "#38B8BF",
                stroke: "#38B8BF"
              }
            }
          ]
        }
      ]
    });

    me.callParent(arguments);
  }
});
