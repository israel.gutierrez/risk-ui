Ext.define("ModelSearchActionPlan", {
  extend: "Ext.data.Model",
  fields: [
    "idDocument",
    "idDetailDocument",
    "descriptionTypeDocument",
    "descriptionEntity",
    "description",
    "dateReception",
    "stateDocument",
    "nameStateDocument",
    "nameStateProcess",
    "descriptionPriority",
    "userEmitted",
    "fullNameEmitted",
    "emailEmitted",
    "fullNameReceptor",
    "userReceptor",
    "emailReceptor",
    "dateEnd",
    "dateAssign",
    "priority",
    "typeDocument",
    "entitySender",
    "observation",
    "category",
    "colorByTime",
    "colorByPerformance",
    "percentage",
    "percentageMonitoring",
    "idActionPlan",
    "descriptionActionPlan",
    "idRisk",
    "codeRisk",
    "versionCorrelative",
    "descriptionRisk",
    "descriptionVersion",
    "idWeakness",
    "codeWeakness",
    "descriptionWeakness",
    "idRiskWeaknessDetail",
    "jobPlace",
    "descriptionJobPlace",
    "employment",
    "descriptionEmployment",
    "workArea",
    "descriptionWorkArea",
    "dateInitPlan",
    "idProcess",
    "descriptionProcess",
    "numberAgree",
    "idAgreement",
    "codeAgreement",
    "codePlan",
    "numberRiskAssociated",
    "codesRisk",
    "numberEvent",
    "codesEvent",
    "indicatorAttachment",
    "indicatorReprogram"
  ]
});

var statesActionPlan = Ext.create("Ext.data.Store", {
  fields: ["id", "name"],
  data: [
    {
      id: "PR",
      name: "Proceso"
    },
    {
      id: "PZ",
      name: "Rechazado"
    },
    {
      id: "CU",
      name: "Culminado"
    }
  ]
});

var storeSearchActionPlan = Ext.create("Ext.data.Store", {
  model: "ModelSearchActionPlan",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.fulfillment.window.ViewWindowSearchActionPlan", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowSearchActionPlan",
  requires: ["DukeSource.view.risk.util.search.AdvancedSearchActionPlan"],
  layout: "border",
  anchorSize: 100,
  title: "Plan de acción",
  titleAlign: "center",
  width: 1000,
  height: 500,
  tbar: [
    {
      xtype: "combobox",
      fieldLabel: "Filtrar por",
      itemId: "comboState",
      value: DukeSource.global.GiroConstants.PROCESS,
      store: statesActionPlan,
      queryMode: "local",
      displayField: "name",
      valueField: "id",
      listeners: {
        select: function(cbo) {
          var panel = cbo.up("window");
          var grid = panel.down("grid");
          grid.store.getProxy().extraParams = { stateDocument: cbo.getValue() };
          grid.store.getProxy().url =
            "http://localhost:9000/giro/showDefaultFilesByState.htm?nameView=ViewPanelDocumentPending";
          grid.down("pagingtoolbar").moveFirst();
        }
      }
    },
    {
      xtype: "button",
      text: "Nuevo",
      iconCls: "add",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      handler: function() {
        var win = Ext.create(
          "DukeSource.view.fulfillment.window.WindowRegisterActionPlan",
          {
            modal: true,
            actionType: "new",
            buttons: [
              {
                text: "Guardar",
                iconCls: "save",
                scale: "medium",
                handler: function(btn) {
                  var win = btn.up("window");
                  var form = win.down("form");
                  if (form.getForm().isValid()) {
                    form.getForm().submit({
                      url:
                        "http://localhost:9000/giro/saveDocument.htm?nameView=PanelProcessRiskEvaluation",
                      waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                      method: "POST",
                      success: function(form, action) {
                        var grid = Ext.ComponentQuery.query(
                          "ViewWindowSearchActionPlan grid"
                        )[0];
                        var valor = Ext.decode(action.response.responseText);
                        if (valor.success) {
                         DukeSource.global.DirtyView.messageNormal(valor.message);
                          grid.getStore().load({
                            params: {
                              stateDocument:
                                DukeSource.global.GiroConstants.PROCESS
                            },
                            url:
                              "http://localhost:9000/giro/showDefaultFilesByState.htm?nameView=PanelProcessRiskEvaluation",
                            callback: function() {
                              Ext.ComponentQuery.query(
                                "ViewWindowSearchActionPlan"
                              )[0]
                                .down("#comboState")
                                .setValue(DukeSource.global.GiroConstants.PROCESS);
                            }
                          });
                          win.doClose();
                        } else {
                         DukeSource.global.DirtyView.messageWarning(valor.message);
                        }
                      },
                      failure: function(form, action) {
                        var valor = Ext.decode(action.response.responseText);
                        if (!valor.success) {
                         DukeSource.global.DirtyView.messageWarning(valor.message);
                        }
                      }
                    });
                  } else {
                   DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
                  }
                }
              },
              {
                text: "Salir",
                scale: "medium",
                iconCls: "logout",
                handler: function() {
                  win.close();
                }
              }
            ]
          }
        ).show();
        win.down("#typeDocument").setValue("1");
        win.down("#stateDocument").setValue(DukeSource.global.GiroConstants.PENDING);
        win.down("#description").setReadOnly(false);
        win.down("#description").setFieldStyle("background: #d9ffdb");
      }
    }
  ],
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "container",
          flex: 2,
          region: "center",
          padding: "2 2 2 0",
          layout: {
            align: "stretch",
            type: "hbox"
          },
          items: [
            {
              xtype: "gridpanel",
              padding: "0 1 0 0",
              columnLines: true,
              store: storeSearchActionPlan,
              flex: 1,
              titleAlign: "center",
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false,
                  locked: true
                },
                {
                  header: "Seguimiento",
                  dataIndex: "percentageMonitoring",
                  tdCls: "column-nor-border-left",
                  align: "left",
                  locked: true,

                  width: 120,
                  renderer: function(value, metaData, record) {
                    return changeMonitoring(value, record, metaData);
                  }
                },
                {
                  header: "Código del plan",
                  headerAlign: "center",
                  align: "center",
                  locked: true,
                  dataIndex: "codePlan",
                  width: 90
                },
                {
                  header: "Plan de acción",
                  headerAlign: "center",
                  dataIndex: "description",
                  width: 500
                },
                {
                  header: "Vigencia",
                  align: "center",
                  dataIndex: "descriptionTypeDocument",
                  width: 110
                },
                {
                  header: "Fecha asignación",
                  align: "center",
                  dataIndex: "dateAssign",
                  type: "date",
                  dateFormat: "d/m/Y",
                  width: 80
                },
                {
                  header: "Fecha inicio",
                  align: "center",
                  dataIndex: "dateInitPlan",
                  type: "date",
                  dateFormat: "d/m/Y",
                  width: 100
                },
                {
                  header: "Fecha límite",
                  align: "center",
                  dataIndex: "dateEnd",
                  type: "date",
                  dateFormat: "d/m/Y",
                  width: 100
                },
                {
                  header: "Involucrados",
                  columns: [
                    {
                      header: "Responsable",
                      dataIndex: "fullNameReceptor",
                      width: 150
                    },
                    {
                      header: "Remitente",
                      dataIndex: "fullNameEmitted",
                      width: 150
                    }
                  ]
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: storeSearchActionPlan,
                displayInfo: true,
                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              },
              listeners: {
                render: function() {
                  var me = this;
                  me.store.getProxy().extraParams = {
                    stateDocument: DukeSource.global.GiroConstants.PROCESS
                  };
                  me.store.getProxy().url =
                    "http://localhost:9000/giro/showDefaultFilesByState.htm?nameView=PanelProcessRiskEvaluation";
                  me.down("pagingtoolbar").moveFirst();
                }
              }
            }
          ]
        },
        {
          xtype: "AdvancedSearchActionPlan",
          padding: "2 0 2 2",
          region: "west",
          collapsed: true,
          flex: 1,
          buttonAlign: "center",
          bodyPadding: 5,
          tbar: [
            {
              xtype: "button",
              scale: "medium",
              text: "Buscar",
              iconCls: "search",
              action: "searchAdvancedActionPlan"
            },
            {
              xtype: "button",
              scale: "medium",
              text: "Limpiar",
              iconCls: "clear",
              margin: "0 5 0 0",
              handler: function() {
                me.down("AdvancedSearchActionPlan")
                  .down("form")
                  .getForm()
                  .reset();
              }
            }
          ]
        }
      ]
    });

    me.callParent(arguments);
  }
});
