Ext.define("ModelFileAttachment", {
  extend: "Ext.data.Model",
  fields: [
    "idDocument",
    "idDetailDocument",
    "idTrackingExigency",
    "idFileAttachment",
    "idTracking",
    "correlative",
    "file",
    "state",
    "nameFile",
    "userName"
  ]
});

var storeFileAttachment = Ext.create("Ext.data.Store", {
  model: "ModelFileAttachment",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    extraParams: {},
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.fulfillment.window.ViewWindowFileAttachment", {
  extend: "Ext.window.Window",
  border: false,
  alias: "widget.ViewWindowFileAttachment",
  height: 383,
  width: 589,

  layout: {
    align: "stretch",
    type: "vbox"
  },
  title: "Archivos adjuntos",
  initComponent: function() {
    var me = this;
    var src = me.src;
    var params = me.params;
    var saveFile = me.saveFile;
    var deleteFile = me.deleteFile;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          padding: "0 0 2 0",
          bodyPadding: "10",
          items: [
            {
              xtype: "textfield",
              name: "idFileAttachment",
              value: "id",
              hidden: true
            },
            {
              xtype: "filefield",
              anchor: "100%",
              allowBlank: false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              itemId: "text",
              name: "nameFile",
              fieldLabel: "Archivo",
              buttonConfig: {
                iconCls: "tesla even-attachment"
              },
              buttonText: ""
            }
          ],
          bbar: [
            "->",
            {
              text: "Eliminar",
              itemId: "deleteFile",
              iconCls: "delete",
              action: deleteFile
            },
            "-",
            {
              text: "Guardar",
              itemId: "saveFile",
              iconCls: "save",
              action: saveFile
            },
            "-"
          ]
        },
        {
          xtype: "gridpanel",
          store: storeFileAttachment,
          flex: 1,
          columns: [
            {
              xtype: "rownumberer",
              width: 25,
              sortable: false
            },
            {
              dataIndex: "idFileAttachment",
              width: 60,
              text: "Codigo"
            },
            {
              dataIndex: "nameFile",
              flex: 1,
              text: "Nombre",
              align: "center"
            },
            {
              dataIndex: "userName",
              flex: 1,
              text: "Usuario registro"
            },
            {
              xtype: "actioncolumn",
              header: "Descargar",
              align: "center",
              width: 80,
              items: [
                {
                  icon: "images/page_white_put.png",
                  handler: function(grid, rowIndex) {
                   DukeSource.global.DirtyView.downloadFileAttachment(
                      grid,
                      rowIndex,
                      src,
                      params
                    );
                  }
                }
              ]
            }
          ],
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: 50,
            store: storeFileAttachment
          }
        }
      ],
      buttons: [
        {
          text: "Salir",
          scope: this,
          handler: this.close,
          iconCls: "logout",
          scale: "medium"
        }
      ],
      buttonAlign: "center"
    });
    me.callParent(arguments);
  }
});
