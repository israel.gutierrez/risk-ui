Ext.define("ModelFileAttach", {
  extend: "Ext.data.Model",
  fields: [
    "idFileAttachment",
    "idDocument",
    "idTask",
    "idAdvanceActivity",
    "nameFile",
    "nameUserInsert"
  ]
});

var storeFileAttach = Ext.create("Ext.data.Store", {
  model: "ModelFileAttach",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListFileAttachmentsOfActivityAdvance.htm",
    extraParams: {},
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.fulfillment.window.ViewWindowViewAttachDocument", {
  extend: "Ext.window.Window",
  border: false,
  alias: "widget.ViewWindowViewAttachDocument",
  height: 383,
  width: 589,
  layout: {
    align: "stretch",
    type: "vbox"
  },
  title: "Archivos adjuntos",
  initComponent: function() {
    var me = this;
    var rowBack = me.row;
    var src = me.src;
    var params = me.params;
    var saveFile = me.saveFile;
    var deleteFile = me.deleteFile;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          padding: "0 0 2 0",
          bodyPadding: "10",
          items: [
            {
              xtype: "textfield",
              name: "idFileAttachment",
              value: "id",
              hidden: true
            },
            {
              xtype: "filefield",
              anchor: "100%",
              allowBlank: false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              itemId: "text",
              name: "nameFile",
              fieldLabel: "Archivo",
              buttonConfig: {
                iconCls: "tesla even-attachment"
              },
              buttonText: ""
            }
          ],
          bbar: [
            "->",
            {
              text: "Eliminar",
              itemId: "deleteFile",
              disabled: true,
              iconCls: "delete",
              handler: function() {
                var grid = me.down("grid");
                var row = grid.getSelectionModel().getSelection()[0];

                var url = settingValues(params, row, me.deleteFile);

                Ext.MessageBox.show({
                  title:DukeSource.global.GiroMessages.TITLE_WARNING,
                  msg: "Estas seguro que desea eliminar este archivo?",
                  icon: Ext.Msg.WARNING,
                  buttonText: {
                    yes: "Si",
                    no: "No"
                  },
                  buttons: Ext.MessageBox.YESNO,
                  fn: function(btn) {
                    if (btn === "yes") {
                      DukeSource.lib.Ajax.request({
                        method: "POST",
                        url: url,
                        success: function(response) {
                          response = Ext.decode(response.responseText);
                          if (response.success) {
                           DukeSource.global.DirtyView.messageNormal(response.message);
                            grid.down("pagingtoolbar").moveFirst();
                          } else {
                           DukeSource.global.DirtyView.messageWarning(response.message);
                          }
                        }
                      });
                    }
                  }
                });
              }
            },
            "-",
            {
              text: "Guardar",
              itemId: "saveFile",
              iconCls: "save",
              handler: function() {
                var form = me.down("form");
                var grid = me.down("grid");

                var url = settingValues(params, rowBack, me.saveFile);

                if (form.getForm().isValid()) {
                  form.getForm().submit({
                    url: url,
                    waitMsg:DukeSource.global.GiroMessages.MESSAGE_LOADING,
                    method: "POST",
                    success: function(form, action) {
                      var valor = Ext.decode(action.response.responseText);
                      if (valor.success) {
                       DukeSource.global.DirtyView.messageAlert(
                         DukeSource.global.GiroMessages.TITLE_MESSAGE,
                          valor.mensaje,
                          Ext.Msg.INFO
                        );
                        grid.down("pagingtoolbar").moveFirst();
                      } else {
                       DukeSource.global.DirtyView.messageAlert(
                         DukeSource.global.GiroMessages.TITLE_ERROR,
                          valor.mensaje,
                          Ext.Msg.ERROR
                        );
                      }
                    },
                    failure: function(form, action) {
                      var valor = Ext.decode(action.response.responseText);
                      if (!valor.success) {
                       DukeSource.global.DirtyView.messageAlert(
                         DukeSource.global.GiroMessages.TITLE_ERROR,
                          valor.mensaje,
                          Ext.Msg.ERROR
                        );
                      }
                    }
                  });
                } else {
                 DukeSource.global.DirtyView.messageAlert(
                   DukeSource.global.GiroMessages.TITLE_WARNING,
                   DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
                    Ext.Msg.ERROR
                  );
                }
              }
            }
          ]
        },
        {
          xtype: "gridpanel",
          store: storeFileAttach,
          flex: 1,
          columns: [
            {
              xtype: "rownumberer",
              width: 25,
              sortable: false
            },
            {
              dataIndex: "idFileAttachment",
              width: 60,
              text: "Código"
            },
            {
              dataIndex: "nameFile",
              flex: 1,
              text: "Nombre",
              align: "center"
            },
            {
              dataIndex: "nameUserInsert",
              flex: 1,
              text: "Usuario que registró"
            },
            {
              xtype: "actioncolumn",
              header: "Descargar",
              align: "center",
              width: 80,
              items: [
                {
                  iconCls: "documentDownload",
                  handler: function(grid, rowIndex) {
                   DukeSource.global.DirtyView.downloadFileAttachment(
                      grid,
                      rowIndex,
                      src,
                      params
                    );
                  }
                }
              ]
            }
          ],
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: 50,
            store: storeFileAttach
          },
          listeners: {
            select: function(grid, record, index, e) {
              me.down("#deleteFile").enable();
            }
          }
        }
      ],
      buttons: [
        {
          text: "Salir",
          scope: this,
          handler: this.close,
          iconCls: "logout",
          scale: "medium"
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});

function settingValues(params, rowBack, url) {
  for (var i = 0; i < params.length; i++) {
    var s = rowBack.get(params[i]);
    if (i == 0) {
      url = url + "?" + params[i] + "=" + s;
    } else {
      url = url + "&" + params[i] + "=" + s;
    }
  }
  return url;
}
