Ext.define("DukeSource.view.fulfillment.window.ViewWindowAgreement", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowAgreement",
  width: 479,
  border: false,
  layout: {
    type: "fit"
  },
  title: "ACUERDO",
  titleAlign: "center",
  initComponent: function() {
    var me = this;
    var originAgreement = me.originAgreement;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          title: "",
          items: [
            {
              xtype: "textfield",
              itemId: "idAgreement",
              name: "idAgreement",
              value: "ID",
              hidden: true
            },
            {
              xtype: "textfield",
              itemId: "state",
              name: "state",
              value: "S",
              hidden: true
            },
            {
              xtype: "textfield",
              name: "indicatorAttach",
              itemId: "indicatorAttach",
              hidden: true,
              value: "N"
            },
            {
              xtype: "container",
              layout: {
                type: "hbox"
              },
              height: 26,
              items: [
                {
                  xtype: "textfield",
                  anchor: "100%",
                  labelWidth: 130,
                  flex: 1.5,
                  fieldCls: "obligatoryTextField",
                  allowBlank: false,
                  itemId: "codeAgreement",
                  name: "codeAgreement",
                  enforceMaxLength: true,
                  maxLength: 90,
                  fieldLabel: "CODIGO DE ACUERDO"
                },
                {
                  xtype: "datefield",
                  anchor: "100%",
                  flex: 1,
                  padding: "0 0 0 5",
                  labelWidth: 60,
                  format: "d/m/Y",
                  name: "dateAgreement",
                  fieldLabel: "FECHA"
                }
              ]
            },
            {
              xtype: "textfield",
              anchor: "100%",
              labelWidth: 130,
              itemId: "codeCommittee",
              name: "codeCommittee",
              enforceMaxLength: true,
              maxLength: 90,
              fieldLabel: "CODIGO COMITE"
            },

            {
              xtype: "UpperCaseTextArea",
              anchor: "100%",
              labelWidth: 130,
              maxLength: 3000,
              allowBlank: false,
              name: "description",
              fieldCls: "obligatoryTextField",
              fieldLabel: "DESCRIPCION",
              height: 120
            }
          ]
        }
      ],
      buttonAlign: "center",
      buttons: [
        {
          text: "GUARDAR",
          scale: "medium",
          iconCls: "save",
          handler: function(button) {
            var win = button.up("window");
            var form = win.down("form");
            var grid = "";
            if (originAgreement == "searchAgreement") {
              grid = Ext.ComponentQuery.query(
                "ViewWindowAgreementSearch grid"
              )[0];
            } else {
              grid = Ext.ComponentQuery.query(
                "ViewPanelRegisterAgreement grid"
              )[0];
            }
            if (form.getForm().isValid()) {
              Ext.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/saveAgreement.htm?nameView=ViewPanelRegisterAgreement",
                params: {
                  jsonData: Ext.JSON.encode(
                    win
                      .down("form")
                      .getForm()
                      .getValues()
                  )
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    win.close();
                   DukeSource.global.DirtyView.messageAlert(
                     DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      response.mensaje,
                      Ext.Msg.INFO
                    );
                   DukeSource.global.DirtyView.searchPaginationGridNormal(
                      "",
                      grid,
                      grid.down("pagingtoolbar"),
                      "http://localhost:9000/giro/findAgreement.htm",
                      "description",
                      "idAgreement"
                    );
                  } else {
                   DukeSource.global.DirtyView.messageAlert(
                     DukeSource.global.GiroMessages.TITLE_ERROR,
                      response.mensaje,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function() {}
              });
            } else {
             DukeSource.global.DirtyView.messageAlert(
               DukeSource.global.GiroMessages.TITLE_WARNING,
               DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
                Ext.Msg.ERROR
              );
            }
          }
        },
        {
          text: "SALIR",
          scale: "medium",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ]
    });

    me.callParent(arguments);
  }
});
