Ext.define("ModelMonitoringDocument", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "idDocument",
    "descriptionStateDocument",
    "stateDocument",
    "percentage",
    "dateMonitoring",
    "description",
    "state"
  ]
});

var storeMonitoringDocument = Ext.create("Ext.data.Store", {
  model: "ModelMonitoringDocument",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    extraParams: {},
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.fulfillment.window.ViewWindowPanelMonitoring", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowPanelMonitoring",
  height: 450,
  width: 760,
  layout: {
    type: "fit"
  },
  title: "Registro de monitoreos",
  tbar: [
    "-",
    {
      text: "Agregar monitoreo",
      iconCls: "add",
      action: "newMonitoringDocument"
    },
    "-",
    {
      text: "Modificar",
      iconCls: "modify",
      disabled: true,
      action: "modifyMonitoringDocument",
      itemId: "modifyMonitoringDocument"
    },
    "-",
    {
      text: "Eliminar",
      disabled: true,
      iconCls: "delete",
      action: "deleteMonitoringDocument",
      itemId: "deleteMonitoringDocument"
    },
    "->",
    {
      text: "Auditoría",
      iconCls: "auditory",
      handler: function(btn) {
        var grid = Ext.ComponentQuery.query(
          "ViewWindowPanelMonitoring grid"
        )[0];
       DukeSource.global.DirtyView.showWindowAuditory(
          grid,
          "http://localhost:9000/giro/findAuditMonitoringActionPlan.htm"
        );
      }
    }
  ],
  initComponent: function() {
    var me = this;
    var idDocument = me.idDocument;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "gridpanel",
          store: storeMonitoringDocument,
          border: false,
          columns: [
            {
              xtype: "rownumberer",
              width: 25,
              sortable: false
            },
            {
              dataIndex: "dateMonitoring",
              width: 100,
              sortable: false,
              text: "Fecha registro",
              align: "center"
            },
            {
              dataIndex: "percentage",
              width: 120,
              sortable: false,
              text: "Porcentaje real de avance",
              align: "center",
              renderer: function(value, metaData, record) {
                return change(value, record, metaData);
              }
            },
            {
              dataIndex: "descriptionStateDocument",
              width: 120,
              text: "Estado",
              sortable: false,
              align: "center"
            },
            {
              dataIndex: "description",
              sortable: false,
              width: 400,
              text: "Descripción",
              align: "center"
            }
          ],
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            store: storeMonitoringDocument,
            items: [
              {
                xtype:"UpperCaseTrigger",
                width: 120,
                action: "searchGridMonitoringDocument"
              }
            ]
          },
          listeners: {
            select: function(grid, record) {
              if (record.index === 0) {
                me.down("#modifyMonitoringDocument").enable();
                me.down("#deleteMonitoringDocument").enable();
              } else {
                me.down("#modifyMonitoringDocument").disable();
                me.down("#deleteMonitoringDocument").disable();
              }
            }
          }
        }
      ],
      buttons: [
        {
          text: "Salir",
          scope: this,
          scale: "medium",
          handler: this.close,
          iconCls: "logout"
        }
      ],
      buttonAlign: "center"
    });
    me.callParent(arguments);
  }
});

function change(val, record, metaData) {
  metaData.tdAttr = 'style="background-color:#74CC70 !important;"';
  return (
    '<div style="background-color:#74CC70;"><span style="color:white;">' +
    val +
    "%</span></div>"
  );
}
