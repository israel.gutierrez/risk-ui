Ext.define("DukeSource.view.fulfillment.window.WindowFormAddOrEditTask", {
  extend: "Ext.window.Window",
  requires: ["DukeSource.view.fulfillment.form.FormTask"],

  width: 700,
  layout: "fit",
  closable: false,

  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "panel",
          items: [
            {
              xtype: "button",
              cls: "m-1",
              iconCls: "arrow_left_16",
              handler: function() {
                me.close();
                Ext.getCmp("WindowTabGridTask").show();
              }
            },
            {
              xtype: "FormTask",
              recordActionPlan: me.recordActionPlan
            }
          ]
        }
      ],
      buttons: [
        {
          text: "Guardar",
          iconCls: "save",
          scale: "medium",
          handler: function() {
            me.down("FormTask").postTaskForm(function() {
              me.close();
              var gridTask = Ext.getCmp("WindowTabGridTask").down(
                'gridpanel[name="gridPanelTask"]'
              );
              gridTask.store.getProxy().url =
                "http://localhost:9000/giro/showTaskByActionPlan.htm";
              gridTask.store.getProxy().extraParams = {
                idActionPlan: me.recordActionPlan.get("idDocument")
              };
              gridTask.down("pagingtoolbar").doRefresh();
              Ext.getCmp("WindowTabGridTask").show();
            });
          }
        },
        {
          text: "Cancelar",
          iconCls: "cancel",
          scale: "medium",
          handler: function() {
            me.close();
            Ext.getCmp("WindowTabGridTask").show();
          }
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});
