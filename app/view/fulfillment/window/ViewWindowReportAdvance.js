Ext.define("DukeSource.view.fulfillment.window.ViewWindowReportAdvance", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowReportAdvance",
  width: 700,
  layout: {
    type: "fit"
  },
  border: false,
  title: "Ingreso del avance a la fecha",
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          title: "",
          items: [
            {
              xtype: "textfield",
              name: "idActionPlan",
              hidden: true
            },
            {
              xtype: "textfield",
              name: "idTaskActionPlan",
              hidden: true
            },
            {
              xtype: "textfield",
              name: "stateDocument",
              hidden: true
            },
            {
              xtype: "textfield",
              name: "idAdvanceActivity",
              value: "id",
              hidden: true
            },
            {
              xtype: "textfield",
              name: "stateRevision",
              value: "P",
              hidden: true
            },
            {
              xtype: "textfield",
              name: "indicatorAttachment",
              value: "N",
              hidden: true
            },
            {
              xtype: "container",
              height: "27",
              layout: {
                type: "hbox",
                align: "stretch"
              },
              border: false,
              items: [
                {
                  flex: 1,
                  border: false
                },
                {
                  flex: 1,
                  border: false
                },
                {
                  xtype: "datefield",
                  flex: 1.3,
                  name: "dateRegister",
                  format: "d/m/Y H:i:s",
                  allowBlank: false,
                  readOnly: true,
                  editable: false,
                  value: new Date(),
                  msgTarget: "side",
                  fieldCls: "readOnlyText",
                  blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  fieldLabel: "Fecha registro",
                  listeners: {
                    specialkey: function(f, e) {
                      DukeSource.global.DirtyView.focusEventEnterObligatory(
                        f,
                        e,
                        me.down("NumberDecimalNumberObligatory")
                      );
                    }
                  }
                }
              ]
            },
            {
              xtype: "UpperCaseTextArea",
              anchor: "100%",
              padding: "5 0 0 0",
              name: "description",
              allowBlank: false,
              height: 70,
              msgTarget: "side",
              maxLength: 600,
              fieldCls: "obligatoryTextField",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              fieldLabel: "Descripción",
              listeners: {
                specialkey: function(f, e) {
                  DukeSource.global.DirtyView.focusEventEnterObligatory(
                    f,
                    e,
                    me.down("#percentage")
                  );
                }
              }
            },
            {
              xtype: "NumberDecimalNumberObligatory",
              maxValue: 100,
              fieldLabel: "Avance",
              allowBlank: false,
              fieldCls: "obligatoryTextField",
              blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              name: "percentage",
              itemId: "percentage",
              listeners: {
                specialkey: function(f, e) {
                  DukeSource.global.DirtyView.focusEventEnterObligatory(
                    f,
                    e,
                    me.down("UpperCaseTextArea[name=observationTask]")
                  );
                }
              }
            },
            {
              xtype: "fileuploadfield",
              fieldLabel: "Archivo",
              buttonConfig: {
                iconCls: "tesla even-attachment"
              },
              buttonText: ""
            },
            {
              xtype: "UpperCaseTextArea",
              anchor: "100%",
              name: "observationTask",
              allowBlank: true,
              hidden: true,
              msgTarget: "side",
              maxLength: 600,
              height: 50,
              fieldLabel: "Observación"
            },
            {
              xtype: "textfield",
              anchor: "100%",
              name: "emailManager",
              itemId: "emailManager",
              fieldLabel: "Enviar correo a",
              readOnly: true,
              fieldCls: "readOnlyText"
            }
          ]
        }
      ]
    });
    me.callParent(arguments);
  }
});
