Ext.define('ModelGridTask', {
    extend: 'Ext.data.Model',
    fields: [
        'category',
        'checked',
        'codeActionPlan',
        'collaborator',
        'dateAssign',
        'dateEnd',
        'dateExpire',
        'dateInitial',
        'dateReception',
        'taskWeight',
        'descriptionTask',
        'descriptionTypeDocument',
        'descriptionWorkArea',
        'emailCollaborator',
        'emailEmitted',
        'emailEmitted',
        'emailReceptor',
        'fullNameCollaborator',
        'fullNameEmitted',
        'fullNameReceptor',
        'idDocument',
        'idTask',
        'indicatorAttachment',
        'jobPlace',
        'leaf',
        'nameStateDocument',
        'percentage',
        'percentageActionPlan',
        'percentageCollaborator',
        'priority',
        'stateDocument',
        'taskWeight',
        'typeDocument',
        'userEmitted',
        'userReceptor',
        'workArea'
    ]
});

var StoreGridTask = Ext.create('Ext.data.Store', {
    model: 'ModelGridTask',
    pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'http://localhost:9000/giro/loadGridDefault.htm',
        reader: {
            type: 'json',
            root: 'data',
            successProperty: 'success',
            totalProperty: 'totalCount'
        }
    }
});

Ext.define('ModelGridProgress', {
    extend: 'Ext.data.Model',
    fields: [
        'checked',
        'dateRegister',
        'description',
        'emailManager',
        'fullName',
        'idActionPlan',
        'idAdvanceActivity',
        'indicatorAttachment',
        'leaf',
        'percentage',
        'username'
    ]
});

var StoreGridProgress = Ext.create('Ext.data.Store', {
    model: 'ModelGridProgress',
    pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: 'http://localhost:9000/giro/loadGridDefault.htm',
        reader: {
            type: 'json',
            root: 'data',
            successProperty: 'success',
            totalProperty: 'totalCount'
        }
    }
});

var createTooltip = function (idElementToShow, text) {
    Ext.create('Ext.tip.ToolTip', {
        target: idElementToShow,
        html: text,
        showDelay: 0,
        hideDelay: 0
    });
};

function settingStyle(val) {
    if (0 <= val && val <= 33.34) {
        return '#f34b4b';
    } else if (33.34 < val && val <= 66.6) {
        return '#ffab23';
    }
    else {
        return '#3ea55a';
    }
}

Ext.define('DukeSource.view.fulfillment.window.WindowTabGridTask', {
    extend: 'Ext.window.Window',
    alias: 'widget.WindowTabGridTask',
    requires: [
        'DukeSource.view.fulfillment.form.FormTask',
        'DukeSource.view.fulfillment.form.FormProgress',
        'Ext.tab.Panel',
        'Ext.tab.Tab',
        'Ext.grid.Panel',
        'Ext.grid.column.Number',
        'Ext.grid.column.Date',
        'Ext.grid.View',
        'Ext.toolbar.Paging'
    ],
    autoShow: true,
    width: 800,
    border: false,
    layout: 'fit',
    id: 'WindowTabGridTask',

    initComponent: function () {
        var me = this;

        var recordActionPlan = me.recordActionPlan;
        var isOwner = recordActionPlan.get('userReceptor') === userName;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'tabpanel',
                    activeTab: 0,
                    border: false,
                    listeners: {
                        render: function (tabPanel) {
                            tabPanel.items.each(function (el) {
                                if (el.title === 'Tareas') {
                                    el.tab.on('click', function () {
                                        tabPanel.down('panel[itemId="progressTab"]').disable();
                                        me.down('panel[title="Avances"]').down('button[id="btnEditProgress"]').disable();
                                        me.down('panel[title="Avances"]').down('button[id="btnDeleteProgress"]').disable();
                                    });
                                }
                            });
                        }
                    },
                    items: [
                        {
                            xtype: 'panel',
                            layout: 'fit',
                            title: 'Tareas',
                            border: false,
                            tabConfig: {
                                xtype: 'tab',
                                flex: 1
                            },
                            dockedItems: [
                                {
                                    xtype: 'container',
                                    dock: 'top',
                                    cls: 'p-05',
                                    layout: 'column',
                                    border: false,
                                    items: [
                                        {
                                            xtype: 'component',
                                            autoEl: {
                                                tag: 'h2',
                                                html: 'Plan de acción'
                                            },
                                            columnWidth: 1,
                                            cls: 'm-0'
                                        },
                                        {
                                            xtype: 'textarea',
                                            columnWidth: 1,
                                            value: recordActionPlan.get('description'),
                                            readOnly: true,
                                            readOnlyCls: 'instructions-read-only',
                                            padding: 5
                                        },
                                        {
                                            xtype: 'container',
                                            columnWidth: 1,
                                            id: 'btnAddAndEdit',
                                            items: [
                                                {
                                                    xtype: 'button',
                                                    margin: '6 4 0 8',
                                                    iconCls: 'add',
                                                    text: 'Agregar',
                                                    scale: 'medium',
                                                    handler: function () {
                                                        var window = Ext.create('DukeSource.view.fulfillment.window.WindowFormAddOrEditTask', {
                                                            modal: true,
                                                            recordActionPlan: recordActionPlan,
                                                            title: me.title
                                                        });
                                                        me.hide();
                                                        window.down('FormTask').validateDates(recordActionPlan.get('dateInitPlan'), recordActionPlan.get('dateEnd'));
                                                        window.show();
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    id: 'btnEditTask',
                                                    disabled: true,
                                                    margin: '6 8 0 4',
                                                    iconCls: 'modify',
                                                    text: 'Editar',
                                                    scale: 'medium',
                                                    listeners: {
                                                        click: function (btn) {
                                                            var grid = btn.up('window').down('grid');
                                                            var rowSelected = grid.getSelectionModel().getSelection()[0];
                                                            var win = Ext.create('DukeSource.view.fulfillment.window.WindowFormAddOrEditTask', {
                                                                modal: true,
                                                                title: me.title,
                                                                height: me.height,
                                                                recordActionPlan: recordActionPlan
                                                            });
                                                            win.down('FormTask').validateDates(recordActionPlan.get('dateInitPlan'), recordActionPlan.get('dateEnd'));
                                                            win.show();
                                                            me.hide();
                                                            win.down('form').getForm().setValues(rowSelected.data);
                                                        },
                                                        render: function () {
                                                            createTooltip('btnEditTask', 'Selecciona una tarea para editarla.');
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    id: 'btnDeleteTask',
                                                    margin: '6 8 0 4',
                                                    disabled: true,
                                                    iconCls: 'delete',
                                                    text: 'Eliminar',
                                                    scale: 'medium',
                                                    listeners: {
                                                        click: function (btn) {
                                                            var grid = btn.up('window').down('#gridPanelTask');
                                                            var row = grid.getSelectionModel().getSelection()[0];

                                                            Ext.MessageBox.show({
                                                                title:DukeSource.global.GiroMessages.TITLE_WARNING,
                                                                msg: 'Estas seguro que desea eliminar esta tarea?',
                                                                icon: Ext.Msg.WARNING,
                                                                buttonText: {
                                                                    yes: "Si", no: "No"
                                                                },
                                                                buttons: Ext.MessageBox.YESNO,
                                                                fn: function (btn) {
                                                                    if (btn === 'yes') {
                                                                        DukeSource.lib.Ajax.request({
                                                                            method: 'POST',
                                                                            url: 'http://localhost:9000/giro/deleteTaskActionPlan.htm',
                                                                            params: {
                                                                                jsonData: Ext.JSON.encode(row.raw)
                                                                            },
                                                                            success: function (response) {
                                                                                response = Ext.decode(response.responseText);
                                                                                if (response.success) {
                                                                                   DukeSource.global.DirtyView.messageNormal(response.message);
                                                                                    grid.getStore().load();
                                                                                } else {
                                                                                   DukeSource.global.DirtyView.messageWarning(response.message);
                                                                                }
                                                                            },
                                                                            failure: function () {
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        },
                                                        render: function () {
                                                            createTooltip('btnDeleteTask', 'Selecciona la primera tarea para eliminarla.');
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    id: 'btnShowAttachment',
                                                    margin: '6 8 0 4',
                                                    hidden: true,
                                                    iconCls: 'attach',
                                                    text: 'Adjuntos',
                                                    scale: 'medium',
                                                    listeners: {
                                                        click: function (btn) {
                                                            var grid = btn.up('window').down('#gridPanelTask');
                                                            var row = grid.getSelectionModel().getSelection()[0];

                                                            var k = Ext.create('DukeSource.view.fulfillment.window.ViewWindowViewAttachDocument', {
                                                                modal: true,
                                                                row: row,
                                                                saveFile: 'http://localhost:9000/giro/saveFileAttachmentTask.htm',
                                                                deleteFile: 'http://localhost:9000/giro/deleteFileAttachmentTask.htm',
                                                                searchGridTrigger: 'searchGridFileAttachmentDocument',
                                                                src: 'http://localhost:9000/giro/downloadTaskFiles.htm',
                                                                params: ["idTask", "idFileAttachment", "nameFile"]
                                                            });
                                                            var gridFileAttachment = k.down('grid');
                                                            gridFileAttachment.store.getProxy().extraParams = {
                                                                idTaskActionPlan: row.get('idTask')
                                                            };
                                                            gridFileAttachment.store.getProxy().url = 'http://localhost:9000/giro/showFilesAttachmentTask.htm';
                                                            gridFileAttachment.down('pagingtoolbar').moveFirst();
                                                            k.idTask = row.get('idTask');
                                                            k.idActionPlan = row.get('idDocument');
                                                            k.show();

                                                        }
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'toolbar',
                                    dock: 'bottom',
                                    border: false,
                                    layout: {
                                        type: 'hbox',
                                        pack: 'center'
                                    },
                                    items: [
                                        {
                                            xtype: 'button',
                                            cls: 'x-btn-default-medium bo-gray',
                                            iconCls: 'logout',
                                            text: 'Salir',
                                            scale: 'medium',
                                            handler: function () {
                                                me.close();
                                            }
                                        }
                                    ]
                                }
                            ],
                            items: [
                                {
                                    xtype: 'container',
                                    layout: 'fit',
                                    items: [
                                        {
                                            xtype: 'gridpanel',
                                            name: 'gridPanelTask',
                                            title: 'Tareas',
                                            itemId: 'gridPanelTask',
                                            store: StoreGridTask,
                                            autoScroll: true,
                                            border: false,
                                            loadMask: true,
                                            columnLines: true,
                                            enableLocking: true,
                                            layout: 'fit',
                                            height: 400,
                                            columns: [
                                                {
                                                    xtype: 'numbercolumn',
                                                    sortable: false,
                                                    dataIndex: 'percentage',
                                                    text: 'Nivel de avance',
                                                    width: 100,
                                                    format: '0',
                                                    locked: true,
                                                    tdCls: 'column-nor-border-left',
                                                    renderer: function (value, metaData, record) {
                                                        var style = settingStyle(record.get('percentage'));
                                                        var icon;
                                                        if (record.get('indicatorAttachment') === 'S') {
                                                            icon = '<i class="tesla even-attachment"></i>'
                                                        } else {
                                                            icon = ''
                                                        }
                                                        return '<div style="background-color: ' + style + '; padding: 8px; width: ' + value + 'px">' + icon + '' + value + '%</div>';
                                                    }
                                                },
                                                {
                                                    text: '',
                                                    align: 'center',
                                                    locked: true,
                                                    sortable: false,
                                                    width: 35,
                                                    renderer: function (value, meta, recordTask) {
                                                        var id = Ext.id();
                                                        Ext.defer(function () {
                                                            new Ext.container.Container({
                                                                layout: 'hbox',
                                                                idRowContainer: id,
                                                                itemId: id,
                                                                id: id,
                                                                items: [
                                                                    {
                                                                        xtype: 'button',
                                                                        iconCls: 'side_expand',
                                                                        tooltip: 'Ver avances',
                                                                        handler: function () {
                                                                            var gridTask = me.down('panel[title="Tareas"]').down('grid');
                                                                            var taskSelected = gridTask.getSelectionModel().getSelection()[0];

                                                                            var progressTab = me.down('panel[itemId="progressTab"]');
                                                                            progressTab.enable();
                                                                            me.down('tabpanel').setActiveTab(1).setTitle('Avances');
                                                                            var descTaskContainer = progressTab.down('component[itemId="descriptionTask"]');
                                                                            var gridProgress = Ext.getCmp('WindowTabGridTask').down('gridpanel[name="gridPanelProgress"]');
                                                                            gridProgress.taskSelected = taskSelected;
                                                                            gridProgress.store.getProxy().url = 'http://localhost:9000/giro/showListAdvanceActivityTask.htm';
                                                                            gridProgress.store.getProxy().extraParams = {
                                                                                idActionPlan: me.recordActionPlan.get('idDocument'),
                                                                                idTaskActionPlan: taskSelected.get('idTask')
                                                                            };
                                                                            gridProgress.down('pagingtoolbar').doRefresh();
                                                                            descTaskContainer.removeAll();
                                                                            descTaskContainer.add([{
                                                                                xtype: 'component',
                                                                                autoEl: {
                                                                                    tag: 'h2',
                                                                                    html: 'Tarea'
                                                                                },
                                                                                cls: 'm-0'
                                                                            },
                                                                                {
                                                                                    xtype: 'component',
                                                                                    autoEl: {
                                                                                        tag: 'p',
                                                                                        html: recordTask.get('descriptionTask')
                                                                                    },
                                                                                    cls: 'p-05 m-0'
                                                                                }]);
                                                                        }
                                                                    },
                                                                    {
                                                                        xtype: 'button',
                                                                        iconCls: 'modify',
                                                                        hidden: true,
                                                                        handler: function () {
                                                                            var win = Ext.create('DukeSource.view.fulfillment.window.WindowFormAddOrEditTask', {
                                                                                modal: true,
                                                                                recordActionPlan: recordActionPlan,
                                                                                title: me.title
                                                                            });
                                                                            var grid = me.down('panel[title="Tareas"]').down('grid');
                                                                            var rowSelected = grid.getSelectionModel().getSelection()[0];
                                                                            win.down('FormTask').validateDates(recordActionPlan.get('dateInitPlan'), recordActionPlan.get('dateEnd'));
                                                                            win.show();
                                                                            me.hide();
                                                                            win.down('form').getForm().setValues(rowSelected.data);
                                                                        }
                                                                    }
                                                                ]
                                                            }).render(document.body, id);
                                                        }, 50);
                                                        recordTask.idRowContainer = id;
                                                        return Ext.String.format('<div id="{0}"></div>', id);
                                                    }
                                                },
                                                {
                                                    xtype: 'numbercolumn',
                                                    dataIndex: 'taskWeight',
                                                    text: 'Peso de tarea',
                                                    format: '.00',
                                                    sortable: false,
                                                    hidden: true,
                                                    align: 'center'
                                                },
                                                {
                                                    header: 'Descripción',
                                                    sortable: false,
                                                    headerAlign: 'center',
                                                    dataIndex: 'descriptionTask',
                                                    width: 500
                                                },
                                                {
                                                    header: 'Fecha de inicio',
                                                    align: 'center',
                                                    dataIndex: 'dateInitial',
                                                    type: 'date',
                                                    sortable: false,
                                                    dateFormat: 'd/m/Y',
                                                    width: 100
                                                },
                                                {
                                                    header: 'Fecha límite',
                                                    align: 'center',
                                                    dataIndex: 'dateExpire',
                                                    type: 'date',
                                                    sortable: false,
                                                    dateFormat: 'd/m/Y',
                                                    width: 100
                                                }
                                            ],
                                            listeners: {

                                                select: function (thisGrid, record) {

                                                    if ((record.index === 0 && isOwner) ||DukeSource.global.GiroConstants.ANALYST === category) {
                                                        me.down('panel[title="Tareas"]').down('button[id="btnEditTask"]').enable();
                                                        me.down('panel[title="Tareas"]').down('button[id="btnDeleteTask"]').enable();
                                                    } else {
                                                        me.down('panel[title="Tareas"]').down('button[id="btnEditTask"]').disable();
                                                        me.down('panel[title="Tareas"]').down('button[id="btnDeleteTask"]').disable();
                                                    }

                                                    me.down('#btnShowAttachment').setVisible(true);
                                                }
                                            },
                                            dockedItems: [
                                                {
                                                    xtype: 'pagingtoolbar',
                                                    name: 'pagingTask',
                                                    store: StoreGridTask,
                                                    dock: 'bottom',
                                                    border: false,
                                                    afterPageText: 'de {0}',
                                                    beforePageText: 'Página',
                                                    displayInfo: true,
                                                    pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
                                                    displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                                                    emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            layout: 'fit',
                            itemId: 'progressTab',
                            title: 'Avances | Elige una tarea para ver sus avances',
                            border: false,
                            disabled: true,
                            tabConfig: {
                                xtype: 'tab',
                                flex: 1
                            },
                            dockedItems: [
                                {
                                    xtype: 'container',
                                    border: false,
                                    dock: 'top',
                                    cls: 'p-05',
                                    layout: 'column',
                                    items: [
                                        {

                                            xtype: 'container',
                                            itemId: 'descriptionTask'
                                        },
                                        {
                                            xtype: 'container',
                                            layout: 'column',
                                            id: 'progressBtnAddAndEdit',
                                            items: [
                                                {
                                                    xtype: 'button',
                                                    margin: '6 4 0 8',
                                                    iconCls: 'add',
                                                    text: 'Agregar',
                                                    scale: 'medium',
                                                    handler: function () {
                                                        var grid = me.down('#progressTab').down('grid');
                                                        var taskSelected = grid.taskSelected;
                                                        var win = Ext.create('DukeSource.view.fulfillment.window.WindowFormAddOrEditProgress', {
                                                            modal: true,
                                                            title: me.title,
                                                            height: me.height,
                                                            rowSelected: taskSelected,
                                                            recordActionPlan: me.recordActionPlan,
                                                            idTaskActionPlanField: {
                                                                xtype: 'textfield',
                                                                name: 'idTaskActionPlan',
                                                                itemId: 'idTaskActionPlan',
                                                                value: taskSelected.get('idTask'),
                                                                hidden: true
                                                            }
                                                        });

                                                        win.down('FormProgress').validateDates(recordActionPlan.get('dateInitPlan'));
                                                        me.hide();
                                                        win.show();
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    id: 'btnEditProgress',
                                                    margin: '6 8 0 4',
                                                    iconCls: 'modify',
                                                    text: 'Editar',
                                                    scale: 'medium',
                                                    disabled: true,
                                                    listeners: {
                                                        click: function (btn) {
                                                            me.hide();
                                                            var grid = btn.up('window').down('panel[title="Avances"]').down('grid');
                                                            var rowSelectedProgress = grid.getSelectionModel().getSelection()[0];
                                                            var taskSelected = grid.taskSelected;
                                                            var win = Ext.create('DukeSource.view.fulfillment.window.WindowFormAddOrEditProgress', {
                                                                modal: true,
                                                                title: me.title,
                                                                height: me.height,
                                                                recordActionPlan: me.recordActionPlan,
                                                                rowSelected: taskSelected,
                                                                idTaskActionPlanField: {
                                                                    xtype: 'textfield',
                                                                    name: 'idTaskActionPlan',
                                                                    itemId: 'idTaskActionPlan',
                                                                    value: taskSelected.get('idTask'),
                                                                    hidden: true
                                                                }
                                                            });
                                                            win.down('FormProgress').validateDates();
                                                            win.show();
                                                            win.down('form').getForm().setValues(rowSelectedProgress.data);
                                                        },
                                                        render: function () {
                                                            createTooltip('btnEditProgress', 'Selecciona un avance para editarlo.')
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    id: 'btnDeleteProgress',
                                                    margin: '6 8 0 4',
                                                    disabled: true,
                                                    iconCls: 'delete',
                                                    text: 'Eliminar',
                                                    scale: 'medium',
                                                    listeners: {
                                                        click: function (btn) {
                                                            var grid = btn.up('window').down('#gridPanelProgress');
                                                            var row = grid.getSelectionModel().getSelection()[0];

                                                            Ext.MessageBox.show({
                                                                title:DukeSource.global.GiroMessages.TITLE_WARNING,
                                                                msg: 'Estas seguro que desea eliminar este avance?',
                                                                icon: Ext.Msg.WARNING,
                                                                buttonText: {
                                                                    yes: "Si", no: "No"
                                                                },
                                                                buttons: Ext.MessageBox.YESNO,
                                                                fn: function (btn) {
                                                                    if (btn === 'yes') {
                                                                        DukeSource.lib.Ajax.request({
                                                                            method: 'POST',
                                                                            url: 'http://localhost:9000/giro/deleteAdvanceActivity.htm',
                                                                            params: {
                                                                                jsonData: Ext.JSON.encode(row.raw)
                                                                            },
                                                                            success: function (response) {
                                                                                response = Ext.decode(response.responseText);
                                                                                if (response.success) {
                                                                                   DukeSource.global.DirtyView.messageNormal(response.message);
                                                                                    grid.getStore().load();
                                                                                } else {
                                                                                   DukeSource.global.DirtyView.messageWarning(response.message);
                                                                                }
                                                                            },
                                                                            failure: function () {
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        },
                                                        render: function () {
                                                            createTooltip('btnDeleteProgress', 'Selecciona la primera tarea para eliminarla.');
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    id: 'btnAttachmentAdvance',
                                                    margin: '6 8 0 4',
                                                    hidden: true,
                                                    iconCls: 'attach',
                                                    text: 'Adjuntos',
                                                    scale: 'medium',
                                                    listeners: {
                                                        click: function (btn) {
                                                            var grid = btn.up('window').down('#gridPanelProgress');
                                                            var row = grid.getSelectionModel().getSelection()[0];

                                                            var k = Ext.create('DukeSource.view.fulfillment.window.ViewWindowViewAttachDocument', {
                                                                modal: true,
                                                                row: row,
                                                                saveFile: 'http://localhost:9000/giro/saveFileAttachAdvance.htm',
                                                                deleteFile: 'http://localhost:9000/giro/deleteFileAttachAdvance.htm',
                                                                src: 'http://localhost:9000/giro/downloadFileAttachAdvance.htm',
                                                                params: ["idFileAttachment", "idAdvanceActivity", "nameFile"]
                                                            });

                                                            var gridFileAttach = k.down('grid');

                                                            gridFileAttach.store.getProxy().extraParams = {
                                                                idAdvanceActivity: row.get('idAdvanceActivity')
                                                            };
                                                            gridFileAttach.store.getProxy().url = 'http://localhost:9000/giro/showFilesAttachAdvance.htm';
                                                            gridFileAttach.down('pagingtoolbar').moveFirst();

                                                            k.idAdvanceActivity = row.get('idAdvanceActivity');
                                                            k.show();

                                                        }
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'toolbar',
                                    dock: 'bottom',
                                    border: false,
                                    layout: {
                                        type: 'hbox',
                                        pack: 'center'
                                    },
                                    items: [
                                        {
                                            xtype: 'button',
                                            cls: 'x-btn-default-medium bo-gray',
                                            iconCls: 'logout',
                                            text: 'Salir',
                                            scale: 'medium',
                                            handler: function () {
                                                me.close();
                                            }
                                        }
                                    ]
                                }
                            ],
                            items: [
                                {
                                    xtype: 'container',
                                    layout: 'fit',
                                    items: [
                                        {
                                            xtype: 'gridpanel',
                                            name: 'gridPanelProgress',
                                            itemId: 'gridPanelProgress',
                                            title: 'Avances',
                                            store: StoreGridProgress,
                                            autoScroll: true,
                                            border: false,
                                            loadMask: true,
                                            columnLines: true,
                                            enableLocking: true,
                                            height: 350,
                                            layout: 'fit',
                                            columns: [
                                                {
                                                    xtype: 'numbercolumn',
                                                    sortable: false,
                                                    dataIndex: 'percentage',
                                                    text: 'Nivel de avance',
                                                    format: '.00',
                                                    locked: true,
                                                    renderer: function (value, metaData, record) {
                                                        var style = settingStyle(record.get('percentage'));
                                                        var icon;
                                                        if (record.get('indicatorAttachment') === 'S') {
                                                            icon = '<i class="tesla even-attachment"></i>'
                                                        } else {
                                                            icon = ''
                                                        }
                                                        return '<div style="background-color: ' + style + '; padding: 8px; width: ' + value + 'px">' + icon + value + '%</div>';
                                                    }
                                                },
                                                {
                                                    text: '',
                                                    sortable: false,
                                                    hidden: true,
                                                    align: 'center',
                                                    locked: true,
                                                    width: 35,
                                                    renderer: function (value, meta, record) {
                                                        var id = Ext.id();
                                                        Ext.defer(function () {
                                                            new Ext.container.Container({
                                                                layout: 'hbox',
                                                                idRowContainer: id,
                                                                itemId: id,
                                                                id: id,
                                                                hidden: true,
                                                                items: [
                                                                    {
                                                                        xtype: 'button',
                                                                        iconCls: 'modify',
                                                                        handler: function () {
                                                                            me.formEditProgress(me.recordActionPlan, me);
                                                                        }
                                                                    }
                                                                ]
                                                            }).render(document.body, id);
                                                        }, 50);
                                                        record.idRowContainer = id;
                                                        return Ext.String.format('<div id="{0}"></div>', id);
                                                    }
                                                },
                                                {
                                                    xtype: 'gridcolumn',
                                                    sortable: false,
                                                    header: 'Descripción',
                                                    dataIndex: 'description',
                                                    headerAlign: 'center',
                                                    width: 500
                                                },
                                                {
                                                    header: 'Fecha',
                                                    sortable: false,
                                                    dataIndex: 'dateRegister',
                                                    type: 'date',
                                                    dateFormat: 'd/m/Y',
                                                    align: 'center',
                                                    width: 100
                                                },
                                                {
                                                    xtype: 'gridcolumn',
                                                    sortable: false,
                                                    dataIndex: 'fullName',
                                                    text: 'Usuario',
                                                    align: 'center',
                                                    width: 150
                                                }
                                            ],
                                            dockedItems: [
                                                {
                                                    xtype: 'pagingtoolbar',
                                                    store: StoreGridProgress,
                                                    dock: 'bottom',
                                                    afterPageText: 'de {0}',
                                                    beforePageText: 'Página',
                                                    displayInfo: true,
                                                    border: false,
                                                    pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
                                                    displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                                                    emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                                                }
                                            ],
                                            listeners: {
                                                select: function (thisGrid, record) {
                                                    if ((record.index === 0 && isOwner) ||DukeSource.global.GiroConstants.ANALYST === category) {
                                                        me.down('panel[title="Avances"]').down('button[id="btnEditProgress"]').enable();
                                                        me.down('panel[title="Avances"]').down('button[id="btnDeleteProgress"]').enable();
                                                    } else {
                                                        me.down('panel[title="Avances"]').down('button[id="btnEditProgress"]').disable();
                                                        me.down('panel[title="Avances"]').down('button[id="btnDeleteProgress"]').disable();
                                                    }

                                                    me.down('#btnAttachmentAdvance').setVisible(true);
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            listeners: {
                afterrender: function () {
                    if (category ===DukeSource.global.GiroConstants.GESTOR) {
                        me.down('#btnAddAndEdit').setDisabled(!isOwner);
                        me.down('#progressBtnAddAndEdit').setDisabled(!isOwner);
                    }
                }
            }
        });

        me.callParent(arguments);
    }
});