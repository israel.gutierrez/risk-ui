Ext.define('DukeSource.view.fulfillment.window.ViewWindowRequestReProgramDocument', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowRequestReProgramDocument',
    width: 550,
    layout: {
        type: 'fit'
    },
    border: false,
    title: 'Solicitud de reprogramaci&oacute;n',
    titleAlign: 'center',
    initComponent: function () {
        var me = this;
        var allowBlank = this.allowBlank;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    title: '',
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'id',
                            value: 'id',
                            hidden: true
                        },
                        {
                            xtype: 'textfield',
                            name: 'idDocument',
                            itemId: 'idDocument',
                            hidden: true
                        },
                        {
                            xtype: 'datefield',
                            name: 'dateReProgram',
                            itemId: 'dateReProgram',
                            labelWidth: 120,
                            format: 'd/m/Y',
                            allowBlank: false,
                            value: new Date(),
                            msgTarget: 'side',
                            fieldCls: 'obligatoryTextField',
                            blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            fieldLabel: 'Nueva fecha l&iacute;mite'
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            labelWidth: 120,
                            name: 'description',
                            allowBlank: false,
                            height: 80,
                            msgTarget: 'side',
                            maxLength: 500,
                            fieldCls: 'obligatoryTextField',
                            blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            fieldLabel: 'Sustento'
                        },
                        {
                            xtype: 'fileuploadfield',
                            anchor: '100%',
                            labelWidth: 120,
                            fieldLabel: 'Documento',
                            buttonConfig: {
                                iconCls: 'tesla even-attachment'
                            },
                            buttonText: ''
                        },
                        {
                            xtype: 'ViewComboForeignKey',
                            fieldLabel: 'Estado',
                            anchor: '100%',
                            hidden: true,
                            labelWidth: 120,
                            allowBlank: allowBlank,
                            fieldCls: 'obligatoryTextField',
                            blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            name: 'stateQualification'
                        },
                        {
                            xtype: 'UpperCaseTextArea',
                            anchor: '100%',
                            labelWidth: 120,
                            name: 'observation',
                            allowBlank: allowBlank,
                            hidden: true,
                            height: 80,
                            msgTarget: 'side',
                            maxLength: 500,
                            fieldCls: 'obligatoryTextField',
                            blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                            fieldLabel: 'Comentarios'
                        },
                        {
                            xtype: 'checkboxfield',
                            anchor: '100%',
                            checked: true,
                            labelWidth: 120,
                            name: 'sendEmail',
                            inputValue: 'S',
                            uncheckedValue: 'N',
                            fieldLabel: 'Enviar notificaci&oacute;n'
                        }
                    ]
                }
            ],
            buttonAlign: 'center'
        });
        me.callParent(arguments);
    }
});