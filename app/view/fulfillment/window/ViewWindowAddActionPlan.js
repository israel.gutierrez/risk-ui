Ext.define("DukeSource.view.fulfillment.window.ViewWindowAddActionPlan", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowAddActionPlan",
  //    height: 165,
  width: 500,
  border: false,
  layout: {
    type: "fit"
  },
  title: "INGRESO PLAN DE ACCION",
  titleAlign: "center",
  modal: true,
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          bodyPadding: 10,
          items: [
            {
              xtype: "textfield",
              value: "S",
              name: "state",
              hidden: true
            },
            {
              xtype: "textfield",
              value: "id",
              name: "idActionPlan",
              hidden: true
            },
            {
              xtype: "ViewComboIndicatorType",
              anchor: "100%",
              name: "indicatorType",
              allowBlank: false,
              labelWidth: 130,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              fieldLabel: "TIPO INDICADOR",
              listeners: {
                render: function(cbo) {
                  cbo.getStore().load();
                }
              }
            },
            {
              xtype: "ViewComboOriginActionPlan",
              anchor: "100%",
              name: "originPlanAction",
              allowBlank: false,
              msgTarget: "side",
              labelWidth: 130,
              fieldCls: "obligatoryTextField",
              blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              fieldLabel: "ORIGEN PLAN ACCION"
            },
            {
              xtype: "UpperCaseTextArea",
              anchor: "100%",
              labelWidth: 130,
              allowBlank: false,
              name: "description",
              fieldCls: "obligatoryTextField",
              fieldLabel: "DESCRIPCION",
              height: 120
            }
          ]
        }
      ],
      buttons: [
        {
          text: "GUARDAR",
          iconCls: "save",
          handler: function(button) {
            var panel = button.up("ViewWindowAddActionPlan");
            var grid = Ext.ComponentQuery.query(
              "ViewWindowSearchActionPlan grid"
            )[0];
            var success = function(response) {
              response = Ext.decode(response.responseText);
              if (response.success) {
                this.messageAlert(
                 DukeSource.global.GiroMessages.TITLE_MESSAGE,
                  response.mensaje,
                  Ext.Msg.INFO
                );
                grid.getStore().load();
                panel.close();
              } else {
                this.messageAlert(
                 DukeSource.global.GiroMessages.TITLE_WARNING,
                  response.mensaje,
                  Ext.Msg.ERROR
                );
              }
            };
           DukeSource.global.DirtyView.saveDataToForm(
              panel,
              "http://localhost:9000/giro/saveActionPlan.htm?nameView=ViewPanelRegisterActionPlan",
              success
            );
          }
        },
        {
          text: "SALIR",
          scope: this,
          handler: this.close,
          iconCls: "logout"
        }
      ]
    });

    me.callParent(arguments);
  }
});
