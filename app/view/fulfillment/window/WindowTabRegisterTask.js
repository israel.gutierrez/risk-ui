Ext.define("DukeSource.view.fulfillment.window.WindowTabRegisterTask", {
  extend: "Ext.window.Window",
  requires: [
    "DukeSource.view.fulfillment.form.FormTask",
    "DukeSource.view.fulfillment.form.FormProgress",
    "Ext.tab.Panel",
    "Ext.tab.Tab",
    "Ext.form.Label",
    "Ext.form.Panel",
    "Ext.form.field.Date",
    "Ext.form.field.TextArea"
  ],
  width: 700,
  border: false,
  closable: true,
  layout: "fit",

  initComponent: function() {
    var me = this;
    var recordActionPlan = me.recordActionPlan;
    var isOwner = recordActionPlan.get("userReceptor") === userName;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "tabpanel",
          border: false,
          activeTab: me.activeTab || 0,
          tabBar: {
            layout: {
              pack: "center",
              align: "center"
            }
          },
          items: [
            {
              xtype: "panel",
              border: false,
              title: "Tareas",
              tabConfig: {
                xtype: "tab",
                flex: 1
              },
              items: [
                me.buttonBack,
                me.alertMessage,
                {
                  xtype: "FormTask",
                  recordActionPlan: me.recordActionPlan
                }
              ],
              listeners: {
                activate: function(tab) {
                  tab
                    .down("form")
                    .getForm()
                    .reset();
                  tab.down("#dateInitial").focus(false, 200);
                }
              }
            },
            {
              xtype: "panel",
              title: "Avances",
              border: false,
              tabConfig: {
                xtype: "tab",
                flex: 1
              },
              items: [
                me.buttonBack,
                {
                  xtype: "FormProgress",
                  recordActionPlan: me.recordActionPlan,
                  idActionPlanField: {
                    xtype: "textfield",
                    name: "idActionPlan",
                    itemId: "idActionPlan",
                    value: me.recordActionPlan.get("idDocument"),
                    hidden: true
                  }
                }
              ],
              listeners: {
                activate: function(tab) {
                  tab
                    .down("form")
                    .getForm()
                    .reset();
                  tab.down("#description").focus(false, 200);
                }
              }
            }
          ]
        }
      ],
      buttons: [
        {
          text: "Guardar",
          itemId: "btnSaveTask",
          iconCls: "save",
          scale: "medium",
          handler: function(btn) {
            var win;
            if (
              me.down("tabpanel").getActiveTab().title ===
              me.down('panel[title="Tareas"]').title
            ) {
              me.down("FormTask").postTaskForm(function() {
                win = Ext.create(
                  "DukeSource.view.fulfillment.window.WindowTabGridTask",
                  {
                    modal: true,
                    recordActionPlan: me.recordActionPlan,
                    title: btn.up("window").title
                  }
                );
                var gridTask = win.down('gridpanel[name="gridPanelTask"]');
                gridTask.store.getProxy().url =
                  "http://localhost:9000/giro/showTaskByActionPlan.htm";
                gridTask.store.getProxy().extraParams = {
                  idActionPlan: me.recordActionPlan.get("idDocument")
                };
                gridTask.down("pagingtoolbar").doRefresh();
                Ext.ComponentQuery.query("ViewPanelDocumentPending")[0]
                  .down("gridpanel")
                  .down("pagingtoolbar")
                  .doRefresh();
                me.close();
              });
            } else if (
              me.down("tabpanel").getActiveTab().title ===
              me.down('panel[title="Avances"]').title
            ) {
              me.down("FormProgress").postProgressForm(function() {
                win = Ext.create(
                  "DukeSource.view.fulfillment.window.WindowGridProgress",
                  {
                    modal: true,
                    recordActionPlan: me.recordActionPlan,
                    title: btn.up("window").title
                  }
                );

                var gridProgress = win.down("grid");
                gridProgress.store.getProxy().url =
                  "http://localhost:9000/giro/showListAdvanceActivityTask.htm";
                gridProgress.store.getProxy().extraParams = {
                  idActionPlan: me.recordActionPlan.get("idDocument")
                };
                gridProgress.down("pagingtoolbar").doRefresh();

                Ext.ComponentQuery.query("ViewPanelDocumentPending")[0]
                  .down("gridpanel")
                  .down("pagingtoolbar")
                  .doRefresh();
                win.show();
                me.close();
              });
            }
          }
        },
        {
          text: "Cancelar",
          iconCls: "cancel",
          scale: "medium",
          handler: function() {
            me.close();
          }
        }
      ],
      buttonAlign: "center",
      listeners: {
        afterrender: function() {
          if (category ===DukeSource.global.GiroConstants.GESTOR) {
            me.down("#btnSaveTask").setDisabled(!isOwner);
          }
        }
      }
    });

    me.callParent(arguments);
  }
});
