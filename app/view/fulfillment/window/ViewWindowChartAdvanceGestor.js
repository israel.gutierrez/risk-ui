Ext.define("advanceGestor", {
  extend: "Ext.data.Model",
  fields: ["user", "taskWeight", "percentage"]
});

Ext.define("DukeSource.view.fulfillment.window.ViewWindowChartAdvanceGestor", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowChartAdvanceGestor",
  height: 430,
  width: 800,
  layout: {
    type: "fit"
  },
  title: "AVANCE PLAN DE ACCI&Oacute;N",

  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "chart",
          store: Ext.create("Ext.data.Store", {
            model: "advanceGestor",
            autoLoad: true,
            proxy: {
              actionMethods: {
                create: "POST",
                read: "POST",
                update: "POST"
              },
              type: "ajax",
              url: "http://localhost:9000/giro/showListDataGraphics.htm",
              extraParams: {
                idDocument: Ext.ComponentQuery.query(
                  "ViewPanelDocumentPending grid"
                )[0]
                  .getSelectionModel()
                  .getSelection()[0]
                  .get("idDocument"),
                idDetailDocument: Ext.ComponentQuery.query(
                  "ViewPanelDocumentPending grid"
                )[0]
                  .getSelectionModel()
                  .getSelection()[0]
                  .get("idDetailDocument")
              },
              reader: {
                totalProperty: "totalCount",
                root: "data",
                successProperty: "success"
              }
            }
          }),
          width: 400,
          insetPadding: 20,
          style: "background:#fff",
          animate: true,
          theme: "Category1",
          axes: [
            {
              type: "Numeric",
              position: "left",
              fields: ["taskWeight", "percentage"],
              title: "ASIGNACION DE PLAN DE ACCION",
              grid: true
            },
            {
              type: "Category",
              position: "bottom",
              fields: ["user"],
              title: "PERSONAL RESPONSABLE"
            }
          ],
          series: [
            {
              type: "column",
              axis: "left",
              xField: "user",
              yField: "taskWeight",
              markerConfig: {
                type: "cross",
                size: 3
              }
            },
            {
              type: "scatter",
              axis: "left",
              xField: "user",
              yField: "percentage",
              tips: {
                trackMouse: true,
                width: 100,
                height: 38,
                renderer: function(storeItem, item) {
                  this.setTitle(
                    "AVANCE" +
                      "<br />" +
                      Ext.util.Format.number(
                        storeItem.get("percentage"),
                        "0,0.00"
                      ) +
                      " %"
                  );
                }
              },
              markerConfig: {
                type: "circle",
                size: 5
              }
            }
          ]
        }
      ]
    });

    me.callParent(arguments);
  }
});
