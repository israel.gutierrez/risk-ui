Ext.define('PopulationPoint', {
    extend: 'Ext.data.Model',
    fields: ['state', 'population']
});

var store = Ext.create('Ext.data.Store', {
    model: 'PopulationPoint',
    data: [{state: "Alabama", population: 4802740},
        {state: "Alaska", population: 722718},
        {state: "Arizona", population: 6482505},
        {state: "Arkansas", population: 2937979},
        {state: "California", population: 37691912},
        {state: "Colorado", population: 5116796},
        {state: "Connecticut", population: 3580709},
        {state: "Delaware", population: 907135},
        {state: "DC", population: 617996}]
});

Ext.define('DukeSource.view.fulfillment.window.ViewWindowImageChart', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowImageChart',
    height: 386,
    width: 581,
    layout: {
        type: 'fit'
    },
    title: 'IMAGEN',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'chart',
                    height: 250,
                    store: store,
                    width: 400,
                    animate: true,
                    insetPadding: 20,
                    series: [
                        {
                            type: 'pie',
                            field: 'population',
                            label: {
                                field: 'state',
                                display: 'outside',
                                font: '12px Arial'
                            }
                        }]
                }
            ]
        });

        me.callParent(arguments);
    }

});