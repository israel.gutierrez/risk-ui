Ext.define("ModelSearchUserProcess", {
  extend: "Ext.data.Model",
  fields: [
    "category",
    "checked",
    "descriptionJobPlace",
    "descriptionProcessType",
    "descriptionProcess",
    "descriptionSubProcess",
    "descriptionActivity",
    "email",
    "fullName",
    "id",
    "idActivity",
    "idProcess",
    "idProcessType",
    "idSubProcess",
    "leaf",
    "level",
    "username"
  ]
});

var StoreSearchUserProcess = Ext.create("Ext.data.Store", {
  model: "ModelSearchUserProcess",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.fulfillment.window.ViewWindowSearchUserProcess", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowSearchUserProcess",
  layout: {
    align: "stretch",
    type: "vbox"
  },
  anchorSize: 100,
  title: "Buscar usuario",
  width: 900,
  height: 500,
  border: false,
  initComponent: function() {
    var me = this;
    var category = this.category;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          height: 45,
          bodyPadding: 10,
          items: [
            {
              xtype: "container",
              anchor: "100%",
              height: 26,
              layout: {
                type: "hbox"
              },
              items: [
                {
                  xtype: "UpperCaseTextField",
                  fieldLabel: "Usuario",
                  flex: 2,
                  listeners: {
                    specialkey: function(field, e) {
                      if (e.getKey() === e.ENTER) {
                        var grid = me.down("grid");
                        grid.store.getProxy().extraParams = {
                          username: field.getValue().toUpperCase()
                        };
                        grid.store.getProxy().url =
                          "http://localhost:9000/giro/showListOwnerToProcessByUser.htm";
                        grid.down("pagingtoolbar").moveFirst();
                      }
                    },
                    afterrender: function(f, e) {
                      f.focus(false, 300);
                    }
                  }
                }
              ]
            }
          ]
        },
        {
          xtype: "container",
          flex: 3,
          layout: {
            align: "stretch",
            type: "hbox"
          },
          items: [
            {
              xtype: "gridpanel",
              padding: "2 0 0 0",
              columnLines: true,
              store: StoreSearchUserProcess,
              flex: 1,
              titleAlign: "center",
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false
                },
                {
                  dataIndex: "fullName",
                  width: 260,
                  text: "Nombre"
                },
                {
                  dataIndex: "descriptionJobPlace",
                  width: 300,
                  text: "Cargo"
                },
                {
                  header: "Proceso",
                  dataIndex: "descriptionProcessType",
                  width: 350,
                  renderer: function(value, p, r) {
                    p.tdCls = "folder-cell";

                    if (
                      r.data["idActivity"] === "" &&
                      r.data["idSubProcess"] === "" &&
                      r.data["idProcess"] === ""
                    ) {
                      return (
                        '<span style="font-weight:bold;">' +
                        r.data["descriptionProcessType"] +
                        "</span>"
                      );
                    } else if (
                      r.data["idActivity"] === "" &&
                      r.data["idSubProcess"] === ""
                    ) {
                      return (
                        '<span style="font-weight:bold;">' +
                        r.data["descriptionProcessType"] +
                        "</span>" +
                        '<span style="color:red;"> | </span>' +
                        r.data["descriptionProcess"]
                      );
                    } else if (r.data["idActivity"] === "") {
                      return (
                        '<span style="font-weight:bold;">' +
                        r.data["descriptionProcessType"] +
                        "</span>" +
                        '<span style="color:red;"> | </span>' +
                        r.data["descriptionProcess"] +
                        '<span style="color:red;"> | </span>' +
                        '<span style="color:#717171">' +
                        r.data["descriptionSubProcess"] +
                        "</span>"
                      );
                    } else {
                      return (
                        '<span style="font-weight:bold;">' +
                        r.data["descriptionProcessType"] +
                        "</span>" +
                        '<span style="color:red;"> | </span>' +
                        r.data["descriptionProcess"] +
                        '<span style="color:red;"> | </span>' +
                        r.data["descriptionSubProcess"] +
                        '<span style="color:red;"> | </span>' +
                        '<span style="color:#717171">' +
                        r.data["descriptionActivity"] +
                        "</span>"
                      );
                    }
                  }
                }
              ],
              bbar: {
                xtype: "pagingtoolbar",
                pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: StoreSearchUserProcess,
                displayInfo: true,
                displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              },
              listeners: {
                render: function(grid) {
                  grid.store.getProxy().extraParams = {};
                  grid.store.getProxy().url =
                    "http://localhost:9000/giro/loadGridDefault.htm";
                  grid.down("pagingtoolbar").moveFirst();
                }
              }
            }
          ]
        }
      ],
      buttons: [
        {
          text: "Salir",
          scale: "medium",
          iconCls: "logout",
          handler: function(btn) {
            me.close();
          }
        }
      ],
      buttonAlign: "center"
    });

    me.callParent(arguments);
  }
});
