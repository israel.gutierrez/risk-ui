Ext.define("ModelFileAttach", {
  extend: "Ext.data.Model",
  fields: ["nameFile", "userIssuing", "idDocument", "idDetailDocument"]
});

var storeFileAttach = Ext.create("Ext.data.Store", {
  model: "ModelFileAttach",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListFileAttachmentsDetail.htm",
    extraParams: {
      idDocument: Ext.ComponentQuery.query("ViewPanelDocumentPending grid")[0]
        .getSelectionModel()
        .getSelection()[0]
        .get("idDocument"),
      idDetailDocument: Ext.ComponentQuery.query(
        "ViewPanelDocumentPending grid"
      )[0]
        .getSelectionModel()
        .getSelection()[0]
        .get("idDetailDocument"),
      propertyOrder: "idDetailDocument"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.fulfillment.window.ViewWindowDocumentAttach", {
  extend: "Ext.window.Window",
  border: false,
  alias: "widget.ViewWindowDocumentAttach",
  height: 383,
  width: 589,
  layout: {
    type: "fit"
  },
  title: "DOCUMENTOS ADJUNTOS",
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "gridpanel",
          store: storeFileAttach,
          flex: 1,
          columns: [
            {
              xtype: "rownumberer",
              width: 25,
              sortable: false
            },
            {
              dataIndex: "idAgency",
              width: 60,
              text: "CODIGO"
            },
            {
              dataIndex: "userIssuing",
              flex: 1,
              text: "NOMBRE"
            },
            {
              dataIndex: "nameFile",
              flex: 1,
              text: "ARCHIVO"
            },
            {
              xtype: "actioncolumn",
              header: "DOCUMENTO",
              align: "center",
              width: 80,
              items: [
                {
                  icon: "images/page_white_put.png",
                  handler: function(grid, rowIndex) {
                    Ext.core.DomHelper.append(document.body, {
                      tag: "iframe",
                      id: "downloadIframe",
                      frameBorder: 0,
                      width: 0,
                      height: 0,
                      css: "display:none;visibility:hidden;height:0px;",
                      src:
                        "downloadDetailDocument.htm?idDocument=" +
                        grid.store.getAt(rowIndex).get("idDocument") +
                        "&idDetailDocument=" +
                        grid.store.getAt(rowIndex).get("idDetailDocument") +
                        "&nameFile=" +
                        grid.store.getAt(rowIndex).get("nameFile")
                    });
                    //                                    new Ext.Window({
                    //                                        modal:true,
                    //                                        width : 900,
                    //                                        height: 600,
                    //                                        layout : 'fit',
                    //                                        items : [
                    //                                            {
                    //                                                xtype : 'component',
                    //                                                autoEl : {
                    //                                                    tag : 'iframe',
                    //                                                    src: 'downloadDetailDocument.htm?idDocument='+grid.store.getAt(rowIndex).get('idDocument')+'&idDetailDocument='+grid.store.getAt(rowIndex).get('idDetailDocument')+'&nameFile='+grid.store.getAt(rowIndex).get('nameFile')
                    //                                                }
                    //                                            }
                    //                                        ]
                    //                                    }).show();
                  }
                }
              ]
            }
          ],
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: 50,
            store: storeFileAttach,
            items: [
              {
                xtype:"UpperCaseTrigger",
                width: 120,
                action: "searchGridAllAgency"
              }
            ]
          },
          listeners: {
            render: function() {
              var me = this;
              me.store.getProxy().extraParams = {
                idDocument: Ext.ComponentQuery.query(
                  "ViewPanelDocumentPending grid"
                )[0]
                  .getSelectionModel()
                  .getSelection()[0]
                  .get("idDocument"),
                idDetailDocument: Ext.ComponentQuery.query(
                  "ViewPanelDocumentPending grid"
                )[0]
                  .getSelectionModel()
                  .getSelection()[0]
                  .get("idDetailDocument"),
                propertyOrder: "id.idDetailDocument"
              };
              me.store.getProxy().url =
                "http://localhost:9000/giro/showListFileAttachmentsDetail.htm";
              me.down("pagingtoolbar").moveFirst();
            }
          }
        }
      ]
    });

    me.callParent(arguments);
  }
});
