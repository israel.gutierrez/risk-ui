Ext.define("ModelRoleAll", {
  extend: "Ext.data.Model",
  fields: ["id", "name"]
});
var storeRolAll = Ext.create("Ext.data.Store", {
  model: "ModelRoleAll",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListRolesActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
storeRolAll.load();

Ext.define("DukeSource.view.fulfillment.window.ViewWindowDocumentTracing", {
  extend: "Ext.window.Window",
  alias: "widget.ViewWindowDocumentTracing",
  layout: {
    align: "stretch",
    type: "vbox"
  },
  anchorSize: 100,
  title: "SEGUIMIENTO DOCUMENTO",
  width: 300,
  height: 250,
  titleAlign: "center",
  border: false,
  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "gridpanel",
          padding: "0 1 0 0",
          store: storeRolAll,
          loadMask: true,
          columnLines: true,
          flex: 1,
          multiSelect: true,
          columns: [
            {
              xtype: "rownumberer",
              width: 25,
              sortable: false
            },
            {
              dataIndex: "id",
              width: 60,
              text: "CODIGO"
            },
            {
              dataIndex: "name",
              flex: 1,
              text: "ROL"
            }
          ],
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: 50,
            store: storeRolAll,
            items: [
              {
                xtype:"UpperCaseTrigger",
                width: 120,
                action: "searchGridAllRole"
              }
            ]
          }
        }
      ],
      buttons: [
        {
          text: "CONFIRMAR",
          iconCls: "logout",
          scope: this,
          handler: this.close,
          iconCls: "save"
        }
      ]
    });

    me.callParent(arguments);
  }
});
