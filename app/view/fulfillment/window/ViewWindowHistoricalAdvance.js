Ext.define('DukeSource.view.fulfillment.window.ViewWindowHistoricalAdvance', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewWindowHistoricalAdvance',
    layout: {
        type: 'fit'
    },
    title: 'HISTORIAL DEL AVANCE DE LA TAREA',
    border: false,
    width: 800,
    height: 600,

    titleAlign: 'center',
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'ViewGridHistoricalAdvance'
                }
            ]
        });

        me.callParent(arguments);
    }

});
