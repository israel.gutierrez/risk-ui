Ext.define('DukeSource.view.fulfillment.panel.EditProgress' , {
    extend: 'Ext.panel.Panel',
    alias: 'widget.EditProgress',
    requires: [
        'Ext.form.Label',
        'Ext.form.Panel',
        'Ext.form.field.Date',
        'Ext.form.field.TextArea'
    ],
    border: false,

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'button',
                    cls: 'm-1',
                    iconCls: 'arrow_left_16',
                    handler: function () {
                        me.close();
                        Ext.getCmp('gridProgress').show();
                    }
                },
                {
                    xtype: 'component',
                    autoEl: {
                        tag: 'h3',
                        html: 'Edita el avance'
                    },
                    cls: 'c-blue p-0-1'
                },
                {
                    xtype: 'fieldset',
                    border: false,
                    activeItem: 0,
                    items: [
                        {
                            xtype: 'form',
                            layout: 'column',
                            border: false,
                            fieldDefaults: {
                                labelCls: 'changeSizeFontToEightPt',
                                fieldCls: 'changeSizeFontToEightPt'
                            },
                            items: [
                                {
                                    xtype: 'textareafield',
                                    columnWidth: 1,
                                    cls: 'm-05',
                                    fieldLabel: 'Descripción',
                                    labelAlign: 'top',
                                    grow: true,
                                    growMax: 130,
                                    labelSeparator: ' '
                                },
                                {
                                    xtype: 'datefield',
                                    columnWidth: 0.3,
                                    cls: 'm-05',
                                    fieldLabel: 'Fecha de registro',
                                    labelAlign: 'top',
                                    labelSeparator: ' '
                                },
                                {
                                    xtype: 'textfield',
                                    columnWidth: 0.4,
                                    cls: 'm-05',
                                    fieldLabel: 'Correo',
                                    labelAlign: 'top',
                                    vtype: 'email',
                                    labelSeparator: ' '
                                },
                                {
                                    xtype: 'filefield',
                                    columnWidth: 0.3,
                                    margin: '22 8',
                                    buttonText: '',
                                    fieldCls: 'readOnly-bg',
                                    buttonConfig: {
                                        iconCls: 'tesla even-attachment'
                                    },
                                    onFileChange: function() {
                                        this.lastValue = null;
                                        Ext.form.field.File.superclass.setValue.call(this, this.fileInputEl.dom.value.replace(/^.*(\\|\/|\:)/, ''));
                                    }
                                }
                            ],
                            buttons: [
                                {
                                    text: 'Guardar',
                                    cls: 'm-lr-05rem',
                                    scale: 'medium',
                                    iconCls: 'save',
                                    handler: function () {
                                        me.close();
                                        /*Ext.getCmp('WindowGridProgress').show()*/
                                        Ext.getCmp('gridProgress').show();
                                    }
                                },
                                {
                                    text: 'Cancelar',
                                    cls: 'm-lr-05rem',
                                    scale: 'medium',
                                    iconCls: 'cancel',
                                    handler: function () {
                                        me.close();
                                        /*Ext.getCmp('WindowGridProgress').show()*/
                                        Ext.getCmp('gridProgress').show();
                                    }
                                }
                            ],
                            buttonAlign: 'center'
                        }
                    ]
                }
                ]
        });

        me.callParent(arguments);
    }

});