Ext.define("ModelGridPanelDocumentPending", {
  extend: "Ext.data.Model",
  fields: [
    "idDocument",
    "idDetailDocument",
    "descriptionTypeDocument",
    "descriptionEntity",
    "description",
    "dateReception",
    "stateDocument",
    "nameStateDocument",
    "nameStateProcess",
    "descriptionPriority",
    "userEmitted",
    "fullNameEmitted",
    "emailEmitted",
    "fullNameReceptor",
    "userReceptor",
    "emailReceptor",
    "dateEnd",
    "dateAssign",
    "priority",
    "typeDocument",
    "entitySender",
    "observation",
    "category",
    "categoryManager",
    "colorByTime",
    "colorByPerformance",
    "percentage",
    "percentageMonitoring",
    "idActionPlan",
    "descriptionActionPlan",
    "jobPlace",
    "descriptionJobPlace",
    "unityClaimant",
    "descriptionUnityClaimant",
    "unity",
    "descriptionUnity",
    "dateInitPlan",
    "idProcess",
    "descriptionProcess",
    "numberAgree",
    "idAgreement",
    "codeAgreement",
    "codePlan",
    "numberRiskAssociated",
    "codesRisk",
    "numberEvent",
    "codesEvent",
    "indicatorAttachment",
    "indicatorReprogram",
    "indicatorPlan",
    "descriptionIndicatorPlan",
    "originPlan",
    "dateInvalid",
    "descriptionInvalid",
    "numberReprogramming",
    "userApprove",
    "checkApprove",
    "commentApprove",
    "codeCorrelative",
    "dateApprove",
    "idOwnerProcess",
    "idProcessType",
    "idProcess",
    "idSubProcess",
    "indicatorManagement",
    "categoryRole",
    "lineDefense",
    "module",
    "upModule"
  ]
});

var StoreGridPanelDocumentPending = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelDocumentPending",
  autoLoad: true,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      "http://localhost:9000/giro/showDefaultFilesByState.htm?nameView=ViewPanelDocumentPending",
    extraParams: {
      stateDocument: "PR"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
var statesPending = Ext.create("Ext.data.Store", {
  fields: ["id", "name"],
  data: [
    {
      id: "PR",
      name: "En proceso"
    },
    {
      id: "CU",
      name: "Culminados"
    },
    {
      id: "IN",
      name: "Inválidos"
    },
    {
      id: "%",
      name: "Todos"
    }
  ]
});

Ext.define("DukeSource.view.fulfillment.ViewPanelDocumentPending", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelDocumentPending",
  itemId: "ViewPanelDocumentPending",
  border: false,
  layout: "border",
  tbar: [
    {
      xtype: "ViewComboProcess",
      labelWidth: 70,
      width: 250,
      queryMode: "remote",
      name: "idProcess",
      hidden: true,
      itemId: "headComboProcess",
      plugins: ["ComboSelectCount"],
      blankText: DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
      labelAlign: "top",
      fieldLabel: "Filtrar por Proceso",
      listeners: {
        beforerender: function(cbo) {
          if (category !== DukeSource.global.GiroConstants.ANALYST) {
            cbo.store.proxy.url =
              "http://localhost:9000/giro/showListProcessAssigned.htm";
            cbo.store.proxy.extraParams = {
              username: userName
            };
          }
        },
        select: function(cbo) {
          var fields = "p.id";
          var values = cbo.getValue();
          var types = "Long";
          var operator = "equal";
          var grid = Ext.ComponentQuery.query(
            "ViewPanelDocumentPending grid"
          )[0];
          grid.store.getProxy().url =
            "http://localhost:9000/giro/findDetailDocument.htm";
          grid.store.getProxy().extraParams = {
            fields: fields,
            values: values,
            types: types,
            operator: operator,
            searchIn: "ActionPlanProcess",
            stateDocument: Ext.ComponentQuery.query(
              "ViewPanelDocumentPending"
            )[0]
              .down("toolbar")
              .down("#comboState")
              .getValue()
          };
          grid.down("pagingtoolbar").moveFirst();
        }
      }
    },
    {
      xtype: "combobox",
      itemId: "comboState",
      fieldLabel: "Filtrar por",
      value: DukeSource.global.GiroConstants.PROCESS,
      labelWidth: 60,
      store: statesPending,
      queryMode: "local",
      displayField: "name",
      valueField: "id",
      listeners: {
        select: function(cbo) {
          if (category === DukeSource.global.GiroConstants.ANALYST) {
            var fields = "p.id";
            var values = Ext.ComponentQuery.query("ViewPanelDocumentPending")[0]
              .down("toolbar")
              .down("ViewComboProcess")
              .getValue();
            var types = "Long";
            var operator = "equal";
            var grid = Ext.ComponentQuery.query(
              "ViewPanelDocumentPending grid"
            )[0];
            grid.store.getProxy().url =
              "http://localhost:9000/giro/showDefaultFilesByState.htm?nameView=ViewPanelDocumentPending"; //values === undefined ? 'showDefaultFilesByState.htm?nameView=ViewPanelDocumentPending' :///findDetailDocument.htm
            grid.store.getProxy().extraParams = {
              fields: fields,
              values: values,
              types: types,
              operator: operator,
              searchIn: "ActionPlan",
              stateDocument: cbo.getValue()
            };
            grid.down("pagingtoolbar").moveFirst();
          } else {
            fields = "";
            values = "";
            types = "";
            operator = "";
            grid = Ext.ComponentQuery.query("ViewPanelDocumentPending grid")[0];
            grid.store.getProxy().url =
              "http://localhost:9000/giro/findDetailDocument.htm";
            grid.store.getProxy().extraParams = {
              fields: fields,
              values: values,
              types: types,
              operator: operator,
              searchIn: "ActionPlanProcess",
              stateDocument: cbo.getValue()
            };
            grid.down("pagingtoolbar").moveFirst();
          }
        }
      }
    },
    "-",
    {
      xtype: "button",
      iconCls: "add",
      cls: "my-btn",
      overCls: "my-over",
      scale: "medium",
      text: "Nuevo",
      hidden: hidden("PLA_WRAP_BTN_NewPlan"),
      action: "newDocument"
    },
    "->",
    {
      xtype: "button",
      iconCls: "detail",
      cls: "my-btn",
      overCls: "my-over",
      scale: "medium",
      hidden: false,
      text: "Procesar planes",
      handler: function() {
        Ext.MessageBox.show({
          title: DukeSource.global.GiroMessages.TITLE_WARNING,
          msg: "Estas seguro de actualizar los estados de planes de acción?",
          icon: Ext.Msg.QUESTION,
          buttonText: {
            yes: "Si",
            no: "No"
          },
          buttons: Ext.MessageBox.YESNO,
          fn: function(btn) {
            if (btn === "yes") {
              Ext.Ajax.request({
                method: "POST",
                url: "http://localhost:9000/giro/updateStatesActionPlan.htm",
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      response.message,
                      Ext.Msg.INFO
                    );
                    var grid = Ext.ComponentQuery.query(
                      "ViewPanelDocumentPending grid"
                    )[0];
                    grid.down("pagingtoolbar").moveFirst();
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      response.message,
                      Ext.Msg.WARNING
                    );
                  }
                },
                failure: function() {}
              });
            } else {
            }
          }
        });
      }
    },
    "-",
    {
      xtype: "button",
      scale: "medium",
      cls: "my-btn",
      overCls: "my-over",
      iconCls: "auditory",
      text: "Auditoría",
      handler: function(btn) {
        var grid = btn.up("panel").down("grid");
        DukeSource.global.DirtyView.showAuditory(
          grid,
          "http://localhost:9000/giro/findAuditDocument.htm"
        );
      }
    }
  ],
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "AdvancedSearchActionPlan",
          padding: 2,
          region: "west",
          flex: 0.8,
          bodyPadding: 5,
          buttonAlign: "center",
          tbar: [
            {
              xtype: "button",
              scale: "medium",
              text: "Buscar",
              iconCls: "search",
              action: "findDetailDocument"
            },
            {
              xtype: "button",
              scale: "medium",
              text: "Limpiar",
              iconCls: "clear",
              margin: "0 5 0 0",
              handler: function() {
                me.down("AdvancedSearchActionPlan")
                  .down("form")
                  .getForm()
                  .reset();
              }
            }
          ]
        },
        {
          xtype: "gridpanel",
          store: StoreGridPanelDocumentPending,
          loadMask: true,
          columnLines: true,
          enableLocking: true,
          region: "center",
          flex: 2,
          border: false,
          style: {
            borderLeft: "1px solid #99bce8"
          },
          columns: [
            {
              xtype: "rownumberer",
              width: 25,
              sortable: false,
              locked: true
            },
            {
              header: "",
              dataIndex: "percentageMonitoring",
              tdCls: "column-nor-border-left",
              align: "left",
              locked: true,
              width: 30,
              renderer: function(value, metaData, record) {
                if (record.get("indicatorAttachment") === "S") {
                  return '<i class="tesla even-attachment"></i>';
                } else {
                  return "";
                }
              }
            },
            {
              header: "Avance del seguimiento",
              dataIndex: "percentageMonitoring",
              tdCls: "column-nor-border-left",
              align: "left",
              locked: true,
              width: 120,
              renderer: function(value, metaData, record) {
                return changeMonitoring(value, record, metaData);
              }
            },
            {
              header: "Código del plan",
              headerAlign: "center",
              align: "center",
              locked: true,
              dataIndex: "codePlan",
              width: 90
            },
            {
              text: "Actividades",
              align: "center",
              locked: true,
              width: 70,
              renderer: function(value, meta, record) {
                var id = Ext.id();
                Ext.defer(function() {
                  new Ext.container.Container({
                    layout: {
                      type: "hbox",
                      pack: "center"
                    },
                    idRowContainer: id,
                    itemId: id,
                    border: "none",
                    id: id,
                    items: [
                      {
                        xtype: "button",
                        iconCls:
                          record.get("indicatorReprogram") ===
                          DukeSource.global.GiroConstants.YES
                            ? "date_error"
                            : "date",
                        tooltip:
                          record.get("indicatorReprogram") ===
                          DukeSource.global.GiroConstants.YES
                            ? "Este plan tiene solicitudes de reprogramaci&oacute;n pendientes"
                            : "",
                        overCls: "",
                        style: {
                          borderStyle: "none",
                          backgroundImage: "none !important",
                          background: "none !important"
                        },
                        handler: function() {
                          var win;
                          if (
                            record.get("indicatorReprogram") ===
                            DukeSource.global.GiroConstants.YES
                          ) {
                            var app = DukeSource.application;
                            app
                              .getController(
                                "DukeSource.controller.fulfillment.ControllerPanelDocumentPending"
                              )
                              ._onAttendRequestReprogram(me, record);
                          } else {
                            if (record.get("indicatorManagement") === "") {
                              win = Ext.create(
                                "DukeSource.view.fulfillment.window.WindowTabRegisterTask",
                                {
                                  modal: true,
                                  title:
                                    "Plan de acción ⇾  " +
                                    '<b class="c-success">' +
                                    record.get("codePlan") +
                                    "</b>",
                                  actionPlanDescription: record.get(
                                    "description"
                                  ),
                                  recordActionPlan: record
                                }
                              );

                              win
                                .down("FormTask")
                                .validateDates(
                                  record.get("dateInitPlan"),
                                  record.get("dateEnd")
                                );
                              win
                                .down("FormProgress")
                                .validateDates(record.get("dateInitPlan"));
                              win.show();
                            } else if (
                              record.get("indicatorManagement") === "TA"
                            ) {
                              win = Ext.create(
                                "DukeSource.view.fulfillment.window.WindowTabGridTask",
                                {
                                  modal: true,
                                  recordActionPlan: record,
                                  title:
                                    "Tareas del plan de acción ⇾ " +
                                    '<b class="c-success">' +
                                    record.get("codePlan") +
                                    "</b>"
                                }
                              );
                              var gridTask = win.down(
                                'gridpanel[name="gridPanelTask"]'
                              );
                              gridTask.store.getProxy().url =
                                "http://localhost:9000/giro/showTaskByActionPlan.htm";
                              gridTask.store.getProxy().extraParams = {
                                idActionPlan: record.get("idDocument")
                              };
                              gridTask
                                .down('pagingtoolbar[name="pagingTask"]')
                                .doRefresh();
                              win.show();

                              win.down("#descriptionTask").focus(false, 100);
                            } else if (
                              record.get("indicatorManagement") === "AV"
                            ) {
                              win = Ext.create(
                                "DukeSource.view.fulfillment.window.WindowGridProgress",
                                {
                                  modal: true,
                                  recordActionPlan: record,
                                  title:
                                    "Avances del plan de acción ⇾ " +
                                    '<b class="c-success">' +
                                    record.get("codePlan") +
                                    "</b>"
                                }
                              );
                              var gridProgress = win.down("gridpanel");
                              gridProgress.store.getProxy().url =
                                "http://localhost:9000/giro/showListAdvanceActivityTask.htm";
                              gridProgress.store.getProxy().extraParams = {
                                idActionPlan: record.get("idDocument")
                              };
                              gridProgress.down("pagingtoolbar").doRefresh();
                              win.show();
                            }
                          }
                        }
                      }
                    ]
                  }).render(document.body, id);
                }, 50);
                record.idRowContainer = id;
                return Ext.String.format('<div id="{0}"></div>', id);
              }
            },
            {
              header: "Plan de acción",
              headerAlign: "center",
              dataIndex: "description",
              width: 500
            },
            {
              header: "Fecha inicio",
              align: "center",
              dataIndex: "dateInitPlan",
              type: "date",
              dateFormat: "d/m/Y",
              width: 100
            },
            {
              header: "Fecha límite",
              align: "center",
              dataIndex: "dateEnd",
              type: "date",
              dateFormat: "d/m/Y",
              width: 100
            },
            {
              header: "Involucrados",
              columns: [
                {
                  header: "Responsable",
                  dataIndex: "fullNameReceptor",
                  width: 150
                },
                {
                  header: "Avance de colaboradores",
                  align: "center",
                  hidden: true,
                  dataIndex: "percentage",
                  width: 100,
                  renderer: function(value, metaData, record) {
                    return changeMonitoring(value, record, metaData);
                  }
                },
                {
                  header: "Remitente",
                  dataIndex: "fullNameEmitted",
                  width: 150
                }
              ]
            },
            {
              header: "Area solicitante",
              dataIndex: "descriptionUnityClaimant",
              width: 220,
              renderer: function(value) {
                var s = value.split(" &#8702; ");
                var path = "";
                for (var i = 1; i < s.length; i++) {
                  var text = s[i];
                  text = s[i] + "<br>";
                  path = path + " &#8702; " + text;
                }
                return path;
              }
            },
            {
              header: "Eventos de pérdida",
              dataIndex: "codesEvent",
              width: 120,
              align: "center",
              renderer: function(value) {
                return '<span style="color:red;">' + value + "</span>";
              }
            }
          ],
          viewConfig: {},
          listeners: {
            render: function(grid) {
              if (category === DukeSource.global.GiroConstants.GESTOR) {
                var risk = Ext.create("Ext.grid.column.Column", {
                  text: "Riesgos",
                  align: "center",
                  dataIndex: "codesRisk",
                  width: 120,
                  renderer: function(a) {
                    return '<span style="color:#00AA00;">' + a + "</span>";
                  }
                });
                grid.normalGrid.headerCt.insert(5, risk);
                grid.getView().refresh();
              } else {
                var validity = Ext.create("Ext.grid.column.Column", {
                  text: "Vigencia",
                  align: "center",
                  dataIndex: "descriptionTypeDocument",
                  width: 110
                });
                var dateAssign = Ext.create("Ext.grid.column.Column", {
                  text: "Fecha asignación",
                  align: "center",
                  dataIndex: "dateAssign",
                  type: "date",
                  dateFormat: "d/m/Y",
                  width: 80
                });
                risk = Ext.create("Ext.grid.column.Column", {
                  header: "Riesgos asociados",
                  columns: [
                    {
                      header: "Riesgos",
                      align: "center",
                      dataIndex: "codesRisk",
                      width: 120,
                      renderer: function(a) {
                        return '<span style="color:#00AA00;">' + a + "</span>";
                      }
                    }
                    /*{
                                             xtype: 'actioncolumn',
                                             align: 'center',
                                             width: 45,
                                             header: 'Ir',
                                             items: [
                                             {
                                             icon: 'images/ir.png',
                                             handler: function (grid, rowIndex) {
                                             var rowRisk = grid.store.getAt(rowIndex);
                                             var panelEvaluation = Ext.ComponentQuery.query('ViewPanelEvaluationRiskOperational')[0];
                                             var panelPreviousEvaluation = Ext.ComponentQuery.query('ViewPanelPreviousIdentifyRiskOperational')[0];
                                             if (panelEvaluation !== undefined) {
                                             panelEvaluation.destroy();
                                             }
                                             if (panelPreviousEvaluation !== undefined) {
                                             panelPreviousEvaluation.destroy();
                                             }
                                             if (Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0] === undefined) {
                                            DukeSource.global.DirtyView.verifyLoadController('DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelPreviousIdentifyRiskOperational');
                                            DukeSource.global.DirtyView.verifyLoadController('DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelIdentifyRiskOperational');
                                             var k = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelIdentifyRiskOperational', {
                                             title: 'EVRO',
                                             origin: 'actionPlan',
                                             closable: true,
                                             border: false
                                             });
                                             DukeSource.getApplication().centerPanel.addPanel(k);
                                             locateRiskByActionPlan(k, rowRisk);
                                             } else {
                                             k = Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0];
                                             DukeSource.getApplication().centerPanel.addPanel(k);
                                             locateRiskByActionPlan(k, rowRisk);
                                             }
                                             }
                                             }
                                             ]
                                             }*/
                  ]
                });
                var type = Ext.create("Ext.grid.column.Column", {
                  text: "Frecuencia de monitoreo",
                  align: "center",
                  dataIndex: "descriptionIndicatorPlan",
                  width: 90
                });
                grid.normalGrid.headerCt.insert(1, validity);
                grid.normalGrid.headerCt.insert(2, dateAssign);
                grid.normalGrid.headerCt.insert(7, risk);
                grid.normalGrid.headerCt.insert(8, type);
                grid.getView().refresh();
              }
            },
            itemdblclick: function(view, record, item, index, e) {
              var app = DukeSource.application;
              app
                .getController(
                  "DukeSource.controller.fulfillment.ControllerPanelDocumentPending"
                )
                ._onModifyDocument(view);
            }
          },
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: StoreGridPanelDocumentPending,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          }
        }
      ]
    });
    me.callParent(arguments);
  }
});

function changeMonitoring(v, m, r) {
  var tmpValue = v / 100;
  var tmpText = v + "%";
  var progressRenderer = (function(pValue, pText) {
    var b = new Ext.ProgressBar();
    if (tmpValue <= 0.3334) {
      b.baseCls = "x-taskBar";
    } else if (tmpValue <= 0.6667) {
      b.baseCls = "x-taskBar-medium";
    } else {
      b.baseCls = "x-taskBar-high";
    }
    return function(pValue, pText) {
      b.updateProgress(pValue, pText, true);
      return Ext.DomHelper.markup(b.getRenderTree());
    };
  })(tmpValue, tmpText);
  return progressRenderer(tmpValue, tmpText);
}

function locateRiskByActionPlan(k, rowRisk) {
  DukeSource.getApplication().centerPanel.addPanel(k);
  var values = rowRisk.get("idDocument");
  var gridMasterPlan = k.down("grid");
  gridMasterPlan.store.getProxy().extraParams = {
    idDocument: values
  };
  gridMasterPlan.store.getProxy().url =
    "http://localhost:9000/giro/showListDocumentRiskActives.htm";
  gridMasterPlan.down("pagingtoolbar").moveFirst();
}
