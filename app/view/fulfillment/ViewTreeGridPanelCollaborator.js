Ext.define("ModelPanelDocumentSchedule", {
  extend: "Ext.data.Model",
  fields: [
    "idDocument",
    "idActionPlan",
    "idTask",
    "idDetailDocument",
    "idScheduleDocument",
    "dateInitial",
    "dateExpire",
    "percentage",
    "percentageCollaborator",
    "taskWeight",
    "descriptionTask",
    "observationTask",
    "user",
    "nameUser",
    "emailCollaborator",
    "detailDocumentObservation",
    "fullNameCollaborator",
    "detailDocumentPercentage",
    "colorByTime",
    "userIssuing",
    "fullNameEmitted",
    "fullNameReceptor",
    "emailEmitted",
    "emailReceptor",
    "emailCollaborator",
    "userLastAssignment",
    "emailUserEndAssignment",
    "descriptionTypeDocument",
    "typeDocument",
    "descriptionActionPlan",
    "codeActionPlan",
    "nameStateDocument",
    "stateDocument",
    "descriptionPriority",
    "priority",
    "category",
    "dateReception",
    "dateEnd",
    "dateAssign",
    "idRisk",
    "jobPlace",
    "descriptionJobPlace",
    "workArea",
    "state",
    "sendEmail",
    "fileAttachment",
    "nameFile",
    "nameUserInsert",
    "indicatorAttachment"
  ]
});

Ext.define("ModelGridAdvanceActivity", {
  extend: "Ext.data.Model",
  fields: [
    "idDocument",
    "idActionPlan",
    "idTaskActionPlan",
    "idAdvanceActivity",
    "nameDetailDocument",
    "nameScheduleDocument",
    "nameDocument",
    "user",
    "nameUser",
    "descriptionTask",
    "observationTask",
    "percentage",
    "description",
    "state",
    "sendEmail",
    "emailManager",
    "dateRegister",
    "dateInitial",
    "stateRevision",
    "indicatorAttachment"
  ]
});

var StoreTreePanelDocumentSchedule = Ext.create("Ext.data.Store", {
  model: "ModelPanelDocumentSchedule",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListScheduleDocumentToCollaborator.htm",
    extraParams: {
      stateDocument: DukeSource.global.GiroConstants.PENDING
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

var StoreGridAdvanceActivity = Ext.create("Ext.data.Store", {
  model: "ModelGridAdvanceActivity",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.fulfillment.ViewTreeGridPanelCollaborator", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewTreeGridPanelCollaborator",
  layout: {
    type: "border",
    padding: ""
  },
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          region: "center",
          title: "Tareas pendientes",
          itemId: "gridTask",
          titleAlign: "center",
          xtype: "gridpanel",
          cls: "x-grid-taskBar",
          store: StoreTreePanelDocumentSchedule,
          autoScroll: true,
          tbar: [
            {
              xtype: "button",
              iconCls: "attachFile",
              text: "Adjuntos",
              action: "attachFileTask"
            }
          ],
          columns: [
            {
              xtype: "rownumberer",
              width: 25,
              sortable: false
            },
            {
              header: "AD",
              align: "center",
              width: 25,
              sortable: true,
              dataIndex: "indicatorAttachment",
              renderer: function(value, metaData, record) {
                if (record.get("indicatorAttachment") == "S") {
                  return '<div><i class="tesla even-attachment"></i></div>';
                } else {
                  return "";
                }
              }
            },
            {
              header: "% Avance",
              width: 100,
              dataIndex: "percentageCollaborator",
              align: "center",
              renderer: function(v, m, r) {
                var tmpValue = v / 100;
                var tmpText = v + "%";
                var progressRenderer = (function(pValue, pText) {
                  var b = new Ext.ProgressBar();
                  if (tmpValue <= 0.3334) {
                    b.baseCls = "x-taskBar";
                  } else if (tmpValue <= 0.6667) {
                    b.baseCls = "x-taskBar-medium";
                  } else {
                    b.baseCls = "x-taskBar-high";
                  }
                  return function(pValue, pText) {
                    b.updateProgress(pValue, pText, true);
                    return Ext.DomHelper.markup(b.getRenderTree());
                  };
                })(tmpValue, tmpText);
                return progressRenderer(tmpValue, tmpText);
              }
            },
            {
              header: "Peso tarea",
              width: 60,
              dataIndex: "taskWeight",
              align: "center"
            },
            {
              header: "Tareas",
              align: "left",
              width: 500,
              sortable: true,
              dataIndex: "descriptionTask"
            },
            {
              header: "Fecha límite",
              width: 100,
              format: "d/m/Y H:i:s",
              dataIndex: "dateExpire",
              align: "center"
            },
            {
              header: "Usuario",
              align: "left",
              width: 250,
              sortable: true,
              dataIndex: "fullNameCollaborator"
            }
          ],
          bbar: {
            xtype: "pagingtoolbar",
            pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
            store: StoreTreePanelDocumentSchedule,
            displayInfo: true,
            displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
            items: [
              "-",
              {
                xtype:"UpperCaseTrigger",
                fieldLabel: "Filtrar",
                action: "filterViewGridTask",
                labelWidth: 60,
                width: 300
              }
            ],
            emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
          },
          padding: 2
        },
        {
          xtype: "panel",
          region: "east",
          flex: 1.2,
          padding: 2,
          layout: "fit",
          split: true,
          title: "Información de la tarea",
          titleAlign: "center",
          titleCollapse: false,
          dockedItems: [
            {
              xtype: "panel",
              margin: 0,
              dock: "top",
              layout: "anchor",
              bodyStyle: "background:#EEF3FA",
              items: [
                {
                  xtype: "fieldset",
                  itemId: "taskDetail",
                  layout: "anchor",
                  anchor: "100%",
                  collapsed: true,
                  checkboxToggle: true,
                  margin: "0 5",
                  title: "Información de la tarea",
                  items: [
                    {
                      xtype: "textareafield",
                      anchor: "100%",
                      height: 45,
                      name: "descriptionTask",
                      itemId: "descriptionTask",
                      padding: "5 5 0 5",
                      fieldStyle: "background:#DDE4ED;",
                      fieldLabel: "Tarea",
                      labelWidth: 60
                    },
                    {
                      xtype: "container",
                      layout: {
                        type: "hbox",
                        align: "stretch",
                        padding: "0 5 5 5"
                      },
                      items: [
                        {
                          xtype: "textfield",
                          flex: 0.5,
                          fieldLabel: "Inicio",
                          name: "dateInitial",
                          itemId: "dateInitial",
                          fieldStyle: "background:#DDE4ED;",
                          labelWidth: 60
                        },
                        {
                          xtype: "textfield",
                          flex: 0.5,
                          fieldLabel: "Fin",
                          name: "dateExpire",
                          itemId: "dateExpire",
                          labelAlign: "right",
                          fieldStyle: "background:#DDE4ED;",
                          labelWidth: 50
                        },
                        {
                          xtype: "textfield",
                          flex: 1,
                          fieldLabel: "Responsable",
                          name: "nameUser",
                          itemId: "nameUser",
                          fieldStyle: "background:#DDE4ED;",
                          labelAlign: "right"
                        }
                      ]
                    }
                  ]
                },
                {
                  xtype: "fieldset",
                  checkboxToggle: true,
                  collapsed: true,
                  margin: "5 5",
                  layout: "anchor",
                  collapsible: true,
                  title: "Información del plan de acción",
                  items: [
                    {
                      xtype: "textfield",
                      padding: "5 5 0 5",
                      fieldLabel: "Código",
                      name: "codeActionPlan",
                      itemId: "codeActionPlan",
                      fieldStyle: "background:#DDE4ED;",
                      labelWidth: 60
                    },
                    {
                      xtype: "textareafield",
                      anchor: "100%",
                      height: 45,
                      padding: "5 5 0 5",
                      name: "descriptionActionPlan",
                      itemId: "descriptionActionPlan",
                      fieldLabel: "Plan de acción",
                      fieldStyle: "background:#DDE4ED;",
                      labelWidth: 60
                    },
                    {
                      xtype: "container",
                      layout: {
                        type: "hbox",
                        align: "stretch",
                        padding: "0 5 5 5"
                      },
                      items: [
                        {
                          xtype: "textfield",
                          flex: 0.5,
                          fieldLabel: "Inicio",
                          name: "dateReception",
                          itemId: "dateReception",
                          fieldStyle: "background:#DDE4ED;",
                          labelWidth: 60
                        },
                        {
                          xtype: "textfield",
                          flex: 0.5,
                          fieldLabel: "Fin",
                          name: "dateEnd",
                          itemId: "dateEnd",
                          labelAlign: "right",
                          fieldStyle: "background:#DDE4ED;",
                          labelWidth: 50
                        },
                        {
                          xtype: "textfield",
                          flex: 1,
                          fieldLabel: "Emisor del plan",
                          name: "descriptionPriority",
                          itemId: "descriptionPriority",
                          fieldStyle: "background:#DDE4ED;",
                          labelAlign: "right"
                        }
                      ]
                    }
                  ]
                }
              ]
            },
            {
              xtype: "toolbar",
              dock: "bottom",
              items: [
                {
                  xtype: "pagingtoolbar",
                  itemId: "ptAdvanceActivity",
                  pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                  store: StoreGridAdvanceActivity,
                  displayInfo: true,
                  displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                  items: [
                    "-",
                    {
                      xtype:"UpperCaseTrigger",
                      fieldLabel: "Filtrar",
                      action: "filterViewGridAdvanceActivity",
                      labelWidth: 60,
                      width: 300
                    }
                  ],
                  emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
                }
              ]
            },
            {
              xtype: "toolbar",
              dock: "top",
              items: [
                {
                  xtype: "button",
                  iconCls: "add",
                  text: "Agregar avance",
                  action: "addAdvanceActivity"
                },
                {
                  xtype: "button",
                  iconCls: "modify",
                  text: "Modificar",
                  action: "modifyAdvanceActivity"
                },
                {
                  xtype: "button",
                  iconCls: "attachFile",
                  text: "Adjuntos",
                  action: "attachFileAdvanceActivity"
                }
              ]
            }
          ],
          items: [
            {
              xtype: "gridpanel",
              name: "gridAdvanceActivity",
              itemId: "gridAdvanceActivity",
              anchor: "100%",
              title: "Avance de las tareas",
              autoScroll: true,
              border: false,
              store: StoreGridAdvanceActivity,
              titleAlign: "center",
              columns: [
                {
                  xtype: "rownumberer",
                  width: 25,
                  sortable: false
                },
                {
                  header: "AD",
                  align: "center",
                  width: 25,
                  sortable: true,
                  dataIndex: "indicatorAttachment",
                  renderer: function(value, metaData, record) {
                    if (record.get("indicatorAttachment") == "S") {
                      return '<div><i class="tesla even-attachment"></i></div>';
                    } else {
                      return "";
                    }
                  }
                },
                {
                  header: "Nivel avance",
                  align: "center",
                  width: 100,
                  sortable: true,
                  dataIndex: "percentage",
                  renderer: function(v, m, r) {
                    var tmpValue = v / 100;
                    var tmpText = v + "%";
                    var progressRenderer = (function(pValue, pText) {
                      var b = new Ext.ProgressBar();
                      if (tmpValue <= 0.3334) {
                        b.baseCls = "x-taskBar";
                      } else if (tmpValue <= 0.6667) {
                        b.baseCls = "x-taskBar-medium";
                      } else {
                        b.baseCls = "x-taskBar-high";
                      }
                      return function(pValue, pText) {
                        b.updateProgress(pValue, pText, true);
                        return Ext.DomHelper.markup(b.getRenderTree());
                      };
                    })(tmpValue, tmpText);
                    return progressRenderer(tmpValue, tmpText);
                  }
                },
                {
                  header: "Fecha",
                  align: "center",
                  width: 100,
                  sortable: true,
                  dataIndex: "dateRegister"
                },
                {
                  header: "Avance",
                  align: "left",
                  width: 500,
                  sortable: true,
                  dataIndex: "description"
                }
              ]
            }
          ]
        }
      ]
    });
    me.callParent(arguments);
  }
});

function change(val, record, metaData) {
  if (val === 0) {
    metaData.tdAttr = 'style="border:1px solid #C7CCC0 !important;"';
    return '<div><span style="color:crimson;">' + val + "%</span></div>";
  } else if (1 <= val && val <= 33.34) {
    metaData.tdAttr =
      'style="border:1px solid #C7CCC0 !important;background-color:#FFE7E9;"';
    return '<div><span style="color:crimson;">' + val + "%</span></div>"; //style="background-color:crimson;width:' + val + '%"
  } else if (33.34 < val && val <= 66.6) {
    metaData.tdAttr =
      'style="border:1px solid #C7CCC0 !important;background-color:#F2CE3E;"';
    return (
      '<div><div style="background-color:#FFA700;width:' +
      val +
      '%"><span style="color:white;">' +
      val +
      "%</span></div></div>"
    );
  } else {
    metaData.tdAttr =
      'style="border:1px solid #C7CCC0 !important;background-color:#5BABE1;"';
    return (
      '<div><div style="background-color:#3172D7;width:' +
      val +
      '%"><span style="color:white;">' +
      val +
      "%</span></div></div>"
    );
  }
}
