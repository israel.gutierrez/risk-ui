Ext.define("DukeSource.view.fulfillment.form.FormTask", {
  extend: "Ext.container.Container",
  alias: "widget.FormTask",
  requires: [
    "Ext.form.Label",
    "Ext.form.Panel",
    "Ext.form.field.Date",
    "Ext.form.field.TextArea"
  ],
  border: false,

  initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          fileUpload: true,
          isUpload: true,
          method: "POST",
          title: "Agregar una tarea",
          cls: "panel-title-none form-task bo-none p-0-1",
          layout: "column",
          border: false,
          bodyPadding: 5,
          items: [
            {
              xtype: "textfield",
              name: "indicatorManagement",
              itemId: "indicatorManagement",
              value: "TA",
              hidden: true
            },
            {
              xtype: "textfield",
              name: "idTask",
              itemId: "idTask",
              value: "id",
              hidden: true
            },
            {
              xtype: "datefield",
              name: "dateInitial",
              itemId: "dateInitial",
              columnWidth: 0.2,
              cls: me.handlerCls || "m-05",
              allowBlank: false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              fieldLabel: "Fecha de inicio",
              labelAlign: "top",
              labelSeparator: ""
            },
            {
              xtype: "datefield",
              name: "dateExpire",
              itemId: "dateExpire",
              columnWidth: 0.2,
              cls: me.handlerCls || "m-05",
              allowBlank: false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              fieldLabel: "Fecha fin",
              labelAlign: "top",
              labelSeparator: ""
            },
            {
              xtype: "numberfield",
              name: "taskWeight",
              itemId: "taskWeight",
              columnWidth: 0.2,
              allowBlank: false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              cls: me.handlerCls || "m-05",
              fieldLabel: "Peso (%)",
              labelAlign: "top",
              hidden: true,
              maxValue: 100,
              minValue: 0,
              value: 100,
              decimalPrecision: 2,
              labelSeparator: ""
            },
            {
              xtype: "textfield",
              name: "collaborator",
              itemId: "collaborator",
              hidden: true
            },
            {
              xtype: "fieldcontainer",
              fieldLabel: "Responsable",
              labelAlign: "top",
              labelSeparator: "",
              hidden: hidden("FT_FDC_TaskOwner"),
              columnWidth: 0.4,
              layout: "hbox",
              cls: me.handlerCls || "m-05",
              items: [
                {
                  xtype: "textfield",
                  name: "fullNameCollaborator",
                  itemId: "fullNameCollaborator",
                  blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
                  allowBlank: true,
                  flex: 1
                },
                {
                  xtype: "button",
                  iconCls: "search",
                  handler: function(button) {
                    var windowMain = button.up("window");
                    var windows = Ext.create(
                      "DukeSource.view.risk.util.search.SearchUser",
                      {
                        modal: true
                      }
                    ).show();
                    windows.down("UpperCaseTextField").focus(false, 100);
                    windows
                      .down("grid")
                      .on("itemdblclick", function(view, row) {
                        windowMain
                          .down("#collaborator")
                          .setValue(row.get("userName"));
                        windowMain
                          .down("#fullNameCollaborator")
                          .setValue(row.get("fullName"));
                        windowMain
                          .down("#emailCollaborator")
                          .setValue(row.get("email"));
                        windows.close();
                      });
                  }
                }
              ]
            },
            {
              xtype: "textareafield",
              name: "descriptionTask",
              itemId: "descriptionTask",
              fieldCls: "obligatoryTextField",
              blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              allowBlank: false,
              msgTarget: "side",
              columnWidth: 1,
              cls: me.handlerCls || "m-05",
              fieldLabel: "Descripción",
              labelAlign: "top",
              grow: true,
              maxLength: 600,
              growMax: 100,
              labelSeparator: " "
            },
            {
              xtype: "filefield",
              name: "taskFile",
              itemId: "taskFile",
              fieldCls: "readOnly-bg",
              fieldLabel: "",
              columnWidth: 0.3,
              margin: me.handlerMargin || "25 8 8",
              buttonText: "",
              buttonConfig: {
                iconCls: "tesla even-attachment"
              },
              onFileChange: function() {
                this.lastValue = null;
                Ext.form.field.File.superclass.setValue.call(
                  this,
                  this.fileInputEl.dom.value.replace(/^.*([\\/:])/, "")
                );
              }
            },
            {
              xtype: "textfield",
              name: "emailCollaborator",
              hidden: hidden("FT_FDC_TaskOwner"),
              itemId: "emailCollaborator",
              columnWidth: 0.6,
              cls: me.handlerCls || "m-05",
              fieldLabel: "Envío de notificación a",
              labelAlign: "top",
              vtype: "email"
            },
            {
              xtype: "textfield",
              name: "indicatorAttachment",
              value: "N",
              hidden: true
            },
            {
              xtype: "textfield",
              name: "idActionPlan",
              itemId: "idActionPlan",
              value: me.recordActionPlan.get("idDocument"),
              hidden: true
            },
            {
              xtype: "textfield",
              name: "stateDocument",
              hidden: true
            },
            {
              xtype: "textfield",
              name: "percentageCollaborator",
              itemId: "percentageCollaborator",
              hidden: true
            },
            {
              xtype: "textfield",
              name: "percentage",
              itemId: "percentage",
              hidden: true
            }
          ]
        }
      ]
    });

    me.callParent(arguments);
  },
  validateDates: function(dateInit, dateEnd) {
    var me = this;
    me.down("#dateInitial").setMinValue(dateInit);
    me.down("#dateInitial").setMaxValue(dateEnd);
    me.down("#dateInitial").on("change", function(inp, value) {
      if (
        value > me.down("#dateExpire").getValue() &&
        me.down("#dateExpire").getValue() !== null
      ) {
        me.down("#dateExpire").setValue(value);
        me.down("#dateExpire").setMinValue(value);
      }
      me.down("#dateExpire").setMinValue(value);
    });
    me.down("#dateExpire").setMinValue(dateInit);
    me.down("#dateExpire").setMaxValue(dateEnd);
  },
  postTaskForm: function(isCorrectFunction) {
    var me = this;
    var form = me.down("form");
    if (form.getForm().isValid()) {
      form.getForm().submit({
        url: "http://localhost:9000/giro/saveTaskActionPlan.htm",
        waitMsg:DukeSource.global.GiroMessages.MESSAGE_LOADING,
        method: "POST",
        success: function(formResponse, action) {
          var response = Ext.decode(action.response.responseText);
          if (response.success) {
            isCorrectFunction();
          } else {
           DukeSource.global.DirtyView.messageWarning(response.message);
          }
        },
        failure: function(form, action) {
          var response = Ext.decode(action.response.responseText);
          if (!response.success) {
           DukeSource.global.DirtyView.messageWarning(response.message);
          }
        }
      });
    }
  }
});
