Ext.define("DukeSource.view.fulfillment.form.FormProgress", {
  extend: "Ext.container.Container",
  alias: "widget.FormProgress",
  requires: [
    "Ext.form.Label",
    "Ext.form.Panel",
    "Ext.form.field.Date",
    "Ext.form.field.TextArea",
    "Ext.ux.DateTimeField"
  ],
  border: false,
  initComponent: function() {
    var me = this;
    Ext.applyIf(me, {
      items: [
        {
          xtype: "form",
          title: "Agregar un avance",
          layout: "column",
          cls: "panel-title-none form-progress  bo-none p-0-1",
          border: false,
          height: 205,
          items: [
            me.idActionPlanField,
            me.idTaskActionPlan,
            {
              xtype: "textfield",
              name: "indicatorManagement",
              itemId: "indicatorManagement",
              value: "AV",
              hidden: true
            },
            {
              xtype: "textfield",
              name: "idAdvanceActivity",
              itemId: "idAdvanceActivity",
              value: "id",
              hidden: true
            },
            {
              xtype: "textfield",
              name: "indicatorAttachment",
              value: "N",
              hidden: true
            },
            {
              xtype: "textarea",
              name: "description",
              maxLength: 600,
              itemId: "description",
              fieldCls: "obligatoryTextField",
              blankText:DukeSource.global.GiroMessages.MESSAGE_REQUIRE,
              allowBlank: false,
              msgTarget: "side",
              columnWidth: 1,
              cls: me.handlerCls || "m-05",
              fieldLabel: "Descripción",
              labelAlign: "top",
              grow: true,
              growMax: 130,
              labelSeparator: ""
            },
            {
              xtype: "datetimefield",
              fieldLabel: "Fecha de registro",
              name: "dateRegister",
              itemId: "dateRegister",
              labelAlign: "top",
              columnWidth: 0.2,
              format: "d/m/Y H:i",
              cls: me.handlerCls || "m-05",
              allowBlank: false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              value: new Date(),
              labelSeparator: ""
            },
            {
              xtype: "numberfield",
              name: "percentage",
              itemId: "percentage",
              allowBlank: false,
              msgTarget: "side",
              fieldCls: "obligatoryTextField",
              maxValue: 100,
              minValue: 0,
              value: 0,
              decimalPrecision: 2,
              labelSeparator: "",
              fieldLabel: "Avance (%)",
              labelAlign: "top",
              columnWidth: 0.15,
              cls: me.handlerCls || "m-05"
            },
            {
              xtype: "filefield",
              name: "progressFile",
              itemId: "progressFile",
              columnWidth: 0.25,
              margin: me.handlerMargin || "25 8 8",
              buttonText: "",
              fieldCls: "readOnly-bg",
              buttonConfig: {
                iconCls: "tesla even-attachment"
              },
              onFileChange: function() {
                this.lastValue = null;
                Ext.form.field.File.superclass.setValue.call(
                  this,
                  this.fileInputEl.dom.value.replace(/^.*([\\/:])/, "")
                );
              }
            },
            {
              xtype: "textfield",
              name: "emailManager",
              hidden: true,
              itemId: "emailManager",
              columnWidth: 0.4,
              cls: me.handlerCls || "m-05",
              fieldLabel: "Correo",
              labelAlign: "top",
              vtype: "email",
              labelSeparator: " "
            }
          ]
        }
      ]
    });

    me.callParent(arguments);
  },
  validateDates: function(actionPlanInitDate) {
    var me = this;
    me.down("#dateRegister").setMinValue(actionPlanInitDate);
    me.down("#dateRegister").setMaxValue(new Date());
  },
  postProgressForm: function(isCorrectFunction) {
    var me = this;
    var form = me.down("form");
    if (form.getForm().isValid()) {
      form.getForm().submit({
        url: "http://localhost:9000/giro/saveAdvanceActivity.htm",
        waitMsg:DukeSource.global.GiroMessages.MESSAGE_LOADING,
        method: "POST",
        success: function(formResponse, action) {
          var response = Ext.decode(action.response.responseText);
          if (response.success) {
            isCorrectFunction();
          } else {
           DukeSource.global.DirtyView.messageWarning(response.message);
          }
        },
        failure: function(form, action) {
          var response = Ext.decode(action.response.responseText);
          if (!response.success) {
           DukeSource.global.DirtyView.messageWarning(response.message);
          }
        }
      });
    }
  }
});
