Ext.define("PopulationPoint", {
  extend: "Ext.data.Model",
  fields: ["DESCRIPTIONCAL", "PERCENTOFTOTAL", "NUMBERDOCUMENTS"]
});

var states = Ext.create("Ext.data.Store", {
  fields: ["id", "name"],
  data: [
    {
      id: "PE",
      name: "PENDIENTE"
    },
    {
      id: "PR",
      name: "PROCESO"
    },
    {
      id: "CU",
      name: "CULMINADO"
    }
  ]
});

Ext.define("DukeSource.view.fulfillment.ViewPanelIndicatorCharts", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewPanelIndicatorCharts",
  border: false,
  layout: "fit",
  tbar: [
    {
      text: "RECALCULAR",
      iconCls: "operation",
      xtype: "button",
      handler: function() {
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/reloadDataPerformanceCake.htm"
        });
      }
    }
  ],

  initComponent: function() {
    this.items = [
      {
        xtype: "chart",
        //                height: 250,
        store: Ext.create("Ext.data.Store", {
          model: "PopulationPoint",
          autoLoad: true,
          proxy: {
            actionMethods: {
              create: "POST",
              read: "POST",
              update: "POST"
            },
            type: "ajax",
            url:
              "http://localhost:9000/giro/showDataGraphicsPerformanceCake.htm",
            //                    extraParams:{
            //                        idDocument:Ext.ComponentQuery.query('ViewPanelDocumentPending grid')[0].getSelectionModel().getSelection()[0].get('idDocument'),
            //                        idDetailDocument:Ext.ComponentQuery.query('ViewPanelDocumentPending grid')[0].getSelectionModel().getSelection()[0].get('idDetailDocument')
            //                    },
            reader: {
              totalProperty: "totalCount",
              root: "data",
              successProperty: "success"
            }
          }
        }),
        //                width: 400,
        animate: true,
        shadow: true,
        legend: {
          position: "right"
        },
        insetPadding: 60,
        theme: "Base:gradients",
        insetPadding: 20,
        series: [
          {
            type: "pie",
            field: "PERCENTOFTOTAL",
            showInLegend: true,
            tips: {
              trackMouse: true,
              width: 180,
              height: 40,
              renderer: function(storeItem, item) {
                //calculate percentage.
                //                                var total = 0;
                //                                store.each(function (rec) {
                //                                    total += rec.get('data1');
                //                                });
                this.setTitle(
                  "Nro. DOCUMENTOS : " +
                    storeItem.get("NUMBERDOCUMENTS") +
                    "<br>PORCENTAJE : " +
                    storeItem.get("PERCENTOFTOTAL") +
                    " %"
                );
              }
            },
            highlight: {
              segment: {
                margin: 20
              }
            },
            label: {
              field: "DESCRIPTIONCAL",
              display: "rotate", //'outside',
              contrast: true,
              font: "18px Arial"
            }
          }
        ]
      }
    ];
    this.callParent();
  }
});
