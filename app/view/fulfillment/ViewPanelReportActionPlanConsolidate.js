Ext.define('DukeSource.view.fulfillment.ViewPanelReportActionPlanConsolidate', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelReportActionPlanConsolidate',
    border: false,
    layout: 'fit',

    initComponent: function () {
        var me = this;
        this.items = [
            {
                xtype: 'container',
                name: 'generalContainer',
                border: false,
                layout: {
                    align: 'stretch',
                    type: 'border'
                },
                items: [
                    {
                        xtype: 'panel',
                        padding: '2 2 2 0',
                        flex: 3,
                        name: 'panelReport',
                        region: 'center',
                        titleAlign: 'center',
                        title: 'CONSOLIDADO DE PLANES DE ACCION',
                        layout: 'fit'
                    },
                    {
                        xtype: 'panel',
                        padding: '2 0 2 2',
                        region: 'west',
                        collapseDirection: 'right',
                        collapsed: false,
                        collapsible: true,
                        split: true,
                        flex: 1.2,
                        layout: 'anchor',
                        title: 'PARAMETROS',
                        items: [
                            {
                                xtype: 'container',
                                anchor: '100%',
                                layout: {
                                    type: 'anchor'
                                },
                                padding: '5',
                                items: [
                                    {
                                        xtype: 'container',
                                        layout: {
                                            align: 'stretch',
                                            pack: 'end',
                                            padding: '10 40 10 10',
                                            type: 'hbox'
                                        },
                                        items: [
                                            {
                                                xtype: 'button',
                                                scale: 'medium',
                                                text: 'CONSOLIDADO',
                                                iconCls: 'excel',
                                                action: 'generateReportActionPlanConsolidatePdf'
                                            },
                                            {
                                                xtype: 'button',
                                                scale: 'medium',
                                                text: 'PLANES x PROCESO',
                                                iconCls: 'excel',
                                                action: 'generateReportActionPlanConsolidateXls'
                                            }
                                        ]
                                    }

                                ]
                            }
                        ]
                    }
                ]

            }
        ];
        this.callParent();
    }
});



