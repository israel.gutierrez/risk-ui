Ext.define('DukeSource.view.fulfillment.AddMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.AddMenu',
    width: 190,
    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: []
        });

        me.callParent(arguments);
    }
});