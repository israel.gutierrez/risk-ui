Ext.define('DukeSource.view.fulfillment.ViewReportActionPlanAdvancedManager', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewReportActionPlanAdvancedManager',
    border: false,
    layout: 'fit',
    initComponent: function () {
        var me = this;
        this.items = [
            {
                xtype: 'container',
                name: 'generalContainer',
                border: false,
                layout: {
                    align: 'stretch',
                    type: 'border'
                },
                items: [
                    {
                        xtype: 'panel',
                        padding: 2,
                        flex: 3,
                        name: 'panelReport',
                        region: 'center',
                        titleAlign: 'center',
                        layout: 'fit'
                    },
                    {
                        xtype: 'form',
                        padding: 2,
                        region: 'west',
                        collapseDirection: 'right',
                        collapsed: false,
                        collapsible: true,
                        split: true,
                        flex: 1.2,
                        layout: 'anchor',
                        fieldDefaults: {
                            labelCls: 'changeSizeFontToEightPt',
                            fieldCls: 'changeSizeFontToEightPt'
                        },
                        title: 'BUSQUEDA',
                        items: [
                            {
                                xtype: 'container',
                                anchor: '100%',
                                layout: {
                                    type: 'anchor'
                                },
                                padding: 5,
                                items: [
                                    {
                                        xtype: 'ViewComboTypeRiskEvaluation',
                                        flex: 1,
                                        allowBlank: false,
                                        anchor: '100%',
                                        msgTarget: 'side',
                                        multiSelect: true,
                                        fieldCls: 'obligatoryTextField',
                                        // blankText: DukeSource.view.risk.util.Messages.MESSAGE_OBLIGATORIO,
                                        fieldLabel: 'TIPO EVALUACIÓN',
                                        name: 'typeEvaluation',
                                        itemId: 'typeEvaluation'
                                    },
                                    {
                                        xtype: 'container',
                                        height: 26,
                                        layout: {
                                            type: 'hbox',
                                            align: 'stretch'
                                        },
                                        items: [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'FECHA ASIGNACION',
                                                allowBlank: true,
                                                flex: 1.8,
                                                format: 'd/m/Y',
                                                name: 'dateInit',
                                                itemId: 'dateInit',
                                                listeners: {
                                                    specialkey: function (f, e) {
                                                        DukeSource.lib.GenericProcessToView.focusEventEnter(f, e, me.down('datefield[name=dateEnd]'))
                                                    }
                                                }
                                            },
                                            {
                                                xtype: 'datefield',
                                                allowBlank: true,
                                                margin: '0 2 0 5',
                                                labelWidth: 5,
                                                flex: 1,
                                                format: 'd/m/Y',
                                                name: 'dateEnd',
                                                itemId: 'dateEnd',
                                                listeners: {
                                                    specialkey: function (f, e) {
                                                        DukeSource.lib.GenericProcessToView.focusEventEnter(f, e, me.down('combobox[name=stateIncident]'))
                                                    }
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'container',
                                        height: 26,
                                        layout: {
                                            type: 'hbox',
                                            align: 'stretch'
                                        },
                                        items: [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'FECHA INICIO',
                                                allowBlank: true,
                                                flex: 1.8,
                                                format: 'd/m/Y',
                                                name: 'dateInitInit',
                                                itemId: 'dateInitInit',
                                                listeners: {
                                                    specialkey: function (f, e) {
                                                        DukeSource.lib.GenericProcessToView.focusEventEnter(f, e, me.down('datefield[name=dateEnd]'))
                                                    }
                                                }
                                            },
                                            {
                                                xtype: 'datefield',
                                                allowBlank: true,
                                                margin: '0 2 0 5',
                                                labelWidth: 5,
                                                flex: 1,
                                                format: 'd/m/Y',
                                                name: 'dateInitEnd',
                                                itemId: 'dateInitEnd',
                                                listeners: {
                                                    specialkey: function (f, e) {
                                                        DukeSource.lib.GenericProcessToView.focusEventEnter(f, e, me.down('combobox[name=stateIncident]'))
                                                    }
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'container',
                                        height: 26,
                                        layout: {
                                            type: 'hbox',
                                            align: 'stretch'
                                        },
                                        items: [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'FECHA FIN',
                                                allowBlank: true,
                                                flex: 1.8,
                                                format: 'd/m/Y',
                                                name: 'dateEndInit',
                                                itemId: 'dateEndInit',
                                                listeners: {
                                                    specialkey: function (f, e) {
                                                        DukeSource.lib.GenericProcessToView.focusEventEnter(f, e, me.down('datefield[name=dateEnd]'))
                                                    }
                                                }
                                            },
                                            {
                                                xtype: 'datefield',
                                                allowBlank: true,
                                                margin: '0 2 0 5',
                                                labelWidth: 5,
                                                flex: 1,
                                                format: 'd/m/Y',
                                                name: 'dateEndEnd',
                                                itemId: 'dateEndEnd',
                                                listeners: {
                                                    specialkey: function (f, e) {
                                                        DukeSource.lib.GenericProcessToView.focusEventEnter(f, e, me.down('combobox[name=stateIncident]'))
                                                    }
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'container',
                                        height: 26,
                                        layout: {
                                            type: 'hbox',
                                            align: 'stretch'
                                        },
                                        items: [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'FECHA MONITOREO',
                                                allowBlank: true,
                                                flex: 1.8,
                                                format: 'd/m/Y',
                                                name: 'dateMonInit',
                                                itemId: 'dateMonInit',
                                                listeners: {
                                                    specialkey: function (f, e) {
                                                        DukeSource.lib.GenericProcessToView.focusEventEnter(f, e, me.down('datefield[name=dateEnd]'))
                                                    }
                                                }
                                            },
                                            {
                                                xtype: 'datefield',
                                                allowBlank: true,
                                                margin: '0 2 0 5',
                                                labelWidth: 5,
                                                flex: 1,
                                                format: 'd/m/Y',
                                                name: 'dateMonEnd',
                                                itemId: 'dateMonEnd',
                                                listeners: {
                                                    specialkey: function (f, e) {
                                                        DukeSource.lib.GenericProcessToView.focusEventEnter(f, e, me.down('combobox[name=stateIncident]'))
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        ],
                        tbar: [
                            '->',
                            {
                                xtype: 'button',
                                scale: 'medium',
                                text: 'GENERAR',
                                iconCls: 'excel',
                                action: 'generateReportAPlanAdvancedManagerXls'
                            },
                            {
                                xtype: 'button',
                                scale: 'medium',
                                text: 'LIMPIAR',
                                iconCls: 'clear',
                                handler: function () {
                                    me.down('form').form.reset();
                                }
                            }
                        ]
                    }
                ]

            }
        ];
        this.callParent();
    }
});