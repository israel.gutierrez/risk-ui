Ext.define('DukeSource.view.fulfillment.ViewPanelReportsActionPlan', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelReportsActionPlan',
    border: false,
    layout: 'fit',
    initComponent: function () {
        var me = this;
        this.items = [
            {
                xtype: 'container',
                name: 'generalContainer',
                border: false,
                layout: {
                    align: 'stretch',
                    type: 'border'
                },
                items: [
                    {
                        xtype: 'panel',
                        padding: '2 2 2 0',
                        flex: 3,
                        border: false,
                        itemId: 'panelReport',
                        titleAlign: 'center',
                        region: 'center',
                        layout: {
                            type: 'hbox',
                            align: 'stretch'
                        },
                        items: [
                            {
                                xtype: 'panel',
                                flex: 1,
                                title: 'Filtros',
                                itemId: 'panelFilter',
                                items: [
                                    {
                                        xtype: 'container',
                                        hidden: true,
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                name: 'nameReport',
                                                itemId: 'nameReport',
                                                hidden: true
                                            },
                                            {
                                                xtype: 'textfield',
                                                name: 'nameFile',
                                                itemId: 'nameFile',
                                                hidden: true
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'form',
                                        border: false,
                                        hidden: true,
                                        bodyPadding: 5,
                                        fieldDefaults: {
                                            labelCls: 'changeSizeFontToEightPt',
                                            fieldCls: 'changeSizeFontToEightPt'
                                        },
                                        items: [],
                                        buttons: [
                                            {
                                                text: 'Limpiar',
                                                scale: 'medium',
                                                itemId: 'cleanButton',
                                                iconCls: 'clear',
                                                handler: function () {
                                                    me.down('form').getForm().reset();
                                                }
                                            },
                                            {
                                                text: 'Generar',
                                                scale: 'medium',
                                                iconCls: 'excel',
                                                action: 'generateReportsActionPlan'
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                xtype: 'container',
                                itemId: 'containerReport',
                                flex: 3,
                                margins: '0 0 0 2',
                                style: {
                                    background: '#dde8f4',
                                    border: '#99bce8 solid 1px !important'
                                },
                                layout: 'fit',
                                items: []
                            }
                        ]
                    },
                    {
                        xtype: 'panel',
                        title: 'Reportes',
                        region: 'west',
                        width: 250,
                        height: 300,
                        collapsible: true,
                        layout: 'accordion',
                        margins: '2',
                        items: [
                            {
                                xtype: 'menu',
                                floating: false,
                                bodyStyle: 'background-color:#f3f7fb !important;',
                                items: [
                                    {
                                        text: "Consolidado de planes y riesgos",
                                        leaf: true,
                                        nameDownload: 'Consolidado_plane_riesgos',
                                        nameReport: nameReport('ConsolidateActionPlanAndRisk'),
                                        cleanButton: false,
                                        iconCls: 'application'
                                    },
                                    {
                                        text: "Consolidado de planes",
                                        leaf: true,
                                        nameDownload: 'Consolidado_planes',
                                        nameReport: nameReport('FullActionPlan'),
                                        hidden: hidden('FullActionPlan'),
                                        cleanButton: false,
                                        iconCls: 'application'
                                    },
                                    {
                                        text: "Reporte reprogramaciones",
                                        leaf: true,
                                        nameDownload: 'Planes_reprogramados',
                                        nameReport: nameReport('ActionPlanAndReprogrammed'),
                                        hidden: hidden('ActionPlanAndReprogrammed'),
                                        cleanButton: false,
                                        iconCls: 'application'
                                    }
                                ],
                                listeners: {
                                    click: function (menu, item, e) {
                                        me.down('form').getForm().reset();
                                        me.down('form').setVisible(true);
                                        me.down('#nameFile').setValue(item.nameDownload);
                                        me.down('#nameReport').setValue(item.nameReport);
                                        me.down('#cleanButton').setVisible(item.cleanButton);
                                        me.down('#panelFilter').setTitle('Filtros - ' + item.text);
                                    }
                                }
                            }
                        ]
                    }
                ]

            }
        ];
        this.callParent();
    }
});

