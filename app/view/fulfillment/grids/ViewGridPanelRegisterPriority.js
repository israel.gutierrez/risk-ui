Ext.define("DukeSource.view.fulfillment.grids.ViewGridPanelRegisterPriority", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridPanelRegisterPriority",
  store: "fulfillment.grids.StoreGridPanelRegisterPriority",
  loadMask: true,
  columnLines: true,
  bbar: {
    xtype: "pagingtoolbar",
    pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
    store: "fulfillment.grids.StoreGridPanelRegisterPriority",
    displayInfo: true,
    displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
    items: [
      "-",
      //            {text:'EXPORTAR A PDF',action:'exportPriorityPdf',iconCls:'pdf'},'-',
      //            {text:'EXPORTAR A EXCEL',action:'exportPriorityExcel',iconCls:'excel'},'-',
      {
        xtype:"UpperCaseTrigger",
        fieldLabel: "FILTRAR",
        action: "searchTriggerGridPriority",
        labelWidth: 60,
        width: 300
      }
    ],
    emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
  },
  listeners: {
    render: function() {
      var me = this;
     DukeSource.global.DirtyView.searchPaginationGridNormal(
        "",
        me,
        me.down("pagingtoolbar"),
        "http://localhost:9000/giro/findPriority.htm",
        "description",
        "description"
      );
    }
  },
  initComponent: function() {
    this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
      clicksToMoveEditor: 1,
      saveBtnText: "GUARDAR",
      cancelBtnText: "CANCELAR",
      autoCancel: false,
      completeEdit: function() {
        var me = this;
        var grid = me.grid;
        var selModel = grid.getSelectionModel();
        var record = selModel.getLastSelected();
        if (me.editing && me.validateEdit()) {
          me.editing = false;

          Ext.Ajax.request({
            method: "POST",
            url:
              "http://localhost:9000/giro/savePriority.htm?nameView=ViewPanelRegisterPriority",
            params: {
              jsonData: Ext.JSON.encode(record.data)
            },
            success: function(response) {
              response = Ext.decode(response.responseText);
              if (response.success) {
               DukeSource.global.DirtyView.messageAlert(
                 DukeSource.global.GiroMessages.TITLE_MESSAGE,
                  response.mensaje,
                  Ext.Msg.INFO
                );
               DukeSource.global.DirtyView.searchPaginationGridNormal(
                  "",
                  grid,
                  grid.down("pagingtoolbar"),
                  "http://localhost:9000/giro/findPriority.htm",
                  "description",
                  "description"
                );
              } else {
               DukeSource.global.DirtyView.messageAlert(
                 DukeSource.global.GiroMessages.TITLE_ERROR,
                  response.mensaje,
                  Ext.Msg.ERROR
                );
              }
            },
            failure: function() {}
          });
          me.fireEvent("edit", me, me.context);
        }
      }
    });
    this.columns = [
      { xtype: "rownumberer", width: 50, sortable: false },
      { header: "CODIGO", align: "center", dataIndex: "idPriority", flex: 1 },

      {
        header: "DESCRIPCION",
        dataIndex: "description",
        flex: 2,
        editor: {
          xtype: "UpperCaseTextFieldObligatory"
        }
      },
      {
        header: "ABREVIATURA",
        dataIndex: "abreviature",
        flex: 2,
        editor: {
          xtype: "UpperCaseTextFieldObligatory"
        }
      },
      { header: "ESTADO", align: "center", dataIndex: "state", flex: 1 }
    ];
    this.callParent(arguments);
  }
});
