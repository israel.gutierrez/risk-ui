Ext.define("ModelGridHistoricalAdvance", {
  extend: "Ext.data.Model",
  fields: [
    "id",
    "idAdvanceActivity",
    "idDetailDocument",
    "idDocument",
    "idScheduleDocument",
    "dateInitial",
    "nameUser",
    "descriptionStateDocument",
    "descriptionTask",
    "observationTask",
    "percentage",
    "state",
    "stateDocument",
    "stateRevision",
    "user"
  ]
});
var StoreGridHistoricalAdvance = Ext.create("Ext.data.Store", {
  model: "ModelGridHistoricalAdvance",
  autoLoad: false,
  groupField: "descriptionAgency",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.fulfillment.grids.ViewGridHistoricalAdvance", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridHistoricalAdvance",
  loadMask: true,
  columnLines: true,
  cls: "incident-grid",
  viewConfig: {
    stripeRows: true
  },
  store: StoreGridHistoricalAdvance,
  features: [
    {
      ftype: "grouping",
      id: "idGroupPendindInc",
      groupHeaderTpl:
        '{name} ({rows.length} total{[values.rows.length > 1 ? "s" : ""]})',
      hideGroupedHeader: true,
      startCollapsed: true
    }
  ],
  tbar: [],
  bbar: {
    xtype: "pagingtoolbar",
    pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
    store: StoreGridHistoricalAdvance,
    displayInfo: true,
    displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
    items: [
      "-",
      {
        iconCls: "print",
        handler: function() {
         DukeSource.global.DirtyView.printElementTogrid(
            Ext.ComponentQuery.query("ViewGridHistoricalEvent")[0]
          );
        }
      },
      "-",
      {
        xtype:"UpperCaseTrigger",
        listeners: {
          keyup: function(text) {
            DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(
              text,
              text.up("grid")
            );
          }
        },
        fieldLabel: "FILTRAR",
        labelWidth: 60,
        width: 300
      }
    ],
    emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
  },
  listeners: {},
  initComponent: function() {
    (this.columns = [
      { xtype: "rownumberer", width: 25, sortable: false },
      {
        header: "DESCRIPCI&Oacute;N",
        headerAlign: "center",
        dataIndex: "descriptionTask",
        width: 300
      },
      {
        header: "FECHA<br>REGISTRO</br>",
        align: "center",
        dataIndex: "dateInitial",
        type: "date",
        dateFormat: "d/m/Y H:i:s",
        width: 80
      },
      {
        header: "USUARIO",
        align: "center",
        dataIndex: "nameUser",
        width: 130,
        tdCls: "custom-column"
      },
      {
        header: "OBSERVACION",
        align: "center",
        dataIndex: "observationTask",
        type: "date",
        width: 180
      },
      {
        header: "AVANCE",
        align: "center",
        dataIndex: "percentage",
        width: 80,
        tdCls: "custom-column"
      },
      {
        header: "ESTADO REGISTRO",
        align: "center",
        dataIndex: "descriptionStateDocument",
        width: 130
      },
      {
        header: "CATEGORIA",
        align: "center",
        dataIndex: "stateRevision",
        width: 50
      }
    ]),
      this.callParent(arguments);
  }
});
