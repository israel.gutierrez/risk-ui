Ext.define("ModelGridPanelDocumentPending", {
  extend: "Ext.data.Model",
  fields: ["id", "name", "description", "state"]
});

var StoreGridPanelDocumentPending = Ext.create("Ext.data.Store", {
  model: "ModelGridPanelDocumentPending",
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListUserAgenciesActives.htm",
    extraParams: {
      propertyFind: "user.username",
      valueFind: "",
      propertyOrder: "idUserAgency"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
StoreGridPanelDocumentPending.load();

Ext.define("DukeSource.view.fulfillment.grids.ViewGridPanelDocumentPending", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridPanelDocumentPending",
  store: StoreGridPanelDocumentPending,
  loadMask: true,
  columnLines: true,
  bbar: {
    xtype: "pagingtoolbar",
    pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
    store: StoreGridPanelDocumentPending,
    displayInfo: true,
    displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
    items: [
      "-",
      {
        xtype:"UpperCaseTrigger",
        fieldLabel: "FILTRAR",
        action: "searchTriggerGridRole",
        labelWidth: 60,
        width: 300
      }
    ],
    emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
  },

  initComponent: function() {
    this.columns = [
      { xtype: "rownumberer", width: 25, sortable: false },
      { header: "Nro EXPEDIENTE", dataIndex: "name", flex: 3 },
      { header: "TIP.DOCUMENTO", dataIndex: "description", flex: 3 },
      { header: "ENT.REMITENTE", dataIndex: "description", flex: 3 },
      { header: "PLAN DE ACCION", dataIndex: "description", flex: 5 },
      { header: "F.RECEPCION", dataIndex: "description", flex: 3 },
      { header: "ESTADO", dataIndex: "description", flex: 3 },
      { header: "ESTADO PLAN ACC.", dataIndex: "description", flex: 3 },
      { header: "RESPONSABLE", dataIndex: "description", flex: 4 },
      { header: "F.LIMITE", dataIndex: "description", flex: 3 }
    ];

    this.callParent(arguments);
  }
});
