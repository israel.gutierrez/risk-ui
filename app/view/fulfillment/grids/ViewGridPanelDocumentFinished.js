Ext.define("DukeSource.view.fulfillment.grids.ViewGridPanelDocumentFinished", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridPanelDocumentFinished",
  store: "fulfillment.grids.StoreGridPanelDocumentFinished",
  loadMask: true,
  columnLines: true,
  bbar: {
    xtype: "pagingtoolbar",
    pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
    store: "fulfillment.grids.StoreGridPanelDocumentFinished",
    displayInfo: true,
    displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
    items: [
      "-",
      {
        xtype:"UpperCaseTrigger",
        fieldLabel: "FILTRAR",
        action: "searchTriggerGridRole",
        labelWidth: 60,
        width: 300
      }
    ],
    emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
  },
  listeners: {
    render: function() {
      var me = this;
      me.store.load();
    }
  },

  initComponent: function() {
    this.columns = [
      { xtype: "rownumberer", width: 25, sortable: false },
      { header: "", hidden: true, dataIndex: "id", width: 25 },
      {
        header: "ROL",
        dataIndex: "name",
        flex: 1,
        editor: {
          allowBlank: false,
          fieldStyle: "text-transform:uppercase"
        }
      },
      {
        header: "DESCRIPCION",
        dataIndex: "description",
        flex: 3,
        editor: {
          allowBlank: false,
          fieldStyle: "text-transform:uppercase"
        }
      },
      {
        header: "ESTADO",
        dataIndex: "state",
        flex: 1,
        editor: new Ext.form.field.ComboBox({
          typeAhead: true,
          triggerAction: "all",
          selectOnTab: true,
          allowBlank: false,
          store: [
            ["S", "SI"],
            ["N", "NO"]
          ],
          lazyRender: true,
          listClass: "x-combo-list-small"
        })
      }
    ];

    this.callParent(arguments);
  }
});
