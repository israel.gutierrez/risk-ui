Ext.define(
  "DukeSource.view.fulfillment.grids.ViewGridPanelRegisterEntitySender",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterEntitySender",
    store: "fulfillment.grids.StoreGridPanelRegisterEntitySender",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "fulfillment.grids.StoreGridPanelRegisterEntitySender",
      displayInfo: true,
      displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        //            {text: 'EXPORTAR A PDF', action: 'exportEntitySenderPdf', iconCls: 'pdf'}, '-',
        //            {text: 'EXPORTAR A EXCEL', action: 'exportEntitySenderExcel', iconCls: 'excel'}, '-',
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridEntitySender",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
       DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findEntitySender.htm",
          "description",
          "idEntitySender"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveEntitySender.htm?nameView=ViewPanelRegisterEntitySender",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                 DukeSource.global.DirtyView.messageAlert(
                   DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                 DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findEntitySender.htm",
                    "description",
                    "idEntitySender"
                  );
                } else {
                 DukeSource.global.DirtyView.messageAlert(
                   DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        {
          header: "CODIGO",
          align: "center",
          dataIndex: "idEntitySender",
          flex: 1
        },
        {
          header: "RUC",
          dataIndex: "ruc",
          align: "center",
          flex: 2,
          editor: {
            xtype: "NumberFormatObligatory",
            maxLength: 11
          }
        },
        {
          header: "DESCRIPCION",
          dataIndex: "description",
          flex: 3,
          editor: {
            xtype: "UpperCaseTextFieldObligatory"
          }
        },
        { header: "ESTADO", align: "center", dataIndex: "state", width: 60 }
      ];
      this.callParent(arguments);
    }
  }
);
