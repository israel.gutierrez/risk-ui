Ext.define("DukeSource.view.fulfillment.grids.ViewGridPanelRegisterAgreement", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridPanelRegisterAgreement",
  store: "fulfillment.grids.StoreGridPanelRegisterAgreement",
  loadMask: true,
  columnLines: true,
  bbar: {
    xtype: "pagingtoolbar",
    pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
    store: "fulfillment.grids.StoreGridPanelRegisterAgreement",
    displayInfo: true,
    displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
    items: [
      "-",
      {
        xtype:"UpperCaseTrigger",
        fieldLabel: "FILTRAR",
        action: "searchTriggerGridAgreement",
        labelWidth: 60,
        width: 300
      }
    ],
    emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
  },
  listeners: {
    render: function() {
      var me = this;
     DukeSource.global.DirtyView.searchPaginationGridNormal(
        "",
        me,
        me.down("pagingtoolbar"),
        "http://localhost:9000/giro/showListAgreementActives.htm",
        "description",
        "description"
      );
    }
  },
  initComponent: function() {
    this.columns = [
      {
        xtype: "rownumberer",
        width: 50,
        sortable: false
      },
      {
        header: "ADJ.",
        dataIndex: "indicatorAttach",
        width: 30,
        renderer: function(value, metaData, record) {
          if (record.get("indicatorAttach") === "S") {
            return '<div><i class="tesla even-attachment"></i></div>';
          } else {
            return "";
          }
        }
      },
      {
        header: "CODIGO DE COMITE",
        dataIndex: "codeCommittee",
        width: 180,
        editor: {
          xtype: "UpperCaseTextFieldObligatory",
          maxLength: 100
        }
      },
      {
        header: "CODIGO DE ACUERDO",
        dataIndex: "codeAgreement",
        width: 180,
        editor: {
          xtype: "UpperCaseTextFieldObligatory",
          maxLength: 100
        }
      },
      {
        header: "DESCRIPCION",
        dataIndex: "description",
        flex: 1,
        editor: {
          xtype: "UpperCaseTextFieldObligatory",
          maxLength: 3000
        }
      },
      {
        header: "FECHA ACUERDO",
        dataIndex: "dateAgreement",
        width: 120,
        editor: {
          xtype: "datefield",
          format: "d/m/Y"
        }
      },
      {
        header: "ESTADO",
        dataIndex: "state",
        width: 60
      }
    ];
    this.callParent(arguments);
  }
});
