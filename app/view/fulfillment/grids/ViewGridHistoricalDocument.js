Ext.define("ModelGridHistoricalDocument", {
  extend: "Ext.data.Model",
  fields: [
    "idDocument",
    "idDetailDocument",
    "descriptionTypeDocument",
    "descriptionEntity",
    "description",
    "dateReception",
    "stateDocument",
    "nameStateDocument",
    "nameStateProcess",
    "descriptionPriority",
    "userEmitted",
    "fullNameEmitted",
    "emailEmitted",
    "fullNameReceptor",
    "userReceptor",
    "emailReceptor",
    "dateEnd",
    "dateAssign",
    "priority",
    "typeDocument",
    "entitySender",
    "observation",
    "category",
    "categoryManager",
    "colorByTime",
    "colorByPerformance",
    "percentage",
    "percentageMonitoring",
    "idActionPlan",
    "descriptionActionPlan",
    "idRisk",
    "codeRisk",
    "versionCorrelative",
    "descriptionRisk",
    "descriptionVersion",
    "idWeakness",
    "codeWeakness",
    "descriptionWeakness",
    "idRiskWeaknessDetail",
    "jobPlace",
    "descriptionJobPlace",
    "employment",
    "descriptionEmployment",
    "workArea",
    "descriptionWorkArea",
    "dateInitPlan",
    "idProcess",
    "descriptionProcess",
    "numberAgree",
    "idAgreement",
    "codeAgreement",
    "codePlan",
    "numberRiskAssociated",
    "codesRisk",
    "numberEvent",
    "codesEvent",
    "indicatorAttachment",
    "indicatorReprogram",
    "indicatorPlan",
    "descriptionIndicatorPlan",
    "originPlan"
  ]
});
var StoreGridHistoricalDocument = Ext.create("Ext.data.Store", {
  model: "ModelGridHistoricalDocument",
  autoLoad: false,
  groupField: "descriptionAgency",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.htm",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});

Ext.define("DukeSource.view.fulfillment.grids.ViewGridHistoricalDocument", {
  extend: "Ext.grid.Panel",
  alias: "widget.ViewGridHistoricalDocument",
  loadMask: true,
  columnLines: true,
  cls: "incident-grid",
  viewConfig: {
    stripeRows: true
  },
  store: StoreGridHistoricalDocument,
  bbar: {
    xtype: "pagingtoolbar",
    pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
    store: StoreGridHistoricalDocument,
    displayInfo: true,
    displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
    items: [
      "-",
      {
        iconCls: "print",
        handler: function() {
         DukeSource.global.DirtyView.printElementTogrid(
            Ext.ComponentQuery.query("ViewGridHistoricalEvent")[0]
          );
        }
      },
      "-",
      {
        xtype:"UpperCaseTrigger",
        listeners: {
          keyup: function(text) {
            DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(
              text,
              text.up("grid")
            );
          }
        },
        fieldLabel: "FILTRAR",
        labelWidth: 60,
        width: 300
      }
    ],
    emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
  },
  listeners: {},
  initComponent: function() {
    this.columns = [
      { xtype: "rownumberer", width: 25, sortable: false, locked: true },
      {
        header: "C&Oacute;DIGO DEL PLAN</br><b>Y MONITOREO</b>",
        align: "left",
        dataIndex: "codePlan",
        locked: true,
        width: 150,
        renderer: function(value, metaData, record) {
          var reprogram;
          var style = settingStyle(record.get("percentageMonitoring"));
          var state;
          if (record.get("stateDocument") == "PR") {
            metaData.tdAttr = 'style="background-color:#d4ecff !important;"';
            state =
              '<div class="' +
              style +
              '">' +
              record.get("percentageMonitoring") +
              "</div>";
          } else if (
            record.get("stateDocument") == "PE" ||
            record.get("stateDocument") == "PZ"
          ) {
            metaData.tdAttr = 'style="background-color:#ffdfdf !important;"';
            state =
              '<div class="' +
              style +
              '">' +
              record.get("percentageMonitoring") +
              "</div>";
          } else if (record.get("stateDocument") == "CU") {
            metaData.tdAttr = 'style="background-color:#d7ffe5 !important;"';
            state =
              '<div class="' +
              style +
              '">' +
              record.get("percentageMonitoring") +
              "</div>";
          } else {
            metaData.tdAttr = 'style="background-color:#d8d8d8 !important;"';
            state =
              '<div class="' +
              style +
              '">' +
              record.get("percentageMonitoring") +
              "</div>";
          }

          if (record.get("indicatorReprogram") == "S") {
            reprogram =
              '<div class="tesla even-retweet" style="display: inline-block"></div>';
          } else {
            reprogram = "<div></div>";
          }

          if (record.get("indicatorAttachment") == "S") {
            return (
              '<div class="icon-attachment"><i class="tesla even-attachment"></i></div><div style="display:inline-block;position:absolute;">' +
              '<span class="text-main-icon">' +
              value +
              "</span>" +
              state +
              reprogram +
              "</div>"
            );
          } else {
            return (
              '<div class="icon-attachment"></div><div style="display:inline-block;position:absolute;">' +
              '<span class="text-main-icon">' +
              value +
              "</span>" +
              state +
              reprogram +
              "</div>"
            );
          }
        }
      },
      {
        header: "AVANCE DE<br><b>COLABORADORES</b>",
        align: "center",
        dataIndex: "percentage",
        width: 110,
        locked: true,
        renderer: function(value, metaData, record, row, col, store, gridView) {
          return changeMonitoring(value, record, metaData);
        },
        hidden: category !=DukeSource.global.GiroConstants.ANALYST
      },
      {
        header: "PLAN DE ACCI&Oacute;N",
        headerAlign: "center",
        dataIndex: "description",
        width: 500
      },
      {
        header: "VIGENCIA",
        align: "center",
        dataIndex: "descriptionTypeDocument",
        width: 110
      },
      {
        header: "FECHA<br>ASIGNACI&Oacute;N</br>",
        align: "center",
        dataIndex: "dateAssign",
        type: "date",
        dateFormat: "d/m/Y",
        width: 80
      },
      {
        header: "FECHA<br>INICIO DEL PLAN</br>",
        align: "center",
        dataIndex: "dateInitPlan",
        type: "date",
        dateFormat: "d/m/Y",
        width: 100
      },
      {
        header: "FECHA<br>L&Iacute;MITE DEL PLAN</br>",
        align: "center",
        dataIndex: "dateEnd",
        type: "date",
        dateFormat: "d/m/Y",
        width: 100
      },
      {
        header: "INVOLUCRADOS",
        columns: [
          {
            header: "RESPONSABLE",
            dataIndex: "fullNameReceptor",
            width: 150
          },
          {
            header: "REMITENTE",
            dataIndex: "fullNameEmitted",
            width: 150
          }
        ]
      },

      {
        header: "RIESGOS <br>ASOCIADOS</br>",
        columns: [
          {
            header: "RIESGOS",
            align: "center",
            dataIndex: "codesRisk",
            width: 120,
            renderer: function(a) {
              return '<span style="color:#00AA00;">' + a + "</span>";
            }
          },
          {
            xtype: "actioncolumn",
            align: "center",
            width: 45,
            header: "IR",
            hidden: category !=DukeSource.global.GiroConstants.ANALYST,
            items: [
              {
                icon: "images/ir.png",
                handler: function(grid, rowIndex) {
                  var rowRisk = grid.store.getAt(rowIndex);
                  var panelEvaluation = Ext.ComponentQuery.query(
                    "ViewPanelEvaluationRiskOperational"
                  )[0];
                  var panelPreviousEvaluation = Ext.ComponentQuery.query(
                    "ViewPanelPreviousIdentifyRiskOperational"
                  )[0];
                  if (panelEvaluation != undefined) {
                    panelEvaluation.destroy();
                  }
                  if (panelPreviousEvaluation != undefined) {
                    panelPreviousEvaluation.destroy();
                  }
                  if (
                    Ext.ComponentQuery.query(
                      "ViewPanelIdentifyRiskOperational"
                    )[0] == undefined
                  ) {
                   DukeSource.global.DirtyView.verifyLoadController(
                      "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelPreviousIdentifyRiskOperational"
                    );
                   DukeSource.global.DirtyView.verifyLoadController(
                      "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelIdentifyRiskOperational"
                    );
                    var k = Ext.create(
                      "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelIdentifyRiskOperational",
                      {
                        title: "EVRO",
                        origin: "actionPlan",
                        closable: true,
                        border: false
                      }
                    );
                    DukeSource.getApplication().centerPanel.addPanel(k);
                    locateRiskByActionPlan(k, rowRisk);
                  } else {
                    var k = Ext.ComponentQuery.query(
                      "ViewPanelIdentifyRiskOperational"
                    )[0];
                    DukeSource.getApplication().centerPanel.addPanel(k);
                    locateRiskByActionPlan(k, rowRisk);
                  }
                }
              }
            ]
          }
        ]
      },
      {
        header: "EVENTOS DE<br> PÉRDIDA",
        dataIndex: "codesEvent",
        width: 120,
        align: "center",
        renderer: function(a) {
          return '<span style="color:red;">' + a + "</span>";
        }
      },
      {
        header: "TIPO",
        align: "center",
        dataIndex: "descriptionEntity",
        width: 90
      }
    ];
    this.callParent(arguments);
  }
});

function changeMonitoring(v, m, r) {
  var tmpValue = v / 100;
  var tmpText = v + "%";
  var progressRenderer = (function(pValue, pText) {
    var b = new Ext.ProgressBar();
    if (tmpValue <= 0.3334) {
      b.baseCls = "x-taskBar";
    } else if (tmpValue <= 0.6667) {
      b.baseCls = "x-taskBar-medium";
    } else {
      b.baseCls = "x-taskBar-high";
    }
    return function(pValue, pText) {
      b.updateProgress(pValue, pText, true);
      return Ext.DomHelper.markup(b.getRenderTree());
    };
  })(tmpValue, tmpText);
  return progressRenderer(tmpValue, tmpText);
}

function settingStyle(val) {
  if (0 <= val && val <= 33.34) {
    return "buttonActionPlanRed";
  } else if (33.34 < val && val <= 66.6) {
    return "buttonActionPlanYellow";
  } else {
    return "buttonActionPlanGreen";
  }
}
