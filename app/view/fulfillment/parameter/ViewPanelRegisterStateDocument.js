Ext.define('DukeSource.view.fulfillment.parameter.ViewPanelRegisterStateDocument', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterStateDocument',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newStateDocument'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteStateDocument'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDataGridStateDocument',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'stateDocumentAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterStateDocument', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
