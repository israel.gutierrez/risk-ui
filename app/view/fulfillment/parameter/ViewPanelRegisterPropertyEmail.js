Ext.define('DukeSource.view.fulfillment.parameter.ViewPanelRegisterPropertyEmail', {
    extend: 'Ext.window.Window',
    alias: 'widget.ViewPanelRegisterPropertyEmail',
    border: true,
    layout: 'fit',
    title: 'Configuración del servidor de correo',
    tbar: [
        {
            text: 'Nuevo',
            iconCls: 'add',
            action: 'newEmail'
        }, '-',
        {
            text: 'Modificar',
            iconCls: 'modify',
            action: 'modifyEmail'
        }, '-',
        {
            text: 'Eliminar',
            iconCls: 'delete',
            action: 'deleteEmail'
        }, '-',
        {
            text: 'Test de conexión',
            iconCls: 'finish',
            action: 'testGridEmail'
        }, '-',
        '->',
        {
            text: 'Auditoría',
            action: 'propertyEmailAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'ViewGridPanelRegisterPropertyEmail',
                    border: false
                }
            ],
            buttons: [
                {
                    text: 'SALIR',
                    scope: this,
                    scale: 'medium',
                    handler: this.close,
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });
        me.callParent(arguments);
    }

});
