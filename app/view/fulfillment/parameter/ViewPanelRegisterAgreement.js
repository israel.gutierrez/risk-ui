Ext.define('DukeSource.view.fulfillment.parameter.ViewPanelRegisterAgreement', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterAgreement',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            scale: 'medium',
            cls: 'my-btn',
            overCls: 'my-over',
            handler: function () {
                Ext.create('DukeSource.view.fulfillment.window.ViewWindowAgreement', {
                    originAgreement: 'catalogAgreement',
                    modal: true
                }).show();
            }
        }, '-',
        {
            text: 'MODIFICAR',
            iconCls: 'modify',
            scale: 'medium',
            cls: 'my-btn',
            overCls: 'my-over',
            action: 'modifyAgreement'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            scale: 'medium',
            cls: 'my-btn',
            overCls: 'my-over',
            action: 'deleteAgreement'
        }, '-',
        {
            text: 'ADJUNTO',
            iconCls: 'gestionfile',
            scale: 'medium',
            cls: 'my-btn',
            overCls: 'my-over',
            handler: function () {
                var grid = Ext.ComponentQuery.query('ViewGridPanelRegisterAgreement')[0];
                var record = grid.getSelectionModel().getSelection()[0];
                if (record != undefined) {
                    var view = Ext.create('DukeSource.view.fulfillment.window.ViewWindowAgreementFileAttachment', {
                        idAgreement: record.get('idAgreement'),
                        modal: true
                    });
                    view.title = 'ADJUNTAR DOCUMENTO DE ACUERDO - ' + record.get('idAgreement');
                    view.down('#agreement').setValue(record.get('idAgreement'));

                    view.show();
                } else {
                   DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, 'Seleccione un REGISTRO por favor', Ext.Msg.ERROR);
                }
            }
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchAgreement',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            hidden: true,
            action: 'agreementAuditory',
            scale: 'medium',
            cls: 'my-btn',
            overCls: 'my-over',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterAgreement', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
