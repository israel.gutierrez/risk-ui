Ext.require(["Ext.ux.ColorField"]);
Ext.define(
  "DukeSource.view.fulfillment.parameter.grids.ViewGridPanelRegisterParameterPerformantsTime",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterParameterPerformantsTime",
    store: "fulfillment.grids.StoreGridPanelRegisterParameterPerformantsTime",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "fulfillment.grids.StoreGridPanelRegisterParameterPerformantsTime",
      displayInfo: true,
      displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        //            {text: 'EXPORTAR A PDF', action: 'exportParameterPerformantsTimePdf', iconCls: 'pdf'}, '-',
        //            {text: 'EXPORTAR A EXCEL', action: 'exportParameterPerformantsTimeExcel', iconCls: 'excel'}, '-',
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridParameterPerformantsTime",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
       DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/findParameterPerformantsTime.htm",
          "description",
          "description"
        );
      }
    },
    initComponent: function() {
      this.plugins = Ext.create("Ext.grid.plugin.RowEditing", {
        clicksToMoveEditor: 1,
        saveBtnText: "GUARDAR",
        cancelBtnText: "CANCELAR",
        autoCancel: false,
        completeEdit: function() {
          var me = this;
          var grid = me.grid;
          var selModel = grid.getSelectionModel();
          var record = selModel.getLastSelected();
          if (me.editing && me.validateEdit()) {
            me.editing = false;

            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/saveParameterPerformantsTime.htm?nameView=ViewPanelRegisterParameterPerformantsTime",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                 DukeSource.global.DirtyView.messageAlert(
                   DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                 DukeSource.global.DirtyView.searchPaginationGridNormal(
                    "",
                    grid,
                    grid.down("pagingtoolbar"),
                    "http://localhost:9000/giro/findParameterPerformantsTime.htm",
                    "description",
                    "description"
                  );
                } else {
                 DukeSource.global.DirtyView.messageAlert(
                   DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            me.fireEvent("edit", me, me.context);
          }
        }
      });
      this.columns = [
        { xtype: "rownumberer", width: 50, sortable: false },
        {
          header: "CODIGO",
          align: "center",
          dataIndex: "idParameterPerformantsTime",
          flex: 1
        },
        {
          header: "DESCRIPCION",
          dataIndex: "description",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextFieldObligatory",
            allowBlank: false
          }
        },
        {
          header: "PORCENTAGE DEL <br>TOTAL DE DIAS",
          align: "center",
          dataIndex: "percentByDays",
          flex: 1,
          editor: {
            xtype: "NumberFormatObligatory",
            allowBlank: false
          }
        },
        {
          header: "COLOR DE DIAS",
          align: "center",
          dataIndex: "hexadecimalColorDays",
          flex: 1,
          editor: {
            xtype: "colorfield",
            allowBlank: false
          },
          renderer: function(value) {
            return (
              '<div  style="background-color:#' +
              value +
              "; color:#" +
              value +
              ';"> ' +
              value +
              "</div>"
            );
          }
        },
        {
          header: "DESCRIPCION DE <br>RENDIMIENTO",
          dataIndex: "descriptionByPerformance",
          flex: 1,
          editor: {
            xtype: "UpperCaseTextFieldObligatory"
          }
        },
        {
          header: "NUMERO DE <br>PORCENTAJE INICIAL",
          align: "center",
          dataIndex: "numberInitialPercentage",
          flex: 1,
          editor: {
            xtype: "NumberFormatObligatory"
          }
        },
        {
          header: "NUMERO DE <br>PORCENTAJE FINAL",
          align: "center",
          dataIndex: "numberFinalPercentage",
          flex: 1,
          editor: {
            xtype: "NumberFormatObligatory"
          }
        },
        {
          header: "COLOR RENDIMIENTO",
          align: "center",
          dataIndex: "hexadecimalColorPerformance",
          flex: 1,
          editor: {
            xtype: "colorfield",
            allowBlank: false
          },
          renderer: function(value) {
            return (
              '<div  style="background-color:#' +
              value +
              "; color:#" +
              value +
              ';"> ' +
              value +
              "</div>"
            );
          }
        },
        { header: "ESTADO", align: "center", dataIndex: "state", width: 60 }
      ];
      this.callParent(arguments);
    }
  }
);
