Ext.define(
  "DukeSource.view.fulfillment.parameter.grids.ViewGridPanelRegisterPropertyEmail",
  {
    extend: "Ext.grid.Panel",
    alias: "widget.ViewGridPanelRegisterPropertyEmail",
    store: "fulfillment.grids.StoreGridPanelRegisterPropertyEmail",
    loadMask: true,
    columnLines: true,
    bbar: {
      xtype: "pagingtoolbar",
      pageSize:DukeSource.global.GiroConstants.ITEMS_PAGE,
      store: "fulfillment.grids.StoreGridPanelRegisterPropertyEmail",
      displayInfo: true,
      displayMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
      items: [
        "-",
        {
          xtype:"UpperCaseTrigger",
          fieldLabel: "FILTRAR",
          action: "searchTriggerGridPropertyEmail",
          labelWidth: 60,
          width: 300
        }
      ],
      emptyMsg:DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
    },
    listeners: {
      render: function() {
        var me = this;
       DukeSource.global.DirtyView.searchPaginationGridNormal(
          "",
          me,
          me.down("pagingtoolbar"),
          "http://localhost:9000/giro/showListPropertyEmailActives.htm",
          "state",
          "state"
        );
      }
    },
    initComponent: function() {
      this.columns = [
        {
          xtype: "rownumberer",
          width: 40,
          sortable: false
        },
        {
          header: "Código",
          dataIndex: "id",
          hidden: true,
          width: 80
        },
        {
          header: "Cuenta de correo",
          dataIndex: "emailAccount",
          width: 200
        },
        {
          header: "Usuario",
          dataIndex: "userName",
          width: 200
        },
        {
          header: "Password",
          dataIndex: "password",
          width: 150,
          renderer: function(val) {
            var toReturn = "";
            for (var x = 0; x < val.length; x++) {
              toReturn += "&#x25cf;";
            }
            return toReturn;
          }
        },
        {
          header: "Servidor smtp",
          dataIndex: "host",
          width: 200
        },
        {
          header: "Puerto",
          dataIndex: "port",
          width: 100
        },
        {
          header: "Permite ttls",
          dataIndex: "permitTTLS",
          width: 150
        },
        {
          header: "Correo destino",
          dataIndex: "to",
          width: 200
        }
        /*
            {
                header: 'AUTHORIZACION', dataIndex: 'authorization', width: 150
            },
            {
                header: 'PERMITE FALLBACK', dataIndex: 'fallBack', width: 120
            }*/
      ];
      this.callParent(arguments);
    }
  }
);
