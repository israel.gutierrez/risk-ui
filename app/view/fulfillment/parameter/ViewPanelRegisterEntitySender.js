Ext.define('DukeSource.view.fulfillment.parameter.ViewPanelRegisterEntitySender', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterEntitySender',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newEntitySender'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteEntitySender'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDataGridEntitySender',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'entitySenderAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterEntitySender', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
