Ext.define('DukeSource.view.fulfillment.parameter.ViewPanelRegisterPriority', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterPriority',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newPriority'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deletePriority'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDataGridPriority',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'priorityAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterPriority', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
