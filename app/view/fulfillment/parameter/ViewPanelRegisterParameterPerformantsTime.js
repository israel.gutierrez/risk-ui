Ext.define('DukeSource.view.fulfillment.parameter.ViewPanelRegisterParameterPerformantsTime', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterParameterPerformantsTime',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newParameterPerformantsTime'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteParameterPerformantsTime'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDataGridParameterPerformantsTime',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'parameterPerformantsTimeAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterParameterPerformantsTime', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
