Ext.define('DukeSource.view.fulfillment.parameter.ViewPanelRegisterTypeDocument', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewPanelRegisterTypeDocument',
    border: false,
    layout: 'fit',
    tbar: [
        {
            text: 'NUEVO',
            iconCls: 'add',
            action: 'newTypeDocument'
        }, '-',
        {
            text: 'ELIMINAR',
            iconCls: 'delete',
            action: 'deleteTypeDocument'
        }, '-',
        {
            xtype: 'UpperCaseTextField',
            action: 'searchDataGridTypeDocument',
            fieldLabel: 'BUSCAR',
            labelWidth: 60,
            width: 300
        }, '-',
        '->',
        {
            text: 'AUDITORIA',
            action: 'typeDocumentAuditory',
            iconCls: 'auditory'
        }
    ],
    initComponent: function () {
        this.items = [
            {xtype: 'ViewGridPanelRegisterTypeDocument', padding: '2 2 2 2'}
        ];
        this.callParent();
    }
});
