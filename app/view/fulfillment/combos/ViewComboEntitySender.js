Ext.define("ModelComboEntitySender", {
  extend: "Ext.data.Model",
  fields: ["idEntitySender", "description"]
});

var StoreComboEntitySender = Ext.create("Ext.data.Store", {
  model: "ModelComboEntitySender",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListEntitySenderActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
//StoreComboEntitySender.load();
Ext.define("DukeSource.view.fulfillment.combos.ViewComboEntitySender", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboEntitySender",
  queryMode: "local",
  displayField: "description",
  valueField: "idEntitySender",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: StoreComboEntitySender
});
