Ext.define("ModelComboPriority", {
  extend: "Ext.data.Model",
  fields: ["idPriority", "description"]
});

var StoreComboPriority = Ext.create("Ext.data.Store", {
  model: "ModelComboPriority",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListPriorityActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
Ext.define("DukeSource.view.fulfillment.combos.ViewComboPriority", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboPriority",
  queryMode: "remote",
  displayField: "description",
  valueField: "idPriority",

  editable: true,
  forceSelection: true,
  typeAhead: true,
  store: StoreComboPriority
});
