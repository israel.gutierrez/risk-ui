Ext.define('DukeSource.view.fulfillment.combos.ViewComboParameterPerformantsTime', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboParameterPerformantsTime',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idParameterPerformantsTime',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'fulfillment.combos.StoreComboParameterPerformantsTime'
});
