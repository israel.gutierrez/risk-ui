Ext.define("ModelComboTypeDocument", {
  extend: "Ext.data.Model",
  fields: ["idTypeDocument", "description"]
});

var StoreComboTypeDocument = Ext.create("Ext.data.Store", {
  model: "ModelComboTypeDocument",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListTypeDocumentActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
//StoreComboTypeDocument.load();

Ext.define("DukeSource.view.fulfillment.combos.ViewComboTypeDocument", {
  extend: "Ext.form.ComboBox",
  alias: "widget.ViewComboTypeDocument",
  queryMode: "local",
  displayField: "description",
  valueField: "idTypeDocument",

  editable: true,
  forceSelection: true,
  initComponent: function() {
    this.on("render", this.handleFieldRender, this);
    this.callParent(arguments);
  },
  handleFieldRender: function() {
    var me = this;
    me.store.load();
  },
  store: StoreComboTypeDocument
});
