Ext.define('DukeSource.view.fulfillment.combos.ViewComboStateDocument', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.ViewComboStateDocument',
    queryMode: 'local',
    displayField: 'description',
    valueField: 'idStateDocument',

    editable: true,
    forceSelection: true,
    initComponent: function () {
        this.on('render', this.handleFieldRender, this);
        this.callParent(arguments);
    },
    handleFieldRender: function () {
        var me = this;
        me.store.load();
    },
    store: 'fulfillment.parameter.combos.StoreComboStateDocument'
});
