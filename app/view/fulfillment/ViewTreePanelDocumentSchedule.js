Ext.define("ModelTreePanelDocumentSchedule", {
  extend: "Ext.data.Model",
  fields: [
    { name: "id", type: "string" },
    { name: "text", type: "string" },
    { name: "fieldOne", type: "string" },
    { name: "fieldTwo", type: "string" },
    { name: "fieldThree", type: "string" },
    { name: "fieldFour", type: "string" },
    { name: "fieldFive", type: "string" },
    { name: "fieldSix", type: "string" }
  ]
});

var StoreTreePanelDocumentSchedule = Ext.create("Ext.data.TreeStore", {
  model: "ModelTreePanelDocumentSchedule",
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListScheduleDocument.htm",
    extraParams: {
      stateDocument: DukeSource.global.GiroConstants.PENDING
    }
  },
  root: {
    text: "Tree display of Countries",
    id: "myTree",
    expanded: true
  },
  folderSort: true,
  listeners: {
    load: function(tree, node, records) {
      if (node.get("checked")) {
        node.eachChild(function(childNode) {
          childNode.set("checked", true);
        });
      }
    }
  }
});

Ext.define("DukeSource.view.fulfillment.ViewTreePanelDocumentSchedule", {
  extend: "Ext.panel.Panel",
  alias: "widget.ViewTreePanelDocumentSchedule",
  border: false,
  layout: "fit",
  tbar: [],
  initComponent: function() {
    var me = this;
    this.items = [
      {
        xtype: "treepanel",
        store: StoreTreePanelDocumentSchedule,
        useArrows: true,
        multiSelect: true,
        singleExpand: false,
        plugins: [Ext.create("Ext.grid.plugin.CellEditing")],
        rootVisible: false,
        columns: [
          {
            xtype: "treecolumn",
            text: "PLAN DE ACCION -> TAREAS -> AVANCES ",
            flex: 4,
            sortable: true,
            dataIndex: "text"
          },
          {
            text: "RESPONSABLE",
            flex: 1,
            sortable: true,
            dataIndex: "fieldOne",
            align: "center"
          },
          {
            text: "FECHA REGISTRO",
            flex: 1,
            sortable: true,
            dataIndex: "fieldTwo",
            align: "center"
          },
          {
            text: "FECHA LIMITE",
            flex: 1,
            sortable: true,
            dataIndex: "fieldThree",
            align: "center"
          },
          {
            text: "% IMPORTANCIA",
            flex: 1,
            sortable: true,
            dataIndex: "fieldSix",
            align: "center"
          },
          {
            text: "% AVANCE",
            flex: 1,
            sortable: true,
            dataIndex: "fieldFour",
            align: "center"
          }
        ],
        padding: "2 2 2 2",
        bbar: [
          {
            xtype: "button",
            text: "Actualizar",
            iconCls: "operations",
            handler: function() {
              me.down("treepanel").store.load(
                me.down("treepanel").store.tree.getRootNode()
              );
            }
          }
        ]
      }
    ];
    this.callParent();
  }
});

function changeField(val, record, metaData) {
  if (record.get("stateRevision") == "P") {
    metaData.tdAttr = 'style="background-color: #65B9FF !important;"';
    return val;
  } else {
    metaData.tdAttr = 'style="background-color: #FFD242 !important;"';
    return val;
  }
}
