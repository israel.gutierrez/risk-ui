Ext.define('DukeSource.view.fulfillment.ViewReportActionPlanGradeAdvanced', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.ViewReportActionPlanGradeAdvanced',
    border: false,
    layout: 'fit',
    initComponent: function () {
        var me = this;
        this.items = [
            {
                xtype: 'container',
                name: 'generalContainer',
                border: false,
                layout: {
                    align: 'stretch',
                    type: 'border'
                },
                items: [
                    {
                        xtype: 'panel',
                        padding: 2,
                        flex: 3,
                        name: 'panelReport',
                        region: 'center',
                        titleAlign: 'center',
                        layout: 'fit'
                    },
                    {
                        xtype: 'form',
                        padding: 2,
                        region: 'west',
                        collapseDirection: 'right',
                        collapsed: false,
                        collapsible: true,
                        split: true,
                        flex: 1.2,
                        layout: 'anchor',
                        fieldDefaults: {
                            labelCls: 'changeSizeFontToEightPt',
                            fieldCls: 'changeSizeFontToEightPt'
                        },
                        title: 'BUSQUEDA',
                        items: [
                            {
                                xtype: 'container',
                                anchor: '100%',
                                layout: {
                                    type: 'anchor'
                                },
                                padding: 5,
                                items: [
                                    {
                                        xtype: 'container',
                                        height: 26,
                                        layout: {
                                            type: 'hbox',
                                            align: 'stretch'
                                        },
                                        items: [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'FECHA REGISTRO',
                                                allowBlank: false,
                                                fieldCls: 'obligatoryTextField',
                                                // blankText: DukeSource.view.risk.util.Messages.MESSAGE_OBLIGATORIO,
                                                flex: 1.8,
                                                format: 'd/m/Y',
                                                name: 'dateInit',
                                                listeners: {
                                                    specialkey: function (f, e) {
                                                        DukeSource.lib.GenericProcessToView.focusEventEnter(f, e, me.down('datefield[name=dateEnd]'))
                                                    }
                                                }
                                            },
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'A',
                                                allowBlank: false,
                                                margin: '0 2 0 5',
                                                fieldCls: 'obligatoryTextField',
                                                // blankText: DukeSource.view.risk.util.Messages.MESSAGE_OBLIGATORIO,
                                                labelWidth: 10,
                                                flex: 1,
                                                format: 'd/m/Y',
                                                name: 'dateEnd',
                                                listeners: {
                                                    specialkey: function (f, e) {
                                                        //DukeSource.lib.GenericProcessToView.focusEventEnter(f, e, me.down('combobox[name=stateIncident]'))
                                                    }
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        xtype: 'ViewComboTypeRiskEvaluation',
                                        flex: 1,
                                        allowBlank: false,
                                        anchor: '100%',
                                        msgTarget: 'side',
                                        multiSelect: true,
                                        fieldCls: 'obligatoryTextField',
                                        // blankText: DukeSource.view.risk.util.Messages.MESSAGE_OBLIGATORIO,
                                        fieldLabel: 'TIPO EVALUACIÓN',
                                        name: 'typeRiskEvaluation'
                                    },
                                    {
                                        xtype: 'ViewComboProcess',
                                        queryMode: 'remote',
                                        fieldLabel: 'PROCESO',
                                        emptyText: 'Seleccionar',
                                        forceSelection: false,
                                        anchor: '100%',
                                        name: 'process'
                                    }
                                ]
                            }
                        ],
                        tbar: [
                            {
                                xtype: 'button',
                                scale: 'medium',
                                text: 'GENERAR',
                                iconCls: 'excel',
                                action: 'generateReportActionPlanAdvancedXls'
                            }
                        ]
                    }
                ]

            }
        ];
        this.callParent();
    }
});