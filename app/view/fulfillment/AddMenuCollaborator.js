Ext.define('DukeSource.view.fulfillment.AddMenuCollaborator', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.AddMenuCollaborator',
    width: 180,
    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {});
        me.callParent(arguments);
    }
});