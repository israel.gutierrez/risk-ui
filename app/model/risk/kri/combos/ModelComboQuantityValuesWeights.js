Ext.define('DukeSource.model.risk.kri.combos.ModelComboQuantityValuesWeights', {
    extend: 'Ext.data.Model',
    fields: [
        'idQuantityValuesWeights',
        'description'
    ]
});
