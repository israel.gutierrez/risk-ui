Ext.define('DukeSource.model.risk.kri.combos.ModelComboDetailCriteriaKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idDetailCriteriaKri',
        'description'
    ]
});
