Ext.define('DukeSource.model.risk.kri.combos.ModelComboNormalizerKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idNormalizerKri',
        'description'
    ]
});
