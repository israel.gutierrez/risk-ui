Ext.define('DukeSource.model.risk.kri.combos.ModelComboQualificationKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idQualificationKri',
        'description'
    ]
});
