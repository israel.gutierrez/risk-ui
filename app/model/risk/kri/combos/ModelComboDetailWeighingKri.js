Ext.define('DukeSource.model.risk.kri.combos.ModelComboDetailWeighingKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idDetailWeighingKri',
        'description'
    ]
});
