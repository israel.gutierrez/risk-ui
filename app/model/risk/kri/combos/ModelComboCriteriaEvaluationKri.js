Ext.define('DukeSource.model.risk.kri.combos.ModelComboCriteriaEvaluationKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idCriteriaEvaluationKri',
        'description'
    ]
});
