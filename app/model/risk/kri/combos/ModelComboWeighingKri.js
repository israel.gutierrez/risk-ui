Ext.define('DukeSource.model.risk.kri.combos.ModelComboWeighingKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idWeighingKri',
        'description'
    ]
});
