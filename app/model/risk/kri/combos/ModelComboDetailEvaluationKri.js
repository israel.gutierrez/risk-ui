Ext.define('DukeSource.model.risk.kri.combos.ModelComboDetailEvaluationKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idDetailEvaluationKri',
        'description'
    ]
});
