Ext.define('DukeSource.model.risk.kri.combos.ModelComboKeyRiskIndicator', {
    extend: 'Ext.data.Model',
    fields: [
        'idKeyRiskIndicator',
        'nameKri'
    ]
});
