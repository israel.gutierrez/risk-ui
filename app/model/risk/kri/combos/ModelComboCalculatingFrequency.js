Ext.define('DukeSource.model.risk.kri.combos.ModelComboCalculatingFrequency', {
    extend: 'Ext.data.Model',
    fields: [
        'idCalculatingFrequency',
        'description'
    ]
});
