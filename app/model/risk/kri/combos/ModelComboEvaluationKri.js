Ext.define('DukeSource.model.risk.kri.combos.ModelComboEvaluationKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idEvaluationKri',
        'description'
    ]
});
