Ext.define('DukeSource.model.risk.kri.grids.ModelGridPanelRegisterNormalizerKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idNormalizerKri'
        , 'criteriaEvaluationKri'
        , 'nameCriteriaEvaluationKri'
        , 'keyRiskIndicator'
        , 'nameKeyRiskIndicator'
        , 'description'
        , 'weighing'
        , 'normalizerKri'
        , 'bestValue'
        , 'worstValue'
        , 'state'
    ]
});
