Ext.define('DukeSource.model.risk.kri.grids.ModelGridPanelRegisterQualificationEventOne', {
    extend: 'Ext.data.Model',
    fields: [
        'idQualificationEventOne'
        , 'description'
        , 'rangeInf'
        , 'rangeSup'
        , 'levelColour'
        , 'state'
        , 'legend'
    ]
});
