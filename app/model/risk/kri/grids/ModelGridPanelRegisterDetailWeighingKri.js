Ext.define('DukeSource.model.risk.kri.grids.ModelGridPanelRegisterDetailWeighingKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idDetailWeighingKri'
        , 'idWeighingKri'
        , 'normalizerKri'
        , 'nameNormalizerKri'
        , 'weighing'
        , 'state'
    ]
});
