Ext.define('DukeSource.model.risk.kri.grids.ModelGridPanelRegisterCalculatingFrequency', {
    extend: 'Ext.data.Model',
    fields: [
        'idCalculatingFrequency'
        , 'description'
        , 'abbreviation'
        , 'state'
        , 'weighing'
        , 'equivalentFrequency'
    ]
});
