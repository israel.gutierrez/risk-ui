Ext.define('DukeSource.model.risk.kri.grids.ModelGridPanelRegisterDetailCriteriaKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idDetailCriteriaKri'
        , 'criteriaEvaluationKri'
        , 'nameCriteriaEvaluationKri'
        , 'description'
        , 'state'
    ]
});
