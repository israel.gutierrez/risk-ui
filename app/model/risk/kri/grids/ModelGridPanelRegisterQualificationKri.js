Ext.define('DukeSource.model.risk.kri.grids.ModelGridPanelRegisterQualificationKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idQualificationKri'
        , 'description'
        , 'rangeInf'
        , 'rangeSup'
        , 'levelColour'
        , 'state'
    ]
});
