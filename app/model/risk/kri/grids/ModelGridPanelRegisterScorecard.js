Ext.define('DukeSource.model.risk.kri.grids.ModelGridPanelRegisterScorecard', {
    extend: 'Ext.data.Model',
    fields: [
        'idKeyRiskIndicator'
        , 'idEventOne'
        , 'weight'
        , 'relationshipKRI'
        , 'state'
    ]
});
