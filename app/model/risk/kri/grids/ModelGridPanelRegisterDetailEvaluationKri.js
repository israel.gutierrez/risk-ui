Ext.define('DukeSource.model.risk.kri.grids.ModelGridPanelRegisterDetailEvaluationKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idDetailEvaluationKri'
        , 'evaluationKri'
        , 'nameEvaluationKri'
        , 'description'
        , 'indicatorValue'
        , 'indicatorNormalize'
        , 'weighing'
        , 'state'
    ]
});
