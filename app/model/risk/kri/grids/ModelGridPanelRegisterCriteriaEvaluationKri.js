Ext.define('DukeSource.model.risk.kri.grids.ModelGridPanelRegisterCriteriaEvaluationKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idCriteriaEvaluationKri'
        , 'description'
        , 'state'
    ]
});
