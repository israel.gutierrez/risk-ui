Ext.define('DukeSource.model.risk.kri.grids.ModelGridPanelRegisterKeyRiskIndicator', {
    extend: 'Ext.data.Model',
    fields: [
        'idKeyRiskIndicator'
        , 'process'
        , 'nameProcess'
        , 'subProcess'
        , 'nameSubProcess'
        , 'workArea'
        , 'nameWorkArea'
        , 'factorRisk'
        , 'nameFactorRisk'
        , 'managerRisk'
        , 'nameManagerRisk'
        , 'calculatingFrequency'
        , 'nameCalculatingFrequency'
        , 'codeKri'
        , 'nameKri'
        , 'description'
        , 'abbreviation'
        , 'remarks'
        , 'formCalculating'
        , 'indicatorKri'
        , 'coverage'
        , 'modeEvaluation'
        , 'unitMeasure'
        , 'state'
    ]
});
