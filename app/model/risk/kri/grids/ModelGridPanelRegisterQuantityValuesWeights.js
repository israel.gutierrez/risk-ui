Ext.define('DukeSource.model.risk.kri.grids.ModelGridPanelRegisterQuantityValuesWeights', {
    extend: 'Ext.data.Model',
    fields: [
        'idQuantityValuesWeights'
        , 'detailWeighingKri'
        , 'nameDetailWeighingKri'
        , 'weighingKri'
        , 'nameWeighingKri'
        , 'description'
        , 'weighing'
        , 'equivalentFrequency'
        , 'state'
    ]
});
