Ext.define('DukeSource.model.risk.kri.grids.ModelTreeGridPanelRegisterDetailCriteriaKri', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'text',
        'fieldName',
        'typeCriteria',
        'surfaceArea',
        'population',
        'lifeExpectancy',
        'gnp',
        'capital',
        'code'
    ]
});