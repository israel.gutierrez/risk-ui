Ext.define('DukeSource.model.risk.kri.grids.ModelGridPanelRegisterWeighingKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idWeighingKri'
        , 'description'
        , 'weighing'
        , 'unitMeasure'
        , 'state'
    ]
});
