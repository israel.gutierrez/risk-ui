Ext.define('DukeSource.model.risk.kri.grids.ModelGridPanelRegisterEvaluationKri', {
    extend: 'Ext.data.Model',
    fields: [
        'idEvaluationKri'
        , 'detailWeighingKri'
        , 'nameDetailWeighingKri'
        , 'weighingKri'
        , 'nameWeighingKri'
        , 'keyRiskIndicator'
        , 'nameKeyRiskIndicator'
        , 'normalizerKri'
        , 'nameNormalizerKri'
        , 'description'
        , 'evaluationResul'
        , 'state'
    ]
});
