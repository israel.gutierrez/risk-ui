Ext.define('DukeSource.model.risk.DatabaseEventsLost.grids.ModelGridIncidentsRiskOperational', {
    extend: 'Ext.data.Model',
    fields: [

        'idDetailIncidents'
        , 'idIncidents'
        , 'actionPlan'
        , 'nameActionPlan'
        , 'factorRisk'
        , 'nameFactorRisk'
        , 'lossType'
        , 'nameIdLostType'
        , 'eventOne'
        , 'nameEventOne'
        , 'eventTwo'
        , 'nameEventTwo'
        , 'eventThree'
        , 'nameEventThree'
        , 'currency'
        , 'nameCurrency'
        , 'descriptionLarge'
        , 'assignReport'
        , 'dateOccurrence'
        , 'dateDiscovery'
        , 'netLoss'
        , 'indicatorIncidents'
        , 'nameUserReport'
        , 'fullNameUserRegister'
        , 'agency'
    ]
});
