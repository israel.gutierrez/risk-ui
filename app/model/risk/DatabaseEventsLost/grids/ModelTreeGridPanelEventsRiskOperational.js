Ext.define('DukeSource.model.risk.DatabaseEventsLost.grids.ModelTreeGridPanelEventsRiskOperational', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'text', type: 'string'},
        {name: 'surfaceArea', convert: formatDecimals2},
        {name: 'population', convert: formatDecimals2},
        {name: 'lifeExpectancy', convert: formatDecimals},
        {name: 'gnp', convert: formatDecimals},
        {name: 'capital', type: 'string'},
        {name: 'code', type: 'string'}
    ]
});