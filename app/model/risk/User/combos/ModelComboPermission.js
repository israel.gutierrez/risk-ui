Ext.define('DukeSource.model.risk.User.combos.ModelComboPermission', {
    extend: 'Ext.data.Model',
    fields: [
        'value',
        'description'
    ]
});
