Ext.define('DukeSource.model.risk.User.combos.ModelComboAuthorization', {
    extend: 'Ext.data.Model',
    fields: [
        'idSpecialProcess',
        'description'
    ]
});
