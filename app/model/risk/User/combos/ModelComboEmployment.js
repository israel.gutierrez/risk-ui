Ext.define('DukeSource.model.risk.User.combos.ModelComboEmployment', {
    extend: 'Ext.data.Model',
    fields: [
        'idEmployment',
        'description'
    ]
});
