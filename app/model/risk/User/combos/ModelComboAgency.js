Ext.define('DukeSource.model.risk.User.combos.ModelComboAgency', {
    extend: 'Ext.data.Model',
    fields: [
        'idAgency',
        'description',
        'descriptionRegion'
    ]
});
