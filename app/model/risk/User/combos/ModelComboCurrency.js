Ext.define('DukeSource.model.risk.User.combos.ModelComboCurrency', {
    extend: 'Ext.data.Model',
    fields: [
        'idCurrency',
        'description'
    ]
});

