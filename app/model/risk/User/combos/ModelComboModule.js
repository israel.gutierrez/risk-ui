Ext.define('DukeSource.model.risk.User.combos.ModelComboModule', {
    extend: 'Ext.data.Model',
    fields: [
        'idModule',
        'description'
    ]
});
