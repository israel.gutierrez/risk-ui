Ext.define('DukeSource.model.risk.User.grids.ModelGridPanelRegisterConstantApplication', {
    extend: 'Ext.data.Model',
    fields: [
        'idConstantApplication'
        , 'module'
        , 'nameModule'
        , 'codeConstant'
        , 'description'
        , 'valueConstant'
        , 'typeConstants'
        , 'state'
    ]
});
