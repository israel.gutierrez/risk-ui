Ext.define('DukeSource.model.risk.User.grids.ModelGridPanelRegisterAccess', {
    extend: 'Ext.data.Model',
    fields: [
        'idAccess'
        , 'valueAccess'
        , 'typeAccess'
        , 'module'
        , 'nameModule'
        , 'codeAccess'
        , 'icon'
        , 'nameClass'
        , 'nameTab'
        , 'closable'
        , 'permitSubTabs'
        , 'typeItem'
        , 'nameView'
        , 'state'
    ]
});
