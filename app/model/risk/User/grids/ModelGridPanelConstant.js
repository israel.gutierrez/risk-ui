Ext.define('DukeSource.model.risk.User.grids.ModelGridPanelConstant', {
    extend: 'Ext.data.Model',
    fields: [
        'UserCode'
        , 'UserName'
        , 'UserRol'
    ]
});

