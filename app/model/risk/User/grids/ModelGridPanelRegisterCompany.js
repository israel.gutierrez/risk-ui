Ext.define('DukeSource.model.risk.User.grids.ModelGridPanelRegisterCompany', {
    extend: 'Ext.data.Model',
    fields: [
        'id'
        , 'description'
        , 'address'
        , 'titleReport'
        , 'state'
        , 'ruc'
    ]
});
