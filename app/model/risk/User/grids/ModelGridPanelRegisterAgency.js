Ext.define('DukeSource.model.risk.User.grids.ModelGridPanelRegisterAgency', {
    extend: 'Ext.data.Model',
    fields: [
        'idAgency'
        , 'description'
        , 'abbreviation'
        , 'state'
        , 'address'

    ]
});
