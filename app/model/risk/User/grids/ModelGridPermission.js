Ext.define('DukeSource.model.risk.User.grids.ModelGridPermission', {
    extend: 'Ext.data.Model',
    fields: [
        'descriptionAccess'
        , 'descriptionModule'
        , 'typeAccess'
        , 'indicatorOptionConsult'
        , 'indicatorOptionSave'
        , 'indicatorOptionUpdate'
        , 'indicatorOptionDelete'
    ]
});


