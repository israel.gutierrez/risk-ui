Ext.define('DukeSource.model.risk.User.grids.ModelGridPanelRegisterConstant', {
    extend: 'Ext.data.Model',
    fields: [
        'id'
        , 'codeConstant'
        , 'description'
        , 'valor'
        , 'type'
        , 'state'
        , 'idModule'
        , 'descriptionModule'
    ]
});
