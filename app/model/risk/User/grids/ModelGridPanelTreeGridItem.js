Ext.define('DukeSource.model.risk.User.grids.ModelGridPanelTreeGridItem', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'text',
        'type',
        'iconCls',
        'leaf',
        'checked'

    ]
});

