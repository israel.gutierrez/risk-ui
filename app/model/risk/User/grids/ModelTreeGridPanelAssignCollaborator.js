function formatDecimals(v, record) {
    if (record.get('capital') == "") {
        return "";
    }
    else {
        return (new Number(v)).toFixed(2);
    }
}

function formatDecimals2(v, record) {
    return (new Number(v)).toFixed(2);
}

Ext.define('DukeSource.model.risk.User.grids.ModelTreeGridPanelAssignCollaborator', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'text', type: 'string'},
        {name: 'fieldOne', type: 'string'}
    ]
});