Ext.define('DukeSource.model.risk.User.grids.ModelGridPanelRegisterRol', {
    extend: 'Ext.data.Model',
    fields: [
        'id'
        , 'name'
        , 'description'
        , 'state'
        , 'category'
        , 'lineDefense'
        , 'module'
        , 'upModule'
    ]
});
