Ext.define('DukeSource.model.risk.User.grids.ModelTreeGridPanelAssignJobPlace', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'text', type: 'string'},
        {name: 'fieldOne', type: 'string'},//
        {name: 'category', type: 'string'},
        {name: 'idJobPlace', type: 'string'},
        {name: 'workArea', type: 'string'},
        {name: 'fullName', type: 'string'}
    ]
});