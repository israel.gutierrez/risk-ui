Ext.define('DukeSource.model.risk.User.grids.ModelGridPermissionCheck', {
    extend: 'Ext.data.Model',
    fields: [
        'valueAccess'
        , 'descriptionModule'
        , 'typeItem'
        , 'access'
        , 'idRoleAccess'
        , 'role'
        , {
            name: 'indicatorOptionConsult', type: 'bool',
            convert: function (v) {
                return (v === "S" || v === true) ? true : false;
            }
        }
        , {
            name: 'indicatorOptionSave', type: 'bool',
            convert: function (v) {
                return (v === "S" || v === true) ? true : false;
            }
        }
        , {
            name: 'indicatorOptionUpdate', type: 'bool',
            convert: function (v) {
                return (v === "S" || v === true) ? true : false;
            }
        }
        , {
            name: 'indicatorOptionDelete', type: 'bool',
            convert: function (v) {
                return (v === "S" || v === true) ? true : false;
            }
        }
    ]
});


