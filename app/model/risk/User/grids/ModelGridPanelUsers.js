Ext.define('DukeSource.model.risk.User.grids.ModelGridPanelUsers', {
    extend: 'Ext.data.Model',
    fields: [
        'userName',
        'login',
        'agency',
        'idRole',
        'idUserRole',
        'workArea',
        'userEmployment',
        'fullName',
        'category',
        'descriptionCategory',
        'hourStart',
        'hourEnd',
        'state',
        'email',
        {
            name: 'active', type: 'bool',
            convert: function (v) {
                return (v === "A" || v === true) ? true : false;
            }
        }
    ]
});

