Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterDetailControlType', {
    extend: 'Ext.data.Model',
    fields: [
        'idDetailControlType'
        , 'controlType'
        , 'nameControlType'
        , 'valueTypificationControl'
        , 'typificationControl'
        , 'state'
    ]
});
