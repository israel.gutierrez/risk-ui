Ext.define('DukeSource.model.risk.parameter.grids.ModelTreeGridPanelRegisterBusinessLine', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'text', type: 'string'},
        {name: 'lineUp', type: 'string'},
        {name: 'activeProduct', type: 'string'},
        {name: 'fieldOne', type: 'string'},
        {name: 'codeEntitySupervisor', type: 'string'}
    ]
});