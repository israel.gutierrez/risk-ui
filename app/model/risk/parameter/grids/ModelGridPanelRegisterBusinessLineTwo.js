Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterBusinessLineTwo', {
    extend: 'Ext.data.Model',
    fields: [
        'businessLineOne'
        , 'idBusinessLineTwo'
        , 'description'
        , 'state'
    ]
});
