Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterProduct', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'idProduct',
        'description',
        'text',
        'parent',
        'parentId',
        'depth',
        'path',
        'descriptionProduct',
        'factor',
        'state'
    ]
});
