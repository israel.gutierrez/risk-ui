Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterAccountPlan', {
    extend: 'Ext.data.Model',
    fields: [
        'idAccountPlan'
        , 'codeAccount'
        , 'orderAccount'
        , 'currency'
        , 'nameCurrency'
        , 'description'
        , 'state'
    ]
});
