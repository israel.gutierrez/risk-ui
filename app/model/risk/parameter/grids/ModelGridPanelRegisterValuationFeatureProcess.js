Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterValuationFeatureProcess', {
    extend: 'Ext.data.Model',
    fields: [
        'idValuationFeatureProcess'
        , 'featureProcess'
        , 'nameFeatureProcess'
        , 'valueFeature'
        , 'description'
        , 'valuationConcept'
        , 'state'
    ]
});
