Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterScorecardMaster', {
    extend: 'Ext.data.Model',
    fields: [
        'idScorecardMaster'
        , 'description'
        , 'state'
    ]
});
