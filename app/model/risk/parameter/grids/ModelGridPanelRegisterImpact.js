Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterImpact', {
    extend: 'Ext.data.Model',
    fields: [
        'idImpact'
        , 'description'
        , 'equivalentValue'
        , 'percentage'
        , 'operationalRiskExposition'
        , {name: 'valueMinimal', type: 'int'}
        , {name: 'valueMaximo', type: 'int'}
        , {name: 'maxAmount', type: 'int'}
        , {name: 'average', type: 'int'}
        , 'idOperationalRiskExposition'
    ]
});
