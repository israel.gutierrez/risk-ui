Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterRiskType', {
    extend: 'Ext.data.Model',
    fields: [
        'idRiskType'
        , 'description'
        , 'state'
    ]
});
