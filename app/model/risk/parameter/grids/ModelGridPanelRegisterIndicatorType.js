Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterIndicatorType', {
    extend: 'Ext.data.Model',
    fields: [
        'idIndicatorType'
        , 'description'
        , 'state'
    ]
});
