Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterSubClassification', {
    extend: 'Ext.data.Model',
    fields: [
        'idSubClassification'
        , 'description'
        , 'state'
    ]
});
