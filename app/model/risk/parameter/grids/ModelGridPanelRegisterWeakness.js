Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterWeakness', {
    extend: 'Ext.data.Model',
    fields: [
        'idWeakness'
        , 'codeWeakness'
        , 'description'
        , 'state'
    ]
});
