Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterTypeEvent', {
    extend: 'Ext.data.Model',
    fields: [
        'idTypeEvent'
        , 'description'
        , 'state'
    ]
});
