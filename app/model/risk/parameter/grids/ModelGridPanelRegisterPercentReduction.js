Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterPercentReduction', {
    extend: 'Ext.data.Model',
    fields: [
        'idPercentReduction'
        , 'valuePercent'
        , 'state'
        , 'typePercent'
    ]
});
