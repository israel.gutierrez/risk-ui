Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterSubProcess', {
    extend: 'Ext.data.Model',
    fields: [
        'process',
        'idSubProcess',
        'nameProcess',
        'abbreviation',
        'description',
        'state'
    ]
});
