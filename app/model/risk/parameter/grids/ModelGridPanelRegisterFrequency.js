Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterFrequency', {
    extend: 'Ext.data.Model',
    fields: [
        'idFrequency'
        , 'description'
        , 'percentage'
        , 'equivalentValue'
    ]
});
