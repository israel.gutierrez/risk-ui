Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterStateTables', {
    extend: 'Ext.data.Model',
    fields: [
        'idStateTables'
        , 'description'
        , 'nameTable'
        , 'state'
    ]
});
