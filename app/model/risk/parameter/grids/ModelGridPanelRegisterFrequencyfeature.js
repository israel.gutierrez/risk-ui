Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterFrequencyfeature', {
    extend: 'Ext.data.Model',
    fields: [
        'idFrequencyFeature'
        , 'description'
        , 'state'
    ]
});
