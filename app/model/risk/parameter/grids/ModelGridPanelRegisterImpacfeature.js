Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterImpacfeature', {
    extend: 'Ext.data.Model',
    fields: [
        'idImpactFeature'
        , 'description'
        , 'state'
        , 'type'
    ]
});
