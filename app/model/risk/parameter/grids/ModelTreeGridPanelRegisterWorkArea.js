Ext.define('DukeSource.model.risk.parameter.grids.ModelTreeGridPanelRegisterWorkArea', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'idWorkArea',
        'description',
        'codeMigration',
        'text',
        'parent',
        'parentId',
        'state'
    ]
});
