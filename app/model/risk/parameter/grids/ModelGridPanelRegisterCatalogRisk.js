Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterCatalogRisk', {
    extend: 'Ext.data.Model',
    fields: [
        'idRisk'
        , 'description'
        , 'codeRisk'
        , 'state'
    ]
});
