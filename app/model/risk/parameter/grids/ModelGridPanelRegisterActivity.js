Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterActivity', {
    extend: 'Ext.data.Model',
    fields: [
        'idActivity'
        , 'process'
        , 'subProcess'
        , 'description'
        , 'evaluator'
        , 'state'
    ]
});
