Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterControlType', {
    extend: 'Ext.data.Model',
    fields: [
        'idControlType'
        , 'weightedControl'
        , 'description'
        , 'state'
    ]
});
