Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterTypeEffect', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'abbreviation',
        'description',
        'state'
    ]
});

