Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterScaleBusinessLine', {
    extend: 'Ext.data.Model',
    fields: [
        'idScaleRisk'
        , 'description'
        , {name: 'percentageExposition', type: 'int'}
        , 'state'
        , {name: 'rangeInf', type: 'int'}
        , {name: 'rangeSup', type: 'int'}
        , 'levelColour'
        , {name: 'businessLineOne', type: 'int'}
        , {name: 'operationalRiskExposition', type: 'int'}
        , {name: 'valueOperationalRiskExposition', type: 'int'}
    ]
});
