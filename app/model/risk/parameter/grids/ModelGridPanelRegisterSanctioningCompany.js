Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterSanctioningCompany', {
    extend: 'Ext.data.Model',
    fields: [
        'idSanctioningCompany'
        , 'description'
        , 'state'
    ]
});
