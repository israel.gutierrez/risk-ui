Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterFactorRisk', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'idFactorRisk',
        'description',
        'text',
        'parent',
        'parentId',
        'depth',
        'state'
    ]
});
