Ext.define('DukeSource.model.risk.parameter.grids.ModelTreeGridPanelRegisterControlType', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'typeControl', type: 'string'},
        {name: 'text', type: 'string'},
        {name: 'fieldOne', type: 'string'},
        {name: 'percentage', type: 'string'},
        {name: 'abbreviation', type: 'string'},
        {name: 'fieldTwo', type: 'string'}
    ]
});