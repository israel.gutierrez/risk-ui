Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterScaleRatingActionPlan', {
    extend: 'Ext.data.Model',
    fields: [
        'idScaleRatingActionPlan'
        , 'codeColour'
        , 'rangeMin'
        , 'rangeMax'
        , 'description'
        , 'state'
    ]
});
