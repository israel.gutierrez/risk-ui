Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterAdjustmentFactorBusiness', {
    extend: 'Ext.data.Model',
    fields: [
        'idAdjustmentFactorBusiness'
        , 'titleReport'
        , 'nameTitleReport'
        , 'groupReport'
        , 'nameGroupReport'
        , 'fixedFactor'
        , 'adjustedFactor'
        , 'fixedAdjusted'
        , 'dateStartValidity'
        , 'dateEndValidity'
        , 'state'
    ]
});
