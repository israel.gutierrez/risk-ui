Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterAvailability', {
    extend: 'Ext.data.Model',
    fields: [
        'idAvailability'
        , 'description'
        , 'state'
    ]
});
