Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterProcess', {
    extend: 'Ext.data.Model',
    fields: [
        'idProcess'
        , 'processType'
        , 'nameProcessType'
        , 'abbreviation'
        , 'description'
        , 'state'
    ]
});
