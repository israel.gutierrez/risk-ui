Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterReasonSafety', {
    extend: 'Ext.data.Model',
    fields: [
        'idReasonSafety'
        , 'description'
        , 'codeCause'
        , 'state'
    ]
});
