Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterAgency', {
    extend: 'Ext.data.Model',
    fields: [
        'idAgency'
        , 'company'
        , 'nameCompany'
        , 'description'
        , 'state'
    ]
});
