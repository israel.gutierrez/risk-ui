Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterSafetyCriterion', {
    extend: 'Ext.data.Model',
    fields: [
        'idSafetyCriterion'
        , 'description'
        , 'state'
    ]
});
