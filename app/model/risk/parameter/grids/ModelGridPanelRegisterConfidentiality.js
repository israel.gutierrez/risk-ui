Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterConfidentiality', {
    extend: 'Ext.data.Model',
    fields: [
        'idConfidentiality'
        , 'description'
        , 'state'
    ]
});
