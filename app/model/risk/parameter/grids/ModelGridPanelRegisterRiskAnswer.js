Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterRiskAnswer', {
    extend: 'Ext.data.Model',
    fields: [
        'idRiskAnswer'
        , 'description'
        , 'state'
        , 'minActionPlan'
        , 'maxActionPlan'
        , 'minControl'
        , 'maxControl'
    ]
});
