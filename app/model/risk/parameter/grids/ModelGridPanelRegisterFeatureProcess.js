Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterFeatureProcess', {
    extend: 'Ext.data.Model',
    fields: [
        'idFeatureProcess'
        , 'description'
        , 'state'
    ]
});
