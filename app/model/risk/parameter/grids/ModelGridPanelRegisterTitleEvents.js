Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterTitleEvents', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description',
        'abbreviation',
        'state'
    ]
});


