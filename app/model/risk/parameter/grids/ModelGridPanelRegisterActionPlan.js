Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterActionPlan', {
    extend: 'Ext.data.Model',
    fields: [
        'idActionPlan'
        , 'codePlan'
        , 'indicatorType'
        , 'nameIndicatorType'
        , 'originPlanAction'
        , 'description'
        , 'state'
    ]
});
