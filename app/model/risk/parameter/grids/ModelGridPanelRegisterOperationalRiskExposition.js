Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterOperationalRiskExposition', {
    extend: 'Ext.data.Model',
    fields: [
        'idOperationalRiskExposition'
        , 'maxAmount'
        , 'year'
        , 'validity'
    ]
});
