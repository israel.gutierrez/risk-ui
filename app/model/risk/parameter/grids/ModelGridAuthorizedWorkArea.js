Ext.define('DukeSource.model.risk.parameter.grids.ModelGridAuthorizedWorkArea', {
    extend: 'Ext.data.Model',
    fields: [
        'id'
        , 'workArea'
        , 'user'
        , 'eventState'
        , 'level'
        , 'type'
        , 'fullName'
        , 'descriptionStateEvent'
        , 'abbreviationStateEvent'
        , 'descriptionRule'
        , 'maxAmount'
        , 'minAmount'
        , 'sequence'
        , 'rule'
        , 'state'
    ]
});
