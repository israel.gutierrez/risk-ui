Ext.define('DukeSource.model.risk.parameter.grids.ModelTreeGridPanelRegisterCompany', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'text', type: 'string'},
        {name: 'location', type: 'string'},
        {name: 'codePAF', type: 'string'},
        {name: 'zipCode', type: 'string'},
        {name: 'code', type: 'string'}
    ]
});