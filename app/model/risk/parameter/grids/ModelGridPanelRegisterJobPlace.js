Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterJobPlace', {
    extend: 'Ext.data.Model',
    fields: [
        'idJobPlace'
        , 'workArea'
        , 'nameWorkArea'
        , 'description'
        , 'state'
    ]
});
