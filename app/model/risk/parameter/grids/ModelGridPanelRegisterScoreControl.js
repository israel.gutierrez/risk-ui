Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterScoreControl', {
    extend: 'Ext.data.Model',
    fields: [
        'idScoreControl'
        , 'description'
        , 'codeColour'
        , 'scoreControl'
        , 'maxEffectiveness'
        , 'state'
        , 'minEffectiveness'
        , 'scoreMin'
        , 'scoreMax'
        , 'typeControl'
    ]
});
