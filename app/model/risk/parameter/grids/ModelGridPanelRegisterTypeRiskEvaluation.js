Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterTypeRiskEvaluation', {
    extend: 'Ext.data.Model',
    fields: [
        'idTypeRiskEvaluation'
        , 'description'
        , 'state'
    ]
});
