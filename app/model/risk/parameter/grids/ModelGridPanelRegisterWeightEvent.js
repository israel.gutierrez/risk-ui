Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterWeightEvent', {
    extend: 'Ext.data.Model',
    fields: [
        'idEventOne'
        , 'idScorecardMaster'
        , {'name': 'weight', 'type': "Number"}
        , 'state'
        , 'nameEventOne'
        , 'nameScorecardMaster'
        , {'name': 'score', 'type': "Number"}
        , {'name': 'weighting', 'type': "Number"}
        , 'year'
        , 'month'
        , 'yearMonth'
    ]
});
