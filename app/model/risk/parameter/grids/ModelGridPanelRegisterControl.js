Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterControl', {
    extend: 'Ext.data.Model',
    fields: [
        'idControl'
        , 'typeProcess'
        , 'jobPlace'
        , 'jobPlaceDescription'
        , 'nameTypeProcess'
        , 'idProcess'
        , 'nameIdProcess'
        , 'idSubProcess'
        , 'nameIdSubProcess'
        , 'idManagerRisk'
        , 'fullName'
        , 'typeControl'
        , 'codeControl'
        , 'description'
        , 'descriptionControl'
        , 'externalDescription'
        , 'internalDescription'
        , 'state'
        , 'stateEvaluated'
        , 'colorQualification'
        , 'valueScoreControl'
        , 'descriptionEvaluation'
    ]
});
