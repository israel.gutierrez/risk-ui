Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterFixedAssets', {
    extend: 'Ext.data.Model',
    fields: [
        'idFixedAssets'
        , 'codeAssets'
        , 'technicalCustodio'
        , 'availability'
        , 'nameAvailability'
        , 'confidentiality'
        , 'nameConfidentiality'
        , 'integrity'
        , 'nameIntegrity'
        , 'description'
        , 'nameTechnicalCustodio'
        , 'state'
        , 'nameOwner'
        , 'owner'
    ]
});
