Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterTechnicalCustodio', {
    extend: 'Ext.data.Model',
    fields: [
        'idTechnicalCustodio'
        , 'description'
        , 'state'
    ]
});
