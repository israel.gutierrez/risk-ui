Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterSystemClosed', {
    extend: 'Ext.data.Model',
    fields: [
        'idSystemClosed'
        , {name: 'dateClose'}
//              ,'dateClose'
        , 'description'
        , 'state'
    ]
});
