Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterBusinessLineOne', {
    extend: 'Ext.data.Model',
    fields: [
        'idBusinessLineOne'
        , 'description'
        , 'state'
    ]
});
