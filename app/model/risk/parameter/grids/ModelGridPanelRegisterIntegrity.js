Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterIntegrity', {
    extend: 'Ext.data.Model',
    fields: [
        'idIntegrity'
        , 'description'
        , 'state'
    ]
});
