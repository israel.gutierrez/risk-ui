Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterDetailImpactFeature', {
    extend: 'Ext.data.Model',
    fields: [
        'idImpact'
        , 'idImpactFeature'
        , 'description'
        , 'state'
    ]
});
