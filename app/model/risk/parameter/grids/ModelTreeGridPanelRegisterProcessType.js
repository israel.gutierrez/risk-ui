Ext.define('DukeSource.model.risk.parameter.grids.ModelTreeGridPanelRegisterProcessType', {
    extend: 'Ext.data.Model',
    fields: [
        'idProcess',
        'idActivity',
        'idSubProcess',
        'idProcessType',
        'processType',
        'descriptionProcessType',
        'descriptionSubProcess',
        'descriptionProcess',
        'descriptionLong',
        'abbreviation',
        'alias',
        'description',
        'state',
        'text',
        'parent',
        'evaluator',
        'transactional',
        'input',
        'output',
        'generateProduct',
        'importance'
    ]
});
