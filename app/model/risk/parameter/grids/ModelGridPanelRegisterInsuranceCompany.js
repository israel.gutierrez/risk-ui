Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterInsuranceCompany', {
    extend: 'Ext.data.Model',
    fields: [
        'idInsuranceCompany'
        , 'description'
        , 'state'
    ]
});
