Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterTypeChange', {
    extend: 'Ext.data.Model',
    fields: [
        'idTypeChange'
        , 'currency'
        , 'nameCurrency'
        , 'dateProcess'
        , 'valueSell'
        , 'valueBuy'
        , 'valueFixed'
    ]
});
