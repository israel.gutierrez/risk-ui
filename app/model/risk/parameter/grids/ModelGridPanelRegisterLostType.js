Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterLostType', {
    extend: 'Ext.data.Model',
    fields: [
        'idLostType'
        , 'description'
        , 'state'
    ]
});
