Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterDetailFrequencyFeature', {
    extend: 'Ext.data.Model',
    fields: [
        'idFrequencyFeature'
        , 'idFrequency'
        , 'description'
        , 'state'
    ]
});
