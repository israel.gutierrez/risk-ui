Ext.define('DukeSource.model.risk.parameter.grids.ModelTreeGridPanelRegisterEvent', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'text', type: 'string'},
        {name: 'code', type: 'string'},
        {name: 'codeEntitySupervisor', type: 'string'}
    ]
});