Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterAdjustmentFactorProvitional', {
    extend: 'Ext.data.Model',
    fields: [
        'idAdjustmentFactor'
        , 'reverseOverallLimit'
        , 'valueAdjustment'
        , 'dateStartValidity'
        , 'fixedFactor'
        , 'dateEndValidity'
        , 'state'
    ]
});
