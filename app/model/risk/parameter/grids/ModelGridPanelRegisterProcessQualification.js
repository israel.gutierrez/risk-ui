Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterProcessQualification', {
    extend: 'Ext.data.Model',
    fields: [
        'idProcessQualification'
        , 'valueQualification'
        , 'description'
        , 'state'
    ]
});
