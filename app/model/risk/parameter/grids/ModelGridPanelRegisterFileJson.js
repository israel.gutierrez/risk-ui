Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterFileJson', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        {name: 'text', type: 'string'},
        {name: 'fieldOne', type: 'string'},
        {name: 'fieldTwo', type: 'string'},
        {name: 'fieldThree', type: 'string'}
    ]
});