Ext.define('DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterCurrency', {
    extend: 'Ext.data.Model',
    fields: [
        'idCurrency'
        , 'description'
        , 'symbol'
        , 'state'
    ]
});
