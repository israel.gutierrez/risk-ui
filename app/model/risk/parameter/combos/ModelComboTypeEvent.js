Ext.define('DukeSource.model.risk.parameter.combos.ModelComboTypeEvent', {
    extend: 'Ext.data.Model',
    fields: [
        'idTypeEvent',
        'description'
    ]
});
