Ext.define('DukeSource.model.risk.parameter.combos.ModelComboTechnicalCustodio', {
    extend: 'Ext.data.Model',
    fields: [
        'idTechnicalCustodio',
        'description'
    ]
});
