Ext.define('DukeSource.model.risk.parameter.combos.ModelComboWorkArea', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'idWorkArea', type: 'int'}
        , 'description'
    ]
});
