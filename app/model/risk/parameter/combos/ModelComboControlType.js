Ext.define('DukeSource.model.risk.parameter.combos.ModelComboControlType', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
