Ext.define('DukeSource.model.risk.parameter.combos.ModelComboDetailImpactFeature', {
    extend: 'Ext.data.Model',
    fields: [
        'idDetailImpactFeature',
        'description'
    ]
});
