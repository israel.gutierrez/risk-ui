Ext.define('DukeSource.model.risk.parameter.combos.ModelComboEventThree', {
    extend: 'Ext.data.Model',
    fields: [
        'idEventThree',
        'description'
    ]
});
