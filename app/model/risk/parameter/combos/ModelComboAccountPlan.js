Ext.define('DukeSource.model.risk.parameter.combos.ModelComboAccountPlan', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
