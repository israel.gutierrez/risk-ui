Ext.define('DukeSource.model.risk.parameter.combos.ModelComboLostType', {
    extend: 'Ext.data.Model',
    fields: [
        'idLostType',
        'description'
    ]
});
