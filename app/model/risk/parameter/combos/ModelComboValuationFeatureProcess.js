Ext.define('DukeSource.model.risk.parameter.combos.ModelComboValuationFeatureProcess', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
