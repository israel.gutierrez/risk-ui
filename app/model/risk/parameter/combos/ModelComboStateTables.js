Ext.define('DukeSource.model.risk.parameter.combos.ModelComboStateTables', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
