Ext.define('DukeSource.model.risk.parameter.combos.ModelComboIndicatorType', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
