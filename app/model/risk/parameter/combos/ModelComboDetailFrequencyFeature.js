Ext.define('DukeSource.model.risk.parameter.combos.ModelComboDetailFrequencyFeature', {
    extend: 'Ext.data.Model',
    fields: [
        'idDetailFrequencyFeature',
        'description'
    ]
});
