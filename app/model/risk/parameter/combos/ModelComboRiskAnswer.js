Ext.define('DukeSource.model.risk.parameter.combos.ModelComboRiskAnswer', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
