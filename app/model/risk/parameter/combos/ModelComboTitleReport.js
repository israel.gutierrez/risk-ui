Ext.define('DukeSource.model.risk.parameter.combos.ModelComboTitleReport', {
    extend: 'Ext.data.Model',
    fields: [
        'idTitleReport',
        'description'
    ]
});
