Ext.define('DukeSource.model.risk.parameter.combos.ModelComboScoreControl', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
