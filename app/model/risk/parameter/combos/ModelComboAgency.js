Ext.define('DukeSource.model.risk.parameter.combos.ModelComboAgency', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
