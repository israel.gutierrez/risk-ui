Ext.define('DukeSource.model.risk.parameter.combos.ModelComboRiskType', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
