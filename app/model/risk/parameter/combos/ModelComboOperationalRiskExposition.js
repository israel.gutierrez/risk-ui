Ext.define('DukeSource.model.risk.parameter.combos.ModelComboOperationalRiskExposition', {
    extend: 'Ext.data.Model',
    fields: [
        'idOperationalRiskExposition',
        'description'
    ]
});
