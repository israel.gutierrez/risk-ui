Ext.define('DukeSource.model.risk.parameter.combos.ModelComboProduct', {
    extend: 'Ext.data.Model',
    fields: [
        'idProduct',
        'description'
    ]
});
