Ext.define('DukeSource.model.risk.parameter.combos.ModelComboProcessType', {
    extend: 'Ext.data.Model',
    fields: [
        'idProcessType',
        'description'
    ]
});
