Ext.define('DukeSource.model.risk.parameter.combos.ModelComboSanctioningCompany', {
    extend: 'Ext.data.Model',
    fields: [
        'idSanctioningCompany',
        'description'
    ]
});
