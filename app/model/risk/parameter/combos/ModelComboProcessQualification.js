Ext.define('DukeSource.model.risk.parameter.combos.ModelComboProcessQualification', {
    extend: 'Ext.data.Model',
    fields: [
        'idProcessQualification',
        'description'
    ]
});
