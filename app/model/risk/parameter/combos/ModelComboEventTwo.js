Ext.define('DukeSource.model.risk.parameter.combos.ModelComboEventTwo', {
    extend: 'Ext.data.Model',
    fields: [
        'idEventTwo',
        'description'
    ]
});
