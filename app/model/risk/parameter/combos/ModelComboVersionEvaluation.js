Ext.define('DukeSource.model.risk.parameter.combos.ModelComboVersionEvaluation', {
    extend: 'Ext.data.Model',
    fields: [
        'idVersionEvaluation',
        'description'
    ]
});
