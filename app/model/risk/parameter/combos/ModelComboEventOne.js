Ext.define('DukeSource.model.risk.parameter.combos.ModelComboEventOne', {
    extend: 'Ext.data.Model',
    fields: [
        'idEventOne',
        'description'
    ]
});
