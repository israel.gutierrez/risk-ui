Ext.define('DukeSource.model.risk.parameter.combos.ModelComboBusinessLineTwo', {
    extend: 'Ext.data.Model',
    fields: [
        'idBusinessLineTwo',
        'description'
    ]
});
