Ext.define('DukeSource.model.risk.parameter.combos.ModelComboActivity', {
    extend: 'Ext.data.Model',
    fields: [
        'idActivity',
        'description'
    ]
});
