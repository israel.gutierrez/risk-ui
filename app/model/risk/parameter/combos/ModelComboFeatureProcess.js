Ext.define('DukeSource.model.risk.parameter.combos.ModelComboFeatureProcess', {
    extend: 'Ext.data.Model',
    fields: [
        'idFeatureProcess',
        'description'
    ]
});
