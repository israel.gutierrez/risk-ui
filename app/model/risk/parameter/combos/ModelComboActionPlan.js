Ext.define('DukeSource.model.risk.parameter.combos.ModelComboActionPlan', {
    extend: 'Ext.data.Model',
    fields: [
        'idActionPlan',
        'description'
    ]
});
