Ext.define('DukeSource.model.risk.parameter.combos.ModelComboPercentReduction', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
