Ext.define('DukeSource.model.risk.parameter.combos.ModelComboScaleRatingActionPlan', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
