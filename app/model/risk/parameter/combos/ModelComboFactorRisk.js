Ext.define('DukeSource.model.risk.parameter.combos.ModelComboFactorRisk', {
    extend: 'Ext.data.Model',
    fields: [
        'idFactorRisk',
        'description'
    ]
});
