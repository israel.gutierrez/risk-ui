Ext.define('DukeSource.model.risk.parameter.combos.ModelComboTypeChange', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
