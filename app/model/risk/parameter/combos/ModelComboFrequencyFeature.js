Ext.define('DukeSource.model.risk.parameter.combos.ModelComboFrequencyFeature', {
    extend: 'Ext.data.Model',
    fields: [
        'idFrequencyFeature',
        'description'
    ]
});
