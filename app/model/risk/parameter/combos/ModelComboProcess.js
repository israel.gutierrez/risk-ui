Ext.define('DukeSource.model.risk.parameter.combos.ModelComboProcess', {
    extend: 'Ext.data.Model',
    fields: [
        'idProcess',
        'description'
    ]
});
