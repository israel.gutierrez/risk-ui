Ext.define('DukeSource.model.risk.parameter.combos.ModelComboScaleRisk', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
