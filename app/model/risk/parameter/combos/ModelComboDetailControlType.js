Ext.define('DukeSource.model.risk.parameter.combos.ModelComboDetailControlType', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'idControlType',
        'description'
    ]
});
