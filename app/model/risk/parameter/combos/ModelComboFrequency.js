Ext.define('DukeSource.model.risk.parameter.combos.ModelComboFrequency', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
