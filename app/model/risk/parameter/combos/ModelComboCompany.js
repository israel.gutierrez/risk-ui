Ext.define('DukeSource.model.risk.parameter.combos.ModelComboCompany', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
