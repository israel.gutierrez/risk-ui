Ext.define('DukeSource.model.risk.parameter.combos.ModelComboRegion', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
