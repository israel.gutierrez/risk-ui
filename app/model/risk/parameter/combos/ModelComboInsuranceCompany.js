Ext.define('DukeSource.model.risk.parameter.combos.ModelComboInsuranceCompany', {
    extend: 'Ext.data.Model',
    fields: [
        'idInsuranceCompany',
        'description'
    ]
});
