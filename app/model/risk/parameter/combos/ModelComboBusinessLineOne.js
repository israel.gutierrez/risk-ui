Ext.define('DukeSource.model.risk.parameter.combos.ModelComboBusinessLineOne', {
    extend: 'Ext.data.Model',
    fields: [
        'idBusinessLineOne',
        'description'
    ]
});
