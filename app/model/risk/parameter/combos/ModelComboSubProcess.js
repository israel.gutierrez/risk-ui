Ext.define('DukeSource.model.risk.parameter.combos.ModelComboSubProcess', {
    extend: 'Ext.data.Model',
    fields: [
        'idSubProcess',
        'description'
    ]
});
