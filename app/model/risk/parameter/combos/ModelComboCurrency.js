Ext.define('DukeSource.model.risk.parameter.combos.ModelComboCurrency', {
    extend: 'Ext.data.Model',
    fields: [
        'idCurrency',
        'description'
    ]
});
