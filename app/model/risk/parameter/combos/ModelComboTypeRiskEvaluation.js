Ext.define('DukeSource.model.risk.parameter.combos.ModelComboTypeRiskEvaluation', {
    extend: 'Ext.data.Model',
    fields: [
        'idTypeRiskEvaluation',
        'description'
    ]
});
