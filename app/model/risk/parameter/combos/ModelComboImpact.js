Ext.define('DukeSource.model.risk.parameter.combos.ModelComboImpact', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'description'
    ]
});
