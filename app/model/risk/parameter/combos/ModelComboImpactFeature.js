Ext.define('DukeSource.model.risk.parameter.combos.ModelComboImpactFeature', {
    extend: 'Ext.data.Model',
    fields: [
        'idImpactFeature',
        'description'
    ]
});
