Ext.define('DukeSource.model.risk.EvaluatorRiskOperational.grids.ModelGridPanelMapGeneralLegend', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'valueInherent', type: 'number'} //count
        , 'colourInherent' //colour
        , 'descriptionRisk' //description Scale Risk
        , {name: 'valueResidual', type: 'number'} //% concentration
    ]
});
