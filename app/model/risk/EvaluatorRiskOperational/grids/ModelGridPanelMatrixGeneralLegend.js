Ext.define('DukeSource.model.risk.EvaluatorRiskOperational.grids.ModelGridPanelMatrixGeneralLegend', {
    extend: 'Ext.data.Model',
    fields: [
        'codeRisk'
        , 'descriptionRisk'
        , {name: 'idMatrixInherent', type: 'number'}
        , {name: 'valueInherent', type: 'number'}
        , 'colourInherent'
        , {name: 'idMatrixResidual', type: 'number'}
        , {name: 'valueResidual', type: 'number'}
        , 'colourResidual'
    ]
});
