Ext.define('DukeSource.model.fulfillment.grids.ModelGridPanelRegisterAgreement', {
    extend: 'Ext.data.Model',
    fields: [
        'idAgreement'
        , 'codeCommittee'
        , 'codeAgreement'
        , 'dateAgreement'
        , 'description'
        , 'indicatorAttach'
        , 'state'
    ]
});
