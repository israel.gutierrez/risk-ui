Ext.define('DukeSource.model.fulfillment.grids.ModelGridPanelRegisterEntitySender', {
    extend: 'Ext.data.Model',
    fields: [
        'idEntitySender'
        , 'ruc'
        , 'description'
        , 'state'
    ]
});
