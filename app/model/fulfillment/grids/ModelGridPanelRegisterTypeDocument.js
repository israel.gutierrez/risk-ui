Ext.define('DukeSource.model.fulfillment.grids.ModelGridPanelRegisterTypeDocument', {
    extend: 'Ext.data.Model',
    fields: [
        'idTypeDocument'
        , 'description'
        , 'state'
    ]
});
