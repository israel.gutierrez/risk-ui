Ext.define('DukeSource.model.fulfillment.grids.ModelGridPanelRegisterParameterPerformantsTime', {
    extend: 'Ext.data.Model',
    fields: [
        'idParameterPerformantsTime'
        , 'description'
        , 'percentByDays'
        , 'hexadecimalColorDays'
        , 'descriptionByPerformance'
        , 'numberInitialPercentage'
        , 'numberFinalPercentage'
        , 'hexadecimalColorPerformance'
        , 'state'
    ]
});
