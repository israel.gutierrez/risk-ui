Ext.define('DukeSource.model.fulfillment.grids.ModelGridPanelRegisterPropertyEmail', {
    extend: 'Ext.data.Model',
    fields: [
        'id'
        , 'emailAccount'
        , 'password'
        , 'host'
        , 'port'
        , 'permitTTLS'
        , 'authorization'
        , 'to'
        , 'userName'
        , 'state'
    ]
});
