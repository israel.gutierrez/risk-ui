Ext.define('DukeSource.model.fulfillment.grids.ModelGridPanelRegisterStateDocument', {
    extend: 'Ext.data.Model',
    fields: [
        'idStateDocument'
        , 'description'
        , 'state'
    ]
});
