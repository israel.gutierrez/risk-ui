Ext.define('DukeSource.model.fulfillment.grids.ModelGridPanelRegisterPriority', {
    extend: 'Ext.data.Model',
    fields: [
        'idPriority'
        , 'description'
        , 'abreviature'
        , 'state'
    ]
});
