Ext.define('DukeSource.model.fulfillment.grids.ModelGridPanelDocumentProcess', {
    extend: 'Ext.data.Model',
    fields: [
        'id'
        , 'name'
        , 'description'
        , 'state'
    ]
});
