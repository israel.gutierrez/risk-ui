Ext.define('DukeSource.model.fulfillment.grids.ModelGridPanelDocumentFinished', {
    extend: 'Ext.data.Model',
    fields: [
        'id'
        , 'name'
        , 'description'
        , 'state'
    ]
});
