Ext.define('DukeSource.model.fulfillment.grids.ModelGridPanelDocumentPending', {
    extend: 'Ext.data.Model',
    fields: [
        'id'
        , 'name'
        , 'description'
        , 'state'
    ]
});
