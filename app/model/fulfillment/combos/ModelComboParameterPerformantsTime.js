Ext.define('DukeSource.model.fulfillment.combos.ModelComboParameterPerformantsTime', {
    extend: 'Ext.data.Model',
    fields: [
        'idParameterPerformantsTime',
        'description'
    ]
});
