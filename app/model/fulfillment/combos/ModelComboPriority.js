Ext.define('DukeSource.model.fulfillment.combos.ModelComboPriority', {
    extend: 'Ext.data.Model',
    fields: [
        'idPriority',
        'description'
    ]
});
