Ext.define('DukeSource.model.fulfillment.combos.ModelComboStateDocument', {
    extend: 'Ext.data.Model',
    fields: [
        'idStateDocument',
        'description'
    ]
});
