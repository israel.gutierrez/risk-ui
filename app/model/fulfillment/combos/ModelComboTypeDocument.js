Ext.define('DukeSource.model.fulfillment.combos.ModelComboTypeDocument', {
    extend: 'Ext.data.Model',
    fields: [
        'idTypeDocument',
        'description'
    ]
});
