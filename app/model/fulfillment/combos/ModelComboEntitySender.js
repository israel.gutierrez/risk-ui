Ext.define('DukeSource.model.fulfillment.combos.ModelComboEntitySender', {
    extend: 'Ext.data.Model',
    fields: [
        'idEntitySender',
        'description'
    ]
});
