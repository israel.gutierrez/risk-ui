Ext.define("DukeSource.lib.Ajax", {
  extend: "Ext.data.Connection",
  singleton: true,
  constructor: function(e) {
    this.callParent([e]);
    this.on("beforerequest", function() {
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_MESSAGE,
        closable: false,
        msg: "GIRO - TOPRISK, esta procesando...",
        width: 300,
        icon: Ext.Msg.INFO,
        wait: true,
        waitConfig: { interval: 200 }
      });
    });
    this.on("requestcomplete", function(conn, response, options) {
      Ext.MessageBox.hide();
    });
    this.on("requestexception", function(conn, response, options) {
      if (response.status === 901 || response.status === 302) {
        Ext.Msg.alert(
          "Session Expired",
          "El tiempo de actividad ha expirado. Por favor ingrese nuevamente.",
          function(btn, text) {
            if (btn === "ok") {
              window.location.href = "logout";
            }
          }
        );
      }
    });
  }
});
