Ext.define("DukeSource.store.StyleFields", {
  extend: "Ext.data.Store",
  alias: "store.StyleFields",
  fields: ["id", codexCompany],
  autoLoad: true,
  proxy: {
    type: "ajax",
    url: "app/i18n/style/" + langLocale + ".json",
    reader: {
      type: "json",
      root: "data"
    }
  }
});
