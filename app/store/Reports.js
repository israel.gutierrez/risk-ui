Ext.define("DukeSource.store.Reports", {
  extend: "Ext.data.Store",
  alias: "store.Reports",
  fields: ["id", codexCompany],
  autoLoad: true,
  proxy: {
    type: "ajax",
    url: "app/i18n/reports/" + langLocale + ".json",
    reader: {
      type: "json",
      root: "data"
    }
  }
});
