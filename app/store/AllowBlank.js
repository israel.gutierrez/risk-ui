Ext.define("DukeSource.store.AllowBlank", {
  extend: "Ext.data.Store",
  alias: "store.AllowBlank",
  fields: ["id", codexCompany],
  autoLoad: true,
  proxy: {
    type: "ajax",
    url: "app/i18n/allow_blank/" + langLocale + ".json",
    reader: {
      type: "json",
      root: "data"
    }
  }
});
