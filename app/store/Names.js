Ext.define("DukeSource.store.Names", {
  extend: "Ext.data.Store",
  alias: "store.Names",
  fields: ["id", codexCompany],
  proxy: {
    type: "ajax",
    url: "app/i18n/names/" + langLocale + ".json",
    reader: {
      type: "json",
      root: "data"
    }
  }
});
