Ext.define("DukeSource.store.TreeStore", {
  extend: "Ext.data.TreeStore",
  alias: "store.TreeStore",
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListTreePanel.htm",
    reader: {
      type: "json",
      root: "instanceList",
      successProperty: "success"
    }
  },
  root: { expanded: true }
});
