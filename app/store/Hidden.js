Ext.define("DukeSource.store.Hidden", {
  extend: "Ext.data.Store",
  alias: "store.Hidden",
  fields: ["id", codexCompany],
  autoLoad: true,
  proxy: {
    type: "ajax",
    url: "app/i18n/hidden/" + langLocale + ".json",
    reader: {
      type: "json",
      root: "data"
    }
  }
});
