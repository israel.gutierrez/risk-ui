Ext.define("DukeSource.store.fulfillment.grids.StoreGridPanelDocumentPending", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.fulfillment.grids.ModelGridPanelDocumentPending",
  autoSync: true,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListRolesActives.htm",
    method: "POST",
    extraParams: {
      propertyOrder: "nameRole"
    },
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
