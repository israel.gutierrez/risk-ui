Ext.define(
  "DukeSource.store.fulfillment.grids.StoreGridPanelRegisterParameterPerformantsTime",
  {
    extend: "Ext.data.Store",
    model:
      "DukeSource.model.fulfillment.grids.ModelGridPanelRegisterParameterPerformantsTime",
    autoLoad: false,
    proxy: {
      actionMethods: {
        create: "POST",
        read: "POST",
        update: "POST"
      },
      type: "ajax",
      url: "http://localhost:9000/giro/loadGridDefault.htm",
      reader: {
        type: "json",
        totalProperty: "totalCount",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
