Ext.define(
  "DukeSource.store.fulfillment.grids.StoreGridPanelDocumentFinished",
  {
    extend: "Ext.data.Store",
    model: "DukeSource.model.fulfillment.grids.ModelGridPanelDocumentFinished",
    autoSync: true,
    proxy: {
      type: "ajax",
      url: "http://localhost:9000/giro/showListRolesActives.htm",
      method: "POST",
      extraParams: {
        propertyOrder: "nameRole"
      },
      reader: {
        type: "json",
        totalProperty: "totalCount",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
