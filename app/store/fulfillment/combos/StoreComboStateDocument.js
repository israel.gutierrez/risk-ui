Ext.define("DukeSource.store.fulfillment.combos.StoreComboStateDocument", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.fulfillment.combos.ModelComboStateDocument",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListStateDocumentActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
