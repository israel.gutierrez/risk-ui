Ext.define("DukeSource.store.fulfillment.combos.StoreComboTypeDocument", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.fulfillment.combos.ModelComboTypeDocument",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListTypeDocumentActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
