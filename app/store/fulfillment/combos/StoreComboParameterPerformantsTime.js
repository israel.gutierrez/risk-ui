Ext.define(
  "DukeSource.store.fulfillment.combos.StoreComboParameterPerformantsTime",
  {
    extend: "Ext.data.Store",
    model:
      "DukeSource.model.fulfillment.combos.ModelComboParameterPerformantsTime",
    pageSize: 9999,
    proxy: {
      type: "ajax",
      url:
        "http://localhost:9000/giro/showListParameterPerformantsTimeActivesComboBox.htm",
      extraParams: {
        propertyOrder: "description"
      },
      reader: {
        type: "json",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
