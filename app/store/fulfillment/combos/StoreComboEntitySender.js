Ext.define("DukeSource.store.fulfillment.combos.StoreComboEntitySender", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.fulfillment.combos.ModelComboEntitySender",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListEntitySenderActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
