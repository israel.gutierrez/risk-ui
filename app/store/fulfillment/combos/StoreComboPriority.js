Ext.define("DukeSource.store.fulfillment.combos.StoreComboPriority", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.fulfillment.combos.ModelComboPriority",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListPriorityActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
