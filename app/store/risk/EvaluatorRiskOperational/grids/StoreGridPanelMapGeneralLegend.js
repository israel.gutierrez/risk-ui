Ext.define(
  "DukeSource.store.risk.EvaluatorRiskOperational.grids.StoreGridPanelMapGeneralLegend",
  {
    extend: "Ext.data.Store",
    model:
      "DukeSource.model.risk.EvaluatorRiskOperational.grids.ModelGridPanelMapGeneralLegend",
    proxy: {
      actionMethods: {
        create: "POST",
        read: "POST",
        update: "POST"
      },
      type: "ajax",
      url: "http://localhost:9000/giro/loadGridDefault.htm",
      reader: {
        type: "json",
        totalProperty: "totalCount",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
