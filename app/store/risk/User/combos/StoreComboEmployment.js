Ext.define("DukeSource.store.risk.User.combos.StoreComboEmployment", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.User.combos.ModelComboEmployment",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListEmploymentsActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
