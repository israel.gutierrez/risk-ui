Ext.define("DukeSource.store.risk.User.combos.StoreComboAgency", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.User.combos.ModelComboAgency",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListAgencyActivesComboBox.htm",
    extraParams: {
      propertyOrder: "a.description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
