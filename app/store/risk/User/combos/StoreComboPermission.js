Ext.define("DukeSource.store.risk.User.combos.StoreComboPermission", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.User.combos.ModelComboPermission",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListForeignKeysComboBox.htm",
    extraParams: {
      propertyOrder: "description",
      propertyFind: "identified",
      valueFind: "TYPEACCESS"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
