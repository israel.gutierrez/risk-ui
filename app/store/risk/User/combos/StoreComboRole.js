Ext.define("DukeSource.store.risk.User.combos.StoreComboRole", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.User.combos.ModelComboRole",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListRolesActivesComboBox.htm",
    extraParams: {
      propertyOrder: "nameRole"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
