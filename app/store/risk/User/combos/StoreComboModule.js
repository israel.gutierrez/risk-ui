Ext.define("DukeSource.store.risk.User.combos.StoreComboModule", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.User.combos.ModelComboModule",
  pageSize: 9999,
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListModule.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
