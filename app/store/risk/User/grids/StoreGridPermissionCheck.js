Ext.define("DukeSource.store.risk.User.grids.StoreGridPermissionCheck", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.User.grids.ModelGridPermissionCheck",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/loadGridDefault.html",
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
