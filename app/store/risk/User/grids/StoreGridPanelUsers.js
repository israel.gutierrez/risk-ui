Ext.define("DukeSource.store.risk.User.grids.StoreGridPanelUsers", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.User.grids.ModelGridPanelUsers",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListUserActives.htm",
    extraParams: {
      propertyOrder: "fullName"
    },
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
