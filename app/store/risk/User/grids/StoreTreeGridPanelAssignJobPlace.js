Ext.define(
  "DukeSource.store.risk.User.grids.StoreTreeGridPanelAssignJobPlace",
  {
    extend: "Ext.data.TreeStore",
    model: "DukeSource.model.risk.User.grids.ModelTreeGridPanelAssignJobPlace",
    proxy: {
      type: "ajax",
      url: "http://localhost:9000/giro/showJobPlaceByHierArchy.htm"
    },
    root: {
      text: "Tree display of Countries",
      id: "myTree",
      expanded: true
    },
    folderSort: true,
    sorters: [
      {
        property: "text",
        direction: "ASC"
      }
    ],
    listeners: {
      load: function(tree, node, records) {
        if (node.get("checked")) {
          node.eachChild(function(childNode) {
            childNode.set("checked", true);
          });
        }
      }
    }
  }
);
