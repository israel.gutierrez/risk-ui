Ext.define("DukeSource.store.risk.User.grids.StoreGridPanelRegisterCompany", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.User.grids.ModelGridPanelRegisterCompany",
  proxy: {
    actionMethods: {
      create: "POST",
      read: "POST",
      update: "POST"
    },
    type: "ajax",
    url: "http://localhost:9000/giro/showListCompany.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
