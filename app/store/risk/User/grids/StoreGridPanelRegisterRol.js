Ext.define("DukeSource.store.risk.User.grids.StoreGridPanelRegisterRol", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.User.grids.ModelGridPanelRegisterRol",
  autoSync: true,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListRolesActives.htm",
    method: "POST",
    extraParams: {
      propertyOrder: "nameRole"
    },
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
