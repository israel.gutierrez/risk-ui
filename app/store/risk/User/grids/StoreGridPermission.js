Ext.define("DukeSource.store.risk.User.grids.StoreGridPermission", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.User.grids.ModelGridPermission",
  //  autoLoad: true,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/findEmpresa.html",
    method: "POST",
    extraParams: {
      codigoArticulo: "",
      descripcionArticulo: ""
    },
    reader: {
      type: "json",
      totalProperty: "totalCount",
      root: "data",
      successProperty: "success"
    }
  }
});
