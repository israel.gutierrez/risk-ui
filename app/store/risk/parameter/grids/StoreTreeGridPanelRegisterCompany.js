Ext.define(
  "DukeSource.store.risk.parameter.grids.StoreTreeGridPanelRegisterCompany",
  {
    extend: "Ext.data.TreeStore",
    model:
      "DukeSource.model.risk.parameter.grids.ModelTreeGridPanelRegisterCompany",
    proxy: {
      type: "ajax",
      url: "http://localhost:9000/giro/showListCompanyActives.htm",
      reader: {
        type: "json",
        totalProperty: "totalCount",
        successProperty: "success"
      },
      extraParams: {
        node: "root",
        depth: 0
      }
    },
    root: {
      text: "",
      id: "myTree",
      expanded: true
    },
    folderSort: true,
    sorters: [
      {
        property: "text",
        direction: "ASC"
      }
    ],
    listeners: {
      load: function(tree, node, records) {
        if (node.get("checked")) {
          node.eachChild(function(childNode) {
            childNode.set("checked", true);
          });
        }
      }
    }
  }
);
