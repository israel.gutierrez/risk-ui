Ext.define(
  "DukeSource.store.risk.parameter.grids.StoreGridPanelRegisterIndicatorType",
  {
    extend: "Ext.data.Store",
    model:
      "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterIndicatorType",
    proxy: {
      actionMethods: {
        create: "POST",
        read: "POST",
        update: "POST"
      },
      type: "ajax",
      url: "http://localhost:9000/giro/loadGridDefault.htm",
      reader: {
        type: "json",
        totalProperty: "totalCount",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
