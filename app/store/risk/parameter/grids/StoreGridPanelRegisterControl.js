Ext.define(
  "DukeSource.store.risk.parameter.grids.StoreGridPanelRegisterControl",
  {
    extend: "Ext.data.Store",
    model:
      "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterControl",
    proxy: {
      actionMethods: {
        create: "POST",
        read: "POST",
        update: "POST"
      },
      type: "ajax",
      url: "http://localhost:9000/giro/loadGridDefault.htm",
      params: {
        start: 0,
        limit: 500
      },
      reader: {
        type: "json",
        totalProperty: "totalCount",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
