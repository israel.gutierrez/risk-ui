Ext.define(
  "DukeSource.store.risk.parameter.grids.StoreTreeGridPanelRegisterFeatureProcess",
  {
    extend: "Ext.data.TreeStore",
    model:
      "DukeSource.model.risk.parameter.grids.ModelTreeGridPanelRegisterFeatureProcess",
    proxy: {
      type: "ajax",
      url: "http://localhost:9000/giro/findFeatureProcess.htm"
    },
    root: {
      text: "Tree display of Countries",
      id: "myTree",
      expanded: true
    },
    folderSort: true,
    sorters: [
      {
        property: "text",
        direction: "ASC"
      }
    ],
    listeners: {
      load: function(tree, node, records) {
        if (node.get("checked")) {
          node.eachChild(function(childNode) {
            childNode.set("checked", true);
          });
        }
      }
    }
  }
);
