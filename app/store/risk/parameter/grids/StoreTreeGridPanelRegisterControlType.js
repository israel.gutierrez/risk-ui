Ext.define(
  "DukeSource.store.risk.parameter.grids.StoreTreeGridPanelRegisterControlType",
  {
    extend: "Ext.data.TreeStore",
    model:
      "DukeSource.model.risk.parameter.grids.ModelTreeGridPanelRegisterControlType",
    proxy: {
      type: "ajax",
      url: "http://localhost:9000/giro/findControlType.htm",
      extraParams: {
        propertyOrder: "valuetypificationControl"
      },
      reader: {
        type: "json",
        root: "data",
        successProperty: "success"
      }
    },
    root: {
      text: "root",
      id: "myTree",
      expanded: true
    },
    folderSort: true,
    sorters: [
      {
        property: "text",
        direction: "ASC"
      }
    ],
    listeners: {
      load: function(tree, node, records) {
        if (node.get("checked")) {
          node.eachChild(function(childNode) {
            childNode.set("checked", true);
          });
        }
      }
    }
  }
);
