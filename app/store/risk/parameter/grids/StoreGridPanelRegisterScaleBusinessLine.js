Ext.define('DukeSource.store.risk.parameter.grids.StoreGridPanelRegisterScaleBusinessLine', {
    extend: 'Ext.data.Store',
    model: 'DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterScaleBusinessLine',
    proxy: {
        actionMethods: {
            create: 'POST',
            read: 'POST',
            update: 'POST'
        },
        type: 'ajax',
        url: 'loadGridDefault.htm',
        reader: {
            type: 'json',
            totalProperty: 'totalCount',
            root: 'data',
            successProperty: 'success'
        }
    }
});
