Ext.define(
  "DukeSource.store.risk.parameter.grids.StoreTreeGridPanelRegisterProcessType",
  {
    extend: "Ext.data.TreeStore",
    model:
      "DukeSource.model.risk.parameter.grids.ModelTreeGridPanelRegisterProcessType",
    proxy: {
      type: "ajax",
      url: "http://localhost:9000/giro/showListProcessTypeActives.htm",
      extraParams: {
        depth: "0",
        check: true
      }
    },
    folderSort: true,
    sorters: [
      {
        property: "text",
        direction: "ASC"
      }
    ],
    root: {
      text: "",
      id: "root",
      node: "root",
      depth: "0",
      expanded: true
    }
  }
);
