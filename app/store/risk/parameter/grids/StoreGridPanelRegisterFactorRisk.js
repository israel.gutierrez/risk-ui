Ext.define(
  "DukeSource.store.risk.parameter.grids.StoreGridPanelRegisterFactorRisk",
  {
    extend: "Ext.data.TreeStore",
    model:
      "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterFactorRisk",
    autoLoad: true,
    proxy: {
      type: "ajax",
      url: "http://localhost:9000/giro/showListFactorRiskActives.htm",
      extraParams: {
        node: "root",
        depth: 0
      }
    },
    root: {
      id: "root",
      description: "",
      expanded: true
    },
    folderSort: true,
    sorters: [
      {
        property: "description",
        direction: "ASC"
      }
    ]
  }
);
