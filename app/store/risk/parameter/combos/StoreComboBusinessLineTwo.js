Ext.define("DukeSource.store.risk.parameter.combos.StoreComboBusinessLineTwo", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.parameter.combos.ModelComboBusinessLineTwo",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListBusinessLineTwoActivesComboBox.htm",
    extraParams: {
      valueFind: "valueFind"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
