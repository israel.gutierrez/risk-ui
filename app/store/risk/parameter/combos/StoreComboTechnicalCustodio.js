Ext.define(
  "DukeSource.store.risk.parameter.combos.StoreComboTechnicalCustodio",
  {
    extend: "Ext.data.Store",
    model: "DukeSource.model.risk.parameter.combos.ModelComboTechnicalCustodio",
    pageSize: 9999,
    proxy: {
      type: "ajax",
      url:
        "http://localhost:9000/giro/showListTechnicalCustodioActivesComboBox.htm",
      extraParams: {
        propertyOrder: "description"
      },
      reader: {
        type: "json",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
