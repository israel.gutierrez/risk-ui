Ext.define(
  "DukeSource.store.risk.parameter.combos.StoreComboDetailFrequencyFeature",
  {
    extend: "Ext.data.Store",
    model:
      "DukeSource.model.risk.parameter.combos.ModelComboDetailFrequencyFeature",
    pageSize: 9999,
    proxy: {
      type: "ajax",
      url:
        "http://localhost:9000/giro/showListDetailFrequencyFeatureActivesComboBox.htm",
      extraParams: {
        propertyOrder: "description"
      },
      reader: {
        type: "json",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
