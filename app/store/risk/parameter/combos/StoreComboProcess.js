Ext.define("DukeSource.store.risk.parameter.combos.StoreComboProcess", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.parameter.combos.ModelComboProcess",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListProcessActives.htm",
    extraParams: {
      valueFind: "1"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
