Ext.define(
  "DukeSource.store.risk.parameter.combos.StoreComboDetailImpactFeature",
  {
    extend: "Ext.data.Store",
    model:
      "DukeSource.model.risk.parameter.combos.ModelComboDetailImpactFeature",
    pageSize: 9999,
    proxy: {
      type: "ajax",
      url:
        "http://localhost:9000/giro/showListDetailImpactFeatureActivesComboBox.htm",
      extraParams: {
        propertyOrder: "description"
      },
      reader: {
        type: "json",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
