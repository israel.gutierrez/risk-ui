Ext.define("DukeSource.store.risk.parameter.combos.StoreComboImpactFeature", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.parameter.combos.ModelComboImpactFeature",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListImpactFeatureActivesComboBox.htm",
    extraParams: {
      propertyOrder: "if.description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
