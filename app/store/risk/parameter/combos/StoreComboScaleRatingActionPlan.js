Ext.define(
  "DukeSource.store.risk.parameter.combos.StoreComboScaleRatingActionPlan",
  {
    extend: "Ext.data.Store",
    model:
      "DukeSource.model.risk.parameter.combos.ModelComboScaleRatingActionPlan",
    pageSize: 9999,
    proxy: {
      type: "ajax",
      url:
        "http://localhost:9000/giro/showListScaleRatingActionPlanActives.htm",
      extraParams: {
        propertyOrder: "description"
      },
      reader: {
        type: "json",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
