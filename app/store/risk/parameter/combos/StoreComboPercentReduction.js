Ext.define(
  "DukeSource.store.risk.parameter.combos.StoreComboPercentReduction",
  {
    extend: "Ext.data.Store",
    model: "DukeSource.model.risk.parameter.combos.ModelComboPercentReduction",
    pageSize: 9999,
    proxy: {
      type: "ajax",
      url: "http://localhost:9000/giro/showListPercentReductionActives.htm",
      extraParams: {
        propertyOrder: "description"
      },
      reader: {
        type: "json",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
