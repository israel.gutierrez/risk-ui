Ext.define("DukeSource.store.risk.parameter.combos.StoreComboWorkArea", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.parameter.combos.ModelComboWorkArea",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListWorkAreaActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
