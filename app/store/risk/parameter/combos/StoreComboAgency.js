Ext.define("DukeSource.store.risk.parameter.combos.StoreComboAgency", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.parameter.combos.ModelComboAgency",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListAgencyActives.htm",
    extraParams: {
      propertyOrder: "a.description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
