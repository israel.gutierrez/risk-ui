Ext.define("DukeSource.store.risk.parameter.combos.StoreComboAccountPlan", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.parameter.combos.ModelComboAccountPlan",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListAccountPlanActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
