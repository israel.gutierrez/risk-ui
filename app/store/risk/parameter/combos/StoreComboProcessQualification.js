Ext.define(
  "DukeSource.store.risk.parameter.combos.StoreComboProcessQualification",
  {
    extend: "Ext.data.Store",
    model:
      "DukeSource.model.risk.parameter.combos.ModelComboProcessQualification",
    pageSize: 9999,
    proxy: {
      type: "ajax",
      url:
        "http://localhost:9000/giro/showListProcessQualificationActivesComboBox.htm",
      extraParams: {
        propertyOrder: "description"
      },
      reader: {
        type: "json",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
