Ext.define("DukeSource.store.risk.parameter.combos.StoreComboActionPlan", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.parameter.combos.ModelComboActionPlan",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListActionPlanActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
