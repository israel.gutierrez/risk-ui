Ext.define("DukeSource.store.risk.parameter.combos.StoreComboProduct", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.parameter.combos.ModelComboProduct",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListBusinessLineThreeActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
