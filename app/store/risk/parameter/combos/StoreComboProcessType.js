Ext.define("DukeSource.store.risk.parameter.combos.StoreComboProcessType", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.parameter.combos.ModelComboProcessType",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListProcessTypeActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
