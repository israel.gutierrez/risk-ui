Ext.define(
  "DukeSource.store.risk.parameter.combos.StoreComboFrequencyFeature",
  {
    extend: "Ext.data.Store",
    model: "DukeSource.model.risk.parameter.combos.ModelComboFrequencyFeature",
    pageSize: 9999,
    proxy: {
      type: "ajax",
      url:
        "http://localhost:9000/giro/showListFrequencyFeatureActivesComboBox.htm",
      extraParams: {
        propertyOrder: "description"
      },
      reader: {
        type: "json",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
