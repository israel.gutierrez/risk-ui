Ext.define(
  "DukeSource.store.risk.parameter.combos.StoreComboInsuranceCompany",
  {
    extend: "Ext.data.Store",
    model: "DukeSource.model.risk.parameter.combos.ModelComboInsuranceCompany",
    pageSize: 9999,
    proxy: {
      type: "ajax",
      url: "http://localhost:9000/giro/showListInsuranceCompanyActives.htm",
      extraParams: {
        propertyOrder: "description"
      },
      reader: {
        type: "json",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
