Ext.define(
  "DukeSource.store.risk.parameter.combos.StoreComboTypeRiskEvaluation",
  {
    extend: "Ext.data.Store",
    model:
      "DukeSource.model.risk.parameter.combos.ModelComboTypeRiskEvaluation",
    pageSize: 9999,
    proxy: {
      type: "ajax",
      url:
        "http://localhost:9000/giro/showListTypeRiskEvaluationActivesComboBox.htm",
      extraParams: {
        propertyOrder: "description"
      },
      reader: {
        type: "json",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
