Ext.define(
  "DukeSource.store.risk.kri.grids.StoreTreeGridPanelRegisterDetailCriteriaKri",
  {
    extend: "Ext.data.TreeStore",
    model:
      "DukeSource.model.risk.kri.grids.ModelTreeGridPanelRegisterDetailCriteriaKri",
    proxy: {
      type: "ajax",
      url: "http://localhost:9000/giro/findDetailCriteriaKri.htm",
      reader: {
        type: "json",
        totalProperty: "totalCount",
        root: "data",
        successProperty: "success"
      }
    },
    root: {
      text: "Tree display of Countries",
      id: "myTree",
      expanded: true
    },
    folderSort: true,
    sorters: [
      {
        property: "text",
        direction: "ASC"
      }
    ],
    listeners: {
      load: function(tree, node, records) {
        if (node.get("checked")) {
          node.eachChild(function(childNode) {
            childNode.set("checked", true);
          });
        }
      }
    }
  }
);
