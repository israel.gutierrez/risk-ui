Ext.define(
  "DukeSource.store.risk.kri.grids.StoreGridPanelRegisterQuantityValuesWeights",
  {
    extend: "Ext.data.Store",
    model:
      "DukeSource.model.risk.kri.grids.ModelGridPanelRegisterQuantityValuesWeights",
    proxy: {
      actionMethods: {
        create: "POST",
        read: "POST",
        update: "POST"
      },
      type: "ajax",
      url: "http://localhost:9000/giro/loadGridDefault.htm",
      reader: {
        type: "json",
        totalProperty: "totalCount",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
