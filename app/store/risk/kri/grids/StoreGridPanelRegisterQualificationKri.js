Ext.define(
  "DukeSource.store.risk.kri.grids.StoreGridPanelRegisterQualificationKri",
  {
    extend: "Ext.data.Store",
    model:
      "DukeSource.model.risk.kri.grids.ModelGridPanelRegisterQualificationKri",
    proxy: {
      actionMethods: {
        create: "POST",
        read: "POST",
        update: "POST"
      },
      type: "ajax",
      url: "http://localhost:9000/giro/loadGridDefault.htm",
      reader: {
        type: "json",
        totalProperty: "totalCount",
        root: "data",
        successProperty: "success"
      }
    }
  }
);
