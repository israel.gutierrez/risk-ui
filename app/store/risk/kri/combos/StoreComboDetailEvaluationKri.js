Ext.define("DukeSource.store.risk.kri.combos.StoreComboDetailEvaluationKri", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.kri.combos.ModelComboDetailEvaluationKri",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListDetailEvaluationKriActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
