Ext.define("DukeSource.store.risk.kri.combos.StoreComboQuantityValuesWeights", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.kri.combos.ModelComboQuantityValuesWeights",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListQuantityValuesWeightsActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
