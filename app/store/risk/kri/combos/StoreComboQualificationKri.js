Ext.define("DukeSource.store.risk.kri.combos.StoreComboQualificationKri", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.kri.combos.ModelComboQualificationKri",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListQualificationKriActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
