Ext.define("DukeSource.store.risk.kri.combos.StoreComboWeighingKri", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.kri.combos.ModelComboWeighingKri",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListWeighingKriActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
