Ext.define("DukeSource.store.risk.kri.combos.StoreComboDetailWeighingKri", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.kri.combos.ModelComboDetailWeighingKri",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListDetailWeighingKriActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
