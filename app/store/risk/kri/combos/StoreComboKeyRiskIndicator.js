Ext.define("DukeSource.store.risk.kri.combos.StoreComboKeyRiskIndicator", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.kri.combos.ModelComboKeyRiskIndicator",
  pageSize: 9999,
  autoLoad: false,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListKeyRiskIndicatorActives.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
