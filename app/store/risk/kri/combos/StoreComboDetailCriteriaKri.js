Ext.define("DukeSource.store.risk.kri.combos.StoreComboDetailCriteriaKri", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.kri.combos.ModelComboDetailCriteriaKri",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListDetailCriteriaKriActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
