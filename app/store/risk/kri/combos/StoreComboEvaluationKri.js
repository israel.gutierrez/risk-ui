Ext.define("DukeSource.store.risk.kri.combos.StoreComboEvaluationKri", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.kri.combos.ModelComboEvaluationKri",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url: "http://localhost:9000/giro/showListEvaluationKriActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
