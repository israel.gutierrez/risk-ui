Ext.define("DukeSource.store.risk.kri.combos.StoreComboCalculatingFrequency", {
  extend: "Ext.data.Store",
  model: "DukeSource.model.risk.kri.combos.ModelComboCalculatingFrequency",
  pageSize: 9999,
  proxy: {
    type: "ajax",
    url:
      "http://localhost:9000/giro/showListCalculatingFrequencyActivesComboBox.htm",
    extraParams: {
      propertyOrder: "description"
    },
    reader: {
      type: "json",
      root: "data",
      successProperty: "success"
    }
  }
});
