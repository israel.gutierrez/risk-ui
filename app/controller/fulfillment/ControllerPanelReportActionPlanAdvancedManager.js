Ext.define(
  "DukeSource.controller.fulfillment.ControllerPanelReportActionPlanAdvancedManager",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboTypeRiskEvaluation",
      "risk.parameter.combos.ViewComboProcess"
    ],
    init: function() {
      this.control({
        "[action=generateReportAPlanAdvancedManagerXls]": {
          click: this._onGenerateReportAPlanAdvancedManagerXls
        }
      });
    },

    _onGenerateReportAPlanAdvancedManagerXls: function(btn) {
      var panel = btn.up("container[name=generalContainer]");

      var typeEvaluation = panel
        .down("#typeEvaluation")
        .getValue()
        .toString();
      var dateInit =
        panel.down("#dateInit").getValue() == undefined
          ? "01/01/1990"
          : panel.down("#dateInit").getRawValue();
      var dateEnd =
        panel.down("#dateEnd").getValue() == undefined
          ? "31/12/2999"
          : panel.down("#dateEnd").getRawValue();
      var dateInitInit =
        panel.down("#dateInitInit").getValue() == undefined
          ? "01/01/1990"
          : panel.down("#dateInitInit").getRawValue();
      var dateInitEnd =
        panel.down("#dateInitEnd").getValue() == undefined
          ? "31/12/2999"
          : panel.down("#dateInitEnd").getRawValue();
      var dateEndInit =
        panel.down("#dateEndInit").getValue() == undefined
          ? "01/01/1990"
          : panel.down("#dateEndInit").getRawValue();
      var dateEndEnd =
        panel.down("#dateEndEnd").getValue() == undefined
          ? "31/12/2999"
          : panel.down("#dateEndEnd").getRawValue();
      var dateMonInit =
        panel.down("#dateMonInit").getValue() == undefined
          ? "01/01/1990"
          : panel.down("#dateMonInit").getRawValue();
      var dateMonEnd =
        panel.down("#dateMonEnd").getValue() == undefined
          ? "31/12/2999"
          : panel.down("#dateMonEnd").getRawValue();

      var value =
        typeEvaluation +
        ";" +
        dateInit +
        ";" +
        dateEnd +
        ";" +
        dateInitInit +
        ";" +
        dateInitEnd +
        ";" +
        dateEndInit +
        ";" +
        dateEndEnd +
        ";" +
        dateMonInit +
        ";" +
        dateMonEnd +
        ";";

      var panelReport = panel.down("panel[name=panelReport]");
      panelReport.removeAll();
      panelReport.add({
        xtype: "component",
        autoEl: {
          tag: "iframe",
          src:
            "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
            value +
            "&names=" +
            "typeEvaluation,dateInit,dateEnd,dateInitInit,dateInitEnd," +
            "dateEndInit,dateEndEnd,dateMonInit,dateMonEnd" +
            "&types=" +
            "String,Date,Date,Date,Date,Date,Date,Date,Date" +
            "&nameReport=" +
            "ROSE00001XLS" +
            "&typeReport=noStandard"
        }
      });
    }
  }
);
