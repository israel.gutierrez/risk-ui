Ext.define(
  "DukeSource.controller.fulfillment.ControllerTreeGridPanelCollaborator",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [],
    init: function() {
      this.control({
        "ViewTreeGridPanelCollaborator grid[itemId=gridTask]": {
          itemclick: this._onItemClickGrid
        },
        "[action=saveTaskCollaborator]": {
          click: this._onSaveTaskCollaborator
        },
        "[action=addAdvanceActivity]": {
          click: this._onAddAdvanceActivity
        },
        "[action=attachFileAdvanceActivity]": {
          click: this._onAttachFileAdvanceActivity
        },
        "[action=modifyAdvanceActivity]": {
          click: this._onModifyAdvanceActivity
        },
        "[action=attachFileTask]": {
          click: this._onAttachFileTask
        }
      });
    },

    _onAttachFileTask: function() {
      var gridTask = Ext.ComponentQuery.query(
        "ViewTreeGridPanelCollaborator"
      )[0].down("#gridTask");
      var record = gridTask.getSelectionModel().getSelection()[0];
      var idDocument = record.data["idDocument"];
      var idScheduleDocument = record.data["idScheduleDocument"];
      var attachWindow = Ext.create(
        "DukeSource.view.fulfillment.window.ViewWindowViewAttachDocument",
        {
          modal: true,
          row: record,
          saveFile:
            "http://localhost:9000/giro/saveFileAttachmentsSchedule.htm",
          deleteFile:
            "http://localhost:9000/giro/deleteFileAttachmentsSchedule.htm",
          searchGridTrigger: "searchGridFileAttachmentDocument",
          src: "http://localhost:9000/giro/downloadFileAttachmentsSchedule.htm",
          params: [
            "idDocument",
            "idScheduleDocument",
            "idFileAttachment",
            "nameFile"
          ],
          buttons: [
            {
              text: "SALIR",
              iconCls: "logout",
              handler: function() {
                attachWindow.close();
              }
            }
          ]
        }
      ).show();
      attachWindow.idDocument = idDocument;
      attachWindow.idScheduleDocument = idScheduleDocument;
      attachWindow.down("grid").store.getProxy().extraParams = {
        idDocument: idDocument,
        idScheduleDocument: idScheduleDocument
      };
      attachWindow.down("grid").store.getProxy().url =
        "http://localhost:9000/giro/showListFileAttachmentsSchedule.htm";
      attachWindow
        .down("grid")
        .down("pagingtoolbar")
        .moveFirst();
    },
    _onModifyAdvanceActivity: function() {
      if (
        Ext.ComponentQuery.query("ViewWindowReportAdvance")["0"] === undefined
      ) {
        var gridAdvanceActivity = Ext.ComponentQuery.query(
          "ViewTreeGridPanelCollaborator"
        )[0].down("#gridAdvanceActivity");
        var record = gridAdvanceActivity.getSelectionModel().getSelection()[0];

        if (
          category === DukeSource.global.GiroConstants.COLLABORATOR &&
          record.raw["stateRevision"] ===
            DukeSource.global.GiroConstants.PROCESS
        ) {
          var advanceWindow = Ext.create(
            "DukeSource.view.fulfillment.window.ViewWindowReportAdvance",
            {
              buttons: [
                {
                  text: "Guardar",
                  iconCls: "save",
                  scale: "medium",
                  action: "saveTaskCollaborator"
                },
                {
                  text: "Salir",
                  iconCls: "logout",
                  scale: "medium",
                  handler: function() {
                    advanceWindow.close();
                  }
                }
              ],
              buttonAlign: "center"
            }
          );
          advanceWindow
            .down("form")
            .getForm()
            .setValues(record.raw);
          advanceWindow
            .down("textfield[name=idActionPlan]")
            .setValue(record.data["idActionPlan"]);
          advanceWindow
            .down("textfield[name=idTaskActionPlan]")
            .setValue(record.data["idTaskActionPlan"]);
          advanceWindow
            .down("textfield[name=idAdvanceActivity]")
            .setValue(record.data["idAdvanceActivity"]);
          //advanceWindow.down('datefield').setMinValue(record.data['dateReception']);
          //advanceWindow.down('datefield').setMaxValue(record.data['dateEnd']);
          advanceWindow
            .down("textfield[name=emailManager]")
            .setValue(record.data["emailUserEndAssignment"]); //emailUserEndAssignment
          advanceWindow.show();
          advanceWindow.down("UpperCaseTextArea").focus(false, 100);
          if (category === DukeSource.global.GiroConstants.GESTOR)
            advanceWindow
              .down("textfield[name=stateRevision]")
              .setValue(DukeSource.global.GiroConstants.REVISED);
          advanceWindow.show();
        } else {
          DukeSource.global.DirtyView.messageWarning(
            DukeSource.global.GiroMessages.MESSAGE_ADVANCE_VERIFIED
          );
        }
      } else {
      }
    },
    _onAttachFileAdvanceActivity: function() {
      var gridAdvanceActivity = Ext.ComponentQuery.query(
        "ViewTreeGridPanelCollaborator"
      )[0].down("#gridAdvanceActivity");
      var row = gridAdvanceActivity.getSelectionModel().getSelection()[0];
      var k = Ext.create(
        "DukeSource.view.fulfillment.window.ViewWindowViewAttachDocument",
        {
          row: row,
          saveFile:
            "http://localhost:9000/giro/saveFileAttachmentsOfActivityAdvance.htm",
          deleteFile:
            "http://localhost:9000/giro/deleteFileAttachmentsOfActivityAdvance.htm",
          searchGridTrigger: "searchGridFileAttachmentDocument",
          src:
            "http://localhost:9000/giro/downloadFileAttachmentsOfActivityAdvance.htm",
          params: [
            "idDocument",
            "idScheduleDocument",
            "idFileAttachment",
            "idAdvanceActivity",
            "nameFile"
          ]
        }
      );
      var gridFileAttachment = k.down("grid");
      gridFileAttachment.store.getProxy().extraParams = {
        idDocument: row.get("idDocument"),
        idScheduleDocument: row.get("idScheduleDocument"),
        idAdvanceActivity: row.get("idAdvanceActivity")
      };
      gridFileAttachment.store.getProxy().url =
        "http://localhost:9000/giro/showListFileAttachmentsOfActivityAdvance.htm";
      gridFileAttachment.down("pagingtoolbar").moveFirst();
      k.idDocument = row.get("idDocument");
      k.idScheduleDocument = row.get("idScheduleDocument");
      k.idAdvanceActivity = row.get("idAdvanceActivity");
      k.show();
    },
    _onAddAdvanceActivity: function(btn) {
      var gridTask = Ext.ComponentQuery.query(
        "ViewTreeGridPanelCollaborator"
      )[0].down("#gridTask");
      var record = gridTask.getSelectionModel().getSelection()[0];
      if (record === undefined) {
        DukeSource.global.DirtyView.messageWarning(
          DukeSource.global.GiroMessages.MESSAGE_ITEM
        );
      } else {
        if (
          Ext.ComponentQuery.query("ViewWindowReportAdvance")["0"] === undefined
        ) {
          var advanceWindow = Ext.create(
            "DukeSource.view.fulfillment.window.ViewWindowReportAdvance",
            {
              modal: true,
              buttons: [
                {
                  text: "Guardar",
                  iconCls: "save",
                  scale: "medium",
                  action: "saveTaskCollaborator"
                },
                {
                  text: "Salir",
                  iconCls: "logout",
                  scale: "medium",
                  handler: function() {
                    advanceWindow.close();
                  }
                }
              ],
              buttonAlign: "center"
            }
          );
          advanceWindow
            .down("textfield[name=idActionPlan]")
            .setValue(record.data["idActionPlan"]);
          advanceWindow
            .down("textfield[name=idTaskActionPlan]")
            .setValue(record.data["idTask"]);
          advanceWindow
            .down("#emailManager")
            .setValue(
              record.data["emailEmitted"] +
                ";" +
                record.data["emailReceptor"] +
                ";"
            );
          advanceWindow.show();
          advanceWindow.down("UpperCaseTextArea").focus(false, 100);
        }
      }
    },
    _onItemClickGrid: function(view, record, item, index, e) {
      var infoTask = Ext.ComponentQuery.query(
        "ViewTreeGridPanelCollaborator fieldset"
      )[0];
      infoTask.down("#descriptionTask").setValue(record.get("descriptionTask"));
      infoTask.down("#dateInitial").setValue(record.get("dateInitial"));
      infoTask.down("#dateExpire").setValue(record.get("dateExpire"));
      infoTask.down("#nameUser").setValue(record.get("fullNameReceptor"));

      var infoPlan = Ext.ComponentQuery.query(
        "ViewTreeGridPanelCollaborator fieldset"
      )[1];
      infoPlan.down("#codeActionPlan").setValue(record.get("codeActionPlan"));
      infoPlan
        .down("#descriptionActionPlan")
        .setValue(record.get("descriptionActionPlan"));
      infoPlan.down("#dateReception").setValue(record.get("dateReception"));
      infoPlan.down("#dateEnd").setValue(record.get("dateEnd"));
      infoPlan
        .down("#descriptionPriority")
        .setValue(record.get("fullNameEmitted"));

      var gridActivity = Ext.ComponentQuery.query(
        "ViewTreeGridPanelCollaborator"
      )[0].down("#gridAdvanceActivity");
      gridActivity.store.getProxy().url =
        "http://localhost:9000/giro/showListAdvanceActivityTask.htm?nameView=ViewTreeGridPanelCollaborator";
      gridActivity.store.getProxy().extraParams = {
        idActionPlan: record.data["idActionPlan"],
        idTaskActionPlan: record.data["idTask"]
      };
      Ext.ComponentQuery.query("ViewTreeGridPanelCollaborator")[0]
        .down("#ptAdvanceActivity")
        .doRefresh();
    },
    _onSaveTaskCollaborator: function(button) {
      var win = button.up("window");
      var form = win.down("form");
      if (form.getForm().isValid()) {
        form.getForm().submit({
          url:
            "http://localhost:9000/giro/saveAdvanceActivity.htm?nameView=ViewTreeGridPanelCollaborator",
          waitMsg: "GIRO - TOPRISK, esta procesando...",
          method: "POST",
          success: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (valor.success) {
              DukeSource.global.DirtyView.messageNormal(valor.message);
              win.close();

              var gridTask = Ext.ComponentQuery.query(
                "ViewTreeGridPanelCollaborator"
              )[0].down("#gridTask");
              var gridActivity = Ext.ComponentQuery.query(
                "ViewTreeGridPanelCollaborator"
              )[0].down("#gridAdvanceActivity");
              var record = gridTask.getSelectionModel().getSelection()[0];
              gridActivity.store.getProxy().url =
                "http://localhost:9000/giro/showListAdvanceActivityTask.htm?nameView=ViewTreeGridPanelCollaborator";
              gridActivity.store.getProxy().extraParams = {
                idActionPlan: record.data["idActionPlan"],
                idTaskActionPlan: record.data["idTask"]
              };
              Ext.ComponentQuery.query("ViewTreeGridPanelCollaborator")[0]
                .down("#ptAdvanceActivity")
                .moveFirst();
              gridTask.store.getProxy().url =
                "http://localhost:9000/giro/showListScheduleDocumentToCollaborator.htm?nameView=ViewTreeGridPanelCollaborator";
              gridTask.down("pagingtoolbar").doRefresh();
            } else {
              DukeSource.global.DirtyView.messageWarning(valor.message);
            }
          },
          failure: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (!valor.success) {
              DukeSource.global.DirtyView.messageWarning(valor.message);
            }
          }
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE
        );
      }
    }
  }
);
