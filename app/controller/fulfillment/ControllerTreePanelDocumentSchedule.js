Ext.define(
  "DukeSource.controller.fulfillment.ControllerTreePanelDocumentSchedule",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: ["fulfillment.grids.ViewGridHistoricalAdvance"],
    init: function() {
      this.control({
        "ViewTreePanelDocumentSchedule treepanel": {
          itemcontextmenu: this.treeCollaboratorRightClick
        },
        "[action=modifyWorkSchedule]": {
          click: this._onModifyWorkSchedule
        },
        "[action=consultWorkSchedule]": {
          click: this._onConsultWorkSchedule
        },
        "[action=deleteWorkSchedule]": {
          click: this._onDeleteWorkSchedule
        },
        "[action=saveAdvancedTask]": {
          click: this._onSaveAdvancedTask
        },
        "[action=historicalAdvance]": {
          click: this._onHistoricalAdvance
        }
      });
    },
    treeCollaboratorRightClick: function(view, record, node, index, e) {
      e.stopEvent();
      this.currentRecord = record;
      if (category == DukeSource.global.GiroConstants.GESTOR) {
        if (record.get("depth") === 1) {
          var addMenu = Ext.create(
            "DukeSource.view.fulfillment.AddMenuCollaborator",
            {
              items: [
                {
                  xtype: "menuitem",
                  text: "Agregar Tarea",
                  iconCls: "add",
                  handler: function() {
                    var windows = Ext.create(
                      "DukeSource.view.fulfillment.window.ViewWindowAddWork",
                      {
                        modal: true,
                        buttons: [
                          {
                            text: "GUARDAR",
                            iconCls: "save",
                            handler: function() {
                              var treepanel = Ext.ComponentQuery.query(
                                "ViewTreePanelDocumentSchedule treepanel"
                              )[0];
                              var form = windows.down("form");
                              if (form.getForm().isValid()) {
                                form.getForm().submit({
                                  url:
                                    "http://localhost:9000/giro/saveScheduleDocument.htm?nameView=ViewPanelDocumentPending",
                                  waitMsg:
                                    DukeSource.global.GiroMessages
                                      .MESSAGE_LOADING,
                                  method: "POST",
                                  success: function(form, action) {
                                    var valor = Ext.decode(
                                      action.response.responseText
                                    );
                                    if (valor.success) {
                                      DukeSource.global.DirtyView.messageAlert(
                                        DukeSource.global.GiroMessages
                                          .TITLE_MESSAGE,
                                        valor.mensaje,
                                        Ext.Msg.INFO
                                      );
                                      var refreshNode = treepanel
                                        .getStore()
                                        .getNodeById(valor.data);
                                      refreshNode.removeAll(false);
                                      treepanel.getStore().load({
                                        node: refreshNode
                                      });
                                      windows.close();
                                    } else {
                                      DukeSource.global.DirtyView.messageAlert(
                                        DukeSource.global.GiroMessages
                                          .TITLE_ERROR,
                                        valor.mensaje,
                                        Ext.Msg.ERROR
                                      );
                                    }
                                  },
                                  failure: function(form, action) {
                                    var valor = Ext.decode(
                                      action.response.responseText
                                    );
                                    if (!valor.success) {
                                      DukeSource.global.DirtyView.messageAlert(
                                        DukeSource.global.GiroMessages
                                          .TITLE_ERROR,
                                        valor.mensaje,
                                        Ext.Msg.ERROR
                                      );
                                    }
                                  }
                                });
                              } else {
                                DukeSource.global.DirtyView.messageAlert(
                                  DukeSource.global.GiroMessages.TITLE_WARNING,
                                  DukeSource.global.GiroMessages
                                    .MESSAGE_COMPLETE,
                                  Ext.Msg.ERROR
                                );
                              }
                            }
                          }
                        ]
                      }
                    );
                    var form = windows.down("form");
                    form
                      .down("datefield[name=dateInitial]")
                      .setMinValue(record.raw["dateInitPlan"]);
                    form
                      .down("datefield[name=dateInitial]")
                      .setMaxValue(record.raw["dateEnd"]);
                    form
                      .down("datefield[name=dateExpire]")
                      .setMaxValue(record.raw["dateEnd"]);
                    form
                      .down("textfield[name=idDetailDocument]")
                      .setValue(record.raw["idDetailDocument"]);
                    form
                      .down("textfield[name=idDocument]")
                      .setValue(record.raw["idDocument"]);
                    form
                      .down("textfield[name=stateDocument]")
                      .setValue(record.raw["stateDocument"]);
                    windows.show();
                    form
                      .down("UpperCaseTextField[name=descriptionTask]")
                      .focus(false, 100);
                  }
                }
              ]
            }
          );
          addMenu.showAt(e.getXY());
        }
      }
      if (record.get("depth") === 2) {
        var addMenu = Ext.create(
          "DukeSource.view.fulfillment.AddMenuCollaborator",
          {
            items: [
              {
                xtype: "menuitem",
                iconCls: "modify",
                action: "modifyWorkSchedule",
                text: "Verificar tarea",
                node: node,
                depth: record.get("depth")
              },
              {
                xtype: "menuitem",
                iconCls: "consult",
                action: "consultWorkSchedule",
                text: "Consultar tarea"
              },
              {
                xtype: "menuitem",
                iconCls: "delete",
                action: "deleteWorkSchedule",
                text: "Eliminar tarea"
              },
              {
                xtype: "menuitem",
                iconCls: "attachFile",
                text: "Adjuntos de la tarea",
                handler: function() {
                  var attachWindow = Ext.create(
                    "DukeSource.view.fulfillment.window.ViewWindowViewAttachDocument",
                    {
                      idTree: record.get("id"),
                      modal: true,
                      buttons: [
                        {
                          text: "SALIR",
                          iconCls: "logout",
                          handler: function() {
                            attachWindow.close();
                          }
                        }
                      ]
                    }
                  ).show();
                  attachWindow
                    .down("grid")
                    .getStore()
                    .load({
                      params: {
                        id: record.get("id")
                      },
                      url:
                        "http://localhost:9000/giro/showListFileAttachmentsSchedule.htm"
                    });
                }
              },
              {
                xtype: "menuitem",
                iconCls: "consult",
                action: "historicalAdvance",
                text: "Historial de los avances",
                rec: record
              }
            ]
          }
        );
        addMenu.showAt(e.getXY());
      }
      if (record.get("depth") === 3) {
        var addMenu = Ext.create(
          "DukeSource.view.fulfillment.AddMenuCollaborator",
          {
            items: [
              {
                xtype: "menuitem",
                iconCls: "modify",
                text: "Modificar avance",
                handler: function() {
                  if (
                    Ext.ComponentQuery.query("ViewWindowReportAdvance")["0"] ==
                    undefined
                  ) {
                    var tree = Ext.ComponentQuery.query(
                      "ViewTreePanelDocumentSchedule treepanel"
                    )[0];
                    if (
                      category == DukeSource.global.GiroConstants.GESTOR ||
                      record.raw["stateRevision"] ==
                        DukeSource.global.GiroConstants.PENDING_REVISED
                    ) {
                      var advanceWindow = Ext.create(
                        "DukeSource.view.fulfillment.window.ViewWindowReportAdvance",
                        {
                          buttons: [
                            {
                              text: "GUARDAR",
                              iconCls: "save",
                              action: "saveAdvancedTask"
                            },
                            {
                              text: "SALIR",
                              iconCls: "logout",
                              handler: function() {
                                advanceWindow.close();
                              }
                            }
                          ]
                        }
                      );
                      advanceWindow
                        .down("form")
                        .getForm()
                        .setValues(
                          tree.getSelectionModel().getSelection()[0].raw
                        );
                      advanceWindow
                        .down("textfield[name=idScheduleDocument]")
                        .setValue(record.get("id"));
                      advanceWindow
                        .down("datefield")
                        .setMinValue(record.get("fieldTwo"));
                      advanceWindow
                        .down("datefield")
                        .setMaxValue(record.get("fieldThree"));
                      if (category == DukeSource.global.GiroConstants.GESTOR)
                        advanceWindow
                          .down("textfield[name=stateRevision]")
                          .setValue(DukeSource.global.GiroConstants.REVISED);
                      advanceWindow.show();
                    } else {
                      DukeSource.global.DirtyView.messageAlert(
                        DukeSource.global.GiroMessages.TITLE_WARNING,
                        DukeSource.global.GiroMessages.MESSAGE_ADVANCE_VERIFIED,
                        Ext.Msg.ERROR
                      );
                    }
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      DukeSource.global.GiroMessages.MESSAGE_ITEM,
                      Ext.Msg.WARNING
                    );
                  }
                }
              },
              {
                xtype: "menuitem",
                iconCls: "attachFile",
                text: "Adjuntos de avance",
                handler: function() {
                  var attachWindow = Ext.create(
                    "DukeSource.view.fulfillment.window.ViewWindowManagementAttachDocument",
                    {
                      idTree: record.get("id"),
                      modal: true,
                      buttons: [
                        {
                          text: "SALIR",
                          iconCls: "logout",
                          handler: function() {
                            attachWindow.close();
                          }
                        }
                      ]
                    }
                  ).show();
                  attachWindow.down("grid").store.getProxy().extraParams = {
                    id: record.get("id")
                  };
                  attachWindow.down("grid").store.getProxy().url =
                    "http://localhost:9000/giro/showListFileAttachmentsOfActivityAdvance.htm";
                  attachWindow
                    .down("grid")
                    .down("pagingtoolbar")
                    .moveFirst();
                }
              }
            ]
          }
        );
        addMenu.showAt(e.getXY());
      }
    },
    _onModifyWorkSchedule: function(node, depth) {
      var tree = Ext.ComponentQuery.query(
        "ViewTreePanelDocumentSchedule treepanel"
      )[0];
      var window = Ext.create(
        "DukeSource.view.fulfillment.window.ViewWindowAddWork",
        {
          modal: true,
          buttons: [
            {
              text: "GUARDAR",
              iconCls: "save",
              handler: function(button) {
                var treepanel = Ext.ComponentQuery.query(
                  "ViewTreePanelDocumentSchedule treepanel"
                )[0];
                var window = button.up("window");
                var form = window.down("form");
                if (form.getForm().isValid()) {
                  form.getForm().submit({
                    url:
                      "http://localhost:9000/giro/saveScheduleDocument.htm?nameView=ViewPanelDocumentPending",
                    waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
                    method: "POST",
                    success: function(form, action) {
                      var valor = Ext.decode(action.response.responseText);
                      if (valor.success) {
                        var refreshNode = treepanel
                          .getStore()
                          .getNodeById(valor.data);
                        refreshNode.removeAll(false);
                        treepanel.getStore().load({
                          node: refreshNode
                        });
                        window.close();
                      } else {
                        DukeSource.global.DirtyView.messageAlert(
                          DukeSource.global.GiroMessages.TITLE_ERROR,
                          valor.mensaje,
                          Ext.Msg.ERROR
                        );
                      }
                    },
                    failure: function(form, action) {
                      var valor = Ext.decode(action.response.responseText);
                      if (!valor.success) {
                        DukeSource.global.DirtyView.messageAlert(
                          DukeSource.global.GiroMessages.TITLE_ERROR,
                          valor.mensaje,
                          Ext.Msg.ERROR
                        );
                      }
                    }
                  });
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_WARNING,
                    DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
                    Ext.Msg.ERROR
                  );
                }
              }
            }
          ]
        }
      );
      var form = window.down("form");
      var node = tree.getSelectionModel().getSelection()[0];
      form.getForm().setValues(node.raw);
      form
        .down("datefield[name=dateInitial]")
        .setMinValue(node.parentNode.get("fieldTwo"));
      form
        .down("datefield[name=dateInitial]")
        .setMaxValue(node.parentNode.get("fieldThree"));
      form
        .down("datefield[name=dateExpire]")
        .setMaxValue(node.parentNode.get("fieldThree"));
      window.show();
      form.down("UpperCaseTextField[name=descriptionTask]").focus(false, 100);
    },
    _onConsultWorkSchedule: function() {
      var tree = Ext.ComponentQuery.query(
        "ViewTreePanelDocumentSchedule treepanel"
      )[0];
      var window = Ext.create(
        "DukeSource.view.fulfillment.window.ViewWindowAddWork",
        {
          modal: true
        }
      );
      window
        .down("form")
        .query(".textfield, .numberfield")
        .forEach(function(c) {
          DukeSource.global.DirtyView.toReadOnly(c);
        });
      window
        .down("form")
        .query(".button, .fileuploadfield")
        .forEach(function(c) {
          c.disable();
        });
      window
        .down("form")
        .getForm()
        .setValues(tree.getSelectionModel().getSelection()[0].raw);
      window.show();
    },
    _onDeleteWorkSchedule: function() {
      var tree = Ext.ComponentQuery.query(
        "ViewTreePanelDocumentSchedule treepanel"
      )[0];
      var jsonData = this.currentRecord;
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_WARNING,
        msg: "Estas seguro que desea eliminar esta tarea?",
        icon: Ext.Msg.QUESTION,
        buttonText: {
          yes: "Si",
          no: "No"
        },
        buttons: Ext.MessageBox.YESNO,
        fn: function(btn) {
          if (btn == "yes") {
            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/deleteScheduleDocument.htm?nameView=ViewPanelDocumentPending",
              params: {
                jsonData: Ext.JSON.encode(jsonData.raw)
              },
              success: function(response) {
                var response = Ext.decode(response.responseText);
                if (response.success) {
                  var refreshNode = Ext.ComponentQuery.query(
                    "ViewTreePanelDocumentSchedule treepanel"
                  )[0]
                    .getStore()
                    .getNodeById(response.data);
                  refreshNode.removeAll(false);
                  Ext.ComponentQuery.query(
                    "ViewTreePanelDocumentSchedule treepanel"
                  )[0]
                    .getStore()
                    .load({
                      node: refreshNode
                    });
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_WARNING,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
          } else {
          }
        }
      });
    },
    _onSaveAdvancedTask: function(button) {
      var window = button.up("window");
      var form = window.down("form");
      if (form.getForm().isValid()) {
        form.getForm().submit({
          url:
            "http://localhost:9000/giro/saveAdvanceActivity.htm?nameView=ViewTreePanelDocumentSchedule",
          method: "POST",
          success: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (valor.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                valor.mensaje,
                Ext.Msg.INFO
              );
              window.close();
              var refreshNode = Ext.ComponentQuery.query(
                "ViewTreePanelDocumentSchedule treepanel"
              )[0]
                .getStore()
                .getNodeById(valor.data);
              refreshNode.removeAll(false);
              Ext.ComponentQuery.query(
                "ViewTreePanelDocumentSchedule treepanel"
              )[0]
                .getStore()
                .load({
                  node: refreshNode
                });
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                valor.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (!valor.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                valor.mensaje,
                Ext.Msg.ERROR
              );
            }
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onHistoricalAdvance: function(view) {
      var windows = Ext.create(
        "DukeSource.view.fulfillment.window.ViewWindowHistoricalAdvance",
        {
          modal: true
        }
      );
      windows
        .down("grid")
        .getStore()
        .load({
          url:
            "http://localhost:9000/giro/showListHistoricalAdvanceActivity.htm",
          params: {
            idDocument: view.rec.raw["idDocument"],
            idDetailDocument: view.rec.raw["idDetailDocument"],
            idScheduleDocument: view.rec.raw["idScheduleDocument"]
          }
        });
      windows.title = windows.title + "-" + view.rec.raw["descriptionTask"];
      windows.show();
    }
  }
);
