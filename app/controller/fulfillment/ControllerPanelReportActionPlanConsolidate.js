Ext.define(
  "DukeSource.controller.fulfillment.ControllerPanelReportActionPlanConsolidate",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: ["risk.parameter.combos.ViewComboTypeRiskEvaluation"],
    init: function() {
      this.control({
        "[action=generateReportActionPlanConsolidatePdf]": {
          click: this._onGenerateReportActionPlanConsolidatePdf
        },
        "[action=generateReportActionPlanConsolidateXls]": {
          click: this._ongenerateReportActionPlanConsolidateXls
        }
      });
    },

    _onGenerateReportActionPlanConsolidatePdf: function(btn) {
      Ext.core.DomHelper.append(document.body, {
        tag: "iframe",
        id: "downloadIframe",
        frameBorder: 0,
        width: 0,
        height: 0,
        css: "display:none;visibility:hidden;height:0px;",
        src:
          "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
          "&names=" +
          "&types=" +
          "&nameReport=PLAN0001XLS"
      });
    },
    _ongenerateReportActionPlanConsolidateXls: function(btn) {
      Ext.core.DomHelper.append(document.body, {
        tag: "iframe",
        id: "downloadIframe",
        frameBorder: 0,
        width: 0,
        height: 0,
        css: "display:none;visibility:hidden;height:0px;",
        src:
          "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
          "&names=" +
          "&types=" +
          "&nameReport=RORIEV0006XLS"
      });
    }
  }
);
