Ext.define(
  "DukeSource.controller.fulfillment.ControllerPanelReportsActionPlan",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    init: function() {
      this.control({
        "[action=generateReportsActionPlan]": {
          click: this.executeEvolutionRisk
        }
      });
    },
    executeEvolutionRisk: function(btn) {
      var form = btn.up("form");
      if (form.getForm().isValid()) {
        var view = Ext.ComponentQuery.query("ViewPanelReportsActionPlan")[0];

        var nameDownload = view.down("#nameFile").getValue();
        var nameReport = view.down("#nameReport").getValue();

        var containerReport = view.down("#containerReport");

        containerReport.removeAll();
        containerReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            hidden: true,
            src:
              "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
              "&names=" +
              "" +
              "&types=" +
              "" +
              "&nameReport=" +
              nameReport +
              "&nameDownload=" +
              nameDownload
          }
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE
        );
      }
    }
  }
);
