Ext.define(
  "DukeSource.controller.fulfillment.parameter.ControllerPanelRegisterStateDocument",
  {
    extend: "Ext.app.Controller",
    stores: ["fulfillment.grids.StoreGridPanelRegisterStateDocument"],
    models: ["fulfillment.grids.ModelGridPanelRegisterStateDocument"],
    views: ["fulfillment.grids.ViewGridPanelRegisterStateDocument"],
    init: function() {
      this.control({
        "[action=searchTriggerGridStateDocument]": {
          keyup: this._onSearchTriggerGridStateDocument
        },
        "[action=exportStateDocumentPdf]": {
          click: this._onExportStateDocumentPdf
        },
        "[action=exportStateDocumentExcel]": {
          click: this._onExportStateDocumentExcel
        },
        "[action=stateDocumentAuditory]": {
          click: this._onStateDocumentAuditory
        },
        "[action=newStateDocument]": {
          click: this._newStateDocument
        },
        "[action=deleteStateDocument]": {
          click: this._onDeleteStateDocument
        },
        "[action=searchStateDocument]": {
          specialkey: this._searchStateDocument
        }
      });
    },
    _newStateDocument: function() {
      var modelStateDocument = Ext.create(
        "DukeSource.model.fulfillment.grids.ModelGridPanelRegisterStateDocument",
        {
          idStateDocument: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterStateDocument"
      )[0];
      var panel = general.down("ViewGridPanelRegisterStateDocument");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelStateDocument);
      editor.startEdit(0, 0);
    },
    _onDeleteStateDocument: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterStateDocument grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteStateDocument.htm?nameView=ViewPanelRegisterStateDocument"
      );
    },
    _searchStateDocument: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterStateDocument grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findStateDocument.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridStateDocument: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterStateDocument grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportStateDocumentPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportStateDocumentExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onStateDocumentAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterStateDocument grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditStateDocument.htm"
      );
    }
  }
);
