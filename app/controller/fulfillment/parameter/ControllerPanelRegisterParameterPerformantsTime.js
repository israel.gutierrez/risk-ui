Ext.define(
  "DukeSource.controller.fulfillment.parameter.ControllerPanelRegisterParameterPerformantsTime",
  {
    extend: "Ext.app.Controller",
    stores: [
      "fulfillment.grids.StoreGridPanelRegisterParameterPerformantsTime"
    ],
    models: [
      "fulfillment.grids.ModelGridPanelRegisterParameterPerformantsTime"
    ],
    views: [
      "fulfillment.parameter.grids.ViewGridPanelRegisterParameterPerformantsTime",
      "fulfillment.parameter.ViewPanelRegisterParameterPerformantsTime"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridParameterPerformantsTime]": {
          keyup: this._onSearchTriggerGridParameterPerformantsTime
        },
        "[action=exportParameterPerformantsTimePdf]": {
          click: this._onExportParameterPerformantsTimePdf
        },
        "[action=exportParameterPerformantsTimeExcel]": {
          click: this._onExportParameterPerformantsTimeExcel
        },
        "[action=parameterPerformantsTimeAuditory]": {
          click: this._onParameterPerformantsTimeAuditory
        },
        "[action=newParameterPerformantsTime]": {
          click: this._newParameterPerformantsTime
        },
        "[action=deleteParameterPerformantsTime]": {
          click: this._onDeleteParameterPerformantsTime
        },
        "[action=searchParameterPerformantsTime]": {
          specialkey: this._searchParameterPerformantsTime
        }
      });
    },
    _newParameterPerformantsTime: function() {
      var modelParameterPerformantsTime = Ext.create(
        "DukeSource.model.fulfillment.grids.ModelGridPanelRegisterParameterPerformantsTime",
        {
          idParameterPerformantsTime: "id",
          description: "",
          numberInitialDay: "",
          numberFinalDay: "",
          numberInitialPercentage: "",
          numberFinalPercentage: "",
          hexadecimalColor: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterParameterPerformantsTime"
      )[0];
      var panel = general.down("ViewGridPanelRegisterParameterPerformantsTime");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelParameterPerformantsTime);
      editor.startEdit(0, 0);
    },
    _onDeleteParameterPerformantsTime: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterParameterPerformantsTime grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteParameterPerformantsTime.htm?nameView=ViewPanelRegisterParameterPerformantsTime"
      );
    },
    _searchParameterPerformantsTime: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterParameterPerformantsTime grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findParameterPerformantsTime.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridParameterPerformantsTime: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterParameterPerformantsTime grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportParameterPerformantsTimePdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportParameterPerformantsTimeExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onParameterPerformantsTimeAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterParameterPerformantsTime grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditParameterPerformantsTime.htm"
      );
    }
  }
);
