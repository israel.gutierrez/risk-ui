Ext.define(
  "DukeSource.controller.fulfillment.parameter.ControllerPanelRegisterTypeDocument",
  {
    extend: "Ext.app.Controller",
    stores: ["fulfillment.grids.StoreGridPanelRegisterTypeDocument"],
    models: ["fulfillment.grids.ModelGridPanelRegisterTypeDocument"],
    views: ["fulfillment.grids.ViewGridPanelRegisterTypeDocument"],
    init: function() {
      this.control({
        "[action=searchTriggerGridTypeDocument]": {
          keyup: this._onSearchTriggerGridTypeDocument
        },
        "[action=exportTypeDocumentPdf]": {
          click: this._onExportTypeDocumentPdf
        },
        "[action=exportTypeDocumentExcel]": {
          click: this._onExportTypeDocumentExcel
        },
        "[action=typeDocumentAuditory]": {
          click: this._onTypeDocumentAuditory
        },
        "[action=newTypeDocument]": {
          click: this._newTypeDocument
        },
        "[action=deleteTypeDocument]": {
          click: this._onDeleteTypeDocument
        },
        "[action=searchTypeDocument]": {
          specialkey: this._searchTypeDocument
        }
      });
    },
    _newTypeDocument: function() {
      var modelTypeDocument = Ext.create(
        "DukeSource.model.fulfillment.grids.ModelGridPanelRegisterTypeDocument",
        {
          idTypeDocument: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeDocument"
      )[0];
      var panel = general.down("ViewGridPanelRegisterTypeDocument");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelTypeDocument);
      editor.startEdit(0, 0);
    },
    _onDeleteTypeDocument: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeDocument grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteTypeDocument.htm?nameView=ViewPanelRegisterTypeDocument"
      );
    },
    _searchTypeDocument: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterTypeDocument grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findTypeDocument.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridTypeDocument: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeDocument grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportTypeDocumentPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportTypeDocumentExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onTypeDocumentAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeDocument grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditTypeDocument.htm"
      );
    }
  }
);
