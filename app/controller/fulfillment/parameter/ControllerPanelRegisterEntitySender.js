Ext.define(
  "DukeSource.controller.fulfillment.parameter.ControllerPanelRegisterEntitySender",
  {
    extend: "Ext.app.Controller",
    stores: ["fulfillment.grids.StoreGridPanelRegisterEntitySender"],
    models: ["fulfillment.grids.ModelGridPanelRegisterEntitySender"],
    views: ["fulfillment.grids.ViewGridPanelRegisterEntitySender"],
    init: function() {
      this.control({
        "[action=searchTriggerGridEntitySender]": {
          keyup: this._onSearchTriggerGridEntitySender
        },
        "[action=exportEntitySenderPdf]": {
          click: this._onExportEntitySenderPdf
        },
        "[action=exportEntitySenderExcel]": {
          click: this._onExportEntitySenderExcel
        },
        "[action=entitySenderAuditory]": {
          click: this._onEntitySenderAuditory
        },
        "[action=newEntitySender]": {
          click: this._newEntitySender
        },
        "[action=deleteEntitySender]": {
          click: this._onDeleteEntitySender
        },
        "[action=searchEntitySender]": {
          specialkey: this._searchEntitySender
        }
      });
    },
    _newEntitySender: function() {
      var modelEntitySender = Ext.create(
        "DukeSource.model.fulfillment.grids.ModelGridPanelRegisterEntitySender",
        {
          idEntitySender: "id",
          ruc: "",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterEntitySender"
      )[0];
      var panel = general.down("ViewGridPanelRegisterEntitySender");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelEntitySender);
      editor.startEdit(0, 0);
    },
    _onDeleteEntitySender: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterEntitySender grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteEntitySender.htm?nameView=ViewPanelRegisterEntitySender"
      );
    },
    _searchEntitySender: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterEntitySender grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findEntitySender.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridEntitySender: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterEntitySender grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportEntitySenderPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportEntitySenderExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onEntitySenderAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterEntitySender grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditEntitySender.htm"
      );
    }
  }
);
