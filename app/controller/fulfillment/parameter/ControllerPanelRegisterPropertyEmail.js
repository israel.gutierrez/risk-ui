Ext.define(
  "DukeSource.controller.fulfillment.parameter.ControllerPanelRegisterPropertyEmail",
  {
    extend: "Ext.app.Controller",
    stores: ["fulfillment.grids.StoreGridPanelRegisterPropertyEmail"],
    models: ["fulfillment.grids.ModelGridPanelRegisterPropertyEmail"],
    views: [
      "fulfillment.parameter.grids.ViewGridPanelRegisterPropertyEmail",
      "fulfillment.parameter.ViewPanelRegisterPropertyEmail"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridPropertyEmail]": {
          keyup: this._onSearchTriggerGridPropertyEmail
        },
        "[action=exportPropertyEmailPdf]": {
          click: this._onExportPropertyEmailPdf
        },
        "[action=exportPropertyEmailExcel]": {
          click: this._onExportPropertyEmailExcel
        },
        "[action=propertyEmailAuditory]": {
          click: this._onPropertyEmailAuditory
        },
        "[action=newPropertyEmail]": {
          click: this._newPropertyEmail
        },
        "[action=deletePropertyEmail]": {
          click: this._onDeletePropertyEmail
        },
        "[action=searchPropertyEmail]": {
          specialkey: this._searchPropertyEmail
        }
      });
    },
    _newPropertyEmail: function() {
      var modelPropertyEmail = Ext.create(
        "DukeSource.model.fulfillment.grids.ModelGridPanelRegisterPropertyEmail",
        {
          idPropertyEmail: "id",
          generalEmail: "",
          password: "",
          nameServiceSMTP: "",
          port: "",
          permitTTLS: "",
          authorization: "",
          nameClass: "",
          fallBack: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterPropertyEmail"
      )[0];
      var panel = general.down("ViewGridPanelRegisterPropertyEmail");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelPropertyEmail);
      editor.startEdit(0, 0);
    },
    _onDeletePropertyEmail: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterPropertyEmail grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deletePropertyEmail.htm?nameView=ViewPanelRegisterPropertyEmail"
      );
    },
    _searchPropertyEmail: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterPropertyEmail grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findPropertyEmail.htm",
          "generalEmail",
          "generalEmail"
        );
      } else {
      }
    },
    _onSearchTriggerGridPropertyEmail: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterPropertyEmail grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportPropertyEmailPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportPropertyEmailExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onPropertyEmailAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterPropertyEmail grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditPropertyEmail.htm"
      );
    }
  }
);
