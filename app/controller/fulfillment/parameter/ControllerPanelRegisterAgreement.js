Ext.define(
  "DukeSource.controller.fulfillment.parameter.ControllerPanelRegisterAgreement",
  {
    extend: "Ext.app.Controller",
    stores: ["fulfillment.grids.StoreGridPanelRegisterAgreement"],
    models: ["fulfillment.grids.ModelGridPanelRegisterAgreement"],
    views: [
      "fulfillment.grids.ViewGridPanelRegisterAgreement",
      "fulfillment.parameter.ViewPanelRegisterAgreement"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridAgreement]": {
          keyup: this._onSearchTriggerGridAgreement
        },
        "[action=exportAgreementPdf]": {
          click: this._onExportAgreementPdf
        },
        "[action=exportAgreementExcel]": {
          click: this._onExportAgreementExcel
        },
        "[action=agreementAuditory]": {
          click: this._onAgreementAuditory
        },
        "[action=newAgreement]": {
          click: this._newAgreement
        },
        "[action=saveAgreement]": {
          click: this._onSaveAgreement
        },
        "[action=modifyAgreement]": {
          click: this._onModifyAgreement
        },
        "[action=deleteAgreement]": {
          click: this._onDeleteAgreement
        },
        "[action=searchAgreement]": {
          specialkey: this._searchAgreement
        },
        "[action=showAttachment]": {
          click: this._onShowAttachment()
        }
      });
    },
    _newAgreement: function() {
      Ext.create("DukeSource.view.fulfillment.window.ViewWindowAgreement", {
        modal: true
      }).show();
    },
    _onSaveAgreement: function(button) {
      var win = button.up("window");
      var form = win.down("form");
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterAgreement grid")[0];
      if (form.getForm().isValid()) {
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveAgreement.htm?nameView=ViewPanelRegisterAgreement",
          params: {
            jsonData: Ext.JSON.encode(
              win
                .down("form")
                .getForm()
                .getValues()
            )
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              win.close();
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                response.mensaje,
                Ext.Msg.INFO
              );
              DukeSource.global.DirtyView.searchPaginationGridNormal(
                "",
                grid,
                grid.down("pagingtoolbar"),
                "http://localhost:9000/giro/findAgreement.htm",
                "description",
                "idAgreement"
              );
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onModifyAgreement: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterAgreement grid")[0];
      if (grid.getSelectionModel().getCount() == 0) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        var win = Ext.create(
          "DukeSource.view.fulfillment.window.ViewWindowAgreement",
          {
            modal: true
          }
        ).show();
        win
          .down("form")
          .getForm()
          .loadRecord(grid.getSelectionModel().getSelection()[0]);
      }
    },
    _onDeleteAgreement: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterAgreement grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteAgreement.htm?nameView=ViewPanelRegisterAgreement"
      );
    },
    _searchAgreement: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterAgreement grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findAgreement.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onShowAttachment: function() {},
    _onSearchTriggerGridAgreement: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterAgreement grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportAgreementPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportAgreementExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onAgreementAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterAgreement grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditAgreement.htm"
      );
    }
  }
);
