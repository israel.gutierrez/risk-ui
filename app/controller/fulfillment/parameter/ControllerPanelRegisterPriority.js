Ext.define(
  "DukeSource.controller.fulfillment.parameter.ControllerPanelRegisterPriority",
  {
    extend: "Ext.app.Controller",
    stores: ["fulfillment.grids.StoreGridPanelRegisterPriority"],
    models: ["fulfillment.grids.ModelGridPanelRegisterPriority"],
    views: ["fulfillment.grids.ViewGridPanelRegisterPriority"],
    init: function() {
      this.control({
        "[action=searchTriggerGridPriority]": {
          keyup: this._onSearchTriggerGridPriority
        },
        "[action=exportPriorityPdf]": {
          click: this._onExportPriorityPdf
        },
        "[action=exportPriorityExcel]": {
          click: this._onExportPriorityExcel
        },
        "[action=priorityAuditory]": {
          click: this._onPriorityAuditory
        },
        "[action=newPriority]": {
          click: this._newPriority
        },
        "[action=deletePriority]": {
          click: this._onDeletePriority
        },
        "[action=searchPriority]": {
          specialkey: this._searchPriority
        }
      });
    },
    _newPriority: function() {
      var modelPriority = Ext.create(
        "DukeSource.model.fulfillment.grids.ModelGridPanelRegisterPriority",
        {
          idPriority: "id",
          description: "",
          abreviature: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterPriority")[0];
      var panel = general.down("ViewGridPanelRegisterPriority");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelPriority);
      editor.startEdit(0, 0);
    },
    _onDeletePriority: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterPriority grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deletePriority.htm?nameView=ViewPanelRegisterPriority"
      );
    },
    _searchPriority: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterPriority grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findPriority.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridPriority: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterPriority grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportPriorityPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportPriorityExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onPriorityAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterPriority grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditPriority.htm"
      );
    }
  }
);
