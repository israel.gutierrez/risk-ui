Ext.define('DukeSource.controller.fulfillment.ControllerPanelDocumentPending', {
    extend: 'Ext.app.Controller',
    stores: [],
    models: [],
    views: [
        'fulfillment.combos.ViewComboTypeDocument',
        'risk.parameter.combos.ViewComboOriginActionPlan',
        'risk.util.search.AdvancedSearchActionPlan',
        'risk.parameter.combos.ViewComboIndicatorType'
    ],
    init: function () {
        this.control({
            '[action=searchTriggerGridFulFillment]': {
                keyup: this._onSearchTriggerGridFulFillment
            },
            '[action=newDocument]': {
                click: this._onNewDocument
            },
            'ViewPanelDocumentPending grid': {
                itemcontextmenu: this.treeRightClick
            },
            'ViewWindowTreeGridAssignWork grid[name=gridTask]': {
                itemcontextmenu: this.treeRightClickWorkFulfillment,
                itemclick: this._onItemClickGridTask
            },
            '[action=saveDocumentFulfillment]': {
                click: this._onSaveDocumentFulfillment
            },
            '[action=modifyDocument]': {
                click: this._onModifyDocument
            },
            '[action=inactivateDocument]': {
                click: this._onInactivateDocument
            },
            '[action=addMonitoring]': {
                click: this._onAddMonitoring
            },
            '[action=newMonitoringDocument]': {
                click: this._onNewMonitoringDocument
            },
            '[action=modifyMonitoringDocument]': {
                click: this._onModifyMonitoringDocument
            },
            '[action=deleteMonitoringDocument]': {
                click: this._onDeleteMonitoringDocument
            },
            '[action=requestReProgramDocument]': {
                click: this._onRequestReProgramDocument
            },
            '[action=attendRequestReprogram]': {
                click: this._onAttendRequestReprogram
            },
            '[action=attendRequestDocument]': {
                click: this._onAttendRequestDocument
            },
            '[action=searchResponsible]': {
                click: this._onSearchResponsible
            },
            '[action=viewHistoryDocument]': {
                click: this._onViewHistoryDocument
            },
            '[action=addWorkGestor]': {
                click: this._onWindowAddWorkManager
            },
            '[action=addWorkToFulfillment]': {
                click: this._onAddWorkToFulfillment
            },
            '[action=modifyWorkFulfillment]': {
                click: this._onModifyWorkFulfillment
            },
            '[action=deleteWorkFulfillment]': {
                click: this._onDeleteWorkFulfillment
            },
            '[action=approveActionPlan]': {
                click: this._onApproveActionPlan
            },
            '[action=modifyAdvanceActivity]': {
                click: this._onModifyAdvanceActivity
            },
            '[action=fileAttachedDocument]': {
                click: this._onFileAttachedDocument
            },
            '[action=deleteFileAttachDocument]': {
                click: this._onDeleteFileAttachDocument
            },
            '[action=saveFileAttachDocument]': {
                click: this._onSaveFileAttachDocument
            },
            '[action=searchGridFileAttachmentDocument]': {
                keyup: this._onSearchTriggerFileAttachmentDocument
            },
            '[action=collapseExpandAP]': {
                click: this._onCollapseExpandAP
            },
            '[action=historicalAdvanceActivity]': {
                click: this._onHistoricalAdvanceActivity
            },
            '[action=saveAgreementOfActionPlan]': {
                click: this._onSaveAgreementOfActionPlan
            },
            '[action=deleteActionPlanStatePending]': {
                click: this._onDeleteActionPlanStatePending
            },
            '[action=finishActionPlanStatePending]': {
                click: this._onFinishActionPlanStatePending
            },
            '[action=attachFileTaskManager]': {
                click: this._onAttachFileTaskManager
            },
            '[action=modifyAdvanceActivityManager]': {
                click: this._onModifyAdvanceActivityManager
            },
            '[action=saveAdvancedActivity]': {
                click: this._onSaveAdvancedActivity
            },
            '[action=attachFileAdvanceActivityManager]': {
                click: this._onAttachFileAdvanceActivityManager
            },
            'ViewWindowTreeGridAssignWork grid[name=gridAdvanceActivity]': {
                itemcontextmenu: this.rightClickGridActivity
            },
            '[action=findDetailDocument]': {
                click: this._onFindDetailDocument
            }
        });
    },
    _onSearchTriggerGridFulFillment: function (text) {
        var grid = Ext.ComponentQuery.query("ViewPanelDocumentPending grid")[0];
        DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },

    _onNewDocument: function () {

        var view = Ext.create('DukeSource.view.fulfillment.window.WindowRegisterActionPlan', {
            modal: true,
            actionType: 'new',
            isAvailable: true
        }).show();

        view.down('ViewComboTypeDocument').setValue("1");

    },

    treeRightClick: function (view, rec, node, index, e) {
        e.stopEvent();
        var addMenu = Ext.create('DukeSource.view.fulfillment.AddMenu', {});
        addMenu.removeAll();
        if (category === DukeSource.global.GiroConstants.ANALYST) {
            if (rec.get('stateDocument') === DukeSource.global.GiroConstants.PROCESS) {
                addMenu.add(
                    {
                        text: 'Seguimiento',
                        iconCls: 'add',
                        action: 'addMonitoring',
                        rec: rec
                    },
                    {
                        text: 'Modificar Condiciones',
                        iconCls: 'modify',
                        hidden: true,
                        action: 'modifyDocument',
                        rec: rec
                    },
                    {
                        text: 'Tareas y Avances',
                        iconCls: 'date_error',
                        hidden: true,
                        action: 'addWorkGestor',
                        rec: rec
                    },
                    {
                        text: 'Vincular a:',
                        iconCls: 'relation',
                        hidden: hidden('PLA_WPDP_ICTX_LinkTo'),
                        menu: {
                            items: [
                                {
                                    xtype: 'menuitem',
                                    text: 'Incidentes',
                                    handler: function () {
                                        Ext.create('DukeSource.view.fulfillment.window.WindowActionPlanRelationIncident', {
                                            modal: true,
                                            idActionPlan: rec.get('id')
                                        }).show();

                                    }
                                },
                                {
                                    xtype: 'menuitem',
                                    text: 'Eventos',
                                    handler: function () {
                                        Ext.create('DukeSource.view.fulfillment.window.WindowActionPlanRelationEvent', {
                                            modal: true,
                                            idActionPlan: rec.get('id')
                                        }).show();

                                    }
                                },
                                {
                                    xtype: 'menuitem',
                                    text: 'Indicadores',
                                    handler: function () {
                                        Ext.create('DukeSource.view.fulfillment.window.WindowActionPlanRelationIndicator', {
                                            modal: true,
                                            idActionPlan: rec.get('id')
                                        }).show();

                                    }
                                }
                            ]
                        }
                    },
                    {
                        text: 'Solicitudes de reprogramación',
                        iconCls: 'date_error',
                        hidden: hidden('WAP_RBN_RequestReprogramming'),
                        handler: function () {
                            var app = DukeSource.application;
                            app.getController('DukeSource.controller.fulfillment.ControllerPanelDocumentPending')._onAttendRequestReprogram(rec);
                        },
                        rec: rec
                    },
                    {
                        text: 'Documentos Adjuntos',
                        iconCls: 'gestionfile',
                        action: 'fileAttachedDocument'
                    },
                    {
                        text: 'Invalidar Plan',
                        iconCls: 'blocked',
                        action: 'inactivateDocument',
                        rec: rec
                    }
                );
            } else if (rec.get('stateDocument') === DukeSource.global.GiroConstants.INVALID || rec.get('stateDocument') === DukeSource.global.GiroConstants.FINISHED) {
                addMenu.add(
                    {
                        text: 'Consultar seguimiento',
                        iconCls: 'add',
                        action: 'addMonitoring',
                        rec: rec
                    },
                    {
                        text: 'Consultar Plan',
                        iconCls: 'modify',
                        action: 'modifyDocument',
                        rec: rec
                    },
                    {
                        text: 'Consultar Tareas',
                        iconCls: 'seguimiento',
                        hidden: true,
                        action: 'addWorkGestor',
                        rec: rec
                    },
                    {
                        text: 'Solicitudes de reprogramación',
                        iconCls: 'date_error',
                        action: 'attendRequestReprogram',
                        hidden: true,
                        rec: rec
                    },
                    {
                        text: 'Documentos Adjuntos',
                        iconCls: 'gestionfile',
                        action: 'fileAttachedDocument'
                    }
                );
            }
        }
        if (category === DukeSource.global.GiroConstants.GESTOR || category === DukeSource.global.GiroConstants.SECURITY_ANALYST) {
            if (rec.get('stateDocument') === DukeSource.global.GiroConstants.PROCESS) {
                addMenu.add(
                    {
                        text: 'Gestionar Tareas',
                        iconCls: 'users',
                        hidden: true,
                        action: 'addWorkGestor',
                        rec: rec
                    },
                    {
                        text: 'Consultar seguimiento',
                        iconCls: 'consult',
                        action: 'addMonitoring',
                        rec: rec
                    },
                    {
                        text: 'Solicitar reprogramaci&oacute;n',
                        iconCls: 'replicate',
                        hidden: hidden('WAP_RBN_RequestReprogramming'),
                        action: 'requestReProgramDocument',
                        rec: rec
                    },
                    {
                        text: 'Documentos Adjuntos',
                        iconCls: 'gestionfile',
                        action: 'fileAttachedDocument'
                    }
                )
            } else {
                addMenu.add(
                    {
                        text: 'Gestionar Tareas',
                        iconCls: 'users',
                        hidden: true,
                        action: 'addWorkGestor',
                        rec: rec
                    },
                    {
                        text: 'Consultar seguimiento',
                        iconCls: 'consult',
                        action: 'addMonitoring',
                        rec: rec
                    },
                    {
                        text: 'Documentos Adjuntos',
                        iconCls: 'gestionfile',
                        action: 'fileAttachedDocument'
                    }
                )
            }
        }
        addMenu.showAt(e.getXY());
    },

    treeRightClickWorkFulfillment: function (view, rec, node, index, e) {
        e.stopEvent();
        var addMenu = Ext.create('DukeSource.view.fulfillment.AddMenu', {});
        var available = rec.raw['stateDocument'] === DukeSource.global.GiroConstants.FINISHED;
        addMenu.removeAll();
        addMenu.add(
            {
                text: 'Agregar tarea',
                iconCls: 'add',
                action: 'addWorkToFulfillment',
                hidden: available
            },
            {
                text: 'Modificar tarea',
                iconCls: 'modify',
                action: 'modifyWorkFulfillment',
                hidden: available
            },
            {
                text: 'Eliminar tarea',
                iconCls: 'delete',
                action: 'deleteWorkFulfillment',
                hidden: available
            },
            {
                text: 'Historial de avance',
                iconCls: 'consult',
                action: 'historicalAdvanceActivity'
            },
            {
                text: 'Archivos Adjuntos',
                iconCls: 'attachFile',
                action: 'attachFileTaskManager'
            }
        );
        addMenu.showAt(e.getXY());
    },

    _onSaveDocumentFulfillment: function (button) {
        var win = button.up('window');
        var form = win.down('form');

        if (form.getForm().isValid()) {
            form.getForm().submit({

                url: 'http://localhost:9000/giro/saveDocument.htm?nameView=ViewPanelDocumentPending',
                waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
                method: 'POST',
                success: function (form, action) {

                    var panelDocument = Ext.ComponentQuery.query('ViewPanelDocumentPending')[0];
                    var grid = panelDocument.down('grid');
                    var valor = Ext.decode(action.response.responseText);

                    if (valor.success) {
                        DukeSource.global.DirtyView.messageNormal(valor.message);
                        grid.down('pagingtoolbar').doRefresh();
                        win.doClose();
                    } else {
                        DukeSource.global.DirtyView.messageWarning(valor.message);
                    }
                },
                failure: function (form, action) {
                    var valor = Ext.decode(action.response.responseText);
                    if (!valor.success) {
                        DukeSource.global.DirtyView.messageWarning(valor.message);
                    }

                }
            });

        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }
    },

    _onSearchResponsible: function () {
        var windowMain = Ext.ComponentQuery.query('WindowRegisterActionPlan')[0];
        var windows = Ext.create('DukeSource.view.risk.util.search.SearchUser', {
            modal: true
        }).show();
        var row = windows.down('grid').getSelectionModel().getSelection()[0];
        windows.down('grid').on('itemdblclick', function () {
            windowMain.down('textfield[name=userEndAssignment]').setValue(row.get('userName'));
            windowMain.down('UpperCaseTextFieldReadOnly[name=userLastAssignment]').setValue(row.get('fullName'));
            windowMain.down('textfield[name=emailUserReceptor]').setValue(row.get('email'));
            windows.close();
        });
    },

    _onWindowAddWorkManager: function (view) {
        var finished = DukeSource.global.GiroConstants.FINISHED;
        var invalid = DukeSource.global.GiroConstants.INVALID;
        var windows = Ext.create('DukeSource.view.fulfillment.window.ViewWindowTreeGridAssignWork', {
            modal: true,
            titleAlign: 'center'
        });
        windows.title = windows.title + ' - ' + view.rec.raw['description'];
        var available = (view.rec.raw['stateDocument'] === finished || view.rec.raw['stateDocument'] === invalid);
        windows.down('#gridTask').down('toolbar').add(0, {
            text: 'OPCIONES',
            iconCls: 'operations',
            itemId: 'operations',
            width: 160,
            menu: {
                width: 160,
                items: [{
                    text: 'Agregar tarea',
                    iconCls: 'add',
                    action: 'addWorkToFulfillment',
                    hidden: available
                }, {
                    text: 'Modificar tarea',
                    iconCls: 'modify',
                    action: 'modifyWorkFulfillment',
                    hidden: available
                }, {
                    text: 'Eliminar tarea',
                    iconCls: 'delete',
                    action: 'deleteWorkFulfillment',
                    hidden: available
                }, {
                    text: 'Historial de avance',
                    iconCls: 'consult',
                    action: 'historicalAdvanceActivity'
                }, {
                    text: 'Archivos Adjuntos',
                    iconCls: 'attachFile',
                    action: 'attachFileTaskManager'
                }
                ]
            }
        });
        windows.show();

        if (available) {
            windows.down('#gridAdvanceActivity').down('toolbar').child().setVisible(false);
        }

        var grid = windows.down('#gridTask');
        grid.store.getProxy().url = 'http://localhost:9000/giro/showListScheduleDocument.htm';
        grid.store.getProxy().extraParams = {
            idDocument: view.rec.raw['idDocument']
        };
        grid.down('pagingtoolbar').moveFirst();
    },

    _onAddWorkToFulfillment: function () {
        var grid = Ext.ComponentQuery.query('ViewPanelDocumentPending grid')[0];
        var row = grid.getSelectionModel().getSelection()[0];
        var window = Ext.create('DukeSource.view.fulfillment.window.ViewWindowAddWork', {
            modal: true,
            buttons: [
                {
                    text: 'GUARDAR',
                    scale: 'medium',
                    iconCls: 'save',
                    handler: function (button) {
                        var gridTask = Ext.ComponentQuery.query('ViewWindowTreeGridAssignWork')[0].down('#gridTask');
                        var windows = button.up('window');
                        var form = windows.down('form');
                        if (form.getForm().isValid()) {
                            form.getForm().submit({
                                url: 'http://localhost:9000/giro/saveScheduleDocument.htm?nameView=ViewPanelDocumentPending',
                                waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
                                method: 'POST',
                                success: function (form, action) {
                                    var valor = Ext.decode(action.response.responseText);
                                    if (valor.success) {
                                        DukeSource.global.DirtyView.messageNormal(valor.message);
                                        gridTask.getStore().load();
                                        windows.close();
                                    } else {
                                        DukeSource.global.DirtyView.messageWarning(valor.message);
                                    }
                                },
                                failure: function (form, action) {
                                    var valor = Ext.decode(action.response.responseText);
                                    if (!valor.success) {
                                        DukeSource.global.DirtyView.messageWarning(valor.message);
                                    }
                                }
                            })
                        } else {
                            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
                        }
                    }
                },
                {
                    text: 'SALIR',
                    scale: 'medium',
                    iconCls: 'logout',
                    handler: function () {
                        window.close();
                    }
                }
            ],
            buttonAlign: 'center'
        });
        var form = window.down('form');
        form.getForm().loadRecord(grid.getSelectionModel().getSelection()[0]);
        form.down('#dateInitial').setMinValue(row.get('dateInitPlan'));
        form.down('#dateInitial').setMaxValue(row.get('dateEnd'));
        form.down('#dateExpire').setMaxValue(row.get('dateEnd'));
        window.show();
        window.down('#descriptionTask').focus(false, 100);
    },

    _onModifyWorkFulfillment: function (record) {
        var grid = Ext.ComponentQuery.query('ViewPanelDocumentPending grid')[0];
        var row = grid.getSelectionModel().getSelection()[0];
        var gridTask = Ext.ComponentQuery.query('ViewWindowTreeGridAssignWork')[0].down('#gridTask');
        var window = Ext.create('DukeSource.view.fulfillment.window.ViewWindowAddWork', {
            modal: true,
            buttons: [
                {
                    text: 'Guardar',
                    iconCls: 'save',
                    handler: function (button) {
                        var window = button.up('window');
                        var form = window.down('form');
                        if (form.getForm().isValid()) {
                            form.getForm().submit({
                                url: 'http://localhost:9000/giro/saveScheduleDocument.htm?nameView=ViewPanelDocumentPending',
                                waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
                                method: 'POST',
                                success: function (form, action) {
                                    var valor = Ext.decode(action.response.responseText);
                                    if (valor.success) {
                                        DukeSource.global.DirtyView.messageNormal(valor.message);
                                        gridTask.getStore().load();
                                        window.close();
                                    } else {
                                        DukeSource.global.DirtyView.messageWarning(valor.message);
                                    }
                                },
                                failure: function (form, action) {
                                    var valor = Ext.decode(action.response.responseText);
                                    if (!valor.success) {
                                        DukeSource.global.DirtyView.messageWarning(valor.message);
                                    }
                                }
                            })
                        } else {
                            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
                        }
                    }
                }
            ]
        });
        var form = window.down('form');
        form.getForm().loadRecord(row);
        form.getForm().setValues(gridTask.getSelectionModel().getSelection()[0].raw);
        form.down('datefield[name=dateInitial]').setMinValue(row.get('dateInitPlan'));
        form.down('datefield[name=dateInitial]').setMaxValue(row.get('dateEnd'));
        form.down('datefield[name=dateExpire]').setMaxValue(row.get('dateEnd'));
        window.show();
    },

    _onDeleteWorkFulfillment: function (record) {
        var view = Ext.ComponentQuery.query('ViewWindowTreeGridAssignWork')[0];
        var gridTask = view.down('#gridTask');
        var row = gridTask.getSelectionModel().getSelection()[0];

        Ext.MessageBox.show({
            title: DukeSource.global.GiroMessages.TITLE_WARNING,
            msg: 'Estas seguro que desea eliminar esta tarea?',
            icon: Ext.Msg.WARNING,
            buttonText: {
                yes: "Si", no: "No"
            },
            buttons: Ext.MessageBox.YESNO,
            fn: function (btn) {
                if (btn === 'yes') {
                    DukeSource.lib.Ajax.request({
                        method: 'POST',
                        url: 'http://localhost:9000/giro/deleteScheduleDocument.htm?nameView=ViewPanelDocumentPending',
                        params: {
                            jsonData: Ext.JSON.encode(row.raw)
                        },
                        success: function (response) {
                            response = Ext.decode(response.responseText);
                            if (response.success) {
                                DukeSource.global.DirtyView.messageNormal(response.message);

                                gridTask.getStore().load();
                                Ext.each(view.down('#taskDetail').query('field'), function (field) {
                                    field.reset();
                                });
                            } else {
                                DukeSource.global.DirtyView.messageWarning(response.message);
                            }
                        },
                        failure: function () {
                        }
                    });
                } else {
                }
            }
        });
    },

    _onModifyAdvanceActivity: function (view, node, button) {
        var record = view.record;
        if (category === DukeSource.global.GiroConstants.GESTOR || record.raw['stateRevision'] === DukeSource.global.GiroConstants.PENDING_REVISED) {
            var advanceWindow = Ext.create('DukeSource.view.fulfillment.window.ViewWindowReportAdvance', {
                modal: true,
                buttons: [
                    {
                        text: 'Guardar',
                        iconCls: 'save',
                        scale: 'medium',
                        handler: function (button) {
                            var win = button.up('window');
                            var form = win.down('form');
                            if (form.getForm().isValid()) {
                                form.getForm().submit({
                                    url: 'http://localhost:9000/giro/saveAdvanceActivity.htm?nameView=ViewTreePanelDocumentSchedule',
                                    waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
                                    method: 'POST',
                                    success: function (form, action) {
                                        var valor = Ext.decode(action.response.responseText);
                                        if (valor.success) {

                                            DukeSource.global.DirtyView.messageNormal(valor.message);
                                            win.close();

                                            var refreshNode = Ext.ComponentQuery.query('ViewWindowTreeGridAssignWork treepanel')[0].getStore()
                                                .getNodeById(valor.data);
                                            refreshNode.removeAll(false);
                                            Ext.ComponentQuery.query('ViewWindowTreeGridAssignWork treepanel')[0].getStore().load({
                                                node: refreshNode
                                            })
                                        } else {
                                            DukeSource.global.DirtyView.messageWarning(valor.message);
                                        }
                                    },
                                    failure: function (form, action) {
                                        var valor = Ext.decode(action.response.responseText);
                                        if (!valor.success) {
                                            DukeSource.global.DirtyView.messageWarning(valor.message);
                                        }

                                    }
                                })
                            } else {
                                DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
                            }
                        }
                    },
                    {
                        text: 'Salir',
                        iconCls: 'logout',
                        scale: 'medium',
                        handler: function () {
                            advanceWindow.close();
                        }
                    }
                ]
            });
            advanceWindow.down('form').getForm().setValues(record.raw);
            advanceWindow.down('datefield').setMinValue(record.raw['fieldTwo']);
            advanceWindow.down('datefield').setMaxValue(record.raw['fieldThree']);
            if (category === DukeSource.global.GiroConstants.GESTOR) {
                advanceWindow.down('textfield[name=stateRevision]').setValue(DukeSource.global.GiroConstants.REVISED);
            }
            advanceWindow.show();
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ADVANCE_VERIFIED);
        }
    },

    _onFileAttachedDocument: function () {
        var grid = Ext.ComponentQuery.query('ViewPanelDocumentPending')[0].down('grid');
        var row = grid.getSelectionModel().getSelection()[0];
        var k = Ext.create('DukeSource.view.fulfillment.window.ViewWindowFileAttachment', {
            modal: true,
            saveFile: 'saveFileAttachDocument',
            deleteFile: 'deleteFileAttachDocument',
            searchGridTrigger: 'searchGridFileAttachmentDocument',
            src: 'http://localhost:9000/giro/downloadDocument.htm',
            params: ["idDocument", "idFileAttachment", "nameFile"]
        });
        var gridFileAttachment = k.down('grid');
        gridFileAttachment.store.getProxy().extraParams = {
            idDocument: row.get('idDocument')
        };
        gridFileAttachment.store.getProxy().url = 'http://localhost:9000/giro/findFileAttachmentsDocumentAll.htm';
        gridFileAttachment.down('pagingtoolbar').moveFirst();
        k.idDocument = row.get('idDocument');
        k.show();
        if (row.get('stateDocument') === DukeSource.global.GiroConstants.FINISHED || row.get('stateDocument') === DukeSource.global.GiroConstants.INVALID) {
            k.down('toolbar').setVisible(false);
        }
    },

    _onDeleteFileAttachDocument: function () {
        var k = Ext.ComponentQuery.query('ViewWindowFileAttachment')[0];
        var grid = k.down('grid');
        var row = grid.getSelectionModel().getSelection()[0];
        k.idFileAttachment = row.get('idFileAttachment');
        DukeSource.lib.Ajax.request({
            method: 'POST',
            url: 'http://localhost:9000/giro/deleteFileAttachmentsDocument.htm',
            params: {
                idDocument: k.idDocument,
                idFileAttachment: k.idFileAttachment
            },
            success: function (response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                    grid.store.getProxy().extraParams = {
                        idDocument: k.idDocument
                    };
                    grid.store.getProxy().url = 'http://localhost:9000/giro/findFileAttachmentsDocumentAll.htm';
                    grid.down('pagingtoolbar').moveFirst();
                } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                }
            },
            failure: function (response) {
                DukeSource.global.DirtyView.messageWarning(response.message);
            }
        });
    },

    _onSaveFileAttachDocument: function () {
        var k = Ext.ComponentQuery.query('ViewWindowFileAttachment')[0];
        var form = k.down('form');
        var grid = k.down('grid');
        if (form.getForm().isValid()) {
            form.getForm().submit({
                url: 'http://localhost:9000/giro/saveFileAttachmentsDocument.htm?idDocument=' + k.idDocument,
                waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
                method: 'POST',
                success: function (form, action) {
                    var valor = Ext.decode(action.response.responseText);
                    if (valor.success) {
                        DukeSource.global.DirtyView.messageNormal(valor.message);

                        grid.store.getProxy().extraParams = {
                            idDocument: k.idDocument
                        };
                        grid.store.getProxy().url = 'http://localhost:9000/giro/findFileAttachmentsDocumentAll.htm';
                        grid.down('pagingtoolbar').moveFirst();
                    } else {
                        DukeSource.global.DirtyView.messageWarning(valor.message);
                    }
                },
                failure: function (form, action) {
                    var valor = Ext.decode(action.response.responseText);
                    if (!valor.success) {
                        DukeSource.global.DirtyView.messageWarning(valor.message);
                    }

                }
            })
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }

    },

    _onSearchTriggerFileAttachmentDocument: function (text) {
        var grid = Ext.ComponentQuery.query("ViewWindowFileAttachment grid")[0];
        DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },

    _onCollapseExpandAP: function (btn, pressed) {
        var view = btn.up('pagingtoolbar').up('grid').getView();
        var groupFeature = view.getFeature('groupSummary');
        if (pressed) {
            btn.setText('Collapse All');
            view.getEl().query('.x-grid-group-hd').forEach(function (group) {
                var groupBody = Ext.fly(group.nextSibling, '_grouping');
                groupFeature.expand(groupBody);
            });
        } else {
            btn.setText('Expand All');
            view.getEl().query('.x-grid-group-hd').forEach(function (group) {
                var groupBody = Ext.fly(group.nextSibling, '_grouping');
                groupFeature.collapse(groupBody);
            });
        }
    },

    _onModifyDocument: function (lastView) {
        var grid = Ext.ComponentQuery.query('ViewPanelDocumentPending grid')[0];
        var row = grid.getSelectionModel().getSelection()[0];
        var isAvailable = (row.get('stateDocument') !== DukeSource.global.GiroConstants.FINISHED || row.get('stateDocument') !== DukeSource.global.GiroConstants.INVALID);

        var isOwner = LINE_DEFENSE > 1 ? true : row.get('userReceptor') === userName;
        var isInMyModule = row.get('categoryRole') === category;

        DukeSource.lib.Ajax.request({
            method: 'POST',
            scope: this,
            url: 'http://localhost:9000/giro/showActionPlanById.htm?nameView=ViewPanelDocumentPending',
            params: {
                idActionPlan: row.get('idDocument')
            },
            success: function (response) {
                response = Ext.decode(response.responseText);
                if (response.success) {

                    var win = Ext.create('DukeSource.view.fulfillment.window.WindowRegisterActionPlan', {
                        title: 'Detalle plan de acción' + '-' + row.get('codePlan'),
                        rowActionPlan: row,
                        isOwner: isOwner,
                        isAvailable: isAvailable,
                        isInMyModule: isInMyModule,
                        modal: true,
                        actionType: 'modify'

                    });
                    win.show(undefined, function () {
                        win.down('form').query('.combobox').forEach(function (c) {
                            c.getStore().load();
                        });
                        win.down('form').getForm().setValues(response.data);
                        win.down('#description').setReadOnly(false);
                        if (lastView.xtype === 'datefield') {
                            win.down('datefield[name=dateEnd]').setValue(lastView.getValue());
                            if (win.down('ViewComboTypeDocument').getValue() === '3') {
                                win.down('ViewComboTypeDocument').setValue("4");
                            } else {
                                win.down('ViewComboTypeDocument').setValue("2");
                            }
                        }
                        win.down('#btnApprovePlan').setVisible(true);
                        if (row.get('checkApprove') === DukeSource.global.GiroConstants.YES) {
                            win.down('#codePlan').fieldCls = 'box-approve';
                            win.down('#btnApprovePlan').setText('Consultar aprobación');
                        }

                        win.down('#dateInitTemp').setValue(row.get('dateInitPlan'));
                        win.down('#dateEndTemp').setValue(row.get('dateEnd'));
                        win.down('#sendEmail').setValue(row.get('emailReceptor'));
                        win.down('#typeDocumentTemp').setValue(row.get('typeDocument'));
                    });

                } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                }
            }
        });
    },

    _onViewHistoryDocument: function (view) {
        var windows = Ext.create('DukeSource.view.fulfillment.window.ViewWindowHistoricalDocument', {
            modal: true
        });
        windows.down('grid').getStore().load({
            url: 'http://localhost:9000/giro/showListHistoricalDocuments.htm',
            params: {
                idDocument: view.rec.raw['idDocument']
            }
        });
        windows.title = windows.title + "-" + view.rec.raw['descriptionActionPlan'];
        windows.show();
    },

    _onInactivateDocument: function (views) {
        var view = views;
        Ext.MessageBox.show({
            title: DukeSource.global.GiroMessages.TITLE_WARNING,
            msg: 'Estas seguro que desea invalidar este plan de acción, todas las tareas y avances serán inactivados?',
            icon: Ext.Msg.WARNING,
            buttonText: {
                yes: "Si",
                no: "No"
            },
            buttons: Ext.MessageBox.YESNO,
            fn: function (btn) {
                if (btn === 'yes') {
                    var win = Ext.create('DukeSource.view.fulfillment.window.WindowInvalidPlan', {
                        modal: true,
                        idDocument: view.rec.raw['idDocument'],
                        buttons: [
                            {
                                text: 'Guardar',
                                scale: 'medium',
                                iconCls: 'save',
                                handler: function () {
                                    var form = win.down('form');
                                    if (form.getForm().isValid()) {
                                        form.getForm().submit({
                                            url: 'http://localhost:9000/giro/inactivateDocument.htm?nameView=ViewPanelDocumentPending',
                                            waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
                                            method: 'POST',
                                            success: function (form, action) {
                                                var valor = Ext.decode(action.response.responseText);
                                                if (valor.success) {
                                                    var grid = Ext.ComponentQuery.query('ViewPanelDocumentPending grid')[0];
                                                    DukeSource.global.DirtyView.messageNormal(valor.message);
                                                    win.close();
                                                    grid.getStore().load({
                                                        params: {
                                                            stateDocument: DukeSource.global.GiroConstants.PROCESS
                                                        },
                                                        url: 'http://localhost:9000/giro/showDefaultFilesByState.htm?nameView=ViewPanelDocumentPending',
                                                        callback: function (records, operation, success) {
                                                        }
                                                    });
                                                } else {
                                                    DukeSource.global.DirtyView.messageWarning(valor.message);
                                                }
                                            },
                                            failure: function (form, action) {
                                                var valor = Ext.decode(action.response.responseText);
                                                if (!valor.success) {
                                                    DukeSource.global.DirtyView.messageWarning(valor.message);
                                                }
                                            }
                                        })
                                    } else {
                                        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
                                    }
                                }
                            },
                            {
                                text: 'SALIR',
                                scale: 'medium',
                                iconCls: 'logout',
                                handler: function () {
                                    win.close();
                                }
                            }
                        ]
                    });
                    win.show();
                    win.down('#descriptionInvalid').focus(false, 100);
                } else {
                }
            }
        });
    },

    _onAddMonitoring: function (view) {
        var invalid = DukeSource.global.GiroConstants.INVALID;
        var win = Ext.create('DukeSource.view.fulfillment.window.ViewWindowPanelMonitoring', {
            idDocument: view.rec.raw['idDocument'],
            modal: true
        });
        var grid = win.down('grid');
        grid.store.getProxy().url = 'http://localhost:9000/giro/showListMonitoringDocumentActives.htm';
        grid.store.getProxy().extraParams = {
            idDocument: view.rec.raw['idDocument']
        };
        grid.getStore().load({
            callback: function () {
                if (category === DukeSource.global.GiroConstants.ANALYST) {
                    var arguments = win.down('grid').getStore().data.items;
                    for (var i = 0; i < arguments.length; i++) {
                        var obj = arguments[i];
                        if (obj.data['dateMonitoring'] === Ext.Date.format(new Date(), 'j/n/Y')) {
                            DukeSource.global.DirtyView.messageWarning('Ya existe un monitoreo para este día');
                        }
                    }
                }
            }
        });
        win.title = win.title + ' &#8702; <span style="color: red">' + view.rec.raw['codePlan'] + '</span>';
        win.view = view;
        win.show();
        if (category === DukeSource.global.GiroConstants.GESTOR || view.rec.raw['stateDocument'] === invalid) {
            win.down('toolbar').setVisible(false);
        }
    },

    _onNewMonitoringDocument: function (btn) {
        var windows = btn.up('window');
        var view = Ext.create('DukeSource.view.fulfillment.window.ViewWindowRegisterMonitoring', {
            modal: true,
            buttons: [
                {
                    text: 'Guardar',
                    iconCls: 'save',
                    scale: 'medium',
                    handler: function () {
                        if (view.down('form').getForm().isValid()) {
                            DukeSource.lib.Ajax.request({
                                method: 'POST',
                                url: 'http://localhost:9000/giro/saveMonitoringDocument.htm?nameView=ViewPanelDocumentPending',
                                params: {
                                    jsonData: Ext.JSON.encode(view.down('form').getForm().getValues())
                                },
                                success: function (response) {
                                    response = Ext.decode(response.responseText);
                                    if (response.success) {
                                        DukeSource.global.DirtyView.messageNormal(response.message);
                                        windows.down('grid').getStore().load({
                                            url: 'http://localhost:9000/giro/showListMonitoringDocumentActives.htm',
                                            params: {
                                                idDocument: windows.view.rec.raw['idDocument']
                                            }
                                        });
                                        view.close();
                                    } else {
                                        DukeSource.global.DirtyView.messageWarning(response.message);
                                    }
                                },
                                failure: function () {
                                }
                            });
                        } else {
                            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_REQUIRE);
                        }
                    }
                },
                {
                    text: 'Salir',
                    scale: 'medium',
                    handler: function () {
                        view.close();
                    },
                    iconCls: 'logout'
                }
            ],
            buttonAlign: 'center'
        });
        view.down('textfield[name=idDocument]').setValue(windows.view.rec.raw['idDocument']);
        view.down('ViewComboForeignKey').getStore().load({
            url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
            params: {
                propertyFind: 'identified',
                valueFind: 'STATEADVANCE',
                propertyOrder: 'description'
            }
        });
        view.show();
        view.down('NumberDecimalNumberObligatory').focus(false, 100);
    },

    _onModifyMonitoringDocument: function (btn) {
        var windows = btn.up('window');
        var row = windows.down('grid').getSelectionModel().getSelection()[0];
        if (row !== undefined) {
            var view = Ext.create('DukeSource.view.fulfillment.window.ViewWindowRegisterMonitoring', {
                modal: true,
                buttons: [
                    {
                        text: 'Guardar',
                        scale: 'medium',
                        iconCls: 'save',
                        handler: function () {
                            DukeSource.lib.Ajax.request({
                                method: 'POST',
                                url: 'http://localhost:9000/giro/saveMonitoringDocument.htm?nameView=ViewPanelDocumentPending',
                                params: {
                                    jsonData: Ext.JSON.encode(view.down('form').getForm().getValues())
                                },
                                success: function (response) {
                                    response = Ext.decode(response.responseText);
                                    if (response.success) {
                                        DukeSource.global.DirtyView.messageNormal(response.message);
                                        windows.down('grid').getStore().load({
                                            url: 'http://localhost:9000/giro/showListMonitoringDocumentActives.htm',
                                            params: {
                                                idDocument: windows.view.rec.raw['idDocument']
                                            }
                                        });
                                        view.close();
                                    } else {
                                        DukeSource.global.DirtyView.messageWarning(response.message);
                                    }
                                },
                                failure: function () {
                                }
                            });
                        }
                    },
                    {
                        text: 'Salir',
                        scale: 'medium',
                        handler: function () {
                            view.close();
                        },
                        iconCls: 'logout'
                    }
                ],
                buttonAlign: 'center'
            });
            view.down('ViewComboForeignKey').getStore().load({
                url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                params: {
                    propertyFind: 'identified',
                    valueFind: 'STATEADVANCE',
                    propertyOrder: 'description'
                }
            });
            view.down('form').getForm().loadRecord(row);
            view.down('textfield[name=idDocument]').setValue(windows.view.rec.raw['idDocument']);
            view.show();
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
        }
    },

    _onDeleteMonitoringDocument: function (btn) {
        var windows = btn.up('window');
        var row = windows.down('grid').getSelectionModel().getSelection()[0];
        if (row !== undefined) {
            Ext.MessageBox.show({
                title: DukeSource.global.GiroMessages.TITLE_WARNING,
                msg: 'Estás seguro que desea eliminar este registro de monitoreo?',
                icon: Ext.Msg.QUESTION,
                buttonText: {
                    yes: "Si",
                    no: "No"
                },
                buttons: Ext.MessageBox.YESNO,
                fn: function (btn) {
                    if (btn === 'yes') {
                        DukeSource.lib.Ajax.request({
                            method: 'POST',
                            url: 'http://localhost:9000/giro/deleteMonitoringDocument.htm?nameView=ViewPanelDocumentPending',
                            params: {
                                jsonData: Ext.JSON.encode(row.raw)
                            },
                            success: function (response) {
                                response = Ext.decode(response.responseText);
                                if (response.success) {
                                    DukeSource.global.DirtyView.messageNormal(response.message);
                                    windows.down('grid').getStore().load({
                                        url: 'http://localhost:9000/giro/showListMonitoringDocumentActives.htm',
                                        params: {
                                            idDocument: row.get('idDocument')
                                        }
                                    });
                                } else {
                                    DukeSource.global.DirtyView.messageWarning(response.message);
                                }
                            },
                            failure: function () {
                            }
                        });
                    } else {
                    }
                }
            });
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
        }
    },

    _onRequestReProgramDocument: function (lastView) {
        var view = Ext.create('DukeSource.view.fulfillment.window.ViewWindowRequestReProgramDocument', {
            modal: true,
            allowBlank: true,
            buttons: [
                {
                    text: 'Guardar',
                    iconCls: 'save',
                    scale: 'medium',
                    handler: function () {
                        var form = view.down('form').getForm();
                        if (form.isValid()) {
                            form.submit({
                                method: 'POST',
                                url: 'http://localhost:9000/giro/saveRequestReprogram.htm?nameView=ViewPanelDocumentPending',
                                waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
                                success: function (valor, action) {
                                    var response = Ext.decode(action.response.responseText);
                                    if (response.success) {
                                        DukeSource.global.DirtyView.messageNormal(response.message);
                                        view.close();
                                    } else {
                                        DukeSource.global.DirtyView.messageWarning(response.message);
                                    }
                                },
                                failure: function (valor, action) {
                                    var response = Ext.decode(action.response.responseText);
                                    DukeSource.global.DirtyView.messageWarning(response.message);
                                }
                            });
                        } else {
                            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
                        }

                    }
                },
                {
                    text: 'Salir',
                    iconCls: 'logout',
                    scale: 'medium',
                    handler: function () {
                        view.close();
                    }
                }
            ]
        });
        view.down('#idDocument').setValue(lastView.rec.raw['idDocument']);
        view.show();
        view.down('#dateReProgram').setMinValue(new Date());
        view.down('#dateReProgram').focus(false, 100);
    },

    _onAttendRequestReprogram: function (record) {
        var windows = Ext.create('DukeSource.view.fulfillment.window.ViewWindowPanelAttendRequestDocument', {
            modal: true
        });
        windows.down('grid').getStore().load({
            url: 'http://localhost:9000/giro/showListRequestReprogramActives.htm',
            params: {
                idDocument: record.get('idDocument')
            }
        });
        windows.title = windows.title + " - " + record.get('codePlan');
        windows.show();

        if (record.get('stateDocument') === DukeSource.global.GiroConstants.FINISHED || record.get('stateDocument') === DukeSource.global.GiroConstants.INVALID) {
            windows.down('toolbar').setVisible(false);
        }
    },

    _onAttendRequestDocument: function (btn) {
        var windows = btn.up('window');

        var row = Ext.ComponentQuery.query('ViewWindowPanelAttendRequestDocument grid')[0].getSelectionModel().getSelection()[0];

        if (row === undefined) {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
        } else {
            var view = Ext.create('DukeSource.view.fulfillment.window.ViewWindowRequestReProgramDocument', {
                allowBlank: false,
                modal: true,
                buttons: [
                    {
                        text: 'Salir',
                        iconCls: 'logout',
                        scale: 'medium',
                        handler: function () {
                            view.close();
                        }
                    },
                    {
                        text: 'Guardar',
                        iconCls: 'save',
                        scale: 'medium',
                        handler: function () {
                            var form = view.down('form').getForm();
                            if (form.isValid()) {
                                form.submit({
                                    method: 'POST',
                                    url: 'http://localhost:9000/giro/saveRequestReprogram.htm?nameView=ViewPanelDocumentPending',
                                    waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
                                    success: function (valor, action) {
                                        var response = Ext.decode(action.response.responseText);

                                        if (response.success) {

                                            windows.down('grid').getStore().load({
                                                url: 'http://localhost:9000/giro/showListRequestReprogramActives.htm',
                                                params: {
                                                    idDocument: row.get('idDocument')
                                                }
                                            });

                                            DukeSource.global.DirtyView.messageNormal(response.message);
                                            view.close();

                                        } else {
                                            DukeSource.global.DirtyView.messageWarning(response.message);
                                        }
                                    },
                                    failure: function (valor, action) {
                                        var response = Ext.decode(action.response.responseText);
                                        DukeSource.global.DirtyView.messageWarning(response.message);
                                    }
                                });
                            } else {
                                DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
                            }
                        }
                    }
                ]
            });
            view.down('textfield[name=idDocument]').setValue(row.get('idDocument'));
            view.down('fileuploadfield').setVisible(false);
            var description = view.down('UpperCaseTextArea[name=description]');
            var dateReProgram = view.down('datefield[name=dateReProgram]');
            description.setReadOnly(true);
            description.setFieldStyle('background: #D8D8D8');
            dateReProgram.setReadOnly(true);
            dateReProgram.setFieldStyle('background: #D8D8D8');
            view.height = 345;
            view.width = 554;
            view.down('form').getForm().loadRecord(row);
            view.down('ViewComboForeignKey').getStore().load({
                url: 'http://localhost:9000/giro/showListForeignKeysByTableNAme.htm',
                params: {
                    propertyFind: 'identified',
                    valueFind: 'STATEREQUEST',
                    propertyOrder: 'description'
                }
            });
            view.down('UpperCaseTextArea[name=observation]').setVisible(true);
            view.down('ViewComboForeignKey').setVisible(true);
            view.show();
            view.down('ViewComboForeignKey').focus(false, 100);
        }
    },

    _onHistoricalAdvanceActivity: function () {
        var gridTask = Ext.ComponentQuery.query('ViewWindowTreeGridAssignWork')[0].down('#gridTask');
        var row = gridTask.getSelectionModel().getSelection()[0];
        var windows = Ext.create('DukeSource.view.fulfillment.window.ViewWindowHistoricalAdvance', {
            modal: true
        });
        windows.down('grid').getStore().load({
            url: 'http://localhost:9000/giro/showListHistoricalAdvanceActivity.htm',
            params: {
                idDocument: row.raw['idDocument'],
                idDetailDocument: row.raw['idDetailDocument'],
                idScheduleDocument: row.raw['idScheduleDocument']
            }
        });
        windows.title = windows.title + "-" + row.raw['descriptionTask'];
        windows.show();
    },

    _onSaveAgreementOfActionPlan: function (btn) {
        var win = btn.up('window');
        DukeSource.lib.Ajax.request({
            method: 'POST',
            url: 'http://localhost:9000/giro/saveAgreement.htm?nameView=ViewPanelRegisterAgreement',
            params: {
                jsonData: Ext.JSON.encode(win.down('form').getForm().getValues())
            },
            success: function (response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                    DukeSource.global.DirtyView.messageNormal(response.message);

                    Ext.ComponentQuery.query('ViewWindowAgreementSearch')[0].down('grid').getStore().load();
                    win.close();

                } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                }
            },
            failure: function () {
            }
        });
    },

    _onDeleteActionPlanStatePending: function (view) {
        var jsonData = view.rec;
        Ext.MessageBox.show({
            title: DukeSource.global.GiroMessages.TITLE_WARNING,
            msg: 'Estas seguro que desea eliminar este plan de acción?',
            icon: Ext.Msg.QUESTION,
            buttonText: {
                yes: "Si",
                no: "No"
            },
            buttons: Ext.MessageBox.YESNO,
            fn: function (btn) {
                if (btn === 'yes') {
                    DukeSource.lib.Ajax.request({
                        method: 'POST',
                        url: 'http://localhost:9000/giro/deleteDetailDocument.htm?nameView=ViewPanelDocumentPending',
                        params: {
                            jsonData: Ext.JSON.encode(jsonData.raw)
                        },
                        success: function (response) {
                            response = Ext.decode(response.responseText);
                            if (response.success) {
                                var grid = Ext.ComponentQuery.query('ViewPanelDocumentPending grid')[0];

                                DukeSource.global.DirtyView.messageNormal(response.message);

                                grid.getStore().load({
                                    params: {
                                        stateDocument: DukeSource.global.GiroConstants.PENDING
                                    },
                                    url: 'http://localhost:9000/giro/showDefaultFilesByState.htm?nameView=ViewPanelDocumentPending',
                                    callback: function (records, operation, success) {
                                    }
                                });
                            } else {
                                DukeSource.global.DirtyView.messageWarning(response.message);
                            }
                        },
                        failure: function () {
                        }
                    });
                } else {
                }
            }
        });
    },

    _onFinishActionPlanStatePending: function (view) {
        var jsonData = view.rec;
        Ext.MessageBox.show({
            title: DukeSource.global.GiroMessages.TITLE_WARNING,
            msg: 'Estas seguro que desea dar por culminar este plan de acción?',
            icon: Ext.Msg.QUESTION,
            buttonText: {
                yes: "Si",
                no: "No"
            },
            buttons: Ext.MessageBox.YESNO,
            fn: function (btn) {
                if (btn === 'yes') {
                    DukeSource.lib.Ajax.request({
                        method: 'POST',
                        url: 'http://localhost:9000/giro/finalizeDetailDocument.htm?nameView=ViewPanelDocumentPending',
                        params: {
                            jsonData: Ext.JSON.encode(jsonData.raw)
                        },
                        success: function (response) {
                            response = Ext.decode(response.responseText);
                            if (response.success) {
                                var grid = Ext.ComponentQuery.query('ViewPanelDocumentPending grid')[0];

                                DukeSource.global.DirtyView.messageNormal(response.message);

                                grid.getStore().load({
                                    params: {
                                        stateDocument: DukeSource.global.GiroConstants.PENDING
                                    },
                                    url: 'http://localhost:9000/giro/showDefaultFilesByState.htm?nameView=ViewPanelDocumentPending',
                                    callback: function (records, operation, success) {
                                    }
                                });
                            } else {
                                DukeSource.global.DirtyView.messageWarning(response.message);
                            }
                        },
                        failure: function () {
                        }
                    });
                } else {
                }
            }
        });
    },

    _onAttachFileTaskManager: function () {
        var gridTask = Ext.ComponentQuery.query('ViewWindowTreeGridAssignWork')[0].down('#gridTask');
        var row = gridTask.getSelectionModel().getSelection()[0];
        var k = Ext.create('DukeSource.view.fulfillment.window.ViewWindowViewAttachDocument', {
            row: row,
            saveFile: 'http://localhost:9000/giro/saveFileAttachmentsSchedule.htm',
            deleteFile: 'http://localhost:9000/giro/deleteFileAttachmentsSchedule.htm',
            searchGridTrigger: 'searchGridFileAttachmentDocument',
            src: 'http://localhost:9000/giro/downloadFileAttachmentsSchedule.htm',
            params: ["idDocument", "idDetailDocument", "idScheduleDocument", "idFileAttachment", "nameFile"]
        });
        var gridFileAttachment = k.down('grid');
        gridFileAttachment.store.getProxy().extraParams = {
            idDocument: row.get('idDocument'),
            idDetailDocument: row.get('idDetailDocument'),
            idScheduleDocument: row.get('idScheduleDocument')
        };
        gridFileAttachment.store.getProxy().url = 'http://localhost:9000/giro/showListFileAttachmentsSchedule.htm';
        gridFileAttachment.down('pagingtoolbar').moveFirst();
        k.idDocument = row.get('idDocument');
        k.idDetailDocument = row.get('idDetailDocument');
        k.idScheduleDocument = row.get('idScheduleDocument');
        k.show();
    },

    _onItemClickGridTask: function (view, record) {
        var gridActivity = Ext.ComponentQuery.query('ViewWindowTreeGridAssignWork')[0].down('#gridAdvanceActivity');
        gridActivity.store.getProxy().url = 'http://localhost:9000/giro/showListAdvanceActivityTask.htm?nameView=ViewTreeGridPanelCollaborator';
        gridActivity.store.getProxy().extraParams = {
            idDocument: record.get('idDocument'),
            idDetailDocument: record.get('idDetailDocument'),
            idScheduleDocument: record.get('idScheduleDocument')
        };
        Ext.ComponentQuery.query('ViewWindowTreeGridAssignWork')[0].down('#ptAdvanceActivity').moveFirst();
    },

    _onModifyAdvanceActivityManager: function () {
        var gridActivity = Ext.ComponentQuery.query('ViewWindowTreeGridAssignWork')[0].down('#gridAdvanceActivity');
        var row = gridActivity.getSelectionModel().getSelection()[0];
        if (row !== undefined) {
            if (category === DukeSource.global.GiroConstants.GESTOR || row.raw['stateRevision'] === DukeSource.global.GiroConstants.PENDING_REVISED) {
                var advanceWindow = Ext.create('DukeSource.view.fulfillment.window.ViewWindowReportAdvance', {
                    buttons: [
                        {
                            text: 'Guardar',
                            iconCls: 'save',
                            action: 'saveAdvancedActivity'
                        },
                        {
                            text: 'Salir',
                            iconCls: 'logout',
                            handler: function () {
                                advanceWindow.close();
                            }
                        }
                    ]
                });
                advanceWindow.title = 'Verificación de avance de tarea';
                advanceWindow.down('form').getForm().setValues(row.raw);
                advanceWindow.down('textfield[name=idScheduleDocument]').setValue(row.get('idScheduleDocument'));
                advanceWindow.down('datefield').setMinValue(row.get('fieldTwo'));
                advanceWindow.down('datefield').setMaxValue(row.get('fieldThree'));
                if (category === DukeSource.global.GiroConstants.GESTOR) {
                    advanceWindow.down('textfield[name=stateRevision]').setValue(DukeSource.global.GiroConstants.REVISED);
                }
                advanceWindow.show();
            } else {
                DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ADVANCE_VERIFIED);
            }
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
        }
    },

    _onSaveAdvancedActivity: function (button) {
        var windows = button.up('window');
        var form = windows.down('form');
        if (form.getForm().isValid()) {
            form.getForm().submit({
                url: 'http://localhost:9000/giro/saveAdvanceActivity.htm?nameView=ViewPanelDocumentPending',
                waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
                method: 'POST',
                success: function (form, action) {
                    var valor = Ext.decode(action.response.responseText);

                    if (valor.success) {
                        DukeSource.global.DirtyView.messageNormal(valor.message);
                        windows.close();

                        var gridTask = Ext.ComponentQuery.query('ViewWindowTreeGridAssignWork')[0].down('#gridTask');
                        var row = gridTask.getSelectionModel().getSelection()[0];
                        var gridActivity = Ext.ComponentQuery.query('ViewWindowTreeGridAssignWork')[0].down('#gridAdvanceActivity');
                        gridActivity.store.getProxy().url = 'http://localhost:9000/giro/showListAdvanceActivityTask.htm?nameView=ViewPanelDocumentPending';
                        gridActivity.store.getProxy().extraParams = {
                            idDocument: row.get('idDocument'),
                            idDetailDocument: row.get('idDetailDocument'),
                            idScheduleDocument: row.get('idScheduleDocument')
                        };
                        Ext.ComponentQuery.query('ViewWindowTreeGridAssignWork')[0].down('#ptAdvanceActivity').moveFirst();
                    } else {
                        DukeSource.global.DirtyView.messageWarning(valor.message);
                    }
                },
                failure: function (form, action) {
                    var valor = Ext.decode(action.response.responseText);
                    if (!valor.success) {
                        DukeSource.global.DirtyView.messageWarning(valor.message);
                    }

                }
            })
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }
    },

    _onAttachFileAdvanceActivityManager: function () {
        var gridTask = Ext.ComponentQuery.query('ViewWindowTreeGridAssignWork')[0].down('#gridAdvanceActivity');
        var row = gridTask.getSelectionModel().getSelection()[0];
        var k = Ext.create('DukeSource.view.fulfillment.window.ViewWindowViewAttachDocument', {
            row: row,
            saveFile: 'http://localhost:9000/giro/saveFileAttachmentsOfActivityAdvance.htm',
            deleteFile: 'http://localhost:9000/giro/deleteFileAttachmentsOfActivityAdvance.htm',
            searchGridTrigger: 'searchGridFileAttachmentDocument',
            src: 'http://localhost:9000/giro/downloadFileAttachAdvance.htm',
            params: ["idFileAttachment", "idAdvance", "nameFile"]
        });
        var gridFileAttachment = k.down('grid');
        gridFileAttachment.store.getProxy().extraParams = {
            idAdvance: row.get('idAdvance')
        };
        gridFileAttachment.store.getProxy().url = 'http://localhost:9000/giro/showFilesAttachAdvance.htm';
        gridFileAttachment.down('pagingtoolbar').moveFirst();

        k.idAdvance = row.get('idAdvance');
        k.show();

    },

    rightClickGridActivity: function (view, rec, node, index, e) {
        e.stopEvent();
        var gridTask = Ext.ComponentQuery.query('ViewWindowTreeGridAssignWork')[0].down('#gridTask');
        var row = gridTask.getSelectionModel().getSelection()[0];
        var available = row.raw['stateDocument'] === DukeSource.global.GiroConstants.FINISHED;
        var addMenu = Ext.create('DukeSource.view.fulfillment.AddMenu', {});
        addMenu.removeAll();
        addMenu.add(
            {
                text: 'Verificar Avance',
                iconCls: 'modify',
                action: 'modifyAdvanceActivityManager',
                hidden: available
            }, {
                text: 'Archivos Adjuntos',
                iconCls: 'attachFile',
                action: 'attachFileAdvanceActivityManager'
            }
        );
        addMenu.showAt(e.getXY());
    },

    _onFindDetailDocument: function (btn) {
        var me = btn.up('#AdvancedSearchActionPlan');
        if (me.getForm().isValid()) {
            var validityPlan = me.down('#validityPlan').getValue() === null ? '' : me.down('#validityPlan').getValue();
            var statePlan = me.down('#statePlan').getValue() === undefined ? '' : me.down('#statePlan').getValue();
            var values =
                me.down('#dateInitFirst').getRawValue() + ',' + me.down('#dateInitSecond').getRawValue() + ',' +
                validityPlan + ',' + statePlan + ',' + me.down('#idUserEmitted').getRawValue() + ',' + me.down('#idUserReceptor').getRawValue() + ',' + me.down('#codePlan').getRawValue();
            var fields = 'd.dateReception,d.dateReception,' +
                'td.idTypeDocument,sd.idStateDocument,ue.username,ur.username,d.codePlan';
            var types = 'Date,Date,Long,String,String,String,String';
            var operator = 'majorEqual,minorEqual,equal,like,equal,equal,equal';
            var grid = Ext.ComponentQuery.query('ViewPanelDocumentPending grid')[0];


            grid.store.getProxy().url = 'http://localhost:9000/giro/findDetailDocument.htm';
            grid.store.getProxy().extraParams = {
                fields: fields,
                values: values,
                types: types,
                operator: operator,
                searchIn: category === DukeSource.global.GiroConstants.ANALYST ? 'ActionPlan' : 'ActionPlanProcess',
                stateDocument: statePlan
            };
            grid.down('pagingtoolbar').moveFirst();
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_FIELDS_ERROR);
        }
    },

    _onApproveActionPlan: function (btn) {
        var win = btn.up('window');
        var form = win.down('form').getForm();

        if (form.isValid()) {
            DukeSource.lib.Ajax.request({
                waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                method: 'POST',
                url: 'http://localhost:9000/giro/approveActionPlan.htm',
                params: {
                    jsonData: Ext.JSON.encode(form.getValues())
                },
                scope: this,
                success: function (response) {

                    response = Ext.decode(response.responseText);

                    if (response.success) {

                        DukeSource.global.DirtyView.messageNormal(response.message);
                        win.close();

                        var window = Ext.ComponentQuery.query('WindowRegisterActionPlan')[0];
                        window.close();

                        var grid = Ext.ComponentQuery.query('ViewPanelDocumentPending grid')[0];
                        grid.down('pagingtoolbar').doRefresh();

                    } else {
                        DukeSource.global.DirtyView.messageWarning(response.message);
                    }
                },
                failure: function () {
                }
            });
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }
    }
});