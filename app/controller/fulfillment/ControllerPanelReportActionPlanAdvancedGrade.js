Ext.define(
  "DukeSource.controller.fulfillment.ControllerPanelReportActionPlanAdvancedGrade",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboTypeRiskEvaluation",
      "risk.parameter.combos.ViewComboProcess"
    ],
    init: function() {
      this.control({
        // '[action=generateReportActionPlanConsolidatePdf]': {
        //     click: this._onGenerateReportActionPlanConsolidatePdf
        // },
        "[action=generateReportActionPlanAdvancedXls]": {
          click: this._onGenerateReportActionPlanAdvancedXls
        }
      });
    },

    // _onGenerateReportActionPlanConsolidatePdf: function (btn) {
    //     var container = btn.up('container[name=generalContainer]');
    //     var agreement = container.down('textfield[name=idAgreement]').getValue() == "" ? 'T' : container.down('textfield[name=idAgreement]').getValue();
    //     var value = container.down('datefield[name=dateInit]').getRawValue() + ';' +
    //         container.down('datefield[name=dateEnd]').getRawValue() + ';' + agreement + ';' + container.down('ViewComboTypeRiskEvaluation').getValue().toString()
    //         + ';' + container.down('#process').getValue().toString();
    //     var panelReport = container.down('panel[name=panelReport]');
    //     panelReport.removeAll();
    //     panelReport.add(
    //         {
    //             xtype: 'component',
    //             autoEl: {
    //                 tag: 'iframe',
    //                 src: 'pdfGeneralReport.htm?values=' + value + '&names=' + 'dateInit,dateEnd,' +
    //                 'agreement,typeEvaluation,process' +
    //                 '&types=' + 'Date,Date,String,String,String' + '&nameReport=' + 'RORIEV0006' + '&typeReport=noStandard'
    //             }
    //         });
    // },
    _onGenerateReportActionPlanAdvancedXls: function(btn) {
      var container = btn.up("container[name=generalContainer]");
      // var agreement = container.down('textfield[name=idAgreement]').getValue() == "" ? 'T' : container.down('textfield[name=idAgreement]').getValue();
      var process =
        container.down("ViewComboProcess").getValue() == null
          ? "T"
          : container.down("ViewComboProcess").getValue();
      var value =
        container.down("datefield[name=dateInit]").getRawValue() +
        ";" +
        container.down("datefield[name=dateEnd]").getRawValue() +
        ";" +
        container
          .down("ViewComboTypeRiskEvaluation")
          .getValue()
          .toString() +
        ";" +
        process;
      var panelReport = container.down("panel[name=panelReport]");
      panelReport.removeAll();
      panelReport.add({
        xtype: "component",
        autoEl: {
          tag: "iframe",
          src:
            "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
            value +
            "&names=" +
            "dateIni,dateEnd,typeEvaluation,process" +
            "&types=" +
            "Date,Date,String,String" +
            "&nameReport=" +
            "ROXXX0011XLS" +
            "&typeReport=noStandard"
        }
      });
    }
  }
);
