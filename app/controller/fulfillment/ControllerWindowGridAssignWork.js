Ext.define('DukeSource.controller.fulfillment.ControllerWindowGridAssignWork', {
    extend: 'Ext.app.Controller',
    stores: [],
    models: [],
    views: [],
    init: function () {
        this.control({
            '[action=exportFulFillmentPdf]': {
                click: this._onExportFulFillmentPdf

            }
        });
    }

});