Ext.define('DukeSource.controller.main.SessionController', {
    singleton: true,
    interval: 1000 * 60 * 15,
    lastActive: null,
    maxInactive: 1000 * 60 * 0.15,
    remaining: 0,
    ui: Ext.getBody(),

    window: Ext.create('Ext.window.Window', {
        bodyPadding: 5,
        closable: false,
        closeAction: 'hide',
        modal: true,
        resizable: false,
        title: 'TIEMPO DE SESSION FINALIZADO',
        width: 325,
        items: [
            {
                xtype: 'container',
                frame: true,
                html: "Su session expira automaticamente luego de 15 minutos de inactividad.</br>"
            },
            {
                xtype: 'label',
                text: ''
            }
        ],
        buttons: [
            {
                text: 'Logout',
                url: 'logout',
                hrefTarget: '_self',
                handler: function () {
                    DukeSource.controller.main.SessionController.window.hide();
                    Ext.getComponent();
                }
            }
        ]
    }),

    constructor: function (config) {
        var me = this;
        this.sessionTask = {
            run: me.monitorUI,
            interval: me.interval,
            scope: me
        };
        this.countDownTask = {
            run: me.countDown,
            interval: 1000,
            scope: me
        };
    },

    captureActivity: function (eventObj, el, eventOptions) {
        this.lastActive = new Date();
    },

    monitorUI: function () {
        var now = new Date();
        var inactive = (now - this.lastActive);

        if (inactive >= this.maxInactive) {
            this.stop();
            this.window.show();
            this.remaining = 60;
            Ext.TaskManager.start(this.countDownTask);
        }
    },

    start: function () {
        this.lastActive = new Date();

        this.ui = Ext.getBody();

        this.ui.on('mousemove', this.captureActivity, this);
        this.ui.on('keydown', this.captureActivity, this);

        Ext.TaskManager.start(this.sessionTask);
    },

    stop: function () {
        Ext.TaskManager.stop(this.sessionTask);
        this.ui.un('mousemove', this.captureActivity, this);
        this.ui.un('keydown', this.captureActivity, this);
    },

    countDown: function () {
//        this.window.down('label').update('Your session will expire in ' + this.remaining + ' second' + ((this.remaining == 1) ? '.' : 's.'));
//
//        --this.remaining;
//
//        if (this.remaining < 0) {
//            this.window.down('button[action="logout"]').handler();
//        }
    }
});