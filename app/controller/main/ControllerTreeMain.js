Ext.define("DukeSource.controller.main.ControllerTreeMain", {
  extend: "Ext.app.Controller",
  models: [],
  stores: [],
  views: [],
  refs: [
    {
      ref: "viewTreeMain",
      selector: "ViewTreeMain"
    }
  ],
  init: function() {
    this.control({
      ViewTreeMain: {
        itemclick: this._onItemClick
      }
    });
  },
  _onItemClick: function(view, record) {
    var id = record.get("id");
    if (id !== undefined) {
      DukeSource.lib.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/buildPanel.htm",
        params: {
          id: id
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          var data = response.data;

          if (data["typeAccess"] === "2") {
            if (data["permitSubTabs"] === "N") {
              view.up("panel").collapse();

              DukeSource.getApplication().loadController(
                data["nameController"]
              );
              DukeSource.getApplication().centerPanel.addPanel(
                Ext.create(data["nameClass"], {
                  title: data["nameTab"],
                  closeAction: "destroy",
                  closable: data["closable"],
                  typeObject: data["typeObject"]
                })
              );
            } else {
              var generalView = Ext.create(
                "DukeSource.view.main.ViewPanelGeneral",
                {
                  title: data["nameTab"],
                  closable: true,
                  typeObject: data["typeObject"]
                }
              );

              view.up("panel").collapse();

              DukeSource.getApplication().centerPanel.addPanel(generalView);
              DukeSource.getApplication().loadController(
                data["nameController"]
              );
              generalView.addPanel(
                Ext.create(data["nameClass"], {
                  title: data["nameTab"]
                })
              );
            }
          } else {
          }
        },
        failure: function(err) {}
      });
    } else {
    }
  }
});
