Ext.define("DukeSource.controller.risk.kri.ControllerPanelEvaluationKriRisk", {
  extend: "Ext.app.Controller",
  stores: [],
  models: [],
  views: [
    "risk.parameter.combos.ViewComboMonth",
    "risk.parameter.combos.ViewComboYear",
    "risk.parameter.combos.ViewComboYearKri",
    "risk.parameter.combos.ViewComboMonthKri"
  ],
  init: function() {
    this.control({
      "[action=saveEvaluationKri]": {
        click: this._onSaveEvaluationKri
      },
      "[action=entryEvaluationKri]": {
        click: this._onEntryEvaluationKri
      },
      "[action=searchEvaluationKri]": {
        click: this._onSearchEvaluationKri
      },
      "[action=deleteEvaluationKri]": {
        click: this._onDeleteEvaluationKri
      },
      "[action=consultEvaluationKri]": {
        select: this._onConsultEvaluationKri
      },
      "[action=trackingKri]": {
        click: this._onTrackingKri
      }
    });
  },

  _onSaveEvaluationKri: function(button) {
    var panel = button.up("PanelEvaluationKriRisk");
    var grid = panel.down("grid");
    var arrayDataToSave = [];
    var records = grid.getStore().getRange();
    Ext.Array.each(records, function(record, index, countriesItSelf) {
      arrayDataToSave.push(record.data);
    });
    var form = panel.down("form[region=north]").getForm();

    if (form.isValid()) {
      form.submit({
        method: "POST",
        url:
          "http://localhost:9000/giro/saveKriByYearMonth.htm?nameView=ViewPanelConfigMasterKri",
        timeout: 120000,
        params: {
          idKeyRiskIndicator: panel.down("#idKeyRiskIndicator").getValue(),
          indicatorNormalizer: panel.down("#indicatorNormalizer").getValue(),
          year: panel.down("ViewComboYearKri").getValue(),
          month: panel.down("ViewComboMonthKri").getValue(),
          indicatorKri: panel.down("#indicatorKri").getValue(),
          dateEvaluation: panel.down("#dateEvaluation").getValue(),
          timeKri: "S",
          jsonData: Ext.JSON.encode(arrayDataToSave)
        },
        waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
        success: function(form, action) {
          var response = Ext.decode(action.response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageNormal(response.message);
            var data = JSON.parse(response.data);
            var evaluation = JSON.parse(response.evaluation);

            panel
              .down("#resultQualification")
              .setValue(evaluation.evaluationNotNormalizer);
            DukeSource.global.DirtyView.colorToElement(
              panel.down("#resultQualification"),
              evaluation.levelColourQualificationEachKri
            );
            panel
              .down("#descriptionQualification")
              .setValue(evaluation.descriptionQualificationEachKri);
            DukeSource.global.DirtyView.colorToElement(
              panel.down("#descriptionQualification"),
              evaluation.levelColourQualificationEachKri
            );

            panel
              .down("UpperCaseTextFieldReadOnly[name=resultNormalized]")
              .setValue(evaluation.evaluationResult);
            DukeSource.global.DirtyView.colorToElement(
              panel.down("UpperCaseTextFieldReadOnly[name=resultNormalized]"),
              evaluation.levelColour
            );
            panel
              .down("UpperCaseTextFieldReadOnly[name=descriptionResult]")
              .setValue(evaluation.descriptionQualificationKri);
            DukeSource.global.DirtyView.colorToElement(
              panel.down("UpperCaseTextFieldReadOnly[name=descriptionResult]"),
              evaluation.levelColour
            );
            grid.store.loadRawData(data, false);
          } else {
            DukeSource.global.DirtyView.messageWarning(response.message);
          }
        },
        failure: function(form, action) {
          var valor = Ext.decode(action.response.responseText);
          if (!valor.success) {
            DukeSource.global.DirtyView.messageWarning(valor.message);
          }
        }
      });
    } else {
      DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
    }
  },

  _onEntryEvaluationKri: function() {
    var panel = Ext.ComponentQuery.query("PanelEvaluationKriRisk")[0];
    var grid = panel.down("grid");
    var store = grid.getStore();
    var searchKri = Ext.create(
      "DukeSource.view.risk.kri.windows.ViewWindowSearchMasterKri",
      {
        modal: true
      }
    ).show();

    searchKri.down("grid").on("itemdblclick", function(view, record) {
      DukeSource.lib.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/getTemplateJsonDefault.htm",
        params: {
          idKeyRiskIndicator: record.get("idKeyRiskIndicator"),
          indicatorNormalizer: record.get("indicatorNormalizer"),
          indicatorKri: record.get("indicatorKri")
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            panel.down("#btnSaveEvaluation").setVisible(true);
            panel
              .down("form")
              .getForm()
              .loadRecord(record);
            panel.down("#dateEvaluation").setValue(response.dateEvaluation);

            DukeSource.global.DirtyView.toReadOnly(panel.down("#resultQualification"));
            panel.down("#resultQualification").reset();
            DukeSource.global.DirtyView.toReadOnly(panel.down("#descriptionQualification"));
            panel.down("#descriptionQualification").reset();

            DukeSource.global.DirtyView.toReadOnly(panel.down("#resultNormalized"));
            panel.down("#resultNormalized").reset();
            DukeSource.global.DirtyView.toReadOnly(panel.down("#descriptionResult"));
            panel.down("#descriptionResult").reset();

            var comboYearKri = panel.down("ViewComboYearKri");
            var comboMonthKri = panel.down("ViewComboMonthKri");
            comboYearKri.getStore().removeAll();
            comboMonthKri.getStore().removeAll();

            comboYearKri
              .getStore()
              .loadRawData(JSON.parse(response.years), true);
            comboMonthKri
              .getStore()
              .loadRawData(JSON.parse(response.months), true);

            comboYearKri.setValue(comboYearKri.getStore().first().data.keyInt);
            comboMonthKri.setValue(comboMonthKri.getStore().last().data.keyInt);

            comboYearKri.setReadOnly(true);
            comboMonthKri.setReadOnly(true);

            var fields = JSON.parse(response.fields);
            var columns = JSON.parse(response.columns);
            var data = JSON.parse(response.data);
            store.model.setFields(fields);
            grid.reconfigure(store, columns);
            store.removeAll();
            store.loadRawData(data, true);
            searchKri.close();
          } else {
            DukeSource.global.DirtyView.messageWarning(response.message);
          }
        },
        failure: function() {}
      });
    });
  },

  _onSearchEvaluationKri: function() {
    var panel = Ext.ComponentQuery.query("PanelEvaluationKriRisk")[0];
    var grid = panel.down("grid");
    var store = grid.getStore();
    if (
      Ext.ComponentQuery.query("ViewWindowSearchMasterKri")[0] === undefined
    ) {
      var searchKri = Ext.create(
        "DukeSource.view.risk.kri.windows.ViewWindowSearchMasterKri",
        {
          modal: true
        }
      ).show();
    }
    searchKri.down("grid").on("itemdblclick", function(view, record) {
      var yearWindow = record.get("year");
      var monthWindow = record.get("month");
      panel.record = record;

      refreshEvaluationKri(record, yearWindow, monthWindow, panel, store, grid);
      searchKri.close();
    });
  },

  _onConsultEvaluationKri: function() {
    var panel = Ext.ComponentQuery.query("PanelEvaluationKriRisk")[0];
    var grid = panel.down("grid");
    var store = grid.getStore();
    DukeSource.lib.Ajax.request({
      method: "POST",
      url: "http://localhost:9000/giro/buildJsonByIdKriAndYearMonth.htm",
      params: {
        idKeyRiskIndicator: panel.down("#idKeyRiskIndicator").getValue(),
        indicatorNormalizer: panel.down("#indicatorNormalizer").getValue(),
        indicatorKri: panel.down("#indicatorKri").getValue(),
        year: panel.down("ViewComboYearKri").getValue(),
        month: panel.down("ViewComboMonthKri").getValue()
      },
      success: function(response) {
        response = Ext.decode(response.responseText);
        if (response.success) {
          var fields = JSON.parse(response.fields);
          var columns = JSON.parse(response.columns);
          var data = JSON.parse(response.data);
          store.model.setFields(fields);
          grid.reconfigure(store, columns);
          store.removeAll();
          store.loadRawData(data, true);

          var evaluation = JSON.parse(response.evaluation);

          panel
            .down("#resultQualification")
            .setValue(evaluation.evaluationNotNormalizer);
          DukeSource.global.DirtyView.colorToElement(
            panel.down("#resultQualification"),
            evaluation.levelColourQualificationEachKri
          );
          panel
            .down("#descriptionQualification")
            .setValue(evaluation.descriptionQualificationEachKri);
          DukeSource.global.DirtyView.colorToElement(
            panel.down("#descriptionQualification"),
            evaluation.levelColourQualificationEachKri
          );
          panel.down("#year").setValue(response.year);
          panel.down("#month").setValue(response.month);
          panel.down("#dateEvaluation").setValue(response.dateEvaluation);

          if (
            panel.down("textfield[name=indicatorNormalizer]").getValue() === "N"
          ) {
            DukeSource.global.DirtyView.toReadOnly(panel.down("#resultNormalized"));
            panel.down("#resultNormalized").reset();
            DukeSource.global.DirtyView.toReadOnly(panel.down("#descriptionResult"));
            panel.down("#descriptionResult").reset();
          } else if (
            panel.down("textfield[name=indicatorNormalizer]").getValue() === "S"
          ) {
            panel
              .down("#resultNormalized")
              .setValue(evaluation.evaluationResult);
            DukeSource.global.DirtyView.colorToElement(
              panel.down("#resultNormalized"),
              evaluation.levelColour
            );
            panel
              .down("#descriptionResult")
              .setValue(evaluation.descriptionQualificationKri);
            DukeSource.global.DirtyView.colorToElement(
              panel.down("#descriptionResult"),
              evaluation.levelColour
            );
          }
        } else {
          DukeSource.global.DirtyView.messageWarning(response.message);
        }
      },
      failure: function() {}
    });
  },

  _onDeleteEvaluationKri: function(btn) {
    var panel = Ext.ComponentQuery.query("PanelEvaluationKriRisk")[0];
    var year = panel.down("#year");
    var month = panel.down("#month");
    var grid = panel.down("grid");

    if (!panel.down("#btnSaveEvaluation").isVisible()) {
      DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
    } else {
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
        msg:
          "Desea eliminar la evaluación " +
          year.fieldLabel +
          ": " +
          year.getRawValue() +
          " y " +
          month.fieldLabel +
          ": " +
          month.getRawValue() +
          "?",
        icon: Ext.Msg.WARNING,
        buttonText: {
          yes: "Si"
        },
        buttons: Ext.MessageBox.YESNO,
        fn: function(btn) {
          if (btn === "yes") {
            DukeSource.lib.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/deleteDataEvaluationKri.htm?nameView=ViewPanelConfigMasterKri",
              params: {
                idKeyRiskIndicator: panel.down("#idKeyRiskIndicator").value,
                indicatorNormalizer: panel.down("#indicatorNormalizer").value,
                indicatorKri: panel.down("#indicatorKri").value,
                year: year.value,
                month: month.value
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageNormal(response.message);

                  if (response.year !== []) {
                    var comboYearKri = panel.down("ViewComboYearKri");
                    var comboMonthKri = panel.down("ViewComboMonthKri");
                    comboYearKri.getStore().removeAll();
                    comboYearKri
                      .getStore()
                      .loadRawData(JSON.parse(response.years), true);
                    comboMonthKri.getStore().removeAll();
                    comboMonthKri
                      .getStore()
                      .loadRawData(JSON.parse(response.months), true);
                    comboYearKri.setReadOnly(false);
                    comboMonthKri.setReadOnly(false);
                  }
                  grid.down("pagingtoolbar").doRefresh();
                  panel
                    .down("#containerResult")
                    .getForm()
                    .reset();
                  panel
                    .down("#containerResult")
                    .query(".textfield")
                    .forEach(function(c) {
                      DukeSource.global.DirtyView.toReadOnly(c);
                    });
                } else {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              },
              failure: function(response) {
                DukeSource.global.DirtyView.messageWarning(response.message);
              }
            });
          }
        }
      });
    }
  },

  _onTrackingKri: function() {
    var panel = Ext.ComponentQuery.query("PanelEvaluationKriRisk")[0];
    var grid = panel.down("grid");
    var row = grid.getSelectionModel().getSelection()[0];

    if (row === undefined) {
      DukeSource.global.DirtyView.messageAlert(
        DukeSource.global.GiroMessages.TITLE_WARNING,
        DukeSource.global.GiroMessages.MESSAGE_ITEM,
        Ext.Msg.WARNING
      );
    } else {
      var view = Ext.create(
        "DukeSource.view.risk.kri.windows.WindowTraceEvaluationKri",
        {
          modal: true,
          idKri: row.get("idKeyRiskIndicator"),
          yearMonth: row.get("yearMonth")
        }
      );
      view
        .down("grid")
        .setTitle(
          view.title +
            " ---> " +
            panel.down("#codeKri").getValue() +
            " ---> " +
            row.get("description")
        );

      view.down("#gridTrackEvaluationKri").store.getProxy().extraParams = {
        idKri: row.get("idKeyRiskIndicator"),
        yearMonth: row.get("yearMonth")
      };
      view.down("#gridTrackEvaluationKri").store.getProxy().url =
        "http://localhost:9000/giro/showTracingEvaluationKri.htm";
      view
        .down("#gridTrackEvaluationKri")
        .down("pagingtoolbar")
        .moveFirst();

      view.show();
    }
  }
});

function refreshEvaluationKri(
  record,
  yearWindow,
  monthWindow,
  panel,
  store,
  grid
) {
  store.removeAll();
  panel
    .down("ViewComboYearKri")
    .getStore()
    .removeAll();
  panel
    .down("ViewComboMonthKri")
    .getStore()
    .removeAll();
  DukeSource.lib.Ajax.request({
    method: "POST",
    url: "http://localhost:9000/giro/buildJsonLastDataInsert.htm",
    params: {
      idKeyRiskIndicator: record.get("idKeyRiskIndicator"),
      year: yearWindow,
      month: monthWindow,
      indicatorNormalizer: record.get("indicatorNormalizer"),
      indicatorKri: record.get("indicatorKri")
    },
    success: function(response) {
      response = Ext.decode(response.responseText);
      if (response.success) {
        panel.down("#btnSaveEvaluation").setVisible(true);
        panel
          .down("#idKeyRiskIndicator")
          .setValue(record.get("idKeyRiskIndicator"));
        panel.down("#codeKri").setValue(record.get("codeKri"));
        panel.down("#nameKri").setValue(record.get("nameKri"));
        panel.down("#abbreviation").setValue(record.get("abbreviation"));
        panel
          .down("#indicatorNormalizer")
          .setValue(record.get("indicatorNormalizer"));
        panel.down("#indicatorKri").setValue(record.get("indicatorKri"));
        panel
          .down("#abbreviationFrequency")
          .setValue(record.get("abbreviationFrequency"));
        var evaluation = JSON.parse(response.evaluation);
        if (evaluation.length === 1) {
          DukeSource.global.DirtyView.messageWarning("No existe ninguna evaluación");
        }
        panel
          .down("#resultQualification")
          .setValue(evaluation.evaluationNotNormalizer);
        DukeSource.global.DirtyView.colorToElement(
          panel.down("#resultQualification"),
          evaluation.levelColourQualificationEachKri
        );
        panel
          .down("#descriptionQualification")
          .setValue(evaluation.descriptionQualificationEachKri);
        DukeSource.global.DirtyView.colorToElement(
          panel.down("#descriptionQualification"),
          evaluation.levelColourQualificationEachKri
        );
        if (record.get("indicatorNormalizer") === "N") {
          DukeSource.global.DirtyView.toReadOnly(panel.down("#resultNormalized"));
          panel.down("#resultNormalized").reset();
          DukeSource.global.DirtyView.toReadOnly(panel.down("#descriptionResult"));
          panel.down("#descriptionResult").reset();
        } else if (record.get("indicatorNormalizer") === "S") {
          panel.down("#resultNormalized").setValue(evaluation.evaluationResult);
          DukeSource.global.DirtyView.colorToElement(
            panel.down("#resultNormalized"),
            evaluation.levelColourQualificationEachKri
          );
          panel
            .down("#descriptionResult")
            .setValue(evaluation.descriptionQualificationKri);
          DukeSource.global.DirtyView.colorToElement(
            panel.down("#descriptionResult"),
            evaluation.levelColourQualificationEachKri
          );
        }
        var comboYearKri = panel.down("ViewComboYearKri");

        var comboMonthKri = panel.down("ViewComboMonthKri");
        comboYearKri.getStore().removeAll();
        comboYearKri.getStore().loadRawData(JSON.parse(response.years), true);
        comboYearKri.setValue(comboYearKri.getStore().first().data.keyInt);
        comboMonthKri.getStore().removeAll();
        comboMonthKri.getStore().loadRawData(JSON.parse(response.months), true);
        comboMonthKri.setValue(comboMonthKri.getStore().last().data.keyInt);
        comboYearKri.setReadOnly(false);
        comboMonthKri.setReadOnly(false);
        var fields = JSON.parse(response.fields);
        var columns = JSON.parse(response.columns);
        var data = JSON.parse(response.data);
        panel.down("#dateEvaluation").setValue(response.dateEvaluation);

        store.model.setFields(fields);
        grid.reconfigure(store, columns);
        store.removeAll();
        store.loadRawData(data, true);
      } else {
        DukeSource.global.DirtyView.messageWarning(response.message);
      }
    },
    failure: function() {}
  });
}
