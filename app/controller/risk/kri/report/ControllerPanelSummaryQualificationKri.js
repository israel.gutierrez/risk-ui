Ext.define(
  "DukeSource.controller.risk.kri.report.ControllerPanelSummaryQualificationKri",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboYear",
      "risk.parameter.combos.ViewComboMonth"
    ],
    init: function() {
      this.control({
        "[action=searchMasterKriQualification]": {
          click: this._onSearchMasterKriQualification
        },
        "[action=reSearchMasterKriQualification]": {
          click: this._onReSearchMasterKriQualification
        }
      });
    },
    _onSearchMasterKriQualification: function() {
      var panel = Ext.ComponentQuery.query(
        "ViewPanelSummaryQualificationKri"
      )[0];
      if (
        Ext.ComponentQuery.query("ViewWindowSearchMasterKri")[0] == undefined
      ) {
        var searchKri = Ext.create(
          "DukeSource.view.risk.kri.windows.ViewWindowSearchMasterKri",
          {}
        );
        searchKri.down("datefield[name=dateStart]").setDisabled(false);
        searchKri.down("datefield[name=dateLimit]").setDisabled(false);
        searchKri.down("datefield[name=dateStart]").setVisible(true);
        searchKri.down("datefield[name=dateLimit]").setVisible(true);
        searchKri.show();
        searchKri.down("grid").on("itemdblclick", function(view, record) {
          var dateStart = searchKri
            .down("datefield[name=dateStart]")
            .getRawValue();
          var dateLimit = searchKri
            .down("datefield[name=dateLimit]")
            .getRawValue();
          var normalized = searchKri.down("#normalized").getRawValue();
          panel
            .down("textfield[name=idKeyRiskIndicator]")
            .setValue(record.get("idKeyRiskIndicator"));
          panel
            .down("UpperCaseTextFieldReadOnly[name=codeKri]")
            .setValue(record.get("codeKri"));
          panel
            .down("UpperCaseTextFieldReadOnly[name=nameKri]")
            .setValue(record.get("nameKri"));
          panel
            .down("UpperCaseTextFieldReadOnly[name=abbreviation]")
            .setValue(record.get("abbreviation"));
          panel.down("datefield[name=dateStart]").setValue(dateStart);
          panel.down("datefield[name=dateLimit]").setValue(dateLimit);
          if (dateStart == "" || dateLimit == "") {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              "La fecha de Inicio y Fin son obligatorios",
              Ext.Msg.WARNING
            );
          } else {
            var grid = panel.down("grid");
            grid.store.getProxy().extraParams = {
              idKeyRiskIndicator: record.get("idKeyRiskIndicator"),
              normalized: normalized,
              dateStart: dateStart,
              dateLimit: dateLimit
            };
            grid.store.getProxy().url =
              "http://localhost:9000/giro/getEvaluationKrisByRangeDate.htm";
            grid.down("pagingtoolbar").moveFirst();

            var chart = panel.down("chart");
            Ext.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/getJsonByRangeDate.htm",
              params: {
                idKeyRiskIndicator: record.get("idKeyRiskIndicator"),
                dateStart: dateStart,
                normalized: normalized,
                dateLimit: dateLimit
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  var color = JSON.parse(response.color);
                  chart.themeAttrs.colors = [
                    "#" + color[0].color1 + "",
                    "#" + color[0].color2 + "",
                    "#" + color[0].color3 + "",
                    "#" + color[0].color4 + "",
                    "#" + color[0].color5 + "",
                    "#" + color[0].color6 + "",
                    "#" + color[0].color7 + "",
                    "#" + color[0].color8 + "",
                    "#" + color[0].color9 + "",
                    "#" + color[0].color10 + ""
                  ];
                  chart.getStore().loadData(JSON.parse(response.data));
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
            searchKri.close();
          }
        });
      }
    },
    _onReSearchMasterKriQualification: function() {
      var panel = Ext.ComponentQuery.query(
        "ViewPanelSummaryQualificationKri"
      )[0];
      var grid = panel.down("grid");
      var idKeyRiskIndicator = panel
        .down("textfield[name=idKeyRiskIndicator]")
        .getValue();
      var dateStart = panel.down("datefield[name=dateStart]").getRawValue();
      var dateLimit = panel.down("datefield[name=dateLimit]").getRawValue();
      var normalized = panel.down("#normalized").getRawValue();
      grid.store.getProxy().extraParams = {
        idKeyRiskIndicator: idKeyRiskIndicator,
        dateStart: dateStart,
        dateLimit: dateLimit,
        normalized: normalized
      };
      grid.store.getProxy().url =
        "http://localhost:9000/giro/getEvaluationKrisByRangeDate.htm";
      grid.down("pagingtoolbar").moveFirst();

      var chart = panel.down("chart");
      Ext.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/getJsonByRangeDate.htm",
        params: {
          idKeyRiskIndicator: idKeyRiskIndicator,
          dateStart: dateStart,
          dateLimit: dateLimit,
          normalized: normalized
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            var color = JSON.parse(response.color);
            chart.themeAttrs.colors = [
              "#" + color[0].color1 + "",
              "#" + color[0].color2 + "",
              "#" + color[0].color3 + "",
              "#" + color[0].color4 + "",
              "#" + color[0].color5 + "",
              "#" + color[0].color6 + "",
              "#" + color[0].color7 + "",
              "#" + color[0].color8 + "",
              "#" + color[0].color9 + "",
              "#" + color[0].color10 + ""
            ];
            chart.getStore().loadData(JSON.parse(response.data));
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    }
  }
);
