Ext.define(
  "DukeSource.controller.risk.kri.report.ControllerPanelGeneralQualificationEvent",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboYearKri",
      "risk.parameter.combos.ViewComboMonthKri",
      "risk.parameter.combos.ViewComboEventOne"
    ],
    init: function() {
      this.control({
        "[action=reSearchMasterEventOneQualification]": {
          click: this._onReSearchMasterEventOneQualification
        }
      });
    },

    _onReSearchMasterEventOneQualification: function() {
      var panel = Ext.ComponentQuery.query(
        "ViewPanelGeneralQualificationEvent"
      )[0];
      var cbo = panel.down("ViewComboEventOne");
      var dateStart = panel.down("datefield[name=dateStart]").getRawValue();
      var dateLimit = panel.down("datefield[name=dateLimit]").getRawValue();

      if (Ext.isEmpty(dateStart) || Ext.isEmpty(dateLimit)) {
      } else {
        var grid = panel.down("grid");
        grid.store.getProxy().extraParams = {
          idEventOne: cbo.getValue(),
          dateStart: dateStart,
          dateLimit: dateLimit
        };
        grid.store.getProxy().url =
          "http://localhost:9000/giro/getScoreEventByRangeDate.htm";
        grid.down("pagingtoolbar").moveFirst();
        var chart = panel.down("chart");
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/buildJsoScoreEventByRangeDate.htm",
          params: {
            idEventOne: cbo.getValue(),
            dateStart: dateStart,
            dateLimit: dateLimit
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              var color = JSON.parse(response.color);
              chart.themeAttrs.colors = [
                "#" + color[0].color1 + "",
                "#" + color[0].color2 + "",
                "#" + color[0].color3 + "",
                "#" + color[0].color4 + "",
                "#" + color[0].color5 + "",
                "#" + color[0].color6 + "",
                "#" + color[0].color7 + "",
                "#" + color[0].color8 + "",
                "#" + color[0].color9 + "",
                "#" + color[0].color10 + ""
              ];
              chart.getStore().loadData(JSON.parse(response.data));
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      }
    }
  }
);
