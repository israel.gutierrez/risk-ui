Ext.define(
  "DukeSource.controller.risk.kri.report.ControllerPanelSummaryQualificationEvent",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.kri.grids.StoreGridPanelRegisterQualificationEventOne",
      "risk.parameter.grids.StoreGridPanelRegisterEventOne"
    ],
    models: [
      "risk.kri.grids.ModelGridPanelRegisterQualificationEventOne",
      "risk.parameter.grids.ModelGridPanelRegisterEventOne"
    ],
    views: [
      "risk.parameter.combos.ViewComboYear",
      "risk.parameter.combos.ViewComboMonth",
      "risk.parameter.combos.ViewComboEventOne",
      "risk.kri.grids.ViewGridPanelShortQualificationEventOne",
      "risk.parameter.grids.ViewGridPanelShortEventOne"

      //        stores:['risk.parameter.grids.StoreGridPanelRegisterEventOne'],
      //    models:['risk.parameter.grids.ModelGridPanelRegisterEventOne'],
      //    views:[ 'risk.parameter.grids.ViewGridPanelRegisterEventOne'
      //    ,'risk.parameter.factorsevents.ViewPanelRegisterEventOne'
      //],
    ],
    init: function() {
      this.control({
        "[action=reSearchMasterEventOneQualification]": {
          click: this._onReSearchMasterEventOneQualification
        },
        "[action=selectEventOne]": {
          click: this._onSelectComboxEventOne
        }
      });
    },
    _onSelectComboxEventOne: function(cbo, record) {
      var panel = Ext.ComponentQuery.query(
        "ViewPanelSummaryQualificationEvent"
      )[0];
      var dateStart = panel.down("datefield[name=dateStart]").getRawValue();
      var dateLimit = panel.down("datefield[name=dateLimit]").getRawValue();
      if (Ext.isEmpty(dateStart) || Ext.isEmpty(dateLimit)) {
      } else {
        var chart = panel.down("chart");
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/buildJsoScoreEventByRangeDate.htm",
          params: {
            idEventOne: cbo.getValue(),
            dateStart: dateStart,
            dateLimit: dateLimit
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              var color = JSON.parse(response.color);
              chart.themeAttrs.colors = [
                "#" + color[0].color1 + "",
                "#" + color[0].color2 + "",
                "#" + color[0].color3 + "",
                "#" + color[0].color4 + "",
                "#" + color[0].color5 + "",
                "#" + color[0].color6 + "",
                "#" + color[0].color7 + "",
                "#" + color[0].color8 + "",
                "#" + color[0].color9 + "",
                "#" + color[0].color10 + ""
              ];
              chart.getStore().loadData(JSON.parse(response.data));
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      }
    },
    _onReSearchMasterEventOneQualification: function(btn) {
      var panel = Ext.ComponentQuery.query(
        "ViewPanelSummaryQualificationEvent"
      )[0];
      var dateStart = panel.down("datefield[name=dateStart]").getRawValue();
      var dateLimit = panel.down("datefield[name=dateLimit]").getRawValue();

      if (Ext.isEmpty(dateStart) || Ext.isEmpty(dateLimit)) {
      } else {
        var chart = panel.down("chart[itemId=chartSummary]");
        var chartAverage = panel.down("chart[itemId=chartAverage]");
        var grid = panel.down("gridpanel[itemId=gridEvolutionRisk]");
        var store = grid.getStore();

        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/buildJsoScoreEventByRangeDate.htm",
          params: {
            dateStart: dateStart,
            dateLimit: dateLimit
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              var color = JSON.parse(response.color);
              chart.themeAttrs.colors = [
                "#" + color[0].color1 + "",
                "#" + color[0].color2 + "",
                "#" + color[0].color3 + "",
                "#" + color[0].color4 + "",
                "#" + color[0].color5 + "",
                "#" + color[0].color6 + "",
                "#" + color[0].color7 + "",
                "#" + color[0].color8 + "",
                "#" + color[0].color9 + "",
                "#" + color[0].color10 + ""
              ];
              chart.getStore().loadData(JSON.parse(response.data));
              //                        var colorAverage = new Array("#3366FF", "#99CC00", "#FFFF00", "#FF6600", "#FF0000");
              chartAverage.themeAttrs.colors = [
                "#" + color[0].color1 + "",
                "#" + color[0].color2 + "",
                "#" + color[0].color3 + "",
                "#" + color[0].color4 + "",
                "#" + color[0].color5 + "",
                "#" + color[0].color6 + "",
                "#" + color[0].color7 + "",
                "#" + color[0].color8 + "",
                "#" + color[0].color9 + "",
                "#" + color[0].color10 + ""
              ];
              chartAverage
                .getStore()
                .loadData(JSON.parse(response.dataAverage));
              var fieldsParse = JSON.parse(response.fieldEventOne);
              var columnsParse = JSON.parse(response.columnEventOne);
              var dataParse = JSON.parse(response.dataEventOne);

              store.model.setFields(fieldsParse);
              var col = "c";
              for (var i = 0; i < columnsParse.length; i++) {
                columnsParse[i]["renderer"] = function(
                  value,
                  metaData,
                  record,
                  rowIndex,
                  columnIndex,
                  store,
                  view
                ) {
                  if (columnIndex === 0) {
                    if (rowIndex >= dataParse.length - 2) {
                      metaData.tdAttr =
                        'style="background-color:  #C9C9CD !important;height:50px; color: #4F85C6;font-weight: bold; text-align:center;"';
                    } else {
                      metaData.tdAttr =
                        'style="background-color: #EEEEEF !important;height:50px;"';
                    }
                  } else if (rowIndex >= dataParse.length - 2) {
                    metaData.tdAttr =
                      'style="background-color: #' +
                      record.get(col + columnIndex) +
                      '!important;height:50px;"';
                  } else {
                    metaData.tdAttr =
                      'style="height:50px; background-color:  #FFFFFF !important; font-weight: bold; color: #' +
                      record.get(col + columnIndex) +
                      '; "';
                  }
                  return "<span >" + value + "</span>";
                };
              }
              grid.reconfigure(store, columnsParse);
              store.removeAll();
              store.loadRawData(dataParse, true);
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      }
    }
  }
);
