Ext.define(
  "DukeSource.controller.risk.kri.report.ControllerPanelDetailReportKri",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: ["risk.kri.combos.ViewComboDesignReportKri"],
    init: function() {
      this.control({
        "[action=searchDetailReportKri]": {
          click: this._onSearchDetailReportKri
        },
        "[action=searchResumeDetailKri]": {
          click: this._onSearchResumeDetailKri
        },
        "[action=designReportDetailKri]": {
          click: this._onDesignReportDetailKri
        },
        "[action=loadDataChartKri]": {
          click: this._onLoadDataChartKri
        },
        "[action=saveChartReportDetailKri]": {
          click: this._onSaveChartReportDetailKri
        }
      });
    },
    _onSearchDetailReportKri: function() {
      var panel = Ext.ComponentQuery.query("ViewPanelDetailReportKri")[0];
      var grid = panel.down("#gridReportDetail");
      var chart = panel.down("#chartDetailKri");
      var store = grid.getStore();
      if (
        Ext.ComponentQuery.query("ViewWindowSearchMasterKri")[0] == undefined
      ) {
        var searchKri = Ext.create(
          "DukeSource.view.risk.kri.windows.ViewWindowSearchMasterKri",
          { modal: true }
        );
        searchKri.down("datefield[name=dateStart]").setDisabled(false);
        searchKri.down("datefield[name=dateLimit]").setDisabled(false);
        searchKri.down("datefield[name=dateStart]").setVisible(true);
        searchKri.down("datefield[name=dateLimit]").setVisible(true);
        searchKri.show();
        searchKri.down("grid").on("itemdblclick", function(view, record) {
          if (
            searchKri.down("datefield[name=dateStart]").getRawValue() == "" ||
            searchKri.down("datefield[name=dateLimit]").getRawValue() == ""
          ) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              "INGRESE LA FECHA INICIAL Y FINAL",
              Ext.Msg.WARNING
            );
          } else {
            store.removeAll();
            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/buildJsonEvaluationKriByIdRangeYearMonth.htm",
              params: {
                idKeyRiskIndicator: record.get("idKeyRiskIndicator"),
                dateInitial: searchKri
                  .down("datefield[name=dateStart]")
                  .getRawValue(),
                dateFinal: searchKri
                  .down("datefield[name=dateLimit]")
                  .getRawValue()
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  var dateStart = searchKri
                    .down("datefield[name=dateStart]")
                    .getRawValue();
                  var dateLimit = searchKri
                    .down("datefield[name=dateLimit]")
                    .getRawValue();
                  panel
                    .down("textfield[name=idKeyRiskIndicator]")
                    .setValue(record.get("idKeyRiskIndicator"));
                  panel
                    .down("UpperCaseTextFieldReadOnly[name=codeKri]")
                    .setValue(record.get("codeKri"));
                  panel
                    .down("UpperCaseTextFieldReadOnly[name=nameKri]")
                    .setValue(record.get("nameKri"));
                  panel
                    .down("UpperCaseTextFieldReadOnly[name=abbreviation]")
                    .setValue(record.get("abbreviation"));
                  panel.down("datefield[name=dateStart]").setValue(dateStart);
                  panel.down("datefield[name=dateLimit]").setValue(dateLimit);
                  var fields = Ext.JSON.decode(response.fields);
                  var columns = Ext.JSON.decode(response.columns);
                  var data = Ext.JSON.decode(response.data);

                  store.model.setFields(fields);
                  grid.reconfigure(store, columns);
                  store.removeAll();
                  store.loadRawData(data, true);
                  chart.removeAll();
                  //                            chart.getStore().loadData(data);
                  searchKri.close();
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
          }
        });
      }
    },
    _onSearchResumeDetailKri: function() {
      var panel = Ext.ComponentQuery.query("ViewPanelDetailReportKri")[0];
      var grid = panel.down("#gridReportDetail");
      var store = grid.getStore();
      if (
        panel.down("datefield[name=dateStart]").getRawValue() == "" ||
        panel.down("datefield[name=dateLimit]").getRawValue() == ""
      ) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          "SELECCIONE UN KRI E INGRESE LA FECHA INICIAL Y FINAL",
          Ext.Msg.WARNING
        );
      } else {
        store.removeAll();
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/buildJsonEvaluationKriByIdRangeYearMonth.htm",
          params: {
            idKeyRiskIndicator: panel
              .down("textfield[name=idKeyRiskIndicator]")
              .getValue(),
            dateInitial: panel.down("datefield[name=dateStart]").getRawValue(),
            dateFinal: panel.down("datefield[name=dateLimit]").getRawValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              var fields = Ext.JSON.decode(response.fields);
              var columns = Ext.JSON.decode(response.columns);
              var data = Ext.JSON.decode(response.data);

              store.model.setFields(fields);
              grid.reconfigure(store, columns);
              store.removeAll();
              store.loadRawData(data, true);
              chart.removeAll();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      }
    },
    _onDesignReportDetailKri: function() {
      var panel = Ext.ComponentQuery.query("ViewPanelDetailReportKri")[0];
      var chart = panel.down("#chartDetailKri");
      var formWin = Ext.ComponentQuery.query("ViewWindowDesignReport")[0];
      var headers = formWin.down("form").getValues()["columnReportKri"];
      var values = formWin
        .down("form")
        .down("#columnReportKri")
        .getRawValue(); //Ext.ComponentQuery.query('ViewWindowDesignReport')[0].down('form').down('#columnReportKri').getRawValue().split("|")
      var array = values.split("|");
      if (formWin.down("#typeChart").getValue() == "1") {
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/buildJsonEvaluationKriForGraphicColumn.htm",
          params: {
            idKeyRiskIndicator: panel
              .down("textfield[name=idKeyRiskIndicator]")
              .getValue(),
            dateInitial: panel.down("datefield[name=dateStart]").getRawValue(),
            dateFinal: panel.down("datefield[name=dateLimit]").getRawValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              var columns = Ext.JSON.decode(response.columns);
              Ext.define("chartDetailReportKri", {
                extend: "Ext.data.Model",
                fields: columns
              });
              chart.removeAll();
              chart.add({
                xtype: "chart",
                itemId: "chartForKri",
                store: Ext.create("Ext.data.Store", {
                  model: "chartDetailReportKri",
                  autoLoad: true,
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url: "http://localhost:9000/giro/loadGridDefault.htm",
                    reader: {
                      totalProperty: "totalCount",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                }),
                region: "east",
                animate: true,
                shadow: true,
                insetPadding: 20,
                legend: {
                  visible: true,
                  position: "right",
                  labelFont: "8px Arial"
                },
                axes: [
                  {
                    type: "Numeric",
                    position: "left",
                    fields: headers,
                    roundToDecimal: false,
                    label: {
                      renderer: function(v) {
                        return String(v);
                      }
                    },
                    grid: true
                  },
                  {
                    type: "Category",
                    position: "bottom",
                    fields: ["dateReport"]
                  }
                ],
                series: [
                  {
                    type: "column",
                    label: {
                      display: "insideEnd",
                      "text-anchor": "middle",
                      field: headers,
                      renderer: Ext.util.Format.numberRenderer("0"),
                      orientation: "horizontal",
                      //                                        color: '#333',
                      font: "bold 12px Arial"
                    },
                    axis: "bottom",
                    title: array,
                    gutter: 80,
                    xField: "dateReport",
                    yField: headers,
                    stacked: true,
                    style: {
                      opacity: 0.93
                    }
                  }
                ]
              });
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else if (formWin.down("#typeChart").getValue() == "2") {
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/buildJsonEvaluationKriForGraphicColumn.htm",
          params: {
            idKeyRiskIndicator: panel
              .down("textfield[name=idKeyRiskIndicator]")
              .getValue(),
            dateInitial: panel.down("datefield[name=dateStart]").getRawValue(),
            dateFinal: panel.down("datefield[name=dateLimit]").getRawValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              var columns = Ext.JSON.decode(response.columns);
              Ext.define("chartDetailReportKri", {
                extend: "Ext.data.Model",
                fields: columns
              });
              chart.removeAll();
              chart.add({
                xtype: "chart",
                store: Ext.create("Ext.data.Store", {
                  model: "chartDetailReportKri",
                  autoLoad: true,
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url: "http://localhost:9000/giro/loadGridDefault.htm",
                    reader: {
                      totalProperty: "totalCount",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                }),
                region: "east",
                animate: true,
                shadow: true,
                insetPadding: 20,
                legend: {
                  visible: true,
                  position: "right",
                  labelFont: "8px Arial"
                },
                axes: [
                  {
                    type: "Numeric",
                    position: "left",
                    fields: headers,
                    //                                        title: 'RATIO',
                    roundToDecimal: false,
                    label: {
                      renderer: function(v) {
                        return String(v);
                      }
                    },
                    grid: true
                  },
                  {
                    type: "Category",
                    position: "bottom",
                    fields: ["dateReport"]
                    //                                        ,
                    //                                        title: 'TIEMPO'
                  }
                ],
                series: [
                  {
                    type: "column",
                    label: {
                      display: "outside",
                      "text-anchor": "middle",
                      field: headers,
                      renderer: Ext.util.Format.numberRenderer("0"),
                      orientation: "horizontal",
                      //                                            color: '#333'
                      font: "bold 12px Arial"
                    },
                    axis: "bottom",
                    gutter: 80,
                    xField: "dateReport",
                    yField: headers,
                    title: array,
                    style: {
                      opacity: 0.93
                    }
                  }
                ]
              });
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/buildJsonEvaluationKriForGraphicColumn.htm",
          params: {
            idKeyRiskIndicator: panel
              .down("textfield[name=idKeyRiskIndicator]")
              .getValue(),
            dateInitial: panel.down("datefield[name=dateStart]").getRawValue(),
            dateFinal: panel.down("datefield[name=dateLimit]").getRawValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              var columns = Ext.JSON.decode(response.columns);
              Ext.define("chartDetailReportKri", {
                extend: "Ext.data.Model",
                fields: columns
              });
              chart.removeAll();
              chart.add({
                xtype: "chart",
                store: Ext.create("Ext.data.Store", {
                  model: "chartDetailReportKri",
                  autoLoad: true,
                  proxy: {
                    actionMethods: {
                      create: "POST",
                      read: "POST",
                      update: "POST"
                    },
                    type: "ajax",
                    url: "http://localhost:9000/giro/loadGridDefault.htm",
                    reader: {
                      totalProperty: "totalCount",
                      root: "data",
                      successProperty: "success"
                    }
                  }
                }),
                region: "east",
                animate: true,
                shadow: true,
                insetPadding: 20,
                legend: {
                  visible: true,
                  position: "right",
                  labelFont: "8px Arial"
                },
                axes: [
                  {
                    type: "Numeric",
                    position: "left",
                    fields: headers,
                    //                                    title: 'RATIO',
                    roundToDecimal: false,
                    label: {
                      renderer: function(v) {
                        return String(v);
                      }
                    },
                    grid: true
                  },
                  {
                    type: "Numeric",
                    position: "right",
                    fields: formWin.down("form").getValues()["valueY2"],
                    minimum: 0
                  },
                  {
                    type: "Category",
                    position: "bottom",
                    fields: ["dateReport"]
                    //                                    ,
                    //                                label: {
                    //                                    rotate: {
                    //                                        degrees: 270
                    //                                    }
                    //                                },
                    //                                    title: 'TIEMPO'
                  }
                ],
                series: [
                  {
                    type: "column",
                    label: {
                      display: "outside",
                      "text-anchor": "middle",
                      field: headers,
                      renderer: Ext.util.Format.numberRenderer("0"),
                      orientation: "horizontal",
                      //                                        color: '#333'
                      font: "bold 12px Arial"
                    },
                    axis: "bottom",
                    gutter: 80,
                    title: array,
                    xField: "dateReport",
                    yField: headers,
                    style: {
                      opacity: 0.93
                    }
                  },
                  {
                    type: "line",
                    axis: "right",
                    smooth: true,
                    highlight: {
                      size: 7,
                      radius: 7
                    },
                    label: {
                      display: "rotate",
                      font: "bold 12px Arial",
                      field: formWin.down("form").getValues()["lineReportKri"],
                      renderer: function(val) {
                        return val;
                      }
                    },
                    style: {
                      stroke: "#ff0000",
                      fill: "#ff0000"
                    },
                    title: [formWin.down("#lineReportKri").getRawValue()],
                    xField: "dateReport",
                    yField: formWin.down("form").getValues()["lineReportKri"],
                    markerConfig: {
                      type: "circle",
                      size: 4,
                      radius: 4,
                      "stroke-width": 0
                    }
                  }
                ]
              });
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      }
    },
    _onLoadDataChartKri: function() {
      var panel = Ext.ComponentQuery.query("ViewPanelDetailReportKri")[0];
      var chart = panel.down("chart");
      if (
        panel.down("UpperCaseTextFieldReadOnly[name=nameKri]").getValue() == ""
      ) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          "SELECCIONE UN KRI",
          Ext.Msg.WARNING
        );
      } else {
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/buildJsonEvaluationKriForGraphicData.htm",
          params: {
            idKeyRiskIndicator: panel
              .down("textfield[name=idKeyRiskIndicator]")
              .getValue(),
            dateInitial: panel.down("datefield[name=dateStart]").getRawValue(),
            dateFinal: panel.down("datefield[name=dateLimit]").getRawValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              var data = Ext.JSON.decode(response.data);
              chart.getStore().loadData(data);
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      }
    },
    _onSaveChartReportDetailKri: function() {
      var panel = Ext.ComponentQuery.query("ViewPanelDetailReportKri")[0];
      var chart = panel.down("chart");
      if (
        panel.down("UpperCaseTextFieldReadOnly[name=nameKri]").getValue() == ""
      ) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          "SELECCIONE UN KRI",
          Ext.Msg.WARNING
        );
      } else {
        Ext.MessageBox.confirm(
          DukeSource.global.GiroMessages.TITLE_CONFIRM,
          "Desea GUARDAR el reporte en imagen?",
          function(choice) {
            if (choice == "yes") {
              chart.save({
                type: "image/png"
              });
            }
          }
        );
      }
    }
  }
);
