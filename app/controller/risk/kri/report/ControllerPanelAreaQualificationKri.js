Ext.define(
  "DukeSource.controller.risk.kri.report.ControllerPanelAreaQualificationKri",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboYear",
      "risk.parameter.combos.ViewComboProcessType",
      "risk.parameter.combos.ViewComboProcess",
      "risk.util.search.AdvancedSearchRisk"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridAreaQualification]": {
          keyup: this._onSearchTriggerGridAreaQualification
        }
      });
    },
    _onSearchTriggerGridAreaQualification: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelAreaQualificationKri grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    }
  }
);
