Ext.define(
  "DukeSource.controller.risk.kri.ControllerPanelRegisterEvaluationKri",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboMonth",
      "risk.parameter.combos.ViewComboYear",
      "risk.parameter.combos.ViewComboYearKri",
      "risk.parameter.combos.ViewComboMonthKri"
    ],
    init: function() {
      this.control({
        "[action=saveEvaluationKri]": {
          click: this._onSaveEvaluationKri
        },
        "[action=entryEvaluationKri]": {
          click: this._onEntryEvaluationKri
        },
        "[action=viewDetailEvaluationKri]": {
          click: this._onViewDetailEvaluationKri
        },
        "[action=addTraceEvaluationKri]": {
          click: this._onAddTraceEvaluationKri
        },
        "[action=deleteEvaluationKri]": {
          click: this._onDeleteEvaluationKri
        },
        "[action=consultEvaluationKri]": {
          select: this._onConsultEvaluationKri
        },
        "[action=downloadFormatEvaluation]": {
          click: this._onDownloadFormatEvaluation
        },
        "[action=downloadKriParameter]": {
          click: this._onDownloadKriParameter
        }
      });
    },

    _onSaveEvaluationKri: function(button) {
      var panel = button.up("window");
      var form = panel.down("form").getForm();

      /*var grid = panel.down('grid');
        var arrayDataToSave = [];
        var records = grid.getStore().getRange();
        Ext.Array.each(records, function (record, index, countriesItSelf) {
            arrayDataToSave.push(record.data);
        });*/

      if (form.isValid()) {
        form.submit({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveKriByYearMonth.htm?nameView=ViewPanelConfigMasterKri",
          timeout: 120000,
          waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
          success: function(form, action) {
            var response = Ext.decode(action.response.responseText);
            if (response.success) {
              var panel = Ext.ComponentQuery.query(
                "ViewPanelRegisterEvaluationKri"
              )[0];
              var grid = panel.down("grid");
              grid.down("pagingtoolbar").moveFirst();
              DukeSource.global.DirtyView.messageNormal(response.message);
            } else {
              DukeSource.global.DirtyView.messageWarning(response.message);
            }
          },
          failure: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (!valor.success) {
              DukeSource.global.DirtyView.messageWarning(valor.message);
            }
          }
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
      }
    },
    _onEntryEvaluationKri: function() {
      var panel = Ext.ComponentQuery.query("ViewPanelRegisterEvaluationKri")[0];
      var grid = panel.down("grid");
      var row = grid.getSelectionModel().getSelection()[0];
      var rowMaster = panel.rowMaster;

      DukeSource.lib.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/getTemplateJsonDefault.htm",
        params: {
          idKeyRiskIndicator: rowMaster.get("idKeyRiskIndicator"),
          indicatorNormalizer: rowMaster.get("indicatorNormalizer"),
          indicatorKri: rowMaster.get("indicatorKri")
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            var win = Ext.create(
              "DukeSource.view.risk.kri.windows.WindowRegisterEvaluationKri",
              {
                modal: true
              }
            );
            win
              .down("form")
              .getForm()
              .loadRecord(rowMaster);
            win.down("#year").setValue(response.year);
            win.down("#month").setValue(response.month);
            win.show();
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },
    _onViewDetailEvaluationKri: function(btn) {
      var win = Ext.create(
        "DukeSource.view.risk.kri.windows.WindowDetailEvaluationKri",
        {
          modal: true
        }
      );
      var panel = Ext.ComponentQuery.query("ViewPanelRegisterEvaluationKri")[0];
      var grid = panel.down("grid");
      var row = grid.getSelectionModel().getSelection()[0];

      if (row === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        DukeSource.lib.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/buildJsonByIdKriAndYearMonth.htm",
          params: {
            idKeyRiskIndicator: row.get("idKeyRiskIndicator"),
            indicatorNormalizer: row.get("indicatorNormalizer"),
            indicatorKri: row.get("indicatorKri"),
            year: row.get("year"),
            month: row.get("month")
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              Ext.define("ModelGriEvalKri", {
                extend: "Ext.data.Model",
                fields: JSON.parse(response.fields)
              });

              var storeDetailEval = Ext.create("Ext.data.Store", {
                model: "ModelGriEvalKri",
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                proxy: {
                  actionMethods: {
                    create: "POST",
                    read: "POST",
                    update: "POST"
                  },
                  type: "ajax",
                  url:
                    "http://localhost:9000/giro/buildJsonByIdKriAndYearMonth.htm",
                  reader: {
                    type: "json",
                    root: "data",
                    successProperty: "success",
                    totalProperty: "totalCount"
                  }
                }
              });
              storeDetailEval.loadData(JSON.parse(response.data));
              storeDetailEval.totalCount = response.totalCount;

              var pagingBar = Ext.create("Ext.toolbar.Paging", {
                pageSize: DukeSource.global.GiroConstants.ITEMS_PAGE,
                store: storeDetailEval,
                displayInfo: true,
                displayMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_PAGE,
                emptyMsg: DukeSource.global.GiroMessages.MESSAGE_GRID_NUMBER
              });

              pagingBar.onLoad();
              var grid = Ext.create("Ext.grid.Panel", {
                id: "gridDetailEvaluationKri",
                store: storeDetailEval,
                loadMask: true,
                columnLines: true,
                region: "center",
                flex: 20,
                multiSelect: true,
                forceFit: true,
                columns: JSON.parse(response.columns),
                stripeRows: true,
                bbar: pagingBar
              });

              win.add(grid);
              win.show();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      }
    },
    _onConsultEvaluationKri: function() {
      var panel = Ext.ComponentQuery.query("ViewPanelRegisterEvaluationKri")[0];
      var grid = panel.down("grid");
      var store = grid.getStore();
      DukeSource.lib.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/buildJsonByIdKriAndYearMonth.htm",
        params: {
          idKeyRiskIndicator: panel.down("#idKri").getValue(),
          indicatorNormalizer: panel.down("#indicatorNormalizer").getValue(),
          indicatorKri: panel.down("#indicatorKri").getValue(),
          year: panel.down("ViewComboYearKri").getValue(),
          month: panel.down("ViewComboMonthKri").getValue()
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            var fields = JSON.parse(response.fields);
            var columns = JSON.parse(response.columns);
            var data = JSON.parse(response.data);
            store.model.setFields(fields);
            grid.reconfigure(store, columns);
            store.removeAll();
            store.loadRawData(data, true);
            var evaluation = JSON.parse(response.evaluation);
            // panel.down('#timeKri').setRawValue(evaluation.timeKri);

            panel
              .down("#resultQualification")
              .setValue(evaluation.evaluationNotNormalizer);
            DukeSource.global.DirtyView.colorToElement(
              panel.down("#resultQualification"),
              evaluation.levelColourQualificationEachKri
            );
            panel
              .down("#descriptionQualification")
              .setValue(evaluation.descriptionQualificationEachKri);
            DukeSource.global.DirtyView.colorToElement(
              panel.down("#descriptionQualification"),
              evaluation.levelColourQualificationEachKri
            );
            if (
              panel.down("textfield[name=indicatorNormalizer]").getValue() ==
              "N"
            ) {
              DukeSource.global.DirtyView.toReadOnly(panel.down("#resultNormalized"));
              panel.down("#resultNormalized").reset();
              DukeSource.global.DirtyView.toReadOnly(panel.down("#descriptionResult"));
              panel.down("#descriptionResult").reset();
            } else if (
              panel.down("textfield[name=indicatorNormalizer]").getValue() ==
              "S"
            ) {
              panel
                .down("#resultNormalized")
                .setValue(evaluation.evaluationResult);
              DukeSource.global.DirtyView.colorToElement(
                panel.down("#resultNormalized"),
                evaluation.levelColour
              );
              panel
                .down("#descriptionResult")
                .setValue(evaluation.descriptionQualificationKri);
              DukeSource.global.DirtyView.colorToElement(
                panel.down("#descriptionResult"),
                evaluation.levelColour
              );
            }
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },
    _onDeleteEvaluationKri: function(btn) {
      var panel = Ext.ComponentQuery.query("ViewPanelRegisterEvaluationKri")[0];
      var year = panel.down("#year");
      var frequency = panel.down("#abbreviationFrequency");
      var month = panel.down("#month");
      var grid = panel.down("grid");
      var row = grid.getSelectionModel().getSelection()[0];

      if (row === undefined) {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
      } else {
        Ext.MessageBox.show({
          title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
          msg:
            "Desea eliminar la evaluación del " +
            year.fieldLabel +
            ": " +
            year.getRawValue() +
            " y " +
            frequency.getRawValue() +
            ": " +
            month.getRawValue() +
            "?",
          icon: Ext.Msg.WARNING,
          buttonText: {
            yes: "Si"
          },
          buttons: Ext.MessageBox.YESNO,
          fn: function(btn) {
            if (btn === "yes") {
              DukeSource.lib.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/deleteDataEvaluationKri.htm?nameView=ViewPanelConfigMasterKri",
                params: {
                  idKeyRiskIndicator: row.get("idKeyRiskIndicator"),
                  indicatorNormalizer: row.get("indicatorNormalizer"),
                  indicatorKri: row.get("indicatorKri"),
                  year: row.get("year"),
                  month: row.get("month")
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    DukeSource.global.DirtyView.messageNormal(response.message);
                    grid.down("pagingtoolbar").doRefresh();

                    panel.down("#containerResult").removeAll();
                  } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                  }
                },
                failure: function(response) {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              });
            }
          }
        });
      }
    },
    _onAddTraceEvaluationKri: function() {
      var panel = Ext.ComponentQuery.query("ViewPanelRegisterEvaluationKri")[0];
      var grid = panel.down("grid");
      var row = grid.getSelectionModel().getSelection()[0];

      if (row === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        var view = Ext.create(
          "DukeSource.view.risk.kri.windows.WindowTraceEvaluationKri",
          {
            modal: true,
            idKri: row.get("idKeyRiskIndicator"),
            yearMonth: row.get("yearMonth")
          }
        );
        view
          .down("grid")
          .setTitle(
            view.title +
              " ---> " +
              panel.down("#codeKri").getValue() +
              " ---> " +
              row.get("description")
          );

        view.down("#gridTrackEvaluationKri").store.getProxy().extraParams = {
          idKri: row.get("idKeyRiskIndicator"),
          yearMonth: row.get("yearMonth")
        };
        view.down("#gridTrackEvaluationKri").store.getProxy().url =
          "http://localhost:9000/giro/showTracingEvaluationKri.htm";
        view
          .down("#gridTrackEvaluationKri")
          .down("pagingtoolbar")
          .moveFirst();

        view.show();
      }
    },

    _onDownloadFormatEvaluation: function() {
      var panel = Ext.ComponentQuery.query("ViewPanelRegisterEvaluationKri")[0];
      var grid = panel.down("grid");
      var row = grid.getSelectionModel().getSelection()[0];

      if (row === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_KRI_NOT_FIND,
          Ext.Msg.WARNING
        );
      } else {
        Ext.core.DomHelper.append(document.body, {
          tag: "iframe",
          id: "downloadIframe",
          frameBorder: 0,
          width: 0,
          height: 0,
          css: "display:none;visibility:hidden;height:0px;",
          src:
            "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
            row.get("idKeyRiskIndicator") +
            "&names=idKri" +
            "&types=Integer" +
            "&nameReport=KRI0001XLS"
        });
      }
    },
    _onDownloadKriParameter: function() {
      Ext.core.DomHelper.append(document.body, {
        tag: "iframe",
        id: "downloadIframe",
        frameBorder: 0,
        width: 0,
        height: 0,
        css: "display:none;visibility:hidden;height:0px;",
        src:
          "http://localhost:9000/giro/xlsGeneralReport.htm?values=&names=&types=&nameReport=KRI0002XLS"
      });
    }
  }
);

function refreshEvaluationKri(
  record,
  yearWindow,
  monthWindow,
  panel,
  store,
  grid
) {
  DukeSource.lib.Ajax.request({
    method: "POST",
    url: "http://localhost:9000/giro/buildJsonLastDataInsert.htm",
    params: {
      idKeyRiskIndicator: record.get("idKeyRiskIndicator"),
      year: yearWindow,
      month: monthWindow,
      indicatorNormalizer: record.get("indicatorNormalizer"),
      indicatorKri: record.get("indicatorKri")
    },
    success: function(response) {
      response = Ext.decode(response.responseText);
      if (response.success) {
        panel.down("#btnSaveEvaluation").setVisible(true);
        panel.down("#idKri").setValue(record.get("idKeyRiskIndicator"));
        panel.down("#codeKri").setValue(record.get("codeKri"));
        panel.down("#nameKri").setValue(record.get("nameKri"));
        panel.down("#abbreviation").setValue(record.get("abbreviation"));
        panel
          .down("#indicatorNormalizer")
          .setValue(record.get("indicatorNormalizer"));
        panel.down("#indicatorKri").setValue(record.get("indicatorKri"));
        var evaluation = JSON.parse(response.evaluation);
        panel
          .down("#resultQualification")
          .setValue(evaluation.evaluationNotNormalizer);
        DukeSource.global.DirtyView.colorToElement(
          panel.down("#resultQualification"),
          evaluation.levelColourQualificationEachKri
        );
        panel
          .down("#descriptionQualification")
          .setValue(evaluation.descriptionQualificationEachKri);
        DukeSource.global.DirtyView.colorToElement(
          panel.down("#descriptionQualification"),
          evaluation.levelColourQualificationEachKri
        );
        if (record.get("indicatorNormalizer") == "N") {
          DukeSource.global.DirtyView.toReadOnly(panel.down("#resultNormalized"));
          panel.down("#resultNormalized").reset();
          DukeSource.global.DirtyView.toReadOnly(panel.down("#descriptionResult"));
          panel.down("#descriptionResult").reset();
        } else if (record.get("indicatorNormalizer") == "S") {
          panel.down("#resultNormalized").setValue(evaluation.evaluationResult);
          DukeSource.global.DirtyView.colorToElement(
            panel.down("#resultNormalized"),
            evaluation.levelColourQualificationEachKri
          );
          panel
            .down("#descriptionResult")
            .setValue(evaluation.descriptionQualificationKri);
          DukeSource.global.DirtyView.colorToElement(
            panel.down("#descriptionResult"),
            evaluation.levelColourQualificationEachKri
          );
        }

        var comboYearKri = panel.down("ViewComboYearKri");
        var comboMonthKri = panel.down("ViewComboMonthKri");
        comboYearKri.getStore().removeAll();
        comboYearKri.getStore().loadRawData(JSON.parse(response.year), true);
        comboYearKri.setValue(comboYearKri.getStore().first().data.keyInt);
        comboMonthKri.getStore().removeAll();
        comboMonthKri.getStore().loadRawData(JSON.parse(response.month), true);
        comboMonthKri.setValue(comboMonthKri.getStore().last().data.keyInt);
        comboYearKri.setReadOnly(false);
        comboMonthKri.setReadOnly(false);
        var fields = JSON.parse(response.fields);
        var columns = JSON.parse(response.columns);
        var data = JSON.parse(response.data);
        store.model.setFields(fields);
        grid.reconfigure(store, columns);
        store.removeAll();
        store.loadRawData(data, true);
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_ERROR,
          response.mensaje,
          Ext.Msg.ERROR
        );
      }
    },
    failure: function() {}
  });
}
