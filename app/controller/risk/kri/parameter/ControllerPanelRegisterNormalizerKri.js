Ext.define(
  "DukeSource.controller.risk.kri.parameter.ControllerPanelRegisterNormalizerKri",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.kri.grids.StoreGridPanelRegisterNormalizerKri"],
    models: ["risk.kri.grids.ModelGridPanelRegisterNormalizerKri"],
    views: [
      "risk.kri.grids.ViewGridPanelRegisterNormalizerKri",
      "risk.kri.parameter.ViewPanelRegisterNormalizerKri"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridNormalizerKri]": {
          keyup: this._onSearchTriggerGridNormalizerKri
        },
        "[action=exportNormalizerKriPdf]": {
          click: this._onExportNormalizerKriPdf
        },
        "[action=exportNormalizerKriExcel]": {
          click: this._onExportNormalizerKriExcel
        },
        "[action=normalizerKriAuditory]": {
          click: this._onNormalizerKriAuditory
        },
        "[action=newNormalizerKri]": {
          click: this._newNormalizerKri
        },
        "[action=deleteNormalizerKri]": {
          click: this._onDeleteNormalizerKri
        },
        "[action=searchNormalizerKri]": {
          specialkey: this._searchNormalizerKri
        }
      });
    },
    _newNormalizerKri: function() {
      var modelNormalizerKri = Ext.create(
        "DukeSource.model.risk.kri.grids.ModelGridPanelRegisterNormalizerKri",
        {
          idNormalizerKri: "id",
          criteriaEvaluationKri: "",
          keyRiskIndicator: "",
          description: "",
          weighing: "",
          normalizerKri: "",
          bestValue: "",
          worstValue: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterNormalizerKri"
      )[0];
      var panel = general.down("ViewGridPanelRegisterNormalizerKri");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelNormalizerKri);
      editor.startEdit(0, 0);
    },
    _onDeleteNormalizerKri: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterNormalizerKri grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteNormalizerKri.htm?nameView=ViewPanelRegisterNormalizerKri"
      );
    },
    _searchNormalizerKri: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterNormalizerKri grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findNormalizerKri.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridNormalizerKri: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterNormalizerKri grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportNormalizerKriPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportNormalizerKriExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onNormalizerKriAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterNormalizerKri grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditNormalizerKri.htm"
      );
    }
  }
);
