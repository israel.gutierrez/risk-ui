Ext.define(
  "DukeSource.controller.risk.kri.parameter.ControllerPanelRegisterWeighingKri",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.kri.grids.StoreGridPanelRegisterWeighingKri"],
    models: ["risk.kri.grids.ModelGridPanelRegisterWeighingKri"],
    views: [
      "risk.kri.grids.ViewGridPanelRegisterWeighingKri",
      "risk.kri.parameter.ViewPanelRegisterWeighingKri"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridWeighingKri]": {
          keyup: this._onSearchTriggerGridWeighingKri
        },
        "[action=exportWeighingKriPdf]": {
          click: this._onExportWeighingKriPdf
        },
        "[action=exportWeighingKriExcel]": {
          click: this._onExportWeighingKriExcel
        },
        "[action=weighingKriAuditory]": {
          click: this._onWeighingKriAuditory
        },
        "[action=newWeighingKri]": {
          click: this._newWeighingKri
        },
        "[action=deleteWeighingKri]": {
          click: this._onDeleteWeighingKri
        },
        "[action=searchWeighingKri]": {
          specialkey: this._searchWeighingKri
        }
      });
    },
    _newWeighingKri: function() {
      var modelWeighingKri = Ext.create(
        "DukeSource.model.risk.kri.grids.ModelGridPanelRegisterWeighingKri",
        {
          idWeighingKri: "id",
          description: "",
          weighing: "",
          unitMeasure: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterWeighingKri")[0];
      var panel = general.down("ViewGridPanelRegisterWeighingKri");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelWeighingKri);
      editor.startEdit(0, 0);
    },
    _onDeleteWeighingKri: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterWeighingKri grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteWeighingKri.htm?nameView=ViewPanelRegisterWeighingKri"
      );
    },
    _searchWeighingKri: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterWeighingKri grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findWeighingKri.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridWeighingKri: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterWeighingKri grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportWeighingKriPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportWeighingKriExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onWeighingKriAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterWeighingKri grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditWeighingKri.htm"
      );
    }
  }
);
