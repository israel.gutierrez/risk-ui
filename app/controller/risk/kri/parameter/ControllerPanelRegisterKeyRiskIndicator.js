Ext.define(
  "DukeSource.controller.risk.kri.parameter.ControllerPanelRegisterKeyRiskIndicator",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.kri.grids.StoreGridPanelRegisterKeyRiskIndicator"],
    models: ["risk.kri.grids.ModelGridPanelRegisterKeyRiskIndicator"],
    views: [
      "risk.kri.grids.ViewGridPanelRegisterKeyRiskIndicator",
      "risk.kri.parameter.ViewPanelRegisterKeyRiskIndicator"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridKeyRiskIndicator]": {
          keyup: this._onSearchTriggerGridKeyRiskIndicator
        },
        "[action=exportKeyRiskIndicatorPdf]": {
          click: this._onExportKeyRiskIndicatorPdf
        },
        "[action=exportKeyRiskIndicatorExcel]": {
          click: this._onExportKeyRiskIndicatorExcel
        },
        "[action=keyRiskIndicatorAuditory]": {
          click: this._onKeyRiskIndicatorAuditory
        },
        "[action=newKeyRiskIndicator]": {
          click: this._newKeyRiskIndicator
        },
        "[action=deleteKeyRiskIndicator]": {
          click: this._onDeleteKeyRiskIndicator
        },
        "[action=searchKeyRiskIndicator]": {
          specialkey: this._searchKeyRiskIndicator
        }
      });
    },
    _newKeyRiskIndicator: function() {
      var modelKeyRiskIndicator = Ext.create(
        "DukeSource.model.risk.kri.grids.ModelGridPanelRegisterKeyRiskIndicator",
        {
          idKeyRiskIndicator: "id",
          process: "",
          subProcess: "",
          workArea: "",
          factorRisk: "",
          managerRisk: "",
          calculatingFrequency: "",
          codeKri: "",
          nameKri: "",
          description: "",
          abbreviation: "",
          remarks: "",
          formCalculating: "",
          indicatorKri: "",
          coverage: "",
          modeEvaluation: "",
          unitMeasure: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterKeyRiskIndicator"
      )[0];
      var panel = general.down("ViewGridPanelRegisterKeyRiskIndicator");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelKeyRiskIndicator);
      editor.startEdit(0, 0);
    },
    _onDeleteKeyRiskIndicator: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterKeyRiskIndicator grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteKeyRiskIndicator.htm?nameView=ViewPanelRegisterKeyRiskIndicator"
      );
    },
    _searchKeyRiskIndicator: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterKeyRiskIndicator grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findKeyRiskIndicator.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridKeyRiskIndicator: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterKeyRiskIndicator grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportKeyRiskIndicatorPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportKeyRiskIndicatorExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onKeyRiskIndicatorAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterKeyRiskIndicator grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditKeyRiskIndicator.htm"
      );
    }
  }
);
