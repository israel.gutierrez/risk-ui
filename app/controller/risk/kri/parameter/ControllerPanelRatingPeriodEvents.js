Ext.define(
  "DukeSource.controller.risk.kri.parameter.ControllerPanelRatingPeriodEvents",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.kri.grids.StoreGridPanelRegisterScorecard",
      "risk.parameter.grids.StoreGridPanelRegisterWeightEvent"
    ],
    models: [
      "risk.kri.grids.ModelGridPanelRegisterScorecard",
      "risk.parameter.grids.ModelGridPanelRegisterWeightEvent"
    ],
    views: [
      "risk.kri.grids.ViewGridPanelRegisterScorecard",

      "risk.kri.parameter.ViewPanelRatingPeriodEvents",
      "DukeSource.view.risk.kri.combos.ViewComboScorecardMaster",
      "DukeSource.view.risk.kri.ViewTabRatingPeriodEvents",
      "risk.parameter.grids.ViewGridPanelRankingWeightEvent",
      "DukeSource.view.risk.parameter.combos.ViewComboMonth",
      "DukeSource.view.risk.parameter.combos.ViewComboYear"
      //        ,'DukeSource.view.risk.kri.windows.ViewWindowNewRatingPeriodEvent'
      //        ,'DukeSource.view.risk.parameter.combos.ViewComboYear'
      //        ,'DukeSource.view.risk.parameter.combos.ViewComboMonth'
    ],
    init: function() {
      this.control({
        "[action=newWindowSelectRatingPeriod]": {
          click: this._newWindowSelectRatingPeriod
        },
        "[action=findRantingPeriod]": {
          click: this._onFindRantingPeriod
        },
        "[action=newRantingPeriod]": {
          click: this._newRantingPeriod
        },
        "[action=calculateRantingPeriod]": {
          click: this._onCalculateRantingPeriod
        },
        "[action=saveRanting]": {
          click: this._onSaveRanting
        }
      });
    },

    _onSaveRanting: function(button) {
      var panelScorecard = button.up("ViewPanelRatingPeriodEvents");
      var gridWeightEvent = panelScorecard.down(
        "ViewGridPanelRankingWeightEvent"
      );
      var recordWeightEvents = gridWeightEvent.getStore().getRange();
      var arrayWeightEvents = new Array();
      Ext.Array.each(recordWeightEvents, function(
        record,
        index,
        countriesItSelf
      ) {
        arrayWeightEvents.push(record.data);
      });
      Ext.Ajax.request({
        method: "POST",
        url:
          "http://localhost:9000/giro/saveScoreEvent.htm?nameView=ViewPanelRatingPeriodEvents",
        params: {
          weightEvents: Ext.JSON.encode(arrayWeightEvents)
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              response.mensaje,
              Ext.Msg.INFO
            );
            panelScorecard.down("ViewComboYear").setReadOnly(false);
            panelScorecard.down("ViewComboMonth").setReadOnly(false);
            panelScorecard.down("button[action=saveRanting]").setVisible(false);
            panelScorecard
              .down("button[itemId=btnSearchRantingPeriod]")
              .setVisible(true);
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },

    _onCalculateRantingPeriod: function(button) {
      var panelScorecard = button.up("ViewPanelRatingPeriodEvents");
      var yearValue = panelScorecard.down("ViewComboYear").getValue();
      var monthValue = panelScorecard.down("ViewComboMonth").getValue();

      var gridWeightEvent = panelScorecard.down(
        "ViewGridPanelRankingWeightEvent"
      );
      var idScoreCardMaster = panelScorecard
        .down("UpperCaseTextFieldReadOnly[itemId=idScorecardMasterItemId]")
        .getValue();
      Ext.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/calculateScoreEvent.htm",
        params: {
          year: yearValue,
          month: monthValue,
          idScoreCardMaster: idScoreCardMaster
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            var datas = JSON.parse(response.data);
            gridWeightEvent.getStore().loadRawData(datas, false);
            button.setVisible(false);

            var evaluation = JSON.parse(response.evaluation);
            panelScorecard
              .down("UpperCaseTextFieldReadOnly[itemId=resultWeighting]")
              .setValue(evaluation.score);
            DukeSource.global.DirtyView.colorToElement(
              panelScorecard.down(
                "UpperCaseTextFieldReadOnly[itemId=resultWeighting]"
              ),
              evaluation.levelColour
            );
            panelScorecard
              .down("UpperCaseTextFieldReadOnly[itemId=descriptionWeighting]")
              .setValue(evaluation.description);
            DukeSource.global.DirtyView.colorToElement(
              panelScorecard.down(
                "UpperCaseTextFieldReadOnly[itemId=descriptionWeighting]"
              ),
              evaluation.levelColour
            );

            panelScorecard
              .down("form[itemId=formResultNormalized]")
              .setVisible(true);
            panelScorecard
              .down("button[action=calculateRantingPeriod]")
              .setVisible(false);
            panelScorecard.down("button[action=saveRanting]").setVisible(true);

            var tabPanels = panelScorecard.down("tabpanel");
            tabPanels.child("ViewGridPanelRankingWeightEvent").tab.show();
            tabPanels.setActiveTab(
              tabPanels.child("ViewGridPanelRankingWeightEvent")
            );
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },

    _newRantingPeriod: function(btn) {
      var window = btn.up("ViewWindowNewRatingPeriodEvent");
      var yearValue = window.down("ViewComboYear").getValue();
      var monthValue = window.down("ViewComboMonth").getValue();
      var panelScorecard = Ext.ComponentQuery.query(
        "ViewPanelRatingPeriodEvents"
      )[0];
      var gridScorecardConfig = panelScorecard.down(
        "ViewTabRatingPeriodEvents grid"
      );
      var storeScorecardConfig = gridScorecardConfig.getStore();
      Ext.Ajax.request({
        method: "POST",
        url:
          "http://localhost:9000/giro/buildJsonDetailScoreCardAndNormalizedNew.htm",
        params: {
          year: yearValue,
          month: monthValue
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            var fields = JSON.parse(response.fields);
            var columns = JSON.parse(response.columns);
            var data = JSON.parse(response.data);
            storeScorecardConfig.model.setFields(fields);

            for (var i = 0; i < columns.length; i++) {
              columns[i]["renderer"] = function(
                value,
                metaData,
                record,
                rowIndex,
                columnIndex,
                store,
                view
              ) {
                if (columnIndex === 15) {
                  metaData.tdAttr =
                    'style="background-color:  #d9ffdb !important; text-align:center;"';
                }
                return "<span >" + value + "</span>";
              };
            }

            gridScorecardConfig.reconfigure(storeScorecardConfig, columns);
            storeScorecardConfig.removeAll();
            storeScorecardConfig.loadRawData(data, true);

            panelScorecard.down("ViewComboYear").setReadOnly(true);
            panelScorecard.down("ViewComboMonth").setReadOnly(true);

            panelScorecard.down("ViewComboMonth").setValue(response.month);
            panelScorecard.down("ViewComboYear").setValue(response.year);
            panelScorecard
              .down(
                "UpperCaseTextFieldReadOnly[itemId=idScorecardMasterItemId]"
              )
              .setValue(response.idScorecardMaster);

            panelScorecard
              .down("button[action=calculateRantingPeriod]")
              .setVisible(true);
            //                    panelScorecard.down('UpperCaseTextFieldReadOnly[itemId=resultWeighting]').setVisible(false);
            //                    panelScorecard.down('UpperCaseTextFieldReadOnly[itemId=descriptionWeighting]').setVisible(false);
            panelScorecard
              .down("button[itemId=btnSearchRantingPeriod]")
              .setVisible(false);
            panelScorecard
              .down("form[itemId=formResultNormalized]")
              .setVisible(false);

            var tabPanels = panelScorecard.down("tabpanel");
            tabPanels.child("ViewTabRatingPeriodEvents").tab.show();
            tabPanels.child("ViewGridPanelRankingWeightEvent").tab.hide();
            tabPanels.setActiveTab(
              tabPanels.child("ViewTabRatingPeriodEvents")
            );
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },

    _onFindRantingPeriod: function() {
      var panelScorecard = Ext.ComponentQuery.query(
        "ViewPanelRatingPeriodEvents"
      )[0];
      var yearValue = panelScorecard.down("ViewComboYear").getValue();
      var monthValue = panelScorecard.down("ViewComboMonth").getValue();

      var gridScorecardConfig = panelScorecard.down(
        "ViewTabRatingPeriodEvents grid"
      );
      var storeScorecardConfig = gridScorecardConfig.getStore();
      Ext.Ajax.request({
        method: "POST",
        url:
          "http://localhost:9000/giro/buildJsonDetailScoreCardAndNormalized.htm",
        params: {
          year: yearValue,
          month: monthValue
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            var fields = JSON.parse(response.fields);
            var columns = JSON.parse(response.columns);
            var data = JSON.parse(response.data);
            storeScorecardConfig.model.setFields(fields);

            for (var i = 0; i < columns.length; i++) {
              columns[i]["renderer"] = function(
                value,
                metaData,
                record,
                rowIndex,
                columnIndex,
                store,
                view
              ) {
                if (columnIndex === 15) {
                  metaData.tdAttr =
                    'style="background-color:  #d9ffdb !important; text-align:center;"';
                }
                return "<span >" + value + "</span>";
              };
            }

            gridScorecardConfig.reconfigure(storeScorecardConfig, columns);
            storeScorecardConfig.removeAll();
            storeScorecardConfig.loadRawData(data, true);
            var gridWeightEvent = panelScorecard.down(
              "ViewGridPanelRankingWeightEvent"
            );

            var datas = JSON.parse(response.listWeightEventBeans);
            gridWeightEvent.getStore().loadRawData(datas, false);

            var evaluation = JSON.parse(response.evaluation);
            panelScorecard
              .down("UpperCaseTextFieldReadOnly[itemId=resultWeighting]")
              .setValue(evaluation.score);
            DukeSource.global.DirtyView.colorToElement(
              panelScorecard.down(
                "UpperCaseTextFieldReadOnly[itemId=resultWeighting]"
              ),
              evaluation.levelColour
            );
            panelScorecard
              .down("UpperCaseTextFieldReadOnly[itemId=descriptionWeighting]")
              .setValue(evaluation.description);
            DukeSource.global.DirtyView.colorToElement(
              panelScorecard.down(
                "UpperCaseTextFieldReadOnly[itemId=descriptionWeighting]"
              ),
              evaluation.levelColour
            );

            var tabPanels = panelScorecard.down("tabpanel");
            tabPanels.child("ViewTabRatingPeriodEvents").tab.show();
            tabPanels.child("ViewGridPanelRankingWeightEvent").tab.show();
            tabPanels.setActiveTab(
              tabPanels.child("ViewTabRatingPeriodEvents")
            );

            //                    panelScorecard.down('button[action=calculateRantingPeriod]').setVisible(true);
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },

    _newWindowSelectRatingPeriod: function() {
      var window = Ext.create(
        "DukeSource.view.risk.kri.windows.ViewWindowNewRatingPeriodEvent",
        {
          modal: true
        }
      );
      window.show();
    }
  }
);
