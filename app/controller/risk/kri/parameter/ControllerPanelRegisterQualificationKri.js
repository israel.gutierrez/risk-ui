Ext.define(
  "DukeSource.controller.risk.kri.parameter.ControllerPanelRegisterQualificationKri",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.kri.grids.StoreGridPanelRegisterQualificationKri"],
    models: ["risk.kri.grids.ModelGridPanelRegisterQualificationKri"],
    views: [
      "risk.kri.grids.ViewGridPanelRegisterQualificationKri",
      "risk.kri.parameter.ViewPanelRegisterQualificationKri"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridQualificationKri]": {
          keyup: this._onSearchTriggerGridQualificationKri
        },
        "[action=exportQualificationKriPdf]": {
          click: this._onExportQualificationKriPdf
        },
        "[action=exportQualificationKriExcel]": {
          click: this._onExportQualificationKriExcel
        },
        "[action=qualificationKriAuditory]": {
          click: this._onQualificationKriAuditory
        },
        "[action=newQualificationKri]": {
          click: this._newQualificationKri
        },
        "[action=deleteQualificationKri]": {
          click: this._onDeleteQualificationKri
        },
        "[action=searchQualificationKri]": {
          specialkey: this._searchQualificationKri
        }
      });
    },
    _newQualificationKri: function() {
      var modelQualificationKri = Ext.create(
        "DukeSource.model.risk.kri.grids.ModelGridPanelRegisterQualificationKri",
        {
          idQualificationKri: "id",
          description: "",
          rangeInf: "",
          rangeSup: "",
          levelColour: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterQualificationKri"
      )[0];
      var panel = general.down("ViewGridPanelRegisterQualificationKri");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelQualificationKri);
      editor.startEdit(0, 0);
    },
    _onDeleteQualificationKri: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterQualificationKri grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteQualificationKri.htm?nameView=ViewPanelRegisterQualificationKri"
      );
    },
    _searchQualificationKri: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterQualificationKri grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findQualificationKri.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridQualificationKri: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterQualificationKri grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportQualificationKriPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportQualificationKriExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onQualificationKriAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterQualificationKri grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditQualificationKri.htm"
      );
    }
  }
);
