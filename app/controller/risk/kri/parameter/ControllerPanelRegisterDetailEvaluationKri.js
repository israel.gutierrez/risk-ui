Ext.define(
  "DukeSource.controller.risk.kri.parameter.ControllerPanelRegisterDetailEvaluationKri",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.kri.grids.StoreGridPanelRegisterDetailEvaluationKri"],
    models: ["risk.kri.grids.ModelGridPanelRegisterDetailEvaluationKri"],
    views: [
      "risk.kri.grids.ViewGridPanelRegisterDetailEvaluationKri",
      "risk.kri.parameter.ViewPanelRegisterDetailEvaluationKri"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridDetailEvaluationKri]": {
          keyup: this._onSearchTriggerGridDetailEvaluationKri
        },
        "[action=exportDetailEvaluationKriPdf]": {
          click: this._onExportDetailEvaluationKriPdf
        },
        "[action=exportDetailEvaluationKriExcel]": {
          click: this._onExportDetailEvaluationKriExcel
        },
        "[action=detailEvaluationKriAuditory]": {
          click: this._onDetailEvaluationKriAuditory
        },
        "[action=newDetailEvaluationKri]": {
          click: this._newDetailEvaluationKri
        },
        "[action=deleteDetailEvaluationKri]": {
          click: this._onDeleteDetailEvaluationKri
        },
        "[action=searchDetailEvaluationKri]": {
          specialkey: this._searchDetailEvaluationKri
        }
      });
    },
    _newDetailEvaluationKri: function() {
      var modelDetailEvaluationKri = Ext.create(
        "DukeSource.model.risk.kri.grids.ModelGridPanelRegisterDetailEvaluationKri",
        {
          idDetailEvaluationKri: "id",
          evaluationKri: "",
          description: "",
          indicatorValue: "",
          indicatorNormalize: "",
          weighing: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailEvaluationKri"
      )[0];
      var panel = general.down("ViewGridPanelRegisterDetailEvaluationKri");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelDetailEvaluationKri);
      editor.startEdit(0, 0);
    },
    _onDeleteDetailEvaluationKri: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailEvaluationKri grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteDetailEvaluationKri.htm?nameView=ViewPanelRegisterDetailEvaluationKri"
      );
    },
    _searchDetailEvaluationKri: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterDetailEvaluationKri grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findDetailEvaluationKri.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridDetailEvaluationKri: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailEvaluationKri grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportDetailEvaluationKriPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportDetailEvaluationKriExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onDetailEvaluationKriAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailEvaluationKri grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditDetailEvaluationKri.htm"
      );
    }
  }
);
