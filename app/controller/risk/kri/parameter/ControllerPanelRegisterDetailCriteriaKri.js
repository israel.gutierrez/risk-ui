Ext.define(
  "DukeSource.controller.risk.kri.parameter.ControllerPanelRegisterDetailCriteriaKri",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.kri.grids.StoreTreeGridPanelRegisterDetailCriteriaKri"],
    models: ["risk.kri.grids.ModelTreeGridPanelRegisterDetailCriteriaKri"],
    views: ["risk.kri.grids.ViewTreeGridPanelRegisterDetailCriteriaKri"],
    init: function() {
      this.control({
        "[action=searchTriggerGridDetailCriteriaKri]": {
          keyup: this._onSearchTriggerGridDetailCriteriaKri
        },
        "[action=exportDetailCriteriaKriPdf]": {
          click: this._onExportDetailCriteriaKriPdf
        },
        "[action=exportDetailCriteriaKriExcel]": {
          click: this._onExportDetailCriteriaKriExcel
        },
        "[action=detailCriteriaKriAuditory]": {
          click: this._onDetailCriteriaKriAuditory
        },
        "[action=deleteDetailCriteriaKri]": {
          click: this._onDeleteDetailCriteriaKri
        },
        "[action=searchDetailCriteriaKri]": {
          specialkey: this._searchDetailCriteriaKri
        },
        "[action=newDetailCriteriaKri]": {
          click: this._newDetailCriteriaKri
        },
        "[action=saveCriteriaKri]": {
          click: this._onSaveCriteriaKri
        },
        ViewTreeGridPanelRegisterDetailCriteriaKri: {
          itemcontextmenu: this.treeRightClickDetailCriteria
        },
        "AddMenuCriteria menuitem[text=Agregar]": {
          click: this._addDetailCriteriaKri
        },
        "AddMenuCriteria menuitem[text=Editar]": {
          click: this._editCriteriaKri
        },
        "AddMenuCriteria menuitem[text=Eliminar]": {
          click: this._deleteCriteriaKri
        },
        "EditMenuDetailCriteria menuitem[text=Editar]": {
          click: this._editDetailCriteriaKri
        },
        "EditMenuDetailCriteria menuitem[text=Eliminar]": {
          click: this._deleteCriteriaKri
        }
      });
    },
    _newDetailCriteriaKri: function() {
      var win = Ext.create(
        "DukeSource.view.risk.kri.windows.ViewWindowCriteriaKri",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getComponent("id")
        .setValue("myTree/id");
      win.down("#text").focus(false, 200);
      win.show();
    },
    _onSaveCriteriaKri: function(button) {
      var win = button.up("window");
      var form = win.down("form");

      if (form.getForm().isValid()) {
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveDetailCriteriaKri.htm?nameView=ViewPanelRegisterDetailCriteriaKri",
          params: {
            id: form.getComponent("id").getValue(),
            description: form.getComponent("text").getValue(),
            typeCriteria:
              form.down("#typeCriteria") === null
                ? ""
                : form.down("#typeCriteria").getValue(),
            fieldName:
              form.down("#fieldName") === null
                ? ""
                : form.down("#fieldName").getValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              var refreshNode = Ext.ComponentQuery.query(
                "ViewPanelRegisterDetailCriteriaKri ViewTreeGridPanelRegisterDetailCriteriaKri"
              )[0]
                .getStore()
                .getNodeById(response.data);
              refreshNode.removeAll(false);
              Ext.ComponentQuery.query(
                "ViewPanelRegisterDetailCriteriaKri ViewTreeGridPanelRegisterDetailCriteriaKri"
              )[0]
                .getStore()
                .load({
                  node: refreshNode
                });
              win.close();
            } else {
              DukeSource.global.DirtyView.messageWarning(response.message);
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
      }
    },

    treeRightClickDetailCriteria: function(view, record, item, index, e) {
      e.stopEvent();
      this.application.currentRecord = record;
      if (record.get("depth") === 1) {
        var addMenu = Ext.create(
          "DukeSource.view.risk.kri.parameter.AddMenuCriteria",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      if (record.get("depth") === 2) {
        addMenu = Ext.create(
          "DukeSource.view.risk.kri.parameter.EditMenuDetailCriteria",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      return false;
    },

    _addDetailCriteriaKri: function() {
      var win = Ext.create(
        "DukeSource.view.risk.kri.windows.ViewWindowDetailCriteriaKri",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id") + "/id");
      win.down("#text").focus(false, 200);
      win.show();
    },

    _editCriteriaKri: function() {
      var win = Ext.create(
        "DukeSource.view.risk.kri.windows.ViewWindowCriteriaKri",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id"));
      win
        .down("form")
        .getComponent("text")
        .setValue(this.application.currentRecord.get("text"));
      win.down("#text").focus(false, 200);
      win.show();
    },
    _deleteCriteriaKri: function() {
      Ext.Ajax.request({
        method: "POST",
        url:
          "http://localhost:9000/giro/deleteDetailCriteriaKri.htm?nameView=ViewPanelRegisterDetailCriteriaKri",
        params: {
          id: this.application.currentRecord.get("id")
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              response.mensaje,
              Ext.Msg.INFO
            );
            var refreshNode = Ext.ComponentQuery.query(
              "ViewPanelRegisterDetailCriteriaKri ViewTreeGridPanelRegisterDetailCriteriaKri"
            )[0]
              .getStore()
              .getNodeById(response.data);
            refreshNode.removeAll(false);
            Ext.ComponentQuery.query(
              "ViewPanelRegisterDetailCriteriaKri ViewTreeGridPanelRegisterDetailCriteriaKri"
            )[0]
              .getStore()
              .load({
                node: refreshNode
              });
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },
    _editDetailCriteriaKri: function() {
      var win = Ext.create(
        "DukeSource.view.risk.kri.windows.ViewWindowDetailCriteriaKri",
        {
          modal: true
        }
      );
      win.down("#typeCriteria").store.load();
      win
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id"));
      win
        .down("form")
        .getComponent("text")
        .setValue(this.application.currentRecord.get("text"));
      win
        .down("form")
        .getComponent("fieldName")
        .setValue(this.application.currentRecord.get("fieldName"));
      win
        .down("form")
        .getComponent("typeCriteria")
        .setValue(this.application.currentRecord.get("typeCriteria"));
      win.down("#text").focus(false, 200);
      win.show();
    },

    _onDeleteDetailCriteriaKri: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailCriteriaKri grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteDetailCriteriaKri.htm?nameView=ViewPanelRegisterDetailCriteriaKri"
      );
    },
    _searchDetailCriteriaKri: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterDetailCriteriaKri grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findDetailCriteriaKri.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridDetailCriteriaKri: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailCriteriaKri grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportDetailCriteriaKriPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportDetailCriteriaKriExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onDetailCriteriaKriAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailCriteriaKri grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditDetailCriteriaKri.htm"
      );
    }
  }
);
