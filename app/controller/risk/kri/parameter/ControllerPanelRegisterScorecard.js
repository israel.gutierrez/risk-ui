Ext.define(
  "DukeSource.controller.risk.kri.parameter.ControllerPanelRegisterScorecard",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.kri.grids.StoreGridPanelRegisterScorecard",
      "risk.parameter.grids.StoreGridPanelRegisterWeightEvent"
    ],
    models: [
      "risk.kri.grids.ModelGridPanelRegisterScorecard",
      "risk.parameter.grids.ModelGridPanelRegisterWeightEvent"
    ],
    views: [
      "risk.kri.grids.ViewGridPanelRegisterScorecard",

      "DukeSource.view.risk.kri.combos.ViewComboScorecardMaster",
      "DukeSource.view.risk.kri.ViewPanelRegisterScorecardConfig",
      "risk.parameter.grids.ViewGridPanelRegisterWeightEvent",
      "risk.kri.parameter.ViewPanelRegisterScorecard"
      //        , 'risk.kri.parameter.ViewPanelChartPieEventOne'
    ],
    init: function() {
      this.control({
        "[action=searchConfigScorecard]": {
          select: this._onSearchConfigScorecard
        },
        "[action=selectListKris]": {
          click: this._onSelectListKris
        },
        "[action=searchTriggerGridScorecard]": {
          keyup: this._onSearchTriggerGridScorecard
        },
        "[action=exportScorecardPdf]": {
          click: this._onExportScorecardPdf
        },
        "[action=exportScorecardExcel]": {
          click: this._onExportScorecardExcel
        },
        "[action=scorecardAuditory]": {
          click: this._onScorecardAuditory
        },
        "[action=newScorecard]": {
          click: this._newScorecard
        },
        "[action=deleteScorecard]": {
          click: this._onDeleteScorecard
        },
        "[action=searchScorecard]": {
          specialkey: this._searchScorecard
        },
        "[action=saveScoreCardConfig]": {
          click: this._onSaveScoreCardConfig
        },
        "[action=cancelScoreCardConfig]": {
          click: this._onCancelScoreCardConfig
        },
        "[action=continueScoreCardConfig]": {
          click: this._onContinueScoreCardConfig
        }
      });
    },

    _onContinueScoreCardConfig: function(button) {
      var panelGeneral = button.up("ViewPanelRegisterScorecard");
      panelGeneral
        .down("UpperCaseTextFieldObligatory[name=descriptionScorecard]")
        .setVisible(true);
      panelGeneral.down("ViewComboScorecardMaster").setVisible(false);
      panelGeneral
        .down("button[action=cancelScoreCardConfig]")
        .setVisible(true);
      panelGeneral.down("button[action=saveScoreCardConfig]").setVisible(true);
      panelGeneral
        .down("button[action=continueScoreCardConfig]")
        .setVisible(false);
      var tabPanels = panelGeneral.down("tabpanel");
      tabPanels.child("ViewPanelRegisterScorecardConfig").tab.show();
      tabPanels.child("ViewGridPanelRegisterWeightEvent").tab.show();
      tabPanels.setActiveTab(
        tabPanels.child("ViewGridPanelRegisterWeightEvent")
      );
    },

    _onCancelScoreCardConfig: function(button) {
      var panelGeneral = button.up("ViewPanelRegisterScorecard");
      panelGeneral
        .down("UpperCaseTextFieldObligatory[name=descriptionScorecard]")
        .setVisible(false);
      panelGeneral.down("ViewComboScorecardMaster").setVisible(true);
      panelGeneral
        .down("button[action=cancelScoreCardConfig]")
        .setVisible(false);
      panelGeneral.down("button[action=saveScoreCardConfig]").setVisible(false);
      panelGeneral
        .down("button[action=continueScoreCardConfig]")
        .setVisible(false);
      var tabPanels = panelGeneral.down("tabpanel");
      tabPanels.child("ViewPanelRegisterScorecardConfig").tab.show();
      tabPanels.child("ViewGridPanelRegisterWeightEvent").tab.show();
      tabPanels.setActiveTab(
        tabPanels.child("ViewPanelRegisterScorecardConfig")
      );
    },

    _onSaveScoreCardConfig: function(button) {
      var panelGeneral = button.up("ViewPanelRegisterScorecard");
      var gridWeightEvent = panelGeneral.down(
        "ViewGridPanelRegisterWeightEvent"
      );
      var recordWeightEvents = gridWeightEvent.getStore().getRange();

      var validityPeriod = panelGeneral
        .down("datefield[itemId=dateInitial]")
        .getRawValue();
      var expirationPeriod = panelGeneral
        .down("datefield[itemId=dateFinal]")
        .getRawValue();

      var arrayWeightEvents = new Array();
      Ext.Array.each(recordWeightEvents, function(
        record,
        index,
        countriesItSelf
      ) {
        arrayWeightEvents.push(record.data);
      });
      var panelScorecardConfig = panelGeneral.down(
        "ViewPanelRegisterScorecardConfig"
      );
      var gridScorecardConfig = panelScorecardConfig.down("grid");
      var records = gridScorecardConfig.getStore().getRange();
      var arrayScoreCardMaster = new Array();
      Ext.Array.each(records, function(record, index, countriesItSelf) {
        arrayScoreCardMaster.push(record.data);
      });
      Ext.Ajax.request({
        method: "POST",
        url:
          "http://localhost:9000/giro/saveDetailScorecard.htm?nameView=ViewPanelRegisterScorecard",
        params: {
          jsonData: Ext.JSON.encode(arrayScoreCardMaster),
          descriptionScorecard: panelGeneral
            .down("UpperCaseTextFieldObligatory[name=descriptionScorecard]")
            .getValue(),
          weightEvents: Ext.JSON.encode(arrayWeightEvents),
          validityPeriod: validityPeriod,
          expirationPeriod: expirationPeriod
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              response.mensaje,
              Ext.Msg.INFO
            );
            panelGeneral
              .down("UpperCaseTextFieldObligatory[name=descriptionScorecard]")
              .setVisible(false);
            panelGeneral.down("ViewComboScorecardMaster").setVisible(true);
            panelGeneral
              .down("button[action=cancelScoreCardConfig]")
              .setVisible(false);
            panelGeneral
              .down("button[action=saveScoreCardConfig]")
              .setVisible(false);
            panelGeneral
              .down("button[action=continueScoreCardConfig]")
              .setVisible(false);

            var tabPanels = panelGeneral.down("tabpanel");
            tabPanels.child("ViewPanelRegisterScorecardConfig").tab.show();
            tabPanels.child("ViewGridPanelRegisterWeightEvent").tab.show();
            tabPanels.setActiveTab(
              tabPanels.child("ViewPanelRegisterScorecardConfig")
            );
            var comboScorecardMaster = panelGeneral.down(
              "ViewComboScorecardMaster"
            );
            comboScorecardMaster.getStore().load({
              url:
                "http://localhost:9000/giro/showListScorecardMasterActivesComboBox.htm",
              extraParams: {
                propertyOrder: "description"
              },
              callback: function() {
                comboScorecardMaster.reset();
              }
            });
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },

    _onSearchConfigScorecard: function(cbo, record) {
      var panelScorecard = cbo.up("ViewPanelRegisterScorecard");
      var validityPeriod = panelScorecard.down("datefield[itemId=dateInitial]");
      var expirationPeriod = panelScorecard.down("datefield[itemId=dateFinal]");

      var gridWeightEvent = panelScorecard.down(
        "ViewGridPanelRegisterWeightEvent"
      );
      gridWeightEvent.store.getProxy().extraParams = {
        idScorecardMaster: cbo.getValue()
      };
      gridWeightEvent.store.getProxy().url =
        "http://localhost:9000/giro/findWeightEvent.htm";
      gridWeightEvent.down("pagingtoolbar").moveFirst();
      var gridScorecardConfig = panelScorecard.down(
        "ViewPanelRegisterScorecardConfig grid"
      );
      var storeScorecardConfig = gridScorecardConfig.getStore();
      Ext.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/buildJsonDetailScoreCardById.htm",
        params: {
          idScorecardMaster: cbo.getValue()
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            var fields = JSON.parse(response.fields);
            var columns = JSON.parse(response.columns);
            var data = JSON.parse(response.data);
            storeScorecardConfig.model.setFields(fields);
            gridScorecardConfig.reconfigure(storeScorecardConfig, columns);
            storeScorecardConfig.removeAll();
            storeScorecardConfig.loadRawData(data, true);
            validityPeriod.setValue(response.validityPeriod);
            expirationPeriod.setValue(response.expirationPeriod);
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },

    _onSelectListKris: function(btn) {
      var windowMasterKri = btn.up("ViewWindowSelectListMasterKri");
      if (
        windowMasterKri
          .down("form")
          .getForm()
          .isValid()
      ) {
        var gridMasterKri = windowMasterKri.down("gridpanel");
        var selectionGrid = gridMasterKri.getSelectionModel().getSelection();
        if (selectionGrid.length > 0) {
          var arrayDataToSave = new Array();
          Ext.Array.each(selectionGrid, function(record) {
            arrayDataToSave.push(record.data);
          });
          var panelGeneral = Ext.ComponentQuery.query(
            "ViewPanelRegisterScorecard"
          )[0];
          var newDescriptionScoreCard = windowMasterKri.down(
            "UpperCaseTextFieldObligatory[name=newDescriptionScorecard]"
          );

          var gridWeightEvent = panelGeneral.down(
            "tabpanel ViewGridPanelRegisterWeightEvent"
          );
          var newDescriptionScorecard = windowMasterKri.down(
            "UpperCaseTextFieldObligatory[name=newDescriptionScorecard]"
          );
          gridWeightEvent.store.getProxy().extraParams = {
            newDescriptionScorecard: newDescriptionScorecard.getValue()
          };
          gridWeightEvent.store.getProxy().url =
            "http://localhost:9000/giro/newWeightEvent.htm";
          gridWeightEvent.down("pagingtoolbar").moveFirst();
          var gridScorecardConfig = panelGeneral.down(
            "tabpanel ViewPanelRegisterScorecardConfig gridpanel"
          );
          var storeScorecardConfig = gridScorecardConfig.getStore();
          Ext.Ajax.request({
            method: "POST",
            url: "http://localhost:9000/giro/loadGridNewDetailScoreCard.htm",
            params: {
              listKris: Ext.JSON.encode(arrayDataToSave)
            },
            success: function(response) {
              response = Ext.decode(response.responseText);
              if (response.success) {
                var fields = JSON.parse(response.fields);
                var columns = JSON.parse(response.columns);
                var data = JSON.parse(response.data);
                storeScorecardConfig.model.setFields(fields);
                gridScorecardConfig.reconfigure(storeScorecardConfig, columns);
                storeScorecardConfig.removeAll();
                storeScorecardConfig.loadRawData(data, true);

                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_MESSAGE,
                  "Ingrese en matriz de Causa en   PES.IND y REL. IND",
                  Ext.Msg.INFO
                );

                var descriptionScoreCard = panelGeneral.down(
                  "UpperCaseTextFieldObligatory[name=descriptionScorecard]"
                );
                descriptionScoreCard.setValue(
                  newDescriptionScoreCard.getValue()
                );
                descriptionScoreCard.setVisible(true);
                panelGeneral.down("ViewComboScorecardMaster").setVisible(false);
                panelGeneral
                  .down("button[action=cancelScoreCardConfig]")
                  .setVisible(true);
                panelGeneral
                  .down("button[action=saveScoreCardConfig]")
                  .setVisible(false);
                panelGeneral
                  .down("button[action=continueScoreCardConfig]")
                  .setVisible(true);

                var tabPanels = panelGeneral.down("tabpanel");
                tabPanels.child("ViewPanelRegisterScorecardConfig").tab.show();
                tabPanels.child("ViewGridPanelRegisterWeightEvent").tab.hide();
                tabPanels.setActiveTab(
                  tabPanels.child("ViewPanelRegisterScorecardConfig")
                );

                var expirationPeriodWindow = windowMasterKri
                  .down("datefield[name=dateFinalWindow]")
                  .getValue();
                var validityPeriodWindow = windowMasterKri
                  .down("datefield[name=dateInitialWindow]")
                  .getValue();

                panelGeneral
                  .down("datefield[itemId=dateInitial]")
                  .setValue(validityPeriodWindow);
                panelGeneral
                  .down("datefield[itemId=dateFinal]")
                  .setValue(expirationPeriodWindow);
                windowMasterKri.close();
              } else {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_ERROR,
                  response.mensaje,
                  Ext.Msg.ERROR
                );
              }
            },
            failure: function() {}
          });
        } else {
          DukeSource.global.DirtyView.messageAlert(
            DukeSource.global.GiroMessages.TITLE_ERROR,
            "Debe Seleccionar al menos un Kris",
            Ext.Msg.ERROR
          );
        }
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },

    _newScorecard: function() {
      Ext.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/findDateValidityScoreCard.htm",
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            var validityPeriod = response.data;
            var window = Ext.create(
              "DukeSource.view.risk.kri.windows.ViewWindowSelectListMasterKri",
              {
                modal: true
              }
            );
            window.show();
            window
              .down("datefield[name=dateInitialWindow]")
              .setValue(validityPeriod);
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              "ERROR EN OBTENER LA FECHA DE VIGENCIA DEL ANTERIOR CUADRO DE MANDO",
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },
    _onDeleteScorecard: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterScorecard grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteScorecard.htm?nameView=ViewPanelRegisterScorecard"
      );
    },
    _searchScorecard: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterScorecard grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findScorecardMaster.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridScorecard: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterScorecard grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportScorecardPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportScorecardExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onScorecardAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterScorecard grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditScorecard.htm"
      );
    }
  }
);
