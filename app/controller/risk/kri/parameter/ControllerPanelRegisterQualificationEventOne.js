Ext.define(
  "DukeSource.controller.risk.kri.parameter.ControllerPanelRegisterQualificationEventOne",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.kri.grids.StoreGridPanelRegisterQualificationEventOne"],
    models: ["risk.kri.grids.ModelGridPanelRegisterQualificationEventOne"],
    views: [
      "risk.kri.grids.ViewGridPanelRegisterQualificationEventOne",
      "risk.kri.parameter.ViewPanelRegisterQualificationEventOne"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridQualificationEventOne]": {
          keyup: this._onSearchTriggerGridQualificationEventOne
        },
        "[action=exportQualificationEventOnePdf]": {
          click: this._onExportQualificationEventOnePdf
        },
        "[action=exportQualificationEventOneExcel]": {
          click: this._onExportQualificationEventOneExcel
        },
        "[action=qualificationEventOneAuditory]": {
          click: this._onQualificationEventOneAuditory
        },
        "[action=newQualificationEventOne]": {
          click: this._newQualificationEventOne
        },
        "[action=deleteQualificationEventOne]": {
          click: this._onDeleteQualificationEventOne
        },
        "[action=searchQualificationEventOne]": {
          specialkey: this._searchQualificationEventOne
        }
      });
    },
    _newQualificationEventOne: function() {
      var modelQualificationEventOne = Ext.create(
        "DukeSource.model.risk.kri.grids.ModelGridPanelRegisterQualificationEventOne",
        {
          idQualificationEventOne: "id",
          description: "",
          rangeInf: "",
          rangeSup: "",
          levelColour: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterQualificationEventOne"
      )[0];
      var panel = general.down("ViewGridPanelRegisterQualificationEventOne");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelQualificationEventOne);
      editor.startEdit(0, 0);
    },
    _onDeleteQualificationEventOne: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterQualificationEventOne grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteQualificationEventOne.htm?nameView=ViewPanelRegisterQualificationEventOne"
      );
    },
    _searchQualificationEventOne: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterQualificationEventOne grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findQualificationEventOne.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridQualificationEventOne: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterQualificationEventOne grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportQualificationEventOnePdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportQualificationEventOneExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onQualificationEventOneAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterQualificationEventOne grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditQualificationEventOne.htm"
      );
    }
  }
);
