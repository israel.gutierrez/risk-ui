Ext.define(
  "DukeSource.controller.risk.kri.parameter.ControllerPanelRegisterCriteriaEvaluationKri",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.kri.grids.StoreGridPanelRegisterCriteriaEvaluationKri"],
    models: ["risk.kri.grids.ModelGridPanelRegisterCriteriaEvaluationKri"],
    views: [
      "risk.kri.grids.ViewGridPanelRegisterCriteriaEvaluationKri",
      "risk.kri.parameter.ViewPanelRegisterCriteriaEvaluationKri"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridCriteriaEvaluationKri]": {
          keyup: this._onSearchTriggerGridCriteriaEvaluationKri
        },
        "[action=exportCriteriaEvaluationKriPdf]": {
          click: this._onExportCriteriaEvaluationKriPdf
        },
        "[action=exportCriteriaEvaluationKriExcel]": {
          click: this._onExportCriteriaEvaluationKriExcel
        },
        "[action=criteriaEvaluationKriAuditory]": {
          click: this._onCriteriaEvaluationKriAuditory
        },
        "[action=newCriteriaEvaluationKri]": {
          click: this._newCriteriaEvaluationKri
        },
        "[action=deleteCriteriaEvaluationKri]": {
          click: this._onDeleteCriteriaEvaluationKri
        },
        "[action=searchCriteriaEvaluationKri]": {
          specialkey: this._searchCriteriaEvaluationKri
        }
      });
    },
    _newCriteriaEvaluationKri: function() {
      var modelCriteriaEvaluationKri = Ext.create(
        "DukeSource.model.risk.kri.grids.ModelGridPanelRegisterCriteriaEvaluationKri",
        {
          idCriteriaEvaluationKri: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterCriteriaEvaluationKri"
      )[0];
      var panel = general.down("ViewGridPanelRegisterCriteriaEvaluationKri");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelCriteriaEvaluationKri);
      editor.startEdit(0, 0);
    },
    _onDeleteCriteriaEvaluationKri: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterCriteriaEvaluationKri grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteCriteriaEvaluationKri.htm?nameView=ViewPanelRegisterCriteriaEvaluationKri"
      );
    },
    _searchCriteriaEvaluationKri: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterCriteriaEvaluationKri grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findCriteriaEvaluationKri.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridCriteriaEvaluationKri: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterCriteriaEvaluationKri grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportCriteriaEvaluationKriPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportCriteriaEvaluationKriExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onCriteriaEvaluationKriAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterCriteriaEvaluationKri grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditCriteriaEvaluationKri.htm"
      );
    }
  }
);
