Ext.define(
  "DukeSource.controller.risk.kri.parameter.ControllerPanelRegisterDetailWeighingKri",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.kri.grids.StoreGridPanelRegisterDetailWeighingKri"],
    models: ["risk.kri.grids.ModelGridPanelRegisterDetailWeighingKri"],
    views: [
      "risk.kri.grids.ViewGridPanelRegisterDetailWeighingKri",
      "risk.kri.parameter.ViewPanelRegisterDetailWeighingKri"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridDetailWeighingKri]": {
          keyup: this._onSearchTriggerGridDetailWeighingKri
        },
        "[action=exportDetailWeighingKriPdf]": {
          click: this._onExportDetailWeighingKriPdf
        },
        "[action=exportDetailWeighingKriExcel]": {
          click: this._onExportDetailWeighingKriExcel
        },
        "[action=detailWeighingKriAuditory]": {
          click: this._onDetailWeighingKriAuditory
        },
        "[action=newDetailWeighingKri]": {
          click: this._newDetailWeighingKri
        },
        "[action=deleteDetailWeighingKri]": {
          click: this._onDeleteDetailWeighingKri
        },
        "[action=searchDetailWeighingKri]": {
          specialkey: this._searchDetailWeighingKri
        }
      });
    },
    _newDetailWeighingKri: function() {
      var modelDetailWeighingKri = Ext.create(
        "DukeSource.model.risk.kri.grids.ModelGridPanelRegisterDetailWeighingKri",
        {
          idDetailWeighingKri: "id",
          idWeighingKri: "",
          normalizerKri: "",
          weighing: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailWeighingKri"
      )[0];
      var panel = general.down("ViewGridPanelRegisterDetailWeighingKri");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelDetailWeighingKri);
      editor.startEdit(0, 0);
    },
    _onDeleteDetailWeighingKri: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailWeighingKri grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteDetailWeighingKri.htm?nameView=ViewPanelRegisterDetailWeighingKri"
      );
    },
    _searchDetailWeighingKri: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterDetailWeighingKri grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findDetailWeighingKri.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridDetailWeighingKri: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailWeighingKri grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportDetailWeighingKriPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportDetailWeighingKriExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onDetailWeighingKriAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailWeighingKri grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditDetailWeighingKri.htm"
      );
    }
  }
);
