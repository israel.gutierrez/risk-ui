Ext.define(
  "DukeSource.controller.risk.kri.parameter.ControllerPanelRegisterQuantityValuesWeights",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.kri.grids.StoreGridPanelRegisterQuantityValuesWeights"],
    models: ["risk.kri.grids.ModelGridPanelRegisterQuantityValuesWeights"],
    views: [
      "risk.kri.grids.ViewGridPanelRegisterQuantityValuesWeights",
      "risk.kri.parameter.ViewPanelRegisterQuantityValuesWeights"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridQuantityValuesWeights]": {
          keyup: this._onSearchTriggerGridQuantityValuesWeights
        },
        "[action=exportQuantityValuesWeightsPdf]": {
          click: this._onExportQuantityValuesWeightsPdf
        },
        "[action=exportQuantityValuesWeightsExcel]": {
          click: this._onExportQuantityValuesWeightsExcel
        },
        "[action=quantityValuesWeightsAuditory]": {
          click: this._onQuantityValuesWeightsAuditory
        },
        "[action=newQuantityValuesWeights]": {
          click: this._newQuantityValuesWeights
        },
        "[action=deleteQuantityValuesWeights]": {
          click: this._onDeleteQuantityValuesWeights
        },
        "[action=searchQuantityValuesWeights]": {
          specialkey: this._searchQuantityValuesWeights
        }
      });
    },
    _newQuantityValuesWeights: function() {
      var modelQuantityValuesWeights = Ext.create(
        "DukeSource.model.risk.kri.grids.ModelGridPanelRegisterQuantityValuesWeights",
        {
          idQuantityValuesWeights: "id",
          detailWeighingKri: "",
          weighingKri: "",
          description: "",
          weighing: "",
          equivalentFrequency: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterQuantityValuesWeights"
      )[0];
      var panel = general.down("ViewGridPanelRegisterQuantityValuesWeights");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelQuantityValuesWeights);
      editor.startEdit(0, 0);
    },
    _onDeleteQuantityValuesWeights: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterQuantityValuesWeights grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteQuantityValuesWeights.htm?nameView=ViewPanelRegisterQuantityValuesWeights"
      );
    },
    _searchQuantityValuesWeights: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterQuantityValuesWeights grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findQuantityValuesWeights.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridQuantityValuesWeights: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterQuantityValuesWeights grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportQuantityValuesWeightsPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportQuantityValuesWeightsExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onQuantityValuesWeightsAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterQuantityValuesWeights grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditQuantityValuesWeights.htm"
      );
    }
  }
);
