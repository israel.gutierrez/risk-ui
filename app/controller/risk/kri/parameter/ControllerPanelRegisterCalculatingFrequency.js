Ext.define(
  "DukeSource.controller.risk.kri.parameter.ControllerPanelRegisterCalculatingFrequency",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.kri.grids.StoreGridPanelRegisterCalculatingFrequency"],
    models: ["risk.kri.grids.ModelGridPanelRegisterCalculatingFrequency"],
    views: [
      "risk.kri.grids.ViewGridPanelRegisterCalculatingFrequency",
      "risk.kri.parameter.ViewPanelRegisterCalculatingFrequency"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridCalculatingFrequency]": {
          keyup: this._onSearchTriggerGridCalculatingFrequency
        },
        "[action=exportCalculatingFrequencyPdf]": {
          click: this._onExportCalculatingFrequencyPdf
        },
        "[action=exportCalculatingFrequencyExcel]": {
          click: this._onExportCalculatingFrequencyExcel
        },
        "[action=calculatingFrequencyAuditory]": {
          click: this._onCalculatingFrequencyAuditory
        },
        "[action=newCalculatingFrequency]": {
          click: this._newCalculatingFrequency
        },
        "[action=deleteCalculatingFrequency]": {
          click: this._onDeleteCalculatingFrequency
        }
      });
    },
    _newCalculatingFrequency: function() {
      var modelCalculatingFrequency = Ext.create(
        "DukeSource.model.risk.kri.grids.ModelGridPanelRegisterCalculatingFrequency",
        {
          idCalculatingFrequency: "id",
          description: "",
          abbreviation: "",
          state: "S",
          weighing: "",
          equivalentFrequency: ""
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterCalculatingFrequency"
      )[0];
      var panel = general.down("ViewGridPanelRegisterCalculatingFrequency");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelCalculatingFrequency);
      editor.startEdit(0, 0);
    },
    _onDeleteCalculatingFrequency: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterCalculatingFrequency grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteCalculatingFrequency.htm?nameView=ViewPanelRegisterCalculatingFrequency"
      );
    },
    _onSearchTriggerGridCalculatingFrequency: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterCalculatingFrequency grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportCalculatingFrequencyPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportCalculatingFrequencyExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onCalculatingFrequencyAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterCalculatingFrequency grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditCalculatingFrequency.htm"
      );
    }
  }
);
