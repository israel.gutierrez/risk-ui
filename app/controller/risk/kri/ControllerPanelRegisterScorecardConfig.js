Ext.define(
  "DukeSource.controller.risk.kri.ControllerPanelRegisterScorecardConfig",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: ["risk.kri.combos.ViewComboScorecardMaster"],
    init: function() {
      this.control({
        "[action=entryScoreCardConfig]": {
          click: this._onEntryScoreCardConfig
        },
        "[action=saveScoreCardConfig]": {
          click: this._onSaveScoreCardConfig
        },
        "[action=cancelScoreCardConfig]": {
          click: this._onCancelScoreCardConfig
        },

        "[action=hiddenComboxTrue]": {
          click: this._onHiddenComboxTrue
        },
        "[action=hiddenComboxFalse]": {
          click: this._onHiddenComboxFalse
        }
      });
    },

    _onHiddenComboxTrue: function(button) {
      var panel = button.up("ViewPanelRegisterScorecardConfig");
      var grid = panel.down("grid");
      var btSave = panel.down("button[action=saveScoreCardConfig]");
      btSave.setVisible(true);
    },

    _onHiddenComboxFalse: function(button) {
      var panel = button.up("ViewPanelRegisterScorecardConfig");
      var grid = panel.down("grid");
      var btSave = panel.down("button[action=saveScoreCardConfig]");
      btSave.setVisible(false);
    },

    _onSaveScoreCardConfig: function(button) {
      var panel = button.up("ViewPanelRegisterScorecardConfig");
      var grid = panel.down("grid");
      var arrayDataToSave = new Array();
      var records = grid.getStore().getRange();
      Ext.Array.each(records, function(record, index, countriesItSelf) {
        arrayDataToSave.push(record.data);
      });
      if (
        panel
          .down("form[region=north]")
          .getForm()
          .isValid()
      ) {
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveDetailScorecard.htm?nameView=ViewPanelRegisterScorecardConfig",
          params: {
            jsonData: Ext.JSON.encode(arrayDataToSave),
            descriptionScorecard: panel
              .down("ViewComboScorecardMaster")
              .getDisplayValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                response.mensaje,
                Ext.Msg.INFO
              );
              var data = JSON.parse(response.data);
              var evaluation = JSON.parse(response.evaluation);
              panel
                .down("UpperCaseTextFieldReadOnly[name=resultNormalized]")
                .setValue(evaluation.evaluationResult);
              DukeSource.global.DirtyView.colorToElement(
                panel.down("UpperCaseTextFieldReadOnly[name=resultNormalized]"),
                evaluation.levelColour
              );
              panel
                .down("UpperCaseTextFieldReadOnly[name=descriptionResult]")
                .setValue(evaluation.descriptionQualificationKri);
              DukeSource.global.DirtyView.colorToElement(
                panel.down(
                  "UpperCaseTextFieldReadOnly[name=descriptionResult]"
                ),
                evaluation.levelColour
              );
              grid.store.loadRawData(data, false);
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onCancelScoreCardConfig: function(button) {
      var panel = button.up("ViewPanelRegisterScorecardConfig");
      var grid = panel.down("grid");

      var btNew = panel.down("button[action=entryScoreCardConfig]");
      btNew.setVisible(true);

      var btSave = panel.down("button[action=saveScoreCardConfig]");
      btSave.setVisible(false);

      var btCancel = panel.down("button[action=cancelScoreCardConfig]");
      btCancel.setVisible(false);

      var comboScoreMaster = panel.down("ViewComboScorecardMaster");
      comboScoreMaster.setVisible(true);
      panel
        .down("UpperCaseTextFieldObligatory[name=descriptionScorecard]")
        .setVisible(false);
      var store = grid.getStore();
      Ext.Ajax.request({
        method: "POST",
        url:
          "http://localhost:9000/giro/buildJsonDetailScoreCardByIdActual.htm",
        params: {},
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            var fields = JSON.parse(response.fields);
            var columns = JSON.parse(response.columns);
            var data = JSON.parse(response.data);
            store.model.setFields(fields);
            grid.reconfigure(store, columns);
            store.removeAll();
            store.loadRawData(data, true);
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },
    _onEntryScoreCardConfig: function(button) {
      var panel = button.up("ViewPanelRegisterScorecardConfig");
      var grid = panel.down("grid");
      Ext.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/loadGridDetailScoreCard.htm",
        params: {},
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            var store = grid.getStore();
            var fields = JSON.parse(response.fields);
            var columns = JSON.parse(response.columns);
            var data = JSON.parse(response.data);
            store.model.setFields(fields);
            grid.reconfigure(store, columns);
            store.removeAll();
            store.loadRawData(data, true);

            var comboScoreMaster = panel.down("ViewComboScorecardMaster");
            comboScoreMaster.setVisible(false);
            panel
              .down("UpperCaseTextFieldObligatory[name=descriptionScorecard]")
              .setVisible(true);

            var btNew = panel.down("button[action=entryScoreCardConfig]");
            btNew.setVisible(false);

            var btSave = panel.down("button[action=saveScoreCardConfig]");
            btSave.setVisible(true);

            var btCancel = panel.down("button[action=cancelScoreCardConfig]");
            btCancel.setVisible(true);
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    }
  }
);
