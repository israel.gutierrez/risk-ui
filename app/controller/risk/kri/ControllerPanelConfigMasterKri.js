Ext.define("DukeSource.controller.risk.kri.ControllerPanelConfigMasterKri", {
  extend: "Ext.app.Controller",
  stores: [],
  models: [],
  views: [
    "risk.kri.combos.ViewComboCalculationFrequency",
    "risk.parameter.combos.ViewComboWorkArea",
    "risk.kri.combos.ViewComboKriIndicator",
    "risk.kri.combos.ViewComboKriCoverage",
    "risk.kri.combos.ViewComboKriEvaluationMode",
    "risk.kri.combos.ViewComboKriMeasureUnit",
    "risk.kri.combos.ViewComboCriteriaEvaluationKri",
    "risk.parameter.combos.ViewComboJobPlace",
    "risk.parameter.combos.ViewComboActivity",
    "risk.util.search.AdvancedSearchRisk",
    "risk.util.search.AdvancedSearchUser",
    "risk.User.combos.ViewComboAgency"
  ],
  init: function() {
    this.control({
      "[action=saveConfigurationMasterKri]": {
        click: this._onSaveConfigurationMasterKri
      },
      "[action=saveDetailCriteriaKri]": {
        click: this._onSaveDetailCriteriaKri
      },
      "[action=deleteDetailCriteriaKri]": {
        click: this._onDeleteDetailCriteriaKri
      },
      "[action=saveWeighingKri]": {
        click: this._onSaveWeighingKri
      },
      "[action=saveDetailWeighingKri]": {
        click: this._onSaveDetailWeighingKri
      },
      "[action=modifyEntryKri]": {
        click: this._onModifyEntryKri
      },
      "[action=deleteEntryKri]": {
        click: this._onDeleteEntryKri
      },
      "[action=configDetailKri]": {
        click: this._onConfigDetailKri
      },
      "[action=deleteWeighingKri]": {
        click: this._onDeleteWeighingKri
      },
      "[action=goEvaluationKri]": {
        click: this._onGoEvaluationKri
      },
      "[action=modifyWeighingKri]": {
        click: this._onModifyWeighingKri
      },
      "[action=searchTriggerConfigMasterKri]": {
        keyup: this._onSearchTriggerConfigMasterKri
      },
      "[action=searchConfigMasterKri]": {
        specialkey: this._onSearchConfigMasterKri
      },
      "[action=auditoryEntryKri]": {
        click: this._onAuditoryEntryKri
      },
      "[action=showProfileKri]": {
        click: this._onShowProfileKri
      },
      "[action=saveKriUserAssign]": {
        click: this._onSaveKriUserAssign
      },
      "[action=deleteKriUserAssign]": {
        click: this._onDeleteKriUserAssign
      },
      "[action=auditKriUserAssign]": {
        click: this._onAuditKriUserAssign
      }
    });
  },
  _onSaveConfigurationMasterKri: function(button) {
    var win = button.up("ViewWindowEntryKri");
    var grid = Ext.ComponentQuery.query(
      "ViewPanelConfigMasterKri grid[name=masterKri]"
    )[0];
    var gridQualification = win.down("grid");
    var indicatorEval = win.down("#indicatorEvaluation").getValue() === "S";
    var newLimit = win.down("#newLimit").getValue() === "S";
    var isGridModified =
      gridQualification.store.getModifiedRecords().length > 0;

    if (isGridModified && indicatorEval && !newLimit) {
      DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_KRI_EXIST_LIMIT);
    } else {
      var arrayDataToSave = [];
      var records = gridQualification.getStore().getRange();
      Ext.Array.each(records, function(record, index) {
        arrayDataToSave.push(record.data);
      });
      if (
        win
          .down("form")
          .getForm()
          .isValid()
      ) {
        DukeSource.lib.Ajax.request({
          waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
          method: "POST",
          url:
            "http://localhost:9000/giro/saveKeyRiskIndicator.htm?nameView=ViewPanelConfigMasterKri",
          params: {
            jsonData: Ext.JSON.encode(win.down("form").getValues()),
            jsonDataGrid: Ext.JSON.encode(arrayDataToSave),
            newLimit: win.down("#newLimit").getValue(),
            isModified: isGridModified ? "S" : "N"
          },
          scope: this,
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              DukeSource.global.DirtyView.messageNormal(response.message);

              grid.store.getProxy().extraParams = {
                propertyOrder: "idKeyRiskIndicator"
              };
              grid.store.getProxy().url =
                "http://localhost:9000/giro/showListKeyRiskIndicatorActives.htm";
              grid.down("pagingtoolbar").doRefresh();
              win.doClose();
            } else {
              DukeSource.global.DirtyView.messageWarning(response.message);
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
      }
    }
  },
  _onSaveDetailCriteriaKri: function(btn) {
    var win = btn.up("window");
    var recordKri = win.record;

    var grid = win.down("grid[name=detailCriteriaKri]");
    var arrayDataToSave = [];
    var records = grid.getStore().getRange();
    Ext.Array.each(records, function(record, index, countriesItSelf) {
      arrayDataToSave.push(record.data);
    });

    DukeSource.lib.Ajax.request({
      waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
      method: "POST",
      url:
        "http://localhost:9000/giro/saveNormalizerKri.htm?nameView=ViewPanelConfigMasterKri",
      params: {
        jsonData: Ext.JSON.encode(arrayDataToSave),
        idKeyRiskIndicator: recordKri.get("idKeyRiskIndicator")
      },
      success: function(response) {
        response = Ext.decode(response.responseText);
        if (response.success) {
          DukeSource.global.DirtyView.messageNormal(response.message);

          grid.store.getProxy().extraParams = {
            idKeyRiskIndicator: recordKri.get("idKeyRiskIndicator")
          };
          grid.store.getProxy().url =
            "http://localhost:9000/giro/getListNormalizerKri.htm";
          grid.down("pagingtoolbar").doRefresh();
        } else {
          DukeSource.global.DirtyView.messageWarning(response.message);
        }
      },
      failure: function() {}
    });
  },
  _onDeleteDetailCriteriaKri: function(btn) {
    var win = btn.up("window");
    var recordKri = win.record;

    var grid = win.down("grid[name=detailCriteriaKri]");
    var arrayDataToSave = [];
    var records = grid.getSelectionModel().getSelection();
    Ext.Array.each(records, function(record, index, countriesItSelf) {
      arrayDataToSave.push(record.data);
    });

    if (records.length === 0) {
      DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
    } else {
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
        msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
        icon: Ext.Msg.QUESTION,
        buttonText: {
          yes: "Si"
        },
        buttons: Ext.MessageBox.YESNO,
        fn: function(btn) {
          if (btn === "yes") {
            DukeSource.lib.Ajax.request({
              waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
              method: "POST",
              url:
                "http://localhost:9000/giro/deleteListNormalizerKri.htm?nameView=ViewPanelConfigMasterKri",
              params: {
                jsonData: Ext.JSON.encode(arrayDataToSave),
                idKeyRiskIndicator: recordKri.get("idKeyRiskIndicator")
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageNormal(response.message);
                  grid.store.getProxy().extraParams = {
                    idKeyRiskIndicator: recordKri.get("idKeyRiskIndicator")
                  };
                  grid.store.getProxy().url =
                    "http://localhost:9000/giro/getListNormalizerKri.htm";
                  grid.down("pagingtoolbar").doRefresh();
                } else {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              },
              failure: function() {}
            });
          }
        }
      });
    }
  },
  _onSaveWeighingKri: function(button) {
    var panel = button.up("ViewWindowWeighingMasterKri");
    var grid = Ext.ComponentQuery.query(
      "WindowDetailConfigKri grid[name=detailWeighingKri]"
    )[0];
    var success = function(response) {
      response = Ext.decode(response.responseText);
      if (response.success) {
        DukeSource.global.DirtyView.messageNormal(response.message);

        grid.store.getProxy().extraParams = {
          idKeyRiskIndicator: panel
            .down("textfield[name=idKeyRiskIndicator]")
            .getValue()
        };
        grid.store.getProxy().url =
          "http://localhost:9000/giro/findWeighingKri.htm";
        grid.down("pagingtoolbar").moveFirst();
        panel.close();
      } else {
        DukeSource.global.DirtyView.messageWarning(response.message);
      }
    };
    DukeSource.global.DirtyView.saveDataToForm(
      panel,
      "http://localhost:9000/giro/saveWeighingKri.htm?nameView=ViewPanelConfigMasterKri",
      success
    );
  },
  _onSaveDetailWeighingKri: function(button) {
    var panel = button.up("ViewWindowDetailWeighingMasterKri");
    var grid = panel.down("grid");

    var recordKri = Ext.ComponentQuery.query("WindowDetailConfigKri")[0].record;

    var arrayDataToSave = [];
    var records = grid.getStore().getRange();
    Ext.Array.each(records, function(record, index, countriesItSelf) {
      arrayDataToSave.push(record.data);
    });
    Ext.Ajax.request({
      waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
      method: "POST",
      url:
        "http://localhost:9000/giro/saveQuantityValuesWeights.htm?nameView=ViewPanelConfigMasterKri",
      params: {
        form: Ext.JSON.encode(panel.down("form").getValues()),
        jsonData: Ext.JSON.encode(arrayDataToSave)
      },
      success: function(response) {
        response = Ext.decode(response.responseText);
        if (response.success) {
          DukeSource.global.DirtyView.messageAlert(
            DukeSource.global.GiroMessages.TITLE_MESSAGE,
            response.message,
            Ext.Msg.INFO
          );
          grid.store.getProxy().extraParams = {
            idKeyRiskIndicator: recordKri.get("idKeyRiskIndicator")
          };
          grid.store.getProxy().url =
            "http://localhost:9000/giro/findWeighingKri.htm";
          grid.down("pagingtoolbar").moveFirst();
          panel.close();
        } else {
          DukeSource.global.DirtyView.messageAlert(
            DukeSource.global.GiroMessages.TITLE_ERROR,
            response.message,
            Ext.Msg.ERROR
          );
        }
      },
      failure: function() {}
    });
  },
  _onModifyEntryKri: function(rec) {
    var panel = Ext.ComponentQuery.query("ViewPanelConfigMasterKri")[0];
    var grid = panel.down("#masterGridKri");
    var row =
      rec === undefined ? grid.getSelectionModel().getSelection()[0] : rec;

    if (row === undefined) {
      DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
    } else {
      if (Ext.ComponentQuery.query("ViewWindowEntryKri")[0] === undefined) {
        var win = Ext.create(
          "DukeSource.view.risk.kri.windows.ViewWindowEntryKri",
          {
            modal: true,
            actionType: "modify"
          }
        );
        win.show(undefined, function() {
          win
            .down("form")
            .getForm()
            .loadRecord(row);
          win
            .down("grid")
            .getStore()
            .load({
              url: "http://localhost:9000/giro/findQualificationEachKri.htm",
              params: {
                propertyFind: "keyRiskIndicator.idKeyRiskIndicator",
                valueFind: row.get("idKeyRiskIndicator"),
                propertyOrder: "rangeInf"
              }
            });
        });
      }
    }
  },
  _onDeleteEntryKri: function() {
    var grid = Ext.ComponentQuery.query(
      "ViewPanelConfigMasterKri grid[name=masterKri]"
    )[0];
    var row = grid.getSelectionModel().getSelection()[0];

    if (
      row.get("indicatorEvaluation") === DukeSource.global.GiroConstants.YES
    ) {
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
        msg: DukeSource.global.GiroMessages.MESSAGE_KRI_EVALUATION_EXIST,
        icon: Ext.Msg.WARNING,
        buttonText: { yes: "Si" },
        buttons: Ext.MessageBox.YESNO,
        fn: function(n) {
          if (n === "yes") {
            DukeSource.global.DirtyView.deleteElementToGrid(
              grid,
              "http://localhost:9000/giro/deleteKeyRiskIndicator.htm?nameView=ViewPanelConfigMasterKri"
            );
          }
        }
      });
    } else {
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteKeyRiskIndicator.htm?nameView=ViewPanelConfigMasterKri"
      );
    }
  },
  _onConfigDetailKri: function(btn) {
    var panel = Ext.ComponentQuery.query("ViewPanelConfigMasterKri")[0];
    var gridParent = panel.down("grid");
    var record = gridParent.getSelectionModel().getSelection()[0];

    if (record === undefined) {
      DukeSource.global.DirtyView.messageAlert(
        DukeSource.global.GiroMessages.TITLE_WARNING,
        DukeSource.global.GiroMessages.MESSAGE_ITEM,
        Ext.Msg.WARNING
      );
    } else {
      var win = Ext.create(
        "DukeSource.view.risk.kri.windows.WindowDetailConfigKri",
        {
          record: record
        }
      );
      win.show();
      win.down("#nameKri").setValue(record.get("nameKri"));
      win.down("#codeKri").setValue(record.get("codeKri"));

      var grid = win.down("grid[name=detailCriteriaKri]");
      grid.store.getProxy().extraParams = {
        idKeyRiskIndicator: record.get("idKeyRiskIndicator")
      };
      grid.store.getProxy().url =
        "http://localhost:9000/giro/getListNormalizerKri.htm";
      grid.getStore().load({
        scope: this
      });
      var grid1 = win.down("grid[name=detailWeighingKri]");
      grid1.store.getProxy().extraParams = {
        idKeyRiskIndicator: record.get("idKeyRiskIndicator")
      };
      grid1.store.getProxy().url =
        "http://localhost:9000/giro/findWeighingKri.htm";
      grid1.down("pagingtoolbar").moveFirst();
    }
  },

  _onDeleteWeighingKri: function() {
    var grid = Ext.ComponentQuery.query(
      "WindowDetailConfigKri grid[name=detailWeighingKri]"
    )[0];
    DukeSource.global.DirtyView.deleteElementToGrid(
      grid,
      "http://localhost:9000/giro/deleteWeighingKri.htm?nameView=ViewPanelConfigMasterKri"
    );
  },
  _onModifyWeighingKri: function() {
    var win = Ext.ComponentQuery.query("WindowDetailConfigKri")[0];
    var grid = win.down("grid[name=detailWeighingKri]");
    var record = grid.getSelectionModel().getSelection()[0];

    if (grid.getSelectionModel().getCount() === 0) {
      DukeSource.global.DirtyView.messageAlert(
        DukeSource.global.GiroMessages.TITLE_WARNING,
        DukeSource.global.GiroMessages.MESSAGE_ITEM,
        Ext.Msg.WARNING
      );
    } else {
      var row = win.record;
      if (row.get("modeEvaluation") === "CUALITATIVO") {
        if (
          Ext.ComponentQuery.query("ViewWindowDetailWeighingMasterKri")[0] ===
          undefined
        ) {
          var windows = Ext.create(
            "DukeSource.view.risk.kri.windows.ViewWindowDetailWeighingMasterKri",
            {}
          );
          windows
            .down("form")
            .getForm()
            .loadRecord(record);
          windows
            .down("grid")
            .getStore()
            .load({
              url:
                "http://localhost:9000/giro/getListQuantityValuesByWeights.htm",
              params: {
                idWeighingKri: record.get("idWeighingKri")
              }
            });
          windows.show();
        }
      } else if (row.get("modeEvaluation") === "CUANTITATIVO") {
        if (
          Ext.ComponentQuery.query("ViewWindowWeighingMasterKri")[0] ===
          undefined
        ) {
          var window = Ext.create(
            "DukeSource.view.risk.kri.windows.ViewWindowWeighingMasterKri",
            {}
          );
          window.down("form").add({
            xtype: "textfield",
            anchor: "100%",
            hidden: true,
            readOnly: true,
            fieldCls: "obligatoryTextField",
            name: "typeColumn"
          });
          window
            .down("form")
            .getForm()
            .loadRecord(record);
          window.show();
        }
      }
    }
  },
  _onSearchTriggerConfigMasterKri: function(text) {
    var grid = Ext.ComponentQuery.query(
      "ViewPanelConfigMasterKri grid[name=masterKri]"
    )[0];
    DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
  },
  _onSearchConfigMasterKri: function(field, e) {
    if (e.getKey() === e.ENTER) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelConfigMasterKri grid[name=masterKri]"
      )[0];
      DukeSource.global.DirtyView.searchPaginationGridToEnter(
        field,
        grid,
        grid.down("pagingtoolbar"),
        "http://localhost:9000/giro/findKeyRiskIndicator.htm",
        "nameKri",
        "description"
      );
    } else {
    }
  },
  _onAuditoryEntryKri: function(btn) {
    var grid = btn.up("panel").down("#masterGridKri");
    DukeSource.global.DirtyView.showWindowAuditory(
      grid,
      "http://localhost:9000/giro/findAuditKeyRiskIndicator.htm"
    );
  },
  _onSaveKriUserAssign: function(btn) {
    var win = btn.up("window");

    var records = win
      .down("grid")
      .getSelectionModel()
      .getSelection();
    var arrayDataToSave = [];
    Ext.Array.each(records, function(record) {
      arrayDataToSave.push(record.data);
    });

    Ext.Ajax.request({
      method: "POST",
      url:
        "http://localhost:9000/giro/saveKriUserAssign.htm?nameView=ViewPanelConfigMasterKri",
      params: {
        jsonData: Ext.JSON.encode(arrayDataToSave),
        idKri: win.parentView.record.get("idKeyRiskIndicator")
      },
      success: function(response) {
        response = Ext.decode(response.responseText);
        if (response.success) {
          win.parentView
            .down("grid")
            .down("pagingtoolbar")
            .moveFirst();
          DukeSource.global.DirtyView.messageAlert(
            DukeSource.global.GiroMessages.TITLE_MESSAGE,
            response.message,
            Ext.Msg.INFO
          );
          win.close();
        } else {
          DukeSource.global.DirtyView.messageAlert(
            DukeSource.global.GiroMessages.TITLE_WARNING,
            response.message,
            Ext.Msg.WARNING
          );
        }
      },
      failure: function() {}
    });
  },

  _onDeleteKriUserAssign: function(btn) {
    var grid = btn.up("window").down("grid");
    DukeSource.global.DirtyView.deleteElementToGrid(
      grid,
      "http://localhost:9000/giro/deleteKriUserAssign.htm"
    );
  },

  _onGoEvaluationKri: function(btn) {
    var gridMaster = btn.up("panel").down("grid");
    var row = gridMaster.getSelectionModel().getSelection()[0];

    if (row === undefined) {
      DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
    } else if (
      row.get("indicatorConfiguration") === DukeSource.global.GiroConstants.NO
    ) {
      DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_KRI_INCOMPLETE);
    } else {
      var nameView =
        row.get("indicatorKri") === "RIESGO"
          ? "PanelEvaluationKriRisk"
          : "ViewPanelRegisterEvaluationKri";
      var nameController =
        row.get("indicatorKri") === "RIESGO"
          ? "ControllerPanelEvaluationKriRisk"
          : "ControllerPanelRegisterEvaluationKri";

      DukeSource.global.DirtyView.verifyLoadController(
        "DukeSource.controller.risk.kri." + nameController
      );

      var panelEvaluation = Ext.ComponentQuery.query(
        "PanelEvaluationKriRisk"
      )[0];

      if (panelEvaluation === undefined) {
        var panel = Ext.create("DukeSource.view.risk.kri." + nameView, {
          title: "EVALUACIÓN KRI",
          closable: true,
          rowMaster: row,
          border: false
        });
        if (row.get("indicatorKri") === "PERFORMANCE") {
          var grid = panel.down("grid");

          grid.store.proxy.url =
            "http://localhost:9000/giro/getEvaluationsActivesForKri.htm";
          grid.store.proxy.extraParams = {
            idKri: row.get("idKeyRiskIndicator")
          };
          grid.down("pagingtoolbar").doRefresh();
        }
        DukeSource.getApplication().centerPanel.addPanel(panel);
        panel
          .down("form")
          .getForm()
          .loadRecord(row);
      } else {
        DukeSource.getApplication().centerPanel.addPanel(panelEvaluation);
      }
    }
  },

  _onAuditKriUserAssign: function(btn) {
    var grid = btn.up("window").down("grid");
    DukeSource.global.DirtyView.showAuditory(
      grid,
      "http://localhost:9000/giro/showAuditKriUserAssign.htm"
    );
  },
  _onShowProfileKri: function(btn) {
    var gridMaster = btn.up("panel").down("grid");
    var row = gridMaster.getSelectionModel().getSelection()[0];

    if (row === undefined) {
      DukeSource.global.DirtyView.messageAlert(
        DukeSource.global.GiroMessages.TITLE_WARNING,
        DukeSource.global.GiroMessages.MESSAGE_ITEM,
        Ext.Msg.WARNING
      );
    } else {
      var win = Ext.create("Ext.window.Window", {
        layout: "fit",
        maximized: true,
        modal: true,
        buttons: [
          {
            text: "Salir",
            scale: "medium",
            handler: function(btn) {
              btn.up("window").close();
            },
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      }).show();
      win.removeAll();
      win.add({
        xtype: "component",
        autoEl: {
          tag: "iframe",
          src:
            "http://localhost:9000/giro/pdfGeneralReport.htm?values=" +
            row.get("idKeyRiskIndicator") +
            "&names=" +
            "idKri" +
            "&types=" +
            "String" +
            "&nameReport=" +
            nameReport("ProfileKRI") +
            "&nameDownload=Ficha_KRI"
        }
      });
    }
  }
});
