Ext.define("DukeSource.controller.risk.kri.ControllerPanelReportsKri", {
  extend: "Ext.app.Controller",
  stores: [],
  models: [],
  views: [],
  init: function() {
    this.control({
      "[action=generateReportsKri]": {
        click: this._onGenerateReportsKri
      }
    });
  },
  _onGenerateReportsKri: function(btn) {
    var form = btn.up("form");
    if (form.getForm().isValid()) {
      var view = Ext.ComponentQuery.query("ViewPanelReportsKri")[0];

      var nameDownload = view.down("#nameFile").getValue();
      var nameReport = view.down("#nameReport").getValue();
      var year =
        form.down("#year").getValue() === null
          ? DukeSource.global.GiroConstants.TEXT_T
          : form.down("#year").getValue();

      var dateInit =
        form.down("#dateInit").getRawValue() === ""
          ? DukeSource.global.GiroConstants.DATE_INIT
          : form.down("#dateInit").getRawValue();
      var dateEnd =
        form.down("#dateEnd").getRawValue() === ""
          ? DukeSource.global.GiroConstants.DATE_END
          : form.down("#dateEnd").getRawValue();

      var containerReport = view.down("#containerReport");

      containerReport.removeAll();
      containerReport.add({
        xtype: "component",
        autoEl: {
          tag: "iframe",
          hidden: true,
          src:
            "http://localhost:9000/giro/xlsGeneralReport.htm?" +
            "values=" +
            year +
            "," +
            dateInit +
            "," +
            dateEnd +
            "&names=year,dateInit,dateEnd" +
            "&types=String,Date,Date" +
            "&nameReport=" +
            nameReport +
            "&nameDownload=" +
            nameDownload
        }
      });
    } else {
      DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
    }
  }
});
