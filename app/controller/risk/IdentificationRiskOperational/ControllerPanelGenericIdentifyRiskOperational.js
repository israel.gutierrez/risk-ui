Ext.define(
  "DukeSource.controller.risk.IdentificationRiskOperational.ControllerPanelGenericIdentifyRiskOperational",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [],

    init: function() {
      this.control({
        "[action=searchAdvancedIdentification]": {
          click: this._searchAdvancedIdentification
        },
        "[action=deleteIdentificationRisk]": {
          click: this._deleteIdentificationRisk
        },
        "[action=evaluateIdentificationRisk]": {
          click: this._evaluateIdentificationRisk
        },
        "[action=saveEvaluationIdentificationRisk]": {
          click: this._saveEvaluationIdentificationRisk
        },
        "[action=approveIdentification]": {
          click: this._approveIdentification
        },
        "ViewPanelGenericIdentifyRiskOperational grid": {
          //  itemcontextmenu: this._rightClickOptions
        }
      });
    },
    _searchAdvancedIdentification: function(btn) {
      if (
        btn
          .up("form")
          .getForm()
          .isValid()
      ) {
        var form = btn.up("form");
        var limit = ",";

        var userRegister =
          form.down("#idUser").getRawValue() === undefined
            ? ""
            : form.down("#idUser").getRawValue();
        var fullNameRegister =
          form.down("#nameUser").getValue() === undefined
            ? ""
            : form.down("#nameUser").getValue();
        var dateEvaluation =
          form.down("#dateEvaluationInit").getRawValue() === undefined
            ? ""
            : form.down("#dateEvaluationInit").getRawValue();
        var stateIdentification =
          form.down("#stateIdentification").getValue() === undefined
            ? ""
            : form.down("#stateIdentification").getValue();
        var grid = Ext.ComponentQuery.query(
          "ViewPanelGenericIdentifyRiskOperational grid"
        )[0];
        grid.store.getProxy().extraParams = {
          fields:
            "ur.id" +
            limit +
            "ur.fullName" +
            limit +
            "ir.dateEvaluation" +
            limit +
            "ir.stateIdentification",
          values:
            userRegister +
            limit +
            fullNameRegister +
            limit +
            dateEvaluation +
            limit +
            stateIdentification +
            limit,
          types:
            "String" + limit + "String" + limit + "Date" + limit + "String",
          operators: "equal" + limit + "like" + limit + "equal" + limit + "like"
        };
        grid.store.getProxy().url =
          "http://localhost:9000/giro/advancedSearchIdentificationRisk.htm";
        grid.down("pagingtoolbar").moveFirst();
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_ADVERTENCIA,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETAR,
          Ext.Msg.ERROR
        );
      }
    },
    _deleteIdentificationRisk: function(btn) {
      var panel = Ext.ComponentQuery.query(
        "ViewPanelGenericIdentifyRiskOperational"
      )[0];
      var grid = panel.down("grid");
      var row = grid.getSelectionModel().getSelection()[0];
      if (row != undefined) {
        Ext.MessageBox.show({
          title: DukeSource.global.GiroMessages.TITLE_WARNING,
          msg: "Estas seguro que desea eliminar esta identificación?",
          icon: Ext.Msg.WARNING,
          buttonText: {
            yes: "Si",
            no: "No"
          },
          buttons: Ext.MessageBox.YESNO,
          fn: function(btn) {
            if (btn == "yes") {
              DukeSource.lib.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/deleteIdentificationRisk.htm?nameView=ViewPanelGenericIdentifyRiskOperational",
                params: {
                  jsonData: Ext.JSON.encode(row.raw)
                },
                success: function(response) {
                  var response = Ext.decode(response.responseText);
                  if (response.success) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      response.message,
                      Ext.Msg.INFO
                    );
                    grid.down("pagingtoolbar").moveFirst();
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      response.message,
                      Ext.Msg.WARNING
                    );
                  }
                },
                failure: function() {}
              });
            } else {
            }
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_ADVERTENCIA,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      }
    },
    _evaluateIdentificationRisk: function(btn) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelGenericIdentifyRiskOperational"
      )[0].down("grid");
      var rows = grid.getSelectionModel().getSelection();
      var havePending = false;
      Ext.Array.each(rows, function(record) {
        if (
          record.get("stateIdentification") === "R" ||
          record.get("checkApprove") === DukeSource.global.GiroConstants.NO
        ) {
          havePending = true;
        }
      });
      if (
        Ext.ComponentQuery.query("ViewWindowRegisterRisk")[0] === undefined &&
        !havePending
      ) {
        var window = Ext.create(
          "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowRegisterRisk",
          {
            link: "saveRisk",
            width: 900
          }
        );
        window.down("textfield[name=idRisk]").setValue("id");
        window.show();
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          "No se pueden evaluar identificaciones pendientes de APROBACION <br>" +
            " o se encuentren EVALUADAS ",
          Ext.Msg.WARNING
        );
      }
    },
    _saveEvaluationIdentificationRisk: function(btn) {
      var win = btn.up("window");
      var form = win.down("form");
      var grid = Ext.ComponentQuery.query(
        "ViewPanelGenericIdentifyRiskOperational"
      )[0].down("grid");
      var rows = grid.getSelectionModel().getSelection();

      var arrayDataToSave = [];
      Ext.Array.each(rows, function(record) {
        arrayDataToSave.push(record.data);
      });

      if (form.getForm().isValid()) {
        DukeSource.lib.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveEvaluationIdentificationRisk.htm?nameView=ViewPanelGenericIdentifyRiskOperational",
          params: {
            jsonData: Ext.JSON.encode(form.getForm().getValues()),
            identifications: Ext.JSON.encode(arrayDataToSave)
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              win.close();
              grid.down("pagingtoolbar").moveFirst();

              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                response.message,
                Ext.Msg.INFO
              );
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.message,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function(response) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.message,
              Ext.Msg.ERROR
            );
          }
        });
      }
    },
    _rightClickOptions: function(view, rec, node, index, e) {
      e.stopEvent();
      var addMenu = Ext.create("DukeSource.view.fulfillment.AddMenu", {});
      addMenu.removeAll();
      addMenu.add({
        text: "Historico de Identificaciones",
        iconCls: "fileAttach",
        handler: function() {
          var win = Ext.create(
            "DukeSource.view.risk.IdentificationRiskOperational.windows.ViewDetailIdentifyRiskOperational",
            {
              modal: false,
              width: 900,
              height: 500
            }
          );
          win.show();
          var grid = win.down("grid");
          grid.store.proxy.url =
            "http://localhost:9000/giro/showHistoricIdentificationRisk.htm?nameView=ViewPanelGenericIdentifyRiskOperational";
          grid.store.proxy.extraParams = {
            idIdentification: rec.get("id")
          };
          grid.down("pagingtoolbar").moveFirst();
        }
      });
      addMenu.showAt(e.getXY());
    },
    _approveIdentification: function(btn) {
      var win = btn.up("window");
      var form = win.down("form").getForm();

      DukeSource.lib.Ajax.request({
        waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
        method: "POST",
        url: "http://localhost:9000/giro/approveIdentification.htm",
        params: {
          jsonData: Ext.JSON.encode(form.getValues())
        },
        scope: this,
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              response.message,
              Ext.Msg.INFO
            );
            win.close();

            var windows = Ext.ComponentQuery.query(
              "ViewWindowIdentifyRiskOperational"
            )[0];
            windows.close();

            var grid = Ext.ComponentQuery.query(
              "ViewPanelGenericIdentifyRiskOperational grid"
            )[0];
            grid.down("pagingtoolbar").moveFirst();
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              response.message,
              Ext.Msg.WARNING
            );
          }
        },
        failure: function() {}
      });
    }
  }
);
