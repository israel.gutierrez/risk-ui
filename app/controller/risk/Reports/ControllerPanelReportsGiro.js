Ext.define("DukeSource.controller.risk.Reports.ControllerPanelReportsGiro", {
  extend: "Ext.app.Controller",
  stores: ["risk.kri.combos.StoreComboKeyRiskIndicator"],
  models: ["risk.kri.combos.ModelComboKeyRiskIndicator"],
  views: [
    "risk.parameter.combos.ViewComboProcessType",
    "risk.kri.combos.ViewComboKeyRiskIndicator",
    "risk.Reports.ViewPanelMatrixReport",
    "risk.parameter.combos.ViewComboYear",
    "risk.parameter.combos.ViewComboOperationalRiskExposition",
    "risk.parameter.combos.ViewComboTypeMatrix",
    "risk.parameter.combos.ViewComboTypeRiskEvaluation"
  ],
  init: function() {
    this.control({
      "[action=reportGiroXls]": {
        click: this._onReportGiroXls
      },
      "[action=identifiedFullRiskInMatrix]": {
        click: this._onIdentifiedFullRiskInMatrix
      }
    });
  },
  _onReportGiroXls: function(btn) {
    var view = Ext.ComponentQuery.query("ViewPanelReportsGiro")[0];
    var form = view.down("#formParameters");
    var panelReport = view.down("#panelReport");
    if (form.getForm().isValid()) {
      if (form.down("#dateInit") != undefined) {
        var dateInit =
          form.down("#dateInit").getRawValue() == ""
            ? "1990-01-01"
            : form.down("#dateInit").getRawValue();
        var dateEnd =
          form.down("#dateEnd").getRawValue() == ""
            ? "2099-12-31"
            : form.down("#dateEnd").getRawValue();
        var dateInitOccur = "1990-01-01";
        var dateEndOccur = "2099-12-31";
        if (form.down("checkbox").getValue()) {
          dateInitOccur = dateInit;
          dateEndOccur = dateEnd;
          dateInit = "1990-01-01";
          dateEnd = "2099-12-31";
        }

        var value =
          dateInit + "," + dateEnd + "," + dateInitOccur + "," + dateEndOccur;
        panelReport.removeAll();
        panelReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            src:
              btn.typeReport +
              "http://localhost:9000/giro/GeneralReport.htm?values=" +
              value +
              "&names=" +
              "dateInit,dateEnd,dateInitOccur,dateEndOccur" +
              "&types=" +
              "Date,Date,Date,Date" +
              "&nameReport=" +
              btn.nameReport
          }
        });
      } else if (form.down("#idRiskEvaluationMatrix") != undefined) {
        var idEvaluation =
          form.down("#idRiskEvaluationMatrix").getValue() == undefined
            ? "T"
            : form.down("#idRiskEvaluationMatrix").getValue();
        var idProcess =
          form.down("#processType").getValue() == undefined
            ? "T"
            : form.down("#processType").getValue();
        var value = idEvaluation + "," + idProcess;
        panelReport.removeAll();
        panelReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            src:
              btn.typeReport +
              "http://localhost:9000/giro/GeneralReport.htm?values=" +
              value +
              "&names=" +
              "idEvaluation,idProcess" +
              "&types=" +
              "String,String" +
              "&nameReport=" +
              btn.nameReport
          }
        });
      } else if (form.down("ViewComboOperationalRiskExposition") != undefined) {
        var idMaxExp = form
          .down("ViewComboOperationalRiskExposition")
          .getValue();
        var idMatrix = form.down("ViewComboTypeMatrix").getValue();
        var value = idMatrix + "," + idMaxExp;
        panelReport.removeAll();
        panelReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            src:
              btn.typeReport +
              "http://localhost:9000/giro/GeneralReport.htm?values=" +
              value +
              "&names=" +
              "idMatrix,idMaxExp" +
              "&types=" +
              "String,String" +
              "&nameReport=" +
              btn.nameReport
          }
        });
      } else {
        var idProcess =
          form.down("#processType").getValue() == undefined
            ? "T"
            : form.down("#processType").getValue();
        var idKri =
          form.down("ViewComboKeyRiskIndicator").getValue() == undefined
            ? "T"
            : form.down("ViewComboKeyRiskIndicator").getValue();
        var year =
          form.down("ViewComboYear").getValue() == undefined
            ? "0"
            : form.down("ViewComboYear").getValue();
        var value = idProcess + "," + idKri + "," + year;
        panelReport.removeAll();
        panelReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            src:
              btn.typeReport +
              "http://localhost:9000/giro/GeneralReport.htm?values=" +
              value +
              "&names=" +
              "idTypeProcess,idKri,year" +
              "&types=" +
              "String,String,Integer" +
              "&nameReport=" +
              btn.nameReport
          }
        });
      }
    } else {
      DukeSource.global.DirtyView.messageAlert(
        DukeSource.global.GiroMessages.TITLE_WARNING,
        DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
        Ext.Msg.ERROR
      );
    }
  },
  _onIdentifiedFullRiskInMatrix: function(me) {
    var panel = me.up("ViewPanelMatrixReport");
    var form = panel.down("form");
    if (form.getForm().isValid()) {
      var typeMatrix = panel.down("ViewComboTypeMatrix").getValue();
      var businessLineOne = panel.down("#idRiskEvaluationMatrix").getValue();
      var process = panel.down("ViewComboProcessType").getValue();
      var grid = panel.down("grid[name=matrixGeneral]");
      var valueExposition = panel.down("ViewComboOperationalRiskExposition");
      var typeRisk = "ALL";
      Ext.Ajax.request({
        method: "POST",
        url:
          "http://localhost:9000/giro/getMatrixGeneralFilter.htm?nameView=ViewPanelReportsGiro",
        params: {
          maxExposition: panel
            .down("ViewComboOperationalRiskExposition")
            .getValue(),
          valueMaxExposition: valueExposition.getRawValue(),
          typeMatrix: typeMatrix,
          businessLineOne: businessLineOne,
          process: process,
          subProcess: "",
          typeRisk: typeRisk
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          grid.getView().refresh();
          if (response.success) {
            var data = Ext.JSON.decode(response.data);
            for (var i = 0; i < data.length; i++) {
              var valueMatrix = data[i]["valueMatrix"];
              var codeRisk = data[i]["codeRisk"];
              var positionX = data[i]["positionX"] + 1;
              var positionY = data[i]["positionY"];
              var cell = grid
                .getView()
                .getCellByPosition({ row: positionY, column: positionX });
              var value = cell.dom.textContent;
              if ((value = Ext.util.Format.number(valueMatrix, "0,0.00"))) {
                cell.insertHtml(
                  "afterBegin",
                  '<button type="button" title="Inherente de: ' +
                    codeRisk +
                    '" class="buttonRiskInherent"> </button>',
                  true
                );
              }
            }
            var data2 = Ext.JSON.decode(response.data2);
            for (var j = 0; j < data.length; j++) {
              var valueMatrix2 = data2[j]["valueMatrix"];
              var codeRisk2 = data2[j]["codeRisk"];
              var positionX2 = data2[j]["positionX"] + 1;
              var positionY2 = data2[j]["positionY"];
              var cell2 = grid
                .getView()
                .getCellByPosition({ row: positionY2, column: positionX2 });
              var value2 = cell2.dom.textContent;
              if ((value2 = Ext.util.Format.number(valueMatrix2, "0,0.00"))) {
                cell2.insertHtml(
                  "afterBegin",
                  '<button type="button" title="Residual de: ' +
                    codeRisk2 +
                    '" class="buttonRiskResidual">X</button>',
                  true
                );
              }
            }
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    } else {
      DukeSource.global.DirtyView.messageAlert(
        DukeSource.global.GiroMessages.TITLE_WARNING,
        DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
        Ext.Msg.ERROR
      );
    }
  }
});
