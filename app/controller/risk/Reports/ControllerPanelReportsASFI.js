Ext.define("DukeSource.controller.risk.Reports.ControllerPanelReportsASFI", {
  extend: "Ext.app.Controller",
  stores: [],
  models: [],
  views: [],
  init: function() {
    this.control({
      "[action=generateReportASFI]": {
        click: this._onGenerateReport
      }
    });
  },
  _onGenerateReport: function() {
    var view = Ext.ComponentQuery.query("PanelReportsASFI")[0];

    var form = view.down("form");
    var nameDownload = form.down("#nameFile").getValue();
    var nameReport = form.down("#nameReport").getValue();
    var year = form.down("#year").getValue();
    var month = form.down("#month").getValue();
    var previousYear = form.down("#lastYear").getValue();
    var previousMonth = form.down("#lastMonth").getValue();

    var dateInit =
      form.down("#dateInit").getRawValue() === ""
        ? "01/01/1900"
        : form.down("#dateInit").getRawValue();
    var dateEnd =
      form.down("#dateEnd").getRawValue() === ""
        ? "31/12/2999"
        : form.down("#dateEnd").getRawValue();
    var dateReport = form.down("#dateReport").getRawValue();
    var eventState =
      form.down("#eventState").getValue().length === 0
        ? "14,15,16"
        : form.down("#eventState").getValue();

    var userDetailDescriptionBuilder = form
      .down("#userDetailDescriptionBuilder")
      .getValue();
    var userDetailDescriptionReviser = form
      .down("#userDetailDescriptionReviser")
      .getValue();
    var userDetailDescriptionApprove = form
      .down("#userDetailDescriptionApprove")
      .getValue();
    var isCritic =
      form.down("#isCritic").getValue() === null
        ? "T"
        : form.down("#isCritic").getValue();
    var online = form.down("#online").getValue();

    var containerReport = view.down("#containerReport");
    if (form.getForm().isValid()) {
      containerReport.removeAll();
      containerReport.add({
        xtype: "component",
        autoEl: {
          tag: "iframe",
          hidden: true,
          src:
            "http://localhost:9000/giro/generateReportASFI.htm?values=" +
            dateInit +
            "|" +
            dateEnd +
            "|" +
            eventState +
            "|" +
            userDetailDescriptionBuilder +
            "|" +
            userDetailDescriptionReviser +
            "|" +
            userDetailDescriptionApprove +
            "|" +
            isCritic +
            "|" +
            online +
            "|" +
            dateReport +
            "|" +
            year +
            "|" +
            month +
            "|" +
            previousYear +
            "|" +
            previousMonth +
            "&names=" +
            "dateInit|dateEnd|eventState|builder|reviser|approve|isCritic|online|dateReport|year|month|previousYear|previousMonth" +
            "&types=" +
            "Date|Date|String|String|String|String|String|String|Date|Integer|Integer|Integer|Integer" +
            "&nameReport=" +
            nameReport +
            "&nameDownload=" +
            nameDownload
        }
      });
    } else {
      DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
    }
  }
});
