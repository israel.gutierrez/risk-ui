Ext.define(
  "DukeSource.controller.risk.parameter.kri.ControllerPanelRegisterScorecardMaster",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterScorecardMaster"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterScorecardMaster"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterScorecardMaster",
      "risk.parameter.kri.ViewPanelRegisterScorecardMaster"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridScorecardMaster]": {
          keyup: this._onSearchTriggerGridScorecardMaster
        },
        "[action=exportScorecardMasterPdf]": {
          click: this._onExportScorecardMasterPdf
        },
        "[action=exportScorecardMasterExcel]": {
          click: this._onExportScorecardMasterExcel
        },
        "[action=scorecardMasterAuditory]": {
          click: this._onScorecardMasterAuditory
        },
        "[action=newScorecardMaster]": {
          click: this._newScorecardMaster
        },
        "[action=saveScorecardMaster]": {
          click: this._onSaveScorecardMaster
        },
        "[action=deleteScorecardMaster]": {
          click: this._onDeleteScorecardMaster
        },
        "[action=searchScorecardMaster]": {
          specialkey: this._searchScorecardMaster
        },
        "ViewPanelRegisterScorecardMaster grid": {
          itemdblclick: this._onModifyScorecardMaster
        }
      });
    },
    _newScorecardMaster: function() {
      var window = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowScorecardMaster",
        {
          modal: true
        }
      ).show();
    },
    _onSaveScorecardMaster: function() {
      var window = Ext.ComponentQuery.query("ViewWindowScorecardMaster")[0];
      if (
        window
          .down("form")
          .getForm()
          .isValid()
      ) {
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveScorecardMaster.htm?nameView=ViewPanelRegisterScorecardMaster",
          params: {
            jsonData: Ext.JSON.encode(window.down("form").getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              window.close();
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.INFO
              );
              Ext.ComponentQuery.query(
                "ViewPanelRegisterScorecardMaster grid"
              )[0]
                .getStore()
                .load();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        //localhost:9000/giro/deleteScorecardMaster.htm
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onDeleteScorecardMaster: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterScorecardMaster grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteScorecardMaster.htm?nameView=ViewPanelRegisterScorecardMaster"
      );
    },
    _searchScorecardMaster: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterScorecardMaster grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findScorecardMaster.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridScorecardMaster: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterScorecardMaster grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportScorecardMasterPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportScorecardMasterExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onScorecardMasterAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterScorecardMaster grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditScorecardMaster.htm"
      );
    },
    _onModifyScorecardMaster: function(grid) {
      var window = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowScorecardMaster",
        {
          modal: true
        }
      ).show();
      window
        .down("form")
        .getForm()
        .loadRecord(grid.getSelectionModel().getSelection()[0]);
    }
  }
);
