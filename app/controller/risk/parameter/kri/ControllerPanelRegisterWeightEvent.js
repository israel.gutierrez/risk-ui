Ext.define(
  "DukeSource.controller.risk.parameter.kri.ControllerPanelRegisterWeightEvent",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterWeightEvent"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterWeightEvent"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterWeightEvent",
      "risk.parameter.kri.ViewPanelRegisterWeightEvent",
      "DukeSource.view.risk.kri.combos.ViewComboScorecardMaster"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridWeightEvent]": {
          keyup: this._onSearchTriggerGridWeightEvent
        },
        "[action=exportWeightEventPdf]": {
          click: this._onExportWeightEventPdf
        },
        "[action=exportWeightEventExcel]": {
          click: this._onExportWeightEventExcel
        },
        "[action=weightEventAuditory]": {
          click: this._onWeightEventAuditory
        },
        "[action=newWeightEvent]": {
          click: this._newWeightEvent
        },
        "[action=saveWeightEvent]": {
          click: this._onSaveWeightEvent
        },
        "[action=deleteWeightEvent]": {
          click: this._onDeleteWeightEvent
        },
        "[action=searchWeightEvent]": {
          specialkey: this._searchWeightEvent
        },
        "ViewPanelRegisterWeightEvent grid": {
          itemdblclick: this._onModifyWeightEvent
        }
      });
    },
    _newWeightEvent: function() {
      var window = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowWeightEvent",
        {
          modal: true
        }
      ).show();
    },
    _onSaveWeightEvent: function() {
      var window = Ext.ComponentQuery.query("ViewWindowWeightEvent")[0];
      if (
        window
          .down("form")
          .getForm()
          .isValid()
      ) {
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/saveWeightEvent.htm",
          params: {
            jsonData: Ext.JSON.encode(window.down("form").getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              window.close();
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.INFO
              );
              Ext.ComponentQuery.query("ViewPanelRegisterWeightEvent grid")[0]
                .getStore()
                .load();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onDeleteWeightEvent: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterWeightEvent grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteWeightEvent.htm"
      );
    },
    _searchWeightEvent: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterWeightEvent grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findWeightEvent.htm",
          "state",
          "state"
        );
      } else {
      }
    },
    _onSearchTriggerGridWeightEvent: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterWeightEvent grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportWeightEventPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportWeightEventExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onWeightEventAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterWeightEvent grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditWeightEvent.htm"
      );
    },
    _onModifyWeightEvent: function(grid) {
      var window = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowWeightEvent",
        {
          modal: true
        }
      ).show();
      window
        .down("form")
        .getForm()
        .loadRecord(grid.getSelectionModel().getSelection()[0]);
    }
  }
);
