Ext.define(
  "DukeSource.controller.risk.parameter.safetyInformation.ControllerPanelRegisterFixedAssets",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.parameter.grids.StoreGridPanelRegisterFixedAssets",
      "risk.parameter.combos.StoreComboTechnicalCustodio"
    ],
    models: [
      "risk.parameter.grids.ModelGridPanelRegisterFixedAssets",
      "risk.parameter.combos.ModelComboTechnicalCustodio"
    ],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterFixedAssets",
      "risk.parameter.safetyInformation.ViewPanelRegisterFixedAssets",
      "risk.parameter.combos.ViewComboJobPlace",
      "risk.parameter.combos.ViewComboConfidentiality",
      "risk.parameter.combos.ViewComboIntegrity",
      "risk.parameter.combos.ViewComboAvailability",
      "risk.parameter.combos.ViewComboWorkArea"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridFixedAssets]": {
          keyup: this._onSearchTriggerGridFixedAssets
        },
        "[action=exportFixedAssetsPdf]": {
          click: this._onExportFixedAssetsPdf
        },
        "[action=exportFixedAssetsExcel]": {
          click: this._onExportFixedAssetsExcel
        },
        "[action=fixedAssetsAuditory]": {
          click: this._onFixedAssetsAuditory
        },
        "[action=saveFixedAssets]": {
          click: this._onSaveFixedAssets
        },
        "[action=newFixedAssets]": {
          click: this._newFixedAssets
        },
        "[action=deleteFixedAssets]": {
          click: this._onDeleteFixedAssets
        },
        "[action=searchFixedAssets]": {
          specialkey: this._searchFixedAssets
        },
        "ViewPanelRegisterFixedAssets grid": {
          itemdblclick: this._onModifyFixedAssets
        }
      });
    },
    _onSaveFixedAssets: function(btn) {
      var panel = btn.up("window");
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterFixedAssets"
      )[0].down("grid");
      if (
        panel
          .down("form")
          .getForm()
          .isValid()
      ) {
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveFixedAssets.htm?nameView=ViewPanelRegisterFixedAssets",
          params: {
            jsonData: Ext.JSON.encode(panel.down("form").getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                response.mensaje,
                Ext.Msg.INFO
              );
              panel.close();
              DukeSource.global.DirtyView.searchPaginationGridNormal(
                "",
                grid,
                grid.down("pagingtoolbar"),
                "http://localhost:9000/giro/findFixedAssets.htm",
                "description",
                "description"
              );
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _newFixedAssets: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowRegisterFixedAssets",
        {
          modal: true
        }
      ).show();
      win.down("#description").focus(false, 200);
    },
    _onDeleteFixedAssets: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterFixedAssets grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteFixedAssets.htm?nameView=ViewPanelRegisterFixedAssets"
      );
    },
    _searchFixedAssets: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterFixedAssets grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findFixedAssets.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridFixedAssets: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterFixedAssets grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportFixedAssetsPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportFixedAssetsExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onFixedAssetsAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterFixedAssets grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditFixedAssets.htm"
      );
    },
    _onModifyFixedAssets: function(grid, record) {
      var window = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowRegisterFixedAssets",
        {
          modal: true
        }
      ).show();
      window
        .down("ViewComboWorkArea")
        .getStore()
        .load();
      window
        .down("ViewComboJobPlace")
        .getStore()
        .load();
      window
        .down("form")
        .getForm()
        .loadRecord(record);
    }
  }
);
