Ext.define(
  "DukeSource.controller.risk.parameter.safetyInformation.ControllerPanelRegisterTechnicalCustodio",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterTechnicalCustodio"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterTechnicalCustodio"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterTechnicalCustodio",
      "risk.parameter.safetyInformation.ViewPanelRegisterTechnicalCustodio"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridTechnicalCustodio]": {
          keyup: this._onSearchTriggerGridTechnicalCustodio
        },
        "[action=exportTechnicalCustodioPdf]": {
          click: this._onExportTechnicalCustodioPdf
        },
        "[action=exportTechnicalCustodioExcel]": {
          click: this._onExportTechnicalCustodioExcel
        },
        "[action=technicalCustodioAuditory]": {
          click: this._onTechnicalCustodioAuditory
        },
        "[action=newTechnicalCustodio]": {
          click: this._newTechnicalCustodio
        },
        "[action=deleteTechnicalCustodio]": {
          click: this._onDeleteTechnicalCustodio
        },
        "[action=searchTechnicalCustodio]": {
          specialkey: this._searchTechnicalCustodio
        }
      });
    },
    _newTechnicalCustodio: function() {
      var modelTechnicalCustodio = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterTechnicalCustodio",
        {
          idTechnicalCustodio: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterTechnicalCustodio"
      )[0];
      var panel = general.down("ViewGridPanelRegisterTechnicalCustodio");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelTechnicalCustodio);
      editor.startEdit(0, 0);
    },
    _onDeleteTechnicalCustodio: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTechnicalCustodio grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteTechnicalCustodio.htm?nameView=ViewPanelRegisterTechnicalCustodio"
      );
    },
    _searchTechnicalCustodio: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterTechnicalCustodio grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findTechnicalCustodio.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridTechnicalCustodio: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTechnicalCustodio grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportTechnicalCustodioPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportTechnicalCustodioExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onTechnicalCustodioAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTechnicalCustodio grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditTechnicalCustodio.htm"
      );
    }
  }
);
