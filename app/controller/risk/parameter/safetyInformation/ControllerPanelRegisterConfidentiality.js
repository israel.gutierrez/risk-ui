Ext.define(
  "DukeSource.controller.risk.parameter.safetyInformation.ControllerPanelRegisterConfidentiality",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterConfidentiality"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterConfidentiality"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterConfidentiality",
      "risk.parameter.safetyInformation.ViewPanelRegisterConfidentiality"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridConfidentiality]": {
          keyup: this._onSearchTriggerGridConfidentiality
        },
        "[action=exportConfidentialityPdf]": {
          click: this._onExportConfidentialityPdf
        },
        "[action=exportConfidentialityExcel]": {
          click: this._onExportConfidentialityExcel
        },
        "[action=confidentialityAuditory]": {
          click: this._onConfidentialityAuditory
        },
        "[action=newConfidentiality]": {
          click: this._newConfidentiality
        },
        "[action=deleteConfidentiality]": {
          click: this._onDeleteConfidentiality
        },
        "[action=searchConfidentiality]": {
          specialkey: this._searchConfidentiality
        }
      });
    },
    _newConfidentiality: function() {
      var modelConfidentiality = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterConfidentiality",
        {
          idConfidentiality: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterConfidentiality"
      )[0];
      var panel = general.down("ViewGridPanelRegisterConfidentiality");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelConfidentiality);
      editor.startEdit(0, 0);
    },
    _onDeleteConfidentiality: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterConfidentiality grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteConfidentiality.htm?nameView=ViewPanelRegisterConfidentiality"
      );
    },
    _searchConfidentiality: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterConfidentiality grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findConfidentiality.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridConfidentiality: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterConfidentiality grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportConfidentialityPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportConfidentialityExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onConfidentialityAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterConfidentiality grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditConfidentiality.htm"
      );
    }
  }
);
