Ext.define(
  "DukeSource.controller.risk.parameter.safetyInformation.ControllerPanelRegisterAvailability",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterAvailability"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterAvailability"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterAvailability",
      "risk.parameter.safetyInformation.ViewPanelRegisterAvailability"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridAvailability]": {
          keyup: this._onSearchTriggerGridAvailability
        },
        "[action=exportAvailabilityPdf]": {
          click: this._onExportAvailabilityPdf
        },
        "[action=exportAvailabilityExcel]": {
          click: this._onExportAvailabilityExcel
        },
        "[action=availabilityAuditory]": {
          click: this._onAvailabilityAuditory
        },
        "[action=newAvailability]": {
          click: this._newAvailability
        },
        "[action=deleteAvailability]": {
          click: this._onDeleteAvailability
        },
        "[action=searchAvailability]": {
          specialkey: this._searchAvailability
        }
      });
    },
    _newAvailability: function() {
      var modelAvailability = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterAvailability",
        {
          idAvailability: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterAvailability"
      )[0];
      var panel = general.down("ViewGridPanelRegisterAvailability");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelAvailability);
      editor.startEdit(0, 0);
    },
    _onDeleteAvailability: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterAvailability grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteAvailability.htm?nameView=ViewPanelRegisterAvailability"
      );
    },
    _searchAvailability: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterAvailability grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findAvailability.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridAvailability: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterAvailability grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportAvailabilityPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportAvailabilityExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onAvailabilityAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterAvailability grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditAvailability.htm"
      );
    }
  }
);
