Ext.define(
  "DukeSource.controller.risk.parameter.safetyInformation.ControllerPanelRegisterReasonSafety",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterReasonSafety"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterReasonSafety"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterReasonSafety",
      "risk.parameter.safetyInformation.ViewPanelRegisterReasonSafety"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridReasonSafety]": {
          keyup: this._onSearchTriggerGridReasonSafety
        },
        "[action=exportReasonSafetyPdf]": {
          click: this._onExportReasonSafetyPdf
        },
        "[action=exportReasonSafetyExcel]": {
          click: this._onExportReasonSafetyExcel
        },
        "[action=reasonSafetyAuditory]": {
          click: this._onReasonSafetyAuditory
        },
        "[action=newReasonSafety]": {
          click: this._newReasonSafety
        },
        "[action=deleteReasonSafety]": {
          click: this._onDeleteReasonSafety
        },
        "[action=searchReasonSafety]": {
          specialkey: this._searchReasonSafety
        }
      });
    },
    _newReasonSafety: function() {
      var modelReasonSafety = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterReasonSafety",
        {
          idReasonSafety: "id",
          description: "",
          codeCause: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterReasonSafety"
      )[0];
      var panel = general.down("ViewGridPanelRegisterReasonSafety");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelReasonSafety);
      editor.startEdit(0, 0);
    },
    _onDeleteReasonSafety: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterReasonSafety grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteReasonSafety.htm?nameView=ViewPanelRegisterReasonSafety"
      );
    },
    _searchReasonSafety: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterReasonSafety grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findReasonSafety.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridReasonSafety: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterReasonSafety grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportReasonSafetyPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportReasonSafetyExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onReasonSafetyAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterReasonSafety grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditReasonSafety.htm"
      );
    }
  }
);
