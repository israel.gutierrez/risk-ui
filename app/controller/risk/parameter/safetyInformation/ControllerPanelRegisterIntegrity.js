Ext.define(
  "DukeSource.controller.risk.parameter.safetyInformation.ControllerPanelRegisterIntegrity",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterIntegrity"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterIntegrity"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterIntegrity",
      "risk.parameter.safetyInformation.ViewPanelRegisterIntegrity"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridIntegrity]": {
          keyup: this._onSearchTriggerGridIntegrity
        },
        "[action=exportIntegrityPdf]": {
          click: this._onExportIntegrityPdf
        },
        "[action=exportIntegrityExcel]": {
          click: this._onExportIntegrityExcel
        },
        "[action=integrityAuditory]": {
          click: this._onIntegrityAuditory
        },
        "[action=newIntegrity]": {
          click: this._newIntegrity
        },
        "[action=deleteIntegrity]": {
          click: this._onDeleteIntegrity
        },
        "[action=searchIntegrity]": {
          specialkey: this._searchIntegrity
        }
      });
    },
    _newIntegrity: function() {
      var modelIntegrity = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterIntegrity",
        {
          idIntegrity: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterIntegrity")[0];
      var panel = general.down("ViewGridPanelRegisterIntegrity");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelIntegrity);
      editor.startEdit(0, 0);
    },
    _onDeleteIntegrity: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterIntegrity grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteIntegrity.htm?nameView=ViewPanelRegisterIntegrity"
      );
    },
    _searchIntegrity: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterIntegrity grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findIntegrity.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridIntegrity: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterIntegrity grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportIntegrityPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportIntegrityExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onIntegrityAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterIntegrity grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditIntegrity.htm"
      );
    }
  }
);
