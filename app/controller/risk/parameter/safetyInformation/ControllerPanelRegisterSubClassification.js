Ext.define(
  "DukeSource.controller.risk.parameter.safetyInformation.ControllerPanelRegisterSubClassification",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterSubClassification"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterSubClassification"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterSubClassification",
      "risk.parameter.safetyInformation.ViewPanelRegisterSubClassification"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridSubClassification]": {
          keyup: this._onSearchTriggerGridSubClassification
        },
        "[action=exportSubClassificationPdf]": {
          click: this._onExportSubClassificationPdf
        },
        "[action=exportSubClassificationExcel]": {
          click: this._onExportSubClassificationExcel
        },
        "[action=subClassificationAuditory]": {
          click: this._onSubClassificationAuditory
        },
        "[action=newSubClassification]": {
          click: this._newSubClassification
        },
        "[action=deleteSubClassification]": {
          click: this._onDeleteSubClassification
        },
        "[action=searchSubClassification]": {
          specialkey: this._searchSubClassification
        }
      });
    },
    _newSubClassification: function() {
      var modelSubClassification = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterSubClassification",
        {
          idSubClassification: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterSubClassification"
      )[0];
      var panel = general.down("ViewGridPanelRegisterSubClassification");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelSubClassification);
      editor.startEdit(0, 0);
    },
    _onDeleteSubClassification: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSubClassification grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteSubClassification.htm?nameView=ViewPanelRegisterSubClassification"
      );
    },
    _searchSubClassification: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterSubClassification grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findSubClassification.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridSubClassification: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSubClassification grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportSubClassificationPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportSubClassificationExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onSubClassificationAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSubClassification grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditSubClassification.htm"
      );
    }
  }
);
