Ext.define(
  "DukeSource.controller.risk.parameter.safetyInformation.ControllerPanelRegisterSafetyCriterion",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterSafetyCriterion"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterSafetyCriterion"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterSafetyCriterion",
      "risk.parameter.safetyInformation.ViewPanelRegisterSafetyCriterion"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridSafetyCriterion]": {
          keyup: this._onSearchTriggerGridSafetyCriterion
        },
        "[action=exportSafetyCriterionPdf]": {
          click: this._onExportSafetyCriterionPdf
        },
        "[action=exportSafetyCriterionExcel]": {
          click: this._onExportSafetyCriterionExcel
        },
        "[action=safetyCriterionAuditory]": {
          click: this._onSafetyCriterionAuditory
        },
        "[action=newSafetyCriterion]": {
          click: this._newSafetyCriterion
        },
        "[action=deleteSafetyCriterion]": {
          click: this._onDeleteSafetyCriterion
        },
        "[action=searchSafetyCriterion]": {
          specialkey: this._searchSafetyCriterion
        }
      });
    },
    _newSafetyCriterion: function() {
      var modelSafetyCriterion = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterSafetyCriterion",
        {
          idSafetyCriterion: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterSafetyCriterion"
      )[0];
      var panel = general.down("ViewGridPanelRegisterSafetyCriterion");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelSafetyCriterion);
      editor.startEdit(0, 0);
    },
    _onDeleteSafetyCriterion: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSafetyCriterion grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteSafetyCriterion.htm?nameView=ViewPanelRegisterSafetyCriterion"
      );
    },
    _searchSafetyCriterion: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterSafetyCriterion grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findSafetyCriterion.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridSafetyCriterion: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSafetyCriterion grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportSafetyCriterionPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportSafetyCriterionExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onSafetyCriterionAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSafetyCriterion grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditSafetyCriterion.htm"
      );
    }
  }
);
