Ext.define(
  "DukeSource.controller.risk.parameter.process.ControllerPanelRegisterSubProcess",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.parameter.grids.StoreGridPanelRegisterSubProcess",
      "risk.parameter.combos.StoreComboProcess"
    ],
    models: [
      "risk.parameter.grids.ModelGridPanelRegisterSubProcess",
      "risk.parameter.combos.ModelComboProcess"
    ],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterSubProcess",
      "risk.parameter.process.ViewPanelRegisterSubProcess",
      "risk.parameter.combos.ViewComboProcess",
      "risk.util.ViewComboYesNo"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridSubProcess]": {
          keyup: this._onSearchTriggerGridSubProcess
        },
        "[action=exportSubProcessPdf]": {
          click: this._onExportSubProcessPdf
        },
        "[action=exportSubProcessExcel]": {
          click: this._onExportSubProcessExcel
        },
        "[action=subProcessAuditory]": {
          click: this._onSubProcessAuditory
        },
        "[action=newSubProcess]": {
          click: this._newSubProcess
        },
        "[action=saveSubProcess]": {
          click: this._onSaveSubProcess
        },
        "[action=deleteSubProcess]": {
          click: this._onDeleteSubProcess
        },
        "[action=searchSubProcess]": {
          specialkey: this._searchSubProcess
        },
        "ViewPanelRegisterSubProcess grid": {
          itemdblclick: this._onModifySubProcess
        }
      });
    },
    _newSubProcess: function() {
      var window = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowSubProcess",
        {
          modal: true
        }
      ).show();
    },
    _onSaveSubProcess: function() {
      var window = Ext.ComponentQuery.query("ViewWindowSubProcess")[0];
      if (
        window
          .down("form")
          .getForm()
          .isValid()
      ) {
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/saveSubProcess.htm",
          params: {
            jsonData: Ext.JSON.encode(window.down("form").getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              window.close();
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.INFO
              );
              Ext.ComponentQuery.query("ViewPanelRegisterSubProcess grid")[0]
                .getStore()
                .load();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onDeleteSubProcess: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSubProcess grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteSubProcess.htm"
      );
    },
    _searchSubProcess: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterSubProcess grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findSubProcess.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridSubProcess: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSubProcess grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportSubProcessPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportSubProcessExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onSubProcessAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSubProcess grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditSubProcess.htm"
      );
    },
    _onModifySubProcess: function(grid) {
      var window = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowSubProcess",
        {
          modal: true
        }
      ).show();
      window
        .down("form")
        .getForm()
        .loadRecord(grid.getSelectionModel().getSelection()[0]);
    }
  }
);
