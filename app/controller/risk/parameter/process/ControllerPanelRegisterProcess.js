Ext.define(
  "DukeSource.controller.risk.parameter.process.ControllerPanelRegisterProcess",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.parameter.grids.StoreGridPanelRegisterProcess",
      "risk.parameter.combos.StoreComboProcessType"
    ],
    models: [
      "risk.parameter.grids.ModelGridPanelRegisterProcess",
      "risk.parameter.combos.ModelComboProcessType"
    ],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterProcess",
      "risk.parameter.process.ViewPanelRegisterProcess",
      "risk.util.ViewComboYesNo",
      "risk.parameter.combos.ViewComboProcessType"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridProcess]": {
          keyup: this._onSearchTriggerGridProcess
        },
        "[action=exportProcessPdf]": {
          click: this._onExportProcessPdf
        },
        "[action=exportProcessExcel]": {
          click: this._onExportProcessExcel
        },
        "[action=processAuditory]": {
          click: this._onProcessAuditory
        }
      });
    },
    _newProcess: function() {
      var tree = Ext.ComponentQuery.query("ViewPanelRegisterProcessType")[0];
      var node = tree.getSelectionModel().getSelection()[0];

      var window = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowProcess",
        {
          modal: true
        }
      ).show();
    },
    _onSearchTriggerGridProcess: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterProcess grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportProcessPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportProcessExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onProcessAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterProcess grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditProcess.htm"
      );
    }
  }
);
