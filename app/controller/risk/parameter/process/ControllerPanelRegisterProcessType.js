Ext.define(
  "DukeSource.controller.risk.parameter.process.ControllerPanelRegisterProcessType",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreTreeGridPanelRegisterProcessType"],
    models: ["risk.parameter.grids.ModelTreeGridPanelRegisterProcessType"],
    views: [
      "risk.parameter.grids.ViewTreeGridPanelRegisterProcessType",
      "risk.parameter.combos.ViewComboFeatureProcess",
      "risk.parameter.combos.ViewComboProcessQualification",
      "risk.parameter.combos.ViewComboSubprocessImportance",
      "risk.parameter.combos.ViewComboWorkArea",
      "risk.parameter.combos.ViewComboJobPlace",
      "risk.util.search.AdvancedSearchUser",
      "risk.User.combos.ViewComboAgency"
    ],
    init: function() {
      this.control({
        "[action=newProcess]": {
          click: this._newProcess
        },
        "[action=saveProcess]": {
          click: this._saveProcess
        },
        "[action=modifyProcess]": {
          click: this._editProcess
        },
        "[action=deleteProcess]": {
          click: this._deleteProcess
        },
        "[action=assignOwnerProcess]": {
          click: this._assignOwnerToProcess
        },
        "[action=saveOwnerProcess]": {
          click: this._saveOwnerProcess
        },
        "[action=deleteOwnerProcess]": {
          click: this._deleteOwnerProcess
        },
        "[action=evaluateProcess]": {
          click: this._evaluateProcess
        },
        "[action=saveEvaluationProcess]": {
          click: this._saveEvaluationProcess
        },
        "[action=manageAttachment]": {
          click: this._addArchiveSubProcess
        },
        "[action=reportInventoryProcess]": {
          click: this._onReportInventoryProcess
        },
        "[action=saveFileAttachmentsProcess]": {
          click: this._saveFileAttachmentsProcess
        },
        "[action=deleteFileAttachmentsProcess]": {
          click: this._deleteFileAttachmentsProcess
        },
        "[action=consultResponsible]": {
          click: this._onConsultResponsible
        },
        ViewTreeGridPanelRegisterProcessType: {
          itemcontextmenu: this._clickRightProcess
        }
      });
    },

    settingIds: function(depth, view, node) {
      if (depth === 1) {
        view.down("#idProcessType").setValue(node.data["idProcessType"]);
      } else if (depth === 2) {
        view.down("#idProcessType").setValue(node.data["idProcessType"]);
        view.down("#idProcess").setValue(node.data["idProcess"]);
      } else if (depth > 2) {
        view.down("#idProcessType").setValue(node.data["idProcessType"]);
        view.down("#idProcess").setValue(node.data["idProcess"]);
        view.down("#idSubProcess").setValue(node.data["idSubProcess"]);
      }
    },
    _newProcess: function() {
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterProcessType"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];
      var depth = node === undefined ? 0 : node.data["depth"];
      var nameView =
        depth === 0
          ? "ViewWindowProcessType"
          : depth === 1
          ? "ViewWindowProcess"
          : depth === 2
          ? "ViewWindowSubProcess"
          : "ViewWindowActivity";

      var prefix =
        depth === 0 ? undefined : node.getPath("description", " &#8702; ");
      var view = Ext.create(
        "DukeSource.view.risk.parameter.windows." + nameView,
        {
          modal: true,
          prefix: prefix,
          isNew: true,
          depth: node === undefined ? 0 : node.data["depth"]
        }
      );
      view.show();
      this.settingIds(depth, view, node);
      view.down("#description").focus(false, 100);
    },
    _saveProcess: function(button) {
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterProcessType"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];
      var win = button.up("window");
      var depth = win.depth;
      var form = win.down("form");

      form
        .down("#path")
        .setValue(
          depth === 0
            ? form.down("#description").getValue()
            : win.prefix.replace(" &#8702;  &#8702; ", "")
        );

      if (form.getForm().isValid()) {
        DukeSource.lib.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/saveProcessType.htm",
          params: {
            jsonData: Ext.JSON.encode(form.getForm().getValues()),
            depth: depth
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            node = win.isNew === true ? node : node.parentNode;
            if (response.success) {
              tree.store.getProxy().extraParams = { depth: depth };
              tree.store.load({
                node: node
              });
              win.close();
              DukeSource.global.DirtyView.messageNormal(response.message);
            } else {
              DukeSource.global.DirtyView.messageWarning(response.message);
            }
          },
          failure: function(response) {
            DukeSource.global.DirtyView.messageWarning(response.message);
          }
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
      }
    },
    _editProcess: function() {
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterProcessType"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];
      var depth = node.data["depth"];

      var prefix = node.getPath("description", " &#8702; ");

      var nameView =
        depth === 1
          ? "ViewWindowProcessType"
          : depth === 2
          ? "ViewWindowProcess"
          : depth === 3
          ? "ViewWindowSubProcess"
          : "ViewWindowActivity";
      var view = Ext.create(
        "DukeSource.view.risk.parameter.windows." + nameView,
        {
          modal: true,
          isNew: false,
          controller: this,
          prefix: prefix,
          depth: depth - 1
        }
      );
      view.show(undefined, function() {
        view
          .down("form")
          .getForm()
          .setValues(node.raw);
        view.controller.settingIds(depth, view, node);
      });
    },
    _deleteProcess: function() {
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterProcessType"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];
      var depth = node.data["depth"] - 1;
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
        msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
        icon: Ext.Msg.WARNING,
        buttonText: {
          yes: "Si"
        },
        buttons: Ext.MessageBox.YESNO,
        fn: function(btn) {
          if (btn === "yes") {
            DukeSource.lib.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/deleteProcessType.htm",
              params: {
                jsonData: Ext.JSON.encode(node.data),
                depth: depth
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  tree.store.getProxy().extraParams = { depth: depth };
                  tree.store.load({
                    node: node.parentNode
                  });
                  window.close();
                  DukeSource.global.DirtyView.messageNormal(response.message);
                } else {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              },
              failure: function() {}
            });
          }
        }
      });
    },

    settingByDepth: function(recordsProcess, process, depth, ids) {
      Ext.Array.each(recordsProcess, function(record, index, countriesItSelf) {
        process.push(record.data);
        if (depth == 1) {
          ids = ids + parseInt(record.data["idProcessType"]) + ",";
        } else if (depth == 2) {
          ids = ids + parseInt(record.data["idProcess"]) + ",";
        } else if (depth == 3) {
          ids = ids + parseInt(record.data["idSubProcess"]) + ",";
        } else if (depth == 4) {
          ids = ids + parseInt(record.data["idActivity"]) + ",";
        }
      });
      return ids.substr(0, ids.length - 1);
    },
    _assignOwnerToProcess: function(btn) {
      var type = btn.type;
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterProcessType"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];

      if (node === undefined) {
        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.MESSAGE_ITEM);
      } else {
        var view = Ext.create(
          "DukeSource.view.risk.parameter.process.ViewWindowAddOwnerToProcess",
          {
            modal: true,
            type: type,
            actionButton: "saveOwnerProcess"
          }
        );
        view.title = view.title + " - " + node.data["description"];
        view.show();
      }
    },
    _saveOwnerProcess: function(btn) {
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterProcessType"
      )[0];
      var view = btn.up("window");
      var grid = view.down("grid");
      var node = tree.getSelectionModel().getSelection()[0];
      var depth = node.data["depth"];

      var process = [];
      var ids = "";
      var recordsProcess = tree.getSelectionModel().getSelection();
      ids = this.settingByDepth(recordsProcess, process, depth, ids);
      var owners = [];
      var recordsOwner = grid.getSelectionModel().getSelection();
      Ext.Array.each(recordsOwner, function(record, index, countriesItSelf) {
        owners.push(record.data);
      });
      DukeSource.lib.Ajax.request({
        method: "POST",
        url:
          "http://localhost:9000/giro/saveOwnerToProcess.htm?nameView=ViewPanelRegisterProcessType",
        params: {
          process: Ext.JSON.encode(process),
          owners: Ext.JSON.encode(owners),
          depth: depth
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            var panel = Ext.ComponentQuery.query(
              "ViewPanelRegisterProcessType"
            )[0];
            var grid = panel.down("#griOwnerProcess");
            grid.store.getProxy().url =
              "http://localhost:9000/giro/showListOwnerToProcess.htm?nameView=ViewPanelRegisterProcessType";
            grid.store.getProxy().extraParams = {
              depth: depth,
              ids: ids
            };
            grid.down("pagingtoolbar").moveFirst();
            view.close();
            DukeSource.global.DirtyView.messageNormal(response.message);
          } else {
            DukeSource.global.DirtyView.messageWarning(response.message);
          }
        },
        failure: function() {}
      });
    },
    _deleteOwnerProcess: function(btn) {
      var grid = btn.up("gridpanel");
      var owners = [];
      var recordsOwner = grid.getSelectionModel().getSelection();
      Ext.Array.each(recordsOwner, function(record, index, countriesItSelf) {
        owners.push(record.data);
      });
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_WARNING,
        msg: "Estas seguro que desea eliminar la asignación?",
        icon: Ext.Msg.WARNING,
        buttonText: {
          yes: "Si",
          no: "No"
        },
        buttons: Ext.MessageBox.YESNO,
        fn: function(btn) {
          if (btn === "yes") {
            DukeSource.lib.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/deleteOwnerProcess.htm?nameView=ViewPanelRegisterProcessType",
              params: {
                owners: Ext.JSON.encode(owners)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  grid.down("pagingtoolbar").moveFirst();
                  DukeSource.global.DirtyView.messageNormal(response.message);
                } else {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              },
              failure: function(response) {
                DukeSource.global.DirtyView.messageWarning(response.message);
              }
            });
          } else {
          }
        }
      });
    },

    _evaluateProcess: function() {
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterProcessType"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];

      if (node === undefined) {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
      } else {
        var depth = node.data["depth"];
        var view = Ext.create(
          "DukeSource.view.risk.parameter.windows.ViewWindowEvaluationSubProcess",
          {
            modal: true
          }
        );

        view.title = view.title + " - " + node.data["description"];
        view
          .down("form")
          .getComponent("id")
          .setValue(node.data["id"]);
        view.show();
        var grid = view.down("grid");
        grid.store.getProxy().extraParams = {
          propertyFind: "description",
          id: node.data["id"],
          propertyOrder: "description",
          depth: depth
        };
        grid.store.getProxy().url =
          "http://localhost:9000/giro/showListFeatureProcessToEvaluate.htm";
        grid.down("pagingtoolbar").moveFirst();

        DukeSource.lib.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/showLastQualificationProcess.htm",
          params: {
            id: node.data["id"],
            depth: depth
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              view
                .down("form")
                .getForm()
                .setValues(response.data);
            } else {
              DukeSource.global.DirtyView.messageWarning(response.message);
            }
          },
          failure: function() {}
        });
      }
    },
    _saveEvaluationProcess: function(button) {
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterProcessType"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];
      var depth = node.data["depth"];

      var win = button.up("window");
      var form = win.down("form");
      var records = win
        .down("grid")
        .getStore()
        .getRange();
      var arrayDataToSave = [];
      Ext.Array.each(records, function(record) {
        arrayDataToSave.push(record.data);
      });
      if (form.getForm().isValid()) {
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveEvaluationProcess.htm?nameView=ViewPanelRegisterProcessType",
          params: {
            jsonData: Ext.JSON.encode(arrayDataToSave),
            id: node.data["id"],
            depth: depth,
            idProcessQualification: win
              .down("ViewComboProcessQualification")
              .getValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                response.mensaje,
                Ext.Msg.INFO
              );
              win.close();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _addArchiveSubProcess: function() {
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterProcessType"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];
      var depth = node.data["depth"];
      var k = Ext.create(
        "DukeSource.view.fulfillment.window.ViewWindowFileAttachment",
        {
          saveFile: "saveFileAttachmentsProcess",
          deleteFile: "deleteFileAttachmentsProcess",
          searchGridTrigger: "searchGridFileAttachmentDocument",
          src: "http://localhost:9000/giro/downloadFileAttachmentsProcess.htm",
          params: ["idFileAttachment", "nameFile"]
        }
      );
      k.title = k.title + " - " + node.data["description"];
      var gridFileAttachment = k.down("grid");
      gridFileAttachment.store.getProxy().extraParams = {
        id: node.data["id"],
        depth: depth
      };
      gridFileAttachment.store.getProxy().url =
        "http://localhost:9000/giro/showListFileAttachmentsProcess.htm";
      gridFileAttachment.down("pagingtoolbar").moveFirst();
      k.id = node.data["id"];
      k.depth = depth;
      k.show();
    },
    _saveFileAttachmentsProcess: function(btn) {
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterProcessType"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];
      var depth = node.data["depth"];

      var win = btn.up("window");
      var grid = win.down("grid");
      var form = win.down("form");
      if (form.getForm().isValid()) {
        form.getForm().submit({
          url:
            "http://localhost:9000/giro/saveFileAttachmentsProcess.htm?nameView=ViewPanelRegisterProcessType&id=" +
            win.id +
            "&depth=" +
            win.depth,
          waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
          method: "POST",
          success: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (valor.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                valor.mensaje,
                Ext.Msg.INFO
              );
              grid.store.getProxy().extraParams = {
                id: win.id,
                depth: win.depth
              };
              grid.store.getProxy().url =
                "http://localhost:9000/giro/showListFileAttachmentsProcess.htm";
              grid.down("pagingtoolbar").moveFirst();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                valor.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (!valor.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                valor.mensaje,
                Ext.Msg.ERROR
              );
            }
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _deleteFileAttachmentsProcess: function(btn) {
      var win = btn.up("window");
      var grid = win.down("grid");
      var record = grid.getSelectionModel().getSelection()[0];
      var form = win.down("form");
      DukeSource.lib.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/deleteFileAttachmentsProcess.htm",
        params: {
          idFileAttachment: record.get("idFileAttachment")
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            grid.store.getProxy().extraParams = {
              id: win.id,
              depth: win.depth
            };
            grid.store.getProxy().url =
              "http://localhost:9000/giro/showListFileAttachmentsProcess.htm";
            grid.down("pagingtoolbar").moveFirst();
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function(response) {
          DukeSource.global.DirtyView.messageAlert(
            DukeSource.global.GiroMessages.TITLE_ERROR,
            response.mensaje,
            Ext.Msg.ERROR
          );
        }
      });
    },
    _onConsultResponsible: function(btn) {
      var panel = Ext.ComponentQuery.query("ViewPanelRegisterProcessType")[0];
      var tree = panel.down("ViewTreeGridPanelRegisterProcessType");
      var node = tree.getSelectionModel().getSelection()[0];

      if (node === undefined) {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
      } else {
        var depth = node.data["depth"];

        var process = [];
        var recordsProcess = tree.getSelectionModel().getSelection();
        var ids = "";
        ids = this.settingByDepth(recordsProcess, process, depth, ids);

        var grid = panel.down("#griOwnerProcess");
        grid.store.getProxy().url =
          "http://localhost:9000/giro/showListOwnerToProcess.htm?nameView=ViewPanelRegisterProcessType";
        grid.store.getProxy().extraParams = {
          depth: depth,
          ids: ids
        };
        grid.down("pagingtoolbar").moveFirst();
      }
    },
    _clickRightProcess: function(view, rec, node, index, e) {
      e.stopEvent();
      var addMenu = Ext.create("DukeSource.view.fulfillment.AddMenu", {});
      addMenu.removeAll();
      addMenu.add(
        {
          text: "Nuevo",
          iconCls: "add",
          action: "newProcess"
        },
        {
          text: "Modificar",
          iconCls: "modify",
          action: "modifyProcess"
        },
        {
          text: "Eliminar",
          iconCls: "delete",
          action: "deleteProcess"
        },
        {
          text: "Responsable",
          iconCls: "users",
          action: "assignOwnerProcess"
        },
        {
          text: "Evaluador",
          iconCls: "users",
          action: "assignOwnerProcess"
        },
        {
          text: "Evaluar",
          iconCls: "matrix",
          action: "evaluateProcess"
        },
        {
          text: "Adjuntos",
          iconCls: "gestionfile",
          action: "manageAttachment"
        }
      );
      addMenu.showAt(e.getXY());
    },

    _onReportInventoryProcess: function(btn) {
      var window = Ext.create("Ext.window.Window", {
        layout: "fit",
        cls: "x-hidden",
        modal: false,
        hidden: true,
        buttons: [
          {
            text: "SALIR",
            scale: "medium",
            handler: function(btn) {
              btn.up("window").close();
            },
            iconCls: "logout"
          }
        ],
        buttonAlign: "center"
      }).show();
      window.removeAll();
      window.add({
        xtype: "component",
        autoEl: {
          tag: "iframe",
          src:
            "http://localhost:9000/giro/xlsGeneralReport.htm?values=&names=&types=&nameReport=ROPARAM0001XLS"
        }
      });
    }
  }
);
