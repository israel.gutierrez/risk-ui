Ext.define(
  "DukeSource.controller.risk.parameter.process.ControllerPanelRegisterValuationFeatureProcess",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.parameter.grids.StoreGridPanelRegisterValuationFeatureProcess",
      "risk.parameter.combos.StoreComboFeatureProcess"
    ],
    models: [
      "risk.parameter.grids.ModelGridPanelRegisterValuationFeatureProcess",
      "risk.parameter.combos.ModelComboFeatureProcess"
    ],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterValuationFeatureProcess",
      "risk.parameter.process.ViewPanelRegisterValuationFeatureProcess",
      "risk.util.ViewComboYesNo",
      "risk.parameter.combos.ViewComboFeatureProcess"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridValuationFeatureProcess]": {
          keyup: this._onSearchTriggerGridValuationFeatureProcess
        },
        "[action=exportValuationFeatureProcessPdf]": {
          click: this._onExportValuationFeatureProcessPdf
        },
        "[action=exportValuationFeatureProcessExcel]": {
          click: this._onExportValuationFeatureProcessExcel
        },
        "[action=valuationFeatureProcessAuditory]": {
          click: this._onValuationFeatureProcessAuditory
        },
        "[action=newValuationFeatureProcess]": {
          click: this._newValuationFeatureProcess
        },
        "[action=saveValuationFeatureProcess]": {
          click: this._onSaveValuationFeatureProcess
        },
        "[action=deleteValuationFeatureProcess]": {
          click: this._onDeleteValuationFeatureProcess
        },
        "[action=searchValuationFeatureProcess]": {
          specialkey: this._searchValuationFeatureProcess
        },
        "ViewPanelRegisterValuationFeatureProcess grid": {
          itemdblclick: this._onModifyValuationFeatureProcess
        }
      });
    },
    _newValuationFeatureProcess: function() {
      var window = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowValuationFeatureProcess",
        {
          modal: true
        }
      ).show();
    },
    _onSaveValuationFeatureProcess: function() {
      var window = Ext.ComponentQuery.query(
        "ViewWindowValuationFeatureProcess"
      )[0];
      if (
        window
          .down("form")
          .getForm()
          .isValid()
      ) {
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/saveValuationFeatureProcess.htm",
          params: {
            jsonData: Ext.JSON.encode(window.down("form").getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              window.close();
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.INFO
              );
              Ext.ComponentQuery.query(
                "ViewPanelRegisterValuationFeatureProcess grid"
              )[0]
                .getStore()
                .load();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onDeleteValuationFeatureProcess: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterValuationFeatureProcess grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteValuationFeatureProcess.htm"
      );
    },
    _searchValuationFeatureProcess: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterValuationFeatureProcess grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findValuationFeatureProcess.htm",
          "idTypeChange",
          "idTypeChange"
        );
      } else {
      }
    },
    _onSearchTriggerGridValuationFeatureProcess: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterValuationFeatureProcess grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportValuationFeatureProcessPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportValuationFeatureProcessExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onValuationFeatureProcessAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterValuationFeatureProcess grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditValuationFeatureProcess.htm"
      );
    },
    _onModifyValuationFeatureProcess: function(grid) {
      var window = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowValuationFeatureProcess",
        {
          modal: true
        }
      ).show();
      window
        .down("form")
        .getForm()
        .loadRecord(grid.getSelectionModel().getSelection()[0]);
    }
  }
);
