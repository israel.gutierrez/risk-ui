Ext.define(
  "DukeSource.controller.risk.parameter.process.ControllerPanelRegisterFeatureProcess",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreTreeGridPanelRegisterFeatureProcess"],
    models: ["risk.parameter.grids.ModelTreeGridPanelRegisterFeatureProcess"],
    views: [
      "risk.parameter.grids.ViewTreeGridPanelRegisterFeatureProcess",
      "risk.parameter.process.ViewPanelRegisterFeatureProcess"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridFeatureProcess]": {
          keyup: this._onSearchTriggerGridFeatureProcess
        },
        "[action=exportFeatureProcessPdf]": {
          click: this._onExportFeatureProcessPdf
        },
        "[action=exportFeatureProcessExcel]": {
          click: this._onExportFeatureProcessExcel
        },
        "[action=featureProcessAuditory]": {
          click: this._onFeatureProcessAuditory
        },
        "[action=newFeatureProcess]": {
          click: this._newFeatureProcess
        },
        "[action=deleteFeatureProcess]": {
          click: this._onDeleteFeatureProcess
        },
        "[action=searchFeatureProcess]": {
          specialkey: this._searchFeatureProcess
        },
        ViewTreeGridPanelRegisterFeatureProcess: {
          itemcontextmenu: this.treeRightClickFeaturesProcess
        },
        "AddMenuValuationFeaturesProcess menuitem[text=Agregar]": {
          click: this._addValuationFeaturesProcess
        },
        "AddMenuValuationFeaturesProcess menuitem[text=Editar]": {
          click: this._editFeaturesProcess
        },
        "AddMenuValuationFeaturesProcess menuitem[text=Eliminar]": {
          click: this._deleteFeaturesProcess
        },
        "[action=saveFeatureProcess]": {
          click: this._onSaveFeatureProcess
        },
        "EditMenuValuationFeaturesProcess menuitem[text=Editar]": {
          click: this._editValuationFeaturesProcess
        },
        "EditMenuValuationFeaturesProcess menuitem[text=Eliminar]": {
          click: this._deleteFeaturesProcess
        }
      });
    },
    _newFeatureProcess: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowFeaturesProcess",
        {
          modal: true
        }
      );
      countryWindow
        .down("form")
        .getComponent("id")
        .setValue("myTree/id");
      countryWindow.show();
    },
    _onDeleteFeatureProcess: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterFeatureProcess grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteFeatureProcess.htm?nameView=ViewPanelRegisterFeatureProcess"
      );
    },
    _searchFeatureProcess: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterFeatureProcess grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findFeatureProcess.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridFeatureProcess: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterFeatureProcess grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportFeatureProcessPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportFeatureProcessExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onFeatureProcessAuditory: function(button) {
      var treeGrid = button
        .up("panel")
        .down("ViewTreeGridPanelRegisterFeatureProcess");
      if (treeGrid.getSelectionModel().getCount() == 0) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        var id = treeGrid
          .getSelectionModel()
          .getSelection()[0]
          .get("id");
        var array = id.split("/");
        if (array.length == 2) {
          Ext.Ajax.request({
            method: "POST",
            url: "http://localhost:9000/giro/findAuditFeatureProcess.htm",
            params: {
              idFeatureProcess: array[1]
            },
            success: function(response) {
              response = Ext.decode(response.responseText);
              if (response.success) {
                Ext.create("DukeSource.view.risk.util.WindowAuditory", {
                  modal: true
                }).show();
                var form = Ext.ComponentQuery.query("WindowAuditory form")[0];
                form.getForm().setValues(response.data);
              } else {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_ERROR,
                  response.mensaje,
                  Ext.Msg.ERROR
                );
              }
            },
            failure: function() {}
          });
        } else if (array.length == 3) {
          Ext.Ajax.request({
            method: "POST",
            url:
              "http://localhost:9000/giro/findAuditValuationFeatureProcess.htm",
            params: {
              idValuationFeatureProcess: array[2]
            },
            success: function(response) {
              response = Ext.decode(response.responseText);
              if (response.success) {
                Ext.create("DukeSource.view.risk.util.WindowAuditory", {
                  modal: true
                }).show();
                var form = Ext.ComponentQuery.query("WindowAuditory form")[0];
                form.getForm().setValues(response.data);
              } else {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_ERROR,
                  response.mensaje,
                  Ext.Msg.ERROR
                );
              }
            },
            failure: function() {}
          });
        }
      }
      //        var grid = Ext.ComponentQuery.query("ViewPanelRegisterFeatureProcess grid")[0];
      //        DukeSource.global.DirtyView.showWindowAuditory(grid,'http://localhost:9000/giro/findAuditFeatureProcess.htm');
    },
    treeRightClickFeaturesProcess: function(view, record, item, index, e) {
      e.stopEvent();
      this.application.currentRecord = record;
      //if the node is a region let user add a country
      if (record.get("depth") === 1) {
        var addMenu = Ext.create(
          "DukeSource.view.risk.parameter.process.AddMenuValuationFeaturesProcess",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      if (record.get("depth") === 2) {
        var addMenu = Ext.create(
          "DukeSource.view.risk.parameter.process.EditMenuValuationFeaturesProcess",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      return false;
    },
    _addValuationFeaturesProcess: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowValuationFeaturesProcess",
        {
          modal: true
        }
      );
      countryWindow
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id") + "/id");
      countryWindow.show();
    },
    _onSaveFeatureProcess: function(button) {
      var window = button.up("window");
      var form = window.down("form");
      if (form.getForm().isValid()) {
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/saveFeatureProcess.htm",
          params: {
            id: form.getComponent("id").getValue(),
            description: form.getComponent("text").getValue(),
            valuationConcept: form.getComponent("valuationConcept").getValue(),
            valueFeature: form.getComponent("valueFeature").getValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              var refreshNode = Ext.ComponentQuery.query(
                "ViewPanelRegisterFeatureProcess ViewTreeGridPanelRegisterFeatureProcess"
              )[0]
                .getStore()
                .getNodeById(response.data);
              refreshNode.removeAll(false);
              Ext.ComponentQuery.query(
                "ViewPanelRegisterFeatureProcess ViewTreeGridPanelRegisterFeatureProcess"
              )[0]
                .getStore()
                .load({
                  node: refreshNode
                });
              window.close();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _editFeaturesProcess: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowFeaturesProcess",
        {
          modal: true
        }
      );
      countryWindow
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id"));
      countryWindow
        .down("form")
        .getComponent("text")
        .setValue(this.application.currentRecord.get("text"));
      countryWindow.show();
    },
    _deleteFeaturesProcess: function() {
      Ext.Ajax.request({
        method: "POST",
        url:
          "http://localhost:9000/giro/deleteFeatureProcess.htm?nameView=ViewPanelRegisterFeatureProcess",
        params: {
          id: this.application.currentRecord.get("id")
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              response.mensaje,
              Ext.Msg.INFO
            );
            var refreshNode = Ext.ComponentQuery.query(
              "ViewPanelRegisterFeatureProcess ViewTreeGridPanelRegisterFeatureProcess"
            )[0]
              .getStore()
              .getNodeById(response.data);
            refreshNode.removeAll(false);
            Ext.ComponentQuery.query(
              "ViewPanelRegisterFeatureProcess ViewTreeGridPanelRegisterFeatureProcess"
            )[0]
              .getStore()
              .load({
                node: refreshNode
              });
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },
    _editValuationFeaturesProcess: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowValuationFeaturesProcess",
        {
          modal: true
        }
      );
      countryWindow
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id"));
      countryWindow
        .down("form")
        .getComponent("text")
        .setValue(this.application.currentRecord.get("text"));
      countryWindow.show();
      countryWindow
        .down("form")
        .getComponent("valuationConcept")
        .setValue(this.application.currentRecord.get("fieldOne"));
      countryWindow.show();
      countryWindow
        .down("form")
        .getComponent("valueFeature")
        .setValue(this.application.currentRecord.get("fieldTwo"));
      countryWindow.show();
    }
  }
);
