Ext.define(
  "DukeSource.controller.risk.parameter.process.ControllerPanelRegisterProcessQualification",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterProcessQualification"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterProcessQualification"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterProcessQualification",
      "risk.parameter.process.ViewPanelRegisterProcessQualification"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridProcessQualification]": {
          keyup: this._onSearchTriggerGridProcessQualification
        },
        "[action=exportProcessQualificationPdf]": {
          click: this._onExportProcessQualificationPdf
        },
        "[action=exportProcessQualificationExcel]": {
          click: this._onExportProcessQualificationExcel
        },
        "[action=processQualificationAuditory]": {
          click: this._onProcessQualificationAuditory
        },
        "[action=newProcessQualification]": {
          click: this._newProcessQualification
        },
        "[action=deleteProcessQualification]": {
          click: this._onDeleteProcessQualification
        },
        "[action=searchProcessQualification]": {
          specialkey: this._searchProcessQualification
        }
      });
    },
    _newProcessQualification: function() {
      var modelProcessQualification = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterProcessQualification",
        {
          idProcessQualification: "id",
          valueFeature: "",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterProcessQualification"
      )[0];
      var panel = general.down("ViewGridPanelRegisterProcessQualification");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelProcessQualification);
      editor.startEdit(0, 0);
    },
    _onDeleteProcessQualification: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterProcessQualification grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteProcessQualification.htm?nameView=ViewPanelRegisterProcessQualification"
      );
    },
    _searchProcessQualification: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterProcessQualification grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findProcessQualification.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridProcessQualification: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterProcessQualification grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportProcessQualificationPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportProcessQualificationExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onProcessQualificationAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterProcessQualification grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditProcessQualification.htm"
      );
    }
  }
);
