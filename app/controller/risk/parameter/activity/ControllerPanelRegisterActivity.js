Ext.define(
  "DukeSource.controller.risk.parameter.activity.ControllerPanelRegisterActivity",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterActivity"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterActivity"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterActivity",
      "risk.parameter.activity.ViewPanelRegisterActivity"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridActivity]": {
          keyup: this._onSearchTriggerGridActivity
        },
        "[action=exportActivityPdf]": {
          click: this._onExportActivityPdf
        },
        "[action=exportActivityExcel]": {
          click: this._onExportActivityExcel
        },
        "[action=activityAuditory]": {
          click: this._onActivityAuditory
        },
        "[action=newActivity]": {
          click: this._newActivity
        },
        "[action=deleteActivity]": {
          click: this._onDeleteActivity
        },
        "[action=searchActivity]": {
          specialkey: this._searchActivity
        }
      });
    },
    _newActivity: function() {
      var modelActivity = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterActivity",
        {
          idActivity: "id",
          process: "",
          subProcess: "",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterActivity")[0];
      var panel = general.down("ViewGridPanelRegisterActivity");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelActivity);
      editor.startEdit(0, 0);
    },
    _onDeleteActivity: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterActivity grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteActivity.htm"
      );
    },
    _searchActivity: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterActivity grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findActivity.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridActivity: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterActivity grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportActivityPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportActivityExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onActivityAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterActivity grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditActivity.htm"
      );
    }
  }
);
