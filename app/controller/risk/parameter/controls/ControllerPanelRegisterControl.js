Ext.define(
  "DukeSource.controller.risk.parameter.controls.ControllerPanelRegisterControl",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterControl"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterControl"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterControl",
      "risk.parameter.controls.ViewPanelRegisterControl",
      "risk.parameter.combos.ViewComboProcessType",
      "risk.parameter.combos.ViewComboProcess",
      "risk.parameter.combos.ViewComboSubProcess",
      "risk.parameter.combos.ViewComboDetailControlType",
      "risk.parameter.combos.ViewComboForeignKey",
      "risk.parameter.combos.ViewComboScoreControl",
      "risk.parameter.combos.ViewComboJobPlace",
      "risk.parameter.combos.ViewComboWorkArea"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridControl]": {
          keyup: this._onSearchTriggerGridControl
        },
        "[action=exportControlPdf]": {
          click: this._onExportControlPdf
        },
        "[action=exportControlExcel]": {
          click: this._onExportControlExcel
        },
        "[action=controlAuditory]": {
          click: this._onControlAuditory
        },
        "[action=newControl]": {
          click: this._newControl
        },
        "[action=evaluateControl]": {
          click: this._onEvaluateControl
        },
        "[action=newVersionControl]": {
          click: this._onNewVersionControl
        },
        "[action=consultVersionControl]": {
          click: this._onConsultVersionControl
        },
        "[action=deleteControl]": {
          click: this._onDeleteControl
        },
        "[action=saveEvaluationControlParameter]": {
          click: this._onSaveEvaluationControlParameter
        },
        "[action=searchControl]": {
          specialkey: this._searchControl
        },
        "[action=modifyControl]": {
          click: this._onModifyControl
        },
        "[action=searchControlAdvanced]": {
          click: this._onSearchControlAdvanced
        },
        "ViewPanelRegisterControl ViewGridPanelRegisterControl": {
          itemcontextmenu: this._onRightClickRegisterControl
        }
      });
    },
    _newControl: function() {
      var view = Ext.create(
        "DukeSource.view.risk.parameter.controls.ViewWindowRegisterControl",
        {
          modal: true
        }
      ).show();
    },
    _onEvaluateControl: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterControl grid")[0];
      var row = grid.getSelectionModel().getSelection()[0];
      if (row === undefined) {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
      } else {
        var window = Ext.create(
          "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowControlEntry",
          {
            modal: true,
            origin: "catalogControl"
          }
        );
        DukeSource.lib.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/showFinalScoreControlActive.htm",
          params: {
            idControl: row.get("idControl"),
            start: "0",
            limit: "100"
          },
          success: function(response) {
            response = Ext.decode(response.responseText);

            if (response.success) {
              window.show(undefined, function() {
                for (var i = 0; i < response.data.length; i++) {
                  var scoreByGroup = {};
                  DukeSource.global.DirtyView.createGroup(scoreByGroup, response, i);

                  scoreByGroup.typeControl = row.get("typeControl");
                  window.numberItems = response.data.length;
                  window.groups = response.data;
                  DukeSource.global.DirtyView.createItemsToFinalScore(window, scoreByGroup);
                }
                window.setWidth(
                  response.data.length > 2 ? 310 * response.data.length : 760
                );

                var scoreFinal = DukeSource.global.DirtyView.createScoreFinal(response);
                scoreFinal.typeControl = row.get("typeControl");

                DukeSource.global.DirtyView.createItemsToFinalScore(window, scoreFinal);
                window
                  .down("#" + scoreFinal.idItemValue)
                  .setFieldLabel("Rating Total");

                DukeSource.global.DirtyView.createButton(window);
                window
                  .down("form")
                  .getForm()
                  .loadRecord(row);

                DukeSource.lib.Ajax.request({
                  method: "POST",
                  url: "http://localhost:9000/giro/findControlById.htm",
                  params: {
                    propertyFind: "idControl",
                    valueFind: row.get("idControl"),
                    propertyOrder: "description",
                    start: "0",
                    limit: "25"
                  },
                  success: function(response) {
                    var form = window.down("form");
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                      form.getForm().setValues(response.data);
                      form
                        .down("#idDetailControl")
                        .setValue(response.data.idControl);
                    } else {
                      DukeSource.global.DirtyView.messageWarning(response.message);
                    }
                    window.down("#descriptionEvaluation").focus(false, 100);
                  }
                });

                var gridControl = window.down("grid");
                gridControl.store.getProxy().extraParams = {
                  idControl: grid
                    .getSelectionModel()
                    .getSelection()[0]
                    .get("idControl")
                };
                gridControl.store.getProxy().url =
                  "http://localhost:9000/giro/showListControlTypeToEvaluateControl.htm";
                gridControl.down("pagingtoolbar").doRefresh();
              });
            } else {
              DukeSource.global.DirtyView.messageWarning(response.message);
            }
          }
        });
      }
    },
    _onDeleteControl: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterControl grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteControl.htm?nameView=ViewPanelRegisterControl"
      );
    },
    _onSaveEvaluationControlParameter: function(button) {
      var panel = button.up("window");

      var records = panel
        .down("grid")
        .getStore()
        .getRange();
      var arrayDataToSave = [];
      Ext.Array.each(records, function(record) {
        arrayDataToSave.push(record.data);
      });
      if (
        panel
          .down("form")
          .getForm()
          .isValid()
      ) {
        DukeSource.lib.Ajax.request({
          waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
          method: "POST",
          url:
            "http://localhost:9000/giro/saveEvaluationControlParameter.htm?nameView=ViewPanelRegisterControl",
          params: {
            grid: Ext.JSON.encode(arrayDataToSave),
            jsonData: Ext.JSON.encode(
              panel.down("#formHeaderControl").getValues()
            ),
            scoreGroups: Ext.JSON.encode(panel.buildObjectGroups())
          },
          scope: this,
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                response.mensaje,
                Ext.Msg.INFO
              );
              if ((panel.origin = "evaluationRisk")) {
                Ext.ComponentQuery.query("ViewWindowSearchControl grid")[0]
                  .getStore()
                  .load();
              } else {
                Ext.ComponentQuery.query(
                  "ViewPanelRegisterControl ViewGridPanelRegisterControl"
                )[0]
                  .getStore()
                  .load();
              }
              panel.close();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _searchControl: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query("ViewPanelRegisterControl grid")[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findControl.htm",
          "c.description",
          "c.idControl"
        );
      } else {
      }
    },
    _onSearchTriggerGridControl: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterControl grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportControlPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportControlExcel: function(btn) {
      var panel = btn.up("form");
      //var panel = button.up('ViewPanelRegisterControl');
      var processType =
        panel.down("#processType").getValue() == undefined
          ? "T"
          : panel.down("#processType").getValue();
      var process =
        panel.down("#process").getValue() == undefined
          ? "T"
          : panel.down("#process").getValue();
      var subProcess =
        panel.down("#subProcess").getValue() == undefined
          ? "T"
          : panel.down("#subProcess").getValue();
      //var grid = panel.up('ViewPanelRegisterControl').down('grid');
      var codeControl =
        panel.down("#codeControl").getValue() == ""
          ? "T"
          : panel.down("#codeControl").getValue();
      var values =
        codeControl + "," + processType + "," + process + "," + subProcess;
      var types = "String,String,String,String";
      Ext.core.DomHelper.append(document.body, {
        tag: "iframe",
        id: "downloadIframe",
        frameBorder: 0,
        width: 0,
        height: 0,
        css: "display:none;visibility:hidden;height:0px;",
        src:
          "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
          values +
          "&names=codeControl,idTypeProcess,idProcess,idSubprocess&types=" +
          types +
          "&nameReport=ROCTRL0001XLS"
      });
    },
    _onControlAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterControl grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditControl.htm"
      );
    },
    _onModifyControl: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterControl grid")[0];
      var record = grid.getSelectionModel().getSelection()[0];

      if (record === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        var win = Ext.create(
          "DukeSource.view.risk.parameter.controls.ViewWindowRegisterControl",
          {
            modal: true
          }
        );

        win
          .down("ViewComboJobPlace")
          .getStore()
          .load();

        if (record.get("typeProcess") !== "") {
          win
            .down("ViewComboProcessType")
            .getStore()
            .load({
              callback: function() {
                win
                  .down("ViewComboProcessType")
                  .setValue(record.get("typeProcess"));
                win
                  .down("ViewComboProcess")
                  .getStore()
                  .load({
                    url:
                      "http://localhost:9000/giro/showListProcessActives.htm",
                    params: {
                      valueFind: grid
                        .getSelectionModel()
                        .getSelection()[0]
                        .get("typeProcess")
                    },
                    callback: function() {
                      win
                        .down("ViewComboProcess")
                        .setValue(record.get("idProcess"));
                      win
                        .down("ViewComboSubProcess")
                        .getStore()
                        .load({
                          url:
                            "http://localhost:9000/giro/showListSubProcessActives.htm",
                          params: {
                            valueFind: record.get("idProcess")
                          }
                        });
                    }
                  });
              }
            });
        }
        if (
          record.get("typeControl") ===
          DukeSource.global.GiroConstants.MITIGATION_INSURANCE
        ) {
          win.down("#jobPlace").allowBlank = true;
          win.down("#jobPlace").setVisible(false);

          win.down("#codeControl").allowBlank = false;
          win.down("#codeControl").setReadOnly(false);
          win.down("#codeControl").setFieldStyle("background: #d9ffdb");
        } else {
          win.down("#jobPlace").allowBlank = false;
          win.down("#jobPlace").setVisible(true);

          win.down("#codeControl").allowBlank = true;
          win.down("#codeControl").setReadOnly(true);
          win.down("#codeControl").setFieldStyle("background: #e2e2e2");
        }

        win.down("ViewComboSubProcess").setValue(record.get("idSubProcess"));
        win.down("ViewComboProcess").setDisabled(false);
        win.down("ViewComboSubProcess").setDisabled(false);
        win
          .down("form")
          .getForm()
          .loadRecord(record);
        win.down("#description").setValue(record.get("descriptionControl"));
        win.show();
      }
    },
    _onSearchControlAdvanced: function(btn) {
      var panel = btn.up("form");
      var processType =
        panel.down("#processType").getValue() == undefined
          ? ""
          : panel.down("#processType").getValue();
      var process =
        panel.down("#process").getValue() == undefined
          ? ""
          : panel.down("#process").getValue();
      var subProcess =
        panel.down("#subProcess").getValue() == undefined
          ? ""
          : panel.down("#subProcess").getValue();
      var grid = panel.up("ViewPanelRegisterControl").down("grid");
      var values =
        panel.down("#codeControl").getValue() +
        "," +
        processType +
        "," +
        process +
        "," +
        subProcess;
      var fields =
        "c.codeControl,pt.idProcessType,p.idProcess,sp.id.idSubProcess";
      var types = "String,Integer,Integer,Integer";
      var operator = "equal,equal,equal,equal";
      grid.store.getProxy().extraParams = {
        fields: fields,
        values: values,
        types: types,
        operators: operator
      };
      grid.store.getProxy().url =
        "http://localhost:9000/giro/advancedSearchControl.htm";
      grid.down("pagingtoolbar").moveFirst();
    },
    _onRightClickRegisterControl: function(view, rec, node, index, e) {
      var app = this.application;
      var panel = Ext.ComponentQuery.query("ViewPanelRegisterControl")[0].down(
        "toolbar"
      );
      e.stopEvent();
      if (panel.down("#state").value) {
        var rowMenu = Ext.create("Ext.menu.Menu", {
          width: 140,
          items: [
            {
              text: "Nuevo",
              iconCls: "new",
              handler: function() {
                app
                  .getController(
                    "DukeSource.controller.risk.parameter.controls.ControllerPanelRegisterControl"
                  )
                  ._newControl();
              }
            },
            {
              text: "Evaluar",
              iconCls: "evaluate",
              handler: function() {
                app
                  .getController(
                    "DukeSource.controller.risk.parameter.controls.ControllerPanelRegisterControl"
                  )
                  ._onEvaluateControl();
              }
            },
            {
              text: "Modificar",
              iconCls: "modify",
              handler: function() {
                app
                  .getController(
                    "DukeSource.controller.risk.parameter.controls.ControllerPanelRegisterControl"
                  )
                  ._onModifyControl();
              }
            },
            {
              text: "Eliminar",
              iconCls: "delete",
              handler: function() {
                app
                  .getController(
                    "DukeSource.controller.risk.parameter.controls.ControllerPanelRegisterControl"
                  )
                  ._onDeleteControl();
              }
            }
          ]
        });
        rowMenu.showAt(e.getXY());
      }
    },
    _onNewVersionControl: function() {
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_WARNING,
        msg:
          "Se iniciara una nueva evaluación del control, tenga en cuenta que puede <br>" +
          "consultar evaluaciones anteriores, Desea continuar",
        icon: Ext.Msg.QUESTION,
        buttonText: {
          yes: "Si",
          no: "No"
        },
        buttons: Ext.MessageBox.YESNO,
        fn: function(btn) {
          if (btn === "yes") {
            var grid = Ext.ComponentQuery.query(
              "ViewPanelRegisterControl grid"
            )[0];
            var row = grid.getSelectionModel().getSelection()[0];
            Ext.ComponentQuery.query("ViewWindowControlEntry")[0].close();
            var windows = Ext.create(
              "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowControlEntry",
              {
                modal: true,
                origin: "catalogControl"
              }
            );
            windows.show();
            windows
              .down("form")
              .getForm()
              .loadRecord(row);
            DukeSource.lib.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/showListGroupControlTypeActives.htm",
              params: {
                propertyOrder: "description",
                start: "0",
                limit: "25"
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  for (var i = 0; i < response.data.length; i++) {
                    var scoreByGroup = {};
                    DukeSource.global.DirtyView.createGroup(scoreByGroup, response, i);
                    scoreByGroup.typeControl = row.get("typeControl");
                    windows.numberItems = response.data.length;
                    windows.groups = response.data;
                    DukeSource.global.DirtyView.createItemsToFinalScore(windows, scoreByGroup);
                  }

                  windows.setWidth(
                    response.data.length > 2 ? 310 * response.data.length : 760
                  );

                  var finalField = DukeSource.global.DirtyView.createScoreFinal(response);
                  DukeSource.global.DirtyView.createItemsToFinalScore(windows, finalField);
                  DukeSource.global.DirtyView.createButton(windows);
                } else {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              },
              failure: function() {}
            });
            var grid1 = windows.down("grid");
            grid1.store.getProxy().extraParams = {
              idControl: 0
            };
            grid1.store.getProxy().url =
              "http://localhost:9000/giro/showListControlTypeToEvaluateControl.htm";
            grid1.down("pagingtoolbar").moveFirst();
          } else {
          }
        }
      });
    },
    _onConsultVersionControl: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterControl grid")[0];
      var row = grid.getSelectionModel().getSelection()[0];
      var viewVersionControl = Ext.create(
        "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowConsultVersionControl",
        {
          modal: true
        }
      );
      viewVersionControl.show();
      viewVersionControl.down("grid").store.getProxy().extraParams = {
        idControl: row.get("idControl")
      };
      viewVersionControl.down("grid").store.getProxy().url =
        "http://localhost:9000/giro/showListVersionEvaluationControlActives.htm";
      viewVersionControl
        .down("grid")
        .down("pagingtoolbar")
        .moveFirst();
    }
  }
);
