Ext.define(
  "DukeSource.controller.risk.parameter.controls.ControllerPanelRegisterScoreControl",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterScoreControl"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterScoreControl"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterScoreControl",
      "risk.parameter.controls.ViewPanelRegisterScoreControl"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridScoreControl]": {
          keyup: this._onSearchTriggerGridScoreControl
        },
        "[action=exportScoreControlPdf]": {
          click: this._onExportScoreControlPdf
        },
        "[action=exportScoreControlExcel]": {
          click: this._onExportScoreControlExcel
        },
        "[action=scoreControlAuditory]": {
          click: this._onScoreControlAuditory
        },
        "[action=newScoreControl]": {
          click: this._newScoreControl
        },
        "[action=deleteScoreControl]": {
          click: this._onDeleteScoreControl
        },
        "[action=searchScoreControl]": {
          specialkey: this._searchScoreControl
        }
      });
    },
    _newScoreControl: function() {
      var modelScoreControl = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterScoreControl",
        {
          idScoreControl: "id",
          description: "",
          codeColour: "",
          minEffectiveness: "",
          maxEffectiveness: "",
          scoreControl: "",
          state: "S",
          scoreMin: "0.00",
          scoreMax: "0.00"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterScoreControl"
      )[0];
      var panel = general.down("ViewGridPanelRegisterScoreControl");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelScoreControl);
      editor.startEdit(0, 0);
    },
    _onDeleteScoreControl: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterScoreControl grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteScoreControl.htm?nameView=ViewPanelRegisterScoreControl"
      );
    },
    _searchScoreControl: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterScoreControl grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findScoreControl.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridScoreControl: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterScoreControl grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportScoreControlPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportScoreControlExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onScoreControlAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterScoreControl grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditScoreControl.htm"
      );
    }
  }
);
