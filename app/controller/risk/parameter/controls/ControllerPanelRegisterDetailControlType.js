Ext.define(
  "DukeSource.controller.risk.parameter.controls.ControllerPanelRegisterDetailControlType",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterDetailControlType"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterDetailControlType"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterDetailControlType",
      "risk.parameter.controls.ViewPanelRegisterDetailControlType"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridDetailControlType]": {
          keyup: this._onSearchTriggerGridDetailControlType
        },
        "[action=exportDetailControlTypePdf]": {
          click: this._onExportDetailControlTypePdf
        },
        "[action=exportDetailControlTypeExcel]": {
          click: this._onExportDetailControlTypeExcel
        },
        "[action=detailControlTypeAuditory]": {
          click: this._onDetailControlTypeAuditory
        },
        "[action=newDetailControlType]": {
          click: this._newDetailControlType
        },
        "[action=deleteDetailControlType]": {
          click: this._onDeleteDetailControlType
        },
        "[action=searchDetailControlType]": {
          specialkey: this._searchDetailControlType
        }
      });
    },
    _newDetailControlType: function() {
      var modelDetailControlType = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterDetailControlType",
        {
          idDetailControlType: "id",
          controlType: "",
          valueTypificationControl: "",
          typificationControl: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailControlType"
      )[0];
      var panel = general.down("ViewGridPanelRegisterDetailControlType");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelDetailControlType);
      editor.startEdit(0, 0);
    },
    _onDeleteDetailControlType: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailControlType grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteDetailControlType.htm"
      );
    },
    _searchDetailControlType: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterDetailControlType grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findDetailControlType.htm",
          "typificationControl",
          "typificationControl"
        );
      } else {
      }
    },
    _onSearchTriggerGridDetailControlType: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailControlType grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportDetailControlTypePdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportDetailControlTypeExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onDetailControlTypeAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailControlType grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditDetailControlType.htm"
      );
    }
  }
);
