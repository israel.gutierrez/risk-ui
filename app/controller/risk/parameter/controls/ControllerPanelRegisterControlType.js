Ext.define(
  "DukeSource.controller.risk.parameter.controls.ControllerPanelRegisterControlType",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreTreeGridPanelRegisterControlType"],
    models: ["risk.parameter.grids.ModelTreeGridPanelRegisterControlType"],
    views: [
      "risk.parameter.grids.ViewTreeGridPanelRegisterControlType",
      "risk.parameter.controls.ViewPanelRegisterControlType"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridControlType]": {
          keyup: this._onSearchTriggerGridControlType
        },
        "[action=exportControlTypePdf]": {
          click: this._onExportControlTypePdf
        },
        "[action=exportControlTypeExcel]": {
          click: this._onExportControlTypeExcel
        },
        "[action=controlTypeAuditory]": {
          click: this._onControlTypeAuditory
        },
        "[action=newControlType]": {
          click: this._newControlType
        },
        "[action=deleteControlType]": {
          click: this._onDeleteControlType
        },
        "[action=searchControlType]": {
          specialkey: this._searchControlType
        },
        ViewTreeGridPanelRegisterControlType: {
          itemcontextmenu: this.treeRightClickControlType
        },
        "AddMenuControlTypeGroup menuitem[text=Agregar]": {
          click: this._addControlType
        },
        "AddMenuControlTypeGroup menuitem[text=Editar]": {
          click: this._editControlTypeGroup
        },
        "AddMenuControlTypeGroup menuitem[text=Eliminar]": {
          click: this._deleteControlType
        },
        "AddMenuControlType menuitem[text=Agregar]": {
          click: this._addDetailControlType
        },
        "AddMenuControlType menuitem[text=Editar]": {
          click: this._editControlType
        },
        "AddMenuControlType menuitem[text=Eliminar]": {
          click: this._deleteControlType
        },
        "[action=saveControlTypeGroup]": {
          click: this._onSaveControlTypeGroup
        },
        "[action=saveControlType]": {
          click: this._onSaveControlType
        },
        "EditMenuControlType menuitem[text=Editar]": {
          click: this._editDetailControlType
        },
        "EditMenuControlType menuitem[text=Eliminar]": {
          click: this._deleteControlType
        }
      });
    },
    _newControlType: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowControlTypeGroup",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getComponent("id")
        .setValue("myTree/id");
      win.show();
      win.down("#text").focus(false, 200);
    },
    _onDeleteControlType: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterControlType grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteControlType.htm"
      );
    },
    _searchControlType: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterControlType grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findControlType.htm",
          "description",
          "idControl"
        );
      } else {
      }
    },
    _onSearchTriggerGridControlType: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterControlType grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportControlTypePdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportControlTypeExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onControlTypeAuditory: function(button) {
      var treeGrid = button
        .up("panel")
        .down("ViewTreeGridPanelRegisterControlType");
      if (treeGrid.getSelectionModel().getCount() == 0) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        var id = treeGrid
          .getSelectionModel()
          .getSelection()[0]
          .get("id");
        var array = id.split("/");
        if (array.length == 2) {
          Ext.Ajax.request({
            method: "POST",
            url: "http://localhost:9000/giro/findAuditGroupControlType.htm",
            params: {
              idGroupControlType: array[1]
            },
            success: function(response) {
              response = Ext.decode(response.responseText);
              if (response.success) {
                Ext.create("DukeSource.view.risk.util.WindowAuditory", {
                  modal: true
                }).show();
                var form = Ext.ComponentQuery.query("WindowAuditory form")[0];
                form.getForm().setValues(response.data);
              } else {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_ERROR,
                  response.mensaje,
                  Ext.Msg.ERROR
                );
              }
            },
            failure: function() {}
          });
        } else if (array.length == 3) {
          Ext.Ajax.request({
            method: "POST",
            url: "http://localhost:9000/giro/findAuditControlType.htm",
            params: {
              idControlType: array[2]
            },
            success: function(response) {
              response = Ext.decode(response.responseText);
              if (response.success) {
                Ext.create("DukeSource.view.risk.util.WindowAuditory", {
                  modal: true
                }).show();
                var form = Ext.ComponentQuery.query("WindowAuditory form")[0];
                form.getForm().setValues(response.data);
              } else {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_ERROR,
                  response.mensaje,
                  Ext.Msg.ERROR
                );
              }
            },
            failure: function() {}
          });
        } else if (array.length == 4) {
          Ext.Ajax.request({
            method: "POST",
            url: "http://localhost:9000/giro/findAuditDetailControlType.htm",
            params: {
              idDetailControlType: array[3]
            },
            success: function(response) {
              response = Ext.decode(response.responseText);
              if (response.success) {
                Ext.create("DukeSource.view.risk.util.WindowAuditory", {
                  modal: true
                }).show();
                var form = Ext.ComponentQuery.query("WindowAuditory form")[0];
                form.getForm().setValues(response.data);
              } else {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_ERROR,
                  response.mensaje,
                  Ext.Msg.ERROR
                );
              }
            },
            failure: function() {}
          });
        }
      }
    },
    treeRightClickControlType: function(view, record, item, index, e) {
      e.stopEvent();
      this.application.currentRecord = record;
      if (record.get("depth") === 1) {
        var addMenu = Ext.create(
          "DukeSource.view.risk.parameter.controls.AddMenuControlTypeGroup",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      if (record.get("depth") === 2) {
        var addMenu = Ext.create(
          "DukeSource.view.risk.parameter.controls.AddMenuControlType",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      if (record.get("depth") === 3) {
        var addMenu = Ext.create(
          "DukeSource.view.risk.parameter.controls.EditMenuControlType",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      return false;
    },
    _addControlType: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowControlType",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id") + "/id");
      win.show();
      win.down("#text").focus(false, 200);
    },
    _editControlTypeGroup: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowControlTypeGroup",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id"));
      win
        .down("form")
        .down("#typeControl")
        .setValue(this.application.currentRecord.get("typeControl"));
      win
        .down("form")
        .getComponent("text")
        .setValue(this.application.currentRecord.get("text"));
      win
        .down("form")
        .getComponent("percentage")
        .setValue(this.application.currentRecord.get("percentage"));
      win
        .down("#abbreviation")
        .setValue(this.application.currentRecord.get("abbreviation"));
      win.down("#text").focus(false, 200);
      win.show();
      win.down("#text").focus(false, 200);
    },

    _addDetailControlType: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowDetailControlType",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id") + "/id");
      win.show();
      win.down("#text").focus(false, 200);
    },
    _onSaveControlTypeGroup: function() {
      var win = Ext.ComponentQuery.query("ViewWindowControlTypeGroup")[0];
      var form = win.down("form");
      if (form.getForm().isValid()) {
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveControlType.htm?nameView=ViewPanelRegisterControlType",
          params: {
            id: form.getComponent("id").getValue(),
            typeControl: form.getComponent("typeControl").getValue(),
            description: form.getComponent("text").getValue(),
            abbreviation: form.getComponent("abbreviation").getValue(),
            percentage: form.down("#percentage").getValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              var refreshNode = Ext.ComponentQuery.query(
                "ViewPanelRegisterControlType ViewTreeGridPanelRegisterControlType"
              )[0]
                .getStore()
                .getNodeById(response.data);
              refreshNode.removeAll(false);
              Ext.ComponentQuery.query(
                "ViewPanelRegisterControlType ViewTreeGridPanelRegisterControlType"
              )[0]
                .getStore()
                .load({
                  node: refreshNode
                });
              win.close();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onSaveControlType: function(button) {
      var window = button.up("window");
      var form = window.down("form");
      if (form.getForm().isValid()) {
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/saveControlType.htm",
          params: {
            id: form.getComponent("id").getValue(),
            description: form.getComponent("text").getValue(),
            weightedControl: form.getComponent("weightedControl").getValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              var refreshNode = Ext.ComponentQuery.query(
                "ViewPanelRegisterControlType ViewTreeGridPanelRegisterControlType"
              )[0]
                .getStore()
                .getNodeById(response.data);
              refreshNode.removeAll(false);
              Ext.ComponentQuery.query(
                "ViewPanelRegisterControlType ViewTreeGridPanelRegisterControlType"
              )[0]
                .getStore()
                .load({
                  node: refreshNode
                });
              window.close();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _editControlType: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowControlType",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id"));
      win
        .down("form")
        .getComponent("text")
        .setValue(this.application.currentRecord.get("text"));
      win
        .down("form")
        .getComponent("weightedControl")
        .setValue(this.application.currentRecord.get("fieldOne"));
      win.show();
    },
    _deleteControlType: function() {
      var rec = this.application.currentRecord.get("id");
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
        msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
        icon: Ext.Msg.QUESTION,
        buttonText: {
          yes: "Si"
        },
        buttons: Ext.MessageBox.YESNO,
        fn: function(btn) {
          if (btn == "yes") {
            Ext.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/deleteControlType.htm",
              params: {
                id: rec
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  var refreshNode = Ext.ComponentQuery.query(
                    "ViewPanelRegisterControlType ViewTreeGridPanelRegisterControlType"
                  )[0]
                    .getStore()
                    .getNodeById(response.data);
                  refreshNode.removeAll(false);
                  Ext.ComponentQuery.query(
                    "ViewPanelRegisterControlType ViewTreeGridPanelRegisterControlType"
                  )[0]
                    .getStore()
                    .load({
                      node: refreshNode
                    });
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_WARNING,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
          }
        }
      });
    },
    _editDetailControlType: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowDetailControlType",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id"));
      win
        .down("form")
        .getComponent("text")
        .setValue(this.application.currentRecord.get("text"));
      win
        .down("form")
        .getComponent("weightedControl")
        .setValue(this.application.currentRecord.get("fieldOne"));
      win.show();
    }
  }
);
