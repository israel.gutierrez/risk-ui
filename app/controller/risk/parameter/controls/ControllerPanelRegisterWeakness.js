Ext.define(
  "DukeSource.controller.risk.parameter.controls.ControllerPanelRegisterWeakness",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterWeakness"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterWeakness"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterWeakness",
      "risk.parameter.controls.ViewPanelRegisterWeakness"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridWeakness]": {
          keyup: this._onSearchTriggerGridWeakness
        },
        "[action=exportWeaknessPdf]": {
          click: this._onExportWeaknessPdf
        },
        "[action=exportWeaknessExcel]": {
          click: this._onExportWeaknessExcel
        },
        "[action=weaknessAuditory]": {
          click: this._onWeaknessAuditory
        },
        "[action=newWeakness]": {
          click: this._newWeakness
        },
        "[action=deleteWeakness]": {
          click: this._onDeleteWeakness
        },
        "[action=searchWeakness]": {
          specialkey: this._searchWeakness
        }
      });
    },
    _newWeakness: function() {
      var modelWeakness = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterWeakness",
        {
          idWeakness: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterWeakness")[0];
      var panel = general.down("ViewGridPanelRegisterWeakness");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelWeakness);
      editor.startEdit(0, 0);
    },

    _onDeleteWeakness: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterWeakness grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteWeakness.htm?nameView=ViewPanelRegisterWeakness"
      );
    },
    _searchWeakness: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterWeakness grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findWeakness.htm",
          "description",
          "idWeakness"
        );
      } else {
      }
    },
    _onSearchTriggerGridWeakness: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterWeakness grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportWeaknessPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportWeaknessExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onWeaknessAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterWeakness grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditWeakness.htm"
      );
    }
  }
);
