Ext.define(
  "DukeSource.controller.risk.parameter.equity.ControllerPanelRegisterAdjustmentFactorBusiness",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.parameter.grids.StoreGridPanelRegisterAdjustmentFactorBusiness"
    ],
    models: [
      "risk.parameter.grids.ModelGridPanelRegisterAdjustmentFactorBusiness"
    ],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterAdjustmentFactorBusiness",
      "risk.parameter.equity.ViewPanelRegisterAdjustmentFactorBusiness"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridAdjustmentFactorBusiness]": {
          keyup: this._onSearchTriggerGridAdjustmentFactorBusiness
        },
        "[action=exportAdjustmentFactorBusinessPdf]": {
          click: this._onExportAdjustmentFactorBusinessPdf
        },
        "[action=exportAdjustmentFactorBusinessExcel]": {
          click: this._onExportAdjustmentFactorBusinessExcel
        },
        "[action=adjustmentFactorBusinessAuditory]": {
          click: this._onAdjustmentFactorBusinessAuditory
        },
        "[action=newAdjustmentFactorBusiness]": {
          click: this._newAdjustmentFactorBusiness
        },
        "[action=deleteAdjustmentFactorBusiness]": {
          click: this._onDeleteAdjustmentFactorBusiness
        },
        "[action=searchAdjustmentFactorBusiness]": {
          specialkey: this._searchAdjustmentFactorBusiness
        }
      });
    },
    _newAdjustmentFactorBusiness: function() {
      var modelAdjustmentFactorBusiness = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterAdjustmentFactorBusiness",
        {
          idAdjustmentFactorBusiness: "id",
          titleReport: "",
          groupReport: "",
          fixedFactor: "",
          adjustedFactor: "",
          fixedAdjusted: "",
          dateStartValidity: "",
          dateEndValidity: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterAdjustmentFactorBusiness"
      )[0];
      var panel = general.down("ViewGridPanelRegisterAdjustmentFactorBusiness");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelAdjustmentFactorBusiness);
      editor.startEdit(0, 0);
    },
    _onDeleteAdjustmentFactorBusiness: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterAdjustmentFactorBusiness grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteAdjustmentFactorBusiness.htm?nameView=ViewPanelRegisterAdjustmentFactorBusiness"
      );
    },
    _searchAdjustmentFactorBusiness: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterAdjustmentFactorBusiness grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findAdjustmentFactorBusiness.htm",
          "state",
          "state"
        );
      } else {
      }
    },
    _onSearchTriggerGridAdjustmentFactorBusiness: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterAdjustmentFactorBusiness grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportAdjustmentFactorBusinessPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportAdjustmentFactorBusinessExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onAdjustmentFactorBusinessAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterAdjustmentFactorBusiness grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditAdjustmentFactorBusiness.htm"
      );
    }
  }
);
