Ext.define(
  "DukeSource.controller.risk.parameter.equity.ControllerPanelRegisterAdjustmentFactorProvitional",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.parameter.grids.StoreGridPanelRegisterAdjustmentFactorProvitional"
    ],
    models: [
      "risk.parameter.grids.ModelGridPanelRegisterAdjustmentFactorProvitional"
    ],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterAdjustmentFactorProvitional",
      "risk.parameter.equity.ViewPanelRegisterAdjustmentFactorProvitional"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridAdjustmentFactorProvitional]": {
          keyup: this._onSearchTriggerGridAdjustmentFactorProvitional
        },
        "[action=exportAdjustmentFactorProvitionalPdf]": {
          click: this._onExportAdjustmentFactorProvitionalPdf
        },
        "[action=exportAdjustmentFactorProvitionalExcel]": {
          click: this._onExportAdjustmentFactorProvitionalExcel
        },
        "[action=adjustmentFactorProvitionalAuditory]": {
          click: this._onAdjustmentFactorProvitionalAuditory
        },
        "[action=newAdjustmentFactorProvitional]": {
          click: this._newAdjustmentFactorProvitional
        },
        "[action=deleteAdjustmentFactorProvitional]": {
          click: this._onDeleteAdjustmentFactorProvitional
        },
        "[action=searchAdjustmentFactorProvitional]": {
          specialkey: this._searchAdjustmentFactorProvitional
        }
      });
    },
    _newAdjustmentFactorProvitional: function() {
      var modelAdjustmentFactorProvitional = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterAdjustmentFactorProvitional",
        {
          idAdjustmentFactor: "id",
          valueAdjustment: "",
          dateStartValidity: "",
          dateEndValidity: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterAdjustmentFactorProvitional"
      )[0];
      var panel = general.down(
        "ViewGridPanelRegisterAdjustmentFactorProvitional"
      );
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelAdjustmentFactorProvitional);
      editor.startEdit(0, 0);
    },
    _onDeleteAdjustmentFactorProvitional: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterAdjustmentFactorProvitional grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteAdjustmentFactorProvitional.htm?nameView=ViewPanelRegisterAdjustmentFactorProvitional"
      );
    },
    _searchAdjustmentFactorProvitional: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterAdjustmentFactorProvitional grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findAdjustmentFactorProvitional.htm",
          "state",
          "state"
        );
      } else {
      }
    },
    _onSearchTriggerGridAdjustmentFactorProvitional: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterAdjustmentFactorProvitional grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportAdjustmentFactorProvitionalPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportAdjustmentFactorProvitionalExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onAdjustmentFactorProvitionalAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterAdjustmentFactorProvitional grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditAdjustmentFactorProvitional.htm"
      );
    }
  }
);
