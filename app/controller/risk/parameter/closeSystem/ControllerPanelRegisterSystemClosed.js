Ext.define(
  "DukeSource.controller.risk.parameter.closeSystem.ControllerPanelRegisterSystemClosed",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterSystemClosed"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterSystemClosed"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterSystemClosed",
      "risk.parameter.closeSystem.ViewPanelRegisterSystemClosed"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridSystemClosed]": {
          keyup: this._onSearchTriggerGridSystemClosed
        },
        "[action=saveCloseSystem]": {
          click: this._onSaveCloseSystem
        },
        "[action=modifySystemClosed]": {
          click: this.onModifySystemClosed
        },
        "[action=systemClosedAuditory]": {
          click: this._onSystemClosedAuditory
        },
        "[action=newSystemClosed]": {
          click: this._newSystemClosed
        },
        "[action=deleteSystemClosed]": {
          click: this._onDeleteSystemClosed
        },
        "[action=searchSystemClosed]": {
          specialkey: this._searchSystemClosed
        }
      });
    },
    _newSystemClosed: function() {
      Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowCloseSystem",
        {
          modal: true
        }
      ).show();
    },
    _onSaveCloseSystem: function(btn) {
      var win = btn.up("window");
      if (
        win
          .down("form")
          .getForm()
          .isValid()
      ) {
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveSystemClosed.htm?nameView=ViewPanelRegisterSystemClosed",
          params: {
            jsonData: Ext.JSON.encode(
              win
                .down("form")
                .getForm()
                .getValues()
            )
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              var grid = Ext.ComponentQuery.query(
                "ViewPanelRegisterSystemClosed"
              )[0].down("grid");
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                response.mensaje,
                Ext.Msg.INFO
              );
              DukeSource.global.DirtyView.searchPaginationGridNormal(
                "",
                grid,
                grid.down("pagingtoolbar"),
                "http://localhost:9000/giro/findSystemClosed.htm",
                "description",
                "description"
              );
              win.close();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    onModifySystemClosed: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSystemClosed"
      )[0].down("grid");
      if (grid.getSelectionModel().getCount() == 0) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        var win = Ext.create(
          "DukeSource.view.risk.parameter.windows.ViewWindowCloseSystem",
          {
            modal: true
          }
        );
        win
          .down("form")
          .getForm()
          .loadRecord(grid.getSelectionModel().getSelection()[0]);
        win.show();
      }
    },
    _onDeleteSystemClosed: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSystemClosed grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteSystemClosed.htm?nameView=ViewPanelRegisterSystemClosed"
      );
    },
    _searchSystemClosed: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterSystemClosed grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findSystemClosed.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridSystemClosed: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSystemClosed grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },

    _onSystemClosedAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSystemClosed grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditSystemClosed.htm"
      );
    }
  }
);
