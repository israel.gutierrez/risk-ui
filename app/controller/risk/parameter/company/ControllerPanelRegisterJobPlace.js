Ext.define(
  "DukeSource.controller.risk.parameter.company.ControllerPanelRegisterJobPlace",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterJobPlace"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterJobPlace"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterJobPlace",
      "risk.parameter.company.ViewPanelRegisterJobPlace"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridJobPlace]": {
          keyup: this._onSearchTriggerGridJobPlace
        },
        "[action=exportJobPlacePdf]": {
          click: this._onExportJobPlacePdf
        },
        "[action=exportJobPlaceExcel]": {
          click: this._onExportJobPlaceExcel
        },
        "[action=jobPlaceAuditory]": {
          click: this._onJobPlaceAuditory
        },
        "[action=newJobPlace]": {
          click: this._newJobPlace
        },
        "[action=deleteJobPlace]": {
          click: this._onDeleteJobPlace
        },
        "[action=searchJobPlace]": {
          specialkey: this._searchJobPlace
        }
      });
    },
    _newJobPlace: function() {
      var modelJobPlace = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterJobPlace",
        {
          idJobPlace: "id",
          workArea: "",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterJobPlace")[0];
      var panel = general.down("ViewGridPanelRegisterJobPlace");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelJobPlace);
      editor.startEdit(0, 0);
    },
    _onDeleteJobPlace: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterJobPlace grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteJobPlace.htm"
      );
    },
    _searchJobPlace: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterJobPlace grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findJobPlace.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridJobPlace: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterJobPlace grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportJobPlacePdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportJobPlaceExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onJobPlaceAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterJobPlace grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditJobPlace.htm"
      );
    }
  }
);
