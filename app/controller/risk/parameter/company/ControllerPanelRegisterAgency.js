Ext.define(
  "DukeSource.controller.risk.parameter.company.ControllerPanelRegisterAgency",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterAgency"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterAgency"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterAgency",
      "risk.parameter.company.ViewPanelRegisterAgency"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridAgency]": {
          keyup: this._onSearchTriggerGridAgency
        },
        "[action=exportAgencyPdf]": {
          click: this._onExportAgencyPdf
        },
        "[action=exportAgencyExcel]": {
          click: this._onExportAgencyExcel
        },
        "[action=agencyAuditory]": {
          click: this._onAgencyAuditory
        },
        "[action=newAgency]": {
          click: this._newAgency
        },
        "[action=deleteAgency]": {
          click: this._onDeleteAgency
        },
        "[action=searchAgency]": {
          specialkey: this._searchAgency
        }
      });
    },
    _newAgency: function() {
      var modelAgency = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterAgency",
        {
          idAgency: "id",
          company: "",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterAgency")[0];
      var panel = general.down("ViewGridPanelRegisterAgency");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelAgency);
      editor.startEdit(0, 0);
    },
    _onDeleteAgency: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterAgency grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteAgency.htm"
      );
    },
    _searchAgency: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query("ViewPanelRegisterAgency grid")[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findAgency.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridAgency: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterAgency grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportAgencyPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportAgencyExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onAgencyAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterAgency grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditAgency.htm"
      );
    }
  }
);
