Ext.define(
  "DukeSource.controller.risk.parameter.company.ControllerPanelRegisterCompany",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreTreeGridPanelRegisterCompany"],
    models: ["risk.parameter.grids.ModelTreeGridPanelRegisterCompany"],
    views: ["risk.parameter.grids.ViewTreeGridPanelRegisterCompany"],

    init: function() {
      this.control({
        ViewTreeGridPanelRegisterCompany: {
          itemcontextmenu: this.treeRightClickCompany
        },
        "AddMenuRegion menuitem[text=Agregar]": {
          click: this._addRegion
        },
        "AddMenuRegion menuitem[text=Editar]": {
          click: this._editCompany
        },
        "AddMenuRegion menuitem[text=Eliminar]": {
          click: this._deleteCompany
        },
        "EditMenuRegion menuitem[text=Agregar]": {
          click: this._addAgency
        },
        "EditMenuRegion menuitem[text=Editar]": {
          click: this._editRegion
        },
        "EditMenuRegion menuitem[text=Eliminar]": {
          click: this._deleteCompany
        },
        "EditMenuAgency menuitem[text=Editar]": {
          click: this._editAgency
        },
        "EditMenuAgency menuitem[text=Eliminar]": {
          click: this._deleteCompany
        },
        "[action=saveCompany]": {
          click: this._onSaveCompany
        }
      });
    },
    treeRightClickCompany: function(view, record, item, index, e) {
      e.stopEvent();

      this.application.currentRecord = record;
      var addMenu;

      if (record.get("depth") === 1) {
        addMenu = Ext.create(
          "DukeSource.view.risk.parameter.company.AddMenuRegion",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      if (record.get("depth") === 2) {
        addMenu = Ext.create(
          "DukeSource.view.risk.parameter.company.EditMenuRegion",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      if (record.get("depth") === 3) {
        addMenu = Ext.create(
          "DukeSource.view.risk.parameter.company.EditMenuAgency",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      return false;
    },
    _addRegion: function() {
      var view = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowRegion",
        {
          modal: true
        }
      );
      view
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id") + "-id");
      view.show();
      view.down("#text").focus(false, 200);
    },
    _editCompany: function() {
      var view = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowCompany",
        {
          modal: true
        }
      );
      view
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id"));
      view
        .down("form")
        .getComponent("text")
        .setValue(this.application.currentRecord.get("text"));
      view.show();
      view.down("#text").focus(false, 200);
    },
    _addAgency: function() {
      var view = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowAgency",
        {
          modal: true
        }
      );
      view
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id") + "-id");
      view.down("#text").focus(false, 200);
      view.show();
      view.down("#text").focus(false, 200);
    },
    _editRegion: function() {
      var view = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowRegion",
        {
          modal: true
        }
      );
      view
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id"));
      view
        .down("form")
        .getComponent("text")
        .setValue(this.application.currentRecord.get("text"));
      view
        .down("form")
        .down("#location")
        .setValue(this.application.currentRecord.get("location"));
      view.show();
      view.down("#text").focus(false, 200);
    },
    _editAgency: function() {
      var view = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowAgency",
        {
          modal: true
        }
      );
      view
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id"));
      view
        .down("form")
        .getComponent("text")
        .setValue(this.application.currentRecord.get("text"));
      view.down("#text").focus(false, 200);
      view.show();
    },
    _deleteCompany: function() {
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterCompany"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];
      var idNode = this.application.currentRecord.get("id");

      if (node === undefined) {
        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.MESSAGE_ITEM);
      } else {
        Ext.MessageBox.show({
          title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
          msg: "Esta seguro que desea eliminar el item?",
          icon: Ext.Msg.QUESTION,
          buttonText: {
            yes: "Si"
          },
          buttons: Ext.MessageBox.YESNO,
          fn: function(btn) {
            if (btn === "yes") {
              DukeSource.lib.Ajax.request({
                method: "POST",
                url: "http://localhost:9000/giro/deleteCompany.htm",
                params: {
                  id: idNode
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    tree.store.getProxy().extraParams = {
                      depth: node.data["depth"]
                    };
                    tree.store.load({
                      node: node.parentNode
                    });

                    DukeSource.global.DirtyView.messageNormal(response.message);
                  } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                  }
                },
                failure: function() {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              });
            }
          }
        });
      }
    },
    _onSaveCompany: function(button) {
      var win = button.up("window");
      var form = win.down("form");
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterCompany"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];

      if (form.getForm().isValid()) {
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/saveCompany.htm",
          params: {
            id: form.getComponent("id").getValue(),
            description: form.getComponent("text").getValue(),
            fieldOne:
              form.getComponent("fieldOne") === undefined
                ? ""
                : form.getComponent("fieldOne").getValue(),
            location: form.down("#location").getValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              tree.store.getProxy().extraParams = {
                depth: node.data["depth"]
              };
              tree.store.load({
                node: node.parentNode
              });

              DukeSource.global.DirtyView.messageNormal(response.message);
              win.close();
            } else {
              DukeSource.global.DirtyView.messageWarning(response.message);
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
      }
    }
  }
);
