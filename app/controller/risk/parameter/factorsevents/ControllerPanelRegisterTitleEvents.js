Ext.define(
  "DukeSource.controller.risk.parameter.factorsevents.ControllerPanelRegisterTitleEvents",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterTitleEvents"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterTitleEvents"],
    views: [
      "risk.parameter.grids.ViewGridPanelTitleEvents",
      "risk.parameter.factorsevents.ViewPanelRegisterTitleEvents"
    ],

    init: function() {
      this.control({
        "[action=newTitleEvents]": {
          click: this._newTitleEvents
        },
        "[action=deleteTitleEvents]": {
          click: this._onDeleteTitleEvents
        },
        "[action=searchTitleEvents]": {
          specialkey: this._searchTitleEvents
        }
      });
    },

    _newTitleEvents: function() {
      var modelTypeEffect = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterTitleEvents",
        {
          id: "Id",
          abbreviation: "ABREV",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterTitleEvents")[0];
      var panel = general.down("ViewGridPanelTitleEvents");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelTypeEffect);
      editor.startEdit(0, 0);
    },

    _onDeleteTitleEvents: function() {
      var general = Ext.ComponentQuery.query("ViewPanelRegisterTitleEvents")[0];
      var grid = general.down("ViewGridPanelTitleEvents");
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteEventTittle.htm?nameView=ViewPanelRegisterTitleEvents"
      );
    },

    _searchTitleEvents: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterTitleEvents grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findEventTittle.htm",
          "description",
          "description"
        );
      } else {
      }
    },

    _onSearchTriggerGridFactorRisk: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterFactorRisk grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },

    _onExportFactorRiskPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },

    _onExportFactorRiskExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },

    _onFactorRiskAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterFactorRisk grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditFactorRisk.htm"
      );
    }
  }
);
