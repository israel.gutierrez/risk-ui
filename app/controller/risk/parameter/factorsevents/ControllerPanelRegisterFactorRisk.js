Ext.define(
  "DukeSource.controller.risk.parameter.factorsevents.ControllerPanelRegisterFactorRisk",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterFactorRisk"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterFactorRisk"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterFactorRisk",
      "risk.parameter.factorsevents.ViewPanelRegisterFactorRisk"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridFactorRisk]": {
          keyup: this._onSearchTriggerGridFactorRisk
        },
        "[action=modifyFactorRisk]": {
          click: this._onModifyFactorRisk
        },
        "[action=saveFactorRisk]": {
          click: this._onSaveFactorRisk
        },
        "[action=factorRiskAuditory]": {
          click: this._onFactorRiskAuditory
        },
        "[action=newFactorRisk]": {
          click: this._newFactorRisk
        },
        "[action=deleteFactorRisk]": {
          click: this._onDeleteFactorRisk
        },
        "[action=searchFactorRisk]": {
          specialkey: this._searchFactorRisk
        },
        ViewGridPanelRegisterFactorRisk: {
          itemcontextmenu: this._rightClickFactorRisk
        }
      });
    },
    _newFactorRisk: function() {
      var tree = Ext.ComponentQuery.query("ViewGridPanelRegisterFactorRisk")[0];
      var node = tree.getSelectionModel().getSelection()[0];

      if (node === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        var window = Ext.create(
          "DukeSource.view.risk.parameter.windows.WindowRegisterFactorRisk",
          {
            modal: true,
            origin: "new",
            prefix: node.getPath("description", " &#8702; ").substring(18),
            title: "AGREGAR EN: " + node.data["description"]
          }
        );
        window.down("#parent").setValue(node.data["id"]);
        window.down("#depth").setValue(node.data["depth"] + 1);
        window.show();
        window.down("#description").focus(false, 100);
      }
    },
    _onDeleteFactorRisk: function() {
      var tree = Ext.ComponentQuery.query("ViewGridPanelRegisterFactorRisk")[0];
      var node = tree.getSelectionModel().getSelection()[0];
      if (node === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      }
      if (node.data["depth"] == 1) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          "NO PUEDE SER ELIMINADO, ES UN REGISTRO BASE",
          Ext.Msg.WARNING
        );
      } else {
        Ext.MessageBox.show({
          title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
          msg: "Esta seguro que desea eliminar el item?",
          icon: Ext.Msg.QUESTION,
          buttonText: {
            yes: "Si"
          },
          buttons: Ext.MessageBox.YESNO,
          fn: function(btn) {
            if (btn === "yes") {
              DukeSource.lib.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/deleteFactorRisk.htm?nameView=ViewPanelRegisterFactorRisk",
                params: {
                  node: Ext.JSON.encode(node.data)
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    tree.store.getProxy().extraParams = {
                      depth: node.data["depth"]
                    };
                    tree.store.load({
                      node: node.parentNode
                    });
                    tree.getSelectionModel().deselectAll();
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_CONFIRM,
                      response.mensaje,
                      Ext.Msg.INFO
                    );
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      response.mensaje,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function() {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_WARNING,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              });
            }
          }
        });
      }
    },

    _searchFactorRisk: function(field, e) {
      var me = field.up("panel").down("treepanel");

      if (e.getKey() === e.ENTER) {
        field.getValue() === ""
          ? me.collapseAll()
          : me.filterByText(field.getValue());
      }
    },
    _onSearchTriggerGridFactorRisk: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterFactorRisk grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onModifyFactorRisk: function() {
      var tree = Ext.ComponentQuery.query("ViewGridPanelRegisterFactorRisk")[0];
      var node = tree.getSelectionModel().getSelection()[0];
      if (node === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      }
      if (node.data["depth"] == 1) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          "NO PUEDE SER MODIFICADO, ES UN REGISTRO BASE",
          Ext.Msg.WARNING
        );
      } else {
        var view = Ext.create(
          "DukeSource.view.risk.parameter.windows.WindowRegisterFactorRisk",
          {
            title: "MODIFICAR: " + node.data["description"],
            prefix: node.parentNode
              .getPath("description", " &#8702; ")
              .substring(18),
            origin: "modify",
            modal: true
          }
        );
        view
          .down("form")
          .getForm()
          .setValues(node.raw);
        view.down("#depth").setValue(node.data["depth"]);
        view.show();
        view.down("#description").focus(false, 100);
      }
    },
    _onSaveFactorRisk: function(btn) {
      var tree = Ext.ComponentQuery.query("ViewGridPanelRegisterFactorRisk")[0];
      var node = tree.getSelectionModel().getSelection()[0];
      var windows = btn.up("window");
      var form = windows.down("form");
      form
        .down("#path")
        .setValue(
          windows.prefix + " &#8702; " + form.down("#description").getValue()
        );

      if (form.getForm().isValid()) {
        DukeSource.lib.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveFactorRisk.htm?nameView=ViewPanelRegisterFactorRisk",
          params: {
            jsonData: Ext.JSON.encode(form.getForm().getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            var depth = node.data["depth"];
            if (depth == 1) {
              depth = 2;
            }

            if (response.success) {
              tree.store.getProxy().extraParams = {
                depth: depth
              };
              tree.store.load({
                node: windows.origin == "new" ? node : node.parentNode
              });
              windows.close();
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_CONFIRM,
                response.message,
                Ext.Msg.INFO
              );
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.message,
                Ext.Msg.WARNING
              );
            }
          },
          failure: function() {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              response.message,
              Ext.Msg.WARNING
            );
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.WARNING
        );
      }
    },
    _onFactorRiskAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterFactorRisk grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditFactorRisk.htm"
      );
    },

    _rightClickFactorRisk: function(view, rec, node, index, e) {
      e.stopEvent();
      var addMenu = Ext.create("DukeSource.view.fulfillment.AddMenu", {});
      addMenu.removeAll();
      addMenu.add(
        {
          text: "Nuevo",
          iconCls: "add",
          action: "newFactorRisk"
        },
        {
          text: "Modificar",
          action: "modifyFactorRisk",
          disabled: rec.data.depth === 1,
          iconCls: "modify"
        },
        {
          text: "Eliminar",
          action: "deleteFactorRisk",
          disabled: rec.data.depth === 1,
          iconCls: "delete"
        }
      );
      addMenu.showAt(e.getXY());
    }
  }
);
