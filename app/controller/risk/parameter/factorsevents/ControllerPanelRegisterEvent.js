Ext.define(
  "DukeSource.controller.risk.parameter.factorsevents.ControllerPanelRegisterEvent",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreTreeGridPanelRegisterEvent"],
    models: ["risk.parameter.grids.ModelTreeGridPanelRegisterEvent"],
    views: [
      "risk.parameter.grids.ViewTreeGridPanelRegisterEvent",
      "risk.util.ViewComboYesNo"
    ],

    init: function() {
      this.control({
        "[action=newRegisterEvent]": {
          click: this._onNewRegisterEvent
        },
        ViewTreeGridPanelRegisterEvent: {
          itemclick: this.treeItemClick,
          checkchange: this.treeCheckChange,
          itemexpand: this.treeNodeExpand,
          itemcontextmenu: this.treeRightClick
        },
        "AddMenuEventsTwo menuitem[text=Agregar]": {
          click: this._addEventTwo
        },
        "AddMenuEventsTwo menuitem[text=Editar]": {
          click: this._editEventOne
        },
        "AddMenuEventsTwo menuitem[text=Eliminar]": {
          click: this._deleteEvent
        },
        "[action=saveEventTwo]": {
          click: this._onSaveEventTwo
        },
        "AddMenuEventsThree menuitem[text=Agregar]": {
          click: this._addEventThree
        },
        "AddMenuEventsThree menuitem[text=Editar]": {
          click: this._editEventTwo
        },
        "AddMenuEventsThree menuitem[text=Eliminar]": {
          click: this._deleteEvent
        },
        "EditMenuEventsThree menuitem[text=Editar]": {
          click: this._editEventThree
        },
        "EditMenuEventsThree menuitem[text=Eliminar]": {
          click: this._deleteEvent
        },
        "AddMenuEvents menuitem[text=Edit]": {
          click: this.editCountry
        },
        "AddMenuEvents menuitem[text=Add]": {
          click: this.addCountry
        }
      });
    },
    _onNewRegisterEvent: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowEventOne",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getComponent("id")
        .setValue("myTree/id");
      win.show();
      win.down("#text").focus(false, 200);
    },

    treeItemClick: function(view, record) {},
    treeCheckChange: function(node, checked) {
      node.eachChild(function(childNode) {
        childNode.set("checked", checked);
        this.treeCheckChange(childNode, checked);
      }, this);
    },
    treeNodeExpand: function(node) {},
    treeRightClick: function(view, record, item, index, e) {
      e.stopEvent();
      this.application.currentRecord = record;
      if (record.get("depth") === 1) {
        var addMenu = Ext.create(
          "DukeSource.view.risk.parameter.factorsevents.AddMenuEventsTwo",
          {
            record: record
          }
        );
        addMenu.showAt(e.getXY());
      }
      if (record.get("depth") === 2) {
        addMenu = Ext.create(
          "DukeSource.view.risk.parameter.factorsevents.AddMenuEventsThree",
          {
            record: record
          }
        );
        addMenu.showAt(e.getXY());
      }
      if (record.get("depth") === 3) {
        var editMenu = Ext.create(
          "DukeSource.view.risk.parameter.factorsevents.EditMenuEventsThree",
          {
            record: record
          }
        );
        editMenu.showAt(e.getXY());
      }
      return false;
    },
    _addEventTwo: function(item, e) {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowEventTwo",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id") + "/id");
      win.show();
      win.down("#text").focus(false, 200);
    },

    _deleteEvent: function(item, e) {
      Ext.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/deleteEvent.htm",
        params: {
          id: this.application.currentRecord.get("id")
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageNormal(response.message);
            var refreshNode = Ext.ComponentQuery.query(
              "ViewPanelRegisterEvent ViewTreeGridPanelRegisterEvent"
            )[0]
              .getStore()
              .getNodeById(response.data);
            refreshNode.removeAll(false);
            Ext.ComponentQuery.query(
              "ViewPanelRegisterEvent ViewTreeGridPanelRegisterEvent"
            )[0]
              .getStore()
              .load({
                node: refreshNode
              });
          } else {
            DukeSource.global.DirtyView.messageWarning(response.message);
          }
        },
        failure: function() {}
      });
    },
    _onSaveEventTwo: function(button) {
      var window = button.up("window");
      var form = window.down("form");
      if (form.getForm().isValid()) {
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/saveEvent.htm",
          params: {
            jsonData: Ext.JSON.encode(form.getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              window.close();
              DukeSource.global.DirtyView.messageNormal(response.message);
              var refreshNode = Ext.ComponentQuery.query(
                "ViewPanelRegisterEvent ViewTreeGridPanelRegisterEvent"
              )[0]
                .getStore()
                .getNodeById(response.data);
              refreshNode.removeAll(false);
              Ext.ComponentQuery.query(
                "ViewPanelRegisterEvent ViewTreeGridPanelRegisterEvent"
              )[0]
                .getStore()
                .load({
                  node: refreshNode
                });
            } else {
              DukeSource.global.DirtyView.messageWarning(response.message);
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
      }
    },
    _addEventThree: function(item, e) {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowEventThree",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id") + "/id");
      win.show();
      win.down("#text").focus(false, 200);
    },
    _editEventOne: function(item, e) {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowEventOne",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getForm()
        .setValues(this.application.currentRecord.data);
      win.show();
      win.down("#text").focus(false, 200);
    },
    _editEventTwo: function(item, e) {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowEventTwo",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getForm()
        .setValues(this.application.currentRecord.data);
      win.show();
      win.down("#text").focus(false, 200);
    },
    _editEventThree: function(item) {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowEventThree",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getForm()
        .setValues(this.application.currentRecord.data);
      win.show();
      win.down("#text").focus(false, 200);
    },
    editCountry: function(item, e) {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.company.ViewWindowEdit",
        {
          modal: true
        }
      );
      win.down("form").loadRecord(this.application.currentRecord);
      win
        .down("form")
        .getComponent("code")
        .setReadOnly(true);
      win
        .down("form")
        .getComponent("parentNodeId")
        .setValue(this.application.currentRecord.parentNode.get("id"));
      win.show();
    },
    addCountry: function(item, e) {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.parameter.company.ViewWindowEdit",
        {
          modal: true
        }
      ).show();
    }
  }
);
