Ext.define(
  "DukeSource.controller.risk.parameter.factorsevents.ControllerPanelRegisterTypeEffect",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterTypeEffect"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterTypeEffect"],
    views: [
      "risk.parameter.grids.ViewGridPanelTypeEffect",
      "risk.parameter.factorsevents.ViewPanelRegisterTitleEvents"
    ],
    init: function() {
      this.control({
        // '[action=searchTriggerGridFactorRisk]':{
        //     keyup:this._onSearchTriggerGridFactorRisk
        // },
        // '[action=exportFactorRiskPdf]':{
        //     click:this._onExportFactorRiskPdf
        // },
        // '[action=exportFactorRiskExcel]':{
        //     click:this._onExportFactorRiskExcel
        // },
        // '[action=factorRiskAuditory]':{
        //     click:this._onFactorRiskAuditory
        // },
        "[action=newTypeEffect]": {
          click: this._newTypeEffect
        },
        "[action=deleteTypeEffect]": {
          click: this._onDeleteTypeEffect
        },
        "[action=searchTypeEffect]": {
          specialkey: this._searchTypeEffect
        }
      });
    },
    _newTypeEffect: function() {
      var modelTypeEffect = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterTypeEffect",
        {
          id: "Id",
          abbreviation: "ABREV.",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterTypeEffect")[0];
      var panel = general.down("ViewGridPanelTypeEffect");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelTypeEffect);
      editor.startEdit(0, 0);
    },
    _onDeleteTypeEffect: function() {
      var general = Ext.ComponentQuery.query("ViewPanelRegisterTypeEffect")[0];
      var grid = general.down("ViewGridPanelTypeEffect");

      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteEffectType.htm?nameView=ViewPanelRegisterTypeEffect"
      );
    },
    _searchTypeEffect: function(field, e) {
      if (e.getKey() == e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterTypeEffect grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findEffectType.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridFactorRisk: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterFactorRisk grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportFactorRiskPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportFactorRiskExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onFactorRiskAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterFactorRisk grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditFactorRisk.htm"
      );
    }
  }
);
