Ext.define(
  "DukeSource.controller.risk.parameter.reportregulatory.ControllerPanelRegisterAccountPlan",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterAccountPlan"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterAccountPlan"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterAccountPlan",
      "risk.parameter.reportregulatory.ViewPanelRegisterAccountPlan"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridAccountPlan]": {
          keyup: this._onSearchTriggerGridAccountPlan
        },
        "[action=exportAccountPlanPdf]": {
          click: this._onExportAccountPlanPdf
        },
        "[action=exportAccountPlanExcel]": {
          click: this._onExportAccountPlanExcel
        },
        "[action=accountPlanAuditory]": {
          click: this._onAccountPlanAuditory
        },
        "[action=newAccountPlan]": {
          click: this._newAccountPlan
        },
        "[action=deleteAccountPlan]": {
          click: this._onDeleteAccountPlan
        },
        "[action=searchAccountPlan]": {
          specialkey: this._searchAccountPlan
        }
      });
    },
    _newAccountPlan: function() {
      var modelAccountPlan = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterAccountPlan",
        {
          idAccountPlan: "id",
          codeAccount: "",
          orderAccount: "",
          currency: "",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterAccountPlan")[0];
      var panel = general.down("ViewGridPanelRegisterAccountPlan");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelAccountPlan);
      editor.startEdit(0, 0);
    },
    _onDeleteAccountPlan: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterAccountPlan grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteAccountPlan.htm?nameView=ViewPanelRegisterAccountPlan"
      );
    },
    _searchAccountPlan: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterAccountPlan grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findAccountPlan.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridAccountPlan: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterAccountPlan grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportAccountPlanPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportAccountPlanExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onAccountPlanAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterAccountPlan grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditAccountPlan.htm"
      );
    }
  }
);
