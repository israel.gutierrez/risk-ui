Ext.define(
  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterRiskAnswer",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterRiskAnswer"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterRiskAnswer"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterRiskAnswer",
      "risk.parameter.evaluationrisk.ViewPanelRegisterRiskAnswer"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridRiskAnswer]": {
          keyup: this._onSearchTriggerGridRiskAnswer
        },
        "[action=exportRiskAnswerPdf]": {
          click: this._onExportRiskAnswerPdf
        },
        "[action=exportRiskAnswerExcel]": {
          click: this._onExportRiskAnswerExcel
        },
        "[action=riskAnswerAuditory]": {
          click: this._onRiskAnswerAuditory
        },
        "[action=newRiskAnswer]": {
          click: this._newRiskAnswer
        },
        "[action=deleteRiskAnswer]": {
          click: this._onDeleteRiskAnswer
        },
        "[action=searchRiskAnswer]": {
          specialkey: this._searchRiskAnswer
        }
      });
    },
    _newRiskAnswer: function() {
      var modelRiskAnswer = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterRiskAnswer",
        {
          idRiskAnswer: "id",
          description: "",
          state: "S",
          minActionPlan: "0",
          maxActionPlan: "100",
          minControl: "0",
          maxControl: "100"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterRiskAnswer")[0];
      var panel = general.down("ViewGridPanelRegisterRiskAnswer");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelRiskAnswer);
      editor.startEdit(0, 0);
    },
    _onDeleteRiskAnswer: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterRiskAnswer grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteRiskAnswer.htm?nameView=ViewPanelRegisterRiskAnswer"
      );
    },
    _searchRiskAnswer: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterRiskAnswer grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findRiskAnswer.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridRiskAnswer: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterRiskAnswer grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportRiskAnswerPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportRiskAnswerExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onRiskAnswerAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterRiskAnswer grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditRiskAnswer.htm"
      );
    }
  }
);
