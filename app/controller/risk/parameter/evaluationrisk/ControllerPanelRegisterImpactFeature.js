Ext.define(
  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterImpactFeature",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterImpacfeature"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterImpacfeature"],
    views: ["risk.parameter.grids.ViewGridPanelRegisterImpacfeature"],
    init: function() {
      this.control({
        "[action=searchTriggerGridImpacfeature]": {
          keyup: this._onSearchTriggerGridImpacfeature
        },
        "[action=exportImpacfeaturePdf]": {
          click: this._onExportImpacfeaturePdf
        },
        "[action=exportImpacfeatureExcel]": {
          click: this._onExportImpacfeatureExcel
        },
        "[action=impacfeatureAuditory]": {
          click: this._onImpacfeatureAuditory
        },
        "[action=newImpacfeature]": {
          click: this._newImpacfeature
        },
        "[action=deleteImpacfeature]": {
          click: this._onDeleteImpacfeature
        },
        "[action=searchImpacfeature]": {
          specialkey: this._searchImpacfeature
        }
      });
    },
    _newImpacfeature: function() {
      var modelImpacfeature = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterImpacfeature",
        {
          idImpactFeature: "id",
          description: "",
          state: "S",
          type: ""
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterImpactFeature"
      )[0];
      var panel = general.down("ViewGridPanelRegisterImpacfeature");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelImpacfeature);
      editor.startEdit(0, 0);
    },
    _onDeleteImpacfeature: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterImpactFeature grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteImpactFeature.htm?nameView=ViewPanelRegisterImpactFeature"
      );
    },
    _searchImpacfeature: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterImpactFeature grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findImpactFeature.htm",
          "if.description",
          "if.description"
        );
      } else {
      }
    },
    _onSearchTriggerGridImpacfeature: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterImpactFeature grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportImpacfeaturePdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportImpacfeatureExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onImpacfeatureAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterImpactFeature grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditImpactFeature.htm"
      );
    }
  }
);
