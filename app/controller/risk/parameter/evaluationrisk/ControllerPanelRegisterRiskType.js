Ext.define(
  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterRiskType",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterRiskType"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterRiskType"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterRiskType",
      "risk.parameter.evaluationrisk.ViewPanelRegisterRiskType"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridRiskType]": {
          keyup: this._onSearchTriggerGridRiskType
        },
        "[action=exportRiskTypePdf]": {
          click: this._onExportRiskTypePdf
        },
        "[action=exportRiskTypeExcel]": {
          click: this._onExportRiskTypeExcel
        },
        "[action=riskTypeAuditory]": {
          click: this._onRiskTypeAuditory
        },
        "[action=newRiskType]": {
          click: this._newRiskType
        },
        "[action=deleteRiskType]": {
          click: this._onDeleteRiskType
        },
        "[action=searchRiskType]": {
          specialkey: this._searchRiskType
        }
      });
    },
    _newRiskType: function() {
      var modelRiskType = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterRiskType",
        {
          idRiskType: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterRiskType")[0];
      var panel = general.down("ViewGridPanelRegisterRiskType");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelRiskType);
      editor.startEdit(0, 0);
    },
    _onDeleteRiskType: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterRiskType grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteRiskType.htm?nameView=ViewPanelRegisterRiskType"
      );
    },
    _searchRiskType: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterRiskType grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findRiskType.htm?nameView=ViewPanelRegisterRiskType",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridRiskType: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterRiskType grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportRiskTypePdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportRiskTypeExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onRiskTypeAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterRiskType grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditRiskType.htm"
      );
    }
  }
);
