Ext.define(
  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterReductionImpact",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterPercentReduction"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterPercentReduction"],
    views: ["risk.parameter.grids.ViewGridRegisterReductionImpact"],
    init: function() {
      this.control({
        "[action=searchTriggerGridPercentReductionImpact]": {
          keyup: this._onSearchTriggerGridPercentReductionImpact
        },
        "[action=exportPercentReductionImpactPdf]": {
          click: this._onExportPercentReductionImpactPdf
        },
        "[action=exportPercentReductionImpactExcel]": {
          click: this._onExportPercentReductionImpactExcel
        },
        "[action=percentReductionImpactAuditory]": {
          click: this._onPercentReductionImpactAuditory
        },
        "[action=newPercentReductionImpact]": {
          click: this._newPercentReductionImpact
        },
        "[action=deletePercentReductionImpact]": {
          click: this._onDeletePercentReductionImpact
        },
        "[action=searchPercentReductionImpact]": {
          specialkey: this._searchPercentReductionImpact
        }
      });
    },
    _newPercentReductionImpact: function() {
      var modelPercentReduction = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterPercentReduction",
        {
          idPercentReduction: "id",
          valuePercent: "",
          state: "S",
          typePercent: "I"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterReductionImpact"
      )[0];
      var panel = general.down("ViewGridRegisterReductionImpact");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelPercentReduction);
      editor.startEdit(0, 0);
    },
    _onDeletePercentReductionImpact: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterReductionImpact grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deletePercentReduction.htm"
      );
    },
    _searchPercentReductionImpact: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterReductionImpact grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findPercentReduction.htm",
          "idPercentReduction",
          "idPercentReduction"
        );
      } else {
      }
    },
    _onSearchTriggerGridPercentReductionImpact: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterReductionImpact grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportPercentReductionImpactPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportPercentReductionImpactExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onPercentReductionImpactAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterReductionImpact grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditPercentReduction.htm"
      );
    }
  }
);
