Ext.define(
  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterLostType",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterLostType"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterLostType"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterLostType",
      "risk.parameter.evaluationrisk.ViewPanelRegisterLostType"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridLostType]": {
          keyup: this._onSearchTriggerGridLostType
        },
        "[action=exportLostTypePdf]": {
          click: this._onExportLostTypePdf
        },
        "[action=exportLostTypeExcel]": {
          click: this._onExportLostTypeExcel
        },
        "[action=lostTypeAuditory]": {
          click: this._onLostTypeAuditory
        },
        "[action=newLostType]": {
          click: this._newLostType
        },
        "[action=deleteLostType]": {
          click: this._onDeleteLostType
        },
        "[action=searchLostType]": {
          specialkey: this._searchLostType
        }
      });
    },
    _newLostType: function() {
      var modelLostType = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterLostType",
        {
          idLostType: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterLostType")[0];
      var panel = general.down("ViewGridPanelRegisterLostType");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelLostType);
      editor.startEdit(0, 0);
    },
    _onDeleteLostType: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterLostType grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteLostType.htm"
      );
    },
    _searchLostType: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterLostType grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findLostType.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridLostType: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterLostType grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportLostTypePdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportLostTypeExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onLostTypeAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterLostType grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditLostType.htm"
      );
    }
  }
);
