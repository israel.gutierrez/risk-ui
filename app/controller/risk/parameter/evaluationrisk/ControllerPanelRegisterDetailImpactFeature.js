Ext.define(
  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterDetailImpactFeature",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterDetailImpactFeature"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterDetailImpactFeature"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterDetailImpactFeature",
      "risk.parameter.combos.ViewComboImpactFeature",
      "risk.parameter.combos.ViewComboImpact"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridDetailImpacfeature]": {
          keyup: this._onSearchTriggerGridDetailImpacfeature
        },
        "[action=exportDetailImpacfeaturePdf]": {
          click: this._onExportDetailImpacfeaturePdf
        },
        "[action=exportDetailImpacfeatureExcel]": {
          click: this._onExportDetailImpacfeatureExcel
        },
        "[action=detailImpacfeatureAuditory]": {
          click: this._onDetailImpacfeatureAuditory
        },
        "[action=newDetailImpacfeature]": {
          click: this._newDetailImpacfeature
        },
        "[action=deleteDetailImpacfeature]": {
          click: this._onDeleteDetailImpacfeature
        },
        "[action=searchDetailImpacfeature]": {
          specialkey: this._searchDetailImpacfeature
        }
      });
    },
    _newDetailImpacfeature: function() {
      var modelDetailImpacfeature = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterDetailImpactFeature",
        {
          idImpact: "",
          idImpactFeature: "",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailImpactFeature"
      )[0];
      var panel = general.down("ViewGridPanelRegisterDetailImpactFeature");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelDetailImpacfeature);
      editor.startEdit(0, 0);
    },
    _onDeleteDetailImpacfeature: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailImpactFeature grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteDetailImpactFeature.htm?nameView=ViewPanelRegisterDetailImpactFeature"
      );
    },
    _searchDetailImpacfeature: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterDetailImpactFeature grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findDetailImpactFeature.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridDetailImpacfeature: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailImpactFeature grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportDetailImpacfeaturePdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportDetailImpacfeatureExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onDetailImpacfeatureAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailImpactFeature grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditDetailImpactFeature.htm"
      );
    }
  }
);
