Ext.define(
  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterDetailFrequencyFeature",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.parameter.grids.StoreGridPanelRegisterDetailFrequencyFeature"
    ],
    models: [
      "risk.parameter.grids.ModelGridPanelRegisterDetailFrequencyFeature"
    ],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterDetailFrequencyFeature",
      "risk.parameter.combos.ViewComboFrequencyFeature",
      "risk.parameter.combos.ViewComboFrequency"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridDetailFrequencyfeature]": {
          keyup: this._onSearchTriggerGridDetailFrequencyfeature
        },
        "[action=exportDetailFrequencyfeaturePdf]": {
          click: this._onExportDetailFrequencyfeaturePdf
        },
        "[action=exportDetailFrequencyfeatureExcel]": {
          click: this._onExportDetailFrequencyfeatureExcel
        },
        "[action=detailFrequencyfeatureAuditory]": {
          click: this._onDetailFrequencyfeatureAuditory
        },
        "[action=newDetailFrequencyfeature]": {
          click: this._newDetailFrequencyfeature
        },
        "[action=deleteDetailFrequencyfeature]": {
          click: this._onDeleteDetailFrequencyfeature
        },
        "[action=searchDetailFrequencyfeature]": {
          specialkey: this._searchDetailFrequencyfeature
        }
      });
    },
    _newDetailFrequencyfeature: function() {
      var modelDetailFrequencyfeature = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterDetailFrequencyFeature",
        {
          idFrequencyFeature: "",
          idFrequency: "",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailFrequencyFeature"
      )[0];
      var panel = general.down("ViewGridPanelRegisterDetailFrequencyFeature");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelDetailFrequencyfeature);
      editor.startEdit(0, 0);
    },
    _onDeleteDetailFrequencyfeature: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailFrequencyFeature grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteDetailFrequencyFeature.htm?nameView=ViewPanelRegisterDetailFrequencyFeature"
      );
    },
    _searchDetailFrequencyfeature: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterDetailFrequencyFeature grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findDetailFrequencyFeature.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridDetailFrequencyfeature: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailFrequencyFeature grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportDetailFrequencyfeaturePdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportDetailFrequencyfeatureExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onDetailFrequencyfeatureAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterDetailFrequencyFeature grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditDetailFrequencyFeature.htm"
      );
    }
  }
);
