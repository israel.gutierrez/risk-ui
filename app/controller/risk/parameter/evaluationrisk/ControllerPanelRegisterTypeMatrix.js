Ext.define(
  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterTypeMatrix",
  {
    extend: "Ext.app.Controller",
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterTypeMatrix",
      "risk.parameter.grids.ViewGridPanelRegisterTypeMatrixBusinessLine",
      "risk.parameter.combos.ViewComboBusinessLineOne"
    ],
    init: function() {
      this.control({
        "[action=newTypeMatrix]": {
          click: this._onNewTypeMatrix
        },
        "[action=saveRelationMatrixToBusinessLine]": {
          click: this._onSaveRelationMatrixToBusinessLine
        },
        "ViewPanelRegisterTypeMatrix ViewGridPanelRegisterTypeMatrix": {
          itemdblclick: this._onDblclickTypeMatrix,
          itemcontextmenu: this._onRightClickTypeMatrix
        },
        "[action=searchTriggerGridTypeMatrix]": {
          keyup: this._onSearchTriggerGridTypeMatrix
        },
        "[action=typeMatrixAuditory]": {
          click: this._onTypeMatrixAuditory
        },
        "[action=deleteTypeMatrix]": {
          click: this._onDeleteTypeMatrix
        },
        "[action=searchTypeMatrix]": {
          specialkey: this._onSearchTypeMatrix
        },
        "[action=searchTypeMatrixBusinessLine]": {
          keyup: this._onSearchTypeMatrixBusinessLine
        },
        "[action=typeMatrixBusinessLineAuditory]": {
          click: this._onTypeMatrixBusinessLineAuditory
        },
        "[action=searchTriggerGridTypeMatrixBusinessLine]": {
          click: this._onSearchTriggerGridTypeMatrixBusinessLine
        }
      });
    },
    _onNewTypeMatrix: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowRegisterTypeMatrix"
      );
      win.show();
      win.down("#description").focus(false, 200);
    },
    _onSaveRelationMatrixToBusinessLine: function(btn) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeMatrix"
      )[0].down("grid");
      Ext.Ajax.request({
        method: "POST",
        url:
          "http://localhost:9000/giro/saveTypeMatrix.htm?nameView=ViewPanelRegisterTypeMatrix",
        params: {
          jsonData: Ext.JSON.encode(
            btn
              .up("window")
              .down("form")
              .getForm()
              .getValues()
          )
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              response.mensaje,
              Ext.Msg.INFO
            );
            DukeSource.global.DirtyView.searchPaginationGridNormal(
              "",
              grid,
              grid.down("pagingtoolbar"),
              "http://localhost:9000/giro/findTypeMatrix.htm",
              "description",
              "description"
            );
            btn
              .up("window")
              .down("ViewComboBusinessLineOne")
              .getStore()
              .load({
                callback: function() {
                  btn.up("window").close();
                }
              });
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },
    _onDblclickTypeMatrix: function(grid, record) {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowRegisterTypeMatrix"
      );
      win
        .down("form")
        .getForm()
        .loadRecord(record);
      win.show();
    },
    _onRightClickTypeMatrix: function(view, rec, node, index, e) {
      var app = this.application;
      e.stopEvent();
      var rowMenu = Ext.create("Ext.menu.Menu", {
        width: 140,
        items: [
          {
            text: "Nuevo",
            iconCls: "add",
            handler: function() {
              app
                .getController(
                  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterTypeMatrix"
                )
                ._onNewTypeMatrix();
            }
          },
          {
            text: "Modificar",
            iconCls: "modify",
            handler: function() {
              app
                .getController(
                  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterTypeMatrix"
                )
                ._onDblclickTypeMatrix(view, rec);
            }
          },
          {
            text: "Eliminar",
            iconCls: "delete",
            handler: function() {
              app
                .getController(
                  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterTypeMatrix"
                )
                ._onDeleteTypeMatrix();
            }
          }
        ]
      });
      rowMenu.showAt(e.getXY());
    },
    _onDeleteTypeMatrix: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeMatrix ViewGridPanelRegisterTypeMatrix"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteTypeMatrix.htm?nameView=ViewPanelRegisterTypeMatrix"
      );
    },
    _onSearchTypeMatrix: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterTypeMatrix ViewGridPanelRegisterTypeMatrix"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findTypeMatrix.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridTypeMatrix: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeMatrix ViewGridPanelRegisterTypeMatrix"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onTypeMatrixAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeMatrix ViewGridPanelRegisterTypeMatrix"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditTypeMatrix.htm"
      );
    },
    _onSearchTypeMatrixBusinessLine: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterTypeMatrix ViewGridPanelRegisterTypeMatrixBusinessLine"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findTypeMatrix.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onTypeMatrixBusinessLineAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeMatrix ViewGridPanelRegisterTypeMatrixBusinessLine"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditTypeMatrix.htm"
      );
    },
    _onSearchTriggerGridTypeMatrixBusinessLine: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeMatrix ViewGridPanelRegisterTypeMatrixBusinessLine"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    }
  }
);
