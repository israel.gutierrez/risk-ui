Ext.define('DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelConfigurationGeneralMatrix', {
    extend: 'Ext.app.Controller',
    stores: [],
    models: [],
    views: ['risk.parameter.grids.ViewGridPanelRegisterOperationalRiskExposition'
        , 'risk.parameter.grids.ViewGridPanelRegisterConfigurationMatrix'
        , 'risk.parameter.combos.ViewComboOperationalRiskExposition'
        , 'risk.parameter.combos.ViewComboTypeMatrix'
        , 'risk.parameter.combos.ViewComboBusinessLineOne'
        , 'risk.parameter.evaluationrisk.ViewPanelGenerateAllMatrix'
        //Probably Delete
        , 'risk.parameter.grids.ViewGridPanelRegisterFrequency'
        , 'risk.parameter.grids.ViewGridPanelRegisterFrequencyfeature'
        , 'risk.parameter.grids.ViewGridPanelRegisterDetailFrequencyFeature'
        , 'risk.parameter.grids.ViewGridPanelRegisterImpact'
        , 'risk.parameter.grids.ViewGridPanelRegisterImpacfeature'
        , 'risk.parameter.grids.ViewGridPanelRegisterDetailImpactFeature'

        , 'risk.parameter.combos.ViewComboFrequencyFeature'
        , 'risk.parameter.combos.ViewComboFrequency'
        , 'risk.parameter.combos.ViewComboImpactFeature'
        , 'risk.parameter.combos.ViewComboImpact'
        , 'risk.parameter.grids.ViewGridPanelRegisterScaleRisk'
        , 'risk.parameter.grids.ViewGridPanelRegisterScaleBusinessLine'
        , 'risk.parameter.combos.ViewComboBusinessLineOne'
    ],
    refs: [
        {
            ref: 'viewPanelConfigurationGeneralMatrix',
            selector: 'ViewPanelConfigurationGeneralMatrix'
        }
    ],
    init: function () {
        this.control({
            //Risk Exposition
            '[action=newOperationalRiskExposition]': {
                click: this._newOperationalRiskExposition
            },
            '[action=deleteOperationalRiskExposition]': {
                click: this._onDeleteOperationalRiskExposition
            },
            '[action=searchOperationalRiskExposition]': {
                specialkey: this._searchOperationalRiskExposition
            },
            '[action=searchTriggerGridOperationalRiskExposition]': {
                keyup: this._onSearchTriggerGridOperationalRiskExposition
            },
            '[action=operationalRiskExpositionAuditory]': {
                click: this._onOperationalRiskExpositionAuditory
            },

            //Frequency
            '[action=searchTriggerGridFrequency]': {
                keyup: this._onSearchTriggerGridFrequency
            },
            '[action=frequencyAuditory]': {
                click: this._onFrequencyAuditory
            },
            '[action=newFrequency]': {
                click: this._newFrequency
            },
            '[action=deleteFrequency]': {
                click: this._onDeleteFrequency
            },
            '[action=editFrequencyProfileRisk]': {
                click: this._onEditFrequencyProfileRisk
            },
            '[action=updateFrequencyProfileRisk]': {
                click: this._onUpdateFrequencyProfileRisk
            },
            '[action=searchTriggerGridFrequencyfeature]': {
                keyup: this._onSearchTriggerGridFrequencyFeature
            },
            '[action=frequencyfeatureAuditory]': {
                click: this._onFrequencyFeatureAuditory
            },
            '[action=newFrequencyfeature]': {
                click: this._newFrequencyFeature
            },
            '[action=deleteFrequencyfeature]': {
                click: this._onDeleteFrequencyFeature
            },
            '[action=searchFrequencyfeature]': {
                specialkey: this._searchFrequencyFeature
            },
            '[action=searchTriggerGridDetailFrequencyfeature]': {
                keyup: this._onSearchTriggerGridDetailFrequencyFeature
            },
            '[action=detailFrequencyfeatureAuditory]': {
                click: this._onDetailFrequencyFeatureAuditory
            },
            '[action=newDetailFrequencyfeature]': {
                click: this._newDetailFrequencyFeature
            },
            '[action=deleteDetailFrequencyfeature]': {
                click: this._onDeleteDetailFrequencyFeature
            },
            '[action=searchDetailFrequencyfeature]': {
                specialkey: this._searchDetailFrequencyFeature
            },

            //Impact
            '[action=searchTriggerGridImpact]': {
                keyup: this._onSearchTriggerGridImpact
            },
            '[action=impactAuditory]': {
                click: this._onImpactAuditory
            },
            '[action=newImpact]': {
                click: this._newImpact
            },
            '[action=deleteImpact]': {
                click: this._onDeleteImpact
            },
            '[action=editImpactProfileRisk]': {
                click: this._onEditImpactProfileRisk
            },
            '[action=updateImpactProfileRisk]': {
                click: this._onUpdateImpactProfileRisk
            },
            '[action=searchTriggerGridImpacfeature]': {
                keyup: this._onSearchTriggerGridImpactFeature
            },
            '[action=impacfeatureAuditory]': {
                click: this._onImpactFeatureAuditory
            },
            '[action=newImpacfeature]': {
                click: this._newImpactFeature
            },
            '[action=deleteImpacfeature]': {
                click: this._onDeleteImpactFeature
            },
            '[action=searchImpacfeature]': {
                specialkey: this._searchImpactFeature
            },
            '[action=searchTriggerGridDetailImpacfeature]': {
                keyup: this._onSearchTriggerGridDetailImpactFeature
            },
            '[action=detailImpacfeatureAuditory]': {
                click: this._onDetailImpactFeatureAuditory
            },
            '[action=newDetailImpacfeature]': {
                click: this._newDetailImpactFeature
            },
            '[action=deleteDetailImpacfeature]': {
                click: this._onDeleteDetailImpactFeature
            },
            '[action=searchDetailImpacfeature]': {
                specialkey: this._searchDetailImpactFeature
            },

            // Scale risk
            '[action=newScaleRisk]': {
                click: this._newScaleRisk
            },
            '[action=scaleRiskAuditory]': {
                click: this._onScaleRiskAuditory
            },
            '[action=deleteScaleRisk]': {
                click: this._onDeleteScaleRisk
            },
            '[action=generateMatrixScaleRisk]': {
                click: this._onGenerateMatrixScaleRisk
            },
            '[action=deleteMatrixScaleRisk]': {
                click: this._onDeleteMatrixScaleRisk
            },

            // Matrix configuration
            '[action=newConfigurationMatrix]': {
                click: this._onNewConfigurationMatrix
            },
            '[action=deleteConfigurationMatrix]': {
                click: this._onDeleteConfigurationMatrix
            },
            '[action=auditoryConfigurationMatrix]': {
                click: this._onAuditoryConfigurationMatrix
            },
            '[action=replicateConfigurationMatrix]': {
                click: this._onReplicateConfigurationMatrix
            },
            '[action=saveReplicationTypeMatrix]': {
                click: this._onSaveReplicationTypeMatrix
            },
            '[action=checkConfigurationMatrix]': {
                click: this._onCheckConfigurationMatrix
            },
            '[action=getRevisionConfigurationMatrix]': {
                click: this._onGetRevisionConfigurationMatrix
            },
            '[action=configurationFrequency]': {
                click: this._onConfigurationFrequency
            },
            '[action=configurationImpact]': {
                click: this._onConfigurationImpact
            },
            '[action=configurationMatrix]': {
                click: this._onConfigurationMatrix
            },
            '[action=saveCellMatrix]': {
                click: this._onSaveCellMatrix
            }
        });
    },
    //Risk Exposition
    _newOperationalRiskExposition: function () {
        var panel = this.getViewPanelConfigurationGeneralMatrix().down('ViewGridPanelRegisterOperationalRiskExposition');
        Ext.Ajax.request({
            method: 'POST',
            url: 'http://localhost:9000/giro/checkRiskEvaluationPending.htm',
            success: function (response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                    if (response.data.length > 0) {
                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, 'No puedes crea una nueva máxima exposición mientras exitan riesgos pendientes de evaluar', Ext.Msg.WARNING);
                    } else {
                        var modelRiskExposition = Ext.ModelManager.create({
                            'idOperationalRiskExposition': 'id'
                            , 'maxAmount': ''
                            , 'year': ''
                        }, 'ModelGridPanelRegisterOperationalRiskExposition');
                        var editor = panel.editingPlugin;
                        editor.cancelEdit();
                        panel.getStore().insert(0, modelRiskExposition);
                        editor.startEdit(0, 0);
                    }

                } else {
                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, 'Consulte con el administrador del sistema', Ext.Msg.WARNING);
                }
            },
            failure: function () {
                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, 'Consulte con el administrador del sistema', Ext.Msg.WARNING);
            }
        });
    },
    _onDeleteOperationalRiskExposition: function () {
        var grid = this.getViewPanelConfigurationGeneralMatrix().down('ViewGridPanelRegisterOperationalRiskExposition');

        if (grid.getSelectionModel().getCount() == 0) {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_ITEM, Ext.Msg.WARNING);
        } else {
            Ext.MessageBox.show({
                title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
                icon: Ext.Msg.QUESTION,
                buttonText: {
                    yes: "Si"
                },
                buttons: Ext.MessageBox.YESNO,
                fn: function (btn) {
                    if (btn == 'yes') {
                        Ext.Ajax.request({
                            method: 'POST',
                            url: 'http://localhost:9000/giro/deleteOperationalRiskExposition.htm?nameView=ViewPanelConfigurationGeneralMatrix',
                            params: {
                                jsonData: Ext.JSON.encode(grid.getSelectionModel().getSelection()[0].data)
                            },
                            success: function (response) {
                                response = Ext.decode(response.responseText);
                                if (response.success) {
                                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                                    grid.getStore().load({
                                        callback: function () {
                                            Ext.ComponentQuery.query('ViewGridPanelRegisterConfigurationMatrix')[0].down('ViewComboOperationalRiskExposition').getStore().load();
                                        }
                                    });
                                } else {
                                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_ERROR, response.mensaje, Ext.Msg.ERROR);
                                }
                            },
                            failure: function () {

                            }
                        });
                    }
                }
            });

        }


    },
    _searchOperationalRiskExposition: function (field, e) {
        if (e.getKey() === e.ENTER) {
            var grid = this.getViewPanelConfigurationGeneralMatrix().down('ViewGridPanelRegisterOperationalRiskExposition');
            DukeSource.global.DirtyView.searchPaginationGridToEnter(field, grid, grid.down('pagingtoolbar'), 'http://localhost:9000/giro/findOperationalRiskExposition.htm', 'description', 'description');
        } else {
        }
    },
    _onSearchTriggerGridOperationalRiskExposition: function (text) {
        var grid = this.getViewPanelConfigurationGeneralMatrix().down('ViewGridPanelRegisterOperationalRiskExposition');
        DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onOperationalRiskExpositionAuditory: function () {
        var grid = this.getViewPanelConfigurationGeneralMatrix().down('ViewGridPanelRegisterOperationalRiskExposition');
        DukeSource.global.DirtyView.showWindowAuditory(grid, 'http://localhost:9000/giro/findAuditOperationalRiskExposition.htm');
    },

    //Frequency
    _newFrequency: function () {
        var windowConfiguration = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix')[0];
        if (windowConfiguration.stateConfig == 'P' || windowConfiguration.stateConfig == 'N') {
            var modelFrequency = Ext.ModelManager.create({
                'idFrequency': 'id'
                , 'description': ''
                , 'state': 'S'
                , 'type': 'avg'
                , 'valueMinimal': '0.00'
                , 'equivalentValue': '0'
                , 'percentage': '0.0000'
            }, 'ModelGridPanelRegisterFrequency');
            var panel = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix')[0].down('ViewGridPanelRegisterFrequency');
            var editor = panel.editingPlugin;
            editor.cancelEdit();
            panel.getStore().insert(0, modelFrequency);
            editor.startEdit(0, 0);
        } else {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, 'Para agregar, debe ELIMINAR la Matriz generada', Ext.Msg.WARNING);
        }

    },
    _onDeleteFrequency: function () {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterFrequency')[0];
        DukeSource.global.DirtyView.deleteElementToGrid(grid, 'http://localhost:9000/giro/deleteFrequency.htm?nameView=ViewPanelConfigurationGeneralMatrix');
    },
    _onSearchTriggerGridFrequency: function (text) {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterFrequency')[0];
        DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onFrequencyAuditory: function () {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterFrequency')[0];
        DukeSource.global.DirtyView.showWindowAuditory(grid, 'http://localhost:9000/giro/findAuditFrequency.htm');
    },
    _onEditFrequencyProfileRisk: function () {
        var grid = Ext.ComponentQuery.query('ViewGridPanelRegisterFrequency')[0];
        var row = grid.getSelectionModel().getSelection()[0];

        if (row === undefined) {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_ITEM, Ext.Msg.WARNING);
        } else {
            var win = Ext.create('DukeSource.view.risk.parameter.windows.WindowFrequencyProfileRisk', {
                modal: true,
                rowBack: row
            });
            win.title = win.title + ' - ' + '<span style="color: red">' + row.get('description') + '</span>';
            win.show();
            win.down('form').getForm().loadRecord(row);
        }
    },
    _onUpdateFrequencyProfileRisk: function (btn) {
        var win = btn.up('window');
        var grid = Ext.ComponentQuery.query('ViewGridPanelRegisterFrequency')[0];

        DukeSource.lib.Ajax.request({
            method: 'POST',
            url: 'http://localhost:9000/giro/updateFrequencyProfileRisk.htm',
            params: {
                jsonData: Ext.JSON.encode(win.down('form').getForm().getValues())
            },
            success: function (response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.message, Ext.Msg.INFO);
                    win.close();
                    grid.down('pagingtoolbar').moveFirst();
                } else {
                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_ERROR, response.message, Ext.Msg.WARNING);
                }
            },
            failure: function () {
            }
        });
    },
    _newFrequencyFeature: function () {
        var modelFrequencyfeature = Ext.ModelManager.create({
            'idFrequencyFeature': 'id'
            , 'description': ''
            , 'state': 'S'
        }, 'ModelGridPanelRegisterFrequencyfeature');
        var panel = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix')[0].down('ViewGridPanelRegisterFrequencyfeature');
        var editor = panel.editingPlugin;
        editor.cancelEdit();
        panel.getStore().insert(0, modelFrequencyfeature);
        editor.startEdit(0, 0);

    },
    _onDeleteFrequencyFeature: function () {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterFrequencyfeature')[0];
        DukeSource.global.DirtyView.deleteElementToGrid(grid, 'http://localhost:9000/giro/deleteFrequencyFeature.htm?nameView=ViewPanelConfigurationGeneralMatrix');
    },
    _searchFrequencyFeature: function (field, e) {
        if (e.getKey() === e.ENTER) {
            var panelGeneral = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix')[0];
            var grid = panelGeneral.down('ViewGridPanelRegisterFrequencyfeature');
            grid.store.getProxy().extraParams = {
                idTypeMatrix: panelGeneral.down('ViewComboTypeMatrix').getValue(),
                idOperationalRiskExposition: panelGeneral.down('ViewComboOperationalRiskExposition').getValue(),
                propertyFind: 'description',
                valueFind: field.getValue(),
                propertyOrder: 'description'
            };
            grid.store.getProxy().url = 'http://localhost:9000/giro/findFrequencyFeature.htm';
            grid.down('pagingtoolbar').moveFirst();
        } else {
        }
    },
    _onSearchTriggerGridFrequencyFeature: function (text) {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterFrequencyfeature')[0];
        DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onFrequencyFeatureAuditory: function () {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterFrequencyfeature')[0];
        DukeSource.global.DirtyView.showWindowAuditory(grid, 'http://localhost:9000/giro/findAuditFrequencyFeature.htm');
    },
    _newDetailFrequencyFeature: function () {
        var modelDetailFrequencyfeature = Ext.ModelManager.create({
            'idFrequencyFeature': ''
            , 'idFrequency': ''
            , 'description': ''
            , 'state': 'S'
        }, 'ModelGridPanelRegisterDetailFrequencyFeature');
        var panel = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix')[0].down('ViewGridPanelRegisterDetailFrequencyFeature');
        var editor = panel.editingPlugin;
        editor.cancelEdit();
        panel.getStore().insert(0, modelDetailFrequencyfeature);
        editor.startEdit(0, 0);
    },
    _onDeleteDetailFrequencyFeature: function () {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterDetailFrequencyFeature')[0];
        DukeSource.global.DirtyView.deleteElementToGrid(grid, 'http://localhost:9000/giro/deleteDetailFrequencyFeature.htm?nameView=ViewPanelConfigurationGeneralMatrix');
    },
    _searchDetailFrequencyFeature: function (field, e) {
        if (e.getKey() === e.ENTER) {
            var panelGeneral = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix')[0];
            var grid = panelGeneral.down('ViewGridPanelRegisterDetailFrequencyFeature');
            grid.store.getProxy().extraParams = {
                idTypeMatrix: panelGeneral.down('ViewComboTypeMatrix').getValue(),
                idOperationalRiskExposition: panelGeneral.down('ViewComboOperationalRiskExposition').getValue(),
                propertyFind: 'description',
                valueFind: field.getValue(),
                propertyOrder: 'description'
            };
            grid.store.getProxy().url = 'http://localhost:9000/giro/findDetailFrequencyFeature.htm';
            grid.down('pagingtoolbar').moveFirst();
        } else {
        }
    },
    _onSearchTriggerGridDetailFrequencyFeature: function (text) {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterDetailFrequencyFeature')[0];
        DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onDetailFrequencyFeatureAuditory: function () {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterDetailFrequencyFeature')[0];
        DukeSource.global.DirtyView.showWindowAuditory(grid, 'http://localhost:9000/giro/findAuditDetailFrequencyFeature.htm');
    },

    //Impact
    _newImpact: function () {
        var panel = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix')[0];
        if (panel.stateConfig === 'P' || panel.stateConfig === 'N') {
            var gridImpact = panel.down('ViewGridPanelRegisterImpact');
            var modelImpact = Ext.ModelManager.create({
                'idImpact': 'id'
                , 'description': ''
                , 'operationalRiskExposition': panel.down('ViewComboOperationalRiskExposition').getValue()
                , 'maxAmount': panel.down('ViewComboOperationalRiskExposition').getRawValue()
                , 'state': 'S'
                , 'type': 'avg'
                , 'percentage': '0.00'
                , 'valueMinimal': '0.00'
                , 'valueMaximo': '0.00'
                , 'equivalentValue': '0'
                , 'average': '0.00'
            }, 'ModelGridPanelRegisterImpact');
            var editor = gridImpact.editingPlugin;
            editor.cancelEdit();
            gridImpact.getStore().insert(0, modelImpact);
            editor.startEdit(0, 0);
        } else {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, 'Para agregar, debe ELIMINAR la Matriz generada', Ext.Msg.WARNING);
        }

    },
    _onDeleteImpact: function () {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterImpact')[0];
        DukeSource.global.DirtyView.deleteElementToGrid(grid, 'http://localhost:9000/giro/deleteImpact.htm?nameView=ViewPanelConfigurationGeneralMatrix');
    },
    _onSearchTriggerGridImpact: function (text) {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterImpact')[0];
        DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onEditImpactProfileRisk: function () {
        var grid = Ext.ComponentQuery.query('ViewGridPanelRegisterImpact')[0];
        var row = grid.getSelectionModel().getSelection()[0];

        if (row === undefined) {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_ITEM, Ext.Msg.WARNING);
        } else {
            var win = Ext.create('DukeSource.view.risk.parameter.windows.WindowImpactProfileRisk', {
                modal: true,
                rowBack: row
            });
            win.title = win.title + ' - ' + '<span style="color: red">' + row.get('description') + '</span>';
            win.show();
            win.down('form').getForm().loadRecord(row);
        }
    },
    _onUpdateImpactProfileRisk: function (btn) {
        var win = btn.up('window');
        var grid = Ext.ComponentQuery.query('ViewGridPanelRegisterImpact')[0];

        DukeSource.lib.Ajax.request({
            method: 'POST',
            url: 'http://localhost:9000/giro/updateImpactProfileRisk.htm',
            params: {
                jsonData: Ext.JSON.encode(win.down('form').getForm().getValues())
            },
            success: function (response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.message, Ext.Msg.INFO);
                    win.close();
                    grid.down('pagingtoolbar').moveFirst();
                } else {
                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_ERROR, response.message, Ext.Msg.WARNING);
                }
            },
            failure: function () {
            }
        });
    },
    _onImpactAuditory: function () {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterImpact')[0];
        DukeSource.global.DirtyView.showWindowAuditory(grid, 'http://localhost:9000/giro/findAuditImpact.htm');
    },
    _newImpactFeature: function () {
        var modelImpacfeature = Ext.ModelManager.create({
            'idImpactFeature': 'id'
            , 'description': ''
            , 'state': 'S'
        }, 'ModelGridPanelRegisterImpacfeature');
        var panel = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix')[0].down('ViewGridPanelRegisterImpacfeature');
        var editor = panel.editingPlugin;
        editor.cancelEdit();
        panel.getStore().insert(0, modelImpacfeature);
        editor.startEdit(0, 0);
    },
    _onDeleteImpactFeature: function () {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterImpacfeature')[0];
        DukeSource.global.DirtyView.deleteElementToGrid(grid, 'http://localhost:9000/giro/deleteImpactFeature.htm?nameView=ViewPanelConfigurationGeneralMatrix');
    },
    _searchImpactFeature: function (field, e) {
        if (e.getKey() === e.ENTER) {
            var panelGeneral = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix')[0];
            var grid = panelGeneral.down('ViewGridPanelRegisterImpacfeature');
            grid.store.getProxy().extraParams = {
                idTypeMatrix: panelGeneral.down('ViewComboTypeMatrix').getValue(),
                idOperationalRiskExposition: panelGeneral.down('ViewComboOperationalRiskExposition').getValue(),
                propertyFind: 'if.description',
                valueFind: field.getValue(),
                propertyOrder: 'if.description'
            };
            grid.store.getProxy().url = 'http://localhost:9000/giro/findImpactFeature.htm';
            grid.down('pagingtoolbar').moveFirst();

        }
    },
    _onSearchTriggerGridImpactFeature: function (text) {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterImpacfeature')[0];
        DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onImpactFeatureAuditory: function () {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterImpacfeature')[0];
        DukeSource.global.DirtyView.showWindowAuditory(grid, 'http://localhost:9000/giro/findAuditImpactFeature.htm');
    },
    _newDetailImpactFeature: function () {

        var modelDetailImpacfeature = Ext.ModelManager.create({
            'idImpact': ''
            , 'idImpactFeature': ''
            , 'description': ''
            , 'state': 'S'
        }, 'ModelGridPanelRegisterDetailImpactFeature');
        var panel = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix')[0].down('ViewGridPanelRegisterDetailImpactFeature');
        var editor = panel.editingPlugin;
        editor.cancelEdit();
        panel.getStore().insert(0, modelDetailImpacfeature);
        editor.startEdit(0, 0);

    },
    _onDeleteDetailImpactFeature: function () {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterDetailImpactFeature')[0];
        DukeSource.global.DirtyView.deleteElementToGrid(grid, 'http://localhost:9000/giro/deleteDetailImpactFeature.htm?nameView=ViewPanelConfigurationGeneralMatrix');
    },
    _searchDetailImpactFeature: function (field, e) {
        if (e.getKey() === e.ENTER) {
            var panelGeneral = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix')[0];
            var grid = panelGeneral.down('ViewGridPanelRegisterDetailImpactFeature');
            grid.store.getProxy().extraParams = {
                idTypeMatrix: panelGeneral.down('ViewComboTypeMatrix').getValue(),
                idOperationalRiskExposition: panelGeneral.down('ViewComboOperationalRiskExposition').getValue(),
                propertyFind: 'description',
                valueFind: field.getValue(),
                propertyOrder: 'description'
            };
            grid.store.getProxy().url = 'http://localhost:9000/giro/findDetailImpactFeature.htm';
            grid.down('pagingtoolbar').moveFirst();
        } else {
        }
    },
    _onSearchTriggerGridDetailImpactFeature: function (text) {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterDetailImpactFeature')[0];
        DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onDetailImpactFeatureAuditory: function () {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterDetailImpactFeature')[0];
        DukeSource.global.DirtyView.showWindowAuditory(grid, 'http://localhost:9000/giro/findAuditDetailImpactFeature.htm');
    },

    //Scale Risk
    _newScaleRisk: function () {
        var panel = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix')[0];
        if (panel.stateConfig == 'P' || panel.stateConfig == 'N') {
            var gridScaleRisk = panel.down('ViewGridPanelRegisterScaleRisk');
            var modelScaleRisk = Ext.ModelManager.create({
                'idScaleRisk': 'id'
                , 'description': ''
                , 'equivalentValue': '0'
                , 'state': 'S'
                , 'rangeSup': ''
                , 'levelColour': ''
                , 'rangeInf': '0.00'
                , 'idTypeMatrix': 99999999
                , 'businessLineOne': 999
                , 'operationalRiskExposition': panel.down('ViewComboOperationalRiskExposition').getValue()
                , 'valueOperationalRiskExposition': panel.down('ViewComboOperationalRiskExposition').getRawValue()
            }, 'ModelGridPanelRegisterScaleRisk');

            var editor = gridScaleRisk.editingPlugin;
            editor.cancelEdit();
            gridScaleRisk.getStore().insert(0, modelScaleRisk);
            editor.startEdit(0, 0);
        } else {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, 'Para agregar, debe ELIMINAR la Matriz generada', Ext.Msg.WARNING);
        }
    },
    _onDeleteScaleRisk: function () {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterScaleRisk')[0];
        DukeSource.global.DirtyView.deleteElementToGrid(grid, 'http://localhost:9000/giro/deleteScaleRisk.htm?nameView=ViewPanelConfigurationGeneralMatrix');
    },
    _onScaleRiskAuditory: function () {
        var grid = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix ViewGridPanelRegisterScaleRisk')[0];
        DukeSource.global.DirtyView.showWindowAuditory(grid, 'http://localhost:9000/giro/findAuditScaleRisk.htm');
    },
    _onGenerateMatrixScaleRisk: function () {
        var panel = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix')[0];
        var grid = panel.down('grid[name=matrixScaleRisk]');
        var valueExposition = panel.down('ViewComboOperationalRiskExposition');
        var extractExposition = valueExposition.getRawValue();
        if (panel.stateConfig === 'P') {
            Ext.Ajax.request({
                method: 'POST',
                url: 'http://localhost:9000/giro/saveMatrix.htm?nameView=ViewPanelConfigurationGeneralMatrix',
                params: {
                    idOperationalRiskExposition: valueExposition.getValue(),
                    valueMaxExposition: extractExposition.substring(0, extractExposition.length - 4),
                    idTypeMatrix: panel.down('ViewComboTypeMatrix').getValue()
                },
                success: function (response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        panel.stateConfig = 'F';
                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                        grid.store.getProxy().extraParams = {
                            idOperationalRiskExposition: valueExposition.getValue(),
                            idTypeMatrix: panel.down('ViewComboTypeMatrix').getValue()
                        };
                        grid.store.getProxy().url = 'http://localhost:9000/giro/findMatrixAndLoad.htm';
                        grid.down('pagingtoolbar').moveFirst();
                    } else {
                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_ERROR, response.mensaje, Ext.Msg.ERROR);
                    }
                },
                failure: function () {
                }
            });
        } else {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, 'Antes debe ELIMINAR la matriz generada anteriormente', Ext.Msg.WARNING);
        }

    },
    _onDeleteMatrixScaleRisk: function () {

        Ext.MessageBox.show({
            title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
            msg: 'Esta Seguro que desea ELIMINAR la Matriz ?',
            icon: Ext.Msg.QUESTION,
            buttonText: {
                yes: "Si"
            },
            buttons: Ext.MessageBox.YESNO,
            fn: function (btn) {
                if (btn == 'yes') {
                    var panel = Ext.ComponentQuery.query('ViewWindowConfigurationMatrix')[0];
                    var grid = panel.down('grid[name=matrixScaleRisk]');
                    var valueExposition = panel.down('ViewComboOperationalRiskExposition');
                    Ext.Ajax.request({
                        method: 'POST',
                        url: 'http://localhost:9000/giro/deleteMatrix.htm?nameView=ViewPanelConfigurationGeneralMatrix',
                        params: {
                            idOperationalRiskExposition: valueExposition.getValue(),
                            idTypeMatrix: panel.down('ViewComboTypeMatrix').getValue()
                        },
                        success: function (response) {
                            response = Ext.decode(response.responseText);
                            if (response.success) {
                                panel.stateConfig = 'P';
                                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                                grid.store.getProxy().extraParams = {
                                    idOperationalRiskExposition: valueExposition.getValue(),
                                    idTypeMatrix: panel.down('ViewComboTypeMatrix').getValue()
                                };
                                grid.store.getProxy().url = 'http://localhost:9000/giro/findMatrixAndLoad.htm';
                                grid.down('pagingtoolbar').moveFirst();
                            } else {
                                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_ERROR, response.mensaje, Ext.Msg.ERROR);
                            }
                        },
                        failure: function () {
                        }
                    });
                }
            }
        });

    },

    //Matrix configuration
    _onNewConfigurationMatrix: function () {
        var win = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowConfigurationMatrix', {
            stateConfig: 'N'
        });
        var cboRiskExposition = win.down('ViewComboOperationalRiskExposition');
        cboRiskExposition.store.load({
            callback: function (cbo) {
                for (var i = 0; i < cbo.length; i++) {
                    if (cbo[i].data.validity === 'S') {
                        cboRiskExposition.setValue(cbo[i].data.idOperationalRiskExposition);

                        win.down('ViewComboTypeMatrix').getStore().load(
                            {
                                url: 'http://localhost:9000/giro/showListTypeMatrixNoReferencedCombox.htm',
                                params: {
                                    idOperationalRiskExposition: cbo[i].data.idOperationalRiskExposition
                                },
                                callback: function () {
                                    win.show();
                                }
                            }
                        );
                    }
                }
            }
        });

    },
    _onDeleteConfigurationMatrix: function () {
        var grid = Ext.ComponentQuery.query('ViewPanelConfigurationGeneralMatrix ViewGridPanelRegisterConfigurationMatrix')[0];
        DukeSource.global.DirtyView.deleteElementToGrid(grid, 'http://localhost:9000/giro/deleteExpositionMatrix.htm?nameView=ViewPanelConfigurationGeneralMatrix');
    },
    _onAuditoryConfigurationMatrix: function () {
        var grid = Ext.ComponentQuery.query('ViewPanelConfigurationGeneralMatrix ViewGridPanelRegisterConfigurationMatrix')[0];
        DukeSource.global.DirtyView.showWindowAuditory(grid, 'http://localhost:9000/giro/auditExpositionMatrix.htm');
    },
    _onReplicateConfigurationMatrix: function () {
        var gridGral = Ext.ComponentQuery.query('ViewPanelConfigurationGeneralMatrix')[0].down('ViewGridPanelRegisterConfigurationMatrix');
        if (gridGral.getSelectionModel().getCount() === 0) {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_ITEM, Ext.Msg.WARNING);
        } else {
            if (gridGral.getSelectionModel().getSelection()[0].get('stateVerification') === 'A') {
                var win = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowTypeMatrix');
                win.setTitle('Replicar la Matriz : ' + gridGral.getSelectionModel().getSelection()[0].get('nameTypeMatrix'));
                var cboRiskExposition = win.down('ViewComboOperationalRiskExposition');
                cboRiskExposition.store.load({
                    callback: function (cbo) {
                        for (var i = 0; i < cbo.length; i++) {
                            if (cbo[i].data.validity === 'S') {
                                cboRiskExposition.setValue(cbo[i].data.idOperationalRiskExposition);
                                var grid = Ext.ComponentQuery.query('ViewWindowTypeMatrix')[0].down('grid');
                                grid.store.getProxy().extraParams = {idOperationalRiskExposition: cbo[i].data.idOperationalRiskExposition};
                                grid.store.getProxy().url = 'http://localhost:9000/giro/showListTypeMatrixNoReferencedCombox.htm';
                                grid.down('pagingtoolbar').moveFirst();
                                win.show();
                            }
                        }
                    }
                });
            } else {
                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, 'Solo puede replicar matrices aprobadas', Ext.Msg.WARNING);
            }
        }
    },
    _onSaveReplicationTypeMatrix: function (btn) {
        var grid = btn.up('window').down('grid');
        var count = grid.getSelectionModel().getCount();
        var row = grid.getSelectionModel().getSelection()[0];

        if (count === 0) {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_ITEM, Ext.Msg.WARNING);
        } else {
            var gridMain = Ext.ComponentQuery.query('ViewPanelConfigurationGeneralMatrix')[0].down('ViewGridPanelRegisterConfigurationMatrix');
            var rowMain = gridMain.getSelectionModel().getSelection()[0];

            if (count > 1) {
                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, 'Seleccione solo UN Tipo de Matriz', Ext.Msg.WARNING);
            } else {
                Ext.Ajax.request({
                    method: 'POST',
                    url: 'http://localhost:9000/giro/saveExpositionMatrixMassive.htm?nameView=ViewPanelConfigurationGeneralMatrix',
                    params: {
                        idTypeMatrixCopy: rowMain.get('typeMatrix'),
                        idOperationalRiskExpositionCopy: rowMain.get('idOperationalRiskExposition'),
                        idTypeMatrixPaste: row.get('idTypeMatrix'),
                        idOperationalRiskExpositionPaste: btn.up('window').down('ViewComboOperationalRiskExposition').getValue()
                    },
                    success: function (response) {
                        response = Ext.decode(response.responseText);
                        if (response.success) {
                            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                            gridMain.getStore().load();
                            btn.up('window').close();
                        } else {
                            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_ERROR, response.mensaje, Ext.Msg.ERROR);
                        }
                    },
                    failure: function () {
                    }
                });
            }
        }
    },
    _onCheckConfigurationMatrix: function () {
        var grid = Ext.ComponentQuery.query('ViewPanelConfigurationGeneralMatrix')[0].down('ViewGridPanelRegisterConfigurationMatrix');
        var row = grid.getSelectionModel().getSelection()[0];

        if (row === undefined) {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
        } else {
            if (row.get('stateConfig') === 'F' && row.get('stateVerification') === 'P') {
                Ext.MessageBox.show({
                    title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                    msg: 'Esta Seguro que desea APROBAR la Matriz ' + row.get('nameTypeMatrix') + '?\n ' +
                        'Tenga en cuenta que todos los riesgos evaluados previamente, se replicarán con esta nueva máxima exposición,\n' +
                        'y quedaran sin opción a ser modificados posteriormente y quedaran solamente con opción a consulta.',
                    icon: Ext.Msg.QUESTION,
                    buttonText: {
                        yes: "Si"
                    },
                    buttons: Ext.MessageBox.YESNO,
                    fn: function (btn) {
                        if (btn === 'yes') {
                            DukeSource.lib.Ajax.request({
                                method: 'POST',
                                url: 'http://localhost:9000/giro/validityMatrixByAnalyst.htm?nameView=ViewPanelConfigurationGeneralMatrix',
                                timeout: 180000,
                                params: {
                                    idTypeMatrix: row.get('typeMatrix'),
                                    idOperationalRiskExposition: row.get('idOperationalRiskExposition')
                                },
                                success: function (response) {
                                    response = Ext.decode(response.responseText);
                                    if (response.success) {
                                        DukeSource.global.DirtyView.messageNormal(response.message);
                                        grid.getStore().load();
                                    } else {
                                        DukeSource.global.DirtyView.messageWarning(response.message);
                                    }
                                },
                                failure: function () {
                                }
                            });
                        }
                    }
                });
            } else if (row.get('stateVerification') === 'A') {
                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, 'La matriz ' + row.get('nameTypeMatrix') + ' ya fue APROBADA', Ext.Msg.WARNING);
            } else {
                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, 'Solo puede APROBAR matrices con configuración FINALIZADA', Ext.Msg.WARNING);
            }
        }
    },
    _onGetRevisionConfigurationMatrix: function () {
        var grid = Ext.ComponentQuery.query('ViewPanelConfigurationGeneralMatrix')[0].down('ViewGridPanelRegisterConfigurationMatrix');
        if (grid.getSelectionModel().getCount() === 0) {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_ITEM, Ext.Msg.WARNING);
        } else {
            if (grid.getSelectionModel().getSelection()[0].get('stateConfig') === 'F' && grid.getSelectionModel().getSelection()[0].get('stateVerification') === 'P') {
                Ext.MessageBox.show({
                    title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                    msg: 'Esta Seguro de solicitar APROBACIÓN de la Matriz ' + grid.getSelectionModel().getSelection()[0].get('nameTypeMatrix') + '?',
                    icon: Ext.Msg.QUESTION,
                    buttonText: {
                        yes: "Si"
                    },
                    buttons: Ext.MessageBox.YESNO,
                    fn: function (btn) {
                        if (btn == 'yes') {
                            Ext.Ajax.request({
                                method: 'POST',
                                url: 'http://localhost:9000/giro/vvv.htm?nameView=ViewPanelConfigurationGeneralMatrix',
                                params: {
                                    idTypeMatrix: '',
                                    idOperationalRiskExposition: ''
                                },
                                success: function (response) {
                                    response = Ext.decode(response.responseText);
                                    if (response.success) {
                                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                                        grid.getStore().load();
                                    } else {
                                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_ERROR, response.mensaje, Ext.Msg.ERROR);
                                    }
                                },
                                failure: function () {
                                }
                            });
                        }
                    }
                });
            } else if (grid.getSelectionModel().getSelection()[0].get('stateVerification') === 'A') {
                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, 'La matriz ' + grid.getSelectionModel().getSelection()[0].get('nameTypeMatrix') + ' ya fue APROBADA', Ext.Msg.WARNING);
            } else {
                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, 'Solo puede SOLICITAR revisión de matrices con configuración FINALIZADA', Ext.Msg.WARNING);
            }

        }
    },
    _onConfigurationFrequency: function (btn) {
        var panel = btn.up('panel');
        if (panel.down('tabpanel[itemId=configFrequency]') == null) {
            panel.removeAll();
            panel.add(
                {
                    xtype: 'tabpanel',
                    itemId: 'configFrequency',
                    padding: '2 2 2 2',
                    items: [
                        {
                            title: 'Probabilidad',
                            xtype: 'ViewGridPanelRegisterFrequency'
                        },
                        {
                            title: 'Criterio de probabilidad',
                            xtype: 'ViewGridPanelRegisterFrequencyfeature'
                        },
                        {
                            title: 'Detalle criterio de probabilidad',
                            xtype: 'ViewGridPanelRegisterDetailFrequencyFeature'
                        }
                    ]
                }
            )
        }
    },
    _onConfigurationImpact: function (btn) {
        var panel = btn.up('panel');
        if (panel.down('tabpanel[itemId=configImpact]') == null) {
            panel.removeAll();
            panel.add(
                {
                    xtype: 'tabpanel',
                    itemId: 'configImpact',
                    padding: '2 2 2 2',
                    items: [
                        {
                            title: 'Impacto',
                            xtype: 'ViewGridPanelRegisterImpact'
                        },
                        {
                            title: 'Criterio de impacto',
                            xtype: 'ViewGridPanelRegisterImpacfeature'
                        },
                        {
                            title: 'Detalle criterio de impacto',
                            xtype: 'ViewGridPanelRegisterDetailImpactFeature'
                        }

                    ]
                }
            );
        }

    },
    _onConfigurationMatrix: function (btn) {
        var panel = btn.up('panel');
        if (panel.down('ViewPanelGenerateAllMatrix') == null) {
            panel.removeAll();
            panel.add(
                {
                    xtype: 'ViewPanelGenerateAllMatrix', itemId: 'generalMatrix',
                    padding: '2 2 2 2'

                }
            );
        }
    },
    _onSaveCellMatrix: function (me) {
        var win = me.up('window');
        var grid = win.down('grid');
        var row = grid.getSelectionModel().getSelection()[0];
        if (grid.disabled) {
            Ext.Ajax.request({
                method: 'POST',
                url: 'http://localhost:9000/giro/changeCellMatrix.htm',
                params: {
                    ghostIndicator: win.down('toolbar').down('radiogroup').getValue()['riskGhost'],
                    idMatrix: win.idMatrix,
                    descriptionScaleRisk: DukeSource.global.GiroConstants.GHOST
                },
                success: function (response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        win.close();
                        Ext.ComponentQuery.query('ViewWindowConfigurationMatrix grid[name=matrixScaleRisk]')[0].getStore().load()
                    } else {
                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_ERROR, response.mensaje, Ext.Msg.ERROR);
                    }
                },
                failure: function () {
                }
            });
        } else {
            if (row === undefined) {
                DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
            } else {
                if (grid.getSelectionModel().getCount() > 1) {
                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, 'Solo debe Seleccionar UN nivel', Ext.Msg.WARNING);
                } else {
                    Ext.Ajax.request({
                        method: 'POST',
                        url: 'http://localhost:9000/giro/changeCellMatrix.htm',
                        params: {
                            ghostIndicator: win.down('toolbar').down('radiogroup').getValue()['riskGhost'],
                            idMatrix: win.idMatrix,
                            colour: row.data.levelColour,
                            descriptionScaleRisk: row.data.description
                        },
                        success: function (response) {
                            response = Ext.decode(response.responseText);
                            if (response.success) {
                                win.close();
                                Ext.ComponentQuery.query('ViewWindowConfigurationMatrix grid[name=matrixScaleRisk]')[0].getStore().load()
                            } else {
                                DukeSource.global.DirtyView.messageAlert(response.message);
                            }
                        },
                        failure: function () {
                        }
                    });
                }
            }
        }
    }
});