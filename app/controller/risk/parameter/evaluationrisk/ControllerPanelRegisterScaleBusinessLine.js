Ext.define(
  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterScaleBusinessLine",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterScaleBusinessLine"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterScaleBusinessLine"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterScaleBusinessLine",
      "risk.parameter.combos.ViewComboBusinessLineOne",
      "risk.parameter.combos.ViewComboOperationalRiskExposition"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridScaleRisk]": {
          keyup: this._onSearchTriggerGridScaleRisk
        },
        "[action=exportScaleRiskPdf]": {
          click: this._onExportScaleRiskPdf
        },
        "[action=exportScaleRiskExcel]": {
          click: this._onExportScaleRiskExcel
        },
        "[action=scaleRiskAuditory]": {
          click: this._onScaleRiskAuditory
        },
        "[action=newScaleBusinessLine]": {
          click: this._newScaleBusinessLine
        },
        "[action=deleteScaleRisk]": {
          click: this._onDeleteScaleRisk
        },
        "[action=searchScaleRisk]": {
          specialkey: this._searchScaleRisk
        },
        "[action=generateMatrixScaleBusinessLine]": {
          click: this._onGenerateMatrixScaleBusinessLine
        }
      });
    },
    _newScaleBusinessLine: function(button) {
      if (
        button
          .up("ViewPanelRegisterScaleBusinessLine")
          .down("ViewComboBusinessLineOne")
          .getValue() == null
      ) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          "Seleccione una Linea de Negocio",
          Ext.Msg.WARNING
        );
      } else {
        var modelScaleBusinessLine = Ext.create(
          "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterScaleBusinessLine",
          {
            idScaleRisk: "id",
            description: "",
            state: "S",
            rangeInf: "",
            rangeSup: "",
            levelColour: "",
            businessLineOne: button
              .up("ViewPanelRegisterScaleBusinessLine")
              .down("ViewComboBusinessLineOne")
              .getValue() /*indica que ingresa un nivel para el riesgo general(Valor por defecto)*/,
            operationalRiskExposition: button
              .up("ViewPanelRegisterScaleBusinessLine")
              .down("ViewComboOperationalRiskExposition")
              .getValue(),
            valueOperationalRiskExposition: button
              .up("ViewPanelRegisterScaleBusinessLine")
              .down("ViewComboOperationalRiskExposition")
              .getRawValue()
          }
        );
        var general = Ext.ComponentQuery.query(
          "ViewPanelRegisterScaleBusinessLine"
        )[0];
        var panel = general.down("ViewGridPanelRegisterScaleBusinessLine");
        var grid = panel;
        var editor = panel.editingPlugin;
        editor.cancelEdit();
        grid.getStore().insert(0, modelScaleBusinessLine);
        editor.startEdit(0, 0);
      }
    },
    _onDeleteScaleRisk: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterScaleRisk grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteScaleRisk.htm"
      );
    },
    _searchScaleRisk: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterScaleRisk grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findScaleRisk.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridScaleRisk: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterScaleRisk grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportScaleRiskPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportScaleRiskExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onScaleRiskAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterScaleRisk grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditScaleRisk.htm"
      );
    },
    _onGenerateMatrixScaleBusinessLine: function(button) {
      var panel = button.up("panel");
      var grid = panel.down("grid[name=matrixScaleBusinessLine]");
      var valueExposition = panel.down("ViewComboOperationalRiskExposition");
      var valueBusiness = panel.down("ViewComboBusinessLineOne");
      Ext.Ajax.request({
        method: "POST",
        url:
          "http://localhost:9000/giro/saveMatrix.htm?nameView=ViewPanelRegisterScaleBusinessLine",
        params: {
          maxExposition: panel
            .down("ViewComboOperationalRiskExposition")
            .getValue(),
          valueMaxExposition: valueExposition.getRawValue(),
          businessLine: valueBusiness.getValue()
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              response.mensaje,
              Ext.Msg.INFO
            );
            grid.store.getProxy().extraParams = {
              maxExposition: valueExposition.getValue(),
              businessLine: valueBusiness.getValue()
            };
            grid.store.getProxy().url =
              "http://localhost:9000/giro/findMatrixAndLoad.htm";
            grid.down("pagingtoolbar").moveFirst();
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    }
  }
);
