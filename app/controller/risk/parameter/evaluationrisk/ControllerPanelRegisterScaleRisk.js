Ext.define(
  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterScaleRisk",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterScaleRisk"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterScaleRisk"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterScaleRisk",
      "risk.parameter.combos.ViewComboOperationalRiskExposition"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridScaleRisk]": {
          keyup: this._onSearchTriggerGridScaleRisk
        },
        "[action=exportScaleRiskPdf]": {
          click: this._onExportScaleRiskPdf
        },
        "[action=exportScaleRiskExcel]": {
          click: this._onExportScaleRiskExcel
        },
        "[action=scaleRiskAuditory]": {
          click: this._onScaleRiskAuditory
        },
        "[action=newScaleRisk]": {
          click: this._newScaleRisk
        },
        "[action=deleteScaleRisk]": {
          click: this._onDeleteScaleRisk
        },
        "[action=searchScaleRisk]": {
          specialkey: this._searchScaleRisk
        },
        "[action=generateMatrixScaleRisk]": {
          click: this._onGenerateMatrixScaleRisk
        }
      });
    },
    _newScaleRisk: function(button) {
      var modelScaleRisk = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterScaleRisk",
        {
          idScaleRisk: "id",
          description: "",
          state: "S",
          rangeInf: "",
          rangeSup: "",
          levelColour: "",
          businessLineOne: 999 /*indica que ingresa un nivel para el riesgo general(Valor por defecto)*/,
          operationalRiskExposition: button
            .up("ViewPanelRegisterScaleRisk")
            .down("ViewComboOperationalRiskExposition")
            .getValue(),
          valueOperationalRiskExposition: button
            .up("ViewPanelRegisterScaleRisk")
            .down("ViewComboOperationalRiskExposition")
            .getRawValue()
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterScaleRisk")[0];
      var panel = general.down("ViewGridPanelRegisterScaleRisk");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelScaleRisk);
      editor.startEdit(0, 0);
    },
    _onDeleteScaleRisk: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterScaleRisk grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteScaleRisk.htm?nameView=ViewPanelRegisterScaleRisk"
      );
    },
    _searchScaleRisk: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterScaleRisk grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findScaleRisk.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridScaleRisk: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterScaleRisk grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportScaleRiskPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportScaleRiskExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onScaleRiskAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterScaleRisk grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditScaleRisk.htm"
      );
    },
    _onGenerateMatrixScaleRisk: function(button) {
      var panel = button.up("panel");
      var grid = panel.down("grid[name=matrixScaleRisk]");
      var valueExposition = panel.down("ViewComboOperationalRiskExposition");
      Ext.Ajax.request({
        method: "POST",
        url:
          "http://localhost:9000/giro/saveMatrix.htm?nameView=ViewPanelRegisterScaleRisk",
        params: {
          maxExposition: panel
            .down("ViewComboOperationalRiskExposition")
            .getValue(),
          valueMaxExposition: valueExposition.getRawValue(),
          businessLine: "999"
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              response.mensaje,
              Ext.Msg.INFO
            );
            grid.store.getProxy().extraParams = {
              maxExposition: valueExposition.getValue(),
              businessLine: "999"
            };
            grid.store.getProxy().url =
              "http://localhost:9000/giro/findMatrixAndLoad.htm";
            grid.down("pagingtoolbar").moveFirst();
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    }
  }
);
