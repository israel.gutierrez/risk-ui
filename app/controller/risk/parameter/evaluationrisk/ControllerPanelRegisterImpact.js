Ext.define(
  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterImpact",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterImpact"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterImpact"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterImpact",
      "risk.parameter.evaluationrisk.ViewPanelRegisterImpact",
      "risk.parameter.combos.ViewComboOperationalRiskExposition"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridImpact]": {
          keyup: this._onSearchTriggerGridImpact
        },
        "[action=exportImpactPdf]": {
          click: this._onExportImpactPdf
        },
        "[action=exportImpactExcel]": {
          click: this._onExportImpactExcel
        },
        "[action=impactAuditory]": {
          click: this._onImpactAuditory
        },
        "[action=newImpact]": {
          click: this._newImpact
        },
        "[action=deleteImpact]": {
          click: this._onDeleteImpact
        },
        "[action=searchImpact]": {
          specialkey: this._searchImpact
        }
      });
    },
    _newImpact: function(button) {
      var modelImpact = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterImpact",
        {
          idImpact: "id",
          percentage: "0",
          description: "",
          operationalRiskExposition: button
            .up("ViewPanelRegisterImpact")
            .down("ViewComboOperationalRiskExposition")
            .getValue(),
          maxAmount: button
            .up("ViewPanelRegisterImpact")
            .down("ViewComboOperationalRiskExposition")
            .getRawValue(),
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterImpact")[0];
      var panel = general.down("ViewGridPanelRegisterImpact");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelImpact);
      editor.startEdit(0, 0);
    },
    _onDeleteImpact: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterImpact grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteImpact.htm?nameView=ViewPanelRegisterImpact"
      );
    },
    _searchImpact: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query("ViewPanelRegisterImpact grid")[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findImpact.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridImpact: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterImpact grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportImpactPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportImpactExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onImpactAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterImpact grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditImpact.htm"
      );
    }
  }
);
