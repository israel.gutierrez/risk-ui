Ext.define(
  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterFrequency",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterFrequency"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterFrequency"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterFrequency",
      "risk.parameter.evaluationrisk.ViewPanelRegisterFrequency"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridFrequency]": {
          keyup: this._onSearchTriggerGridFrequency
        },
        "[action=exportFrequencyPdf]": {
          click: this._onExportFrequencyPdf
        },
        "[action=exportFrequencyExcel]": {
          click: this._onExportFrequencyExcel
        },
        "[action=frequencyAuditory]": {
          click: this._onFrequencyAuditory
        },
        "[action=newFrequency]": {
          click: this._newFrequency
        },
        "[action=deleteFrequency]": {
          click: this._onDeleteFrequency
        },
        "[action=searchFrequency]": {
          specialkey: this._searchFrequency
        }
      });
    },
    _newFrequency: function() {
      var modelFrequency = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterFrequency",
        {
          idFrequency: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterFrequency")[0];
      var panel = general.down("ViewGridPanelRegisterFrequency");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelFrequency);
      editor.startEdit(0, 0);
    },
    _onDeleteFrequency: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterFrequency grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteFrequency.htm?nameView=ViewPanelRegisterFrequency"
      );
    },
    _searchFrequency: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterFrequency grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findFrequency.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridFrequency: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterFrequency grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportFrequencyPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportFrequencyExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onFrequencyAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterFrequency grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditFrequency.htm"
      );
    }
  }
);
