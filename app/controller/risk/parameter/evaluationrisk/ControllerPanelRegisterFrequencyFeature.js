Ext.define(
  "DukeSource.controller.risk.parameter.evaluationrisk.ControllerPanelRegisterFrequencyFeature",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterFrequencyfeature"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterFrequencyfeature"],
    views: ["risk.parameter.grids.ViewGridPanelRegisterFrequencyfeature"],
    init: function() {
      this.control({
        "[action=searchTriggerGridFrequencyfeature]": {
          keyup: this._onSearchTriggerGridFrequencyfeature
        },
        "[action=exportFrequencyfeaturePdf]": {
          click: this._onExportFrequencyfeaturePdf
        },
        "[action=exportFrequencyfeatureExcel]": {
          click: this._onExportFrequencyfeatureExcel
        },
        "[action=frequencyfeatureAuditory]": {
          click: this._onFrequencyfeatureAuditory
        },
        "[action=newFrequencyfeature]": {
          click: this._newFrequencyfeature
        },
        "[action=deleteFrequencyfeature]": {
          click: this._onDeleteFrequencyfeature
        },
        "[action=searchFrequencyfeature]": {
          specialkey: this._searchFrequencyfeature
        }
      });
    },
    _newFrequencyfeature: function() {
      var modelFrequencyfeature = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterFrequencyfeature",
        {
          idFrequencyFeature: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterFrequencyFeature"
      )[0];
      var panel = general.down("ViewGridPanelRegisterFrequencyfeature");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelFrequencyfeature);
      editor.startEdit(0, 0);
    },
    _onDeleteFrequencyfeature: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterFrequencyFeature grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteFrequencyFeature.htm?nameView=ViewPanelRegisterFrequencyFeature"
      );
    },
    _searchFrequencyfeature: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterFrequencyFeature grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findFrequencyFeature.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridFrequencyfeature: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterFrequencyFeature grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportFrequencyfeaturePdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportFrequencyfeatureExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onFrequencyfeatureAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterFrequencyFeature grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditFrequencyFeature.htm"
      );
    }
  }
);
