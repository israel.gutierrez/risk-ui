Ext.define(
  "DukeSource.controller.risk.parameter.businessContinuity.ControllerPanelOwnerSolution",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.businessContinuity.grid.GridPanelRegisterOwnerSolution",
      "risk.parameter.businessContinuity.PanelRegisterOwnerSolution"
    ],
    init: function() {
      this.control({
        "[action=ownerSolutionAuditory]": {
          click: this._onOwnerSolutionAuditory
        },
        "[action=newOwnerSolution]": {
          click: this._newOwnerSolution
        },
        "[action=deleteOwnerSolution]": {
          click: this._onDeleteOwnerSolution
        },
        "[action=searchOwnerSolution]": {
          specialkey: this._searchOwnerSolution
        }
      });
    },
    _newOwnerSolution: function() {
      var modelOwnerSolution = Ext.create("ModelOwnerSolution", {
        id: "id",
        description: "",
        abbreviation: "",
        state: "S"
      });
      var general = Ext.ComponentQuery.query("PanelRegisterOwnerSolution")[0];
      var panel = general.down("GridPanelRegisterOwnerSolution");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelOwnerSolution);
      editor.startEdit(0, 0);
    },
    _onDeleteOwnerSolution: function() {
      var grid = Ext.ComponentQuery.query("PanelRegisterOwnerSolution grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteOwnerSolution.htm?nameView=PanelRegisterOwnerSolution"
      );
    },
    _searchOwnerSolution: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "PanelRegisterOwnerSolution grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findOwnerSolution.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onOwnerSolutionAuditory: function() {
      var grid = Ext.ComponentQuery.query("PanelRegisterOwnerSolution grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditOwnerSolution.htm"
      );
    }
  }
);
