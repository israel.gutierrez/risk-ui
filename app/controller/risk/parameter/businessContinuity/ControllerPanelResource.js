Ext.define(
  "DukeSource.controller.risk.parameter.businessContinuity.ControllerPanelResource",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.businessContinuity.grid.GridPanelRegisterResource",
      "risk.parameter.businessContinuity.PanelRegisterResource",
      "risk.parameter.combos.ComboTypeResource",
      "risk.parameter.combos.ViewComboWorkArea",
      "risk.parameter.combos.ViewComboJobPlace",
      "risk.parameter.combos.ViewComboAvailability"
    ],
    init: function() {
      this.control({
        "[action=integrityAuditory]": {
          click: this._onIntegrityAuditory
        },
        "PanelRegisterResource grid": {
          itemdblclick: this._modifyResourceContinuity
        },
        "[action=newResourceContinuity]": {
          click: this._newResourceContinuity
        },
        "[action=saveResourceContinuity]": {
          click: this._saveResourceContinuity
        },
        "[action=deleteResourceContinuity]": {
          click: this._deleteResourceContinuity
        },
        "[action=searchResourceContinuity]": {
          specialkey: this._searchResourceContinuity
        }
      });
    },
    _newResourceContinuity: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.WindowRegisterResourceContinuity",
        {
          modal: true
        }
      );
      win.show();
      win.down("#name").focus(false, 200);
    },
    _saveResourceContinuity: function(btn) {
      var grid = Ext.ComponentQuery.query("PanelRegisterResource grid")[0];
      var win = btn.up("window");
      var form = win.down("form").getForm();
      if (form.isValid()) {
        DukeSource.lib.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveResourceContinuity.htm?nameView=PanelRegisterResource",
          params: {
            jsonData: Ext.JSON.encode(form.getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              win.close();
              grid.down("pagingtoolbar").moveFirst();
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                response.mensaje,
                Ext.Msg.INFO
              );
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ADVERTENCIA,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      }
    },
    _deleteResourceContinuity: function() {
      var grid = Ext.ComponentQuery.query("PanelRegisterResource grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteResource.htm?nameView=PanelRegisterResource"
      );
    },
    _modifyResourceContinuity: function(btn) {
      var grid = Ext.ComponentQuery.query("PanelRegisterResource grid")[0];
      var record = grid.getSelectionModel().getSelection()[0];
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.WindowRegisterResourceContinuity",
        {
          modal: true
        }
      );
      win.down("#idWorkArea").store.load();
      win.down("#idJobPlace").store.load();
      win.show();
      win
        .down("form")
        .getForm()
        .loadRecord(record);
      win.down("#name").focus(false, 200);
    },
    _searchResourceContinuity: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query("PanelRegisterResource grid")[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findResource.htm",
          "r.description",
          "r.description"
        );
      } else {
      }
    },
    _onIntegrityAuditory: function() {
      var grid = Ext.ComponentQuery.query("PanelRegisterResource grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findResource.htm"
      );
    }
  }
);
