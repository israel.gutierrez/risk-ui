Ext.define(
  "DukeSource.controller.risk.parameter.businessContinuity.ControllerPanelTypeResource",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.businessContinuity.grid.GridPanelRegisterTypeResource",
      "risk.parameter.businessContinuity.PanelRegisterTypeResource"
    ],
    init: function() {
      this.control({
        "[action=typeResourceAuditory]": {
          click: this._onTypeResourceAuditory
        },
        "[action=newTypeResource]": {
          click: this._newTypeResource
        },
        "[action=deleteTypeResource]": {
          click: this._onDeleteTypeResource
        },
        "[action=searchTypeResource]": {
          specialkey: this._searchTypeResource
        }
      });
    },
    _newTypeResource: function() {
      var modelTypeResource = Ext.create("ModelTypeResource", {
        id: "id",
        description: "",
        abbreviation: "",
        state: "S"
      });
      var general = Ext.ComponentQuery.query("PanelRegisterTypeResource")[0];
      var panel = general.down("GridPanelRegisterTypeResource");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelTypeResource);
      editor.startEdit(0, 0);
    },
    _onDeleteTypeResource: function() {
      var grid = Ext.ComponentQuery.query("PanelRegisterTypeResource grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteTypeResource.htm?nameView=PanelRegisterTypeResource"
      );
    },
    _searchTypeResource: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "PanelRegisterTypeResource grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findTypeResource.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onTypeResourceAuditory: function() {
      var grid = Ext.ComponentQuery.query("PanelRegisterTypeResource grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditTypeResource.htm"
      );
    }
  }
);
