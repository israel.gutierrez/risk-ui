Ext.define(
  "DukeSource.controller.risk.parameter.businessContinuity.ControllerPanelTypeSolution",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.businessContinuity.grid.GridPanelRegisterTypeSolution",
      "risk.parameter.businessContinuity.PanelRegisterTypeSolution"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridTypeSolution]": {
          keyup: this._onSearchTriggerGridTypeSolution
        },
        "[action=exportTypeSolutionPdf]": {
          click: this._onExportTypeSolutionPdf
        },
        "[action=exportTypeSolutionExcel]": {
          click: this._onExportTypeSolutionExcel
        },
        "[action=typeSolutionAuditory]": {
          click: this._onTypeSolutionAuditory
        },
        "[action=newTypeSolution]": {
          click: this._newTypeSolution
        },
        "[action=deleteTypeSolution]": {
          click: this._onDeleteTypeSolution
        },
        "[action=searchTypeSolution]": {
          specialkey: this._searchTypeSolution
        }
      });
    },
    _newTypeSolution: function() {
      var modelTypeSolution = Ext.create("ModelTypeSolution", {
        id: "id",
        description: "",
        abbreviation: "",
        state: "S"
      });
      var general = Ext.ComponentQuery.query("PanelRegisterTypeSolution")[0];
      var panel = general.down("GridPanelRegisterTypeSolution");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelTypeSolution);
      editor.startEdit(0, 0);
    },
    _onDeleteTypeSolution: function() {
      var grid = Ext.ComponentQuery.query("PanelRegisterTypeSolution grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteTypeSolution.htm?nameView=PanelRegisterTypeSolution"
      );
    },
    _searchTypeSolution: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "PanelRegisterTypeSolution grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findTypeSolution.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridTypeSolution: function(text) {
      var grid = Ext.ComponentQuery.query("PanelRegisterTypeSolution grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportTypeSolutionPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportTypeSolutionExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onTypeSolutionAuditory: function() {
      var grid = Ext.ComponentQuery.query("PanelRegisterTypeSolution grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditTypeSolution.htm"
      );
    }
  }
);
