Ext.define(
  "DukeSource.controller.risk.parameter.businessContinuity.ControllerPanelTypeInterruption",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.businessContinuity.grid.GridPanelRegisterTypeInterruption",
      "risk.parameter.businessContinuity.PanelRegisterTypeInterruption"
    ],
    init: function() {
      this.control({
        "[action=typeInterruptionAuditory]": {
          click: this._onTypeInterruptionAuditory
        },
        "[action=newTypeInterruption]": {
          click: this._newTypeInterruption
        },
        "[action=deleteTypeInterruption]": {
          click: this._onDeleteTypeInterruption
        },
        "[action=searchTypeInterruption]": {
          specialkey: this._searchTypeInterruption
        }
      });
    },
    _newTypeInterruption: function() {
      var modelTypeInterruption = Ext.create("ModelTypeInterruption", {
        id: "id",
        description: "",
        abbreviation: "",
        state: "S"
      });
      var general = Ext.ComponentQuery.query(
        "PanelRegisterTypeInterruption"
      )[0];
      var panel = general.down("GridPanelRegisterTypeInterruption");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelTypeInterruption);
      editor.startEdit(0, 0);
    },
    _onDeleteTypeInterruption: function() {
      var grid = Ext.ComponentQuery.query(
        "PanelRegisterTypeInterruption grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteTypeInterruption.htm?nameView=PanelRegisterTypeInterruption"
      );
    },
    _searchTypeInterruption: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "PanelRegisterTypeInterruption grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findTypeInterruption.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onTypeInterruptionAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "PanelRegisterTypeInterruption grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditTypeInterruption.htm"
      );
    }
  }
);
