Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterStateTables",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterStateTables"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterStateTables"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterStateTables",
      "risk.parameter.others.ViewPanelRegisterStateTables"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridStateTables]": {
          keyup: this._onSearchTriggerGridStateTables
        },
        "[action=exportStateTablesPdf]": {
          click: this._onExportStateTablesPdf
        },
        "[action=exportStateTablesExcel]": {
          click: this._onExportStateTablesExcel
        },
        "[action=stateTablesAuditory]": {
          click: this._onStateTablesAuditory
        },
        "[action=newStateTables]": {
          click: this._newStateTables
        },
        "[action=deleteStateTables]": {
          click: this._onDeleteStateTables
        },
        "[action=searchStateTables]": {
          specialkey: this._searchStateTables
        }
      });
    },
    _newStateTables: function() {
      var modelStateTables = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterStateTables",
        {
          idStateTables: "id",
          description: "",
          nameTable: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterStateTables")[0];
      var panel = general.down("ViewGridPanelRegisterStateTables");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelStateTables);
      editor.startEdit(0, 0);
    },
    _onDeleteStateTables: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterStateTables grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteStateTables.htm"
      );
    },
    _searchStateTables: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterStateTables grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findStateTables.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridStateTables: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterStateTables grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportStateTablesPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportStateTablesExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onStateTablesAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterStateTables grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditStateTables.htm"
      );
    }
  }
);
