Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterPercentReduction",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterPercentReduction"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterPercentReduction"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterPercentReduction",
      "risk.parameter.others.ViewPanelRegisterPercentReduction"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridPercentReduction]": {
          keyup: this._onSearchTriggerGridPercentReduction
        },
        "[action=exportPercentReductionPdf]": {
          click: this._onExportPercentReductionPdf
        },
        "[action=exportPercentReductionExcel]": {
          click: this._onExportPercentReductionExcel
        },
        "[action=percentReductionAuditory]": {
          click: this._onPercentReductionAuditory
        },
        "[action=newPercentReduction]": {
          click: this._newPercentReduction
        },
        "[action=deletePercentReduction]": {
          click: this._onDeletePercentReduction
        },
        "[action=searchPercentReduction]": {
          specialkey: this._searchPercentReduction
        }
      });
    },
    _newPercentReduction: function() {
      var modelPercentReduction = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterPercentReduction",
        {
          idPercentReduction: "id",
          valuePercent: "",
          state: "S",
          typePercent: ""
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterPercentReduction"
      )[0];
      var panel = general.down("ViewGridPanelRegisterPercentReduction");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelPercentReduction);
      editor.startEdit(0, 0);
    },
    _onDeletePercentReduction: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterPercentReduction grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deletePercentReduction.htm"
      );
    },
    _searchPercentReduction: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterPercentReduction grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findPercentReduction.htm",
          "idPercentReduction",
          "idPercentReduction"
        );
      } else {
      }
    },
    _onSearchTriggerGridPercentReduction: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterPercentReduction grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportPercentReductionPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportPercentReductionExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onPercentReductionAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterPercentReduction grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditPercentReduction.htm"
      );
    }
  }
);
