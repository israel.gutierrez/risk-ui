Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterTypeEvent",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterTypeEvent"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterTypeEvent"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterTypeEvent",
      "risk.parameter.others.ViewPanelRegisterTypeEvent"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridTypeEvent]": {
          keyup: this._onSearchTriggerGridTypeEvent
        },
        "[action=exportTypeEventPdf]": {
          click: this._onExportTypeEventPdf
        },
        "[action=exportTypeEventExcel]": {
          click: this._onExportTypeEventExcel
        },
        "[action=typeEventAuditory]": {
          click: this._onTypeEventAuditory
        },
        "[action=newTypeEvent]": {
          click: this._newTypeEvent
        },
        "[action=deleteTypeEvent]": {
          click: this._onDeleteTypeEvent
        },
        "[action=searchTypeEvent]": {
          specialkey: this._searchTypeEvent
        }
      });
    },
    _newTypeEvent: function() {
      var modelTypeEvent = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterTypeEvent",
        {
          idTypeEvent: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterTypeEvent")[0];
      var panel = general.down("ViewGridPanelRegisterTypeEvent");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelTypeEvent);
      editor.startEdit(0, 0);
    },
    _onDeleteTypeEvent: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterTypeEvent grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteTypeEvent.htm?nameView=ViewPanelRegisterTypeEvent"
      );
    },
    _searchTypeEvent: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterTypeEvent grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findTypeEvent.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridTypeEvent: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterTypeEvent grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportTypeEventPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportTypeEventExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onTypeEventAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterTypeEvent grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditTypeEvent.htm"
      );
    }
  }
);
