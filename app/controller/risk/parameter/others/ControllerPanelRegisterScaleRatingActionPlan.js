Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterScaleRatingActionPlan",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.parameter.grids.StoreGridPanelRegisterScaleRatingActionPlan"
    ],
    models: [
      "risk.parameter.grids.ModelGridPanelRegisterScaleRatingActionPlan"
    ],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterScaleRatingActionPlan",
      "risk.parameter.others.ViewPanelRegisterScaleRatingActionPlan"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridScaleRatingActionPlan]": {
          keyup: this._onSearchTriggerGridScaleRatingActionPlan
        },
        "[action=exportScaleRatingActionPlanPdf]": {
          click: this._onExportScaleRatingActionPlanPdf
        },
        "[action=exportScaleRatingActionPlanExcel]": {
          click: this._onExportScaleRatingActionPlanExcel
        },
        "[action=scaleRatingActionPlanAuditory]": {
          click: this._onScaleRatingActionPlanAuditory
        },
        "[action=newScaleRatingActionPlan]": {
          click: this._newScaleRatingActionPlan
        },
        "[action=deleteScaleRatingActionPlan]": {
          click: this._onDeleteScaleRatingActionPlan
        },
        "[action=searchScaleRatingActionPlan]": {
          specialkey: this._searchScaleRatingActionPlan
        }
      });
    },
    _newScaleRatingActionPlan: function() {
      var modelScaleRatingActionPlan = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterScaleRatingActionPlan",
        {
          idScaleRatingActionPlan: "id",
          codeColour: "",
          rangeMin: "",
          rangeMax: "",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterScaleRatingActionPlan"
      )[0];
      var panel = general.down("ViewGridPanelRegisterScaleRatingActionPlan");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelScaleRatingActionPlan);
      editor.startEdit(0, 0);
    },
    _onDeleteScaleRatingActionPlan: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterScaleRatingActionPlan grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteScaleRatingActionPlan.htm"
      );
    },
    _searchScaleRatingActionPlan: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterScaleRatingActionPlan grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findScaleRatingActionPlan.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridScaleRatingActionPlan: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterScaleRatingActionPlan grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportScaleRatingActionPlanPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportScaleRatingActionPlanExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onScaleRatingActionPlanAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterScaleRatingActionPlan grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditScaleRatingActionPlan.htm"
      );
    }
  }
);
