Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterGroupIncidents",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterGroupIncidents",
      "risk.parameter.others.ViewPanelRegisterGroupIncidents"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridGroupIncidents]": {
          keyup: this._onSearchTriggerGridGroupIncidents
        },
        "[action=exportGroupIncidentsPdf]": {
          click: this._onExportGroupIncidentsPdf
        },
        "[action=exportGroupIncidentsExcel]": {
          click: this._onExportGroupIncidentsExcel
        },
        "[action=GroupIncidentsAuditory]": {
          click: this._onGroupIncidentsAuditory
        },
        "[action=newGroupIncidents]": {
          click: this._newGroupIncidents
        },
        "[action=deleteGroupIncidents]": {
          click: this._onDeleteGroupIncidents
        },
        "[action=searchGroupIncidents]": {
          specialkey: this._searchGroupIncidents
        }
      });
    },
    _newGroupIncidents: function() {
      var modelGroupIncidents = Ext.create(
        "ModelGridPanelRegisterGroupIncidents",
        {
          idIncidentGroup: "id",
          description: "",
          state: "S"
        }
      );
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterGroupIncidents grid"
      )[0];
      var editor = grid.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelGroupIncidents);
      editor.startEdit(0, 0);
    },
    _onDeleteGroupIncidents: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterGroupIncidents grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteIncidentGroup.htm?nameView=ViewPanelRegisterGroupIncidents"
      );
    },
    _searchGroupIncidents: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterGroupIncidents grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findIncidentGroup.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridGroupIncidents: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterGroupIncidents grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportGroupIncidentsPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportGroupIncidentsExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onGroupIncidentsAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterGroupIncidents grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditGroupIncidents.htm"
      );
    }
  }
);
