Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterActionPlan",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterActionPlan"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterActionPlan"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterActionPlan",
      "risk.parameter.others.ViewPanelRegisterActionPlan",
      "risk.parameter.combos.ViewComboIndicatorType",
      "risk.parameter.combos.ViewComboOriginActionPlan"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridActionPlan]": {
          keyup: this._onSearchTriggerGridActionPlan
        },
        "[action=exportActionPlanPdf]": {
          click: this._onExportActionPlanPdf
        },
        "[action=exportActionPlanExcel]": {
          click: this._onExportActionPlanExcel
        },
        "[action=actionPlanAuditory]": {
          click: this._onActionPlanAuditory
        },
        "[action=newActionPlan]": {
          click: this._newActionPlan
        },
        "[action=deleteActionPlan]": {
          click: this._onDeleteActionPlan
        },
        "[action=searchActionPlan]": {
          specialkey: this._searchActionPlan
        }
      });
    },
    _newActionPlan: function() {
      var modelActionPlan = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterActionPlan",
        {
          idActionPlan: "id",
          codePlan: "",
          indicatorType: "",
          originPlanAction: "",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterActionPlan")[0];
      var panel = general.down("ViewGridPanelRegisterActionPlan");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelActionPlan);
      editor.startEdit(0, 0);
    },
    _onDeleteActionPlan: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterActionPlan grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteActionPlan.htm?nameView=ViewPanelRegisterActionPlan"
      );
    },
    _searchActionPlan: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterActionPlan grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findActionPlan.htm",
          "description",
          "idActionPlan"
        );
      } else {
      }
    },
    _onSearchTriggerGridActionPlan: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterActionPlan grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportActionPlanPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportActionPlanExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onActionPlanAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterActionPlan grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditActionPlan.htm"
      );
    }
  }
);
