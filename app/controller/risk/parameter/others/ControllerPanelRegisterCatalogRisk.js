Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterCatalogRisk",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterCatalogRisk"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterCatalogRisk"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterCatalogRisk",
      "risk.parameter.others.ViewPanelRegisterCatalogRisk",
      "DukeSource.view.risk.util.ViewComboYesNo"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridCatalogRisk]": {
          keyup: this._onSearchTriggerGridCatalogRisk
        },
        "[action=exportCatalogRiskPdf]": {
          click: this._onExportCatalogRiskPdf
        },
        "[action=exportCatalogRiskExcel]": {
          click: this._onExportCatalogRiskExcel
        },
        "[action=catalogRiskAuditory]": {
          click: this._onCatalogRiskAuditory
        },
        "[action=newCatalogRisk]": {
          click: this._newCatalogRisk
        },
        "[action=saveCatalogRisk]": {
          click: this._onSaveCatalogRisk
        },
        "[action=deleteCatalogRisk]": {
          click: this._onDeleteCatalogRisk
        },
        "[action=searchCatalogRisk]": {
          specialkey: this._searchCatalogRisk
        },
        "ViewPanelRegisterCatalogRisk grid": {
          itemdblclick: this._onModifyCatalogRisk
        }
      });
    },
    _newCatalogRisk: function() {
      var window = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowCatalogRisk",
        {
          modal: true
        }
      ).show();
    },
    _onSaveCatalogRisk: function() {
      var win = Ext.ComponentQuery.query("ViewWindowCatalogRisk")[0];
      if (
        win
          .down("form")
          .getForm()
          .isValid()
      ) {
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveCatalogRisk.htm?nameView=ViewPanelRegisterCatalogRisk",
          params: {
            jsonData: Ext.JSON.encode(win.down("form").getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              win.close();
              DukeSource.global.DirtyView.messageNormal(response.message);
              Ext.ComponentQuery.query("ViewPanelRegisterCatalogRisk grid")[0]
                .getStore()
                .load();
            } else {
              DukeSource.global.DirtyView.messageWarning(response.message);
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onDeleteCatalogRisk: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterCatalogRisk grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteCatalogRisk.htm?nameView=ViewPanelRegisterCatalogRisk"
      );
    },
    _searchCatalogRisk: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterCatalogRisk grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findCatalogRisk.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridCatalogRisk: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterCatalogRisk grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportCatalogRiskPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportCatalogRiskExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onCatalogRiskAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterCatalogRisk grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditCatalogRisk.htm"
      );
    },
    _onModifyCatalogRisk: function(grid) {
      var window = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowCatalogRisk",
        {
          modal: true
        }
      ).show();
      window
        .down("form")
        .getForm()
        .loadRecord(grid.getSelectionModel().getSelection()[0]);
    }
  }
);
