Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterTypeRiskEvaluation",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterTypeRiskEvaluation"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterTypeRiskEvaluation"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterTypeRiskEvaluation",
      "risk.parameter.others.ViewPanelRegisterTypeRiskEvaluation"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridTypeRiskEvaluation]": {
          keyup: this._onSearchTriggerGridTypeRiskEvaluation
        },
        "[action=exportTypeRiskEvaluationPdf]": {
          click: this._onExportTypeRiskEvaluationPdf
        },
        "[action=exportTypeRiskEvaluationExcel]": {
          click: this._onExportTypeRiskEvaluationExcel
        },
        "[action=typeRiskEvaluationAuditory]": {
          click: this._onTypeRiskEvaluationAuditory
        },
        "[action=newTypeRiskEvaluation]": {
          click: this._newTypeRiskEvaluation
        },
        "[action=deleteTypeRiskEvaluation]": {
          click: this._onDeleteTypeRiskEvaluation
        },
        "[action=searchTypeRiskEvaluation]": {
          specialkey: this._searchTypeRiskEvaluation
        }
      });
    },
    _newTypeRiskEvaluation: function() {
      var modelTypeRiskEvaluation = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterTypeRiskEvaluation",
        {
          idTypeRiskEvaluation: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeRiskEvaluation"
      )[0];
      var panel = general.down("ViewGridPanelRegisterTypeRiskEvaluation");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelTypeRiskEvaluation);
      editor.startEdit(0, 0);
    },
    _onDeleteTypeRiskEvaluation: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeRiskEvaluation grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteTypeRiskEvaluation.htm?nameView=ViewPanelRegisterTypeRiskEvaluation"
      );
    },
    _searchTypeRiskEvaluation: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterTypeRiskEvaluation grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findTypeRiskEvaluation.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridTypeRiskEvaluation: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeRiskEvaluation grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportTypeRiskEvaluationPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportTypeRiskEvaluationExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onTypeRiskEvaluationAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeRiskEvaluation grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditTypeRiskEvaluation.htm"
      );
    }
  }
);
