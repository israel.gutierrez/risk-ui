Ext.define("DukeSource.controller.risk.parameter.others.ControllerPanelPaf", {
  extend: "Ext.app.Controller",
  stores: [],
  models: [],
  views: [
    "risk.parameter.grids.TreeGridPaf",
    "risk.parameter.others.PanelRegisterPaf"
  ],
  init: function() {
    this.control({
      "[action=modifyPaf]": {
        click: this._onModifyPaf
      },
      "[action=auditPaf]": {
        click: this._onPafAuditory
      },
      "[action=newPaf]": {
        click: this._newPaf
      },
      "[action=deletePaf]": {
        click: this._onDeletePaf
      },
      "[action=savePaf]": {
        click: this._savePaf
      },
      "[action=searchPaf]": {
        specialkey: this._searchPaf
      },
      TreeGridPaf: {
        itemcontextmenu: this._rightClickPaf
      }
    });
  },
  _newPaf: function() {
    var tree = Ext.ComponentQuery.query("TreeGridPaf")[0];
    var node = tree.getSelectionModel().getSelection()[0];

    if (node === undefined) {
      DukeSource.global.DirtyView.messageAlert(
        DukeSource.global.GiroMessages.TITLE_WARNING,
        DukeSource.global.GiroMessages.MESSAGE_ITEM,
        Ext.Msg.WARNING
      );
    } else {
      var window = Ext.create(
        "DukeSource.view.risk.parameter.windows.WindowRegisterPaf",
        {
          modal: true,
          prefix: node.getPath("description", " &#8702; ").substring(18)
        }
      );

      window.down("#parent").setValue(node.data["id"]);
      window.down("#depth").setValue(node.data["depth"] + 1);
      window.show();
      window.down("#description").focus(false, 100);
    }
  },
  _onDeletePaf: function() {
    var tree = Ext.ComponentQuery.query("TreeGridPaf")[0];
    var node = tree.getSelectionModel().getSelection()[0];
    if (node === undefined) {
      DukeSource.global.DirtyView.messageAlert(
        DukeSource.global.GiroMessages.TITLE_WARNING,
        DukeSource.global.GiroMessages.MESSAGE_ITEM,
        Ext.Msg.WARNING
      );
    } else {
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
        msg: "Esta seguro que desea eliminar el item?",
        icon: Ext.Msg.WARNING,
        buttonText: {
          yes: "Si"
        },
        buttons: Ext.MessageBox.YESNO,
        fn: function(btn) {
          if (btn === "yes") {
            DukeSource.lib.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/deletePaf.htm?nameView=PanelRegisterPaf",
              params: {
                node: Ext.JSON.encode(node.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  tree.store.getProxy().extraParams = {
                    depth: node.data["depth"]
                  };
                  tree.store.load({
                    node: node.parentNode
                  });
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_CONFIRM,
                    response.message,
                    Ext.Msg.INFO
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_WARNING,
                    response.message,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_WARNING,
                  response.message,
                  Ext.Msg.ERROR
                );
              }
            });
          }
        }
      });
    }
  },

  _savePaf: function(btn) {
    var tree = Ext.ComponentQuery.query("TreeGridPaf")[0];
    var node = tree.getSelectionModel().getSelection()[0];
    var windows = btn.up("window");
    var form = windows.down("form");
    form
      .down("#path")
      .setValue(
        windows.prefix + " &#8702; " + form.down("#description").getValue()
      );

    if (form.getForm().isValid()) {
      DukeSource.lib.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/savePaf.htm?nameView=PanelRegisterPaf",
        params: {
          jsonData: Ext.JSON.encode(form.getForm().getValues())
        },
        success: function(response) {
          response = Ext.decode(response.responseText);

          if (response.success) {
            tree.store.getProxy().extraParams = {
              depth: node.data["depth"]
            };
            tree.store.load({
              node: node.parentNode
            });
            windows.close();
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_CONFIRM,
              response.message,
              Ext.Msg.INFO
            );
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              response.message,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {
          DukeSource.global.DirtyView.messageAlert(
            DukeSource.global.GiroMessages.TITLE_WARNING,
            response.message,
            Ext.Msg.ERROR
          );
        }
      });
    } else {
      DukeSource.global.DirtyView.messageAlert(
        DukeSource.global.GiroMessages.TITLE_WARNING,
        DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
        Ext.Msg.ERROR
      );
    }
  },
  _onModifyPaf: function(text) {
    var tree = Ext.ComponentQuery.query("TreeGridPaf")[0];
    var node = tree.getSelectionModel().getSelection()[0];

    if (node === undefined) {
      DukeSource.global.DirtyView.messageAlert(
        DukeSource.global.GiroMessages.TITLE_WARNING,
        DukeSource.global.GiroMessages.MESSAGE_ITEM,
        Ext.Msg.WARNING
      );
    } else {
      var view = Ext.create(
        "DukeSource.view.risk.parameter.windows.WindowRegisterPaf",
        {
          modal: true,
          prefix: node.parentNode
            .getPath("description", " &#8702; ")
            .substring(18)
        }
      );
      view
        .down("form")
        .getForm()
        .setValues(node.raw);
      view.down("#depth").setValue(node.data["depth"]);
      view.show();
      view.down("#description").focus(false, 100);
    }
  },
  _onPafAuditory: function() {
    var grid = Ext.ComponentQuery.query("PanelRegisterPaf grid")[0];
    DukeSource.global.DirtyView.showWindowAuditory(
      grid,
      "http://localhost:9000/giro/findAuditPaf.htm"
    );
  },

  _rightClickPaf: function(view, rec, node, index, e) {
    e.stopEvent();
    var addMenu = Ext.create("DukeSource.view.fulfillment.AddMenu", {});
    addMenu.removeAll();
    addMenu.add(
      {
        text: "Nuevo",
        iconCls: "add",
        action: "newPaf"
      },
      {
        text: "Modificar",
        disabled: rec.data.depth === 1,
        action: "modifyPaf",
        iconCls: "modify"
      },
      {
        text: "Eliminar",
        disabled: rec.data.depth === 1,
        action: "deletePaf",
        iconCls: "delete"
      }
    );
    addMenu.showAt(e.getXY());
  },

  _searchPaf: function(field, e) {
    var me = field.up("panel").down("treepanel");

    if (e.getKey() === e.ENTER) {
      field.getValue() === ""
        ? me.collapseAll()
        : me.filterByText(field.getValue());
    }
  }
});
