Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterForeignKeys",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterForeignKeys",
      "risk.parameter.others.ViewPanelRegisterForeignKeys"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridForeignKeys]": {
          keyup: this._onSearchTriggerGridForeignKeys
        },
        "[action=exportForeignKeysPdf]": {
          click: this._onExportForeignKeysPdf
        },
        "[action=exportForeignKeysExcel]": {
          click: this._onExportForeignKeysExcel
        },
        "[action=ForeignKeysAuditory]": {
          click: this._onForeignKeysAuditory
        },
        "[action=newForeignKeys]": {
          click: this._newForeignKeys
        },
        "[action=deleteForeignKeys]": {
          click: this._onDeleteForeignKeys
        },
        "[action=searchForeignKeys]": {
          specialkey: this._searchForeignKeys
        }
      });
    },
    _newForeignKeys: function() {
      var modelForeignKeys = Ext.create("ModelGridPanelRegisterForeignKeys", {
        idFore: "id",
        identified: "",
        nameTable: "",
        nameColumn: "",
        description: "",
        value: "",
        state: "S"
      });
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterForeignKeys grid"
      )[0];
      var editor = grid.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelForeignKeys);
      editor.startEdit(0, 0);
    },
    _onDeleteForeignKeys: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterForeignKeys grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteForeignKeys.htm"
      );
    },
    _searchForeignKeys: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterForeignKeys grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/showListForeignActives.htm",
          "identified",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridForeignKeys: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterForeignKeys grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportForeignKeysPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportForeignKeysExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onForeignKeysAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterForeignKeys grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditForeignKeys.htm"
      );
    }
  }
);
