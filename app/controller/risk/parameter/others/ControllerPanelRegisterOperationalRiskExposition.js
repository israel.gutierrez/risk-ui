Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterOperationalRiskExposition",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.parameter.grids.StoreGridPanelRegisterOperationalRiskExposition"
    ],
    models: [
      "risk.parameter.grids.ModelGridPanelRegisterOperationalRiskExposition"
    ],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterOperationalRiskExposition",
      "risk.parameter.others.ViewPanelRegisterOperationalRiskExposition"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridOperationalRiskExposition]": {
          keyup: this._onSearchTriggerGridOperationalRiskExposition
        },
        "[action=exportOperationalRiskExpositionPdf]": {
          click: this._onExportOperationalRiskExpositionPdf
        },
        "[action=exportOperationalRiskExpositionExcel]": {
          click: this._onExportOperationalRiskExpositionExcel
        },
        "[action=operationalRiskExpositionAuditory]": {
          click: this._onOperationalRiskExpositionAuditory
        },
        "[action=newOperationalRiskExposition]": {
          click: this._newOperationalRiskExposition
        },
        "[action=deleteOperationalRiskExposition]": {
          click: this._onDeleteOperationalRiskExposition
        },
        "[action=searchOperationalRiskExposition]": {
          specialkey: this._searchOperationalRiskExposition
        }
      });
    },
    _newOperationalRiskExposition: function() {
      var modelOperationalRiskExposition = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterOperationalRiskExposition",
        {
          idOperationalRiskExposition: "id",
          maxAmount: "",
          year: ""
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterOperationalRiskExposition"
      )[0];
      var panel = general.down(
        "ViewGridPanelRegisterOperationalRiskExposition"
      );
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelOperationalRiskExposition);
      editor.startEdit(0, 0);
    },
    _onDeleteOperationalRiskExposition: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterOperationalRiskExposition grid"
      )[0];
      if (grid.getSelectionModel().getCount() == 0) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        if (
          grid
            .getSelectionModel()
            .getSelection()[0]
            .get("validity") == "S"
        ) {
          DukeSource.global.DirtyView.messageAlert(
            DukeSource.global.GiroMessages.TITLE_MESSAGE,
            "El item esta en uso, NO se puede eliminar",
            Ext.Msg.WARNING
          );
        } else {
          DukeSource.global.DirtyView.deleteElementToGrid(
            grid,
            "http://localhost:9000/giro/deleteOperationalRiskExposition.htm?nameView=ViewPanelRegisterOperationalRiskExposition"
          );
        }
      }
    },
    _searchOperationalRiskExposition: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterOperationalRiskExposition grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findOperationalRiskExposition.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridOperationalRiskExposition: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterOperationalRiskExposition grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportOperationalRiskExpositionPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportOperationalRiskExpositionExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onOperationalRiskExpositionAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterOperationalRiskExposition grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditOperationalRiskExposition.htm"
      );
    }
  }
);
