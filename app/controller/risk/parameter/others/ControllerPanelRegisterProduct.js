Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterProduct",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterProduct"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterProduct"],
    views: ["risk.parameter.others.ViewPanelRegisterProduct"],
    init: function() {
      this.control({
        "[action=modifyProduct]": {
          click: this._onModifyProduct
        },
        "[action=productAuditory]": {
          click: this._onProductAuditory
        },
        "[action=newProduct]": {
          click: this._newProduct
        },
        "[action=deleteProduct]": {
          click: this._onDeleteProduct
        },
        "[action=saveProduct]": {
          click: this._saveProduct
        },
        TreePanelProduct: {
          itemcontextmenu: this._rightClickProduct
        }
      });
    },
    _newProduct: function() {
      var tree = Ext.ComponentQuery.query("TreePanelProduct")[0];
      var node = tree.getSelectionModel().getSelection()[0];

      if (node === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        var window = Ext.create(
          "DukeSource.view.risk.parameter.windows.WindowRegisterProduct",
          {
            modal: true,
            prefix: node.getPath("description", " &#8702; ").substring(18)
          }
        );

        window.down("#parent").setValue(node.data["id"]);
        window.down("#depth").setValue(node.data["depth"] + 1);
        window.show();
        window.down("#description").focus(false, 100);
      }
    },
    _onDeleteProduct: function() {
      var tree = Ext.ComponentQuery.query("TreePanelProduct")[0];
      var node = tree.getSelectionModel().getSelection()[0];
      if (node === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        Ext.MessageBox.show({
          title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
          msg: "Esta seguro que desea eliminar el item?",
          icon: Ext.Msg.WARNING,
          buttonText: {
            yes: "Si"
          },
          buttons: Ext.MessageBox.YESNO,
          fn: function(btn) {
            if (btn === "yes") {
              DukeSource.lib.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/deleteProduct.htm?nameView=ViewPanelRegisterProduct",
                params: {
                  node: Ext.JSON.encode(node.data)
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    tree.store.getProxy().extraParams = {
                      depth: node.data["depth"]
                    };
                    tree.store.load({
                      node: node.parentNode
                    });
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_CONFIRM,
                      response.mensaje,
                      Ext.Msg.INFO
                    );
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      response.mensaje,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function() {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_WARNING,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              });
            }
          }
        });
      }
    },

    _saveProduct: function(btn) {
      var tree = Ext.ComponentQuery.query("TreePanelProduct")[0];
      var node = tree.getSelectionModel().getSelection()[0];
      var windows = btn.up("window");
      var form = windows.down("form");
      form
        .down("#path")
        .setValue(
          windows.prefix + " &#8702; " + form.down("#description").getValue()
        );

      if (form.getForm().isValid()) {
        DukeSource.lib.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveProduct.htm?nameView=ViewPanelRegisterProduct",
          params: {
            jsonData: Ext.JSON.encode(form.getForm().getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);

            if (response.success) {
              tree.store.getProxy().extraParams = {
                depth: node.data["depth"]
              };
              tree.store.load({
                node: node.parentNode
              });
              windows.close();
              DukeSource.global.DirtyView.messageNormal(response.message);
            } else {
              DukeSource.global.DirtyView.messageAlert(response.message);
            }
          },
          failure: function() {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              response.message,
              Ext.Msg.ERROR
            );
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onModifyProduct: function(text) {
      var tree = Ext.ComponentQuery.query("TreePanelProduct")[0];
      var node = tree.getSelectionModel().getSelection()[0];

      if (node === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        var view = Ext.create(
          "DukeSource.view.risk.parameter.windows.WindowRegisterProduct",
          {
            modal: true,
            prefix: node.parentNode
              .getPath("description", " &#8702; ")
              .substring(18)
          }
        );
        view
          .down("form")
          .getForm()
          .setValues(node.raw);
        view.down("#depth").setValue(node.data["depth"]);
        view.show();
        view.down("#description").focus(false, 100);
      }
    },
    _onProductAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterProduct grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditProduct.htm"
      );
    },

    _rightClickProduct: function(view, rec, node, index, e) {
      e.stopEvent();
      var addMenu = Ext.create("DukeSource.view.fulfillment.AddMenu", {});
      addMenu.removeAll();
      addMenu.add(
        {
          text: "Nuevo",
          iconCls: "add",
          action: "newProduct"
        },
        {
          text: "Modificar",
          disabled: rec.data.depth === 1,
          action: "modifyProduct",
          iconCls: "modify"
        },
        {
          text: "Eliminar",
          disabled: rec.data.depth === 1,
          action: "deleteProduct",
          iconCls: "delete"
        }
      );
      addMenu.showAt(e.getXY());
    }
  }
);
