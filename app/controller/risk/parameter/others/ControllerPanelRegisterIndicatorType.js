Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterIndicatorType",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterIndicatorType"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterIndicatorType"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterIndicatorType",
      "risk.parameter.others.ViewPanelRegisterIndicatorType"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridIndicatorType]": {
          keyup: this._onSearchTriggerGridIndicatorType
        },
        "[action=exportIndicatorTypePdf]": {
          click: this._onExportIndicatorTypePdf
        },
        "[action=exportIndicatorTypeExcel]": {
          click: this._onExportIndicatorTypeExcel
        },
        "[action=indicatorTypeAuditory]": {
          click: this._onIndicatorTypeAuditory
        },
        "[action=newIndicatorType]": {
          click: this._newIndicatorType
        },
        "[action=deleteIndicatorType]": {
          click: this._onDeleteIndicatorType
        },
        "[action=searchIndicatorType]": {
          specialkey: this._searchIndicatorType
        }
      });
    },
    _newIndicatorType: function() {
      var modelIndicatorType = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterIndicatorType",
        {
          idIndicatorType: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterIndicatorType"
      )[0];
      var panel = general.down("ViewGridPanelRegisterIndicatorType");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelIndicatorType);
      editor.startEdit(0, 0);
    },
    _onDeleteIndicatorType: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterIndicatorType grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteIndicatorType.htm?nameView=ViewPanelRegisterIndicatorType"
      );
    },
    _searchIndicatorType: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterIndicatorType grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findIndicatorType.htm?nameView=ViewPanelRegisterIndicatorType",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridIndicatorType: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterIndicatorType grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportIndicatorTypePdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportIndicatorTypeExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onIndicatorTypeAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterIndicatorType grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditIndicatorType.htm"
      );
    }
  }
);
