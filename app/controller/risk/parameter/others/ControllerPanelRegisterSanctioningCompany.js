Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterSanctioningCompany",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterSanctioningCompany"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterSanctioningCompany"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterSanctioningCompany",
      "risk.parameter.others.ViewPanelRegisterSanctioningCompany"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridSanctioningCompany]": {
          keyup: this._onSearchTriggerGridSanctioningCompany
        },
        "[action=exportSanctioningCompanyPdf]": {
          click: this._onExportSanctioningCompanyPdf
        },
        "[action=exportSanctioningCompanyExcel]": {
          click: this._onExportSanctioningCompanyExcel
        },
        "[action=sanctioningCompanyAuditory]": {
          click: this._onSanctioningCompanyAuditory
        },
        "[action=newSanctioningCompany]": {
          click: this._newSanctioningCompany
        },
        "[action=deleteSanctioningCompany]": {
          click: this._onDeleteSanctioningCompany
        },
        "[action=searchSanctioningCompany]": {
          specialkey: this._searchSanctioningCompany
        }
      });
    },
    _newSanctioningCompany: function() {
      var modelSanctioningCompany = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterSanctioningCompany",
        {
          idSanctioningCompany: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterSanctioningCompany"
      )[0];
      var panel = general.down("ViewGridPanelRegisterSanctioningCompany");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelSanctioningCompany);
      editor.startEdit(0, 0);
    },
    _onDeleteSanctioningCompany: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSanctioningCompany grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteSanctioningCompany.htm?nameView=ViewPanelRegisterSanctioningCompany"
      );
    },
    _searchSanctioningCompany: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterSanctioningCompany grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findSanctioningCompany.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridSanctioningCompany: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSanctioningCompany grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportSanctioningCompanyPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportSanctioningCompanyExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onSanctioningCompanyAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterSanctioningCompany grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditSanctioningCompany.htm"
      );
    }
  }
);
