Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterTypeChange",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterTypeChange"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterTypeChange"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterTypeChange",
      "risk.parameter.others.ViewPanelRegisterTypeChange"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridTypeChange]": {
          keyup: this._onSearchTriggerGridTypeChange
        },
        "[action=exportTypeChangePdf]": {
          click: this._onExportTypeChangePdf
        },
        "[action=exportTypeChangeExcel]": {
          click: this._onExportTypeChangeExcel
        },
        "[action=typeChangeAuditory]": {
          click: this._onTypeChangeAuditory
        },
        "[action=newTypeChange]": {
          click: this._newTypeChange
        },
        "[action=deleteTypeChange]": {
          click: this._onDeleteTypeChange
        },
        "[action=searchTypeChange]": {
          specialkey: this._searchTypeChange
        }
      });
    },
    _newTypeChange: function() {
      var modelTypeChange = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterTypeChange",
        {
          idTypeChange: "id",
          currency: "",
          dateProcess: "",
          valueSell: "",
          valueBuy: "",
          valueFixed: ""
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterTypeChange")[0];
      var panel = general.down("ViewGridPanelRegisterTypeChange");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelTypeChange);
      editor.startEdit(0, 0);
    },
    _onDeleteTypeChange: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeChange grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteTypeChange.htm?nameView=ViewPanelRegisterTypeChange"
      );
    },
    _searchTypeChange: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterTypeChange grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findTypeChange.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridTypeChange: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeChange grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportTypeChangePdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportTypeChangeExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onTypeChangeAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterTypeChange grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditTypeChange.htm"
      );
    }
  }
);
