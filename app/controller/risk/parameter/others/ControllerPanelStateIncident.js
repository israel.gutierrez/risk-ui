Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelStateIncident",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.grids.GridPanelRegisterStateIncident",
      "risk.parameter.others.PanelRegisterStateIncident"
    ],
    init: function() {
      this.control({
        "[action=stateIncidentAuditory]": {
          click: this._onStateIncidentAuditory
        },
        "[action=newStateIncident]": {
          click: this._newStateIncident
        },
        "[action=deleteStateIncident]": {
          click: this._onDeleteStateIncident
        },
        "[action=searchStateIncident]": {
          specialkey: this._searchStateIncident
        }
      });
    },
    _newStateIncident: function() {
      var modelStateIncident = Ext.create("ModelStateIncident", {
        id: "id",
        description: "",
        abbreviation: "",
        typeIncident: "",
        colorState: "",
        sequence: "",
        state: "S"
      });
      var general = Ext.ComponentQuery.query("PanelRegisterStateIncident")[0];
      var panel = general.down("GridPanelRegisterStateIncident");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelStateIncident);
      editor.startEdit(0, 0);
    },
    _onDeleteStateIncident: function() {
      var grid = Ext.ComponentQuery.query("PanelRegisterStateIncident grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteStateIncident.htm?nameView=PanelRegisterStateIncident"
      );
    },
    _searchStateIncident: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "PanelRegisterStateIncident grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findStateIncident.htm",
          "si.description",
          "si.description"
        );
      } else {
      }
    },
    _onStateIncidentAuditory: function() {
      var grid = Ext.ComponentQuery.query("PanelRegisterStateIncident grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditStateIncident.htm"
      );
    }
  }
);
