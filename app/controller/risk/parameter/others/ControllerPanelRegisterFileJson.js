Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterFileJson",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.parameter.grids.StoreGridPanelRegisterFileJson",
      "fulfillment.grids.StoreGridPanelRegisterPropertyEmail"
    ],
    models: [
      "risk.parameter.grids.ModelGridPanelRegisterFileJson",
      "fulfillment.grids.ModelGridPanelRegisterPropertyEmail"
    ],
    views: [
      "risk.parameter.others.ViewTreePanelRegisterFileJson",
      "risk.util.ViewComboYesNo",
      "fulfillment.parameter.grids.ViewGridPanelRegisterPropertyEmail"
    ],
    init: function() {
      this.control({
        "[action=newRegisterFileJson]": {
          click: this._onNewRegisterFileJson
        },
        "[action=newSynchronizeJson]": {
          click: this._onNewSynchronizeJson
        },
        ViewTreePanelRegisterFileJson: {
          itemcontextmenu: this.treeRightClickFileJson
        },
        "AddMenuDetailJson menuitem[text=Agregar]": {
          click: this._addDetailJson
        },
        "AddMenuDetailJson menuitem[text=Editar]": {
          click: this._editFileJson
        },
        "AddMenuDetailJson menuitem[text=Eliminar]": {
          click: this._deleteFileJson
        },
        "[action=saveFileJson]": {
          click: this._onSaveFileJson
        },
        "EditMenuDetailJson menuitem[text=Editar]": {
          click: this._editDetailJson
        },
        "EditMenuDetailJson menuitem[text=Eliminar]": {
          click: this._deleteFileJson
        },
        "[action=testGridEmail]": {
          click: this._testGridEmail
        },
        "[action=modifyEmail]": {
          click: this._modifyEmail
        },
        "[action=newEmail]": {
          click: this._newEmail
        },
        "[action=saveEmail]": {
          click: this._saveEmail
        },
        "[action=deleteEmail]": {
          click: this._deleteEmail
        },
        "[action=testActiveDirectory]": {
          click: this._testActiveDirectory
        }
      });
    },
    _onNewRegisterFileJson: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowFileJson",
        {
          modal: true
        }
      );
      countryWindow
        .down("form")
        .getComponent("id")
        .setValue("myTree/id");
      countryWindow.show();
    },

    _onNewSynchronizeJson: function() {
      DukeSource.lib.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/testAllActiveDirectory.htm",
        params: {},
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              response.mensaje,
              Ext.Msg.INFO
            );
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },

    treeRightClickFileJson: function(view, record, item, index, e) {
      e.stopEvent();
      this.application.currentRecord = record;
      if (record.get("depth") === 1) {
        var addMenu = Ext.create(
          "DukeSource.view.risk.parameter.others.AddMenuDetailJson",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      if (record.get("depth") === 2) {
        addMenu = Ext.create(
          "DukeSource.view.risk.parameter.others.EditMenuDetailJson",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      return false;
    },

    _addDetailJson: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowDetailJson",
        {
          modal: true
        }
      );
      countryWindow
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id") + "/id");
      countryWindow.show();
    },

    _editFileJson: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowFileJson",
        {
          modal: true
        }
      );
      win.down("#id").setValue(this.application.currentRecord.get("id"));
      win
        .down("#description")
        .setValue(this.application.currentRecord.get("text"));
      win
        .down("#entityModel")
        .setValue(this.application.currentRecord.get("fieldOne"));
      win
        .down("#nameFile")
        .setValue(this.application.currentRecord.get("fieldTwo"));
      win.show();
      win.down("#description").focus(false, 200);
    },

    _onSaveFileJson: function(button) {
      var win = button.up("window");
      var form = win.down("form");
      if (form.getForm().isValid()) {
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/saveFileJson.htm",
          params: {
            id: form.getComponent("id").getValue(),
            description: form.getComponent("description").getValue(),
            entityModel: form.getComponent("entityModel").getValue(),
            nameFile: form.getComponent("nameFile").getValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              var refreshNode = Ext.ComponentQuery.query(
                "ViewPanelRegisterFileJson ViewTreePanelRegisterFileJson"
              )[0]
                .getStore()
                .getNodeById(response.data);
              refreshNode.removeAll(false);
              Ext.ComponentQuery.query(
                "ViewPanelRegisterFileJson ViewTreePanelRegisterFileJson"
              )[0]
                .getStore()
                .load({
                  node: refreshNode
                });
              win.close();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },

    _deleteFileJson: function(item, e) {
      var id = this.application.currentRecord.get("id");
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
        msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
        icon: Ext.Msg.QUESTION,
        buttonText: {
          yes: "Si"
        },
        buttons: Ext.MessageBox.YESNO,
        fn: function(btn) {
          if (btn == "yes") {
            Ext.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/deleteFileJson.htm",
              params: {
                id: id
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  var refreshNode = Ext.ComponentQuery.query(
                    "ViewPanelRegisterFileJson ViewTreePanelRegisterFileJson"
                  )[0]
                    .getStore()
                    .getNodeById(response.data);
                  refreshNode.removeAll(false);
                  Ext.ComponentQuery.query(
                    "ViewPanelRegisterFileJson ViewTreePanelRegisterFileJson"
                  )[0]
                    .getStore()
                    .load({
                      node: refreshNode
                    });
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_WARNING,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
          }
        }
      });
    },

    _editDetailJson: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowDetailJson",
        {
          modal: true
        }
      );
      win.down("#id").setValue(this.application.currentRecord.get("id"));
      win
        .down("#description")
        .setValue(this.application.currentRecord.get("text"));
      win
        .down("#entityModel")
        .setValue(this.application.currentRecord.get("fieldOne"));
      win
        .down("#nameFile")
        .setValue(this.application.currentRecord.get("fieldTwo"));
      win.show();
      win.down("#description").focus(false, 200);
    },

    _testGridEmail: function(button) {
      var grid = Ext.ComponentQuery.query(
        "ViewGridPanelRegisterPropertyEmail"
      )[0];
      var row = grid.getSelectionModel().getSelection()[0];
      DukeSource.lib.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/testGridEmail.htm",
        params: {
          jsonData: Ext.JSON.encode(row.raw)
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              response.message,
              Ext.Msg.INFO
            );
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              response.message,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },

    _newEmail: function() {
      var view = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowConfigurationTestMail",
        {
          modal: true
        }
      );
      view.show();
      view.down("#host").focus(false, 200);
    },

    _modifyEmail: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewGridPanelRegisterPropertyEmail"
      )[0];
      var row = grid.getSelectionModel().getSelection()[0];
      var view = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowConfigurationTestMail",
        {
          modal: true
        }
      );
      view.show();
      view
        .down("form")
        .getForm()
        .loadRecord(row);
      view.down("#host").focus(false, 200);
    },

    _saveEmail: function(button) {
      var windows = button.up("window");
      var form = windows.down("form");
      if (form.getForm().isValid()) {
        DukeSource.lib.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/savePropertyEmail.htm?nameView=ViewPanelRegisterFileJson",
          params: {
            jsonData: Ext.JSON.encode(form.getForm().getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              windows.close();
              var grid = Ext.ComponentQuery.query(
                "ViewGridPanelRegisterPropertyEmail"
              )[0];
              grid.down("pagingtoolbar").moveFirst();

              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                response.message,
                Ext.Msg.INFO
              );
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.message,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },

    _deleteEmail: function(item, e) {
      var grid = Ext.ComponentQuery.query(
        "ViewGridPanelRegisterPropertyEmail"
      )[0];
      var record = grid.getSelectionModel().getSelection()[0];

      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
        msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
        icon: Ext.Msg.WARNING,
        buttonText: {
          yes: "Si"
        },
        buttons: Ext.MessageBox.YESNO,
        fn: function(btn) {
          if (btn === "yes") {
            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/deletePropertyEmail.htm?nameView=ViewPanelRegisterFileJson",
              params: {
                jsonData: Ext.JSON.encode(record.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  grid.down("pagingtoolbar").moveFirst();
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.message,
                    Ext.Msg.INFO
                  );
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_WARNING,
                    response.mensaje,
                    Ext.Msg.WARNING
                  );
                }
              },
              failure: function() {}
            });
          }
        }
      });
    },
    _testActiveDirectory: function(button) {
      var windows = button.up("window");
      var form = windows.down("form");
      if (form.getForm().isValid()) {
        DukeSource.lib.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/testActiveDirectory.htm",
          params: {
            domain: form.down("#domain").getValue(),
            url: form.down("#url").getValue(),
            username: form.down("#emailAccount").getValue(),
            password: form.down("#password").getValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                response.mensaje,
                Ext.Msg.INFO
              );
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    }
  }
);
