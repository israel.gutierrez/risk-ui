Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterInsuranceCompany",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterInsuranceCompany"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterInsuranceCompany"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterInsuranceCompany",
      "risk.parameter.others.ViewPanelRegisterInsuranceCompany"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridInsuranceCompany]": {
          keyup: this._onSearchTriggerGridInsuranceCompany
        },
        "[action=exportInsuranceCompanyPdf]": {
          click: this._onExportInsuranceCompanyPdf
        },
        "[action=exportInsuranceCompanyExcel]": {
          click: this._onExportInsuranceCompanyExcel
        },
        "[action=insuranceCompanyAuditory]": {
          click: this._onInsuranceCompanyAuditory
        },
        "[action=newInsuranceCompany]": {
          click: this._newInsuranceCompany
        },
        "[action=deleteInsuranceCompany]": {
          click: this._onDeleteInsuranceCompany
        },
        "[action=searchInsuranceCompany]": {
          specialkey: this._searchInsuranceCompany
        }
      });
    },
    _newInsuranceCompany: function() {
      var modelInsuranceCompany = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterInsuranceCompany",
        {
          idInsuranceCompany: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterInsuranceCompany"
      )[0];
      var panel = general.down("ViewGridPanelRegisterInsuranceCompany");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelInsuranceCompany);
      editor.startEdit(0, 0);
    },
    _onDeleteInsuranceCompany: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterInsuranceCompany grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteInsuranceCompany.htm?nameView=ViewPanelRegisterInsuranceCompany"
      );
    },
    _searchInsuranceCompany: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterInsuranceCompany grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findInsuranceCompany.htm?nameView=ViewPanelRegisterInsuranceCompany",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridInsuranceCompany: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterInsuranceCompany grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportInsuranceCompanyPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportInsuranceCompanyExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onInsuranceCompanyAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterInsuranceCompany grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditInsuranceCompany.htm?nameView=ViewPanelRegisterInsuranceCompany"
      );
    }
  }
);
