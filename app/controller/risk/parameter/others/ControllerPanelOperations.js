Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelOperations",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.grids.TreeGridOperations",
      "risk.parameter.others.PanelRegisterOperations"
    ],
    init: function() {
      this.control({
        "[action=modifyOperation]": {
          click: this._onModifyOperation
        },
        "[action=operationAuditory]": {
          click: this._onOperationAuditory
        },
        "[action=newOperation]": {
          click: this._newOperation
        },
        "[action=deleteOperation]": {
          click: this._onDeleteOperation
        },
        "[action=saveOperation]": {
          click: this._saveOperation
        },
        "[action=searchOperation]": {
          specialkey: this._searchOperation
        },
        TreeGridOperations: {
          itemcontextmenu: this._rightClickOperation
        }
      });
    },
    _newOperation: function() {
      var tree = Ext.ComponentQuery.query("TreeGridOperations")[0];
      var node = tree.getSelectionModel().getSelection()[0];

      if (node === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        var window = Ext.create(
          "DukeSource.view.risk.parameter.windows.WindowRegisterOperations",
          {
            modal: true,
            prefix: node.getPath("description", " &#8702; ").substring(18)
          }
        );

        window.down("#parent").setValue(node.data["id"]);
        window.down("#depth").setValue(node.data["depth"] + 1);
        window.show();
        window.down("#description").focus(false, 100);
      }
    },
    _onDeleteOperation: function() {
      var tree = Ext.ComponentQuery.query("TreeGridOperations")[0];
      var node = tree.getSelectionModel().getSelection()[0];
      if (node === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        Ext.MessageBox.show({
          title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
          msg: "Esta seguro que desea eliminar el item?",
          icon: Ext.Msg.WARNING,
          buttonText: {
            yes: "Si"
          },
          buttons: Ext.MessageBox.YESNO,
          fn: function(btn) {
            if (btn === "yes") {
              DukeSource.lib.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/deleteOperation.htm?nameView=PanelRegisterOperations",
                params: {
                  node: Ext.JSON.encode(node.data)
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    tree.store.getProxy().extraParams = {
                      depth: node.data["depth"]
                    };
                    tree.store.load({
                      node: node.parentNode
                    });
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_CONFIRM,
                      response.mensaje,
                      Ext.Msg.INFO
                    );
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      response.mensaje,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function() {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_WARNING,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              });
            }
          }
        });
      }
    },

    _saveOperation: function(btn) {
      var tree = Ext.ComponentQuery.query("TreeGridOperations")[0];
      var node = tree.getSelectionModel().getSelection()[0];
      var windows = btn.up("window");
      var form = windows.down("form");
      form
        .down("#path")
        .setValue(
          windows.prefix + " &#8702; " + form.down("#description").getValue()
        );

      if (form.getForm().isValid()) {
        DukeSource.lib.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveOperation.htm?nameView=PanelRegisterOperations",
          params: {
            jsonData: Ext.JSON.encode(form.getForm().getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);

            if (response.success) {
              tree.store.getProxy().extraParams = {
                depth: node.data["depth"]
              };
              tree.store.load({
                node: node.parentNode
              });
              windows.close();
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_CONFIRM,
                response.message,
                Ext.Msg.INFO
              );
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.message,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              response.message,
              Ext.Msg.ERROR
            );
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onModifyOperation: function(text) {
      var tree = Ext.ComponentQuery.query("TreeGridOperations")[0];
      var node = tree.getSelectionModel().getSelection()[0];

      if (node === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        var view = Ext.create(
          "DukeSource.view.risk.parameter.windows.WindowRegisterOperations",
          {
            modal: true,
            prefix: node.parentNode
              .getPath("description", " &#8702; ")
              .substring(18)
          }
        );
        view
          .down("form")
          .getForm()
          .setValues(node.raw);
        view.down("#depth").setValue(node.data["depth"]);
        view.show();
        view.down("#description").focus(false, 100);
      }
    },
    _onOperationAuditory: function() {
      var grid = Ext.ComponentQuery.query("PanelRegisterOperations grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditOperation.htm"
      );
    },

    _rightClickOperation: function(view, rec, node, index, e) {
      e.stopEvent();
      var addMenu = Ext.create("DukeSource.view.fulfillment.AddMenu", {});
      addMenu.removeAll();
      addMenu.add(
        {
          text: "Nuevo",
          iconCls: "add",
          action: "newOperation"
        },
        {
          text: "Modificar",
          disabled: rec.data.depth === 1,
          action: "modifyOperation",
          iconCls: "modify"
        },
        {
          text: "Eliminar",
          disabled: rec.data.depth === 1,
          action: "deleteOperation",
          iconCls: "delete"
        }
      );
      addMenu.showAt(e.getXY());
    },

    _searchOperation: function(field, e) {
      var me = field.up("panel").down("treepanel");

      if (e.getKey() === e.ENTER) {
        field.getValue() === ""
          ? me.collapseAll()
          : me.filterByText(field.getValue());
      }
    }
  }
);
