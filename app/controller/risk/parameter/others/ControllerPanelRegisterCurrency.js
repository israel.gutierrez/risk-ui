Ext.define(
  "DukeSource.controller.risk.parameter.others.ControllerPanelRegisterCurrency",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterCurrency"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterCurrency"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterCurrency",
      "risk.parameter.others.ViewPanelRegisterCurrency"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridCurrency]": {
          keyup: this._onSearchTriggerGridCurrency
        },
        "[action=exportCurrencyPdf]": {
          click: this._onExportCurrencyPdf
        },
        "[action=exportCurrencyExcel]": {
          click: this._onExportCurrencyExcel
        },
        "[action=currencyAuditory]": {
          click: this._onCurrencyAuditory
        },
        "[action=newCurrency]": {
          click: this._newCurrency
        },
        "[action=deleteCurrency]": {
          click: this._onDeleteCurrency
        },
        "[action=searchCurrency]": {
          specialkey: this._searchCurrency
        }
      });
    },
    _newCurrency: function() {
      var modelCurrency = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterCurrency",
        {
          idCurrency: "id",
          description: "",
          symbol: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query("ViewPanelRegisterCurrency")[0];
      var panel = general.down("ViewGridPanelRegisterCurrency");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelCurrency);
      editor.startEdit(0, 0);
    },
    _onDeleteCurrency: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterCurrency grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteCurrency.htm?nameView=ViewPanelRegisterCurrency"
      );
    },
    _searchCurrency: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterCurrency grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findCurrency.htm?nameView=ViewPanelRegisterCurrency",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridCurrency: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterCurrency grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportCurrencyPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportCurrencyExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onCurrencyAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterCurrency grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditCurrency.htm?nameView=ViewPanelRegisterCurrency"
      );
    }
  }
);
