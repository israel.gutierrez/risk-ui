Ext.define(
  "DukeSource.controller.risk.parameter.task.ControllerPanelManageTask",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: ["risk.parameter.task.ViewPanelManageTask"],
    init: function() {
      this.control({
        "[action=addManageTask]": {
          click: this._addManageTask
        },
        "[action=deleteManageTask]": {
          click: this._deleteManageTask
        },
        "[action=modifyManageTask]": {
          click: this._modifyManageTask
        },
        "[action=addUserToManageTask]": {
          click: this._addUserToManageTask
        },
        "[action=administrationManageTask]": {
          click: this._administrationManageTask
        },
        "[action=tracingManageTask]": {
          click: this._tracingManageTask
        },
        "[action=resultManageTask]": {
          click: this._resultManageTask
        },
        "[action=scheduleManageTask]": {
          click: this._scheduleManageTask
        }
      });
    },
    _addManageTask: function() {
      Ext.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/initScheduler.htm",
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              response.mensaje,
              Ext.Msg.INFO
            );
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },

    _deleteManageTask: function() {
      Ext.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/standByTask.htm",
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              response.message,
              Ext.Msg.INFO
            );
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              response.message,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },
    _modifyManageTask: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelManageTask grid")[0];
      if (grid.getSelectionModel().getCount() == 0) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
      }
    },
    _addUserToManageTask: function() {
      Ext.create(
        "DukeSource.view.risk.parameter.task.window.ViewWindowAddUserToManagerTask",
        {
          modal: true
        }
      ).show();
    },
    _administrationManageTask: function() {},
    _tracingManageTask: function() {},
    _resultManageTask: function() {},
    _scheduleManageTask: function() {}
  }
);
