Ext.define(
  "DukeSource.controller.risk.parameter.areabusiness.ControllerPanelRegisterBusinessLineTwo",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterBusinessLineTwo"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterBusinessLineTwo"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterBusinessLineTwo",
      "risk.parameter.areabusiness.ViewPanelRegisterBusinessLineTwo"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridBusinessLineTwo]": {
          keyup: this._onSearchTriggerGridBusinessLineTwo
        },
        "[action=exportBusinessLineTwoPdf]": {
          click: this._onExportBusinessLineTwoPdf
        },
        "[action=exportBusinessLineTwoExcel]": {
          click: this._onExportBusinessLineTwoExcel
        },
        "[action=businessLineTwoAuditory]": {
          click: this._onBusinessLineTwoAuditory
        },
        "[action=newBusinessLineTwo]": {
          click: this._newBusinessLineTwo
        },
        "[action=deleteBusinessLineTwo]": {
          click: this._onDeleteBusinessLineTwo
        },
        "[action=searchBusinessLineTwo]": {
          specialkey: this._searchBusinessLineTwo
        }
      });
    },
    _newBusinessLineTwo: function() {
      var modelBusinessLineTwo = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterBusinessLineTwo",
        {
          businessLineOne: "id",
          idBusinessLineTwo: "",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterBusinessLineTwo"
      )[0];
      var panel = general.down("ViewGridPanelRegisterBusinessLineTwo");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelBusinessLineTwo);
      editor.startEdit(0, 0);
    },
    _onDeleteBusinessLineTwo: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterBusinessLineTwo grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteBusinessLineTwo.htm"
      );
    },
    _searchBusinessLineTwo: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterBusinessLineTwo grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findBusinessLineTwo.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridBusinessLineTwo: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterBusinessLineTwo grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportBusinessLineTwoPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportBusinessLineTwoExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onBusinessLineTwoAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterBusinessLineTwo grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditBusinessLineTwo.htm"
      );
    }
  }
);
