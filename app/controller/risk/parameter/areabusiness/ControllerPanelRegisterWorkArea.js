Ext.define(
  "DukeSource.controller.risk.parameter.areabusiness.ControllerPanelRegisterWorkArea",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.parameter.grids.StoreTreeGridPanelRegisterWorkArea",
      "risk.parameter.grids.StoreGridAuthorizedWorkArea"
    ],
    models: [
      "risk.parameter.grids.ModelTreeGridPanelRegisterWorkArea",
      "risk.parameter.grids.ModelGridAuthorizedWorkArea"
    ],
    views: [
      "risk.parameter.grids.ViewTreeGridPanelRegisterWorkArea",
      "risk.User.combos.ViewComboAgency",
      "risk.parameter.grids.ViewGridAuthorizedWorkArea",
      "risk.parameter.combos.ViewComboCategory"
    ],
    init: function() {
      this.control({
        "[action=newWorkArea]": {
          click: this._onNewWorkArea
        },
        "[action=saveWorkArea]": {
          click: this._onSaveWorkArea
        },
        "[action=modifyWorkArea]": {
          click: this._modifyWorkArea
        },
        "[action=deleteWorkArea]": {
          click: this._deleteWorkArea
        },
        "[action=authorizedWorkArea]": {
          click: this._authorizedWorkArea
        },
        "[action=newAuthorizedWorkArea]": {
          click: this._newAuthorizedWorkArea
        },
        "[action=saveUserAuthorized]": {
          click: this._saveUserAuthorized
        },
        "[action=modifyAuthorizedWorkArea]": {
          click: this._modifyAuthorizedWorkArea
        },
        "[action=deleteAuthorizedWorkArea]": {
          click: this._deleteAuthorizedWorkArea
        },
        "[action=newJobPlace]": {
          click: this._newJobPlace
        },
        "[action=saveJobPlace]": {
          click: this._onSaveJobPlace
        },
        "[action=modifyJobPlace]": {
          click: this._modifyJobPlace
        },
        "[action=deleteJobPlace]": {
          click: this._deleteJobPlace
        },
        ViewTreeGridPanelRegisterWorkArea: {
          itemcontextmenu: this._rightClickWorkArea
        },
        ViewGridPanelRegisterJobPlace: {
          itemcontextmenu: this._rightClickJobPlace
        }
      });
    },
    _onNewWorkArea: function() {
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterWorkArea"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];
      if (node === undefined) {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
      } else {
        var view = Ext.create(
          "DukeSource.view.risk.parameter.windows.ViewWindowWorkArea",
          {
            modal: true,
            origin: "new",
            prefix: node.getPath("description", " &#8702; ").substring(18),
            title: "AGREGAR EN: " + node.data["description"]
          }
        );
        view.down("#descriptionParent").setValue(node.data["description"]);
        view.down("#parent").setValue(node.data["idWorkArea"]);
        view.down("#levelArea").setValue(node.data["depth"] + 1);

        view.show();
        view.down("#description").focus(false, 100);
      }
    },
    _onSaveWorkArea: function(button) {
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterWorkArea"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];
      var windows = button.up("window");
      var form = windows.down("form");
      form
        .down("#path")
        .setValue(
          windows.prefix + " &#8702; " + form.down("#description").getValue()
        );

      if (form.getForm().isValid()) {
        DukeSource.lib.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/saveWorkArea.htm",
          params: {
            jsonData: Ext.JSON.encode(form.getForm().getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            var depth = node.data["depth"];
            if (depth === 1) {
              depth = 2;
            }
            if (response.success) {
              tree.store.getProxy().extraParams = {
                depth: depth
              };
              tree.store.load({
                node: windows.origin == "new" ? node : node.parentNode
              });
              windows.close();
              DukeSource.global.DirtyView.messageNormal(response.message);
            } else {
              DukeSource.global.DirtyView.messageWarning(response.message);
            }
          },
          failure: function() {
            DukeSource.global.DirtyView.messageWarning(response.message);
          }
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
      }
    },
    _modifyWorkArea: function() {
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterWorkArea"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];

      if (node === undefined) {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
      }
      if (node.data["depth"] === 1) {
        DukeSource.global.DirtyView.messageWarning(
          "No puede ser modificado, es un registro base"
        );
      } else {
        var view = Ext.create(
          "DukeSource.view.risk.parameter.windows.ViewWindowWorkArea",
          {
            modal: true,
            title: "MODIFICAR: " + node.data["description"],
            prefix: node.parentNode
              .getPath("description", " &#8702; ")
              .substring(18),
            origin: "modify"
          }
        );
        view
          .down("form")
          .getForm()
          .setValues(node.raw);
        view
          .down("#descriptionParent")
          .setValue(node.parentNode.data["description"]);
        view.down("#levelArea").setValue(node.data["depth"]);
        view.show();
        view.down("#description").focus(false, 100);
      }
    },
    _deleteWorkArea: function() {
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterWorkArea"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];

      if (node === undefined) {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
      }
      if (node.data["depth"] === 1) {
        DukeSource.global.DirtyView.messageWarning("No puede ser eliminado, es un registro base");
      } else {
        Ext.MessageBox.show({
          title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
          msg: "Esta seguro que desea eliminar el item?",
          icon: Ext.Msg.QUESTION,
          buttonText: {
            yes: "Si"
          },
          buttons: Ext.MessageBox.YESNO,
          fn: function(btn) {
            if (btn === "yes") {
              DukeSource.lib.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/deleteWorkArea.htm?nameView=ViewPanelRegisterWorkArea",
                params: {
                  node: Ext.JSON.encode(node.data),
                  child: node.childNodes.length
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    tree.store.getProxy().extraParams = {
                      depth: node.data["depth"]
                    };
                    tree.store.load({
                      node: node.parentNode
                    });
                    tree.getSelectionModel().deselectAll();
                    DukeSource.global.DirtyView.messageNormal(response.message);
                  } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                  }
                },
                failure: function(response) {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              });
            }
          }
        });
      }
    },
    _newJobPlace: function() {
      var treeWorkArea = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterWorkArea"
      )[0];
      var nodeWorkArea = treeWorkArea.getSelectionModel().getSelection()[0];

      var panel = Ext.ComponentQuery.query("ViewGridPanelRegisterJobPlace")[0];
      var node = panel.getSelectionModel().getSelection()[0];

      if (node === undefined) {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
      } else {
        var view = Ext.create(
          "DukeSource.view.risk.parameter.windows.ViewWindowJobPlace",
          {
            modal: true
          }
        );
        view.down("#workArea").store.load();
        view.down("#parent").store.load();
        view.down("#workArea").setValue(nodeWorkArea.data["idWorkArea"]);
        view.down("#parent").setValue(node.data["idJobPlace"]);
        view.show();
        view.down("#description").focus(false, 100);
      }
    },
    _onSaveJobPlace: function(button) {
      var tree = Ext.ComponentQuery.query("ViewGridPanelRegisterJobPlace")[0];
      var node = tree.getSelectionModel().getSelection()[0];
      var window = button.up("window");
      var form = window.down("form");
      var view = form.getForm();
      if (view.isValid()) {
        DukeSource.lib.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/saveJobPlace.htm",
          params: {
            jsonData: Ext.JSON.encode(form.getForm().getValues()),
            node: window.down("#parent").getValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              tree.store.getProxy().url =
                "http://localhost:9000/giro/showJobPlaceByHierarchyByWorkArea.htm";
              tree.store.getProxy().extraParams = {
                idWorkArea: node.data["workArea"],
                depth: node.data["depth"]
              };
              tree.store.load({
                node: node.parentNode
              });
              window.close();
            } else {
              DukeSource.global.DirtyView.messageWarning(response.message);
            }
          },
          failure: function() {
            DukeSource.global.DirtyView.messageWarning(response.message);
          }
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
      }
    },
    _modifyJobPlace: function() {
      var tree = Ext.ComponentQuery.query("ViewGridPanelRegisterJobPlace")[0];
      var node = tree.getSelectionModel().getSelection()[0];
      if (node === undefined) {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
      } else {
        var view = Ext.create(
          "DukeSource.view.risk.parameter.windows.ViewWindowJobPlace",
          {
            modal: true
          }
        );
        view.down("#workArea").store.load();
        view.down("#parent").store.load();
        view.down("#category").store.load();

        view
          .down("form")
          .getForm()
          .setValues(node.raw);
        view.down("#workArea").setReadOnly(false);
        view.down("#parent").setReadOnly(false);
        view.show();
        view.down("#description").focus(false, 100);
      }
    },
    _deleteJobPlace: function() {
      var tree = Ext.ComponentQuery.query("ViewGridPanelRegisterJobPlace")[0];
      var node = tree.getSelectionModel().getSelection()[0];
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
        msg: "Esta seguro que desea eliminar el item?",
        icon: Ext.Msg.WARNING,
        buttonText: {
          yes: "Si"
        },
        buttons: Ext.MessageBox.YESNO,
        fn: function(btn) {
          if (btn === "yes") {
            DukeSource.lib.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/deleteJobPlace.htm",
              params: {
                node: node.parentNode,
                jsonData: Ext.JSON.encode(node.data)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  tree.store.getProxy().url =
                    "http://localhost:9000/giro/showJobPlaceByHierarchyByWorkArea.htm";
                  tree.store.getProxy().extraParams = {
                    idWorkArea: node.data["workArea"],
                    depth: node.data["depth"]
                  };
                  tree.store.load({
                    node: node.parentNode
                  });
                  DukeSource.global.DirtyView.messageWarning(response.message);
                } else {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              },
              failure: function(response) {
                DukeSource.global.DirtyView.messageWarning(response.message);
              }
            });
          }
        }
      });
    },
    _rightClickWorkArea: function(view, rec, node, index, e) {
      e.stopEvent();
      var addMenu = Ext.create("DukeSource.view.fulfillment.AddMenu", {});
      addMenu.removeAll();
      addMenu.add(
        {
          text: "Nuevo",
          disabled: rec.data.depth === 1,
          iconCls: "add",
          action: "newWorkArea"
        },
        {
          text: "Modificar",
          disabled: rec.data.depth === 1,
          action: "modifyWorkArea",
          iconCls: "modify"
        },
        {
          text: "Aprobadores",
          disabled: rec.data.depth === 1,
          hidden: hidden("PARAM_Tree_AuthorizedWorkArea"),
          action: "authorizedWorkArea",
          iconCls: "users"
        },
        {
          text: "Eliminar",
          disabled: rec.data.depth === 1,
          action: "deleteWorkArea",
          iconCls: "delete"
        }
      );
      addMenu.showAt(e.getXY());
    },
    _rightClickJobPlace: function(view, rec, node, index, e) {
      e.stopEvent();
      var addMenu = Ext.create("DukeSource.view.fulfillment.AddMenu", {});
      addMenu.removeAll();
      addMenu.add(
        {
          text: "Nuevo",
          disabled: rec.data.depth === 1,
          iconCls: "add",
          action: "newJobPlace"
        },
        {
          text: "Modificar",
          disabled: rec.data.depth === 1,
          action: "modifyJobPlace",
          iconCls: "modify"
        },
        {
          text: "Eliminar",
          disabled: rec.data.depth === 1,
          action: "deleteJobPlace",
          iconCls: "delete"
        }
      );
      addMenu.showAt(e.getXY());
    },
    _authorizedWorkArea: function() {
      var tree = Ext.ComponentQuery.query(
        "ViewTreeGridPanelRegisterWorkArea"
      )[0];
      var node = tree.getSelectionModel().getSelection()[0];
      if (node === undefined) {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
      } else {
        var view = Ext.create(
          "DukeSource.view.risk.parameter.windows.WindowAuthorizedWorkArea",
          {
            tree: tree,
            node: node,
            modal: true
          }
        );
        var grid = view.down("grid");
        DukeSource.global.DirtyView.searchPaginationGridNormal(
          "wa.id",
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findUserAuthorized.htm",
          node.raw.idWorkArea,
          "ua.id"
        );
        view.show();
        view.setTitle(view.title + " - " + node.raw.description);
      }
    },
    _newAuthorizedWorkArea: function(btn) {
      var windowParent = btn.up("window");
      var view = Ext.create(
        "DukeSource.view.risk.parameter.windows.WindowRegisterUserAuthorized",
        {
          modal: true,
          winParent: windowParent
        }
      );
      view.show();
      view.down("#workArea").setValue(windowParent.node.raw.idWorkArea);
    },
    _saveUserAuthorized: function(btn) {
      var win = btn.up("window");
      var grid = win.winParent.down("grid");
      var form = win.down("form").getForm();

      if (form.isValid()) {
        DukeSource.lib.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveUserAuthorized.htm?nameView=ViewPanelRegisterWorkArea",
          params: {
            jsonData: Ext.JSON.encode(form.getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              win.close();
              grid.down("pagingtoolbar").doRefresh();
              DukeSource.global.DirtyView.messageNormal(response.message);
            } else {
              DukeSource.global.DirtyView.messageWarning(response.message);
            }
          },
          failure: function() {
            DukeSource.global.DirtyView.messageWarning(response.message);
          }
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
      }
    },
    _modifyAuthorizedWorkArea: function(btn) {
      var windowParent = btn.up("window");
      var grid = windowParent.down("grid");
      var row = grid.getSelectionModel().getSelection()[0];
      var view = Ext.create(
        "DukeSource.view.risk.parameter.windows.WindowRegisterUserAuthorized",
        {
          modal: true,
          winParent: windowParent
        }
      );
      view.show();
      view
        .down("form")
        .getForm()
        .loadRecord(row);
    },
    _deleteAuthorizedWorkArea: function(btn) {
      var win = btn.up("window");
      var grid = win.down("grid");
      var row = grid.getSelectionModel().getSelection()[0];

      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
        msg: "Esta seguro que desea eliminar el item?",
        icon: Ext.Msg.QUESTION,
        buttonText: {
          yes: "Si"
        },
        buttons: Ext.MessageBox.YESNO,
        fn: function(btn) {
          if (btn === "yes") {
            DukeSource.lib.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/deleteUserAuthorized.htm?nameView=ViewPanelRegisterWorkArea",
              params: {
                jsonData: Ext.JSON.encode(row.raw)
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  grid.down("pagingtoolbar").doRefresh();
                  DukeSource.global.DirtyView.messageNormal(response.message);
                } else {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              },
              failure: function() {
                DukeSource.global.DirtyView.messageWarning(response.message);
              }
            });
          }
        }
      });
    }
  }
);
