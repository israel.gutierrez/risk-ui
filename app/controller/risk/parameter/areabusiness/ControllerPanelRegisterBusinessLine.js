Ext.define(
  "DukeSource.controller.risk.parameter.areabusiness.ControllerPanelRegisterBusinessLine",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreTreeGridPanelRegisterBusinessLine"],
    models: ["risk.parameter.grids.ModelTreeGridPanelRegisterBusinessLine"],
    views: [
      "risk.parameter.grids.ViewTreeGridPanelRegisterBusinessLine",
      "risk.util.ViewComboYesNo"
    ],
    init: function() {
      this.control({
        "[action=newRegisterBusinessLine]": {
          click: this._onNewRegisterBusinessLine
        },
        ViewTreeGridPanelRegisterBusinessLine: {
          itemcontextmenu: this.treeRightClickBusinessline
        },
        "AddMenuBusinessLineTwo menuitem[text=Agregar]": {
          click: this._addBusinessLineTwo
        },
        "AddMenuBusinessLineTwo menuitem[text=Editar]": {
          click: this._editBusinessLineOne
        },
        "AddMenuBusinessLineTwo menuitem[text=Eliminar]": {
          click: this._deleteBusinessLine
        },
        "[action=saveBusinessLine]": {
          click: this._onSaveBusinessLine
        },
        "EditMenuBusinessLineTwo menuitem[text=Agregar]": {
          click: this._addBusinessLineThree
        },
        "EditMenuBusinessLineTwo menuitem[text=Editar]": {
          click: this._editBusinessLineTwo
        },
        "EditMenuBusinessLineTwo menuitem[text=Eliminar]": {
          click: this._deleteBusinessLine
        },
        "EditMenuBusinessLineThree menuitem[text=Editar]": {
          click: this._editBusinessLineThree
        },
        "EditMenuBusinessLineThree menuitem[text=Eliminar]": {
          click: this._deleteBusinessLine
        }
      });
    },
    _onNewRegisterBusinessLine: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowBusinessLineOne",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getComponent("id")
        .setValue("myTree/id");
      win.show();
      win.down("#text").focus(false, 200);
    },

    treeRightClickBusinessline: function(view, record, item, index, e) {
      e.stopEvent();

      this.application.currentRecord = record;

      if (record.get("depth") === 1) {
        var addMenu = Ext.create(
          "DukeSource.view.risk.parameter.areabusiness.AddMenuBusinessLineTwo",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      if (record.get("depth") === 2) {
        var addMenu = Ext.create(
          "DukeSource.view.risk.parameter.areabusiness.EditMenuBusinessLineTwo",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      if (record.get("depth") === 3) {
        var addMenu = Ext.create(
          "DukeSource.view.risk.parameter.areabusiness.EditMenuBusinessLineThree",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      return false;
    },

    _addBusinessLineTwo: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowBusinessLineTwo",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id") + "/id");
      win.show();
      win.down("#text").focus(false, 200);
    },
    _editBusinessLineOne: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowBusinessLineOne",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getForm()
        .setValues(this.application.currentRecord.data);
      win.show();
      win.down("#text").focus(false, 200);
    },
    _onSaveBusinessLine: function(button) {
      var window = button.up("window");
      var form = window.down("form");
      if (form.getForm().isValid()) {
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/saveBusinessLine.htm",
          params: {
            jsonData: Ext.JSON.encode(form.getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              var refreshNode = Ext.ComponentQuery.query(
                "ViewPanelRegisterBusinessLine ViewTreeGridPanelRegisterBusinessLine"
              )[0]
                .getStore()
                .getNodeById(response.data);
              refreshNode.removeAll(false);
              Ext.ComponentQuery.query(
                "ViewPanelRegisterBusinessLine ViewTreeGridPanelRegisterBusinessLine"
              )[0]
                .getStore()
                .load({
                  node: refreshNode
                });
              window.close();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },

    _deleteBusinessLine: function(item, e) {
      var id = this.application.currentRecord.get("id");
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
        msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
        icon: Ext.Msg.QUESTION,
        buttonText: {
          yes: "Si"
        },
        buttons: Ext.MessageBox.YESNO,
        fn: function(btn) {
          if (btn == "yes") {
            Ext.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/deleteBusinessLine.htm",
              params: {
                id: id
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  var refreshNode = Ext.ComponentQuery.query(
                    "ViewPanelRegisterBusinessLine ViewTreeGridPanelRegisterBusinessLine"
                  )[0]
                    .getStore()
                    .getNodeById(response.data);
                  refreshNode.removeAll(false);
                  Ext.ComponentQuery.query(
                    "ViewPanelRegisterBusinessLine ViewTreeGridPanelRegisterBusinessLine"
                  )[0]
                    .getStore()
                    .load({
                      node: refreshNode
                    });
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_WARNING,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
          }
        }
      });
    },
    _addBusinessLineThree: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowBusinessLineThree",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id") + "/id");
      win.show();
      win.down("#text").focus(false, 200);
    },
    _editBusinessLineTwo: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowBusinessLineTwo",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getForm()
        .setValues(this.application.currentRecord.data);
      win.show();
      win.down("#text").focus(false, 200);
    },
    _editBusinessLineThree: function() {
      var win = Ext.create(
        "DukeSource.view.risk.parameter.windows.ViewWindowBusinessLineThree",
        {
          modal: true
        }
      );
      win
        .down("form")
        .getForm()
        .setValues(this.application.currentRecord.data);
      win.show();
      win.down("#text").focus(false, 200);
    }
  }
);
