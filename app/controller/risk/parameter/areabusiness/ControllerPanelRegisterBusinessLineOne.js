Ext.define(
  "DukeSource.controller.risk.parameter.areabusiness.ControllerPanelRegisterBusinessLineOne",
  {
    extend: "Ext.app.Controller",
    stores: ["risk.parameter.grids.StoreGridPanelRegisterBusinessLineOne"],
    models: ["risk.parameter.grids.ModelGridPanelRegisterBusinessLineOne"],
    views: [
      "risk.parameter.grids.ViewGridPanelRegisterBusinessLineOne",
      "risk.parameter.areabusiness.ViewPanelRegisterBusinessLineOne"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridBusinessLineOne]": {
          keyup: this._onSearchTriggerGridBusinessLineOne
        },
        "[action=exportBusinessLineOnePdf]": {
          click: this._onExportBusinessLineOnePdf
        },
        "[action=exportBusinessLineOneExcel]": {
          click: this._onExportBusinessLineOneExcel
        },
        "[action=businessLineOneAuditory]": {
          click: this._onBusinessLineOneAuditory
        },
        "[action=newBusinessLineOne]": {
          click: this._newBusinessLineOne
        },
        "[action=deleteBusinessLineOne]": {
          click: this._onDeleteBusinessLineOne
        },
        "[action=searchBusinessLineOne]": {
          specialkey: this._searchBusinessLineOne
        }
      });
    },
    _newBusinessLineOne: function() {
      var modelBusinessLineOne = Ext.create(
        "DukeSource.model.risk.parameter.grids.ModelGridPanelRegisterBusinessLineOne",
        {
          idBusinessLineOne: "id",
          description: "",
          state: "S"
        }
      );
      var general = Ext.ComponentQuery.query(
        "ViewPanelRegisterBusinessLineOne"
      )[0];
      var panel = general.down("ViewGridPanelRegisterBusinessLineOne");
      var grid = panel;
      var editor = panel.editingPlugin;
      editor.cancelEdit();
      grid.getStore().insert(0, modelBusinessLineOne);
      editor.startEdit(0, 0);
    },
    _onDeleteBusinessLineOne: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterBusinessLineOne grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteBusinessLineOne.htm"
      );
    },
    _searchBusinessLineOne: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterBusinessLineOne grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findBusinessLineOne.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridBusinessLineOne: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterBusinessLineOne grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportBusinessLineOnePdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportBusinessLineOneExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onBusinessLineOneAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterBusinessLineOne grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditBusinessLineOne.htm"
      );
    }
  }
);
