Ext.define(
  "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelPreviousIdentifyRiskOperational",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [],

    init: function() {
      this.control({
        "[action=showPanelIdentifyRisk]": {
          click: this._onShowPanelIdentifyRisk
        },
        "[action=searchTriggerGridPreviousIdentifyRisk]": {
          keyup: this._onSearchTriggerGridPreviousIdentifyRisk
        },

        "ViewPanelPreviousIdentifyRiskOperational grid[itemId=previousRiskGrid]": {
          itemcontextmenu: this.rightClickPreviousRiskOperational
        },
        "[action=deletePreviousRiskOperational]": {
          click: this._onDeletePreviousRiskOperational
        },
        "[action=closePreviousPanelIdentifyRisk]": {
          click: this._onClosePreviousPanelIdentifyRisk
        },
        "[action=auditoryPreviousRiskOperational]": {
          click: this._onAuditoryPreviousRiskOperational
        },
        "ViewPanelGeneral[title=EVRO]": {
          add: this._onControlLoadTabs
        },
        "[action=riskPreviousRiskOperational]": {
          click: this._onRiskPreviousRiskOperational
        }
      });
    },
    _onShowPanelIdentifyRisk: function(button) {
      var win = button.up("window");
      var form = win.down("form");
      var grid = Ext.ComponentQuery.query(
        "ViewPanelPreviousIdentifyRiskOperational grid"
      )[0];
      if (form.getForm().isValid()) {
        form.getForm().submit({
          url:
            "http://localhost:9000/giro/saveRiskEvaluationMatrix.htm?nameView=PanelProcessRiskEvaluation",
          waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
          method: "POST",
          success: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (valor.success) {
              win.close();
              DukeSource.global.DirtyView.searchPaginationGridNormal(
                "",
                grid,
                grid.down("pagingtoolbar"),
                "http://localhost:9000/giro/showListRiskEvaluationMatrixActives.htm",
                "description",
                "idRiskEvaluationMatrix"
              );
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                valor.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function(form, action) {
            button.setDisabled(false);
            var valor = Ext.decode(action.response.responseText);
            if (!valor.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                valor.mensaje,
                Ext.Msg.ERROR
              );
            }
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onSearchTriggerGridPreviousIdentifyRisk: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelPreviousIdentifyRiskOperational grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    rightClickPreviousRiskOperational: function(view, rec, node, index, e) {
      var panel = Ext.ComponentQuery.query(
        "ViewPanelPreviousIdentifyRiskOperational"
      )[0];
      e.stopEvent();
      var addMenu = Ext.create(
        "DukeSource.view.risk.EvaluatorRiskOperational.AddMenuPreviousRisk",
        {}
      );
      addMenu.add(
        {
          xtype: "menuitem",
          text: "Ver Riesgos",
          iconCls: "risk",
          scope: this,
          handler: function(btn) {
            this._onShowPreviousRisk(view, rec);
          }
        },
        {
          xtype: "menuitem",
          text: "Modificar",
          iconCls: "modify",
          handler: function() {
            var win = Ext.create(
              "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowPreviousRisk",
              {
                modal: true
              }
            );
            win
              .down("#agency")
              .getStore()
              .load();
            win
              .down("#typeRiskEvaluation")
              .getStore()
              .load();
            win
              .down("form")
              .getForm()
              .loadRecord(rec);
            win.show();
          }
        },
        {
          xtype: "menuitem",
          text: "Gestionar Adjuntos",
          iconCls: "gestionfile",
          handler: function() {
            var countryWindow = Ext.create(
              "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowAttachFileToMatrix",
              {
                riskEvaluationMatrix: rec.get("idRiskEvaluationMatrix"),
                modal: true
              }
            );
            countryWindow.show();
          }
        },
        {
          xtype: "menuitem",
          text: "Eliminar",
          action: "deletePreviousRiskOperational",
          iconCls: "delete"
        }
        /*,
            {
               xtype: 'menuitem',
               text: 'Matriz',
               iconCls: 'matrix',
               scope: this,
               handler: function () {
                   if (Ext.ComponentQuery.query('ViewWindowMatrixByGroupRisk')[0] == undefined) {
                       var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowMatrixByGroupRisk', {
                           idRiskEvaluationMatrix: rec.get('idRiskEvaluationMatrix'),
                           height: 450,
                           width: 900
                       }).show();
                   }
               }
            }*/
      );
      addMenu.showAt(e.getXY());
    },
    _onRiskPreviousRiskOperational: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelPreviousIdentifyRiskOperational grid"
      )[0];
      var record = grid.getSelectionModel().getSelection()[0];
      if (record === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        this._onShowPreviousRisk(grid, record);
      }
    },
    _onDeletePreviousRiskOperational: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelPreviousIdentifyRiskOperational grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteRiskEvaluationMatrix.htm?nameView=PanelProcessRiskEvaluation"
      );
    },
    _onShowPreviousRisk: function(grid, record) {
      Ext.ComponentQuery.query(
        "ViewPanelPreviousIdentifyRiskOperational"
      )[0].disable();
      DukeSource.global.DirtyView.verifyLoadController(
        "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelIdentifyRiskOperational"
      );

      var k = Ext.create(
        "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelIdentifyRiskOperational",
        {
          idRiskEvaluationMatrix: record.get("idRiskEvaluationMatrix"),
          nameEvaluation: record.get("nameEvaluation"),
          closable: true,
          title: "Evaluación de riesgo &#8702; 1",
          callerPanel: "ViewPanelPreviousIdentifyRiskOperational",
          border: false
        }
      );

      DukeSource.getApplication().centerPanel.addPanel(k);

      var gridRisk = k.down("grid[name=riskIdentify]");

      gridRisk.store.getProxy().extraParams = {
        idRiskEvaluationMatrix: record.get("idRiskEvaluationMatrix")
      };
      gridRisk.store.getProxy().url =
        "http://localhost:9000/giro/getRiskByRiskEvaluationMatrix.htm";
      gridRisk.down("pagingtoolbar").moveFirst();

      k.down("grid").setTitle("Evaluación de riesgos");
    },
    _onClosePreviousPanelIdentifyRisk: function(button) {
      button
        .up("ViewPanelPreviousIdentifyRiskOperational")
        .up("panel")
        .destroy();
    },
    _onAuditoryPreviousRiskOperational: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelPreviousIdentifyRiskOperational grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditRiskEvaluationMatrix.htm"
      );
    },

    _onViewMatrixByEvaluation: function(view, rec) {
      var mari = rec.get("idRiskEvaluationMatrix");
      if (
        Ext.ComponentQuery.query("ViewWindowMatrixByGroupRisk")[0] === undefined
      ) {
        var window = Ext.create(
          "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowMatrixByGroupRisk",
          {
            idRiskEvaluationMatrix: rec.get("idRiskEvaluationMatrix"),
            height: 450,
            width: 900
          }
        ).show();
        var gridMatrixRisk = window.down("grid");
        var maxExposition = "";
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/getMaxExpositionValidity.htm",
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              maxExposition = JSON.parse(response.data);
              Ext.Ajax.request({
                method: "POST",
                url: "http://localhost:9000/giro/buildHeaderAxisX.htm",
                params: {
                  valueFind: maxExposition,
                  propertyOrder: "equivalentValue"
                },
                success: function(responseIn) {
                  responseIn = Ext.decode(responseIn.responseText);
                  if (responseIn.success) {
                    var columns = JSON.parse(responseIn.data);
                    var fields = JSON.parse(responseIn.fields);
                    DukeSource.global.DirtyView.createMatrix(columns, fields, gridMatrixRisk);
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_ERROR,
                      responseIn.mensaje,
                      Ext.Msg.ERROR
                    );
                    gridMatrixRisk.getView().refresh();
                    gridMatrixRisk.getView().refresh();
                  }
                },
                failure: function() {}
              });
              gridMatrixRisk.store.getProxy().extraParams = {
                maxExposition: maxExposition,
                businessLine: "999"
              };
              gridMatrixRisk.store.getProxy().url =
                "http://localhost:9000/giro/findMatrixAndLoad.htm";
              gridMatrixRisk.down("pagingtoolbar").moveFirst();
              Ext.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/buildMatrixGeneralByRiskEvaluation.htm",
                params: {
                  maxExposition: maxExposition,
                  businessLine: "999",
                  riskEvaluation: mari
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    var data1 = JSON.parse(response.data1);
                    var data2 = JSON.parse(response.data2);
                    for (var i = 0; i < data1.length; i++) {
                      var valueMatrix = data1[i]["valueMatrix"];
                      var codeRisk = data1[i]["codeRisk"];
                      var positionX = data1[i]["positionX"];
                      var positionY = data1[i]["positionY"] + 1;
                      var cell = gridMatrixRisk.getView().getCellByPosition({
                        row: positionX,
                        column: positionY
                      });
                      var value = cell.dom.textContent;
                      if (
                        (value = Ext.util.Format.number(valueMatrix, "0,0.00"))
                      ) {
                        cell.insertHtml(
                          "afterBegin",
                          '<span style= "display: inline-block;margin-right: 0.02px; border-radius: 50%;color: #000000; font-size: 12px; border: 1px solid #080808; width: 35px; height: 35px; background: rgb(234,234,234); text-align: center; padding: 6px 0.5px">' +
                            codeRisk +
                            "</span>",
                          true
                        );
                      }
                    }
                    for (var i = 0; i < data2.length; i++) {
                      var valueMatrix = data2[i]["valueMatrix"];
                      var codeRisk = data2[i]["codeRisk"];
                      var positionX = data2[i]["positionX"];
                      var positionY = data2[i]["positionY"] + 1;
                      var cell = gridMatrixRisk.getView().getCellByPosition({
                        row: positionX,
                        column: positionY
                      });
                      var value = cell.dom.textContent;
                      if ((value = valueMatrix.toString().concat(".00"))) {
                        cell.insertHtml(
                          "afterBegin",
                          '<span style= "display: inline-block;margin-right: 0.02px; border-radius: 50%;color: #ffffff; font-size: 12px; border: 1.5px solid #ffffff; width: 35px; height: 35px; background: rgba(0,0,0,0.79); text-align: center; padding: 6px 0.5px">' +
                            codeRisk +
                            "</span>",
                          true
                        );
                      }
                    }
                  }
                }
              });
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
      }
    },

    _onControlLoadTabs: function(tabPanel, tab) {
      var panelPrevious = tabPanel.down(
        "ViewPanelPreviousIdentifyRiskOperational"
      );
      tabPanel.items.each(function(panel) {
        panel.tab.clearListeners();
        panel.tab.on(
          "activate",
          function() {
            if (panel.itemId == "viewPanelIdentifyRiskOperational") {
              if (
                "NOMBRE EVALUACI&Oacute;N : " +
                  panelPrevious
                    .down("grid")
                    .getSelectionModel()
                    .getSelection()[0]
                    .get("nameEvaluation") !=
                panel.down("grid").title
              ) {
                var gridRisk = panel.down("grid[name=riskIdentify]");
                gridRisk.store.getProxy().extraParams = {
                  idRiskEvaluationMatrix: panelPrevious
                    .down("grid")
                    .getSelectionModel()
                    .getSelection()[0]
                    .get("idRiskEvaluationMatrix")
                };
                gridRisk.store.getProxy().url =
                  "http://localhost:9000/giro/getRiskByRiskEvaluationMatrix.htm";
                gridRisk.down("pagingtoolbar").moveFirst();
                gridRisk.setTitle(
                  "NOMBRE EVALUACI&Oacute;N : " +
                    panelPrevious
                      .down("grid")
                      .getSelectionModel()
                      .getSelection()[0]
                      .get("nameEvaluation")
                );
                panel.down("#buttonEvaluation").setVisible(false);
                panel.down("#buttonIr").setVisible(false);
                panel.down("ViewComboGridEvaluationRisk").setVisible(false);
              }
            }
          },
          this
        );
      }, this);
    }
  }
);
