Ext.define(
  "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelReportConsolidateLN",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboBusinessLineOne",
      "risk.parameter.combos.ViewComboTypeRiskEvaluation",
      "risk.parameter.combos.ViewComboBusinessLineTwo"
    ],
    init: function() {
      this.control({
        "[action=generateReportConsolidateLN]": {
          click: this._onGenerateReportConsolidateLN
        },
        "[action=generateReportRiskConsolidateLNXls]": {
          click: this._onGenerateReportRiskConsolidateLNXls
        }
      });
    },

    _onGenerateReportConsolidateLN: function(btn) {
      if (
        btn
          .up("form")
          .getForm()
          .isValid()
      ) {
        var container = btn.up("container[name=generalContainer]");
        var businessLineOne =
          container.down("ViewComboBusinessLineOne").getValue() == undefined
            ? "T"
            : container.down("ViewComboBusinessLineOne").getValue();
        var businessLineTwo =
          container.down("ViewComboBusinessLineTwo").getValue() == undefined
            ? "T"
            : container.down("ViewComboBusinessLineTwo").getValue();
        var typeEvaluation = container
          .down("ViewComboTypeRiskEvaluation")
          .getValue();
        var value =
          container.down("datefield[name=dateInit]").getRawValue() +
          ";" +
          container.down("datefield[name=dateEnd]").getRawValue() +
          ";" +
          businessLineOne +
          ";" +
          businessLineTwo +
          ";" +
          typeEvaluation;
        var panelReport = container.down("panel[name=panelReport]");
        panelReport.removeAll();
        panelReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            src:
              "http://localhost:9000/giro/pdfGeneralReport.htm?values=" +
              value +
              "&names=" +
              "dateInit,dateEnd," +
              "businessOne,businessTwo,typeEvaluation" +
              "&types=" +
              "Date,Date,String,String,String" +
              "&nameReport=" +
              "RORIEV0001" +
              "&typeReport=" +
              "noStandard"
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onGenerateReportRiskConsolidateLNXls: function(btn) {
      if (
        btn
          .up("form")
          .getForm()
          .isValid()
      ) {
        var container = btn.up("container[name=generalContainer]");
        var businessLineOne =
          container.down("ViewComboBusinessLineOne").getValue() == undefined
            ? "T"
            : container.down("ViewComboBusinessLineOne").getValue();
        var businessLineTwo =
          container.down("ViewComboBusinessLineTwo").getValue() == undefined
            ? "T"
            : container.down("ViewComboBusinessLineTwo").getValue();
        var typeEvaluation = container
          .down("ViewComboTypeRiskEvaluation")
          .getValue();
        var value =
          container.down("datefield[name=dateInit]").getRawValue() +
          ";" +
          container.down("datefield[name=dateEnd]").getRawValue() +
          ";" +
          businessLineOne +
          ";" +
          businessLineTwo +
          ";" +
          typeEvaluation;
        var panelReport = container.down("panel[name=panelReport]");
        panelReport.removeAll();
        panelReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            src:
              "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
              value +
              "&names=" +
              "dateInit,dateEnd," +
              "businessOne,businessTwo,typeEvaluation" +
              "&types=" +
              "Date,Date,String,String,String" +
              "&nameReport=" +
              "RORIEV0001XLS" +
              "&typeReport=" +
              "noStandard"
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    }
  }
);
