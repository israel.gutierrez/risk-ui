Ext.define('DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelEvaluationRiskOperational', {
    extend: 'Ext.app.Controller',
    stores: [],
    models: [],
    views: [
        'risk.parameter.combos.ViewComboPercentReduction'
    ],
    refs: [
        {
            ref: '',
            selector: ''
        }
    ],
    init: function () {
        this.control({
            '[action=selectEventOneRisk]': {
                select: this._onSelectEventOneRisk
            },
            '[action=selectEventTwoRisk]': {
                select: this._onSelectEventTwoRisk
            },
            '[action=saveAnalisysExposition]': {
                click: this._onSaveAnalysisExposition
            },
            '[action=saveDebilityRiskOperational]': {
                click: this._onSaveDebilityRiskOperational
            },
            'ViewPanelEvaluationRiskOperational grid[itemId=riskDebility]': {
                itemclick: this._onDebilityRiskOperationalGrid
            },
            'ViewPanelEvaluationRiskOperational grid[itemId=gridActionPlan]': {
                itemcontextmenu: this._onActionPlanRikRightClick
            },
            '[action=saveControlRisk]': {
                click: this._onSaveControlRisk
            },
            '[action=goOutEvaluationPanel]': {
                click: this._onGoOutEvaluationPanel
            },
            'ViewPanelEvaluationRiskOperational grid[itemId=riskControl]': {
                itemcontextmenu: this.rightClickControlRisk
            },
            'ViewPanelEvaluationRiskOperational grid[itemId=riskTreatment]': {
                itemcontextmenu: this.rightClickTreatmentRisk
            },
            '[action=deleteDebilityRisk]': {
                click: this._onDeleteDebilityRisk
            },
            '[action=deleteControlRisk]': {
                click: this._onDeleteControlRisk
            },
            '[action=viewDetailActionPlan]': {
                click: this._onViewDetailActionPlan
            },
            '[action=evaluateControlRisk]': {
                click: this._onEvaluateControlRisk
            },
            '[action=saveEvaluationControlParameterRisk]': {
                click: this._onSaveEvaluationControlParameterRisk
            },
            'ViewWindowSearchControl grid': {
                itemcontextmenu: this._onRightClickSearchControl
            },
            '[action=searchAdvancedEventEvaluation]': {
                click: this._onSearchAdvancedEventEvaluation
            },
            '[action=searchAdvancedActionPlan]': {
                click: this._onSearchAdvancedActionPlan
            },
            '[action=deleteEventToRiskInRisk]': {
                click: this._onDeleteEventToRiskInRisk
            },
            '[action=searchAdvancedIdentificationEvaluation]': {
                click: this._SearchAdvancedIdentificationEvaluation
            }
        });
    },
    _onSelectEventOneRisk: function (cbo) {
        var panel = cbo.up('ViewWindowDebilityEntry');
        var comboTwo = panel.down('ViewComboEventTwo');
        comboTwo.getStore().load({
            url: 'http://localhost:9000/giro/showListEventTwoActivesComboBox.htm',
            params: {
                valueFind: cbo.getValue()
            },
            callback: function (cbo) {
                comboTwo.reset();
            }
        });
        comboTwo.setDisabled(false);
        panel.down('ViewComboEventThree').setDisabled(true);
        panel.down('ViewComboEventThree').reset();
    },
    _onSelectEventTwoRisk: function (cbo) {
        var panel = cbo.up('ViewWindowDebilityEntry');
        var comboTree = panel.down('ViewComboEventThree');
        comboTree.getStore().load({
            url: 'http://localhost:9000/giro/showListEventThreeActivesComboBox.htm',
            params: {
                valueFind: panel.down('ViewComboEventOne').getValue(),
                valueFind2: cbo.getValue()
            },
            callback: function () {
                comboTree.reset()
            }
        });
        comboTree.setDisabled(false);
    },
    _onSaveAnalysisExposition: function (button) {
        var panelEval = Ext.ComponentQuery.query('ViewPanelEvaluationRiskOperational')[0];
        var rowIdentify = panelEval.rowIdentify;
        var form = button.up('form');
        if (form.getForm().isValid()) {
            DukeSource.lib.Ajax.request({
                waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                method: 'POST',
                url: 'http://localhost:9000/giro/saveEvaluationInherentOfRisk.htm?nameView=PanelProcessRiskEvaluation',
                params: {
                    idRisk: rowIdentify.get('idRisk'),
                    frequency: form.down('ViewComboFrequency').getValue(),
                    featureFrequencyIds: panelEval.featureFrequencyIds,
                    frequencyIds: panelEval.frequencyIds,
                    featureImpactIds: panelEval.featureImpactIds,
                    impactIds: panelEval.impactIds,
                    impact: form.down('ViewComboImpact').getValue(),
                    scaleRisk: form.down('textfield[name=scaleRisk]').getValue(),
                    codeMatrix: form.down('textfield[name=codeMatrix]').getValue(),
                    idMatrix: form.down('textfield[name=idMatrix]').getValue(),
                    idRiskEvaluationMatrix: rowIdentify.get('idRiskEvaluationMatrix'),
                    idTypeMatrix: form.down('#typeMatrix').getValue(),
                    versionCorrelative: rowIdentify.get('versionCorrelative'),
                    reasonFrequency: form.down('#reasonFrequency').getValue(),
                    reasonImpact: form.down('#reasonImpact').getValue(),
                    expectedLoss: form.down('#expectedLoss').getValue()
                },
                scope: this,
                success: function (response) {
                    response = Ext.decode(response.responseText);

                    if (response.success) {
                        DukeSource.global.DirtyView.messageNormal(response.message);

                        setColorControlAndResidual(panelEval, response);
                        panelEval.down('#headFormEvaluationRisk').getForm().setValues(response.data);

                        settingColor(panelEval.down('#valueRiskInherent'), response.data['colourLost']);

                        panelEval.idOperationalRiskExposition = response.data['idOperationalRiskExposition'];
                        panelEval.idFrequency = response.data['idFrequency'];
                        panelEval.idImpact = response.data['idImpact'];
                        panelEval.equivalentFrequency = response.data['equivalentFrequency'];
                        panelEval.equivalentImpact = response.data['equivalentImpact'];
                    } else {
                        DukeSource.global.DirtyView.messageWarning(response.message);
                    }
                },
                failure: function () {
                }
            });
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }

    },
    _onSaveDebilityRiskOperational: function (button) {
        var panelEval = Ext.ComponentQuery.query('ViewPanelEvaluationRiskOperational')[0];
        var panel = button.up('window');
        var gridDebility = '';
        if (panel.origin === 'Inherint') {
            gridDebility = panelEval.down('#riskDebilityStart');
        } else {
            gridDebility = panelEval.down('#riskDebility');
        }

        if (panel.down('form').getForm().isValid()) {

            DukeSource.lib.Ajax.request({
                waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                method: 'POST',
                url: 'http://localhost:9000/giro/saveRiskWeaknessDetail.htm?nameView=PanelProcessRiskEvaluation',
                params: {
                    idRisk: panelEval.idRisk,
                    jsonData: Ext.JSON.encode(panel.down('form').getValues()),
                    idTypeMatrix: panelEval.down('#typeMatrix').getValue()
                },

                success: function (response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        DukeSource.global.DirtyView.messageNormal(response.message);
                        panel.close();
                        gridDebility.store.getProxy().extraParams = {
                            idRisk: panelEval.idRisk,
                            versionCorrelative: panelEval.versionCorrelative
                        };
                        gridDebility.store.getProxy().url = 'http://localhost:9000/giro/findRiskWeaknessDetail.htm';
                        var debilityIndex = DukeSource.global.DirtyView.getSelectedRowIndex(gridDebility);
                        gridDebility.getStore().load({
                            callback: function () {
                                if (debilityIndex != -1) {
                                    gridDebility.getSelectionModel().select(debilityIndex);
                                }
                            }
                        });
                        setColorControlAndResidual(panelEval, response);
                        panelEval.down('#headFormEvaluationRisk').getForm().setValues(response.data);

                    } else {
                        DukeSource.global.DirtyView.messageWarning(response.message);
                    }
                },
                failure: function () {
                }
            });
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }
    },
    _onDebilityRiskOperationalGrid: function (grid) {
        var panelEval = Ext.ComponentQuery.query('ViewPanelEvaluationRiskOperational')[0];
        var gridRisk = panelEval.down('#riskDebility');
        var gridControl = panelEval.down('#riskControl');
        var gridActionPlan = panelEval.down('#gridActionPlan');

        var rowRisk = gridRisk.getSelectionModel().getSelection()[0];
        gridControl.store.getProxy().extraParams = {
            idRiskWeaknessDetail: rowRisk.get('idRiskWeaknessDetail'),
            idRisk: rowRisk.get('idRisk'),
            versionCorrelative: rowRisk.get('versionCorrelative')
        };
        gridControl.store.getProxy().url = 'http://localhost:9000/giro/findDetailControl.htm';
        gridControl.down('pagingtoolbar').moveFirst();
        gridActionPlan.store.getProxy().extraParams = {
            stateDocument: '',
            idWeakness: rowRisk.get('idWeakness'),
            idRisk: rowRisk.get('idRisk')
        };
        gridActionPlan.store.getProxy().url = 'http://localhost:9000/giro/showDefaultFilesByStateAndIdRisk.htm?nameView=PanelProcessRiskEvaluation'
        gridActionPlan.down('pagingtoolbar').moveFirst();

    },
    _onActionPlanRikRightClick: function (view, rec, node, index, e) {
        var app = this.application;
        e.stopEvent();
        var rowMenu = Ext.create('Ext.menu.Menu', {
            width: 140,
            items: [
                {
                    text: 'SEGUIMIENTO',
                    iconCls: 'seguimiento',
                    hidden: true,
                    handler: function () {
                        var rowActionPlan = rec;
                        if (Ext.ComponentQuery.query('ViewPanelDocumentPending')[0] === undefined) {
                            DukeSource.global.DirtyView.verifyLoadController('DukeSource.controller.fulfillment.ControllerPanelDocumentPending');
                            var k = Ext.create('DukeSource.view.fulfillment.ViewPanelDocumentPending', {
                                title: 'PLANES DE ACCION',
                                closable: true,
                                border: false
                            });
                            locateActionPlan(k, rowActionPlan);
                        } else {
                            var pe = Ext.ComponentQuery.query('ViewPanelDocumentPending')[0];
                            locateActionPlan(pe, rowActionPlan);
                        }
                    }
                }
            ]
        });
        rowMenu.showAt(e.getXY());
    },
    _onSaveControlRisk: function (button) {
        var panelEval = Ext.ComponentQuery.query('ViewPanelEvaluationRiskOperational')[0];
        var gridDebility = panelEval.down('#riskDebility');
        var gridControl = panelEval.down('#riskControl');
        var panel = button.up('window');
        var records = panel.down('grid').getStore().getRange();
        var arrayDataToSave = [];
        var rowWeakness = gridDebility.getSelectionModel().getSelection()[0];

        Ext.Array.each(records, function (record) {
            arrayDataToSave.push(record.data);
        });
        if (panel.down('form').getForm().isValid()) {
            DukeSource.lib.Ajax.request({
                waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                method: 'POST',
                url: 'http://localhost:9000/giro/saveEvaluationControl.htm?nameView=PanelProcessRiskEvaluation',
                params: {
                    grid: Ext.JSON.encode(arrayDataToSave),
                    jsonData: Ext.JSON.encode(panel.down('form').getValues()),
                    idTypeMatrix: panelEval.down('#typeMatrix').getValue()
                },
                scope: this,
                success: function (response) {
                    response = Ext.decode(response.responseText);

                    if (response.success) {

                        DukeSource.global.DirtyView.messageNormal(response.message);
                        panel.close();

                        setColorControlAndResidual(panelEval, response);
                        panelEval.down('#headFormEvaluationRisk').getForm().setValues(response.data);

                        gridControl.store.getProxy().extraParams = {
                            idRiskWeaknessDetail: rowWeakness.get('idRiskWeaknessDetail'),
                            idRisk: rowWeakness.get('idRisk'),
                            versionCorrelative: rowWeakness.get('versionCorrelative')
                        };
                        gridControl.store.getProxy().url = 'http://localhost:9000/giro/findDetailControl.htm';
                        var debilityIndex = DukeSource.global.DirtyView.getSelectedRowIndex(gridDebility);
                        var controlIndex = DukeSource.global.DirtyView.getSelectedRowIndex(gridControl);
                        gridControl.getStore().load({
                            callback: function () {
                                if (controlIndex != -1) {
                                    gridControl.getSelectionModel().select(controlIndex);
                                }
                            }
                        });
                        gridDebility.getStore().load({
                            callback: function () {
                                gridDebility.getSelectionModel().select(debilityIndex);
                            }
                        });
                    } else {
                        DukeSource.global.DirtyView.messageWarning(response.message);
                    }
                }
            });
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }
    },
    _onGoOutEvaluationPanel: function () {

        Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0].setDisabled(false);
        Ext.ComponentQuery.query('ViewPanelEvaluationRiskOperational')[0].destroy();
        Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0].down('#riskIdentify').getStore().load();

    },
    rightClickControlRisk: function (view, rec, node, index, e) {
        e.stopEvent();
        var addMenu = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.AddMenuControl', {});
        addMenu.removeAll();
        addMenu.add(
            {
                text: 'Gestionar adjuntos', iconCls: 'gestionfile', handler: function () {
                    var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowManagementReportControl', {
                        modal: true
                    });
                    window.down('textfield[name=idDetailControl]').setValue(rec.get('idDetailControl'));
                    window.show();
                }
            }
        );
        addMenu.showAt(e.getXY());
    },
    rightClickTreatmentRisk: function (view, rec, node, index, e) {
        e.stopEvent();
        var addMenu = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.AddMenuTreatment', {});
        addMenu.removeAll();
        addMenu.add(
            {
                text: 'Plan de Acci&oacute;n', iconCls: 'auditory', handler: function () {
                    var gralPanel = Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0];
                    var gridRisk = gralPanel.down('#riskIdentify');
                    if (Ext.ComponentQuery.query('ViewPanelDocumentPending')[0] == undefined) {
                        DukeSource.global.DirtyView.verifyLoadController('DukeSource.controller.fulfillment.ControllerPanelDocumentPending');
                        var k = Ext.create('DukeSource.view.fulfillment.ViewPanelDocumentPending', {
                            title: 'PENDIENTES',
                            closable: true,
                            border: false
                        });
                        k.down('textfield[name=idRisk]').setValue(gridRisk.getSelectionModel().getSelection()[0].get('idRisk'));
                        k.down('UpperCaseTextFieldReadOnly[name=codeRisk]').setValue(gridRisk.getSelectionModel().getSelection()[0].get('codeRisk'));
                        k.down('UpperCaseTextFieldReadOnly[name=descriptionRisk]').setValue(gridRisk.getSelectionModel().getSelection()[0].get('descriptionRisk'));
                        k.down('UpperCaseTextFieldReadOnly[name=versionCorrelative]').setValue(gridRisk.getSelectionModel().getSelection()[0].get('versionCorrelative'));
                        DukeSource.getApplication().centerPanel.addPanel(k);
                    } else {
                        var k1 = Ext.ComponentQuery.query('ViewPanelDocumentPending')[0];
                        k1.down('textfield[name=idRisk]').setValue(gridRisk.getSelectionModel().getSelection()[0].get('idRisk'));
                        k1.down('UpperCaseTextFieldReadOnly[name=codeRisk]').setValue(gridRisk.getSelectionModel().getSelection()[0].get('codeRisk'));
                        k1.down('UpperCaseTextFieldReadOnly[name=descriptionRisk]').setValue(gridRisk.getSelectionModel().getSelection()[0].get('descriptionRisk'));
                        k1.down('UpperCaseTextFieldReadOnly[name=versionCorrelative]').setValue(gridRisk.getSelectionModel().getSelection()[0].get('versionCorrelative'));
                        DukeSource.getApplication().centerPanel.addPanel(k1);
                    }
                }
            }
        );
        addMenu.showAt(e.getXY());
    },
    _onDeleteDebilityRisk: function (button) {

        var panelEval = button.up('ViewPanelEvaluationRiskOperational');
        var gridWeakness;
        if (button.origin == 'Inherint') {
            gridWeakness = panelEval.down('#riskDebilityStart');
        } else {
            gridWeakness = panelEval.down('#riskDebility');
        }
        var rowWeakness = gridWeakness.getSelectionModel().getSelection()[0];

        if (rowWeakness === undefined) {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, 'Seleccione una causa', Ext.Msg.WARNING);
        } else {
            Ext.MessageBox.show({
                title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
                icon: Ext.Msg.QUESTION,
                buttonText: {
                    yes: "Si"
                },
                buttons: Ext.MessageBox.YESNO,
                fn: function (btn) {
                    if (btn === 'yes') {
                        DukeSource.lib.Ajax.request({
                            method: 'POST',
                            url: 'http://localhost:9000/giro/deleteRiskWeaknessDetail.htm?nameView=PanelProcessRiskEvaluation',
                            params: {
                                idRiskWeaknessDetail: rowWeakness.get('idRiskWeaknessDetail'),
                                idRisk: rowWeakness.get('idRisk'),
                                versionCorrelative: rowWeakness.get('versionCorrelative'),
                                idTypeMatrix: panelEval.down('#typeMatrix').getValue()
                            },
                            success: function (response) {
                                response = Ext.decode(response.responseText);
                                if (response.success) {

                                    setColorControlAndResidual(panelEval, response);
                                    panelEval.down('#headFormEvaluationRisk').getForm().setValues(response.data);

                                    gridWeakness.getStore().load();
                                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.message, Ext.Msg.INFO);
                                } else {
                                    DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_ERROR, response.mensaje, Ext.Msg.ERROR);
                                }
                            },
                            failure: function () {
                            }
                        });
                    }
                }
            });
        }
    },
    _onDeleteControlRisk: function (button) {

        var panelEval = button.up('ViewPanelEvaluationRiskOperational');
        var gridDebility = panelEval.down('#riskDebility');
        var gridControl = panelEval.down('#riskControl');
        var rowControl = gridControl.getSelectionModel().getSelection()[0];

        if (rowControl === undefined) {
            DukeSource.global.DirtyView.messageWarning('Seleccione un control identificado por favor');
        } else {
            Ext.MessageBox.show({
                title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
                icon: Ext.Msg.QUESTION,
                buttonText: {
                    yes: "Si"
                },
                buttons: Ext.MessageBox.YESNO,
                fn: function (btn) {
                    if (btn === 'yes') {
                        DukeSource.lib.Ajax.request({
                            method: 'POST',
                            url: 'http://localhost:9000/giro/deleteEvaluationControl.htm?nameView=PanelProcessRiskEvaluation',
                            params: {
                                jsonData: Ext.JSON.encode(rowControl.data),
                                idTypeMatrix: panelEval.down('#typeMatrix').getValue()
                            },
                            success: function (response) {
                                response = Ext.decode(response.responseText);
                                if (response.success) {
                                    DukeSource.global.DirtyView.messageNormal(response.message);

                                    setColorControlAndResidual(panelEval, response);
                                    panelEval.down('#headFormEvaluationRisk').getForm().setValues(response.data);

                                    var debilityIndex = DukeSource.global.DirtyView.getSelectedRowIndex(gridDebility);
                                    gridControl.getStore().load();
                                    gridDebility.getStore().load({
                                        callback: function () {
                                            gridDebility.getSelectionModel().select(debilityIndex);
                                        }
                                    });
                                } else {
                                    DukeSource.global.DirtyView.messageWarning(response.message);
                                }
                            },
                            failure: function () {
                            }
                        });
                    }
                }
            });
        }
    },
    _onViewDetailActionPlan: function (button) {
        var gridActionPlan = button.up('grid');
        var rowActionPlan = gridActionPlan.getSelectionModel().getSelection()[0];
        if (Ext.ComponentQuery.query('ViewPanelDocumentPending')[0] === undefined) {
            DukeSource.global.DirtyView.verifyLoadController('DukeSource.controller.fulfillment.ControllerPanelDocumentPending');
            var k = Ext.create('DukeSource.view.fulfillment.ViewPanelDocumentPending', {
                title: 'PLANES DE ACCION',
                closable: true,
                border: false
            });
            locateActionPlan(k, rowActionPlan);
        } else {
            var pe = Ext.ComponentQuery.query('ViewPanelDocumentPending')[0];
            locateActionPlan(pe, rowActionPlan);
        }

    },
    _onEvaluateControlRisk: function (btn) {
        var grid = Ext.ComponentQuery.query("ViewWindowSearchControl grid")[0];
        var recordControl = grid.getSelectionModel().getSelection()[0];

        if (recordControl === undefined) {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
        } else {
            var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowControlEntry', {
                modal: true,
                origin: 'evaluationRisk'
            });

            DukeSource.lib.Ajax.request({
                method: 'POST',
                url: 'http://localhost:9000/giro/showFinalScoreControlActive.htm',
                params: {
                    idControl: recordControl.get('idControl'), start: '0', limit: '100'
                },
                success: function (response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        for (var i = 0; i < response.data.length; i++) {
                            var scoreByGroup = {};
                            DukeSource.global.DirtyView.createGroup(scoreByGroup, response, i);

                            scoreByGroup.typeControl = recordControl.get('typeControl');
                            window.numberItems = response.data.length;
                            window.groups = response.data;
                            DukeSource.global.DirtyView.createItemsToFinalScore(window, scoreByGroup);
                        }
                        window.setWidth(response.data.length > 2 ? 310 * (response.data.length) : 760);

                        var scoreFinal = DukeSource.global.DirtyView.createScoreFinal(response);
                        scoreFinal.typeControl = recordControl.get('typeControl');

                        DukeSource.global.DirtyView.createItemsToFinalScore(window, scoreFinal);
                        window.down('#' + scoreFinal.idItemValue).setFieldLabel('Rating Total');

                        DukeSource.global.DirtyView.createButton(window);
                        window.down('form').getForm().loadRecord(recordControl);
                        window.show();
                    } else {
                        DukeSource.global.DirtyView.messageWarning(response.message);
                    }
                }
            });
            DukeSource.lib.Ajax.request({
                method: 'POST',
                url: 'http://localhost:9000/giro/findControlById.htm',
                params: {
                    propertyFind: 'idControl',
                    valueFind: recordControl.get('idControl'),
                    propertyOrder: 'description',
                    start: '0',
                    limit: '25'
                },
                success: function (response) {
                    var form = window.down('form');
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        form.getForm().setValues(response.data);
                        form.down('#idDetailControl').setValue(response.data.idControl);
                        form.down('#idVersionControl').setValue(response.data.idVersionControl);
                    } else {
                        DukeSource.global.DirtyView.messageWarning(response.message);
                    }
                    window.down('#descriptionEvaluation').focus(false, 100);
                },
                failure: function () {
                }
            });

            var gridControl = window.down('grid');
            gridControl.store.getProxy().extraParams = {
                idControl: recordControl.get('idControl')
            };
            gridControl.store.getProxy().url = 'http://localhost:9000/giro/showListControlTypeToEvaluateControl.htm';
            gridControl.down('pagingtoolbar').moveFirst();

            if (hidden('WEV_BTN_EvaluateControlRisk') && recordControl.get('stateEvaluatedControl') == 'S') {
                window.down("#saveEvaluationControl").setDisabled(true);
                window.down("#gridEvaluationControl").setDisabled(true);
            }

        }
    },
    _onSaveEvaluationControlParameterRisk: function (button) {
        var panel = button.up('window');
        var records = panel.down('grid').getStore().getRange();
        var arrayDataToSave = [];

        Ext.Array.each(records, function (record) {
            arrayDataToSave.push(record.data);
        });
        if (panel.down('form').getForm().isValid()) {
            DukeSource.lib.Ajax.request({
                waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                method: 'POST',
                url: 'http://localhost:9000/giro/saveEvaluationControlParameter.htm?nameView=ViewPanelRegisterControl',
                params: {
                    grid: Ext.JSON.encode(arrayDataToSave),
                    jsonData: Ext.JSON.encode(panel.down('#formHeaderControl').getValues()),
                    scoreGroups: Ext.JSON.encode(panel.buildObjectGroups())
                },
                scope: this,
                success: function (response) {
                    response = Ext.decode(response.responseText);

                    if (response.success) {
                        DukeSource.global.DirtyView.messageNormal(response.message);

                        Ext.ComponentQuery.query('ViewWindowSearchControl')[0].down('grid').getStore().load();
                        panel.close();

                    } else {
                        DukeSource.global.DirtyView.messageWarning(response.message);

                    }
                },
                failure: function () {
                }
            });
        } else {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_COMPLETE, Ext.Msg.ERROR);
        }
    },
    _onRightClickSearchControl: function (view, rec, node, index, e) {
        var app = this.application;
        e.stopEvent();
        var rowMenu = Ext.create('Ext.menu.Menu', {
            width: 140,
            items: [
                {
                    text: 'Nuevo',
                    iconCls: 'new',
                    handler: function () {
                        Ext.create('DukeSource.view.risk.parameter.controls.ViewWindowRegisterControl', {
                            modal: true
                        }).show();
                    }
                },
                {
                    text: 'Evaluar',
                    iconCls: 'evaluate',
                    handler: function () {
                        app.getController('DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelEvaluationRiskOperational')._onEvaluateControlRisk();
                    }
                }]
        });
        rowMenu.showAt(e.getXY());
    },
    _onSearchAdvancedEventEvaluation: function (btn) {
        if (btn.up('form').getForm().isValid()) {
            var form = btn.up('form');
            var lm = ',';
            var codeEvent = form.down('#codeEvent').getRawValue() === undefined ? '' : form.down('#codeEvent').getRawValue();
            var initDateReport = form.down('#dateRegisterInit').getRawValue() === undefined ? '' : form.down('#dateRegisterInit').getRawValue();
            var endDateReport = form.down('#dateRegisterEnd').getRawValue() === undefined ? '' : form.down('#dateRegisterEnd').getRawValue();
            var userName = form.down('#idUser').getValue() === undefined ? '' : form.down('#idUser').getValue();
            var area = form.down('#workArea').getValue() === undefined ? '' : form.down('#workArea').getValue();
            var process = form.down('#process').getValue() === undefined ? '' : form.down('#process').getValue();
            var subProcess = form.down('#subProcess').getValue() === undefined ? '' : form.down('#subProcess').getValue();
            var amount = form.down('#amount').getValue() === undefined ? '' : form.down('#amount').getValue();
            var grid = Ext.ComponentQuery.query('ViewWindowEventRelationRisk grid[itemId=gridEventToRisk]')[0];
            grid.store.getProxy().extraParams = {
                fields: 'ev.codeEvent' + lm + 'ev.dateAcceptLossEvent' + lm + 'ev.dateAcceptLossEvent' + lm + 'ev.audit.userInsert' + lm
                    + 'wa.id' + lm + 'p.idProcess' + 'p.idSubProcess' + lm + 'ev.grossLoss',
                values: codeEvent + lm + initDateReport + lm + endDateReport + lm + userName + lm + area + lm + process + lm + subProcess + lm + amount,
                types: 'String' + lm + 'Date' + lm + 'Date' + lm + 'String' + lm + 'Integer' + lm + 'Integer' + lm + 'Integer' + lm + 'BigDecimal',
                operators: 'equal' + lm + 'majorEqual' + lm + 'minorEqual' + +'equal' + lm + 'equal'
                    + lm + 'equal' + lm + 'equal' + lm + 'majorEqual',
                searchIn: 'Event'
            };
            grid.store.getProxy().url = 'http://localhost:9000/giro/advancedSearchEvent.htm';
            grid.down('pagingtoolbar').moveFirst();
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }
    },
    _onSearchAdvancedActionPlan: function (btn) {
        var me = Ext.ComponentQuery.query('AdvancedSearchActionPlan')[0];
        var form = me.down('form');

        if (form.getForm().isValid()) {
            var values = me.down('#dateInitFirst').getRawValue() + ',' + me.down('#dateInitSecond').getRawValue() + ',' +
                me.down('#idUserEmitted').getRawValue() + ',' +
                me.down('#idUserReceptor').getRawValue() + ',' +
                me.down('#codePlan').getRawValue() + ',' +
                me.down('#validityPlan').getRawValue() + ',' +
                me.down('#statePlan').getValue();
            var fields = 'd.dateReception,d.dateReception,' +
                'ue.username,ur.username,d.codePlan,td.id,sd.id,';
            var types = 'Date,Date,String,String,String,Long,String';
            var operator = 'majorEqual,minorEqual,equal,equal,equal,equal,like';
            var grid = Ext.ComponentQuery.query('ViewWindowSearchActionPlan grid')[0];
            grid.store.getProxy().url = 'http://localhost:9000/giro/findDetailDocument.htm';
            grid.store.getProxy().extraParams = {
                fields: fields,
                values: values,
                types: types,
                operator: operator,
                searchIn: 'ActionPlan'
            };
            grid.down('pagingtoolbar').doRefresh();
        } else {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.MESSAGE_FIELDS_ERROR);
        }
    },
    _onDeleteEventToRiskInRisk: function (btn) {
        var panelRisk = Ext.ComponentQuery.query('ViewPanelEvaluationRiskOperational')[0];

        var grid = panelRisk.down('#riskEventsToRisk');
        var record = grid.getSelectionModel().getSelection()[0];
        if (record === undefined) {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_ITEM, Ext.Msg.WARNING);
        } else {
            Ext.MessageBox.show({
                title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                msg: 'Esta seguro que desea continuar?',
                icon: Ext.Msg.WARNING,
                buttonText: {
                    yes: "Si"
                },
                buttons: Ext.MessageBox.YESNO,
                fn: function (btn) {
                    if (btn === 'yes') {
                        DukeSource.lib.Ajax.request({
                            method: 'POST',
                            url: 'http://localhost:9000/giro/deleteRelationEventRisk.htm?nameView=PanelProcessRiskEvaluation',
                            params: {
                                jsonData: Ext.JSON.encode(record.data),
                                idEvent: record.get('idEvent'),
                                idRisk: panelRisk.idRisk,
                                versionRisk: panelRisk.versionCorrelative
                            },
                            success: function (response) {
                                response = Ext.decode(response.responseText);
                                if (response.success) {
                                    DukeSource.global.DirtyView.messageNormal(response.message);
                                    grid.down('pagingtoolbar').doRefresh();
                                    panelRisk.down('#riskDebilityAssigned').down('pagingtoolbar').doRefresh();

                                    panelRisk.down('#amountEventLost').setValue(response.data['amountEventLost']);
                                    panelRisk.down('#numberEventLost').setValue(response.data['numberEventLost']);
                                } else {
                                    DukeSource.global.DirtyView.messageWarning(response.message);
                                }
                            },
                            failure: function () {
                            }
                        });
                    }
                }
            });
        }
    },

    _SearchAdvancedIdentificationEvaluation: function (btn) {
        if (btn.up('form').getForm().isValid()) {
            var form = btn.up('form');
            var limit = ',';

            var userRegister = form.down('#idUser').getRawValue() === undefined ? '' : form.down('#idUser').getRawValue();
            var fullNameRegister = form.down('#nameUser').getValue() === undefined ? '' : form.down('#nameUser').getValue();
            var dateEvaluation = form.down('#dateEvaluationInit').getRawValue() === undefined ? '' : form.down('#dateEvaluationInit').getRawValue();
            var stateIdentification = form.down('#stateIdentification').getValue() === undefined ? '' : form.down('#stateIdentification').getValue();
            var grid = Ext.ComponentQuery.query('WindowRelationIdentifyEvaluationRisk grid')[0];
            grid.store.getProxy().extraParams = {
                fields: 'ur.id' + limit + 'ur.fullName' + limit + 'ir.dateEvaluation' + limit + 'ir.stateIdentification',
                values: userRegister + limit + fullNameRegister + limit + dateEvaluation + limit + stateIdentification + limit,
                types: 'String' + limit + 'String' + limit + 'Date' + limit + 'String',
                operators: 'equal' + limit + 'like' + limit + 'equal' + limit + 'like'
            };
            grid.store.getProxy().url = 'http://localhost:9000/giro/advancedSearchIdentificationRisk.htm';
            grid.down('pagingtoolbar').moveFirst();
        } else {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_COMPLETE, Ext.Msg.ERROR);
        }
    }
});

function locateActionPlan(k, rowActionPlan) {
    DukeSource.getApplication().centerPanel.addPanel(k);
    k.down('combobox').setValue(rowActionPlan.get('nameStateDocument'));
    var fields = 'r.idRisk,ver.id.correlative,ue.category,w.idWeakness';
    var values = rowActionPlan.get('idRisk') + ',' + rowActionPlan.get('versionCorrelative') + ',' + rowActionPlan.get('category') + ',' + rowActionPlan.get('idWeakness');
    var types = 'Long,Long,String,Long';
    var operator = 'equal,equal,equal,equal';
    var gridMasterPlan = k.down('grid');
    gridMasterPlan.store.getProxy().extraParams = {
        fields: fields,
        values: values,
        types: types,
        operator: operator,
        searchIn: 'ActionPlanWeakness',
        stateDocument: DukeSource.global.GiroConstants.PROCESS
    };
    gridMasterPlan.store.getProxy().url = 'http://localhost:9000/giro/findDetailDocument.htm';
    gridMasterPlan.down('pagingtoolbar').doRefresh();
}

function settingColor(a, color) {
    color = color === undefined ? 'd9ffdb' : color;
    a.setFieldStyle('background-color: #' + color + ';');
}

function setColorControlAndResidual(panel, response) {
    panel.down('#scoreDescriptionControl').reset();
    panel.down('#evaluationFinalControl').reset();
    panel.down('#valueLostResidual').reset();
    panel.down('#descriptionFrequencyResidual').reset();
    panel.down('#descriptionImpactResidual').reset();

    settingColor(panel.down('#evaluationFinalControl'), response.data['colourControl']);
    settingColor(panel.down('#valueLostResidual'), response.data['colourLostResidual']);
    panel.down('#valueRiskInherent').setFieldLabel('Nivel - ' + response.data['descriptionScaleRisk']);
    panel.down('#valueLostResidual').setFieldLabel('Nivel - ' + response.data['descriptionScaleResidual']);
    panel.down('#idMatrixResidual').setValue(response.data['idMatrixResidual']);
    panel.resumeEvaluation = response.data;
}