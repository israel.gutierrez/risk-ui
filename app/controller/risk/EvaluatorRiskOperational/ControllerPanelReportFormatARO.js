Ext.define(
  "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelReportFormatARO",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboProcessType",
      "risk.parameter.combos.ViewComboBusinessLineOne",
      "risk.parameter.combos.ViewComboTypeRiskEvaluation",
      "risk.parameter.combos.ViewComboBusinessLineTwo",
      "risk.parameter.combos.ViewComboProcess"
    ],
    init: function() {
      this.control({
        "[action=generateReportFormatPlainXls]": {
          click: this._onGenerateReportFormatPlainXls
        },
        "[action=generateReportFormatAROXls]": {
          click: this._onGenerateReportFormatAROXls
        }
      });
    },

    _onGenerateReportFormatPlainXls: function(btn) {
      if (
        btn
          .up("form")
          .getForm()
          .isValid()
      ) {
        var container = btn.up("container[name=generalContainer]");
        var maxExposition = container.down("#idMaxExposition").getValue();
        var riskEvaluationMatrix = container
          .down("#idRiskEvaluationMatrix")
          .getValue();
        var businessLineOne =
          container.down("ViewComboBusinessLineOne").getValue() == undefined
            ? "T"
            : container.down("ViewComboBusinessLineOne").getValue();
        var businessLineTwo =
          container.down("ViewComboBusinessLineTwo").getValue() == undefined
            ? "T"
            : container.down("ViewComboBusinessLineTwo").getValue();
        var typeProcess =
          container.down("ViewComboProcessType").getValue() == undefined
            ? "T"
            : container.down("ViewComboProcessType").getValue();
        var typeEvaluation = container
          .down("ViewComboTypeRiskEvaluation")
          .getValue();
        var process =
          container.down("ViewComboProcess").getValue() == undefined
            ? "T"
            : container.down("ViewComboProcess").getValue();
        var value =
          maxExposition +
          ";" +
          riskEvaluationMatrix +
          ";" +
          container.down("datefield[name=dateInit]").getRawValue() +
          ";" +
          container.down("datefield[name=dateEnd]").getRawValue() +
          ";" +
          businessLineOne +
          ";" +
          businessLineTwo +
          ";" +
          typeProcess +
          ";" +
          process +
          ";" +
          typeEvaluation;
        var panelReport = container.down("panel[name=panelReport]");
        panelReport.removeAll();
        panelReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            src:
              "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
              value +
              "&names=" +
              "idMaxExposition,riskEvaluationMatrix,dateInit,dateEnd," +
              "idBusinessOne,idBusinessTwo,typeProcess,process,typeEvaluation" +
              "&types=" +
              "String,String,Date,Date,String,String,String,String,String" +
              "&nameReport=" +
              "RORIEV0003_01XLS" +
              "&typeReport=" +
              "noStandard"
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onGenerateReportFormatAROXls: function(btn) {
      if (
        btn
          .up("form")
          .getForm()
          .isValid()
      ) {
        var container = btn.up("container[name=generalContainer]");
        var maxExposition = container.down("#idMaxExposition").getValue();
        var riskEvaluationMatrix = container
          .down("#idRiskEvaluationMatrix")
          .getValue();
        var businessLineOne =
          container.down("ViewComboBusinessLineOne").getValue() == undefined
            ? "T"
            : container.down("ViewComboBusinessLineOne").getValue();
        var businessLineTwo =
          container.down("ViewComboBusinessLineTwo").getValue() == undefined
            ? "T"
            : container.down("ViewComboBusinessLineTwo").getValue();
        var typeProcess =
          container.down("ViewComboProcessType").getValue() == undefined
            ? "T"
            : container.down("ViewComboProcessType").getValue();
        var typeEvaluation = container
          .down("ViewComboTypeRiskEvaluation")
          .getValue();
        var process =
          container.down("ViewComboProcess").getValue() == undefined
            ? "T"
            : container.down("ViewComboProcess").getValue();
        var value =
          maxExposition +
          ";" +
          riskEvaluationMatrix +
          ";" +
          container.down("datefield[name=dateInit]").getRawValue() +
          ";" +
          container.down("datefield[name=dateEnd]").getRawValue() +
          ";" +
          businessLineOne +
          ";" +
          businessLineTwo +
          ";" +
          typeProcess +
          ";" +
          process +
          ";" +
          typeEvaluation;
        var panelReport = container.down("panel[name=panelReport]");
        panelReport.removeAll();
        panelReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            src:
              "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
              value +
              "&names=" +
              "idMaxExposition,riskEvaluationMatrix,dateInit,dateEnd," +
              "idBusinessOne,idBusinessTwo,typeProcess,process,typeEvaluation" +
              "&types=" +
              "String,String,Date,Date,String,String,String,String,String" +
              "&nameReport=" +
              "RORIEV0003XLS" +
              "&typeReport=" +
              "noStandard"
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    }
  }
);
