Ext.define(
  "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelChartsRisk",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [],
    init: function() {
      this.control({
        "[action=executeEvolutionRisk]": {
          click: this.executeEvolutionRisk
        },
        "ViewPanelChartsRisk [name=matrixOriginal]": {
          celldblclick: this._onViewDetailMap
        },
        "ViewPanelChartsRisk [name=matrixEvolution]": {
          celldblclick: this._onViewDetailMap
        },
        "[action=executeProfileRisk]": {
          click: this._onExecuteProfileRisk
        }
      });
    },
    executeEvolutionRisk: function(btn) {
      if (
        btn
          .up("form")
          .getForm()
          .isValid()
      ) {
        var panel = Ext.ComponentQuery.query("ViewPanelChartsRisk")[0];

        var typeMatrix = panel.down("#typeMatrix").getValue();
        var maxExposition = panel.down("#maxExposition").getValue();
        var dateInit = panel.down("#dateInit").getValue();
        var dateEnd = panel.down("#dateEnd").getValue();

        var gridOriginal = panel.down("#matrixOriginal");
        var gridEvolution = panel.down("#matrixEvolution");

        gridOriginal.setTitle(
          "Riesgos residuales registrados entre: " +
            Ext.Date.format(dateInit, "d/m/Y") +
            " y " +
            Ext.Date.format(dateEnd, "d/m/Y")
        );

        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/getAllRiskMapFilter.htm?nameView=ViewPanelMatrixMap",
          params: {
            maxExposition: maxExposition,
            typeMatrix: typeMatrix,
            businessLineOne: "",
            process: "",
            subProcess: "",
            typeRisk: "RR",
            stateRisk: "",
            versionPosition: "first",
            dateInit: dateInit,
            dateEnd: dateEnd
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            gridOriginal.getView().refresh();
            if (response.success) {
              var data = Ext.JSON.decode(response.data);
              for (var i = 0; i < data.length; i++) {
                var valueMatrix = data[i]["valueMatrix"];
                var positionX = data[i]["positionX"] + 1;
                var positionY = data[i]["positionY"];
                var cell = gridOriginal
                  .getView()
                  .getCellByPosition({ row: positionY, column: positionX });
                var value = cell.dom.textContent;
                if ((value = Ext.util.Format.number(valueMatrix, "0,0.00"))) {
                  cell.insertHtml(
                    "afterBegin",
                    '<button type="button" class="buttonRisk black text-blanco text-shadow-negra">' +
                      valueMatrix +
                      "</button>",
                    true
                  );
                }
              }
            } else {
              DukeSource.global.DirtyView.messageWarning(
                DukeSource.global.GiroMessages.MESSAGE_COMPLETE
              );
            }
          }
        });

        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/getAllRiskMapFilter.htm?nameView=ViewPanelMatrixMap",
          params: {
            maxExposition: maxExposition,
            typeMatrix: typeMatrix,
            businessLineOne: "",
            process: "",
            subProcess: "",
            typeRisk: "RR",
            stateRisk: "",
            versionPosition: "last",
            dateInit: dateInit,
            dateEnd: dateEnd
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            gridEvolution.getView().refresh();
            if (response.success) {
              var data = Ext.JSON.decode(response.data);
              for (var i = 0; i < data.length; i++) {
                var valueMatrix = data[i]["valueMatrix"];
                var positionX = data[i]["positionX"] + 1;
                var positionY = data[i]["positionY"];
                var cell = gridEvolution
                  .getView()
                  .getCellByPosition({ row: positionY, column: positionX });
                var value = cell.dom.textContent;
                if ((value = Ext.util.Format.number(valueMatrix, "0,0.00"))) {
                  cell.insertHtml(
                    "afterBegin",
                    '<button type="button" class="buttonRisk black text-blanco text-shadow-negra">' +
                      valueMatrix +
                      "</button>",
                    true
                  );
                }
              }
            } else {
              DukeSource.global.DirtyView.messageWarning(
                DukeSource.global.GiroMessages.MESSAGE_COMPLETE
              );
            }
          }
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE
        );
      }
    },
    _onViewDetailMap: function(
      grid,
      td,
      cellIndex,
      record,
      tr,
      rowIndex,
      e,
      eOpts
    ) {
      var clickedDataIndex = grid.panel.headerCt.getHeaderAtIndex(cellIndex)
        .dataIndex;
      var clickedCellValue = record.get(clickedDataIndex);

      var win = Ext.create(
        "DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDashboardManagerRisk",
        {
          modal: true
        }
      );
      win.show();
      var gridWin = win.down("grid");
      var fields = "madet.idMatrix";
      var values = clickedCellValue.idMatrix;
      var types = "Long";
      var operator = "equal";
      gridWin.store.proxy.url = "http://localhost:9000/giro/findMatchRisk.htm";
      gridWin.store.proxy.extraParams = {
        fields: fields,
        values: values,
        types: types,
        operators: operator,
        search: "full",
        isLast: "false",
        condition: ""
      };
      gridWin.down("pagingtoolbar").doRefresh();
    },

    _onExecuteProfileRisk: function(btn) {
      var panel = Ext.ComponentQuery.query("ViewPanelChartsRisk")[0];
      panel.down("#containerReport").removeAll();

      var typeMatrix = panel.down("#typeMatrix").getValue();
      var maxExposition = panel.down("#maxExposition").getValue();

      Ext.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/showProfileRisk.htm",
        params: {
          maxExposition: maxExposition,
          typeMatrix: typeMatrix
        },
        success: function(response) {
          response = Ext.decode(response.responseText);

          if (response.success) {
            var heatMaps = Ext.JSON.decode(response.heatMaps);

            for (var i = 0; i < heatMaps.length; i++) {
              var idTypeMatrix = heatMaps[i]["idTypeMatrix"];
              var nameHeatMap = heatMaps[i]["description"];
              var data = Ext.JSON.decode(response["data" + idTypeMatrix]);
              var xAxis = Ext.JSON.decode(response["impact" + idTypeMatrix]);
              var yAxis = Ext.JSON.decode(response["frequency" + idTypeMatrix]);

              panel.down("#containerReport").add({
                xtype: "heat-map-risk",
                nameHeatMap: nameHeatMap,
                id: "firstHeatMap-" + idTypeMatrix
              });
              panel
                .down("#firstHeatMap-" + idTypeMatrix)
                .graphicHeatMap(data, xAxis, yAxis);
            }
          } else {
            DukeSource.global.DirtyView.messageWarning(
              DukeSource.global.GiroMessages.MESSAGE_COMPLETE
            );
          }
        }
      });
    }
  }
);
