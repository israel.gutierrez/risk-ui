Ext.define('DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelIdentifyRiskOperational', {
    extend: 'Ext.app.Controller',
    stores: [],
    models: [],
    views: [],
    refs: [
        {
            ref: 'ViewPanelPreviousIdentifyRiskOperational',
            selector: 'ViewPanelPreviousIdentifyRiskOperational'
        },
        {
            ref: 'viewPanelIdentifyRiskOperational',
            selector: 'ViewPanelIdentifyRiskOperational'
        },
        {
            ref: 'viewPanelEvaluationRiskOperational',
            selector: 'ViewPanelEvaluationRiskOperational'
        }
    ],
    init: function () {
        this.control({
            '[action=newRiskOperational]': {
                click: this._onNewRiskOperational
            },
            '[action=modifyRiskOperational]': {
                click: this._onModifyRiskOperational
            },
            '[action=selectProcessTypeRisk]': {
                select: this._onSelectProcessTypeRisk
            },
            '[action=selectProcessRisk]': {
                select: this._onSelectProcessRisk
            },
            '[action=selectSubProcessRisk]': {
                select: this._onSelectSubProcessRisk
            },
            '[action=saveRiskOperationalDetail]': {
                click: this._onSaveRiskOperationalDetail
            },
            'ViewPanelIdentifyRiskOperational grid[name=riskIdentify]': {
                itemclick: this._onIdentifyRiskOperationalGrid,
                itemcontextmenu: this.rightClickIdentifyRisk
            },
            '[action=startEvaluation]': {
                click: this._onStartEvaluation
            },
            '[action=comboGridSelection]': {
                select: this._onComboGridSelection
            },
            '[action=buttonGoEvaluation]': {
                click: this._onButtonGoEvaluation
            },
            '[action=buttonGoPreviousEvaluation]': {
                click: this._onButtonGoPreviousEvaluation
            },
            '[action=goOutRiskOperationalDetail]': {
                click: this._onGoOutRiskOperationalDetail
            },
            '[action=attachReportToRisk]': {
                click: this._onAttachReportToRisk
            },
            '[action=downloadReportToRisk]': {
                click: this._onDownloadReportToRisk
            },
            '[action=searchTriggerGridRiskIdentify]': {
                keyup: this._onSearchTriggerGridRiskIdentify
            },
            '[action=saveNewDebility]': {
                click: this._onSaveNewDebility
            },
            '[action=saveTreatmentRiskOperational]': {
                click: this._onSaveTreatmentRiskOperational
            }
        });
    },
    _onNewRiskOperational: function () {
        var panelIdentify = Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0];

        if (Ext.ComponentQuery.query('ViewWindowRegisterRisk')[0] === undefined) {
            this.getViewPanelPreviousIdentifyRiskOperational();
            var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowRegisterRisk', {
                link: 'saveRisk',
                idRiskEvaluationMatrix: panelIdentify.idRiskEvaluationMatrix,
                actionType: 'new',
                width: 900
            });


            window.show(undefined, function () {
                window.down('textfield[name=idRisk]').setValue('id');
                window.down('#maxAmount').setValue(maxExpositionActive);

                if (panelIdentify.nodeProcess !== undefined) {
                    window.down('#idProcessType').setValue(panelIdentify.nodeProcess.get('idProcessType'));
                    window.down('#idProcess').setValue(panelIdentify.nodeProcess.get('idProcess'));
                    window.down('#idSubProcess').setValue(panelIdentify.nodeProcess.get('idSubProcess'));
                    window.down('#idActivity').setValue(panelIdentify.nodeProcess.get('idActivity'));
                    window.down('#codeProcess').setValue(panelIdentify.nodeProcess.get('abbreviation'));
                    window.down('#product').setValue(panelIdentify.nodeProcess.get('idProduct'));

                    if (panelIdentify.nodeProcess.get('descriptionProcess') !== '')
                        window.down('#descriptionProcess').setValue(panelIdentify.nodeProcess.get('descriptionProcess'));
                    if (panelIdentify.nodeProcess.get('descriptionProduct') !== '')
                        window.down('#descriptionProduct').setValue(panelIdentify.nodeProcess.get('descriptionProduct'));

                }
                window.down('#description').focus(false, 300);
            });
        } else {
        }
    },

    _onModifyRiskOperational: function () {
        var gridRisk = this.getViewPanelIdentifyRiskOperational().down('grid');
        var row = gridRisk.getSelectionModel().getSelection()[0];
        if (row === undefined) {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
        } else {
            if (Ext.ComponentQuery.query('ViewWindowRegisterRisk')[0] === undefined) {

                var win = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowRegisterRisk', {
                    link: 'updateRisk',
                    actionType: 'modify',
                    width: 900
                });
                DukeSource.lib.Ajax.request({
                    method: 'POST',
                    url: 'http://localhost:9000/giro/getRiskByIdRisk.htm',
                    params: {
                        idRisk: row.get('idRisk'),
                        versionRisk: row.get('versionCorrelative')
                    },
                    scope: this,
                    success: function (response) {
                        response = Ext.decode(response.responseText);
                        if (response.success) {

                            win.down('form').getForm().setValues(response.data);
                            win.show();

                        } else {
                            DukeSource.global.DirtyView.messageWarning(response.message);
                        }
                    }
                });
            } else {
            }
        }
    },

    _onSearchTriggerGridRiskIdentify: function (text) {
        var grid = Ext.ComponentQuery.query("ViewPanelIdentifyRiskOperational grid[name=riskIdentify]")[0];
        DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },

    _onSelectProcessTypeRisk: function (cbo) {
        var panel = cbo.up('ViewWindowRegisterRisk');
        var comboProcess = panel.down('ViewComboProcess');
        var comboSubProcess = panel.down('ViewComboSubProcess');
        var comboActivity = panel.down('ViewComboActivity');
        comboProcess.getStore().load({
            url: 'http://localhost:9000/giro/showListProcessActives.htm',
            params: {
                valueFind: cbo.getValue()
            },
            callback: function (cbo) {
                comboProcess.reset();
                comboSubProcess.reset();
                comboActivity.reset();
                comboSubProcess.setDisabled(true);
                comboActivity.setDisabled(true);
            }
        });
        comboProcess.setDisabled(false);
    },

    _onSelectProcessRisk: function (cbo) {
        var panel = cbo.up('ViewWindowRegisterRisk');
        var comboSubProcess = panel.down('ViewComboSubProcess');
        var comboActivity = panel.down('ViewComboActivity');
        comboSubProcess.getStore().load({
            url: 'http://localhost:9000/giro/showListSubProcessActives.htm',
            params: {
                valueFind: cbo.getValue()
            },
            callback: function (cbo) {
                comboSubProcess.reset();
                comboActivity.reset();
                comboActivity.setDisabled(true);
            }
        });
        comboSubProcess.setDisabled(false);
    },

    _onSelectSubProcessRisk: function (cbo) {
        var panel = cbo.up('ViewWindowRegisterRisk');
        var comboProcess = panel.down('ViewComboProcess');
        var comboActivity = panel.down('ViewComboActivity');
        comboActivity.getStore().load({
            url: 'http://localhost:9000/giro/showListActivityActives.htm',
            params: {
                valueFind: comboProcess.getValue(),
                valueFind2: cbo.getValue()
            },
            callback: function (cbo) {
                comboActivity.reset();
            }
        });
        comboActivity.setDisabled(false);
    },

    _onSaveRiskOperationalDetail: function (button) {
        var win = button.up('ViewWindowRegisterRisk');
        var panel = Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0];
        var grid = this.getViewPanelIdentifyRiskOperational().down('grid');
        if (win.down('form').getForm().isValid()) {
            DukeSource.lib.Ajax.request({
                method: 'POST',
                url: 'http://localhost:9000/giro/' + button.link + '.htm?nameView=PanelProcessRiskEvaluation',
                params: {
                    jsonData: Ext.JSON.encode(win.down('form').getValues())
                },
                success: function (response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        var idRisk = response.idRisk;

                        if (button.link === 'saveRisk') {
                            Ext.MessageBox.show({
                                title: DukeSource.global.GiroMessages.TITLE_WARNING,
                                msg: 'Desea Iniciar con la evaluación del riesgo?',
                                icon: Ext.Msg.QUESTION,
                                buttonText: {
                                    yes: "Si",
                                    no: "No"
                                },
                                buttons: Ext.MessageBox.YESNO,
                                fn: function (btn) {
                                    if (btn === 'yes') {
                                        var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowVersionEvaluationRisk', {
                                            modal: true
                                        });
                                        window.down('textfield[name=idRisk]').setValue(idRisk);
                                        window.show();
                                    } else if (btn === 'no') {
                                        panel.down('#buttonEvaluation').setText('INICIAR EVALUACIÓN');
                                        panel.down('#buttonEvaluation').setVisible(true);
                                    }
                                }
                            });
                        }
                        DukeSource.global.DirtyView.messageNormal(response.message);
                        win.doClose();

                        grid.getStore().load({
                            callback: function () {
                                var gridColumns = grid.getStore().getRange();
                                for (var i = 0; i < gridColumns.length; i++) {
                                    if (gridColumns[i].data['idRisk'] === idRisk.toString()) {
                                        grid.getSelectionModel().select(i);
                                        break;
                                    }
                                }
                            }
                        });
                        panel.down('#buttonEvaluation').setVisible(false);
                        panel.down('#buttonIr').setVisible(false);
                        panel.down('ViewComboGridEvaluationRisk').setVisible(false);

                    } else {
                        DukeSource.global.DirtyView.messageWarning(response.message);
                    }
                }
            });
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }

    },

    _onIdentifyRiskOperationalGrid: function (me, record) {
        var panel = this.getViewPanelIdentifyRiskOperational();
        DukeSource.lib.Ajax.request({
            method: 'POST',
            url: 'http://localhost:9000/giro/existVersionEvaluation.htm',
            params: {
                idRisk: record.get('idRisk')
            },
            scope: this,
            success: function (response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                    if (response.data === 'N') {
                        panel.down('#buttonEvaluation').setText('Iniciar evaluación');
                        panel.down('#buttonEvaluation').setVisible(true);
                        panel.down('#buttonIr').setVisible(false);
                        panel.down('ViewComboGridEvaluationRisk').setVisible(false);
                    } else if (response.data === 'S') {
                        panel.down('#buttonEvaluation').setText('Nueva evaluación');
                        panel.down('#buttonEvaluation').setVisible(true);
                        panel.down('#buttonIr').setVisible(true);
                        var combo = panel.down('ViewComboGridEvaluationRisk').setVisible(true);
                        combo.getStore().load({
                            url: 'http://localhost:9000/giro/showVersionEvaluationActives.htm',
                            params: {
                                idRisk: record.get('idRisk')
                            },
                            callback: function (records) {
                                Ext.Array.each(records, function (rec) {
                                    combo.setValue(record.data['versionCorrelative']);
                                });
                            }
                        });
                        comboRiskEvaluation = combo;
                    }
                }
            },
            failure: function () {
            }
        })
    },

    _onStartEvaluation: function (me) {
        var grid = Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0].down('#riskIdentify');
        var rec = grid.getSelectionModel().getSelection()[0];

        if (rec === undefined) {
            DukeSource.global.DirtyView.warningAlert(DukeSource.global.GiroMessages.MESSAGE_ITEM);
        } else {
            if (me.text === 'Iniciar evaluación') {
                var win = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowVersionEvaluationRisk', {
                    modal: true,
                    record: rec
                });
                win.down('textfield[name=idRisk]').setValue(rec.get('idRisk'));
                win.show();
            } else if (me.text === 'Nueva evaluación') {
                if (rec.get('valueLostResidual') === '') {
                    DukeSource.global.DirtyView.warningAlert('Click en ir a evaluación para culminar esta, luego ingresar una nueva');
                } else {
                    var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowAddVersionEvaluationRisk', {
                        modal: true,
                        record: rec
                    });
                    window.down('textfield[name=idRisk]').setValue(rec.get('idRisk'));
                    window.show();
                }
            }
        }
    },

    _onComboGridSelection: function (combo) {
        var id = combo.getValue();
        var record = combo.findRecord(combo.valueField || combo.displayField, id);
        Ext.Ajax.request({
            method: 'POST',
            url: 'http://localhost:9000/giro/consultAnyVersionEvaluation.htm',
            params: {
                idVersionEvaluation: record.data['idVersionEvaluation'],
                idRisk: record.data['idRisk']
            },
            scope: this,
            success: function (response) {
                response = Ext.decode(response.responseText);
                var gridIdentify = this.getViewPanelIdentifyRiskOperational().down('grid');
                var debilityIndex = DukeSource.global.DirtyView.getSelectedRowIndex(gridIdentify);
                gridIdentify.getSelectionModel().getSelection()[0].set("evaluationFinalControl", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("valueLostResidual", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("descriptionFrequency", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("descriptionImpact", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("descriptionScaleRisk", '');
                gridIdentify.getSelectionModel().getSelection()[0].set("levelColour", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("colourLost", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("valueRiskInherent", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("percentageReductionControl", '');
                gridIdentify.getSelectionModel().getSelection()[0].set("scoreDescriptionControl", '');
                gridIdentify.getSelectionModel().getSelection()[0].set("colourControl", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("idFrequencyReduction", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("idImpactReduction", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("idFrequencyResidual", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("descriptionFrequencyResidual", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("idImpactResidual", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("descriptionFrequencyResidual", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("descriptionImpactResidual", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("descriptionScaleResidual", '');
                gridIdentify.getSelectionModel().getSelection()[0].set("levelColourResidual", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("colourLostResidual", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("valueLostResidual", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("idFrequency", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("equivalentFrequency", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("idImpact", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("equivalentImpact", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("idFrequencyFeature", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("idImpactFeature", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("frequencyReduction", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("impactReduction", null);
                gridIdentify.getSelectionModel().getSelection()[0].set("maxAmount", '');
                gridIdentify.getSelectionModel().getSelection()[0].set(response.data[0]);
                gridIdentify.getSelectionModel().select(debilityIndex);


            },
            failure: function () {
            }
        });
    },

    _onButtonGoEvaluation: function (row) {

        var panelIdentify = this.getViewPanelIdentifyRiskOperational();
        var gridIdentify = panelIdentify.down('#riskIdentify');
        var rowIdentify = row.action !== undefined ? gridIdentify.getSelectionModel().getSelection()[0] : row;

        DukeSource.lib.Ajax.request({
            method: 'POST',
            url: 'http://localhost:9000/giro/findInformationInherent.htm',
            params: {
                idRisk: rowIdentify.get('idRisk'),
                idVersionEvaluation: rowIdentify.get('versionCorrelative')
            },
            success: function (response) {
                response = Ext.decode(response.responseText);

                function settingColor(a, color) {
                    a.setFieldStyle('background-color: #' + color + ';');
                }

                function loadPanelEvaluation() {
                    DukeSource.global.DirtyView.verifyLoadController('DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelEvaluationRiskOperational');

                    Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0].setDisabled(true);

                    var panelEval = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelEvaluationRiskOperational', {
                        rowIdentify: rowIdentify,
                        isVersionActive: rowIdentify.get('isVersionActive'),
                        title: 'Evaluación de riesgo &#8702; 2',
                        closable: false,
                        border: false,
                        idTypeMatrix: rowIdentify.get('typeMatrix'),
                        idRisk: rowIdentify.get('idRisk'),
                        versionCorrelative: rowIdentify.get('versionCorrelative')
                    });

                    panelEval.down('ViewComboTypeMatrix').getStore().load({
                            callback: function () {
                                if (TYPE_MATRIX_SIZE === DukeSource.global.GiroConstants.ONE_S) {
                                    panelEval.down('ViewComboTypeMatrix').setValue(TYPE_MATRIX_DEFAULT);
                                }
                            }
                        }
                    );

                    if (rowIdentify.get('typeMatrix') !== '') {
                        panelEval.down('ViewComboFrequency').getStore().load({
                            params: {
                                idTypeMatrix: rowIdentify.get('typeMatrix'),
                                idOperationalRiskExposition: rowIdentify.get('idOperationalRiskExposition')
                            }
                        });
                        panelEval.down('ViewComboImpact').getStore().load({
                            params: {
                                idTypeMatrix: rowIdentify.get('typeMatrix'),
                                idOperationalRiskExposition: rowIdentify.get('idOperationalRiskExposition')
                            }
                        });

                        var fri = panelEval.down('#formRiskInherint');

                        fri.down('#valueRiskInherentForm').setValue(rowIdentify.get('valueRiskInherent'));
                        fri.down('#amountEventLost').setValue(rowIdentify.get('amountEventLost'));
                        fri.down('#numberEventLost').setValue(rowIdentify.get('numberEventLost'));

                        settingColor(fri.down('#valueRiskInherentForm'), evalInherent['colourLost']);
                        settingColor(fri.down('#descriptionScaleRisk'), evalInherent['colourLost']);
                        settingColor(panelEval.down('#evaluationFinalControl'), rowIdentify.get('colourControl'));
                        settingColor(panelEval.down('#valueRiskInherent'), rowIdentify.get('colourLost'));
                        settingColor(panelEval.down('#valueLostResidual'), rowIdentify.get('colourLostResidual'));
                        settingColor(panelEval.down('#valueTargetRisk'), rowIdentify.get('colorTargetRisk'));

                        panelEval.down('#valueRiskInherent').setFieldLabel('Nivel - ' + rowIdentify.get('descriptionScaleRisk'));
                        panelEval.down('#valueLostResidual').setFieldLabel('Nivel - ' + rowIdentify.get('descriptionScaleResidual'));
                        panelEval.down('#valueTargetRisk').setFieldLabel('Nivel - ' + rowIdentify.get('descriptionScaleTargetRisk'));


                        fri.getForm().setValues(evalInherent);
                        fri.down('#reasonImpact').setValue(rowIdentify.get('reasonImpact'));
                        fri.down('#reasonFrequency').setValue(rowIdentify.get('reasonFrequency'));

                    }
                    panelEval.down('#headFormEvaluationRisk').getForm().loadRecord(rowIdentify);
                    DukeSource.getApplication().centerPanel.addPanel(panelEval);

                }

                if (response.success) {
                    var evalInherent = Ext.JSON.decode(response.evaluationInherentData);
                    loadPanelEvaluation();
                }
                else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                }
            }
        });
    },
    _onButtonGoPreviousEvaluation: function (btn) {
        if (Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0].origin === 'actionPlan' ||
            Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0].origin === 'events' ||
            Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0].origin === 'resumeMatrix') {
            Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0].destroy();
        } else {

            var panelIdentify = Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0];
            var callerPanel = Ext.ComponentQuery.query(panelIdentify.callerPanel)[0];

            callerPanel.setDisabled(false);
            DukeSource.getApplication().centerPanel.editorTabPanelMain.setActiveTab(callerPanel);
            panelIdentify.destroy();
        }

    },

    _onGoOutRiskOperationalDetail: function (button) {
        button.up('ViewPanelIdentifyRiskOperational').destroy();
    },

    rightClickIdentifyRisk: function (view, rec, node, index, e) {

        var app = this.application;
        e.stopEvent();
        var addMenu = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.AddMenu', {});
        addMenu.removeAll();
        Ext.Ajax.request({
            method: 'POST',
            url: 'http://localhost:9000/giro/existVersionEvaluation.htm',
            params: {
                idRisk: rec.get('idRisk')
            },
            scope: this,
            success: function (response) {
                response = Ext.decode(response.responseText);
                var generalPanel = Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0];
                var gridRisk = generalPanel.down('grid[name=riskIdentify]');

                if (response.success) {
                    if (response.data === 'N') {
                        addMenu.add(
                            {
                                text: 'Iniciar versi&oacute;n', iconCls: 'newEval',
                                handler: function () {

                                    var win = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowVersionEvaluationRisk', {
                                        modal: true,
                                        record: rec
                                    });
                                    win.down('textfield[name=idRisk]').setValue(rec.get('idRisk'));
                                    win.show();
                                }
                            },
                            {
                                text: 'Gestionar Adjuntos',
                                iconCls: 'gestionfile',
                                action: 'attachReportToRisk'
                            },
                            {
                                text: 'Modificar',
                                iconCls: 'modify',
                                handler: function () {
                                    app.getController('DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelIdentifyRiskOperational')._onModifyRiskOperational();
                                }
                            },
                            {
                                text: 'Eliminar', iconCls: 'delete', handler: function () {
                                    Ext.MessageBox.show({
                                        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                                        msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
                                        icon: Ext.Msg.QUESTION,
                                        buttonText: {
                                            yes: "Si"
                                        },
                                        buttons: Ext.MessageBox.YESNO,
                                        fn: function (btn) {
                                            if (btn === 'yes') {
                                                deleteRisk(rec, gridRisk);
                                            }
                                        }
                                    });
                                }
                            }
                        );
                        addMenu.showAt(e.getXY());
                    } else if (response.data === 'S') {
                        addMenu.add(
                            {
                                text: 'Tratamiento',
                                iconCls: 'treatment',
                                handler: function () {

                                    if (rec.get('idImpact') === null || rec.get('descriptionScaleResidual') === '') {
                                        DukeSource.global.DirtyView.messageWarning('Antes de Continuar, complete la Evaluación por favor');
                                    } else {
                                        var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowTreatment', {
                                            modal: true,
                                            idRisk: rec.get('idRisk'),
                                            originTreatment: 'IDENTIFICATION'
                                        });
                                        if (rec.get('idTreatmentRisk') === '') {
                                            window.down('#idTreatmentRisk').setValue('id');
                                            window.down('textfield[name=idRisk]').setValue(rec.get('idRisk'));
                                            window.down('textfield[name=versionCorrelative]').setValue(rec.get('versionCorrelative'));
                                            window.down('ViewComboRiskAnswer').getStore().load({
                                                callback: function () {
                                                    window.show();
                                                }
                                            });

                                        } else {
                                            Ext.Ajax.request({
                                                method: 'POST',
                                                url: 'http://localhost:9000/giro/findTreatmentRiskById.htm',
                                                params: {
                                                    idTreatmentRisk: rec.get('idTreatmentRisk')
                                                },
                                                success: function (response) {
                                                    response = Ext.decode(response.responseText);
                                                    if (response.success) {
                                                        window.down('ViewComboRiskAnswer').getStore().load({
                                                            callback: function () {
                                                                window.down('form').getForm().setValues(response.data);
                                                                window.show();
                                                            }
                                                        });

                                                    } else {
                                                        DukeSource.global.DirtyView.messageWarning(response.message);
                                                    }
                                                },
                                                failure: function () {
                                                }
                                            });
                                        }
                                    }
                                }
                            },
                            {
                                text: 'Aprobación',
                                iconCls: 'evaluate',
                                handler: function () {
                                    var win = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.WindowStateRisk', {
                                        modal: true,
                                        idRisk: rec.get('idRisk'),
                                        grid: view
                                    });
                                    win.show();
                                }
                            },
                            {
                                text: 'Modificar',
                                iconCls: 'modify',
                                handler: function () {
                                    app.getController('DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelIdentifyRiskOperational')._onModifyRiskOperational();
                                }
                            },
                            {
                                text: 'Gestionar Adjuntos',
                                iconCls: 'gestionfile',
                                action: 'attachReportToRisk'
                            },
                            {
                                text: 'Eliminar',
                                hidden: rec.get('sequenceStateRisk') !== "0",
                                iconCls: 'delete',
                                handler: function () {
                                    if (rec.get('sequenceStateRisk') === "0") {
                                        Ext.MessageBox.show({
                                            title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                                            msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
                                            icon: Ext.Msg.QUESTION,
                                            buttonText: {
                                                yes: "Si"
                                            },
                                            buttons: Ext.MessageBox.YESNO,
                                            fn: function (btn) {
                                                if (btn === 'yes') {
                                                    deleteRisk(rec, gridRisk);
                                                }
                                            }
                                        });
                                    } else {
                                        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_RISK_DELETE_INVALID);
                                    }

                                }
                            }
                        );
                        addMenu.showAt(e.getXY());
                    }

                } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                }
            },
            failure: function () {
            }
        });

    },

    _onAttachReportToRisk: function () {
        var generalPanel = Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0];
        var gridRisk = generalPanel.down('grid[name=riskIdentify]');
        var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowManagementReportRisk', {
            modal: true
        });
        window.down('textfield[name=idRisk]').setValue(gridRisk.getSelectionModel().getSelection()[0].get('idRisk'));
        window.show();
    },

    _onDownloadReportToRisk: function () {
        var gralPanel = Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0];
        var gridRisk = gralPanel.down('grid[name=riskIdentify]');
        var window = Ext.create('DukeSource.view.risk.EvaluatorRiskOperational.windows.ViewWindowDownloadReportToRisk', {
            modal: true
        });
        window.down('textfield[name=idRisk]').setValue(gridRisk.getSelectionModel().getSelection()[0].get('idRisk'));
        window.show();
    },
    _onSaveNewDebility: function (btn) {
        var win = btn.up('window');
        var grid = Ext.ComponentQuery.query('ViewWindowSearchWeakness grid')[0];
        if (win.down('form').getForm().isValid()) {
            DukeSource.lib.Ajax.request({
                method: 'POST',
                url: 'http://localhost:9000/giro/saveWeakness.htm?nameView=ViewPanelRegisterWeakness',
                params: {
                    jsonData: Ext.JSON.encode(win.down('form').getValues())
                },
                success: function (response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        DukeSource.global.DirtyView.messageNormal(response.message);
                        DukeSource.global.DirtyView.searchPaginationGridNormal('', grid, grid.down('pagingtoolbar'), 'http://localhost:9000/giro/findWeakness.htm', 'description', 'idWeakness');
                        win.close();
                    } else {
                        DukeSource.global.DirtyView.messageWarning(response.message);
                    }
                },
                failure: function () {
                }
            });
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }
    },
    _onSaveTreatmentRiskOperational: function (button) {
        var evaluationPanel = Ext.ComponentQuery.query('ViewPanelEvaluationRiskOperational')[0];

        var panel = button.up('window');
        if (panel.down('form').getForm().isValid()) {
            DukeSource.lib.Ajax.request({
                waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                method: 'POST',
                url: 'http://localhost:9000/giro/saveTreatmentRisk.htm?nameView=PanelProcessRiskEvaluation',
                params: {
                    idRisk: panel.idRisk,
                    jsonData: Ext.JSON.encode(panel.down('form').getValues())
                },
                scope: this,
                success: function (response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        DukeSource.global.DirtyView.messageNormal(response.message);

                        if (panel.originTreatment === 'IDENTIFICATION') {
                            panel.close();
                            var gridIdentify = Ext.ComponentQuery.query('ViewPanelIdentifyRiskOperational')[0].down('#riskIdentify');
                            var riskIndex = DukeSource.global.DirtyView.getSelectedRowIndex(gridIdentify);
                            gridIdentify.getStore().load({
                                callback: function () {
                                    gridIdentify.getSelectionModel().select(riskIndex);
                                }
                            });
                        } else {
                            evaluationPanel.down('#descriptionTreatment').setValue(panel.down('ViewComboRiskAnswer').getRawValue());
                            evaluationPanel.down('#idTreatmentRisk').setValue(response.data);
                            panel.close();
                        }

                    } else {
                        DukeSource.global.DirtyView.messageWarning(response.message);
                    }
                },
                failure: function () {
                }
            });
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }
    }
});

function deleteRisk(rec, gridRisk) {
    DukeSource.lib.Ajax.request({
        method: 'POST',
        url: 'http://localhost:9000/giro/deleteRisk.htm?nameView=PanelProcessRiskEvaluation',
        params: {
            idRisk: rec.get('idRisk')
        },
        success: function (response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
                DukeSource.global.DirtyView.messageNormal(response.message);
                gridRisk.getStore().load();
            }
            else {
                DukeSource.global.DirtyView.messageWarning(response.message);
            }
        },
        failure: function () {

        }
    });
}