Ext.define(
  "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelMapGeneral",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboTypeRiskEvaluation",
      "risk.parameter.combos.ViewComboBusinessLineOne"
    ],
    init: function() {
      this.control({
        "[action=generateReportChartGeneralMapPdf]": {
          click: this._onGenerateReportChartGeneralMapPdf
        },
        "[action=generateReportChartGeneralMapXls]": {
          click: this._onGenerateReportChartGeneralMapXls
        }
      });
    },

    _onGenerateReportChartGeneralMapPdf: function(btn) {
      var container = btn.up("container[name=generalContainer]");
      var businessLineOne =
        container.down("ViewComboBusinessLineOne").getValue() == undefined
          ? "T"
          : container.down("ViewComboBusinessLineOne").getValue();
      var typeEvaluation = container
        .down("ViewComboTypeRiskEvaluation")
        .getValue();
      var value = businessLineOne + ";" + typeEvaluation;
      var panelReport = container.down("panel[name=panelReport]");
      panelReport.removeAll();
      panelReport.add({
        xtype: "component",
        autoEl: {
          tag: "iframe",
          src:
            "http://localhost:9000/giro/pdfGeneralReport.htm?values=" +
            value +
            "&names=" +
            "idBusinessLine,typeEvaluation" +
            "&types=" +
            "String,String" +
            "&nameReport=" +
            "RORIEV0002" +
            "&typeReport=" +
            "noStandard"
        }
      });
    },
    _onGenerateReportChartGeneralMapXls: function(btn) {
      var container = btn.up("container[name=generalContainer]");
      var businessLineOne =
        container.down("ViewComboBusinessLineOne").getValue() == undefined
          ? "T"
          : container.down("ViewComboBusinessLineOne").getValue();
      var typeEvaluation = container
        .down("ViewComboTypeRiskEvaluation")
        .getValue();
      var value = businessLineOne + ";" + typeEvaluation;
      var panelReport = container.down("panel[name=panelReport]");
      panelReport.removeAll();
      panelReport.add({
        xtype: "component",
        autoEl: {
          tag: "iframe",
          src:
            "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
            value +
            "&names=" +
            "idBusinessLine,typeEvaluation" +
            "&types=" +
            "String,String" +
            "&nameReport=" +
            "RORIEV0002" +
            "&typeReport=" +
            "noStandard"
        }
      });
    }
  }
);
