Ext.define(
  "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelMatrixMap",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboOperationalRiskExposition",
      "risk.parameter.combos.ViewComboTypeMatrix",
      "risk.parameter.combos.ViewComboProcess",
      "risk.parameter.combos.ViewComboSubProcess",
      "risk.parameter.combos.ViewComboBusinessLineOne"
    ],
    init: function() {
      this.control({
        "[action=identifiedMapRiskInMatrix]": {
          click: this._onIdentifiedMapRiskInMatrix
        },
        "ViewPanelMatrixMap grid": {
          celldblclick: this._onViewDetailMapSquare
        }
      });
    },
    _onIdentifiedMapRiskInMatrix: function(me) {
      var panel = me.up("ViewPanelMatrixMap");
      var form = panel.down("form");
      if (form.getForm().isValid()) {
        var typeMatrix = panel.down("ViewComboTypeMatrix").getValue();
        var businessLineOne = panel.down("ViewComboBusinessLineOne").getValue();
        var process = panel.down("ViewComboProcess").getValue();
        var subProcess = panel.down("ViewComboSubProcess").getValue();
        var stateRisk = panel.down("#stateRisk").getValue();
        var grid = panel.down("grid[name=matrixGeneral]");
        var valueExposition = panel.down("ViewComboOperationalRiskExposition");
        var typeRisk =
          panel.down("#checkInherent").getValue() == true ? "RI" : "RR";
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/getAllRiskMapFilter.htm?nameView=ViewPanelMatrixMap",
          params: {
            maxExposition: panel
              .down("ViewComboOperationalRiskExposition")
              .getValue(),
            valueMaxExposition: valueExposition.getRawValue(),
            typeMatrix: typeMatrix,
            businessLineOne: businessLineOne,
            process: process,
            subProcess: subProcess,
            typeRisk: typeRisk,
            stateRisk: stateRisk
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            grid.getView().refresh();
            if (response.success) {
              var data = Ext.JSON.decode(response.data);
              for (var i = 0; i < data.length; i++) {
                var valueMatrix = data[i]["valueMatrix"];
                var codeRisk = data[i]["codeRisk"];
                var positionX = data[i]["positionX"] + 1;
                var positionY = data[i]["positionY"];
                var cell = grid
                  .getView()
                  .getCellByPosition({ row: positionY, column: positionX });
                var value = cell.dom.textContent;
                if ((value = Ext.util.Format.number(valueMatrix, "0,0.00"))) {
                  if (data[i]["typeValue"] == "RI") {
                    cell.insertHtml(
                      "afterBegin",
                      '<button type="button" class="buttonRisk">' +
                        valueMatrix +
                        "</button>",
                      true
                    );
                  } else if (data[i]["typeValue"] == "RR") {
                    cell.insertHtml(
                      "afterBegin",
                      '<button type="button" class="buttonRisk black text-blanco text-shadow-negra">' +
                        valueMatrix +
                        "</button>",
                      true
                    );
                  }
                }
              }
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
                Ext.Msg.ERROR
              );
            }
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onViewDetailMapSquare: function(
      me,
      td,
      cellIndex,
      record,
      tr,
      rowIndex,
      e,
      eOpts
    ) {
      var clickedDataIndex = me.panel.headerCt.getHeaderAtIndex(cellIndex)
        .dataIndex;
      var clickedCellValue = record.get(clickedDataIndex);
      var k;
      if (
        Ext.ComponentQuery.query("ViewPanelIdentifyRiskOperational")[0] ==
        undefined
      ) {
        DukeSource.global.DirtyView.verifyLoadController(
          "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelPreviousIdentifyRiskOperational"
        );
        DukeSource.global.DirtyView.verifyLoadController(
          "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelIdentifyRiskOperational"
        );
        var k = Ext.create(
          "DukeSource.view.risk.EvaluatorRiskOperational.ViewPanelIdentifyRiskOperational",
          {
            title: "EVRO",
            origin: "resumeMatrix",
            closable: true,
            border: false
          }
        );
        DukeSource.getApplication().centerPanel.addPanel(k);
      } else {
        k = Ext.ComponentQuery.query("ViewPanelIdentifyRiskOperational")[0];
        DukeSource.getApplication().centerPanel.addPanel(k);
      }

      k.down("grid").setTitle("EVALUACI&Oacute;N DE RIESGOS");
      var grid = k.down("grid");

      var panel = Ext.ComponentQuery.query("ViewPanelMatrixMap")[0];
      var businessLineOne =
        panel.down("ViewComboBusinessLineOne").getValue() == undefined
          ? ""
          : panel.down("ViewComboBusinessLineOne").getValue();
      var process =
        panel.down("ViewComboProcess").getValue() == undefined
          ? ""
          : panel.down("ViewComboProcess").getValue();
      var subProcess =
        panel.down("ViewComboSubProcess").getValue() == undefined
          ? ""
          : panel.down("ViewComboSubProcess").getValue();
      var limit = ",";
      var typeRisk =
        panel.down("#checkInherent").getValue() == true
          ? "ma.idMatrix"
          : "madet.idMatrix";
      grid.store.getProxy().extraParams = {
        fields:
          typeRisk +
          limit +
          "r.businessLineThree.id.idBusinessLineOne" +
          limit +
          "r.subProcess.id.process" +
          limit +
          "r.subProcess.id.idSubProcess",
        values:
          clickedCellValue.idMatrix +
          limit +
          businessLineOne +
          limit +
          process +
          limit +
          subProcess,
        types:
          "Long" + limit + "Integer" + limit + "Integer" + limit + "Integer"
      };
      grid.store.getProxy().url =
        "http://localhost:9000/giro/getDetailMatrixInSquare.htm";
      grid.down("pagingtoolbar").moveFirst();
    }
  }
);
