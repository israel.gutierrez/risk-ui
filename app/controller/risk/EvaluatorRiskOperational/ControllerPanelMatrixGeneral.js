Ext.define(
  "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelMatrixGeneral",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboOperationalRiskExposition",
      "risk.parameter.combos.ViewComboTypeMatrix",
      "risk.parameter.combos.ViewComboProcess",
      "risk.parameter.combos.ViewComboSubProcess"
    ],
    init: function() {
      this.control({
        "[action=identifiedRiskInMatrix]": {
          click: this._onIdentifiedRiskInMatrix
        }
      });
    },
    _onIdentifiedRiskInMatrix: function(me) {
      var panel = me.up("ViewPanelMatrixGeneral");
      var form = panel.down("form");
      if (form.getForm().isValid()) {
        var typeMatrix = panel.down("ViewComboTypeMatrix").getValue();
        var businessLineOne = panel.down("#idBusinessLineOne").getValue();
        var businessLineTwo = panel.down("#idBusinessLineTwo").getValue();
        var process = panel.down("ViewComboProcess").getValue();
        var subProcess = panel.down("ViewComboSubProcess").getValue();
        var grid = panel.down("grid[name=matrixGeneral]");
        var valueExposition = panel.down("ViewComboOperationalRiskExposition");
        var typeRisk =
          panel.down("#checkInherent").getValue() == true ? "RI" : "RR";
        var stateRisk = panel.down("#stateRisk").getValue();
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/getMatrixGeneralFilter.htm?nameView=ViewPanelMatrixGeneral",
          params: {
            maxExposition: panel
              .down("ViewComboOperationalRiskExposition")
              .getValue(),
            valueMaxExposition: valueExposition.getRawValue(),
            typeMatrix: typeMatrix,
            businessLineOne: businessLineOne,
            businessLineTwo: businessLineTwo,
            process: process,
            subProcess: subProcess,
            typeRisk: typeRisk,
            stateRisk: stateRisk
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            grid.getView().refresh();
            if (response.success) {
              var data = Ext.JSON.decode(response.data);
              for (var i = 0; i < data.length; i++) {
                var valueMatrix = data[i]["valueMatrix"];
                var codeRisk = data[i]["codeRisk"];
                var positionX = data[i]["positionX"] + 1;
                var positionY = data[i]["positionY"];
                var cell = grid
                  .getView()
                  .getCellByPosition({ row: positionY, column: positionX });

                if (cell.dom != undefined) {
                  var value = cell.dom.textContent;
                  if ((value = Ext.util.Format.number(valueMatrix, "0,0.00"))) {
                    if (data[i]["typeValue"] == "RI") {
                      cell.insertHtml(
                        "afterBegin",
                        '<button type="button" class="buttonRisk text-negroCodeRisk">' +
                          codeRisk +
                          "</button>",
                        true
                      );
                    } else if (data[i]["typeValue"] == "RR") {
                      cell.insertHtml(
                        "afterBegin",
                        '<button type="button" class="buttonRisk black text-blancoCodeRisk text-shadow-negra">' +
                          codeRisk +
                          "</button>",
                        true
                      );
                    }
                  }
                }
              }
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    }
  }
);
