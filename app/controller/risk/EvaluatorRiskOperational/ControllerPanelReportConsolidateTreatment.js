Ext.define(
  "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelReportConsolidateTreatment",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboTypeRiskEvaluation",
      "risk.parameter.combos.ViewComboBusinessLineOne"
    ],
    init: function() {
      this.control({
        "[action=generateReportConsolidateTreatmentPdf]": {
          click: this._onGenerateReportConsolidateTreatmentPdf
        },
        "[action=generateReportConsolidateTreatmentXls]": {
          click: this._onGenerateReportConsolidateTreatmentXls
        }
      });
    },

    _onGenerateReportConsolidateTreatmentPdf: function(btn) {
      var container = btn.up("container[name=generalContainer]");
      var businessLineOne =
        container.down("ViewComboBusinessLineOne").getValue() == undefined
          ? "T"
          : container.down("ViewComboBusinessLineOne").getValue();
      var typeEvaluation = container
        .down("ViewComboTypeRiskEvaluation")
        .getValue();
      var value =
        container.down("datefield[name=dateInit]").getRawValue() +
        ";" +
        container.down("datefield[name=dateEnd]").getRawValue() +
        ";" +
        businessLineOne +
        ";" +
        typeEvaluation;
      var panelReport = container.down("panel[name=panelReport]");
      panelReport.removeAll();
      panelReport.add({
        xtype: "component",
        autoEl: {
          tag: "iframe",
          src:
            "http://localhost:9000/giro/pdfGeneralReport.htm?values=" +
            value +
            "&names=" +
            "dateInit,dateEnd," +
            "businessLineOne,typeEvaluation" +
            "&types=" +
            "Date,Date,String,String" +
            "&nameReport=" +
            "RORIEV0007" +
            "&typeReport=" +
            "noStandard"
        }
      });
    },
    _onGenerateReportConsolidateTreatmentXls: function(btn) {
      var container = btn.up("container[name=generalContainer]");
      var businessLineOne =
        container.down("ViewComboBusinessLineOne").getValue() == undefined
          ? "T"
          : container.down("ViewComboBusinessLineOne").getValue();
      var typeEvaluation = container
        .down("ViewComboTypeRiskEvaluation")
        .getValue();
      var value =
        container.down("datefield[name=dateInit]").getRawValue() +
        ";" +
        container.down("datefield[name=dateEnd]").getRawValue() +
        ";" +
        businessLineOne +
        ";" +
        typeEvaluation;
      var panelReport = container.down("panel[name=panelReport]");
      panelReport.removeAll();
      panelReport.add({
        xtype: "component",
        autoEl: {
          tag: "iframe",
          src:
            "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
            value +
            "&names=" +
            "dateInit,dateEnd," +
            "businessLineOne,typeEvaluation" +
            "&types=" +
            "Date,Date,String,String" +
            "&nameReport=" +
            "RORIEV0007XLS" +
            "&typeReport=" +
            "noStandard"
        }
      });
    }
  }
);
