Ext.define(
  "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelReportRiskInherintResidual",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboTypeRiskEvaluation",
      "risk.parameter.combos.ViewComboProcessType",
      "risk.parameter.combos.ViewComboProcess"
    ],
    init: function() {
      this.control({
        "[action=generateReportRiskInheritResidualPdf]": {
          click: this._onGenerateReportRiskInheritResidualPdf
        },
        "[action=generateReportRiskInheritResidualXls]": {
          click: this._onGenerateReportRiskInheritResidualXls
        }
      });
    },

    _onGenerateReportRiskInheritResidualPdf: function(btn) {
      var container = btn.up("container[name=generalContainer]");
      var typeProcess =
        container.down("ViewComboProcessType").getValue() == undefined
          ? "T"
          : container.down("ViewComboProcessType").getValue();
      var process =
        container.down("ViewComboProcess").getValue() == undefined
          ? "T"
          : container.down("ViewComboProcess").getValue();
      var typeEvaluation = container
        .down("ViewComboTypeRiskEvaluation")
        .getValue();
      var value =
        container.down("datefield[name=dateInit]").getRawValue() +
        ";" +
        container.down("datefield[name=dateEnd]").getRawValue() +
        ";" +
        typeProcess +
        ";" +
        process +
        ";" +
        typeEvaluation;
      var panelReport = container.down("panel[name=panelReport]");
      panelReport.removeAll();
      panelReport.add({
        xtype: "component",
        autoEl: {
          tag: "iframe",
          src:
            "http://localhost:9000/giro/pdfGeneralReport.htm?values=" +
            value +
            "&names=" +
            "dateInit,dateEnd," +
            "typeProcess,process,typeEvaluation" +
            "&types=" +
            "Date,Date,String,String,String" +
            "&nameReport=" +
            "RORIEV0005" +
            "&typeReport=" +
            "noStandard"
        }
      });
    },
    _onGenerateReportRiskInheritResidualXls: function(btn) {
      var container = btn.up("container[name=generalContainer]");
      var typeProcess =
        container.down("ViewComboProcessType").getValue() == undefined
          ? "T"
          : container.down("ViewComboProcessType").getValue();
      var process =
        container.down("ViewComboProcess").getValue() == undefined
          ? "T"
          : container.down("ViewComboProcess").getValue();
      var typeEvaluation = container
        .down("ViewComboTypeRiskEvaluation")
        .getValue();
      var value =
        container.down("datefield[name=dateInit]").getRawValue() +
        ";" +
        container.down("datefield[name=dateEnd]").getRawValue() +
        ";" +
        typeProcess +
        ";" +
        process +
        ";" +
        typeEvaluation;
      var panelReport = container.down("panel[name=panelReport]");
      panelReport.removeAll();
      panelReport.add({
        xtype: "component",
        autoEl: {
          tag: "iframe",
          src:
            "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
            value +
            "&names=" +
            "dateInit,dateEnd," +
            "typeProcess,process,typeEvaluation" +
            "&types=" +
            "Date,Date,String,String,String" +
            "&nameReport=" +
            "RORIEV0005XLS" +
            "&typeReport=" +
            "noStandard"
        }
      });
    }
  }
);
