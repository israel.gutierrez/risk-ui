Ext.define(
  "DukeSource.controller.risk.EvaluatorRiskOperational.ControllerPanelReportsRisk",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: ["risk.parameter.combos.ViewComboBusinessLineOne"],
    init: function() {
      this.control({
        "[action=generateReportsRisk]": {
          click: this.executeEvolutionRisk
        }
      });
    },
    executeEvolutionRisk: function(btn) {
      var form = btn.up("form");
      if (form.getForm().isValid()) {
        var view = Ext.ComponentQuery.query("ViewPanelReportsRisk")[0];

        var nameDownload = view.down("#nameFile").getValue();
        var nameReport = view.down("#nameReport").getValue();

        var maxExposition =
          form.down("#maxExposition").getValue() == null
            ? DukeSource.global.GiroConstants.TEXT_T
            : form.down("#maxExposition").getValue();
        var typeMatrix =
          form.down("#typeMatrix").getValue() == null
            ? DukeSource.global.GiroConstants.TEXT_T
            : form.down("#typeMatrix").getValue();

        var product =
          form.down("#product").getValue() === ""
            ? DukeSource.global.GiroConstants.CODE_ROOT
            : form.down("#product").getValue();
        var idProcessType =
          form.down("#idProcessType").getValue() === ""
            ? DukeSource.global.GiroConstants.TEXT_T
            : form.down("#idProcessType").getValue();
        var idProcess =
          form.down("#idProcess").getValue() === ""
            ? DukeSource.global.GiroConstants.TEXT_T
            : form.down("#idProcess").getValue();
        var idSubProcess =
          form.down("#idSubProcess").getValue() === ""
            ? DukeSource.global.GiroConstants.TEXT_T
            : form.down("#idSubProcess").getValue();
        var idActivity =
          form.down("#idActivity").getValue() === ""
            ? DukeSource.global.GiroConstants.TEXT_T
            : form.down("#idActivity").getValue();

        var dateInit =
          form.down("#dateInit").getRawValue() === ""
            ? DukeSource.global.GiroConstants.DATE_INIT
            : form.down("#dateInit").getRawValue();
        var dateEnd =
          form.down("#dateEnd").getRawValue() === ""
            ? DukeSource.global.GiroConstants.DATE_END
            : form.down("#dateEnd").getRawValue();

        var containerReport = view.down("#containerReport");

        containerReport.removeAll();
        containerReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            hidden: true,
            src:
              "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
              maxExposition +
              "," +
              typeMatrix +
              "," +
              product +
              "," +
              idProcessType +
              "," +
              idProcess +
              "," +
              idSubProcess +
              "," +
              idActivity +
              "," +
              dateInit +
              "," +
              dateEnd +
              "&names=" +
              "maxExposition,typeMatrix,product,processType,process,subProcess,activity,dateInit,dateEnd" +
              "&types=" +
              "String,String,Integer,String,String,String,String,Date,Date" +
              "&nameReport=" +
              nameReport +
              "&nameDownload=" +
              nameDownload
          }
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
      }
    }
  }
);
