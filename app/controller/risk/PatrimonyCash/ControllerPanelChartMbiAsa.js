Ext.define(
  "DukeSource.controller.risk.PatrimonyCash.ControllerPanelChartMbiAsa",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboYear",
      "risk.parameter.combos.ViewComboMonth",
      //        ,'risk.parameter.combos.ViewComboTitleReport'
      "risk.PatrimonyCash.ViewChartAsaMbi"
    ],
    init: function() {
      this.control({
        "[action=generateChartComparativeMbiAsa]": {
          click: this._onGenerateChartComparativeMbiAsa
        },
        "[action=generateExcelComparativeMbiAsa]": {
          click: this._onGenerateExcelComparativeMbiAsa
        }
      });
    },
    _onGenerateChartComparativeMbiAsa: function(button) {
      var panel = button.up("ViewPanelChartMbiAsa");
      var container = panel.down("container[name=containerChart]");
      var url = "";
      if (panel.down("combobox[name=idTitleReport]").getValue() == "2") {
        url = "http://localhost:9000/giro/findListEquityCapital.htm";
      } else {
        url = "http://localhost:9000/giro/findListEquityCapitalByPeriod.htm";
      }
      Ext.Ajax.request({
        method: "POST",
        url: url,
        params: {
          yearStart:
            panel.down("ViewComboYear[name=yearStart]") == undefined
              ? ""
              : panel.down("ViewComboYear[name=yearStart]").getValue(),
          monthStart:
            panel.down("ViewComboMonth[name=monthStart]") == undefined
              ? ""
              : panel.down("ViewComboMonth[name=monthStart]").getValue(),
          yearFinal:
            panel.down("ViewComboYear[name=yearFinal]") == undefined
              ? ""
              : panel.down("ViewComboYear[name=yearFinal]").getValue(),
          monthFinal:
            panel.down("ViewComboMonth[name=monthFinal]") == undefined
              ? ""
              : panel.down("ViewComboMonth[name=monthFinal]").getValue()
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            container.removeAll();
            container.add({ xtype: "ViewChartAsaMbi", padding: "2 2 2 2" });
            var chart = panel.down("chart");
            var grid = panel.down("grid");
            chart.getStore().loadData(response.data);
            grid.getStore().removeAll();
            grid.getStore().loadRawData(response.data, true);
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },
    _onGenerateExcelComparativeMbiAsa: function(button) {
      var panel = button.up("ViewPanelChartMbiAsa");
      if (
        panel
          .down("form")
          .getForm()
          .isValid()
      ) {
        var yearStart =
          panel.down("ViewComboYear[name=yearStart]") == undefined
            ? ""
            : panel.down("ViewComboYear[name=yearStart]").getValue();
        var monthStart =
          panel.down("ViewComboMonth[name=monthStart]") == undefined
            ? ""
            : panel.down("ViewComboMonth[name=monthStart]").getValue();
        var yearFinal =
          panel.down("ViewComboYear[name=yearFinal]") == undefined
            ? ""
            : panel.down("ViewComboYear[name=yearFinal]").getValue();
        var monthFinal =
          panel.down("ViewComboMonth[name=monthFinal]") == undefined
            ? ""
            : panel.down("ViewComboMonth[name=monthFinal]").getValue();
        var value = yearStart + monthStart + "," + yearFinal + monthFinal;

        Ext.core.DomHelper.append(document.body, {
          tag: "iframe",
          id: "downloadIframe",
          frameBorder: 0,
          width: 0,
          height: 0,
          css: "display:none;visibility:hidden;height:0px;",
          src:
            "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
            value +
            "&names=perFirst,perEnd" +
            "&types=String,String" +
            "&nameReport=ROPATM0002XLS"
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    }
  }
);
