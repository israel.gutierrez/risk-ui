Ext.define(
  "DukeSource.controller.risk.PatrimonyCash.ControllerPanelGenerationReportRegulatory",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboYear",
      "risk.parameter.combos.ViewComboMonth",
      "risk.User.combos.ViewComboAgency",
      "risk.parameter.combos.ViewComboTitleReport",
      "DukeSource.view.risk.PatrimonyCash.grids.ViewGridMIB",
      "DukeSource.view.risk.PatrimonyCash.grids.ViewGridASA"
    ],
    init: function() {
      this.control({
        "[action=generateReportRegulatory]": {
          click: this._onGenerateReportRegulatory
        },
        "[action=generateExcelReportRegulatory]": {
          click: this._onGenerateExcelReportRegulatory
        },
        "[action=generateTxtReportRegulatory]": {
          click: this._onGenerateTxtReportRegulatory
        }
      });
    },
    _onGenerateReportRegulatory: function(button) {
      var panel = button.up("ViewPanelGenerationReportRegulatory");
      var container = panel.down("container[name=containerGenerateReport]");
      var report = panel.down("ViewComboTitleReport");
      if (
        panel
          .down("form")
          .getForm()
          .isValid()
      ) {
        if (report.getRawValue() == "ASA") {
          container.removeAll();
          container.add({ xtype: "ViewGridASA", padding: "2 2 2 2" });
          panel
            .down("grid")
            .getStore()
            .removeAll();
        } else if (report.getRawValue() == "MIB") {
          container.removeAll();
          container.add({ xtype: "ViewGridMIB", padding: "2 2 2 2" });
          panel
            .down("grid")
            .getStore()
            .removeAll();
        }
        DukeSource.lib.Ajax.request({
          method: "POST",
          waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
          url: "http://localhost:9000/giro/buildJsonDetailEquityCapital.htm",
          params: {
            idTitleReport: panel.down("ViewComboTitleReport").getValue(),
            yearUntil: panel.down("ViewComboYear").getValue(),
            monthUntil: panel.down("ViewComboMonth").getValue()
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              panel
                .down("grid")
                .getStore()
                .removeAll();
              panel
                .down("grid")
                .getStore()
                .loadRawData(JSON.parse(response.data), true);
              var value = JSON.parse(response.effectiveEquity);
              panel
                .down("UpperCaseTextFieldReadOnly[name=equity]")
                .setValue(Ext.util.Format.number(value["equity"], "0,0.00"));
              panel
                .down("UpperCaseTextFieldReadOnly[name=weightedAssets]")
                .setValue(
                  Ext.util.Format.number(value["weightedAssets"], "0,0.00")
                );
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onGenerateExcelReportRegulatory: function(button) {
      var panel = button.up("ViewPanelGenerationReportRegulatory");
      if (
        panel
          .down("form")
          .getForm()
          .isValid()
      ) {
        Ext.core.DomHelper.append(document.body, {
          tag: "iframe",
          id: "downloadIframe",
          frameBorder: 0,
          width: 0,
          height: 0,
          css: "display:none;visibility:hidden;height:0px;",
          src:
            "http://localhost:9000/giro/generateReportDetailEquityCapital.htm?idTitleReport=" +
            panel.down("ViewComboTitleReport").getValue() +
            "&yearUntil=" +
            panel.down("ViewComboYear").getValue() +
            "&monthUntil=" +
            panel.down("ViewComboMonth").getValue()
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onGenerateTxtReportRegulatory: function(button) {
      var panel = button.up("ViewPanelGenerationReportRegulatory");
      if (
        panel
          .down("form")
          .getForm()
          .isValid()
      ) {
        Ext.core.DomHelper.append(document.body, {
          tag: "iframe",
          id: "downloadIframe",
          frameBorder: 0,
          width: 0,
          height: 0,
          css: "display:none;visibility:hidden;height:0px;",
          src:
            "http://localhost:9000/giro/createTxtEquityCapital.htm?idTitleReport=" +
            panel.down("ViewComboTitleReport").getValue() +
            "&yearUntil=" +
            panel.down("ViewComboYear").getValue() +
            "&monthUntil=" +
            panel.down("ViewComboMonth").getValue()
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    }
  }
);
