Ext.define(
  "DukeSource.controller.risk.PatrimonyCash.ControllerPanelLoadBalanceAscertainment",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: ["risk.parameter.combos.ViewComboMonth"],
    init: function() {
      this.control({
        "[action=loadBalanceAscertainment]": {
          click: this._onLoadBalanceAscertainment
        },
        "[action=selectBalanceAscertainment]": {
          click: this._onSelectBalanceAscertainment
        },
        "[action=searchBalanceAscertainment]": {
          click: this._onSearchBalanceAscertainment
        },
        "[action=deleteBalanceAscertainment]": {
          click: this.onDeleteBalanceAscertainment
        },
        "ViewPanelLoadBalanceAscertainment grid[itemId=gridBalanceLoad]": {
          itemcontextmenu: this.rightClickBalanceLoad
        }
      });
    },

    _onLoadBalanceAscertainment: function(btn) {
      var panel = Ext.ComponentQuery.query(
        "ViewPanelLoadBalanceAscertainment"
      )[0];
      var grid = panel.down("grid");
      var win = btn.up("ViewWindowLoadBalanceAscertainment");
      win.down("fileuploadfield").allowBlank = false;
      if (
        win
          .down("form")
          .getForm()
          .isValid()
      ) {
        win
          .down("form")
          .getForm()
          .submit({
            waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
            url:
              "http://localhost:9000/giro/saveFormatGeneral.htm?nameView=ViewPanelLoadBalanceAscertainment",
            method: "POST",
            success: function(form, action) {
              var valor = Ext.decode(action.response.responseText);
              if (valor.success) {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_MESSAGE,
                  valor.mensaje,
                  Ext.Msg.INFO
                );
                grid.store.getProxy().extraParams = {
                  month: win.down("ViewComboMonth").getValue(),
                  year: win.down("TextFieldNumberFormatObligatory").getValue()
                };
                grid.store.getProxy().url =
                  "http://localhost:9000/giro/listTrialBalance.htm";
                grid.down("pagingtoolbar").moveFirst();
                grid.setTitle(
                  "BALANCE CARGADO DE " +
                    win.down("ViewComboMonth").getRawValue() +
                    " " +
                    win.down("TextFieldNumberFormatObligatory").getValue()
                );
                Ext.ComponentQuery.query(
                  "ViewPanelLoadBalanceAscertainment grid[itemId=gridBalanceLoad]"
                )[0]
                  .getStore()
                  .load();
                win.close();
              } else {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_ERROR,
                  valor.mensaje,
                  Ext.Msg.ERROR
                );
              }
            },
            failure: function(form, action) {
              var valor = Ext.decode(action.response.responseText);
              if (!valor.success) {
                DukeSource.global.DirtyView.messageAlert(
                  DukeSource.global.GiroMessages.TITLE_ERROR,
                  valor.mensaje,
                  Ext.Msg.ERROR
                );
              }
            }
          });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onSelectBalanceAscertainment: function(btn) {
      var panel = Ext.ComponentQuery.query(
        "ViewPanelLoadBalanceAscertainment"
      )[0];
      var grid = panel.down("grid");
      var win = btn.up("ViewWindowLoadBalanceAscertainment");
      win.down("TextFieldNumberFormatObligatory").allowBlank = false;
      win.down("ViewComboMonth").allowBlank = false;
      win.down("fileuploadfield").allowBlank = true;
      if (
        win
          .down("form")
          .getForm()
          .isValid()
      ) {
        grid.store.getProxy().extraParams = {
          month: win.down("ViewComboMonth").getValue(),
          year: win.down("TextFieldNumberFormatObligatory").getValue()
        };
        grid.store.getProxy().url =
          "http://localhost:9000/giro/listTrialBalance.htm";
        grid.down("pagingtoolbar").moveFirst();
        grid.setTitle(
          "BALANCE DE " +
            win.down("ViewComboMonth").getRawValue() +
            " " +
            win.down("TextFieldNumberFormatObligatory").getValue()
        );
        win.close();
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.WARNING
        );
      }
    },
    _onSearchBalanceAscertainment: function(btn) {
      var panel = btn.up("ViewPanelLoadBalanceAscertainment");
      if (panel.down("#yearBalance").getValue() == "") {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          "Ingrese el A&Ntilde;O",
          Ext.Msg.ERROR
        );
      } else {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelLoadBalanceAscertainment grid[itemId=gridBalanceLoad]"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          panel.down("#yearBalance"),
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findFormatGeneral.htm",
          "year",
          "year"
        );
      }
    },
    onDeleteBalanceAscertainment: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelLoadBalanceAscertainment grid[itemId=gridBalanceLoad]"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteFormatGeneral.htm?nameView=ViewPanelLoadBalanceAscertainment"
      );
    },
    rightClickBalanceLoad: function(view, record, item, index, e) {
      //        var app = this.application;
      var grid = Ext.ComponentQuery.query(
        "ViewPanelLoadBalanceAscertainment grid[itemId=gridBalanceAscertainment]"
      )[0];
      e.stopEvent();
      var rowMenu = Ext.create("Ext.menu.Menu", {
        width: 140,
        items: [
          {
            text: "Consultar",
            iconCls: "search", //descriptionMonth
            handler: function() {
              grid.store.getProxy().extraParams = {
                month: record.get("month"),
                year: record.get("year")
              };
              grid.store.getProxy().url =
                "http://localhost:9000/giro/listTrialBalance.htm";
              grid.down("pagingtoolbar").moveFirst();
              grid.setTitle(
                "BALANCE DE " +
                  record.get("descriptionMonth") +
                  " " +
                  record.get("year")
              );
            }
          }
        ]
      });
      rowMenu.showAt(e.getXY());
    }
  }
);
