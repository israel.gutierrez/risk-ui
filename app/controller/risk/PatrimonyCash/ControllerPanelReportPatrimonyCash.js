Ext.define(
  "DukeSource.controller.risk.PatrimonyCash.ControllerPanelReportPatrimonyCash",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      //        'risk.parameter.combos.ViewComboEventOne',
      //        'risk.parameter.combos.ViewComboEventTwo',
      //        'risk.User.combos.ViewComboAgency',
      //        'risk.parameter.combos.ViewComboBusinessLineOne',
      //        'risk.parameter.combos.ViewComboBusinessLineTwo',
      //        'risk.parameter.combos.ViewComboProcess',
      //        'risk.parameter.combos.ViewComboSubProcess',
      //        'risk.parameter.combos.ViewComboWorkArea',
      //        'risk.DatabaseEventsLost.combos.ViewComboGroupIncident'
    ],
    init: function() {
      this.control({
        "[action=generateReportPatrimonyCashPdf]": {
          click: this._onGenerateReportPatrimonyCashPdf
        },
        "[action=generateReportPatrimonyCashXls]": {
          click: this._onGenerateReportPatrimonyCashXls
        }
      });
    },

    _onGenerateReportPatrimonyCashPdf: function(btn) {
      var container = btn.up("container[name=generalContainer]");
      var dateInit = container.down("datefield[name=dateInit]").getValue();
      var dateEnd = container.down("datefield[name=dateEnd]").getValue();
      var dateInitConcat =
        dateInit.getFullYear() + "" + (dateInit.getMonth() + 1);
      var dateEndConcat = dateEnd.getFullYear() + "" + (dateEnd.getMonth() + 1);
      var value = dateInitConcat + "," + dateEndConcat;
      var panelReport = container.down("panel[name=panelReport]");
      panelReport.removeAll();
      panelReport.add({
        xtype: "component",
        autoEl: {
          tag: "iframe",
          src:
            "http://localhost:9000/giro/pdfGeneralReport.htm?values=" +
            value +
            "&names=" +
            "anioMesInit,anioMesEnd" +
            "&types=" +
            "Integer,Integer" +
            "&nameReport=" +
            "ROPATM0001"
        }
      });
    },
    _onGenerateReportPatrimonyCashXls: function(btn) {
      var container = btn.up("container[name=generalContainer]");
      var dateInit = container.down("datefield[name=dateInit]").getValue();
      var dateEnd = container.down("datefield[name=dateEnd]").getValue();
      var dateInitConcat =
        dateInit.getFullYear() + "" + (dateInit.getMonth() + 1);
      var dateEndConcat = dateEnd.getFullYear() + "" + (dateEnd.getMonth() + 1);
      var value = dateInitConcat + "," + dateEndConcat;
      var panelReport = container.down("panel[name=panelReport]");
      panelReport.removeAll();
      panelReport.add({
        xtype: "component",
        autoEl: {
          tag: "iframe",
          src:
            "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
            value +
            "&names=" +
            "anioMesInit,anioMesEnd" +
            "&types=" +
            "Integer,Integer" +
            "&nameReport=" +
            "ROPATM0001XLS"
        }
      });
    }
  }
);
