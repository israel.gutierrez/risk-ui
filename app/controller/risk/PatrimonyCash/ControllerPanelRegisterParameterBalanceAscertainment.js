Ext.define(
  "DukeSource.controller.risk.PatrimonyCash.ControllerPanelRegisterParameterBalanceAscertainment",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.PatrimonyCash.grids.StoreTreeGridPanelParameterBalanceAscertainment"
    ],
    models: [
      "risk.PatrimonyCash.grids.ModelTreeGridPanelParameterBalanceAscertainment"
    ],
    views: [
      "risk.PatrimonyCash.grids.ViewTreeGridPanelParameterBalanceAscertainment"
    ],
    init: function() {
      this.control({
        "[action=newRegisterTitleParamBalance]": {
          click: this._onNewRegisterTitleParamBalance
        },
        "[action=saveTitleBalanceAscertainment]": {
          click: this._onSaveTitleBalanceAscertainment
        },
        ViewTreeGridPanelParameterBalanceAscertainment: {
          itemcontextmenu: this.treeRightClickBalanceAscertainment
        },
        "AddMenuGroupReport menuitem[text=Agregar]": {
          click: this._addGroupReport
        },
        "AddMenuGroupReport menuitem[text=Adjuntar]": {
          click: this._menuAttachReport
        },
        "[action=attachReport]": {
          click: this._onAttachReport
        },
        "AddMenuGroupReport menuitem[text=Editar]": {
          //
          click: this._editTitleReport
        },
        "AddMenuGroupReport menuitem[text=Eliminar]": {
          click: this._deleteGroupReport
        },
        "AddMenuSubGroupReport menuitem[text=Agregar]": {
          click: this._addSubGroupReport
        },
        "AddMenuSubGroupReport menuitem[text=Formula]": {
          click: this._formSubGroupReport
        },
        "AddMenuSubGroupReport menuitem[text=Editar]": {
          click: this._editGroupReport
        },
        "AddMenuSubGroupReport menuitem[text=Eliminar]": {
          click: this._deleteGroupReport
        },
        "AddMenuItemReport menuitem[text=Agregar]": {
          click: this._addItemReport
        },
        "AddMenuItemReport menuitem[text=Editar]": {
          click: this._editSubGroupReport
        },
        "AddMenuItemReport menuitem[text=Eliminar]": {
          click: this._deleteGroupReport
        },
        "EditMenuItemReport menuitem[text=Editar]": {
          click: this._editItemReport
        },
        "EditMenuItemReport menuitem[text=Eliminar]": {
          click: this._deleteGroupReport
        }
      });
    },
    _onNewRegisterTitleParamBalance: function() {
      var window = Ext.create(
        "DukeSource.view.risk.PatrimonyCash.windows.ViewWindowTitleReport",
        {
          modal: true
        }
      );
      //        window.show();
      window
        .down("form")
        .getComponent("id")
        .setValue("myTree/id");
      window.show();
    },
    _editTitleReport: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.PatrimonyCash.windows.ViewWindowTitleReport",
        {
          modal: true
        }
      );
      countryWindow
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id"));
      countryWindow
        .down("form")
        .getComponent("text")
        .setValue(this.application.currentRecord.get("text"));
      countryWindow.show();
    },
    _editGroupReport: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.PatrimonyCash.windows.ViewWindowGroup",
        {
          modal: true
        }
      );
      countryWindow
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id"));
      countryWindow
        .down("form")
        .getComponent("text")
        .setValue(this.application.currentRecord.get("text"));
      countryWindow.show();
    },
    _editSubGroupReport: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.PatrimonyCash.windows.ViewWindowSubGroup",
        {
          modal: true
        }
      );
      countryWindow
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id"));
      countryWindow
        .down("form")
        .getComponent("text")
        .setValue(this.application.currentRecord.get("text"));
      countryWindow
        .down("form")
        .getComponent("formula")
        .setValue(this.application.currentRecord.get("formula"));
      countryWindow
        .down("form")
        .getComponent("level")
        .setValue(this.application.currentRecord.get("level"));
      countryWindow
        .down("form")
        .getComponent("sign")
        .setValue(this.application.currentRecord.get("sign"));
      countryWindow.show();
    },
    _editItemReport: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.PatrimonyCash.windows.ViewWindowItem",
        {
          modal: true
        }
      );
      countryWindow
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id"));
      countryWindow
        .down("form")
        .getComponent("text")
        .setValue(this.application.currentRecord.get("text"));
      countryWindow.show();
    },
    _onSaveTitleBalanceAscertainment: function(button) {
      var window = button.up("window");
      Ext.Ajax.request({
        method: "POST",
        url:
          "http://localhost:9000/giro/saveTitleReport.htm?nameView=ViewPanelRegisterParameterBalanceAscertainment",
        params: {
          jsonData: Ext.JSON.encode(
            window
              .down("form")
              .getForm()
              .getValues()
          )
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              response.mensaje,
              Ext.Msg.INFO
            );
            var refreshNode = Ext.ComponentQuery.query(
              "ViewPanelRegisterParameterBalanceAscertainment ViewTreeGridPanelParameterBalanceAscertainment"
            )[0]
              .getStore()
              .getNodeById(response.data);
            refreshNode.removeAll(false);
            Ext.ComponentQuery.query(
              "ViewPanelRegisterParameterBalanceAscertainment ViewTreeGridPanelParameterBalanceAscertainment"
            )[0]
              .getStore()
              .load({
                node: refreshNode
              });
            window.close();
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    },
    treeRightClickBalanceAscertainment: function(view, record, item, index, e) {
      //stop the default action
      e.stopEvent();
      //save the current selected record
      this.application.currentRecord = record;
      //if the node is a region let user add a country
      if (record.get("depth") === 1) {
        var addMenu = Ext.create(
          "DukeSource.view.risk.PatrimonyCash.AddMenuGroupReport",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      if (record.get("depth") === 2) {
        var addMenu = Ext.create(
          "DukeSource.view.risk.PatrimonyCash.AddMenuSubGroupReport",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      if (record.get("depth") === 3) {
        var addMenu = Ext.create(
          "DukeSource.view.risk.PatrimonyCash.AddMenuItemReport",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      if (record.get("depth") === 4) {
        var addMenu = Ext.create(
          "DukeSource.view.risk.PatrimonyCash.EditMenuItemReport",
          {}
        );
        addMenu.showAt(e.getXY());
      }
      return false;
    },
    _addGroupReport: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.PatrimonyCash.windows.ViewWindowGroup",
        {
          modal: true
        }
      );
      countryWindow
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id") + "/id");
      countryWindow.show();
    },

    _menuAttachReport: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.PatrimonyCash.windows.ViewWindowAttachReport",
        {
          modal: true
        }
      );
      countryWindow
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id") + "/id");
      countryWindow.show();
    },
    _onAttachReport: function(button) {
      var window = button.up("window");
      var form = window.down("form");
      if (form.getForm().isValid()) {
        form.getForm().submit({
          url:
            "http://localhost:9000/giro/saveFileExcelReport.htm?nameView=ViewPanelRegisterParameterBalanceAscertainment",
          method: "POST",
          success: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (valor.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                valor.mensaje,
                Ext.Msg.INFO
              );
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                valor.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (!valor.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                valor.mensaje,
                Ext.Msg.ERROR
              );
            }
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _addSubGroupReport: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.PatrimonyCash.windows.ViewWindowSubGroup",
        {
          modal: true
        }
      );
      countryWindow
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id") + "/id");
      countryWindow.show();
    },
    _formSubGroupReport: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.PatrimonyCash.windows.ViewWindowFormCalculation",
        {
          modal: true
        }
      ).show();
    },
    _addItemReport: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.PatrimonyCash.windows.ViewWindowItem",
        {
          modal: true
        }
      );
      countryWindow
        .down("form")
        .getComponent("id")
        .setValue(this.application.currentRecord.get("id") + "/id");
      countryWindow.show();
    },
    _deleteGroupReport: function() {
      Ext.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/deleteTitleReport.htm",
        params: {
          id: this.application.currentRecord.get("id")
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_MESSAGE,
              response.mensaje,
              Ext.Msg.INFO
            );
            var refreshNode = Ext.ComponentQuery.query(
              "ViewPanelRegisterParameterBalanceAscertainment ViewTreeGridPanelParameterBalanceAscertainment"
            )[0]
              .getStore()
              .getNodeById(response.data);
            refreshNode.removeAll(false);
            Ext.ComponentQuery.query(
              "ViewPanelRegisterParameterBalanceAscertainment ViewTreeGridPanelParameterBalanceAscertainment"
            )[0]
              .getStore()
              .load({
                node: refreshNode
              });
          } else {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_WARNING,
              response.mensaje,
              Ext.Msg.ERROR
            );
          }
        },
        failure: function() {}
      });
    }
  }
);
