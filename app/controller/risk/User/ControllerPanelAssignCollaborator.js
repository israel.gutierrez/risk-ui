Ext.define(
  "DukeSource.controller.risk.User.ControllerPanelAssignCollaborator",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.util.search.AdvancedSearchUser",
      "risk.User.combos.ViewComboAgency",
      "risk.parameter.combos.ViewComboWorkArea",
      "risk.parameter.combos.ViewComboJobPlace"
    ],
    init: function() {
      this.control({
        "ViewPanelAssignCollaborator grid[name=gridManagerAssign]": {
          itemclick: this._onClickGridManagerAssign
        },
        "ViewPanelAssignCollaborator grid": {
          itemcontextmenu: this.rightClickShowManager
        },
        "[action=addManagerAssign]": {
          click: this._addManager
        },
        "[action=addCollaboratorAssign]": {
          click: this._addCollaborator
        },
        "[action=deleteManagerAssign]": {
          click: this._deleteManager
        },
        "[action=deleteCollaboratorAssign]": {
          click: this._deleteCollaborator
        },
        "[action=saveManagerToAnalyst]": {
          click: this._onSaveManagerToAnalyst
        },
        "[action=saveCollaboratorToManager]": {
          click: this._onSaveCollaboratorToManager
        },
        "[action=filterGridManagerAssign]": {
          keyup: this._onFilterGridManagerAssign
        },
        "[action=filterGridShowManager]": {
          keyup: this._onFilterGridShowManager
        },
        "[action=filterGridShowCollaborator]": {
          keyup: this._onFilterGridShowCollaborator
        },
        "[action=filterGridManagerAvailable]": {
          keyup: this._onFilterGridManagerAvailable
        },
        "[action=filterGridCollaboratorAvailable]": {
          keyup: this._onFilterGridCollaboratorAvailable
        },
        "[action=searchCollaboratorAssign]": {
          specialkey: this._onSearchCollaboratorAssign
        },
        "[action=searchCollaboratorOfBoss]": {
          specialKey: this._onFindCollaboratorAssigned
        },
        "[action=searchAdvancedUser]": {
          click: this._searchAdvancedUser
        }
      });
    },

    _onClickGridManagerAssign: function(grid) {
      var viewPanel = Ext.ComponentQuery.query(
        "ViewPanelAssignCollaborator"
      )[0];
      var record = grid.getSelectionModel().getSelection()[0];
      viewPanel.down("#buttShowManager").setDisabled(true);
      viewPanel.down("#buttShowCollaborator").setDisabled(true);
      if (record != null) {
        if (record.get("category") == "ANALISTA") {
          viewPanel.down("#buttShowManager").setDisabled(false);
        } else if (record.get("category") == "GESTOR") {
          viewPanel.down("#buttShowCollaborator").setDisabled(false);
        }
      }
    },
    _addManager: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.User.windows.ViewWindowAssignGestor",
        {
          modal: true
        }
      );
      countryWindow.show();
    },
    _addCollaborator: function() {
      var countryWindow = Ext.create(
        "DukeSource.view.risk.User.windows.ViewWindowAssignCollaborator",
        {
          modal: true
        }
      );
      countryWindow.show();
    },
    _deleteManager: function() {
      var viewPanel = Ext.ComponentQuery.query(
        "ViewPanelAssignCollaborator"
      )[0];
      var viewManager = Ext.ComponentQuery.query("ViewWindowShowManagers")[0];
      //var gridPanel = viewPanel.down('#gridManagerAssign');
      var gridManager = viewManager.down("#gridShowManager");
      var recordPanel = viewPanel
        .down("#gridManagerAssign")
        .getSelectionModel()
        .getSelection()[0];
      var recordManager = gridManager.getSelectionModel().getSelection()[0];
      if (recordManager != undefined) {
        Ext.MessageBox.show({
          title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
          msg:
            "Esta seguro que desea continuar?" +
            "<br> tenga en cuenta que se eliminaran tambien, todos los <br> " +
            "colaboradores asociados a este gestor",
          icon: Ext.Msg.QUESTION,
          buttonText: {
            yes: "Si"
          },
          buttons: Ext.MessageBox.YESNO,
          fn: function(btn) {
            if (btn == "yes") {
              Ext.Ajax.request({
                method: "POST",
                url: "http://localhost:9000/giro/deleteCollaborator.htm",
                params: {
                  analyst: recordPanel.get("userName"),
                  manager: recordManager.get("userName"),
                  category: "GESTOR"
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      response.mensaje,
                      Ext.Msg.INFO
                    );
                    gridManager.store.getProxy().url =
                      "http://localhost:9000/giro/showListCollaboratorActives.htm";
                    gridManager.store.getProxy().extraParams = {
                      userName: recordPanel.get("userName"),
                      category: "GESTOR"
                    };
                    gridManager.down("pagingtoolbar").moveFirst();
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      response.mensaje,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function() {}
              });
            }
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          "Selecione por favor un REGISTRO",
          Ext.Msg.ERROR
        );
      }
    },
    _deleteCollaborator: function() {
      var viewPanel = Ext.ComponentQuery.query(
        "ViewPanelAssignCollaborator"
      )[0];
      var viewManager = Ext.ComponentQuery.query("ViewWindowShowManagers")[0];
      var viewMe = Ext.ComponentQuery.query("ViewWindowShowCollaborators")[0];
      var gridPanel = viewPanel.down("#gridManagerAssign");
      var gridCollaborator = viewMe.down("#gridShowCollaborator");
      var recordPanel = gridPanel.getSelectionModel().getSelection()[0];
      var recordCollaborator = gridCollaborator
        .getSelectionModel()
        .getSelection()[0];
      var userManager;
      if (recordPanel.get("category") == "GESTOR") {
        userManager = recordPanel.get("userName");
      } else {
        var gridManager = viewManager
          .down("#gridShowManager")
          .getSelectionModel()
          .getSelection()[0];
        userManager = gridManager.get("userName");
      }
      if (recordCollaborator != undefined) {
        Ext.MessageBox.show({
          title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
          msg: "Esta seguro que desea continuar?",
          icon: Ext.Msg.QUESTION,
          buttonText: {
            yes: "Si"
          },
          buttons: Ext.MessageBox.YESNO,
          fn: function(btn) {
            if (btn == "yes") {
              Ext.Ajax.request({
                method: "POST",
                url: "http://localhost:9000/giro/deleteCollaborator.htm",
                params: {
                  manager: userManager,
                  collaborator: recordCollaborator.get("userName"),
                  category: "COLABORADOR"
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_MESSAGE,
                      response.mensaje,
                      Ext.Msg.INFO
                    );
                    recordCollaborator.store.getProxy().url =
                      "http://localhost:9000/giro/showListCollaboratorActives.htm";
                    recordCollaborator.store.getProxy().extraParams = {
                      userName: userManager,
                      category: "COLABORADOR"
                    };
                    gridCollaborator.down("pagingtoolbar").moveFirst();
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_WARNING,
                      response.mensaje,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function() {}
              });
            }
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          "Selecione por favor un REGISTRO",
          Ext.Msg.ERROR
        );
      }
    },
    _onSaveManagerToAnalyst: function(button) {
      var window = button.up("window");
      var records = window
        .down("grid")
        .getSelectionModel()
        .getSelection();
      var arrayDataToSave = [];

      var me = Ext.ComponentQuery.query("ViewPanelAssignCollaborator")[0];
      var meWindow = Ext.ComponentQuery.query("ViewWindowShowManagers")[0];
      var grid = me
        .down("#gridManagerAssign")
        .getSelectionModel()
        .getSelection()[0];
      Ext.Array.each(records, function(record) {
        arrayDataToSave.push(record.data);
      });
      if (arrayDataToSave.length > 0) {
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/saveCollaborator.htm",
          params: {
            listCollaborator: Ext.JSON.encode(arrayDataToSave),
            boss: grid.get("userName"),
            category: "GESTOR"
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                response.mensaje,
                Ext.Msg.INFO
              );
              meWindow
                .down("#gridShowManager")
                .down("pagingtoolbar")
                .moveFirst();
              window.close();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          "Selecione por favor un REGISTRO",
          Ext.Msg.ERROR
        );
      }
    },
    _onSaveCollaboratorToManager: function(button) {
      var window = button.up("window");
      var records = window
        .down("grid")
        .getSelectionModel()
        .getSelection();
      var arrayDataToSave = new Array();

      var viewPanel = Ext.ComponentQuery.query(
        "ViewPanelAssignCollaborator"
      )[0];
      var meWindow = Ext.ComponentQuery.query("ViewWindowShowCollaborators")[0];
      var grid = meWindow.down("#gridShowCollaborator");
      var recordPanel = viewPanel
        .down("#gridManagerAssign")
        .getSelectionModel()
        .getSelection()[0];
      var userManager;
      if (recordPanel.get("category") == "GESTOR") {
        userManager = recordPanel.get("userName");
      } else {
        var viewManager = Ext.ComponentQuery.query("ViewWindowShowManagers")[0];
        var recordManager = viewManager
          .down("#gridShowManager")
          .getSelectionModel()
          .getSelection()[0];
        userManager = recordManager.get("userName");
      }

      Ext.Array.each(records, function(record) {
        arrayDataToSave.push(record.data);
      });
      if (arrayDataToSave.length > 0) {
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/saveCollaborator.htm",
          params: {
            listCollaborator: Ext.JSON.encode(arrayDataToSave),
            boss: userManager,
            category: "COLABORADOR"
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                response.mensaje,
                Ext.Msg.INFO
              );
              grid.down("pagingtoolbar").moveFirst();
              window.close();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          "Selecione por favor un REGISTRO",
          Ext.Msg.ERROR
        );
      }
    },
    rightClickShowManager: function(view, record, item, index, e) {
      e.stopEvent();
      var me = Ext.ComponentQuery.query("ViewPanelAssignCollaborator")[0];
      var addMenu = Ext.create("DukeSource.view.risk.User.AddMenuGestor", {});
      addMenu.removeAll();
      if (record.get("category") == "ANALISTA") {
        addMenu.add({
          text: "VER GESTORES",
          iconCls: "manager",
          handler: function() {
            var countryWindow = Ext.create(
              "DukeSource.view.risk.User.windows.ViewWindowShowManagers",
              {
                modal: true
              }
            );
            countryWindow.show();
          }
        });
      } else if (record.get("category") == "GESTOR") {
        addMenu.add(
          {
            text: "VER ANALISTA",
            iconCls: "manager",
            handler: function() {
              findBossCollaboratorAssign();
            }
          },
          {
            text: "VER COLABORADORES",
            iconCls: "collaborator",
            handler: function() {
              var countryWindow = Ext.create(
                "DukeSource.view.risk.User.windows.ViewWindowShowCollaborators",
                {
                  modal: true
                }
              );
              countryWindow.show();
            }
          }
        );
      } else if (record.get("category") == "COLABORADOR") {
        addMenu.add({
          text: "VER GESTOR",
          iconCls: "manager",
          handler: function() {
            findBossCollaboratorAssign();
          }
        });
      }

      addMenu.showAt(e.getXY());
    },
    _onFilterGridManagerAssign: function(text) {
      var me = Ext.ComponentQuery.query("ViewPanelAssignCollaborator")[0];
      var grid = me.down("#gridManagerAssign");
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onFilterGridShowManager: function(text) {
      var me = Ext.ComponentQuery.query("ViewWindowShowManagers")[0];
      var grid = me.down("#gridShowManager");
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onFilterGridShowCollaborator: function(text) {
      var me = Ext.ComponentQuery.query("ViewWindowShowCollaborators")[0];
      var grid = me.down("#gridShowCollaborator");
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onFilterGridManagerAvailable: function(text) {
      var me = Ext.ComponentQuery.query("ViewWindowAssignGestor")[0];
      var grid = me.down("grid");
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onFilterGridCollaboratorAvailable: function(text) {
      var me = Ext.ComponentQuery.query("ViewWindowAssignCollaborator")[0];
      var grid = me.down("grid");
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onSearchCollaboratorAssign: function(field, e) {
      var me = Ext.ComponentQuery.query("ViewPanelAssignCollaborator")[0];
      if (e.getKey() === e.ENTER) {
        var grid = me.down("#gridManagerAssign");
        grid.store.getProxy().url =
          "http://localhost:9000/giro/findCollaboratorByName.htm";
        grid.store.getProxy().extraParams = {
          fullName: field.getValue()
        };
        grid.down("pagingtoolbar").moveFirst();
      } else {
      }
    },
    _onFindCollaboratorAssigned: function(field, e) {
      var me = Ext.ComponentQuery.query("ViewWindowShowManagers")[0];
      if (e.getKey() === e.ENTER) {
        var grid = me.down("#gridShowManager");
        var record = grid.getSelectionModel().getSelection()[0];
        grid.store.getProxy().url =
          "http://localhost:9000/giro/findCollaboratorByManager.htm";
        grid.store.getProxy().extraParams = {
          fullName: field.getValue(),
          userName: record.get("userName")
        };
        grid.down("pagingtoolbar").moveFirst();
      } else {
      }
    },
    _searchAdvancedUser: function(btn) {
      var form = btn.up("form");
      var grid = Ext.ComponentQuery.query(
        "ViewPanelAssignCollaborator grid"
      )[0];
      DukeSource.global.DirtyView.findAdvancedUserGeneric(form, grid);
    }
  }
);

function findBossCollaboratorAssign() {
  var me = Ext.ComponentQuery.query("ViewPanelAssignCollaborator")[0];
  var recordPanel = me
    .down("#gridManagerAssign")
    .getSelectionModel()
    .getSelection()[0];

  Ext.Ajax.request({
    method: "POST",
    url: "http://localhost:9000/giro/findBossCollaboratorAssign.htm",
    params: {
      userName: recordPanel.get("userName")
    },
    success: function(response) {
      response = Ext.decode(response.responseText);
      if (response.success) {
        var data = response.data;
        var window = Ext.create(
          "DukeSource.view.risk.User.windows.ViewWindowManagerOfCollaborator",
          {
            modal: true
          }
        );
        window.down("#nameManagerUser").setValue(data[0]["fullName"]);
        window.down("#nameAgencyUser").setValue(data[0]["descriptionAgency"]);
        window
          .down("#nameWorkAreaUser")
          .setValue(data[0]["descriptionWorkArea"]);
        window
          .down("#nameJobPlaceUser")
          .setValue(data[0]["descriptionJobPlace"]);
        window.down("#nameCategoryUser").setValue(data[0]["category"]);
        window.show();
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          response.mensaje,
          Ext.Msg.ERROR
        );
      }
    },
    failure: function() {}
  });
}
