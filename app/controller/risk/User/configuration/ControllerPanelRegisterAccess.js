Ext.define(
  "DukeSource.controller.risk.User.configuration.ControllerPanelRegisterAccess",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.User.grids.StoreGridPanelRegisterAccess",
      "risk.User.combos.StoreComboModule"
    ],
    models: [
      "risk.User.grids.ModelGridPanelRegisterAccess",
      "risk.User.combos.ModelComboModule"
    ],
    views: [
      "risk.User.grids.ViewGridPanelRegisterAccess",
      "risk.User.configuration.ViewPanelRegisterAccess",
      "risk.util.ViewComboYesNo",
      "risk.User.combos.ViewComboModule"
    ],
    init: function() {
      this.control({
        "[action=searchTriggerGridAccess]": {
          keyup: this._onSearchTriggerGridAccess
        },
        "[action=exportAccessPdf]": {
          click: this._onExportAccessPdf
        },
        "[action=exportAccessExcel]": {
          click: this._onExportAccessExcel
        },
        "[action=accessAuditory]": {
          click: this._onAccessAuditory
        },
        "[action=newAccess]": {
          click: this._newAccess
        },
        "[action=saveAccess]": {
          click: this._onSaveAccess
        },
        "[action=deleteAccess]": {
          click: this._onDeleteAccess
        },
        "[action=searchAccess]": {
          specialkey: this._searchAccess
        },
        "ViewPanelRegisterAccess grid": {
          itemdblclick: this._onModifyAccess
        }
      });
    },
    _newAccess: function() {
      var window = Ext.create(
        "DukeSource.view.risk.User.windows.ViewWindowAccess",
        {
          modal: true
        }
      ).show();
    },
    _onSaveAccess: function() {
      var window = Ext.ComponentQuery.query("ViewWindowAccess")[0];
      if (
        window
          .down("form")
          .getForm()
          .isValid()
      ) {
        Ext.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/saveAccess.htm",
          params: {
            jsonData: Ext.JSON.encode(window.down("form").getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              window.close();
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.INFO
              );
              Ext.ComponentQuery.query("ViewPanelRegisterAccess grid")[0]
                .getStore()
                .load();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onDeleteAccess: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterAccess grid")[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteAccess.htm"
      );
    },
    _searchAccess: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query("ViewPanelRegisterAccess grid")[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findAccess.htm",
          "idAccess",
          "idAccess"
        );
      } else {
      }
    },
    _onSearchTriggerGridAccess: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterAccess grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportAccessPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportAccessExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onAccessAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterAccess grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditAccess.htm"
      );
    },
    _onModifyAccess: function(grid) {
      var window = Ext.create(
        "DukeSource.view.risk.User.windows.ViewWindowAccess",
        {
          modal: true
        }
      ).show();
      window
        .down("form")
        .getForm()
        .loadRecord(grid.getSelectionModel().getSelection()[0]);
    }
  }
);
