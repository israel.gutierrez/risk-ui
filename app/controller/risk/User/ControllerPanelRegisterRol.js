Ext.define("DukeSource.controller.risk.User.ControllerPanelRegisterRol", {
  extend: "Ext.app.Controller",
  stores: [
    "risk.User.grids.StoreGridPanelRegisterRol",
    "risk.User.combos.StoreComboPermission",
    "risk.User.combos.StoreComboModule",
    "risk.User.combos.StoreComboAgency",
    "risk.User.grids.StoreGridPermissionCheck"
  ],
  models: [
    "risk.User.grids.ModelGridPanelRegisterRol",
    "risk.User.combos.ModelComboPermission",
    "risk.User.combos.ModelComboModule",
    "risk.User.combos.ModelComboAgency",
    "risk.User.grids.ModelGridPermissionCheck"
  ],
  views: [
    "risk.User.ViewPanelRegisterRol",
    "risk.User.grids.ViewGridPanelRegisterRol",
    "risk.User.combos.ViewComboPermission",
    "risk.User.combos.ViewComboModule",
    "risk.User.combos.ViewComboAgency",
    "risk.User.grids.ViewGridPermissionCheck"
  ],
  init: function() {
    this.control({
      "[action = searchTriggerGridPermission]": {
        keyup: this._onSearchTriggerGridPermission
      },
      "[action = searchTriggerGridRole]": {
        keyup: this._onSearchTriggerGridRole
      },
      "[action=newRole]": {
        click: this._newRole
      },
      "[action=assignPermission]": {
        click: this._onAssignPermission
      },
      "[action=deleteRole]": {
        click: this._deleteRole
      },
      "[action=searchRole]": {
        specialkey: this._searchRole
      },
      "[action=searchAccessByModule]": {
        select: this._onSearchAccessByModule
      },
      "[action=searchAccessByModuleType]": {
        select: this._onSearchAccessByModuleType
      },
      "[action=assignPermissionToRol]": {
        click: this._onAssignPermissionToRol
      }
    });
  },
  _newRole: function() {
    var modelRol = Ext.create(
      "DukeSource.model.risk.User.grids.ModelGridPanelRegisterRol",
      {
        id: "id",
        name: "",
        description: "",
        state: "S"
      }
    );
    var panel = Ext.ComponentQuery.query("ViewPanelRegisterRol grid")[0];
    var grid = Ext.ComponentQuery.query("ViewPanelRegisterRol grid")[0];
    var editor = panel.editingPlugin;
    editor.cancelEdit();
    grid.getStore().insert(0, modelRol);
    editor.startEdit(0, 0);
  },
  _onAssignPermission: function() {
    var grid = Ext.ComponentQuery.query("ViewPanelRegisterRol grid")[0];

    if (grid.getSelectionModel().getCount() === 0) {
      DukeSource.global.DirtyView.messageNormal(DukeSource.global.GiroMessages.MESSAGE_ITEM);
    } else {
      var k = Ext.create(
        "DukeSource.view.risk.User.ViewWindowAssignPermission",
        {
          modal: true,
          height: 400,
          width: 1000
        }
      ).show();
      Ext.ComponentQuery.query("textfield[name=idRole]")[0].setValue(
        grid
          .getSelectionModel()
          .getSelection()[0]
          .get("id")
      );
      k.down("ViewGridPermissionCheck").store.getProxy().url =
        "http://localhost:9000/giro/loadGridDefault.htm";
      k.down("ViewGridPermissionCheck")
        .down("pagingtoolbar")
        .moveFirst();
    }
  },
  _deleteRole: function() {
    var grid = Ext.ComponentQuery.query("ViewPanelRegisterRol grid")[0];
    if (grid.getSelectionModel().getCount() === 0) {
      DukeSource.global.DirtyView.messageNormal(DukeSource.global.GiroMessages.MESSAGE_ITEM);
    } else {
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
        msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
        icon: Ext.Msg.QUESTION,
        buttonText: {
          yes: "Si"
        },
        buttons: Ext.MessageBox.YESNO,
        fn: function(btn) {
          if (btn === "yes") {
            Ext.Ajax.request({
              method: "POST",
              url:
                "http://localhost:9000/giro/deleteRol.htm?nameView=ViewPanelRegisterRol",
              params: {
                id: grid
                  .getSelectionModel()
                  .getSelection()[0]
                  .get("id")
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  grid.getStore().load();
                  DukeSource.global.DirtyView.messageNormal(response.message);
                } else {
                  DukeSource.global.DirtyView.messageWarning(response.message);
                }
              },
              failure: function() {}
            });
          } else {
          }
        }
      });
    }
  },
  _searchRole: function(field, e) {
    if (e.getKey() === e.ENTER) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterRol grid")[0];
      DukeSource.global.DirtyView.searchPaginationGridToEnter(
        field,
        grid,
        grid.down("pagingtoolbar"),
        "http://localhost:9000/giro/findMatchRole.htm",
        "nameRole",
        "nameRole"
      );
    }
  },
  _onSearchTriggerGridRole: function(text) {
    var grid = Ext.ComponentQuery.query("ViewPanelRegisterRol grid")[0];
    DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
  },
  _onSearchAccessByModule: function(cbo) {
    var gridPanel = Ext.ComponentQuery.query("ViewPanelRegisterRol grid")[0];
    var window = Ext.ComponentQuery.query("ViewWindowAssignPermission")[0];
    var grid = window.down("ViewGridPermissionCheck");
    grid.store.getProxy().extraParams = {
      role: gridPanel
        .getSelectionModel()
        .getSelection()[0]
        .get("id"),
      agency: window.down("#agency").getValue(),
      valueFind: cbo.getValue()
    };
    grid.store.getProxy().url =
      "http://localhost:9000/giro/showListRoleAccessActivesByModule.htm";
    grid.down("pagingtoolbar").moveFirst();
  },
  _onSearchAccessByModuleType: function(cbo) {
    var gridPanel = Ext.ComponentQuery.query("ViewPanelRegisterRol grid")[0];
    var win = Ext.ComponentQuery.query("ViewWindowAssignPermission")[0];
    var grid = win.down("grid");

    if (win.down("ViewComboModule").getValue() != null) {
      grid.store.getProxy().extraParams = {
        role: gridPanel
          .getSelectionModel()
          .getSelection()[0]
          .get("id"),
        agency: win.down("#agency").getValue(),
        valueFind: win.down("ViewComboModule").getValue(),
        valueFind2: cbo.getRawValue()
      };
      grid.store.getProxy().url =
        "http://localhost:9000/giro/showListRoleAccessActivesByModuleAndType.htm";
      grid.down("pagingtoolbar").moveFirst();
    } else {
      DukeSource.global.DirtyView.messageWarning("Seleccione por favor un MODULO");
    }
  },
  _onAssignPermissionToRol: function() {
    var gridPanel = Ext.ComponentQuery.query("ViewPanelRegisterRol grid")[0];
    var win = Ext.ComponentQuery.query("ViewWindowAssignPermission")[0];

    var arrayDataToSave = [];
    var records = win
      .down("ViewGridPermissionCheck")
      .getStore()
      .getRange();

    Ext.Array.each(records, function(record, index, countriesItSelf) {
      arrayDataToSave.push(record.data);
    });

    Ext.Ajax.request({
      method: "POST",
      url: "http://localhost:9000/giro/saveRoleAccess.htm",
      params: {
        jsonData: Ext.JSON.encode(arrayDataToSave),
        role: gridPanel
          .getSelectionModel()
          .getSelection()[0]
          .get("id"),
        agency: win.down("#agency").getValue(),
        module: win.down("ViewComboModule").getValue()
      },
      success: function(response) {
        response = Ext.decode(response.responseText);
        if (response.success) {
          DukeSource.global.DirtyView.messageNormal(response.message);
          win.close();
        } else {
          DukeSource.global.DirtyView.messageWarning(response.message);
        }
      },
      failure: function() {}
    });
  },
  _onSearchTriggerGridPermission: function(text) {
    var grid = Ext.ComponentQuery.query("ViewWindowAssignPermission grid")[0];
    DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
  }
});
