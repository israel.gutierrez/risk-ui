Ext.define(
  "DukeSource.controller.risk.User.application.ControllerPanelRegisterConstant",
  {
    extend: "Ext.app.Controller",
    autoDestroy: true,
    stores: [
      "risk.User.grids.StoreGridPanelRegisterConstant",
      "risk.User.combos.StoreComboModule"
    ],
    models: [
      "risk.User.grids.ModelGridPanelRegisterConstant",
      "risk.User.combos.ModelComboModule"
    ],
    views: [
      "risk.User.grids.ViewGridPanelRegisterConstant",
      "risk.User.combos.ViewComboModule",
      "risk.User.ViewPanelRegisterConstant"
    ],
    init: function() {
      this.control({
        "[action=constantModule]": {
          select: this._onConstantModule
        },
        "[action=searchTriggerGridConstant]": {
          keyup: this._onSearchTriggerGridConstant
        },
        "[action=searchDataGridConstant]": {
          specialkey: this._onSearchDataGridConstant
        },
        "[action=constantAuditory]": {
          click: this._onConstantAuditory
        },
        "[action=exportPdfConstant]": {
          click: this._onExportPdfConstant
        },
        "[action=exportXlsConstant]": {
          click: this._onExportXlsConstant
        }
      });
    },

    _onConstantModule: function(cbo) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterConstant grid")[0];
      var toolbar = Ext.ComponentQuery.query(
        "ViewGridPanelRegisterConstant pagingtoolbar"
      )[0];
      grid.store.getProxy().extraParams = {
        valueFind: cbo.getValue(),
        propertyOrder: "description"
      };
      grid.store.getProxy().url =
        "http://localhost:9000/giro/findConstantByModule.htm";
      toolbar.moveFirst();
    },
    _onSearchTriggerGridConstant: function(text) {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterConstant grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onSearchDataGridConstant: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterConstant grid"
        )[0];
        var toolbar = Ext.ComponentQuery.query(
          "ViewGridPanelRegisterConstant pagingtoolbar"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          toolbar,
          "http://localhost:9000/giro/findConstant.htm",
          "description",
          "description"
        );
      } else {
      }
    },
    _onConstantAuditory: function() {
      var grid = Ext.ComponentQuery.query("ViewPanelRegisterConstant grid")[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditConstant.htm"
      );
    },
    _onExportPdfConstant: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportXlsConstant: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    }
  }
);
