Ext.define("DukeSource.controller.risk.User.ControllerPanelPermission", {
  extend: "Ext.app.Controller",
  stores: [
    "risk.User.grids.StoreGridPermission",
    "risk.User.grids.StoreGridPermissionCheck",
    //            ,'risk.User.combos.StoreComboPermission'
    "risk.User.combos.StoreComboAgency",
    "risk.User.combos.StoreComboRole"
  ],
  models: [
    "risk.User.grids.ModelGridPermission",
    "risk.User.grids.ModelGridPermissionCheck",
    //            ,'risk.User.combos.ModelComboPermission'
    "risk.User.combos.ModelComboAgency",
    "risk.User.combos.ModelComboRole"
  ],
  views: [
    "risk.User.grids.ViewGridPermission",
    "risk.User.grids.ViewGridPermissionCheck",
    "risk.User.ViewPanelPermission",
    //            ,'risk.User.combos.ViewComboPermission'
    "risk.User.combos.ViewComboAgency",
    "risk.User.combos.ViewComboRole"
  ],
  init: function() {
    this.control({
      "[action = userNameLimitPermissionPanel]": {
        click: this._onUserNameLimitPermissionPanel
      },
      "[action=searchPermissionByRoleAgency]": {
        click: this._onSearchPermissionByRoleAgency
      },
      "[action=goOutPanelPermission]": {
        click: this._onGoOutPanelPermission
      }
    });
  },
  _onUserNameLimitPermissionPanel: function() {
    var win = Ext.create("DukeSource.view.risk.util.search.SearchUser", {
      modal: true,
      height: 353,
      width: 700
    }).show();
    var panel1 = Ext.ComponentQuery.query("ViewPanelPermission")[0];

    win.down("grid").on("itemdblclick", function() {
      var userName = win
        .down("grid")
        .getSelectionModel()
        .getSelection()[0]
        .get("userName");
      panel1.down("UpperCaseTextField[name=user]").setValue(userName);
      panel1.down("UpperCaseTextField[name=fullName]").setValue(
        win
          .down("grid")
          .getSelectionModel()
          .getSelection()[0]
          .get("fullName")
      );
      panel1.down("UpperCaseTextField[name=document]").setValue(
        win
          .down("grid")
          .getSelectionModel()
          .getSelection()[0]
          .get("documentNumber")
      );
      win.close();
      panel1
        .down("ViewComboRole")
        .getStore()
        .load({
          scope: this,
          url:
            "http://localhost:9000/giro/showListUserRolesActivesModelRole.htm",
          params: {
            propertyFind: "user.username",
            propertyOrder: "id",
            valueFind: userName
          },
          callback: function() {
            panel1.down("ViewComboRole").setValue(
              panel1
                .down("ViewComboRole")
                .getStore()
                .getAt(0)
                .get("id")
            );
          }
        });
      panel1.down("ViewComboRole").clearValue();
      panel1
        .down("ViewComboRole")
        .getStore()
        .clearFilter(true);
      panel1
        .down("ViewComboAgency")
        .getStore()
        .load({
          scope: this,
          url:
            "http://localhost:9000/giro/showListUserAgenciesActivesModelAgency.htm",
          params: {
            propertyFind: "user.username",
            propertyOrder: "idUserAgency",
            valueFind: userName
          },
          callback: function() {
            panel1.down("ViewComboRole").setValue(
              panel1
                .down("ViewComboRole")
                .getStore()
                .getAt(0)
                .get("idAgency")
            );
          }
        });
      panel1.down("ViewComboAgency").clearValue();
      panel1
        .down("ViewComboAgency")
        .getStore()
        .clearFilter(true);
      DukeSource.global.DirtyView.focusEvent(panel1.down("ViewComboRole"));
      win.close();
    });
  },
  _onSearchPermissionByRoleAgency: function() {
    var panel = Ext.ComponentQuery.query("ViewPanelPermission")[0];
    var grid = panel.down("grid");
    grid.store.getProxy().extraParams = {
      id: panel.down("ViewComboRole").getValue(),
      idAgency: panel.down("ViewComboAgency").getValue()
    };
    grid.store.getProxy().url =
      "http://localhost:9000/giro/showListPermission.htm";
    grid.down("pagingtoolbar").moveFirst();
  },
  _onGoOutPanelPermission: function() {
    DukeSource.global.DirtyView.messageOut(Ext.ComponentQuery.query("ViewPanelPermission")[0]);
  }
});
