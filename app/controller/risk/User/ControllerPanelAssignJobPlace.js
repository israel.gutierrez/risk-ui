Ext.define("DukeSource.controller.risk.User.ControllerPanelAssignJobPlace", {
  extend: "Ext.app.Controller",
  stores: ["risk.User.grids.StoreTreeGridPanelAssignJobPlace"],
  models: ["risk.User.grids.ModelTreeGridPanelAssignJobPlace"],
  views: ["risk.User.grids.ViewTreeGridPanelAssignJobPlace"],
  init: function() {
    this.control({
      ViewTreeGridPanelAssignJobPlace: {
        itemcontextmenu: this.treeRightClickAssignJobPlace
      },
      "[action=assignJobPlace]": {
        click: this._onAssignJobPlace
      },
      "[action=searchTriggerJobPlace]": {
        keyup: this._onSearchTriggerJobPlace
      },
      "AddMenuGestor menuitem[text=Agregar]": {
        click: this._addGestor
      },
      "EditMenuGestor menuitem[text=Agregar]": {
        click: this._addCollaborator
      },
      "EditMenuGestor menuitem[text=Eliminar]": {
        click: this._deleteCollaborator
      },
      "EditMenuCollaborator menuitem[text=Eliminar]": {
        click: this._deleteCollaborator
      },
      "[action=saveManagerToAnalyst]": {
        click: this._onSaveManagerToAnalyst
      }
    });
  },
  treeRightClickAssignJobPlace: function(view, record, item, index, e) {
    e.stopEvent();
    if (record.get("depth") === 1) {
      var rowMenu = Ext.create("Ext.menu.Menu", {
        width: 160,
        items: [
          {
            text: "Asignar Cargo Gestor",
            iconCls: "add",
            handler: function() {
              if (
                Ext.ComponentQuery.query("SearchJobPlaceOnlyGestor")[0] ==
                undefined
              ) {
                var windowJobPlaceGestor = Ext.create(
                  "DukeSource.view.risk.util.search.SearchJobPlaceOnlyGestor",
                  {
                    category: "GESTOR",
                    idJobPlace: record.get("idJobPlace"),
                    url:
                      "http://localhost:9000/giro/showListJobPlaceByCategory.htm",
                    modal: true
                  }
                );
                windowJobPlaceGestor
                  .down("form")
                  .getComponent("id")
                  .setValue(record.get("id"));
                windowJobPlaceGestor.setTitle(
                  "ASIGNAR CARGO A: " + " " + record.get("fullName")
                );
                windowJobPlaceGestor.show();
              }
            }
          }
        ]
      });
      rowMenu.showAt(e.getXY());
    }
    if (record.get("depth") === 2) {
      var rowMenu = Ext.create("Ext.menu.Menu", {
        width: 185,
        items: [
          {
            text: "Asignar Cargo Colaborador",
            iconCls: "add",
            handler: function() {
              if (
                Ext.ComponentQuery.query("SearchJobPlaceOnlyGestor")[0] ==
                undefined
              ) {
                var windowJobPlaceGestor = Ext.create(
                  "DukeSource.view.risk.util.search.SearchJobPlaceOnlyGestor",
                  {
                    category: "COLABORADOR",
                    idJobPlace: record.get("idJobPlace"),
                    workArea: record.get("workArea"),
                    url:
                      "http://localhost:9000/giro/showListJobPlaceByCategory.htm",
                    modal: true
                  }
                );
                windowJobPlaceGestor
                  .down("form")
                  .getComponent("id")
                  .setValue(record.get("id") /*+"/id"*/);
                windowJobPlaceGestor.setTitle(
                  "ASIGNAR COLABORADOR A: " + " " + record.get("text")
                );
                windowJobPlaceGestor.show();
              }
            }
          },
          {
            text: "Eliminar Cargo Gestor",
            iconCls: "delete",
            handler: function() {
              Ext.MessageBox.show({
                title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
                icon: Ext.Msg.QUESTION,
                buttonText: {
                  yes: "Si"
                },
                buttons: Ext.MessageBox.YESNO,
                fn: function(btn) {
                  if (btn == "yes") {
                    Ext.Ajax.request({
                      method: "POST",
                      url:
                        "http://localhost:9000/giro/deleteHierarchyJobPlace.htm",
                      params: {
                        idJobPlace: record.get("idJobPlace"),
                        node: record.get("id"),
                        category: record.get("category")
                      },
                      success: function(response) {
                        response = Ext.decode(response.responseText);
                        if (response.success) {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_MESSAGE,
                            response.mensaje,
                            Ext.Msg.INFO
                          );
                          var refreshNode = Ext.ComponentQuery.query(
                            "ViewPanelAssignJobPlace ViewTreeGridPanelAssignJobPlace"
                          )[0]
                            .getStore()
                            .getNodeById(response.data);
                          refreshNode.removeAll(false);
                          Ext.ComponentQuery.query(
                            "ViewPanelAssignJobPlace ViewTreeGridPanelAssignJobPlace"
                          )[0]
                            .getStore()
                            .load({
                              node: refreshNode
                            });
                        } else {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_ERROR,
                            response.mensaje,
                            Ext.Msg.ERROR
                          );
                        }
                      },
                      failure: function() {}
                    });
                  }
                }
              });
            }
          }
        ]
      });
      rowMenu.showAt(e.getXY());
    }
    if (record.get("depth") === 3) {
      var rowMenu = Ext.create("Ext.menu.Menu", {
        width: 185,
        items: [
          {
            text: "Eliminar Cargo Colaborador",
            iconCls: "delete",
            handler: function() {
              Ext.MessageBox.show({
                title: DukeSource.global.GiroMessages.TITLE_CONFIRM, //
                msg: DukeSource.global.GiroMessages.MESSAGE_DELETE,
                icon: Ext.Msg.QUESTION,
                buttonText: {
                  yes: "Si"
                },
                buttons: Ext.MessageBox.YESNO,
                fn: function(btn) {
                  if (btn == "yes") {
                    Ext.Ajax.request({
                      method: "POST",
                      url:
                        "http://localhost:9000/giro/deleteHierarchyJobPlace.htm",
                      params: {
                        idJobPlace: record.get("idJobPlace"),
                        node: record.get("id"),
                        category: record.get("category")
                      },
                      success: function(response) {
                        response = Ext.decode(response.responseText);
                        if (response.success) {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_MESSAGE,
                            response.mensaje,
                            Ext.Msg.INFO
                          );
                          var refreshNode = Ext.ComponentQuery.query(
                            "ViewPanelAssignJobPlace ViewTreeGridPanelAssignJobPlace"
                          )[0]
                            .getStore()
                            .getNodeById(response.data);
                          refreshNode.removeAll(false);
                          Ext.ComponentQuery.query(
                            "ViewPanelAssignJobPlace ViewTreeGridPanelAssignJobPlace"
                          )[0]
                            .getStore()
                            .load({
                              node: refreshNode
                            });
                        } else {
                          DukeSource.global.DirtyView.messageAlert(
                            DukeSource.global.GiroMessages.TITLE_ERROR,
                            response.mensaje,
                            Ext.Msg.ERROR
                          );
                        }
                      },
                      failure: function() {}
                    });
                  }
                }
              });
            }
          }
        ]
      });
      rowMenu.showAt(e.getXY());
    }
    return false;
  },
  _onAssignJobPlace: function() {
    var winJobPlace = Ext.ComponentQuery.query("SearchJobPlaceOnlyGestor")[0];
    var gridAvailable = winJobPlace.down("#availableJobPlace");
    var records = gridAvailable.getSelectionModel().getSelection();
    var arrayDataToSave = new Array();
    Ext.Array.each(records, function(record) {
      arrayDataToSave.push(record.data);
    });
    if (gridAvailable.getSelectionModel().getCount() == 0) {
      DukeSource.global.DirtyView.messageAlert(
        DukeSource.global.GiroMessages.TITLE_WARNING,
        "SELECCIONE POR FAVOR UN CARGO A ASIGNAR",
        Ext.Msg.WARNING
      );
    } else {
      Ext.MessageBox.show({
        title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
        msg: "Esta seguro de ASIGNAR EL(LOS) CARGO(S):",
        icon: Ext.Msg.QUESTION,
        buttonText: {
          yes: "Si"
        },
        buttons: Ext.MessageBox.YESNO,
        fn: function(btn) {
          if (btn == "yes") {
            Ext.Ajax.request({
              method: "POST",
              url: "http://localhost:9000/giro/saveHierarchyJobPlace.htm",
              params: {
                listJobPlaces: Ext.JSON.encode(arrayDataToSave),
                node: winJobPlace
                  .down("form")
                  .getComponent("id")
                  .getValue(),
                idJobPlace: winJobPlace.idJobPlace
              },
              success: function(response) {
                response = Ext.decode(response.responseText);
                if (response.success) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_MESSAGE,
                    response.mensaje,
                    Ext.Msg.INFO
                  );
                  winJobPlace.close();
                  var refreshNode = Ext.ComponentQuery.query(
                    "ViewPanelAssignJobPlace ViewTreeGridPanelAssignJobPlace"
                  )[0]
                    .getStore()
                    .getNodeById(response.data);
                  refreshNode.removeAll(false);
                  Ext.ComponentQuery.query(
                    "ViewPanelAssignJobPlace ViewTreeGridPanelAssignJobPlace"
                  )[0]
                    .getStore()
                    .load({
                      node: refreshNode
                    });
                } else {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    response.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              },
              failure: function() {}
            });
          }
        }
      });
    }
  },
  _onSearchTriggerJobPlace: function(text) {
    var grid = Ext.ComponentQuery.query("SearchJobPlaceOnlyGestor grid")[0];
    DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
  },
  _addGestor: function() {
    var countryWindow = Ext.create(
      "DukeSource.view.risk.User.windows.ViewWindowAssignGestor",
      {
        modal: true
      }
    );
    countryWindow
      .down("form")
      .getComponent("id")
      .setValue(this.application.currentRecord.get("id") /*+"/id"*/);
    countryWindow.show();
  },
  _addCollaborator: function() {
    var countryWindow = Ext.create(
      "DukeSource.view.risk.User.windows.ViewWindowAssignCollaborator",
      {
        modal: true
      }
    );
    countryWindow
      .down("form")
      .getComponent("id")
      .setValue(this.application.currentRecord.get("id") /*+"/id"*/);
    countryWindow.show();
  },
  _deleteCollaborator: function() {
    Ext.Ajax.request({
      method: "POST",
      url: "http://localhost:9000/giro/deleteCollaborator.htm",
      params: {
        id: this.application.currentRecord.get("id")
      },
      success: function(response) {
        response = Ext.decode(response.responseText);
        if (response.success) {
          DukeSource.global.DirtyView.messageAlert(
            DukeSource.global.GiroMessages.TITLE_MESSAGE,
            response.mensaje,
            Ext.Msg.INFO
          );
          var refreshNode = Ext.ComponentQuery.query(
            "ViewPanelAssignJobPlace ViewTreeGridPanelAssignJobPlace"
          )[0]
            .getStore()
            .getNodeById(response.data);
          refreshNode.removeAll(false);
          Ext.ComponentQuery.query(
            "ViewPanelAssignJobPlace ViewTreeGridPanelAssignJobPlace"
          )[0]
            .getStore()
            .load({
              node: refreshNode
            });
        } else {
          DukeSource.global.DirtyView.messageAlert(
            DukeSource.global.GiroMessages.TITLE_WARNING,
            response.mensaje,
            Ext.Msg.ERROR
          );
        }
      },
      failure: function() {}
    });
  },
  _onSaveManagerToAnalyst: function(button) {
    var window = button.up("window");
    var records = window
      .down("grid")
      .getSelectionModel()
      .getSelection();
    var arrayDataToSave = new Array();
    Ext.Array.each(records, function(record) {
      arrayDataToSave.push(record.data);
    });
    Ext.Ajax.request({
      method: "POST",
      url: "http://localhost:9000/giro/saveCollaborator.htm",
      params: {
        listCollaborator: Ext.JSON.encode(arrayDataToSave),
        boss: window
          .down("form")
          .getComponent("id")
          .getValue()
      },
      success: function(response) {
        response = Ext.decode(response.responseText);
        if (response.success) {
          DukeSource.global.DirtyView.messageAlert(
            DukeSource.global.GiroMessages.TITLE_MESSAGE,
            response.mensaje,
            Ext.Msg.INFO
          );
          var refreshNode = Ext.ComponentQuery.query(
            "ViewPanelAssignJobPlace ViewTreeGridPanelAssignJobPlace"
          )[0]
            .getStore()
            .getNodeById(response.data);
          refreshNode.removeAll(false);
          Ext.ComponentQuery.query(
            "ViewPanelAssignJobPlace ViewTreeGridPanelAssignJobPlace"
          )[0]
            .getStore()
            .load({
              node: refreshNode
            });
          window.close();
        } else {
          DukeSource.global.DirtyView.messageAlert(
            DukeSource.global.GiroMessages.TITLE_WARNING,
            response.mensaje,
            Ext.Msg.ERROR
          );
        }
      },
      failure: function() {}
    });
  }
});
