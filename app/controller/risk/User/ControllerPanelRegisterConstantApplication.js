Ext.define(
  "DukeSource.controller.risk.User.ControllerPanelRegisterConstantApplication",
  {
    extend: "Ext.app.Controller",
    stores: [
      "risk.User.grids.StoreGridPanelRegisterConstantApplication",
      "risk.User.combos.StoreComboModule"
    ],
    models: [
      "risk.User.grids.ModelGridPanelRegisterConstantApplication",
      "risk.User.combos.ModelComboModule"
    ],
    views: [
      "risk.User.grids.ViewGridPanelRegisterConstantApplication",
      "risk.User.ViewPanelRegisterConstantApplication",
      "risk.User.combos.ViewComboModule"
    ],
    init: function() {
      this.control({
        "[action=constantModule]": {
          select: this._onConstantModule
        },
        "[action=searchTriggerGridConstantApplication]": {
          keyup: this._onSearchTriggerGridConstantApplication
        },
        "[action=exportConstantApplicationPdf]": {
          click: this._onExportConstantApplicationPdf
        },
        "[action=exportConstantApplicationExcel]": {
          click: this._onExportConstantApplicationExcel
        },
        "[action=constantApplicationAuditory]": {
          click: this._onConstantApplicationAuditory
        },
        "[action=newConstantApplication]": {
          click: this._newConstantApplication
        },
        "[action=saveConstantApplication]": {
          click: this._onSaveConstantApplication
        },
        "[action=deleteConstantApplication]": {
          click: this._onDeleteConstantApplication
        },
        "[action=searchConstantApplication]": {
          specialkey: this._searchConstantApplication
        },
        "ViewPanelRegisterConstantApplication grid": {
          itemdblclick: this._onModifyConstantApplication
        }
      });
    },
    _newConstantApplication: function() {
      var window = Ext.create(
        "DukeSource.view.risk.User.windows.ViewWindowConstantApplication",
        {
          modal: true
        }
      ).show();
    },
    _onSaveConstantApplication: function() {
      var window = Ext.ComponentQuery.query("ViewWindowConstantApplication")[0];
      if (
        window
          .down("form")
          .getForm()
          .isValid()
      ) {
        Ext.Ajax.request({
          method: "POST",
          url:
            "http://localhost:9000/giro/saveConstantApplication.htm?nameView=ViewPanelRegisterConstantApplication",
          params: {
            jsonData: Ext.JSON.encode(window.down("form").getValues())
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              window.close();
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.INFO
              );
              Ext.ComponentQuery.query(
                "ViewPanelRegisterConstantApplication grid"
              )[0]
                .getStore()
                .load();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_WARNING,
                response.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function() {}
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onDeleteConstantApplication: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterConstantApplication grid"
      )[0];
      DukeSource.global.DirtyView.deleteElementToGrid(
        grid,
        "http://localhost:9000/giro/deleteConstantApplication.htm?nameView=ViewPanelRegisterConstantApplication"
      );
    },
    _searchConstantApplication: function(field, e) {
      if (e.getKey() === e.ENTER) {
        var grid = Ext.ComponentQuery.query(
          "ViewPanelRegisterConstantApplication grid"
        )[0];
        DukeSource.global.DirtyView.searchPaginationGridToEnter(
          field,
          grid,
          grid.down("pagingtoolbar"),
          "http://localhost:9000/giro/findConstantApplication.htm",
          "codeConstant",
          "description"
        );
      } else {
      }
    },
    _onSearchTriggerGridConstantApplication: function(text) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterConstantApplication grid"
      )[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onExportConstantApplicationPdf: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/pdfParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onExportConstantApplicationExcel: function() {
      DukeSource.global.DirtyView.generateReportXLSPDF(
        "http://localhost:9000/giro/xlsParam.htm?nameReporte=",
        "archivoNew"
      );
    },
    _onConstantApplicationAuditory: function() {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterConstantApplication grid"
      )[0];
      DukeSource.global.DirtyView.showWindowAuditory(
        grid,
        "http://localhost:9000/giro/findAuditConstantApplication.htm"
      );
    },
    _onModifyConstantApplication: function(grid) {
      var window = Ext.create(
        "DukeSource.view.risk.User.windows.ViewWindowConstantApplication",
        {
          modal: true
        }
      ).show();
      window
        .down("form")
        .getForm()
        .loadRecord(grid.getSelectionModel().getSelection()[0]);
    },
    _onConstantModule: function(cbo) {
      var grid = Ext.ComponentQuery.query(
        "ViewPanelRegisterConstantApplication grid"
      )[0];
      var toolbar = Ext.ComponentQuery.query(
        "ViewGridPanelRegisterConstantApplication pagingtoolbar"
      )[0];
      grid.store.getProxy().extraParams = {
        valueFind: cbo.getValue(),
        propertyOrder: "description"
      };
      grid.store.getProxy().url =
        "http://localhost:9000/giro/findConstantApplicationByModule.htm";
      toolbar.moveFirst();
    }
  }
);
