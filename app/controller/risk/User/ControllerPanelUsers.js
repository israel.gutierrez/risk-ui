Ext.define('DukeSource.controller.risk.User.ControllerPanelUsers', {
    extend: 'Ext.app.Controller',
    stores: [
        'risk.User.grids.StoreGridPanelUsers',
        'risk.parameter.combos.StoreComboWorkArea',
        'risk.User.combos.StoreComboAgency',
        'risk.User.combos.StoreComboRole'

    ],
    models: [
        'risk.User.grids.ModelGridPanelUsers',
        'risk.parameter.combos.ModelComboWorkArea',
        'risk.User.combos.ModelComboAgency',
        'risk.User.combos.ModelComboRole'
    ],
    views: [
        'risk.User.ViewPanelUsers',
        'risk.parameter.combos.ViewComboWorkArea',
        'risk.parameter.combos.ViewComboJobPlace',
        'risk.User.grids.ViewGridPanelUsers',
        'risk.User.combos.ViewComboAgency',
        'risk.User.combos.ViewComboRole',
        'risk.parameter.combos.ViewComboCategory'

    ],
    init: function () {
        this.control({
            '[action=selectWorkArea]': {
                select: this._onSelectWorkArea
            },
            '[action=saveUserRisk]': {
                click: this._onSaveUserRisk
            },
            '[action=modifyUser]': {
                click: this._onModifyUser
            },
            '[action=verifyUserUserName]': {
                click: this._onVerifyUserUserName
            },
            '[action=verifyUserDniUser]': {
                click: this._onVerifyUserDniUser
            },
            '[action=deleteUserRisk]': {
                click: this._onDeleteUserRisk
            },
            '[action = registerUser]': {
                click: this._onRegisterUser
            },
            'ViewWindowAssignEmploymentUser grid[title=DISPONIBLES]': {
                itemdblclick: this._onGetDetailGridEmployment
            },
            'ViewWindowAssignEmploymentUser grid[title=ASIGNADOS]': {
                itemdblclick: this._onDeleteEmploymentToUser
            },
            'ViewWindowAssignRoleUser grid[title=DISPONIBLES]': {
                itemdblclick: this._onAddRoleToUser
            },
            'ViewWindowAssignRoleUser grid[title=ASIGNADOS]': {
                itemdblclick: this._onDeleteRoleToUser
            },
            'ViewWindowAssignAgencyUser grid[title=DISPONIBLES]': {
                itemdblclick: this._onAddAgencyToUser
            },
            'ViewWindowAssignAgencyUser grid[title=ASIGNADOS]': {
                itemdblclick: this._onDeleteAgencyToUser
            },
            'ViewPanelUsers combobox[fieldLabel=PUESTO]': {
                select: this._onShowWindowDetail
            },
            'ViewPanelUsers combobox[fieldLabel=ROL]': {
                select: this._onShowWindowDetailRole
            },
            '[action=confirmModifyUser]': {
                click: this._onConfirmModifyUser
            },
            'ViewPanelUsers ViewGridPanelUsers': {
                itemdblclick: this._onModifyUser
            },
            '[action=goOutUser]': {
                click: this._onGoOutUser
            },
            '[action=accessProcessSpecial]': {
                click: this._onAccessProcessSpecial
            },
            '[action=limitOperating]': {
                click: this._onLimitOperating
            },
            '[action=permission]': {
                click: this._onPermission
            },
            '[action=password]': {
                click: this._onPassword
            },
            '[action=actionSearhByDni]': {
                specialkey: this._onActionSearhByDni
            },
            '[action=searchRoleAll]': {
                specialkey: this._onSearchRoleAll
            },
            '[action=searchGridAllRole]': {
                keyup: this._onSearchGridAllRole
            },
            '[action=searchAgencyAll]': {
                specialkey: this._onSearchAgencyAll
            },
            '[action=searchGridAllAgency]': {
                keyup: this._onSearchGridAllAgency
            },
            '[action = searchGridRoleGeneral]': {
                keyup: this._onSearchGridRoleGeneral
            },
            '[action = searchRoleGeneral]': {
                specialkey: this._onSearchRoleGeneral
            },
            '[action = searchEmploymentAll]': {
                specialkey: this._onSearchEmploymentAll
            },
            '[action = searchGridAllEmployment]': {
                keyup: this._onSearchGridAllEmployment
            },
            '[action=sendDetailToEmployment]': {
                click: this._onSendDetailToEmployment
            },
            '[action=sendDetailToComboEmploymet]': {
                click: this._onSendDetailToComboEmploymet
            },
            '[action=confirmDescriptionRole]': {
                click: this._onConfirmDescriptionRole
            },
            '[action=exportUserXls]': {
                click: this._onExportUserXls
            },
            '[action=exportUserPdf]': {
                click: this._onExportUserPdf
            },
            '[action=userAuditory]': {
                click: this._onUserAuditory
            },
            '[action=saveChangePasswordWindow]': {
                click: this._onSaveChangePasswordWindow
            }

        });
    },

    _onSelectWorkArea: function () {
        var panel = Ext.ComponentQuery.query('ViewWindowRegisterUser')[0];
        panel.down('ViewComboJobPlace').reset();
        panel.down('ViewComboJobPlace').getStore().load();
        panel.down('ViewComboJobPlace').setDisabled(false);

    },
    _onSaveUserRisk: function () {
        var win = Ext.ComponentQuery.query('ViewWindowRegisterUser')[0];
        if (win.down('form').getForm().isValid()) {
            Ext.Ajax.request({
                method: 'POST',
                url: win.down('button[action=saveUserRisk]').getText() == 'Guardar' ? 'http://localhost:9000/giro/saveUser.htm' : 'http://localhost:9000/giro/updateUser.htm',
                params: {
                    jsonData: Ext.JSON.encode(win.down('form').getForm().getValues()),
                    employment: win.down('ViewComboJobPlace').getValue(),
                    descriptionCategory: win.down('ViewComboCategory').getRawValue()
                },
                success: function (response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                        var grid = Ext.ComponentQuery.query('ViewPanelUsers grid')[0];
                        grid.store.getProxy().extraParams = {propertyOrder: 'u.username'};
                        grid.store.getProxy().url = 'http://localhost:9000/giro/showListUserActives.htm';
                        grid.down('pagingtoolbar').moveFirst();
                        win.close();

                    } else {
                        DukeSource.global.DirtyView.messageWarning(response.message);
                    }
                },
                failure: function () {
                }
            });
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }
    },
    _onModifyUser: function () {
        if (Ext.ComponentQuery.query('ViewWindowRegisterUser')[0] === undefined) {
            var win = Ext.create('DukeSource.view.risk.User.windows.ViewWindowRegisterUser', {
                modal: true
            });
            var grid = Ext.ComponentQuery.query('ViewPanelUsers grid')[0];
            if (grid.getSelectionModel().getCount() === 0) {
                DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
            } else {
                Ext.Ajax.request({
                    method: 'POST',
                    url: 'http://localhost:9000/giro/loadUserById.htm',
                    params: {
                        idUser: grid.getSelectionModel().getSelection()[0].get('userName')
                    },
                    success: function (response) {
                        response = Ext.decode(response.responseText);
                        if (response.success) {
                            win.down('#idRole').getStore().load();
                            win.down('ViewComboAgency').getStore().load();
                            win.down('ViewComboWorkArea').getStore().load();
                            win.down('ViewComboJobPlace').getStore().load();
                            win.down('form').getForm().setValues(response.data);
                            DukeSource.global.DirtyView.toReadOnly(win.down('UpperCaseTextFieldObligatory[name=login]'));
                            win.down('button[action=saveUserRisk]').setText('Modificar');
                            win.down('#password').setDisabled(true);
                            win.down('#password').setVisible(false);
                            win.down('#rePassword').setDisabled(true);
                            win.down('#rePassword').setVisible(false);
                            win.down('#state').setVisible(true);
                            win.down('#state').setVisible(true);
                            win.show();
                            win.down('#surnamePat').focus(false, 300);
                        } else {
                            DukeSource.global.DirtyView.messageWarning(response.message);
                        }
                    },
                    failure: function () {
                    }
                });
            }
        }

    },
    _onRegisterUser: function () {
        var win = Ext.create('DukeSource.view.risk.User.windows.ViewWindowRegisterUser', {
            modal: true,
            actionBtn: 'saveUserRisk'
        }).show();
        win.down('#firstName').focus(false, 300);
        win.down('ViewComboAgency').setDisabled(false);
        win.down('#idRole').setDisabled(false);

        DukeSource.global.DirtyView.changeObligatoryElement(win.down('UpperCaseTextFieldObligatory[name=login]'));
        DukeSource.global.DirtyView.changeObligatoryElement(win.down('UpperCaseTextFieldObligatory[name=email]'));

    },
    _onVerifyUserUserName: function () {
        var panel = Ext.ComponentQuery.query('ViewWindowRegisterUser')[0];
        var field = panel.down('UpperCaseTextFieldObligatory[name=username]');
        var focusFin = panel.down('textfield[name=numberDocument]');
        if (field.getValue() == '') {
            DukeSource.global.DirtyView.messageToFocus(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_EMPTY, Ext.Msg.WARNING, field);
        } else {
            DukeSource.global.DirtyView.existUserByUserName(field, 'http://localhost:9000/giro/verificationUserExist.htm', field, focusFin);
        }
    },
    _onVerifyUserDniUser: function () {
        var panel = Ext.ComponentQuery.query('ViewWindowRegisterUser')[0];
        var field = panel.down('textfield[name=numberDocument]');
        var focusFin = panel.down('UpperCaseTextFieldObligatory[name=email]');
        if (field.getValue() == '') {
            DukeSource.global.DirtyView.messageToFocus(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_EMPTY, Ext.Msg.WARNING, field);
        } else {
            DukeSource.global.DirtyView.existUserByDni(field, 'http://localhost:9000/giro/findUserByDni.htm', field, focusFin);
        }
    },
    _onDeleteUserRisk: function () {
        var panel = Ext.ComponentQuery.query('ViewPanelUsers')[0];
        var grid = panel.down('ViewGridPanelUsers');
        if (grid.getSelectionModel().getCount() == 0) {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_ITEM, Ext.Msg.WARNING);
        } else {
            Ext.Ajax.request({
                method: 'POST',
                url: 'http://localhost:9000/giro/deleteUser.htm',
                params: {
                    userName: grid.getSelectionModel().getSelection()[0].get('userName')
                },
                success: function (response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                        grid.store.getProxy().extraParams = {propertyOrder: 'fullName'};
                        grid.store.getProxy().url = 'http://localhost:9000/giro/showListUserActives.htm';
                        grid.down('pagingtoolbar').moveFirst();
                    } else {
                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, response.mensaje, Ext.Msg.ERROR);
                    }
                },
                failure: function () {
                }
            });
        }
    },
    _onConfirmModifyUser: function () {

        var win = Ext.ComponentQuery.query('ViewWindowModifyUser')[0];
        if (win.down('form').getForm().isValid()) {
            Ext.Ajax.request({
                method: 'POST',
                url: 'http://localhost:9000/giro/updateUser.htm',
                params: {
                    jsonData: Ext.JSON.encode(win.down('form').getForm().getValues())
                },
                success: function (response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        DukeSource.global.DirtyView.messageAlert(DukeSource.view.finance.util.Messages.TITLE_MENSAJE, response.mensaje, Ext.Msg.INFO);
                        win.close();
                        var grid = Ext.ComponentQuery.query('ViewPanelUsers ViewGridPanelUsers')[0];
                        grid.store.getProxy().extraParams = {propertyOrder: 'fullName'};
                        grid.store.getProxy().url = 'http://localhost:9000/giro/showListUserActives.htm';
                        grid.down('pagingtoolbar').moveFirst();
                    } else {
                        DukeSource.global.DirtyView.messageAlert(DukeSource.view.finance.util.Messages.TITLE_ADVERTENCIA, response.mensaje, Ext.Msg.ERROR);
                    }
                },
                failure: function () {
                }
            });

        } else {
            DukeSource.global.DirtyView.messageAlert(DukeSource.view.finance.util.Messages.TITLE_ADVERTENCIA, DukeSource.view.finance.util.Messages.MESSAGE_COMPLETAR, Ext.Msg.WARNING);
        }
    },
    _onGetDetailGridEmployment: function (grid) {
        var window = Ext.create('DukeSource.view.risk.User.ViewWindowAssignDetailEmploymentUser', {
            height: 160,
            width: 420,
            modal: true,
            buttons: [
                {
                    text: 'CONFIRMAR',
                    action: 'sendDetailToEmployment',
                    iconCls: 'save'

                },
                {
                    text: 'SALIR',
                    scope: this,
                    handler: function () {
                        window.close();
                    },
                    iconCls: 'logout'

                }
            ]
        }).show();
        window.down('textfield[name=employment]').setValue(grid.getSelectionModel().getSelection()[0].get('idEmployment'));
        window.down('textfield[name=descriptionEmployment]').setValue(grid.getSelectionModel().getSelection()[0].get('description'));
        window.down('textfield[name=user]').setValue(Ext.ComponentQuery.query('ViewWindowAssignEmploymentUser textfield[name=ocultar]')[0].getValue());

    },
    _onSendDetailToEmployment: function () {
        var window = Ext.ComponentQuery.query('ViewWindowAssignDetailEmploymentUser')[0];
        var userName = window.down('textfield[name=user]').getValue();
        if (window.down('form').getForm().isValid()) {
            Ext.Ajax.request({
                method: 'POST',
                url: 'http://localhost:9000/giro/saveUserEmployment.htm',
                params: {
                    jsonData: Ext.JSON.encode(window.down('form').getForm().getValues())
                },
                success: function (response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        DukeSource.global.DirtyView.messageAlert(DukeSource.view.finance.util.Messages.TITLE_MENSAJE, response.mensaje, Ext.Msg.INFO);
                        var window = Ext.ComponentQuery.query('ViewWindowAssignEmploymentUser')[0];
                        var grid1 = window.query('grid[title=DISPONIBLES]')[0];
                        var toolbar = grid1.query('pagingtoolbar')[0];
                        DukeSource.global.DirtyView.searchPaginationGridNormal(userName, grid1, toolbar, 'http://localhost:9000/giro/showListEmploymentsAvailableByUser.htm', 'user.username', 'description');

                        var grid2 = window.query('grid[title=ASIGNADOS]')[0];
                        var toolbar2 = grid2.query('pagingtoolbar')[0];

                        DukeSource.global.DirtyView.searchPaginationGridNormal(userName, grid2, toolbar2, 'http://localhost:9000/giro/showListUserEmploymentActives.htm', 'user.username', 'idUserEmployment');
                    } else {
                        DukeSource.global.DirtyView.messageAlert(DukeSource.view.finance.util.Messages.TITLE_ADVERTENCIA, response.mensaje, Ext.Msg.ERROR);
                    }
                },
                failure: function () {
                }
            });
            window.close();
        } else {
            DukeSource.global.DirtyView.messageAlert(DukeSource.view.finance.util.Messages.TITLE_ADVERTENCIA, DukeSource.view.finance.util.Messages.MESSAGE_COMPLETAR, Ext.Msg.ERROR);
        }
    },
    _onDeleteEmploymentToUser: function (grid) {

        var userName = Ext.ComponentQuery.query('ViewWindowAssignEmploymentUser textfield[name=ocultar]')[0].getValue();
        Ext.MessageBox.show({
            title: DukeSource.view.finance.util.Messages.TITLE_CONFIRMAR,
            msg: 'Desea quitar el puesto ' + grid.getSelectionModel().getSelection()[0].get('descriptionEmployment') + '?',
            icon: Ext.Msg.QUESTION,
            buttonText: {
                yes: "Si"
            },
            buttons: Ext.MessageBox.YESNO,
            fn: function (btn) {
                if (btn == 'yes') {
                    Ext.Ajax.request({
                        method: 'POST',
                        url: 'http://localhost:9000/giro/deleteUserEmployment.htm',
                        params: {
                            jsonData: Ext.JSON.encode(grid.getSelectionModel().getSelection()[0].data)
                        },
                        success: function (response) {
                            response = Ext.decode(response.responseText);
                            if (response.success) {
                                DukeSource.global.DirtyView.messageAlert(DukeSource.view.finance.util.Messages.TITLE_MENSAJE, response.mensaje, Ext.Msg.INFO);
                                var grid1 = Ext.ComponentQuery.query('ViewWindowAssignEmploymentUser grid[title=DISPONIBLES]')[0];
                                var toolbar = grid1.query('pagingtoolbar')[0]
                                DukeSource.global.DirtyView.searchPaginationGridNormal(userName, grid1, toolbar, 'http://localhost:9000/giro/showListEmploymentsAvailableByUser.htm', 'user.username', 'description');

                                var grid2 = Ext.ComponentQuery.query('ViewWindowAssignEmploymentUser grid[title=ASIGNADOS]')[0];
                                var toolbar2 = grid2.query('pagingtoolbar')[0]
                                DukeSource.global.DirtyView.searchPaginationGridNormal(userName, grid2, toolbar2, 'http://localhost:9000/giro/showListUserEmploymentActives.htm', 'user.username', 'idUserEmployment');

                            } else {
                                DukeSource.global.DirtyView.messageAlert(DukeSource.view.finance.util.Messages.TITLE_ADVERTENCIA, response.mensaje, Ext.Msg.ERROR);
                            }
                        },
                        failure: function () {
                        }
                    });
                }
            }
        });
    },
    _onAddRoleToUser: function (grid) {
        var userName = Ext.ComponentQuery.query('ViewWindowAssignRoleUser textfield[name=ocultar]')[0].getValue();
        Ext.MessageBox.show({
            title: DukeSource.global.GiroMessages.TITLE_MESSAGE,
            msg: 'Desea agregar el rol ' + grid.getSelectionModel().getSelection()[0].get('name') + '?',
            icon: Ext.Msg.QUESTION,
            buttonText: {
                yes: "Si"
            },
            buttons: Ext.MessageBox.YESNO,
            fn: function (btn) {
                if (btn == 'yes') {
                    Ext.Ajax.request({
                        method: 'POST',
                        url: 'http://localhost:9000/giro/saveUserRole.htm',
                        params: {
                            idRole: grid.getSelectionModel().getSelection()[0].get('id'),
                            userName: userName
                        },
                        success: function (response) {
                            response = Ext.decode(response.responseText);
                            if (response.success) {
                                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                                var grid1 = Ext.ComponentQuery.query('ViewWindowAssignRoleUser grid[title=DISPONIBLES]')[0];
                                var toolbar = grid1.query('pagingtoolbar')[0]
                                DukeSource.global.DirtyView.searchPaginationGridNormal(userName, grid1, toolbar, 'http://localhost:9000/giro/showListRoleAvailableByUser.htm', 'user.username', 'description');

                                var grid2 = Ext.ComponentQuery.query('ViewWindowAssignRoleUser grid[title=ASIGNADOS]')[0];
                                var toolbar2 = grid2.query('pagingtoolbar')[0]
                                DukeSource.global.DirtyView.searchPaginationGridNormal(userName, grid2, toolbar2, 'http://localhost:9000/giro/showListUserRolesActives.htm', 'user.username', 'description');

                            } else {
                                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, response.mensaje, Ext.Msg.ERROR);
                            }
                        },
                        failure: function () {
                        }
                    });
                }
            }
        });

    },
    _onDeleteRoleToUser: function (grid) {

        var userName = Ext.ComponentQuery.query('ViewWindowAssignRoleUser textfield[name=ocultar]')[0].getValue();
        Ext.MessageBox.show({
            title: DukeSource.global.GiroMessages.TITLE_MESSAGE,
            msg: 'Desea eliminar el rol ' + grid.getSelectionModel().getSelection()[0].get('descriptionRole') + '?',
            icon: Ext.Msg.QUESTION,
            buttonText: {
                yes: "Si"
            },
            buttons: Ext.MessageBox.YESNO,
            fn: function (btn) {
                if (btn == 'yes') {

                    Ext.Ajax.request({
                        method: 'POST',
                        url: 'http://localhost:9000/giro/deleteUserRole.htm',
                        params: {
                            jsonData: Ext.JSON.encode(grid.getSelectionModel().getSelection()[0].data)
                        },
                        success: function (response) {
                            response = Ext.decode(response.responseText);
                            if (response.success) {
                                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                                var grid1 = Ext.ComponentQuery.query('ViewWindowAssignRoleUser grid[title=DISPONIBLES]')[0];
                                var toolbar = grid1.query('pagingtoolbar')[0]
                                DukeSource.global.DirtyView.searchPaginationGridNormal(userName, grid1, toolbar, 'http://localhost:9000/giro/showListRoleAvailableByUser.htm', 'user.username', 'description');

                                var grid2 = Ext.ComponentQuery.query('ViewWindowAssignRoleUser grid[title=ASIGNADOS]')[0];
                                var toolbar2 = grid2.query('pagingtoolbar')[0]

                                DukeSource.global.DirtyView.searchPaginationGridNormal(userName, grid2, toolbar2, 'http://localhost:9000/giro/showListUserRolesActives.htm', 'user.username', 'description');
                            } else {
                                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, response.mensaje, Ext.Msg.ERROR);
                            }
                        },
                        failure: function () {
                        }
                    });
                }
            }
        });

    },
    _onAddAgencyToUser: function (grid) {

        var userName = Ext.ComponentQuery.query('ViewWindowAssignAgencyUser textfield[name=ocultar]')[0].getValue();
        Ext.MessageBox.show({
            title: DukeSource.global.GiroMessages.TITLE_MESSAGE,
            msg: 'Desea agregar el agencia ' + grid.getSelectionModel().getSelection()[0].get('description') + '?',
            icon: Ext.Msg.QUESTION,
            buttonText: {
                yes: "Si"
            },
            buttons: Ext.MessageBox.YESNO,
            fn: function (btn) {
                if (btn == 'yes') {
                    Ext.Ajax.request({
                        method: 'POST',
                        url: 'http://localhost:9000/giro/saveUserAgency.htm',
                        params: {
                            idAgency: grid.getSelectionModel().getSelection()[0].get('idAgency'),
                            userName: userName
                        },
                        success: function (response) {
                            response = Ext.decode(response.responseText);
                            if (response.success) {
                                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                                var grid1 = Ext.ComponentQuery.query('ViewWindowAssignAgencyUser grid[title=DISPONIBLES]')[0];
                                var toolbar = grid1.query('pagingtoolbar')[0]
                                DukeSource.global.DirtyView.searchPaginationGridNormal(userName, grid1, toolbar, 'http://localhost:9000/giro/showListAgenciesAvailableByUser.htm', 'user.username', 'description');

                                var grid2 = Ext.ComponentQuery.query('ViewWindowAssignAgencyUser grid[title=ASIGNADOS]')[0];
                                var toolbar2 = grid2.query('pagingtoolbar')[0]
                                DukeSource.global.DirtyView.searchPaginationGridNormal(userName, grid2, toolbar2, 'http://localhost:9000/giro/showListUserAgenciesActives.htm', 'user.username', 'idUserAgency');

                            } else {
                                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, response.mensaje, Ext.Msg.ERROR);
                            }
                        },
                        failure: function () {
                        }
                    });
                }
            }
        });
    },
    _onDeleteAgencyToUser: function (grid) {
        var userName = Ext.ComponentQuery.query('ViewWindowAssignAgencyUser textfield[name=ocultar]')[0].getValue();
        Ext.MessageBox.show({
            title: DukeSource.global.GiroMessages.TITLE_MESSAGE,
            msg: 'Desea eliminar la agencia ' + grid.getSelectionModel().getSelection()[0].get('descriptionAgency') + '?',
            icon: Ext.Msg.QUESTION,
            buttonText: {
                yes: "Si"
            },
            buttons: Ext.MessageBox.YESNO,
            fn: function (btn) {
                if (btn == 'yes') {
                    Ext.Ajax.request({
                        method: 'POST',
                        url: 'http://localhost:9000/giro/deleteUserAgency.htm',
                        params: {
                            jsonData: Ext.JSON.encode(grid.getSelectionModel().getSelection()[0].data)
                        },
                        success: function (response) {
                            response = Ext.decode(response.responseText);
                            if (response.success) {
                                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                                var grid1 = Ext.ComponentQuery.query('ViewWindowAssignAgencyUser grid[title=DISPONIBLES]')[0];
                                var toolbar = grid1.query('pagingtoolbar')[0]
                                DukeSource.global.DirtyView.searchPaginationGridNormal(userName, grid1, toolbar, 'http://localhost:9000/giro/showListAgenciesAvailableByUser.htm', 'user.username', 'description');

                                var grid2 = Ext.ComponentQuery.query('ViewWindowAssignAgencyUser grid[title=ASIGNADOS]')[0];
                                var toolbar2 = grid2.query('pagingtoolbar')[0]
                                DukeSource.global.DirtyView.searchPaginationGridNormal(userName, grid2, toolbar2, 'http://localhost:9000/giro/showListUserAgenciesActives.htm', 'user.username', 'idUserAgency');

                            } else {
                                DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, response.mensaje, Ext.Msg.ERROR);
                            }
                        },
                        failure: function () {
                        }
                    });
                }
            }
        });
    },
    _onGoOutUser: function () {
        DukeSource.global.DirtyView.messageOut(Ext.ComponentQuery.query('ViewPanelUsers')[0]);
    },
    _onAccessProcessSpecial: function () {
        var grid = Ext.ComponentQuery.query('ViewPanelUsers grid')[0];
        if (grid.getSelectionModel().getCount() == 0) {
            DukeSource.global.DirtyView.messageAlert(DukeSource.view.finance.util.Messages.TITLE_ADVERTENCIA, DukeSource.view.finance.util.Messages.MESSAGE_ITEM, Ext.Msg.WARNING);
        } else {
            var win = Ext.create('DukeSource.view.risk.User.ViewWindowAccessProcessSpecial', {
                height: 500,
                width: 1000,
                modal: true
            }).show();
            win.down('UpperCaseTextField[name=user]').setValue(grid.getSelectionModel().getSelection()[0].get('userName'));
            var grid1 = win.down('grid');
            DukeSource.global.DirtyView.searchPaginationGridToEnter(win.down('UpperCaseTextField[name=user]'), grid1, grid1.down('pagingtoolbar'), 'http://localhost:9000/giro/showListSpecialProcessAccessActivesByUser.htm', 'user.username', 'idSpecialProcessAccess');
        }

    },
    _onLimitOperating: function () {
        var grid = Ext.ComponentQuery.query('ViewPanelUsers grid')[0];
        if (grid.getSelectionModel().getCount() == 0) {
            DukeSource.global.DirtyView.messageAlert(DukeSource.view.finance.util.Messages.TITLE_ADVERTENCIA, DukeSource.view.finance.util.Messages.MESSAGE_ITEM, Ext.Msg.WARNING);
        } else {
            var win = Ext.create('DukeSource.view.risk.User.ViewWindowLimitOperating', {
                height: 500,
                width: 1000,
                modal: true
            }).show();
            win.down('UpperCaseTextField[name=user]').setValue(grid.getSelectionModel().getSelection()[0].get('userName'));
            var grid1 = win.down('grid');
            DukeSource.global.DirtyView.searchPaginationGridToEnter(win.query('UpperCaseTextField[name=user]')[0], grid1, grid1.down('pagingtoolbar'), 'http://localhost:9000/giro/showListOperatingLimitActivesByUser.htm', 'user.username', 'idOperatingLimit');
        }

    },
    _onPermission: function () {
        var grid = Ext.ComponentQuery.query('ViewPanelUsers grid')[0];
        if (grid.getSelectionModel().getCount() == 0) {
            Ext.MessageBox.alert("MENSAJE INFORMACION", "Por favor, seleccione un REGISTRO");
        } else {
            Ext.create('DukeSource.view.risk.User.ViewWindowPermission', {
                height: 500,
                width: 1000,
                modal: true
            }).show();
        }
    },
    _onPassword: function () {
        var grid = Ext.ComponentQuery.query('ViewPanelUsers grid')[0];
        if (grid.getSelectionModel().getCount() == 0) {
            Ext.MessageBox.alert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_ITEM);
        } else {
            var win = Ext.create('DukeSource.view.risk.User.ViewWindowPassword', {
                height: 194,
                width: 447,
                modal: true
            }).show();
            win.query('textfield[name=userName]')[0].setValue(grid.getSelectionModel().getSelection()[0].get('userName'));
        }
    },
    _onActionSearhByDni: function (field, e) {
        if (e.getKey() === e.ENTER) {
            Ext.ComponentQuery.query('ViewWindowAgreeDataUser button[text=CONFIRMAR]')[0].focus(false, 20);
        } else {

        }

    },
    _onSearchRoleAll: function (field, e) {
        if (e.getKey() === e.ENTER) {
            var window = Ext.ComponentQuery.query('ViewWindowAssignRoleUser')[0];
            var grid = window.query('grid[title=DISPONIBLES]')[0];
            var userName = window.query('textfield[name=ocultar]')[0].getValue();

            var toolbar = grid.query('pagingtoolbar')[0]

            grid.store.getProxy().extraParams = {
                propertyFind: 'user.username',
                valueFind: userName,
                propertyOrder: 'description',
                valueMatch: field.getValue()
            };
            grid.store.getProxy().url = 'http://localhost:9000/giro/showListMatchRoleAvailableByUser.htm';
            toolbar.moveFirst();
        } else {

        }
    },
    _onSearchGridAllRole: function (text) {
        var grid = Ext.ComponentQuery.query('ViewWindowAssignRoleUser grid[title=DISPONIBLES]')[0];
        DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onSearchAgencyAll: function (field, e) {
        if (e.getKey() === e.ENTER) {
            var window = Ext.ComponentQuery.query('ViewWindowAssignAgencyUser')[0];
            var grid = window.query('grid[title=DISPONIBLES]')[0];
            var userName = window.query('textfield[name=ocultar]')[0].getValue();

            var toolbar = grid.query('pagingtoolbar')[0];
            grid.store.getProxy().extraParams = {
                propertyFind: 'user.username',
                valueFind: userName,
                propertyOrder: 'description',
                valueMatch: field.getValue()
            };
            grid.store.getProxy().url = 'http://localhost:9000/giro/showListMatchAgenciesAvailableByUser.htm';
            toolbar.moveFirst();

        } else {

        }
    },
    _onSearchGridAllAgency: function (text) {
        var grid = Ext.ComponentQuery.query('ViewWindowAssignAgencyUser grid[title=DISPONIBLES]')[0];
        DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onSearchGridRoleGeneral: function (text) {
        var grid = Ext.ComponentQuery.query('ViewPanelUsers grid')[0];
        DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onSearchRoleGeneral: function (field, e) {
        if (e.getKey() === e.ENTER) {
            var grid = Ext.ComponentQuery.query("ViewPanelUsers grid")[0];
            var toolbar = Ext.ComponentQuery.query('ViewGridPanelUsers pagingtoolbar')[0];
            DukeSource.global.DirtyView.searchPaginationGridToEnter(field, grid, toolbar, 'http://localhost:9000/giro/findMatchUser.htm', 'u.fullName', 'u.username')
        } else {

        }
    },
    _onSearchEmploymentAll: function (field, e) {
        if (e.getKey() === e.ENTER) {
            var window = Ext.ComponentQuery.query('ViewWindowAssignEmploymentUser')[0];
            var grid = window.query('grid[title=DISPONIBLES]')[0];
            var userName = window.query('textfield[name=ocultar]')[0].getValue();

            var toolbar = grid.query('pagingtoolbar')[0];
            grid.store.getProxy().extraParams = {
                propertyFind: 'user.username',
                valueFind: userName,
                propertyOrder: 'description',
                valueMatch: field.getValue()
            };
            grid.store.getProxy().url = 'http://localhost:9000/giro/showListMatchEmploymentsAvailableByUser.htm';
            toolbar.moveFirst();
        } else {

        }
    },
    _onSearchGridAllEmployment: function (text) {
        var grid = Ext.ComponentQuery.query('ViewWindowAssignEmploymentUser grid')[0];
        DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },
    _onShowWindowDetail: function () {
        var window = Ext.create('DukeSource.view.risk.User.ViewWindowAssignDetailEmploymentUser', {
            height: 160,
            width: 420,
            modal: true,
            buttons: [
                {
                    text: 'CONFIRMAR',
                    action: 'sendDetailToComboEmploymet',
                    iconCls: 'save'

                }
            ]
        }).show();
    },
    _onSendDetailToComboEmploymet: function () {
        var window = Ext.ComponentQuery.query('ViewWindowAssignDetailEmploymentUser')[0];
        var formUser = window.down('form');
        var panelUser = Ext.ComponentQuery.query('ViewPanelUsers')[0];
        if (formUser.getForm().isValid()) {
            panelUser.down('form').getForm().setValues(formUser.getValues());
            window.close();
            DukeSource.global.DirtyView.focusEvent(panelUser.down('timefield[name=hourStart]'));
        } else {
            DukeSource.global.DirtyView.messageToFocus(DukeSource.view.finance.util.Messages.TITLE_ADVERTENCIA, DukeSource.view.finance.util.Messages.MESSAGE_COMPLETAR, Ext.Msg.WARNING, window.down('numberfield[name=correlativeUserEmployment]'));
        }


    },
    _onShowWindowDetailRole: function () {
        var window = Ext.create('Ext.window.Window', {
            alias: 'widget.WindowDescriptionRole',
            width: 525,
            height: 105,
            closable: false,
            modal: true,
            layout: {
                type: 'fit'
            },
            border: false,
            title: 'DESCRIPCION',
            titleAlign: 'center',
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'UpperCaseTextField',
                            fieldLabel: 'DESCRIPCION',
                            fieldCls: 'obligatoryTextField',
                            allowBlank: false,
                            msgTarget: 'side',
                            blankText: DukeSource.view.risk.util.Messages.MESSAGE_OBLIGATORIO,
                            name: 'roleDescription',
                            anchor: '100%',
                            listeners: {
                                afterrender: function (field) {
                                    field.focus(false, 200);
                                },
                                specialkey: function (f, e) {
                                    if (e.getKey() === e.ENTER) {
                                        DukeSource.global.DirtyView.focusEvent(window.query('button')[0])
                                    } else {
                                    }

                                }
                            }
                        }

                    ]

                }
            ],
            buttons: [
                {
                    text: 'CONFIRMAR',
                    action: 'confirmDescriptionRole',
                    iconCls: 'save'

                }
            ]
        }).show();
    },
    _onConfirmDescriptionRole: function (button) {
        var window = button.up('window');
        var panel = Ext.ComponentQuery.query('ViewPanelUsers')[0];
        if (window.down('form').getForm().isValid()) {
            panel.down('form').getForm().setValues(window.down('form').getForm().getValues());
            window.close();
            DukeSource.global.DirtyView.focusEvent(panel.down('ViewComboAgency[name=idAgency]'));
        } else {
            DukeSource.global.DirtyView.messageToFocus(DukeSource.view.finance.util.Messages.TITLE_ADVERTENCIA, DukeSource.view.finance.util.Messages.MESSAGE_COMPLETAR, Ext.Msg.WARNING, window.down('UpperCaseTextField'));
        }

    },
    _onExportUserXls: function () {
        Ext.core.DomHelper.append(document.body, {
            tag: 'iframe',
            id: 'downloadIframe',
            frameBorder: 0,
            width: 0,
            height: 0,
            css: 'display:none;visibility:hidden;height:0px;',
            src: 'http://localhost:9000/giro/xlsGeneralReport.htm?values=' + '&names=' + '&types=' + '&nameReport=ROUSER0001XLS'
        });

    },
    _onExportUserPdf: function () {
        DukeSource.global.DirtyView.generateReportXLSPDF('http://localhost:9000/giro/pdfParam.htm?nameReporte=', 'archivoNew')
    },
    _onUserAuditory: function () {
        var grid = Ext.ComponentQuery.query("ViewPanelUsers grid")[0];
        DukeSource.global.DirtyView.showWindowAuditory(grid, 'http://localhost:9000/giro/findAuditUser.htm');
    },
    _onSaveChangePasswordWindow: function () {

        var win = Ext.ComponentQuery.query('ViewWindowPassword')[0];

        if (win.down('form').getForm().isValid()) {
            Ext.Ajax.request({
                method: 'POST',
                url: 'http://localhost:9000/giro/changePassword.htm',
                params: {
                    userName: win.down('textfield[name=userName]').getValue(),
                    password: win.down('textfield[name=password]').getValue(),
                    newPassword: win.down('textfield[name=newPassword]').getValue()
                },
                success: function (response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, response.mensaje, Ext.Msg.INFO);
                        win.close();
                    } else {
                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, response.mensaje, Ext.Msg.ERROR);
                    }
                },
                failure: function () {
                }
            });

        } else {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_COMPLETE, Ext.Msg.WARNING);
        }
    }
});
