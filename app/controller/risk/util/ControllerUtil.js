Ext.define("DukeSource.controller.risk.util.ControllerUtil", {
  extend: "Ext.app.Controller",
  views: [
    "risk.util.UpperCaseTextArea",
    "risk.util.UpperCaseTextField",
    "risk.util.UpperCaseTrigger",
    "risk.util.UpperCaseTextFieldObligatory",
    "risk.util.UpperCaseTextFieldReadOnly",
    "risk.util.NumberFormatObligatory",
    "risk.util.NumberDecimalNumber",
    "risk.util.TextFieldNumberFormatObligatory",
    "risk.util.TextFieldNumberFormatReadOnly",
    "risk.util.TextFieldNumberFormat",
    "risk.util.NumberDecimalNumberReadOnly",
    "risk.util.NumberDecimalNumberObligatory"
  ],
  init: function() {
    this.control({});
  }
});
