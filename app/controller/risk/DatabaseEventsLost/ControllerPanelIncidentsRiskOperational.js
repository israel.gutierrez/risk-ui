Ext.define(
  "DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelIncidentsRiskOperational",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [],
    init: function() {
      this.control({
        "[action=savePreIncidents]": {
          click: this._onSavePreIncidents
        },
        "[action=analysisSaveIncident]": {
          click: this._onAnalysisSaveIncident
        },
        "[action=saveIncidents]": {
          click: this._onSaveIncidents
        },
        "[action=searchAdvancedIncident]": {
          click: this._onSearchAdvancedIncident
        },
        "[action=showDocumentIncident]": {
          click: this._onViewFileAttachment
        },
        "[action=saveFileAttachmentsIncidents]": {
          click: this._onSaveFileAttachmentIncident
        },
        "[action=deleteFileAttachmentsIncidents]": {
          click: this._onDeleteFileAttachmentIncident
        },
        "[action=saveFileAttachTrackIncident]": {
          click: this._onSaveFileAttachTrackIncident
        },
        "[action=deleteFileAttachTrackIncident]": {
          click: this._onDeleteFileAttachTrackIncident
        },
        "[action=searchGridFileAttachTrackIncident]": {
          click: this._onSearchGridFileAttachTrackIncident
        },
        "[action=approveIncident]": {
          click: this._onApproveIncident
        },
        " ViewPanelIncidentsRiskOperational ViewGridIncidentsPendingRiskOperational": {
          itemdblclick: this._showModifyIncidentRiskOperational,
          itemcontextmenu: this.rightClickIncidentsRiskBasic
        },
        " ViewPanelIncidentsRiskOperational ViewGridIncidentsRevisedRiskOperational": {
          itemdblclick: this._showModifyIncidentRiskOperational,
          itemcontextmenu: this.rightClickIncidentsRisk
        },
        " ViewPanelIncidentsRiskOperational ViewGridIncidentsRefusedRiskOperational": {
          //itemdblclick: this._showModifyIncidentRiskOperational,
          itemcontextmenu: this.rightClickIncidentsRiskBasic
        }
      });
    },
    _onSavePreIncidents: function(button) {
      var panel = Ext.ComponentQuery.query(
        "ViewPanelIncidentsRiskOperational"
      )[0];
      var gridPending = panel.down("ViewGridIncidentsPendingRiskOperational");
      var window = button.up("window");
      var form = window.down("form");

      if (form.getForm().isValid()) {
        form.getForm().submit({
          url:
            "http://localhost:9000/giro/saveDetailIncidents.htm?nameView=ViewPanelIncidentsRiskOperational",
          waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
          method: "POST",
          success: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (valor.success) {
              if (category === DukeSource.global.GiroConstants.COLLABORATOR) {
                gridPending.store.getProxy().extraParams = {
                  stateIncident: "P"
                };
                gridPending.store.getProxy().url =
                  "http://localhost:9000/giro/getDetailIncidentsByState.htm";
                gridPending.down("pagingtoolbar").moveFirst();
              } else {
                gridPending.store.getProxy().extraParams = {
                  stateIncident: "R",
                  typeIncident: "RO"
                };
                gridPending.store.getProxy().url =
                  "http://localhost:9000/giro/getDetailIncidentsByState.htm";
                gridPending.down("pagingtoolbar").moveFirst();
              }
              DukeSource.global.DirtyView.messageNormal(valor.message);
              window.close();
            } else {
              DukeSource.global.DirtyView.messageWarning(valor.message);
            }
          },
          failure: function(form, action) {
            button.setDisabled(false);
            var valor = Ext.decode(action.response.responseText);
            if (!valor.success) {
              DukeSource.global.DirtyView.messageWarning(valor.message);
            }
          }
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
      }
    },
    _onAnalysisSaveIncident: function(button) {
      var windowPreIncident = button.up("window");

      var windowIncident = Ext.create(
        "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterIncidents",
        {
          typeIncident: windowPreIncident.typeIncident,
          hasPreIncident: true,
          modal: true
        }
      );
      if (windowPreIncident.down("#generateLoss").getValue()) {
        windowIncident.down("#generateLoss").setValue("S");
      } else {
        windowIncident.down("#generateLoss").setValue("N");
      }

      windowIncident
        .down("form")
        .query(".combobox")
        .forEach(function(c) {
          c.getStore().load();
        });

      windowIncident
        .down("#fullNameUserReport")
        .setValue(windowPreIncident.record["nameUserRegister"]);
      windowIncident
        .down("#userReport")
        .setValue(windowPreIncident.record["userRegister"]);
      windowIncident
        .down("#dateRegisterPreIncident")
        .setValue(windowPreIncident.record["dateRegister"]);
      windowIncident
        .down("#workAreaPreIncident")
        .setValue(windowPreIncident.record["workAreaRegister"]);
      windowIncident
        .down("form")
        .getForm()
        .setValues(windowPreIncident.record);
      windowIncident.down("#originIncident").setValue(DukeSource.global.GiroConstants.GIRO);
      windowIncident
        .down("#impact")
        .setValue(windowPreIncident.record["impacts"].split(","));

      windowIncident.timer = setInterval(function() {
        windowIncident.down("#dateRegister").setValue(new Date());
      }, 1000);

      windowIncident.show();

      if (windowPreIncident.record["descriptionFactorRisk"] === "") {
        windowIncident.down("#descriptionFactorRisk").setValue("(Seleccionar)");
      }

      windowPreIncident.close();
    },

    _onSaveIncidents: function(button) {
      var panel = Ext.ComponentQuery.query(
        "ViewPanelIncidentsRiskOperational"
      )[0];
      var gridPending = panel.down("ViewGridIncidentsPendingRiskOperational");
      var gridRevised = panel.down("ViewGridIncidentsRevisedRiskOperational");
      var gridRefused = panel.down("ViewGridIncidentsRefusedRiskOperational");
      var window = button.up("window");
      var form = window.down("form");
      if (form.getForm().isValid()) {
        form.getForm().submit({
          url:
            "http://localhost:9000/giro/saveIncidents.htm?nameView=ViewPanelIncidentsRiskOperational",
          params: {
            userRegister: window.down("#userRegister").getValue(),
            codeIncident: window.down("#codeIncident").getValue()
          },
          waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
          method: "POST",
          success: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (valor.success) {
              if (window.typeIncident === "P" || window.typeIncident === "R") {
                gridPending.store.getProxy().extraParams = {
                  stateIncident: "R",
                  typeIncident: DukeSource.global.GiroConstants.OPERATIONAL
                };
                gridPending.store.getProxy().url =
                  "http://localhost:9000/giro/getDetailIncidentsByState.htm";
                gridPending.down("pagingtoolbar").doRefresh();
              }
              if (window.down("#stateIncident").getValue() == 0) {
                gridRefused.store.getProxy().extraParams = {
                  stateIncident: "X",
                  typeIncident: DukeSource.global.GiroConstants.OPERATIONAL
                };
                gridRefused.store.getProxy().url =
                  "http://localhost:9000/giro/getIncidentsByState.htm";
                gridRefused.down("pagingtoolbar").doRefresh();
              }
              gridRevised.down("pagingtoolbar").doRefresh();

              DukeSource.global.DirtyView.messageNormal(valor.message);
              window.close();
            }
          },
          failure: function(form, action) {
            button.setDisabled(false);
            var valor = Ext.decode(action.response.responseText);
            if (!valor.success) {
              DukeSource.global.DirtyView.messageWarning(valor.message);
            }
          }
        });
      } else {
        form.down("#descriptionFactorRisk").setFieldStyle("color:red");
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
      }
    },

    _showModifyIncidentRiskOperational: function(grid, record) {
      var row = grid.getSelectionModel().getSelection()[0];
      if (
        category === DukeSource.global.GiroConstants.GESTOR ||
        category === DukeSource.global.GiroConstants.COLLABORATOR ||
        (category === DukeSource.global.GiroConstants.ANALYST &&
          (record.get("indicatorIncidents") === "P" ||
            record.get("indicatorIncidents") === "R"))
      ) {
        var win = Ext.create(
          "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowIncidentManager",
          {
            typeIncident: record.get("indicatorIncidents"),
            record: record.data,
            modal: true
          }
        );

        win
          .down("form")
          .query(".combobox")
          .forEach(function(c) {
            c.getStore().load();
          });

        if (record.get("indicatorIncidents") === "A") {
          win.query(".textfield,.numberfield,").forEach(function(c) {
            DukeSource.global.DirtyView.toReadOnly(c);
          });
          win.down("#confidential").setDisabled(true);
          win.down("#document").setDisabled(true);
          win.down("button").setDisabled(true);
        }
        if (category === DukeSource.global.GiroConstants.ANALYST) {
          win.down("#generateLoss").setVisible(codexCompany !== "es_cl_0001");
          win.query(".textfield,.numberfield").forEach(function(c) {
            DukeSource.global.DirtyView.toReadOnly(c);
          });
          win.down("#confidential").setDisabled(true);
          win.down("#document").setDisabled(true);
        }

        win
          .down("form")
          .getForm()
          .setValues(record.data);
        win.down("#impacts").setValue(record.get("impacts").split(","));

        win.down("#dateRegisterTemp").setValue(record.get("dateRegister"));
        win.down("#userReport").setValue(record.get("userRegister"));
        win
          .down("#fullNameUserReport")
          .setValue(record.get("nameUserRegister"));
        win.down("#workAreaReport").setValue(record.get("workAreaRegister"));

        win.show();
      } else if (
        category === DukeSource.global.GiroConstants.ANALYST &&
        !(
          record.get("indicatorIncidents") === "P" ||
          record.get("indicatorIncidents") === "R"
        )
      ) {
        if (record.get("typeIncident") === DukeSource.global.GiroConstants.OPERATIONAL) {
          var hasPreIncident = record.get("idDetailIncidents") !== "";

          win = Ext.create(
            "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterIncidents",
            {
              typeIncident: record.get("indicatorIncidents"),
              hasPreIncident: hasPreIncident,
              modal: true
            }
          );

          win
            .down("form")
            .query(".combobox")
            .forEach(function(c) {
              c.getStore().load();
            });

          this.setRegisterValues(win, record);

          win
            .down("form")
            .getForm()
            .setValues(record.data);
          win.down("#impact").setValue(record.get("impact").split(","));

          if (row.get("state") === "N") {
            win.query(".textfield,.numberfield").forEach(function(c) {
              DukeSource.global.DirtyView.toReadOnly(c);
            });
          }

          win.show();
        } else {
          win = Ext.create(
            "DukeSource.view.risk.DatabaseEventsLost.windows.WindowRegisterIssueCiberSecurity",
            {
              typeIncident: record.get("indicatorIncidents"),
              modal: true
            }
          );
          win
            .down("form")
            .query(".combobox")
            .forEach(function(c) {
              c.getStore().load();
            });

          this.setRegisterValues(win, record);

          win
            .down("form")
            .getForm()
            .setValues(record.data);
          win.show();
        }
      }
    },

    setRegisterValues: function(win, record) {
      win.down("#dateRegisterTemp").setValue(record.get("dateRegister"));
      win.down("#userRegisterTemp").setValue(record.get("userRegister"));
      win
        .down("#fullNameUserRegisterTemp")
        .setValue(record.get("fullNameUserRegister"));
      win
        .down("#workAreaRegisterTemp")
        .setValue(record.get("workAreaRegister"));
      win.down("#codeIncidentTemp").setValue(record.get("codeIncident"));
    },

    rightClickIncidentsRiskBasic: function(view, rec, node, index, e) {
      e.stopEvent();
      var addMenu = Ext.create(
        "DukeSource.view.risk.DatabaseEventsLost.AddMenuIncident",
        {}
      );
      addMenu.removeAll();
      addMenu.add({
        text: "Gestionar Adjuntos",
        iconCls: "gestionfile",
        handler: function() {
          manageFileAttachment(rec);
        }
      });
      addMenu.showAt(e.getXY());
    },

    rightClickIncidentsRisk: function(view, rec, node, index, e) {
      var app = this.application;
      e.stopEvent();
      var addMenu = Ext.create(
        "DukeSource.view.risk.DatabaseEventsLost.AddMenuIncident",
        {}
      );
      addMenu.removeAll();
      if (
        category === DukeSource.global.GiroConstants.COLLABORATOR ||
        category === DukeSource.global.GiroConstants.GESTOR
      ) {
        addMenu.add(
          {
            text: "Modificar-Detalle",
            iconCls: "modify",
            handler: function() {
              app
                .getController(
                  "DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelIncidentsRiskOperational"
                )
                ._showModifyIncidentRiskOperational(view, rec);
            }
          },
          {
            text: "Gestionar Adjuntos",
            iconCls: "gestionfile",
            handler: function() {
              manageFileAttachment(rec);
            }
          }
        );
      }
      if (category === DukeSource.global.GiroConstants.ANALYST) {
        addMenu.add(
          {
            text: "Seguimiento",
            iconCls: "seguimiento",
            handler: function() {
              var window = Ext.create(
                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterTrace",
                {
                  modal: true,
                  idIncident: rec.get("idIncident")
                }
              );
              var idIncident = rec.get("idIncident");
              window.title =
                window.title +
                " - " +
                rec.get("idIncident") +
                " - " +
                rec.get("descriptionShort");
              window.show();
              window.down("#dateTrack").setMinValue(rec.get("dateOccurrence"));
              window.down("#idIncident").setValue(idIncident);
              window.down("#typeIncident").setValue(DukeSource.global.GiroConstants.OPERATIONAL);
              window.down("#gridTrack").store.getProxy().extraParams = {
                idIncident: idIncident,
                typeIncident: DukeSource.global.GiroConstants.OPERATIONAL
              };
              window.down("#gridTrack").store.getProxy().url =
                "http://localhost:9000/giro/showListTrackIncidentActives.htm";
              window
                .down("#gridTrack")
                .down("pagingtoolbar")
                .moveFirst();
            }
          },
          {
            text: "Calificar impacto",
            iconCls: "evaluate",
            handler: function() {
              var win = Ext.create(
                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowEvaluationIncident",
                {
                  modal: true,
                  idIncident: rec.get("idIncident"),
                  typeIncident: rec.get("typeIncident")
                }
              );

              win.title = win.title + "<br>" + rec.get("codeIncident");
              win.show();
            }
          },
          {
            text: "Relacionar a Riesgos",
            iconCls: "risk",
            handler: function() {
              Ext.create(
                "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterIncidentRisk",
                {
                  modal: true,
                  process: rec.get("processOrigin"),
                  idIncident: rec.get("idIncident")
                }
              ).show();
            }
          },
          {
            text: "Gestionar Adjuntos",
            iconCls: "gestionfile",
            handler: function() {
              manageFileAttachment(rec);
            }
          }
        );
      }
      addMenu.showAt(e.getXY());
    },

    _onViewFileAttachment: function() {
      Ext.create(
        "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowIncidentsFileAttach",
        {
          modal: true
        }
      ).show();
    },

    _onSearchAdvancedIncident: function(btn) {
      if (
        btn
          .up("form")
          .getForm()
          .isValid()
      ) {
        var form = btn.up("form");
        var limit = ",";
        var panelReport = form.down("panel[name=panelReport]");
        var codeIncident =
          form.down("#codeIncident").getRawValue() == undefined
            ? ""
            : form.down("#codeIncident").getRawValue();
        var initDateReport =
          form.down("#dateRegisterInit").getRawValue() == undefined
            ? ""
            : form.down("#dateRegisterInit").getRawValue();
        var endDateReport =
          form.down("#dateRegisterEnd").getRawValue() == undefined
            ? ""
            : form.down("#dateRegisterEnd").getRawValue();
        var initDateOccurrence =
          form.down("#dateOccurrenceInit").getRawValue() == undefined
            ? ""
            : form.down("#dateOccurrenceInit").getRawValue();
        var endDateOccurrence =
          form.down("#dateOccurrenceEnd").getRawValue() == undefined
            ? ""
            : form.down("#dateOccurrenceEnd").getRawValue();
        var userName =
          form.down("#idUser").getValue() == undefined
            ? ""
            : form.down("#idUser").getValue();
        var stateIncident =
          form.down("#stateIncident").getValue() == undefined
            ? ""
            : form.down("#stateIncident").getValue();
        var area =
          form.down("#workArea").getValue() == undefined
            ? ""
            : form.down("#workArea").getValue();
        var agency =
          form.down("#agency").getValue() == undefined
            ? ""
            : form.down("#agency").getValue();
        var processOrigin =
          form.down("#processOrigin").getValue() == undefined
            ? ""
            : form.down("#processOrigin").getValue();
        var processImpact =
          form.down("#processImpact").getValue() == undefined
            ? ""
            : form.down("#processImpact").getValue();
        var grid = Ext.ComponentQuery.query(
          "ViewGridIncidentsRevisedRiskOperational"
        )[0];
        grid.store.getProxy().extraParams = {
          fields:
            "i.idIncident" +
            limit +
            "i.dateProcess" +
            limit +
            "i.dateProcess" +
            limit +
            "i.dateOccurrence" +
            limit +
            "i.dateOccurrence" +
            limit +
            "ur.idManagerRisk" +
            limit +
            "si.id" +
            limit +
            "w.idWorkArea" +
            limit +
            "a.idAgency" +
            limit +
            "po.idProcess" +
            limit +
            "pi.idProcess",
          values:
            codeIncident +
            limit +
            initDateReport +
            limit +
            endDateReport +
            limit +
            initDateOccurrence +
            limit +
            endDateOccurrence +
            limit +
            userName +
            limit +
            stateIncident +
            limit +
            area +
            limit +
            agency +
            limit +
            processOrigin +
            limit +
            processImpact,
          types:
            "Long" +
            limit +
            "Date" +
            limit +
            "Date" +
            limit +
            "Date" +
            limit +
            "Date" +
            limit +
            "String" +
            limit +
            "Integer" +
            limit +
            "Integer" +
            limit +
            "Integer" +
            limit +
            "Integer" +
            limit +
            "Integer",
          operators:
            "equal" +
            limit +
            "majorEqual" +
            limit +
            "minorEqual" +
            limit +
            "majorEqual" +
            limit +
            "minorEqual" +
            limit +
            "equal" +
            limit +
            "equal" +
            limit +
            "equal" +
            limit +
            "equal" +
            limit +
            "equal" +
            limit +
            "equal"
        };
        grid.store.getProxy().url =
          "http://localhost:9000/giro/advancedSearchIncident.htm";
        grid.down("pagingtoolbar").moveFirst();
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },

    _onDeleteFileAttachTrackIncident: function() {
      var k = Ext.ComponentQuery.query("ViewWindowFileAttachment")[0];
      var grid = k.down("grid");
      var row = grid.getSelectionModel().getSelection()[0];
      if (row === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        k.idFileAttachment = row.get("idFileAttachment");
        Ext.MessageBox.show({
          title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
          msg: "Esta seguro que desea eliminar el archivo adjunto",
          icon: Ext.Msg.WARNING,
          buttonText: {
            yes: "Si"
          },
          buttons: Ext.MessageBox.YESNO,
          fn: function(btn) {
            if (btn === "yes") {
              DukeSource.lib.Ajax.request({
                method: "POST",
                url:
                  "http://localhost:9000/giro/deleteFileAttachTrackIncident.htm",
                params: {
                  idFileAttachment: k.idFileAttachment,
                  idTracking: k.idTrackIncident
                },
                success: function(response) {
                  response = Ext.decode(response.responseText);
                  if (response.success) {
                    grid.store.getProxy().extraParams = {
                      idTracking: k.idTrackIncident
                    };
                    grid.store.getProxy().url =
                      "http://localhost:9000/giro/findFileAttachmentTrackingIncident.htm";
                    grid.down("pagingtoolbar").moveFirst();
                    Ext.ComponentQuery.query("ViewWindowRegisterTrace")[0]
                      .down("grid")
                      .getStore()
                      .load();
                  } else {
                    DukeSource.global.DirtyView.messageAlert(
                      DukeSource.global.GiroMessages.TITLE_ERROR,
                      response.mensaje,
                      Ext.Msg.ERROR
                    );
                  }
                },
                failure: function(response) {
                  DukeSource.global.DirtyView.messageAlert(
                    DukeSource.global.GiroMessages.TITLE_ERROR,
                    valor.mensaje,
                    Ext.Msg.ERROR
                  );
                }
              });
            }
          }
        });
      }
    },

    _onSaveFileAttachTrackIncident: function() {
      var k = Ext.ComponentQuery.query("ViewWindowFileAttachment")[0];
      var form = k.down("form");
      var grid = k.down("grid");
      if (form.getForm().isValid()) {
        form.getForm().submit({
          url:
            "http://localhost:9000/giro/saveFileAttachTrackIncident.htm?idTrackIncident=" +
            k.idTrackIncident,
          waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
          method: "POST",
          success: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (valor.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                valor.mensaje,
                Ext.Msg.INFO
              );
              grid.store.getProxy().extraParams = {
                idTracking: k.idTrackIncident
              };
              grid.store.getProxy().url =
                "http://localhost:9000/giro/findFileAttachmentTrackingIncident.htm";
              grid.down("pagingtoolbar").moveFirst();
              Ext.ComponentQuery.query("ViewWindowRegisterTrace")[0]
                .down("grid")
                .getStore()
                .load();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                valor.mensaje,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (!valor.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                valor.mensaje,
                Ext.Msg.ERROR
              );
            }
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },

    _onSearchGridFileAttachTrackIncident: function(text) {
      var grid = Ext.ComponentQuery.query("ViewWindowFileAttachment grid")[0];
      DukeSource.lib.TriggerSearchGrid.searchTriggerGeneral(text, grid);
    },

    _onSaveFileAttachmentIncident: function(btn) {
      var window = btn.up("window");
      var form = window.down("form");
      var grid = window.down("grid");
      if (form.getForm().isValid()) {
        form.getForm().submit({
          url:
            "http://localhost:9000/giro/saveFileAttachmentsIncidents.htm?nameView=ViewPanelIncidentsRiskOperational" +
            "&idDetailIncidents=" +
            window.idDetailIncidents +
            "&idIncident=" +
            window.idIncident,
          waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
          method: "POST",
          success: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (valor.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                valor.message,
                Ext.Msg.INFO
              );
              grid.store.getProxy().extraParams = {
                idDetailIncidents: window.idDetailIncidents,
                idIncident: window.idIncident,
                propertyOrder: "nameFile"
              };
              grid.store.getProxy().url =
                "http://localhost:9000/giro/showListFileAttachmentsIncidents.htm";
              grid.down("pagingtoolbar").moveFirst();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                valor.message,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function(form, action) {
            var valor = Ext.decode(action.response.responseText);
            if (!valor.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                valor.message,
                Ext.Msg.ERROR
              );
            }
          }
        });
      } else {
        DukeSource.global.DirtyView.messageNormal(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
      }
    },

    _onDeleteFileAttachmentIncident: function(btn) {
      var k = btn.up("window");
      var grid = k.down("grid");
      var row = grid.getSelectionModel().getSelection()[0];
      if (row === undefined) {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_ITEM,
          Ext.Msg.WARNING
        );
      } else {
        k.idFileAttachment = row.get("idFileAttachment");
        DukeSource.lib.Ajax.request({
          method: "POST",
          url: "http://localhost:9000/giro/deleteFileAttachmentIncident.htm",
          params: {
            idFileAttachment: k.idFileAttachment,
            idDetailIncidents: k.idDetailIncidents,
            idIncident: k.idIncident
          },
          success: function(response) {
            response = Ext.decode(response.responseText);
            if (response.success) {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_MESSAGE,
                response.message,
                Ext.Msg.INFO
              );
              grid.store.getProxy().extraParams = {
                idDetailIncidents: k.idDetailIncidents,
                idIncident: k.idIncident
              };
              grid.store.getProxy().url =
                "http://localhost:9000/giro/showListFileAttachmentsIncidents.htm";
              grid.down("pagingtoolbar").moveFirst();
            } else {
              DukeSource.global.DirtyView.messageAlert(
                DukeSource.global.GiroMessages.TITLE_ERROR,
                response.message,
                Ext.Msg.ERROR
              );
            }
          },
          failure: function(response) {
            DukeSource.global.DirtyView.messageAlert(
              DukeSource.global.GiroMessages.TITLE_ERROR,
              response.message,
              Ext.Msg.ERROR
            );
          }
        });
      }
    },

    _onApproveIncident: function(btn) {
      var win = btn.up("window");
      var form = win.down("form").getForm();

      DukeSource.lib.Ajax.request({
        waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
        method: "POST",
        url: "http://localhost:9000/giro/approveIncident.htm",
        params: {
          jsonData: Ext.JSON.encode(form.getValues()),
          module: win.down("#module").getValue()
        },
        scope: this,
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            DukeSource.global.DirtyView.messageNormal(response.message);
            win.parentWindow.close();
            win.close();

            var grid = Ext.ComponentQuery.query(
              "ViewGridIncidentsRevisedRiskOperational"
            )[0];
            grid.down("pagingtoolbar").moveFirst();
          } else {
            DukeSource.global.DirtyView.messageWarning(response.message);
          }
        },
        failure: function() {}
      });
    }
  }
);

function isConfidential(view, record) {
  if (record.get("confidential") == "CONFIDENCIAL") {
    var cbx = view.down("checkbox");
    cbx.lastValue = true;
    cbx.setRawValue(true);
    cbx.up("form").setBodyStyle("background", "#FFDC9E");
  }
}

function loadComboSecurity(view) {
  view
    .down("#criterionSafety")
    .getStore()
    .load();
  view
    .down("#subClassification")
    .getStore()
    .load();
  if (view.down("#subCategory") !== undefined)
    view
      .down("#subCategory")
      .getStore()
      .load();
  view
    .down("#hourInit")
    .getStore()
    .load();
  view
    .down("#hourEnd")
    .getStore()
    .load();
}

function manageFileAttachment(rec) {
  var idDetailIncidents = rec.get("idDetailIncidents");
  var idIncident = rec.get("idIncident");

  var k = Ext.create(
    "DukeSource.view.fulfillment.window.ViewWindowFileAttachment",
    {
      saveFile: "saveFileAttachmentsIncidents",
      deleteFile: "deleteFileAttachmentsIncidents",
      searchGridTrigger: "searchGridFileAttachmentDocument",
      src: "http://localhost:9000/giro/downloadFileAttachmentsIncidents.htm",
      params: ["idIncident", "idFileAttachment", "nameFile"]
    }
  );
  k.idDetailIncidents = idDetailIncidents == undefined ? "" : idDetailIncidents;
  k.idIncident = idIncident == undefined ? "" : idIncident;

  var gridFileAttachment = k.down("grid");
  gridFileAttachment.store.getProxy().extraParams = {
    idDetailIncidents: k.idDetailIncidents,
    idIncident: k.idIncident
  };
  gridFileAttachment.store.getProxy().url =
    "http://localhost:9000/giro/showListFileAttachmentsIncidents.htm";
  gridFileAttachment.down("pagingtoolbar").moveFirst();
  k.title =
    rec.get("idIncident") +
    " - " +
    k.title +
    " - " +
    rec.get("descriptionShort");
  k.show();
}
