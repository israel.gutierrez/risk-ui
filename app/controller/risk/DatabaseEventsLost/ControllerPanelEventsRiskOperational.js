Ext.define('DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelEventsRiskOperational', {
    alias: 'ControllerPanelEventsRiskOperational',
    extend: 'Ext.app.Controller',
    stores: [],
    models: [],
    views: [],
    init: function () {
        this.control({
            '[action=saveSubEvent]': {
                click: this._onSaveSubEvent
            },
            '[action=navigateEventState]': {
                click: this._onNavigateEventState
            },
            'ViewPanelEventsRiskOperational grid': {
                itemcontextmenu: this.rightClickEvent
            },
            'ViewWindowRegisterSubEvents grid': {
                itemdblclick: this._onLoadDetailSubEvent,
                itemcontextmenu: this.rightClickSubEvent
            },
            '[action=addSubEvents]': {
                click: this._onAddSubEvents
            },
            '[action=relationToRisk]': {
                click: this._onRelationToRisk
            },
            '[action=searchAdvancedEvent]': {
                click: this._onSearchAdvancedEvent
            },
            '[action=exportSearchAdvancedEventXls]': {
                click: this._onExportSearchAdvancedEventViewXls
            },
            '[action=deleteRelationEventRisk]': {
                click: this._onDeleteRelationEventRisk
            },
            '[action=saveFileAttachTrackEvent]': {
                click: this._saveFileAttachTrackEvent
            },
            '[action=deleteFileAttachTrackEvent]': {
                click: this._deleteFileAttachTrackEvent
            },
            '[action=approveEvent]': {
                click: this._approveEvent
            }
        });
    },
    _onSaveSubEvent: function (button) {
        var panel = Ext.ComponentQuery.query('ViewWindowRegisterSubEvents')[0];
        var grid = panel.down('grid');
        var windows = button.up('window');
        var form = windows.down('form');
        if (form.getForm().isValid()) {
            form.getForm().submit({
                url: 'http://localhost:9000/giro/saveEventMaster.htm?nameView=ViewPanelEventsRiskOperational',
                waitMsg: 'Saving...',
                method: 'POST',
                success: function (form, action) {
                    var valor = Ext.decode(action.response.responseText);

                    if (valor.success) {
                        DukeSource.global.DirtyView.messageNormal(valor.message);
                        windows.close();
                        grid.down('pagingtoolbar').moveFirst();
                    } else {
                        DukeSource.global.DirtyView.messageWarning(valor.message);
                    }
                },
                failure: function (form, action) {
                    var valor = Ext.decode(action.response.responseText);
                    if (!valor.success) {
                        DukeSource.global.DirtyView.messageWarning(valor.message);
                    }
                }
            });
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }
    },

    _onLoadEventsRisk: function (grid, record, index) {

        var nameWindow = codexCompany === 'es_cl_0001' ? 'WindowRegisterEventWizard' : 'ViewWindowRegisterEvents';

        var win = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.' + nameWindow, {
            actionType: record.get('sequenceEventState') === '' ? 'revised_manager' : 'modify',
            maximized: category === DukeSource.global.GiroConstants.ANALYST && codexCompany === 'es_cl_0001',
            record: record,
            width: 800,
            gridParent: grid,
            indexRecordParent: index,
            modal: true,
            new: false
        });
        DukeSource.lib.Ajax.request({
            method: 'POST',
            url: 'http://localhost:9000/giro/getEventMasterByIdEvent.htm',
            params: {
                idEvent: record.get('idEvent')
            },
            success: function (response) {
                response = Ext.decode(response.responseText);
                if (response.success) {

                    if (codexCompany === 'es_cl_0001') {
                        win.down('#typeEvent').getStore().load();
                        win.down('#currency').getStore().load();
                        win.down('#fileAttachment').setValue(record.get('fileAttachment'));
                        win.down('#eventMain').setValue('S');
                        win.down('#numberRelation').setValue(record.get('idEvent'));

                        win.title = win.title + "-" + record.get('descriptionShort');
                        win.down('form').getForm().setValues(response.data);
                        settingValuesEvents(win, response);
                        win.down('#gridApprove').store.getProxy().extraParams = {idEvent: record.get('idEvent')};
                        win.down('#gridApprove').store.getProxy().url = 'http://localhost:9000/giro/usersToApproveEvent.htm';
                        win.down('#gridApprove').store.load();

                        win.down('#collaboratorApprove').setVisible(true);
                        win.down('#gridApproveToCollaborator').store.getProxy().extraParams = {idEvent: record.get('idEvent')};
                        win.down('#gridApproveToCollaborator').store.getProxy().url = 'http://localhost:9000/giro/usersToApproveEvent.htm';
                        win.down('#gridApproveToCollaborator').store.load();
                        win.show();
                    } else {
                        win.down('form').query('.combobox').forEach(function (c) {
                            c.getStore().load();
                        });

                        win.down('#numberRelation').setValue(record.get('idEvent'));
                        win.down('#codeEventTemp').setValue(record.get('codeEvent'));
                        win.down('#dateRegisterTemp').setValue(record.get('dateRegister'));
                        win.down('#fullNameUserRegisterTemp').setValue(record.get('fullNameUserRegister'));
                        win.down('#workAreaUserRegisterTemp').setValue(record.get('workAreaUserRegister'));

                        win.down('form').getForm().setValues(response.data);
                        win.down('#btnApproveEvent').setVisible(true);
                        win.show();
                    }
                } else {
                    DukeSource.global.DirtyView.messageWarning(response.message);
                }
            },
            failure: function () {
            }
        });
    },

    _onLoadSubEventsRisk: function (view, record) {
        if (Ext.ComponentQuery.query('ViewWindowRegisterSubEvents')[0] === undefined) {

            DukeSource.lib.Ajax.request({
                method: 'POST',
                url: 'http://localhost:9000/giro/getEventMasterByIdEvent.htm',
                params: {
                    idEvent: record.get('idEvent')
                },
                success: function (response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        var win = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterSubEvents', {
                            width: 900,
                            modal: true,
                            recordMainEvent: response.data
                        });
                        win.setTitle(win.title + ' , ' + record.get('codeEvent'));
                        win.show();
                    } else {
                        DukeSource.global.DirtyView.messageWarning(response.message);
                    }
                },
                failure: function () {
                }
            });
        }
    },

    _onLoadDetailSubEvent: function (grid, record, index) {

        if (Ext.ComponentQuery.query('ViewWindowRegisterSubEventsDetail')[0] === undefined) {
            var win = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterEvents', {
                modal: true,
                width: 800,
                gridParent: grid,
                indexRecordParent: index
            });
            DukeSource.lib.Ajax.request({
                method: 'POST',
                url: 'http://localhost:9000/giro/getEventMasterByIdEvent.htm',
                params: {
                    idEvent: record.get('idEvent')
                },
                success: function (response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        win.down('form').query('.combobox').forEach(function (c) {
                            c.getStore().load();
                        });

                        win.down('#numberRelation').setValue(record.get('idEvent'));
                        win.down('#codeEventTemp').setValue(record.get('codeEvent'));
                        win.down('#dateRegisterTemp').setValue(record.get('dateRegister'));
                        win.down('#fullNameUserRegisterTemp').setValue(record.get('fullNameUserRegister'));
                        win.down('#workAreaUserRegisterTemp').setValue(record.get('workAreaUserRegister'));

                        win.down('form').getForm().setValues(response.data);
                        win.down('#btnApproveEvent').setVisible(true);
                        win.show();
                        // win.down('#saveEventOperational').setVisible(false);
                    } else {
                        DukeSource.global.DirtyView.messageAlert(response.message);
                    }
                },
                failure: function () {
                }
            });
        }
    },

    rightClickEvent: function (view, record, node, index, e) {
        var app = this.application;
        e.stopEvent();
        var addMenu = Ext.create('DukeSource.view.risk.DatabaseEventsLost.AddMenuEvent', {});
        addMenu.removeAll();

        if (category === DukeSource.global.GiroConstants.ANALYST) {
            addMenu.add(
                {
                    text: 'Modificar', iconCls: 'modify', handler: function () {
                        app.getController('DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelEventsRiskOperational')._onLoadEventsRisk(view, record, index);
                    }
                },
                {
                    text: getName('VPER_DBCLK_Tracking'),
                    hidden: hidden('VPER_DBCLK_Tracking'),
                    iconCls: 'tracing',
                    handler: function () {
                        var window = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.WindowRegisterTraceEvent', {
                            modal: true, idEvent: record.get('idEvent')
                        });
                        var idEvent = record.get('idEvent');
                        window.title = "Seguimiento de evento" + ' , ' + record.get('codeEvent');
                        window.show();
                        window.down('#dateTracking').setMinValue(record.get('dateOccurrence'));
                        window.down('#idEvent').setValue(idEvent);
                        window.down('#typeEvent').setValue(DukeSource.global.GiroConstants.EVENT);
                        window.down('#gridTrackEvent').store.getProxy().extraParams = {
                            idEvent: idEvent,
                            typeIncident: DukeSource.global.GiroConstants.EVENT
                        };
                        window.down('#gridTrackEvent').store.getProxy().url = 'http://localhost:9000/giro/showListTrackingEventActives.htm';
                        window.down('#gridTrackEvent').down('pagingtoolbar').moveFirst();
                    }
                },
                {
                    text: 'Recuperación',
                    iconCls: 'recovery',
                    handler: function () {
                        var window = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowAmountSureRecovery', {
                            modal: true,
                            father: view,
                            recordMainEvent: record
                        });
                        window.title = window.title + " &#8702; " + '<span style="color: red">' + record.get('codeEvent') + '</span>';
                        window.show();
                        window.down('#descriptionLarge').focus(false, 200);
                        window.down('#idEvent').setValue(record.get('idEvent'));
                        window.down('#codeAccount').setValue('');
                        var grid = window.down('grid');
                        DukeSource.global.DirtyView.searchPaginationGridNormal(record.get('idEvent'), grid, grid.down('pagingtoolbar'), 'http://localhost:9000/giro/findRecoveryEvent.htm', 'event.idEvent', 'event.idEvent')
                    }
                },
                {
                    text: getName('EVN_CPE_RCLK_Spends'),
                    iconCls: 'spend',
                    hidden: hidden('EVN_CPE_RCLK_Spends'),
                    handler: function () {
                        var window = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.WindowSpendEventLost', {
                            modal: true,
                            recordMainEvent: record
                        });
                        window.title = getName('EVN_CPE_RCLK_Spends') + " &#8702; " + '<span style="color: red">' + record.get('codeEvent') + '</span>';
                        window.show();
                        window.down('#descriptionLarge').focus(false, 100);
                        window.down('#idEvent').setValue(record.get('idEvent'));
                        var grid = window.down('grid');
                        grid.store.getProxy().extraParams = {
                            propertyFind: 'event.idEvent',
                            valueFind: record.get('idEvent'),
                            propertyOrder: 'event.idEvent',
                            typeRecovery: DukeSource.global.GiroConstants.TYPE_SPEND
                        };
                        grid.store.getProxy().url = 'http://localhost:9000/giro/findSpendEvent.htm';
                        grid.down('pagingtoolbar').doRefresh();
                    }
                },
                {
                    text: 'Pagos',
                    iconCls: 'coins_add',
                    hidden: hidden('EVN_CPE_RCLK_Payments'),
                    handler: function () {
                        var window = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.WindowPaymentRegister', {
                            modal: true,
                            recordMainEvent: record
                        });
                        window.title = window.title + " &#8702; " + '<span style="color: red">' + record.get('codeEvent') + '</span>';
                        window.show();
                        window.down('#descriptionLarge').focus(false, 100);
                        window.down('#idEvent').setValue(record.get('idEvent'));
                        var grid = window.down('grid');
                        grid.store.getProxy().extraParams = {
                            propertyFind: 'event.idEvent',
                            valueFind: record.get('idEvent'),
                            propertyOrder: 'event.idEvent',
                            typeRecovery: DukeSource.global.GiroConstants.TYPE_NONE
                        };
                        grid.store.getProxy().url = 'http://localhost:9000/giro/findSpendEvent.htm';
                        grid.down('pagingtoolbar').doRefresh();
                    }
                },
                {
                    text: 'Provisiones',
                    hidden: hidden('VPER_DBCLK_Provision'),
                    iconCls: 'spend',
                    handler: function () {
                        var window = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.WindowProvisionEventLost', {
                            modal: true,
                            recordMainEvent: record
                        });
                        window.title = window.title + ' , ' + record.get('codeEvent');
                        window.show();
                        window.down('#descriptionLarge').focus(false, 100);
                        window.down('#idEvent').setValue(record.get('idEvent'));
                        var grid = window.down('grid');
                        grid.store.getProxy().extraParams = {
                            propertyFind: 'event.idEvent',
                            valueFind: record.get('idEvent'),
                            propertyOrder: 'event.idEvent',
                            typeRecovery: DukeSource.global.GiroConstants.TYPE_PROVISION
                        };
                        grid.store.getProxy().url = 'http://localhost:9000/giro/findSpendEvent.htm';
                        grid.down('pagingtoolbar').moveFirst();
                    }
                },
                {
                    text: 'Riesgos',
                    iconCls: 'risk',
                    handler: function () {
                        if (Ext.ComponentQuery.query('ViewWindowRegisterEventsRisk')[0] === undefined) {
                            Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterEventsRisk').show();
                        }

                    }
                },
                {
                    text: 'Planes de Acción', iconCls: 'detail',
                    handler: function () {

                        Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterEventsActionPlan', {
                            modal: true,
                            idEvent: record.get('idEvent')
                        }).show();

                    }
                },
                {
                    text: 'Sub-eventos',
                    hidden: hidden('VPER_DBCLK_SubEvents'),
                    iconCls: 'sub_event',
                    handler: function () {
                        if (record.get('eventType') === '2') {
                            app.getController('DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelEventsRiskOperational')._onLoadSubEventsRisk(view, record);
                        } else {
                            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, "El evento seleccionado no es de tipo MULTIPLE", Ext.Msg.WARNING);
                        }
                    }
                },
                {
                    text: 'Gestionar Adjuntos', iconCls: 'gestionfile', handler: function () {
                        var window = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowEventsFileAttach', {
                            idEvent: record.get('idEvent'),
                            modal: true
                        });
                        window.title = window.title + ' , ' + record.get('codeEvent');
                        window.show();
                    }
                }
            );
        } else {
            addMenu.add(
                {
                    text: 'Gestionar Adjuntos', iconCls: 'gestionfile', handler: function () {
                        var window = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowEventsFileAttach', {
                            idEvent: record.get('idEvent'),
                            modal: true
                        });
                        window.title = window.title + ' , ' + record.get('codeEvent');
                        window.show();
                    }
                }
            );
        }
        addMenu.showAt(e.getXY());
    },

    rightClickSubEvent: function (view, rec, node, index, e) {
        var app = this.application;
        e.stopEvent();
        var addMenu = Ext.create('DukeSource.view.risk.DatabaseEventsLost.AddMenuEvent', {});
        addMenu.removeAll();
        addMenu.add(
            {
                text: 'Recuperación', iconCls: 'recovery',
                handler: function () {
                    var window = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowAmountSureRecovery', {
                        modal: true
                    });
                    window.title = 'Recuperacion de eventos de perdida' + ' , ' + rec.get('codeEvent');
                    window.show();
                    window.down('#descriptionLarge').focus(false, 100);
                    window.down('#idEvent').setValue(rec.get('idEvent'));
                    window.down('#numberRelation').setValue(rec.get('numberRelation'));
                    window.down('#codeAccount').setValue('');
                    var grid = window.down('grid');
                    DukeSource.global.DirtyView.searchPaginationGridNormal(rec.get('idEvent'), grid, grid.down('pagingtoolbar'), 'http://localhost:9000/giro/findRecoveryEvent.htm', 'event.idEvent', 'event.idEvent')
                }
            },
            {
                text: 'Gastos Relacionados', iconCls: 'spend',
                handler: function () {
                    var window = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.WindowSpendEventLost', {
                        modal: true
                    });
                    window.title = window.title + ' , ' + rec.get('codeEvent');
                    window.show();
                    window.down('#descriptionLarge').focus(false, 100);
                    window.down('#idEvent').setValue(rec.get('idEvent'));
                    window.down('#numberRelation').setValue(rec.get('numberRelation'));
                    var grid = window.down('grid');
                    grid.store.getProxy().extraParams = {
                        propertyFind: 'event.idEvent',
                        valueFind: rec.get('idEvent'),
                        propertyOrder: 'event.idEvent',
                        typeRecovery: DukeSource.global.GiroConstants.TYPE_SPEND
                    };
                    grid.store.getProxy().url = 'http://localhost:9000/giro/findSpendEvent.htm';
                    grid.down('pagingtoolbar').moveFirst();
                }
            },
            {
                text: 'Provisiones', iconCls: 'spend',
                handler: function () {
                    var window = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.WindowProvisionEventLost', {
                        modal: true
                    });
                    window.title = window.title + ' , ' + rec.get('codeEvent');
                    window.show();
                    window.down('#descriptionLarge').focus(false, 100);
                    window.down('#idEvent').setValue(rec.get('idEvent'));
                    window.down('#numberRelation').setValue(rec.get('numberRelation'));
                    var grid = window.down('grid');
                    grid.store.getProxy().extraParams = {
                        propertyFind: 'event.idEvent',
                        valueFind: rec.get('idEvent'),
                        propertyOrder: 'event.idEvent',
                        typeRecovery: DukeSource.global.GiroConstants.TYPE_PROVISION
                    };
                    grid.store.getProxy().url = 'http://localhost:9000/giro/findSpendEvent.htm';
                    grid.down('pagingtoolbar').moveFirst();
                }
            },
            {
                text: 'Gestionar Adjuntos', iconCls: 'gestionfile', handler: function () {
                    var window = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowEventsFileAttach', {
                        idEvent: rec.get('idEvent'),
                        modal: true
                    });
                    window.title = window.title + ' , ' + rec.get('codeEvent');
                    window.show();
                }
            },
            {
                text: 'Historial Sub-Eventos', iconCls: 'attachFile', handler: function () {
                    var window = Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowHistoricalEvent', {
                        modal: true
                    });
                    window.down('grid').getStore().load({
                        url: 'http://localhost:9000/giro/showListHistoricalEventActives.htm',
                        params: {
                            idEvent: rec.get('idEvent')
                        }
                    });
                    window.title = window.title + ' , ' + rec.get('codeEvent');
                    window.show();
                }
            }
        );
        addMenu.showAt(e.getXY());
    },

    _onAddSubEvents: function (btn, view) {
        var app = this.application;
        var panel = btn.up('ViewPanelEventsRiskOperational');
        var record = panel.down('grid').getSelectionModel().getSelection()[0];
        if (record === undefined) {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
        } else {
            if (record.get('eventType') == '2') {
                app.getController('DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelEventsRiskOperational')._onLoadSubEventsRisk(view, record);
            } else {
                DukeSource.global.DirtyView.messageWarning("El evento seleccionado no es de tipo MULTIPLE");
            }
        }
    },

    _onRelationToRisk: function (btn, view) {
        var panel = btn.up('ViewPanelEventsRiskOperational');
        var record = panel.down('grid').getSelectionModel().getSelection()[0];
        if (record === undefined) {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
        } else {
            if (Ext.ComponentQuery.query('ViewWindowRegisterEventsRisk')[0] === undefined) {
                Ext.create('DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterEventsRisk').show();
            }

        }
    },

    _onExportSearchAdvancedEventViewXls: function (btn) {
        var panel = btn.up('AdvancedSearchEvent');
        var form = btn.up('form');
        if (form.getForm().isValid()) {
            var codeEvent = form.down('#codeEvent').getValue() === '' ? 'T' : form.down('#codeEvent').getValue();
            var codeMigration = form.down('#codeMigration').getValue() === '' ? 'T' : form.down('#codeMigration').getRawValue();
            var stateEvent = form.down('#eventState').getValue() === null ? 'T' : form.down('#eventState').getValue();
            var userName = form.down('#idUser').getValue() === '' ? 'T' : form.down('#idUser').getValue();
            var idArea = form.down('#workArea').getValue() === null ? 'T' : form.down('#workArea').getValue();
            var agency = form.down('#agency').getValue() === null ? 'T' : form.down('#agency').getValue();
            var amount = form.down('#amount').getValue() === null ? 0 : form.down('#amount').getValue();
            var indicatorMigration = codeMigration === 'T' ? 'S' : 'N';
            var indicatorAmount = amount === 0 ? 'S' : 'N';

            var dateInit = form.down('#dateRegisterInit').getRawValue() === '' ? '01/01/1900 00:00' : form.down('#dateRegisterInit').getRawValue();
            var dateEnd = form.down('#dateRegisterEnd').getRawValue() === '' ? '31/12/2900 23:59' : form.down('#dateRegisterEnd').getRawValue();


            var value = codeEvent + ',' + codeMigration + ',' + stateEvent + ',' + userName + ',' + idArea + ',' + agency + ',' + amount + ',' + indicatorAmount + ',' + indicatorMigration + ',' + dateInit + ',' + dateEnd;

            var panelReport = panel.down('#panelReport');
            panelReport.removeAll();
            panelReport.add(
                {
                    xtype: 'component',
                    hidden: true,
                    autoEl: {
                        tag: 'iframe',
                        src: 'http://localhost:9000/giro/xlsGeneralReport.htm?values=' + value +
                            '&names=' + 'codeEvent,codeMigration,stateEvent,userName,idArea,idAgency,amount,indicatorAmount,indicatorMigration,dateInit,dateEnd' +
                            '&types=' + 'String,String,String,String,String,String,BigDecimal,String,String,Timestamp,Timestamp' +
                            '&nameReport=' + nameReport('ReportEvent_option_06') +
                            '&nameDownload=report_event'
                    }
                }
            );
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }
    },

    _onSearchAdvancedEvent: function (btn) {
        if (btn.up('form').getForm().isValid()) {
            if (btn.up('form').getForm().isValid()) {
                var form = btn.up('form');
                var lm = ',';

                var codeEvent = form.down('#codeEvent').getRawValue();
                var initDateReport = form.down('#dateRegisterInit').getRawValue();
                var endDateReport = form.down('#dateRegisterEnd').getRawValue();
                var codeMigration = form.down('#codeMigration').getRawValue();
                var eventState = form.down('#eventState').getValue() == null ? '' : form.down('#eventState').getValue();
                var userName = form.down('#idUser').getValue();
                var area = form.down('#workArea').getValue() == null ? '' : form.down('#workArea').getValue();
                var agency = form.down('#agency').getValue() === null ? '' : form.down('#agency').getValue();
                var amount = form.down('#amount').getValue() == null ? '' : form.down('#amount').getValue();
                var grid = Ext.ComponentQuery.query('ViewPanelEventsRiskOperational grid')[0];
                grid.store.getProxy().extraParams = {
                    fields: 'ev.codeEvent,es.id,ev.codeMigration,ev.dateAcceptLossEvent,ev.dateAcceptLossEvent,ev.audit.userInsert,wa.id,a.id,ev.grossLoss',
                    values: codeEvent + lm + eventState + lm + codeMigration + lm + initDateReport + lm + endDateReport + lm + userName + lm + area + lm + agency + lm + amount,
                    types: 'String,Integer,String,Date,Date,String,Integer,Integer,BigDecimal',
                    operators: 'equal,equal,equal,majorEqual,minorEqual,equal,equal,equal,equal',
                    searchIn: 'Event'
                };
                grid.store.getProxy().url = 'http://localhost:9000/giro/advancedSearchEvent.htm';
                grid.down('pagingtoolbar').doRefresh();

            } else {
                DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
            }
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }
    },

    _onDeleteRelationEventRisk: function (btn) {
        var grid = btn.up('window').down('#gridRiskEvent');
        var recordEvent = Ext.ComponentQuery.query('ViewPanelEventsRiskOperational grid')[0].getSelectionModel().getSelection()[0];
        var record = grid.getSelectionModel().getSelection()[0];
        if (record === undefined) {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
        } else {
            Ext.MessageBox.show({
                title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                msg: 'Esta seguro que desea continuar?',
                icon: Ext.Msg.QUESTION,
                buttonText: {
                    yes: "Si"
                },
                buttons: Ext.MessageBox.YESNO,
                fn: function (btn) {
                    if (btn === 'yes') {
                        DukeSource.lib.Ajax.request({
                            method: 'POST',
                            url: 'http://localhost:9000/giro/deleteRelationEventRisk.htm?nameView=ViewPanelEventsRiskOperational',
                            params: {
                                jsonData: Ext.JSON.encode(record.data),
                                idEvent: recordEvent.get('idEvent'),
                                idRisk: record.get('idRisk'),
                                versionRisk: record.get('versionCorrelative')
                            },
                            success: function (response) {
                                response = Ext.decode(response.responseText);
                                if (response.success) {
                                    DukeSource.global.DirtyView.messageNormal(response.message);
                                    grid.down('pagingtoolbar').doRefresh();

                                    var panelRisk = Ext.ComponentQuery.query('ViewPanelEvaluationRiskOperational')[0];

                                    if (panelRisk !== undefined) {
                                        panelRisk.down('#amountEventLost').setValue(response.data['amountEventLost']);
                                        panelRisk.down('#numberEventLost').setValue(response.data['numberEventLost']);
                                    }

                                } else {
                                    DukeSource.global.DirtyView.messageWarning(response.message);
                                }
                            },
                            failure: function () {
                            }
                        });
                    }
                }
            });
        }
    },

    _saveFileAttachTrackEvent: function (btn) {
        var k = Ext.ComponentQuery.query('ViewWindowFileAttachment')[0];
        var form = k.down('form');
        var grid = k.down('grid');
        if (form.getForm().isValid()) {
            form.getForm().submit({
                url: 'http://localhost:9000/giro/saveFileAttachTrackEvent.htm?idTracking=' + k.idTracking,
                waitMsg: DukeSource.global.GiroMessages.MESSAGE_LOADING,
                method: 'POST',
                success: function (form, action) {
                    var valor = Ext.decode(action.response.responseText);
                    if (valor.success) {
                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_MESSAGE, valor.mensaje,
                            Ext.Msg.INFO);
                        grid.store.getProxy().extraParams = {
                            idTracking: k.idTracking
                        };
                        grid.store.getProxy().url = 'http://localhost:9000/giro/findFileAttachmentTrackingEvent.htm';
                        grid.down('pagingtoolbar').moveFirst();
                    } else {
                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_ERROR, valor.mensaje,
                            Ext.Msg.ERROR);
                    }
                },
                failure: function (form, action) {
                    var valor = Ext.decode(action.response.responseText);
                    if (!valor.success) {
                        DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_ERROR, valor.mensaje,
                            Ext.Msg.ERROR);
                    }

                }
            })
        } else {
            DukeSource.global.DirtyView.messageAlert(DukeSource.global.GiroMessages.TITLE_WARNING, DukeSource.global.GiroMessages.MESSAGE_COMPLETE, Ext.Msg.ERROR);
        }
    },

    _deleteFileAttachTrackEvent: function (btn) {
        var k = Ext.ComponentQuery.query('ViewWindowFileAttachment')[0];
        var grid = k.down('grid');
        var row = grid.getSelectionModel().getSelection()[0];
        if (row === undefined) {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_ITEM);
        } else {
            k.idFileAttachment = row.get('idFileAttachment');
            Ext.MessageBox.show({
                title: DukeSource.global.GiroMessages.TITLE_CONFIRM,
                msg: 'Esta seguro que desea eliminar el archivo adjunto?',
                icon: Ext.Msg.WARNING,
                buttonText: {
                    yes: "Si"
                },
                buttons: Ext.MessageBox.YESNO,
                fn: function (btn) {
                    if (btn === 'yes') {
                        DukeSource.lib.Ajax.request({
                            method: 'POST',
                            url: 'http://localhost:9000/giro/deleteFileAttachTrackEvent.htm',
                            params: {
                                idFileAttachment: k.idFileAttachment,
                                idTracking: k.idTracking
                            },
                            success: function (response) {
                                response = Ext.decode(response.responseText);
                                if (response.success) {
                                    grid.store.getProxy().extraParams = {
                                        idTracking: k.idTracking
                                    };
                                    grid.store.getProxy().url = 'http://localhost:9000/giro/findFileAttachmentTrackingEvent.htm';
                                    grid.down('pagingtoolbar').moveFirst();
                                } else {
                                    DukeSource.global.DirtyView.messageWarning(response.message);
                                }
                            },
                            failure: function (response) {
                                DukeSource.global.DirtyView.messageWarning(response.message);
                            }
                        });
                    }
                }
            });
        }
    },

    _approveEvent: function (btn) {
        var win = btn.up('window');

        var form = win.down('form').getForm();

        if (form.isValid()) {
            DukeSource.lib.Ajax.request({
                waitMsg: DukeSource.global.GiroMessages.MESSAGE_SAVING,
                method: 'POST',
                url: 'http://localhost:9000/giro/approveEvent.htm',
                params: {
                    jsonData: Ext.JSON.encode(form.getValues())
                },
                scope: this,
                success: function (response) {
                    response = Ext.decode(response.responseText);
                    if (response.success) {
                        DukeSource.global.DirtyView.messageNormal(response.message);

                        win.close();
                        win.parentWindow.callback(win.parentWindow.gridParent, win.parentWindow.indexRecordParent);
                        win.parentWindow.close();

                    } else {
                        DukeSource.global.DirtyView.messageWarning(response.message);
                    }
                },
                failure: function () {
                }
            });
        } else {
            DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
        }
    },

    _onNavigateEventState: function (btn) {
        var panel = btn.up('window');
        var form = panel.down('form');
        var position = parseInt(btn.index);
        form.getLayout().setActiveItem(position - 1);
    }
});

function settingValuesEvents(win, response) {
    var temp = response.data.stateTemp;
    var checks = win.down('#checksItems');
    checks.query('.checkboxfield').forEach(function (c) {
        c.setValue(temp)
    });

    win.down('#costCenterTempOne').setValue(response.data.costCenter);
    win.down('#descriptionTittleEventTempOne').setValue(response.data.descriptionTittleEvent);
    win.down('#codeEventTempOne').setValue(response.data.codeEvent);
    win.down('#amountOriginTempOne').setValue(response.data.grossLoss);
    win.down('#dateProcessTempOne').setValue(response.data.dateAcceptLossEvent);
    win.down('#descriptionUnityTempOne').setValue(response.data.descriptionUnity);
    win.down('#descriptionLargeTempOne').setValue(response.data.descriptionLarge);


    win.down('#costCenterTempTwo').setValue(response.data.costCenter);
    win.down('#descriptionTittleEventTempTwo').setValue(response.data.descriptionTittleEvent);
    win.down('#codeEventTempTwo').setValue(response.data.codeEvent);
    win.down('#amountOriginTempTwo').setValue(response.data.grossLoss);
    win.down('#descriptionUnityTempTwo').setValue(response.data.descriptionUnity);
    win.down('#descriptionLargeTempTwo').setValue(response.data.descriptionLarge);

}