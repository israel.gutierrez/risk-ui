Ext.define(
  "DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelReportEventsLostIGROP",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboEventOne",
      "risk.parameter.combos.ViewComboEventTwo",
      "risk.User.combos.ViewComboAgency",
      "risk.parameter.combos.ViewComboBusinessLineOne",
      "risk.parameter.combos.ViewComboBusinessLineTwo",
      "risk.parameter.combos.ViewComboProcess",
      "risk.parameter.combos.ViewComboSubProcess",
      "risk.parameter.combos.ViewComboWorkArea",
      "risk.DatabaseEventsLost.combos.ViewComboGroupIncident"
    ],
    init: function() {
      this.control({
        "[action=generateReportEventsLostIGROPPdf]": {
          click: this._onGenerateReportEventsLostIGROPPdf
        },
        "[action=generateReportEventsLostIGROPXls]": {
          click: this._onGenerateReportEventsLostIGROPXls
        }
      });
    },

    _onGenerateReportEventsLostIGROPPdf: function(btn) {
      if (
        btn
          .up("form")
          .getForm()
          .isValid()
      ) {
        var container = btn.up("container[name=generalContainer]");
        var businessLineOne =
          container.down("ViewComboBusinessLineOne").getValue() == undefined
            ? "T"
            : container.down("ViewComboBusinessLineOne").getValue();
        var eventOne =
          container.down("ViewComboEventOne").getValue() == undefined
            ? "T"
            : container.down("ViewComboEventOne").getValue();
        var eventTwo =
          container.down("ViewComboEventTwo").getValue() == undefined
            ? "T"
            : container.down("ViewComboEventTwo").getValue();
        var amount =
          container.down("#amountSearch").getValue() == undefined
            ? "0"
            : container.down("#amountSearch").getValue();
        var value =
          container.down("datefield[name=dateInit]").getRawValue() +
          "," +
          container.down("datefield[name=dateEnd]").getRawValue() +
          "," +
          businessLineOne +
          "," +
          eventOne +
          "," +
          eventTwo +
          "," +
          amount;
        var panelReport = container.down("panel[name=panelReport]");
        panelReport.removeAll();
        panelReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            src:
              "http://localhost:9000/giro/pdfGeneralReport.htm?values=" +
              value +
              "&names=" +
              "dateInit,dateEnd,businessLineOne,eventOne,eventTwo,amount" +
              "&types=" +
              "Date,Date,String,String,String,String" +
              "&nameReport=" +
              "RORIEV0004XLS"
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onGenerateReportEventsLostIGROPXls: function(btn) {
      if (
        btn
          .up("form")
          .getForm()
          .isValid()
      ) {
        var container = btn.up("container[name=generalContainer]");
        var businessLineOne =
          container.down("ViewComboBusinessLineOne").getValue() == undefined
            ? "T"
            : container.down("ViewComboBusinessLineOne").getValue();
        var eventOne =
          container.down("ViewComboEventOne").getValue() == undefined
            ? "T"
            : container.down("ViewComboEventOne").getValue();
        var eventTwo =
          container.down("ViewComboEventTwo").getValue() == undefined
            ? "T"
            : container.down("ViewComboEventTwo").getValue();
        var amount =
          container.down("#amountSearch").getValue() == undefined
            ? "0"
            : container.down("#amountSearch").getValue();
        var value =
          container.down("datefield[name=dateInit]").getRawValue() +
          "," +
          container.down("datefield[name=dateEnd]").getRawValue() +
          "," +
          businessLineOne +
          "," +
          eventOne +
          "," +
          eventTwo +
          "," +
          amount;
        var panelReport = container.down("panel[name=panelReport]");
        panelReport.removeAll();
        panelReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            src:
              "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
              value +
              "&names=" +
              "dateInit,dateEnd,businessLineOne,eventOne,eventTwo,amount" +
              "&types=" +
              "Date,Date,String,String,String,String" +
              "&nameReport=" +
              "RORIEV0004XLS"
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    }
  }
);
