Ext.define(
  "DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelReportIncidents",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: ["risk.User.combos.ViewComboAgency"],
    init: function() {
      this.control({
        "[action=generateReportIncidents]": {
          click: this._onGenerateReportIncidents
        }
      });
    },
    _onGenerateReportIncidents: function(btn) {
      var form = btn.up("form");
      if (form.getForm().isValid()) {
        var view = Ext.ComponentQuery.query("ViewPanelReportIncidents")[0];
        var nameDownload = view.down("#nameFile").getValue();
        var nameReport = view.down("#nameReport").getValue();

        var workArea =
          form.down("#unity").getValue() === ""
            ? "T"
            : form.down("#unity").getValue();
        var stateIncident =
          form.down("#stateIncident").getValue() == null
            ? "T"
            : form.down("#stateIncident").getValue();
        var value =
          form.down("#dateInit").getRawValue() +
          "," +
          form.down("#dateEnd").getRawValue() +
          "," +
          stateIncident +
          "," +
          workArea;

        var containerReport = view.down("#containerReport");
        containerReport.removeAll();
        containerReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            src:
              "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
              value +
              "&names=" +
              "dateInit,dateEnd,stateIncident,workArea" +
              "&types=" +
              "Timestamp,Timestamp,String,String" +
              "&nameReport=" +
              nameReport +
              "&nameDownload=" +
              nameDownload
          }
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
      }
    }
  }
);
