Ext.define(
  "DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelReportEventLoss",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: ["risk.User.combos.ViewComboAgency"],
    init: function() {
      this.control({
        "[action=generateReportsEventLoss]": {
          click: this._onGenerateReportsEventLoss
        }
      });
    },

    _onGenerateReportsEventLoss: function(btn) {
      var form = btn.up("form");
      if (form.getForm().isValid()) {
        var view = Ext.ComponentQuery.query("ViewPanelReportEventLoss")[0];
        var nameDownload = view.down("#nameFile").getValue();
        var nameReport = view.down("#nameReport").getValue();

        var unity =
          form.down("#unity").getValue() === ""
            ? "T"
            : form.down("#unity").getValue();
        var eventState =
          form.down("#eventState").getValue() === null
            ? "T"
            : form.down("#eventState").getValue();
        var agency =
          form.down("#agency").getValue() === null
            ? "T"
            : form.down("#agency").getValue();
        var amount =
          form.down("#amount").getValue() === null
            ? "0.00"
            : form.down("NumberDecimalNumber[name=amountSearch]").getValue();

        var value =
          form.down("#dateInit").getRawValue() +
          "," +
          form.down("#dateEnd").getRawValue() +
          "," +
          eventState +
          "," +
          unity +
          "," +
          agency +
          "," +
          amount;

        var containerReport = view.down("#containerReport");
        containerReport.removeAll();
        containerReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            src:
              "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
              value +
              "&names=" +
              "dateInit,dateEnd,idEventState,idArea,idAgency,amount" +
              "&types=" +
              "Timestamp,Timestamp,String,String,String,BigDecimal" +
              "&nameReport=" +
              nameReport +
              "&nameDownload=" +
              nameDownload
          }
        });
      } else {
        DukeSource.global.DirtyView.messageWarning(DukeSource.global.GiroMessages.MESSAGE_COMPLETE);
      }
    }
  }
);
