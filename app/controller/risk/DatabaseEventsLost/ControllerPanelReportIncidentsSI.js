Ext.define(
  "DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelReportIncidentsSI",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboEventOne",
      "risk.parameter.combos.ViewComboEventTwo",
      "risk.User.combos.ViewComboAgency",
      "risk.parameter.combos.ViewComboBusinessLineOne",
      "risk.parameter.combos.ViewComboBusinessLineTwo",
      "risk.parameter.combos.ViewComboProcess",
      "risk.parameter.combos.ViewComboSubProcess",
      "risk.parameter.combos.ViewComboWorkArea",
      "risk.DatabaseEventsLost.combos.ViewComboGroupIncident"
    ],
    init: function() {
      this.control({
        "[action=generateReportIncidentsSIPdf]": {
          click: this._onGenerateReportIncidentsSIPdf
        },
        "[action=generateReportIncidentsSIXls]": {
          click: this._onGenerateReportIncidentsSIXls
        }
      });
    },
    _onGenerateReportIncidentsSIPdf: function(btn) {
      if (
        btn
          .up("form")
          .getForm()
          .isValid()
      ) {
        var container = btn.up("container[name=generalContainer]");
        var workArea =
          container.down("ViewComboWorkArea").getValue() == undefined
            ? "T"
            : container.down("ViewComboWorkArea").getValue();
        var stateIncident =
          container.down("combobox[name=stateIncident]").getValue() == undefined
            ? "T"
            : container.down("combobox[name=stateIncident]").getValue();
        var agency =
          container.down("ViewComboAgency").getValue() == undefined
            ? "T"
            : container.down("ViewComboAgency").getValue();
        var dateInitOcu = container.down("#dateInitOcu").getRawValue();
        var dateEndOcu = container.down("#dateEndOcu").getRawValue();
        var dateInit =
          container.down("#dateInit").getRawValue() == ""
            ? "1990-01-01"
            : container.down("#dateInit").getRawValue();
        var dateEnd =
          container.down("#dateEnd").getRawValue() == ""
            ? "2099-12-31"
            : container.down("#dateEnd").getRawValue();
        var value =
          dateInit +
          "," +
          dateEnd +
          "," +
          stateIncident +
          "," +
          workArea +
          "," +
          agency +
          "," +
          dateInitOcu +
          "," +
          dateEndOcu;
        var panelReport = container.down("panel[name=panelReport]");
        panelReport.removeAll();
        panelReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            src:
              "http://localhost:9000/giro/pdfGeneralReport.htm?values=" +
              value +
              "&names=" +
              "dateInit,dateEnd,stateIncident,workArea,agency,dateInitOcu,dateEndOcu" +
              "&types=" +
              "Date,Date,String,String,String,Date,Date" +
              "&nameReport=" +
              "SIEVEN0001"
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    },
    _onGenerateReportIncidentsSIXls: function(btn) {
      if (
        btn
          .up("form")
          .getForm()
          .isValid()
      ) {
        var container = btn.up("container[name=generalContainer]");
        var stateIncident =
          container.down("combobox[name=stateIncident]").getValue() == undefined
            ? "T"
            : container.down("combobox[name=stateIncident]").getValue();
        var workArea =
          container.down("ViewComboWorkArea").getValue() == undefined
            ? "T"
            : container.down("ViewComboWorkArea").getValue();
        var agency =
          container.down("ViewComboAgency").getValue() == undefined
            ? "T"
            : container.down("ViewComboAgency").getValue();
        var dateInitOcu = container.down("#dateInitOcu").getRawValue();
        var dateEndOcu = container.down("#dateEndOcu").getRawValue();
        var dateInit =
          container.down("#dateInit").getRawValue() == ""
            ? "01/01/1999"
            : container.down("#dateInit").getRawValue();
        var dateEnd =
          container.down("#dateEnd").getRawValue() == ""
            ? "01/01/2999"
            : container.down("#dateEnd").getRawValue();
        var value =
          dateInit +
          "," +
          dateEnd +
          "," +
          stateIncident +
          "," +
          workArea +
          "," +
          agency +
          "," +
          dateInitOcu +
          "," +
          dateEndOcu;
        var panelReport = container.down("panel[name=panelReport]");
        panelReport.removeAll();
        panelReport.add({
          xtype: "component",
          autoEl: {
            tag: "iframe",
            src:
              "http://localhost:9000/giro/xlsGeneralReport.htm?values=" +
              value +
              "&names=" +
              "dateInit,dateEnd,stateIncident,workArea,agency,dateInitOcu,dateEndOcu" +
              "&types=" +
              "Date,Date,String,String,String,Date,Date" +
              "&nameReport=" +
              "SIEVEN0001XLS"
          }
        });
      } else {
        DukeSource.global.DirtyView.messageAlert(
          DukeSource.global.GiroMessages.TITLE_WARNING,
          DukeSource.global.GiroMessages.MESSAGE_COMPLETE,
          Ext.Msg.ERROR
        );
      }
    }
  }
);
