Ext.define(
  "DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelIncidentsBusinessContinuity",
  {
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [
      "risk.parameter.combos.ViewComboSafetyCriterion",
      "risk.parameter.combos.ViewComboWorkArea",
      "risk.parameter.combos.ViewComboRiskType",
      "risk.User.combos.ViewComboAgency",
      "risk.parameter.combos.ViewComboSubClassification",
      "risk.util.search.AdvancedSearchRisk",
      "risk.parameter.combos.ViewComboProcessType",
      "risk.parameter.combos.ViewComboProcess"
    ],
    init: function() {
      this.control({
        " ViewPanelIncidentsBusinessContinuity ViewGridIncidentsPendingRiskOperational": {
          itemdblclick: this._showModifyDetailIncidentBusinessContinuity,
          itemcontextmenu: this._rightClickIncidentsPendingCN
        },
        " ViewPanelIncidentsBusinessContinuity ViewGridIncidentsRevisedBusinessContinuity": {
          itemdblclick: this._showModifyIncidentBusinessContinuity,
          itemcontextmenu: this._rightClickIncidentsCN
        }
      });
    },
    _showModifyDetailIncidentBusinessContinuity: function(grid, record) {
      if (
        Ext.ComponentQuery.query("ViewWindowRegisterIncidentsCN")[0] ==
        undefined
      ) {
        var windows = Ext.create(
          "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterIncidentsCN",
          {
            typeIncident: record.get("indicatorIncidents"),
            record: record.data
          }
        );

        windows
          .down("#agency")
          .getStore()
          .load({
            callback: function() {
              if (
                !(
                  record.get("workArea") == "" ||
                  record.get("workArea") == undefined
                )
              ) {
                windows
                  .down("#workArea")
                  .getStore()
                  .load();
              }
              if (
                !(
                  record.get("factorRisk") == "" ||
                  record.get("factorRisk") == undefined
                )
              ) {
                windows
                  .down("#factorRisk")
                  .getStore()
                  .load();
              }
              if (!(record.get("lostType") == "2")) {
                windows
                  .down("#currency")
                  .getStore()
                  .load();
                windows.down("#groupLostAmount").setVisible(true);
                windows.down("#amountLoss").allowBlank = false;
                windows
                  .down("#amountLoss")
                  .setFieldStyle("background: #d9ffdb");
                windows.down("#currency").allowBlank = false;
                windows.down("#currency").setFieldStyle("background: #d9ffdb");
              }
              windows
                .down("form")
                .getForm()
                .setValues(record.data);
              windows.show();
            }
          });
      }
    },
    _showModifyIncidentBusinessContinuity: function(grid, record) {
      var row = grid.getSelectionModel().getSelection()[0];

      if (
        Ext.ComponentQuery.query("ViewWindowRegisterIncidentsCN")[0] ==
        undefined
      ) {
        var windows = Ext.create(
          "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowRegisterIncidentsCN",
          {
            typeIncident: record.get("indicatorIncidents")
          }
        );
        if (record.get("indicatorIncidents") == "P") {
          windows
            .down("#workArea")
            .getStore()
            .load();
          windows
            .down("#agency")
            .getStore()
            .load({
              callback: function() {
                if (record.get("typeIncident") == "SI")
                  loadComboSecurity(windows);
                windows
                  .down("form")
                  .getForm()
                  .setValues(record.data);
                if (record.get("typeIncident") == "SI") {
                  windows
                    .down("#criterionSafety")
                    .setValue(record.get("criterionSafety").split(","));
                }
                isConfidential(windows, record);
                windows.show();
              }
            });
        } else if (row.get("indicatorIncidents") == "R") {
          windows
            .down("#workArea")
            .getStore()
            .load();
          windows
            .down("#factorRisk")
            .getStore()
            .load();
          windows
            .down("#lostType")
            .getStore()
            .load();
          var amountInt = windows.down("#amountLoss");
          var currency = windows.down("#currency");
          if (row.get("lostType") == "2") {
            amountInt.setValue("");
            amountInt.allowBlank = true;
            amountInt.setEditable(false);
            currency.allowBlank = true;
            currency.setFieldStyle("background: #FFF");
            amountInt.setFieldStyle("background: #FFF");
          } else {
            windows
              .down("#currency")
              .getStore()
              .load();
          }
          windows
            .down("#agency")
            .getStore()
            .load({
              callback: function() {
                if (record.get("typeIncident") == "SI")
                  loadComboSecurity(windows);
                windows
                  .down("form")
                  .getForm()
                  .setValues(row.data);
                if (record.get("typeIncident") == "SI") {
                  windows
                    .down("#criterionSafety")
                    .setValue(record.get("criterionSafety").split(","));
                }
                isConfidential(windows, record);
                windows.show();
              }
            });
        } else {
          if (row.get("indicatorIncidents") == "X") {
            windows.query(".textfield,.numberfield").forEach(function(c) {
              DukeSource.global.DirtyView.toReadOnly(c);
            });
            windows.down("button[action=saveIncidents]").setDisabled(true);
            windows.down("checkbox[name=confidential]").setVisible(false);
          }
          windows
            .down("#workArea")
            .getStore()
            .load();
          windows
            .down("#factorRisk")
            .getStore()
            .load();
          windows
            .down("#lostType")
            .getStore()
            .load();
          windows
            .down("#incidentsGroup")
            .getStore()
            .load();
          var amountInt = windows.down("#amountLoss");
          var currency = windows.down("#currency");
          if (row.get("lostType") == "2") {
            amountInt.setValue("");
            amountInt.allowBlank = true;
            amountInt.setEditable(false);
            currency.allowBlank = true;
            currency.setFieldStyle("background: #FFF");
            amountInt.setFieldStyle("background: #FFF");
          } else {
            windows
              .down("#currency")
              .getStore()
              .load();
          }
          windows
            .down("#eventOne")
            .getStore()
            .load();
          windows
            .down("#eventTwo")
            .getStore()
            .load({
              url:
                "http://localhost:9000/giro/showListEventTwoActivesComboBox.htm",
              params: {
                valueFind: row.get("eventOne")
              }
            });
          windows.down("#eventTwo").setDisabled(false);
          windows
            .down("#eventThree")
            .getStore()
            .load({
              url:
                "http://localhost:9000/giro/showListEventThreeActivesComboBox.htm",
              params: {
                valueFind: row.get("eventOne"),
                valueFind2: row.get("eventTwo")
              }
            });
          windows.down("#eventThree").setDisabled(false);

          if (row.get("originIncident") == "CO") {
            windows.down("#containerThreeColumn").add(2, {
              xtype: "container",
              itemId: "containerUserReport",
              height: 26,
              layout: "hbox",
              items: [
                {
                  xtype: "textfield",
                  name: "userReport",
                  itemId: "userReport",
                  hidden: true
                },
                {
                  xtype:"UpperCaseTextFieldReadOnly",
                  flex: 1,
                  allowBlank: false,
                  value: row.get("nameUserRegister"),
                  name: "fullNameUserReport",
                  itemId: "fullNameUserReport",
                  fieldLabel: "QUIEN REPORTO"
                },
                {
                  xtype: "button",
                  iconCls: "search",
                  handler: function() {
                    var win = Ext.create(
                      "DukeSource.view.risk.util.search.SearchUser",
                      {
                        modal: true
                      }
                    ).show();
                    win.down("grid").on("itemdblclick", function() {
                      windows.down("textfield[name=userReport]").setValue(
                        win
                          .down("grid")
                          .getSelectionModel()
                          .getSelection()[0]
                          .get("userName")
                      );
                      windows
                        .down(
                          "UpperCaseTextFieldReadOnly[name=fullNameUserReport]"
                        )
                        .setValue(
                          win
                            .down("grid")
                            .getSelectionModel()
                            .getSelection()[0]
                            .get("fullName")
                        );
                      win.close();
                    });
                  }
                }
              ]
            });
            windows.down("#userReport").setValue(row.get("userRegister"));
          }
          windows
            .down("#impactReport")
            .getStore()
            .load();
          windows
            .down("#processOrigin")
            .getStore()
            .load();
          windows
            .down("#subProcessOrigin")
            .getStore()
            .load({
              url: "http://localhost:9000/giro/showListSubProcessActives.htm",
              params: {
                valueFind: row.get("processOrigin")
              }
            });
          windows.down("#subProcessOrigin").setDisabled(false);
          windows
            .down("#processImpact")
            .getStore()
            .load();
          windows
            .down("#subProcessImpact")
            .getStore()
            .load({
              url: "http://localhost:9000/giro/showListSubProcessActives.htm",
              params: {
                valueFind: row.get("processImpact")
              }
            });
          windows.down("#subProcessImpact").setDisabled(false);

          windows
            .down("#originIncident")
            .getStore()
            .load();
          windows
            .down("#indicatorIncidents")
            .getStore()
            .load();
          windows
            .down("#businessLineOne")
            .getStore()
            .load();
          windows
            .down("#businessLineTwo")
            .getStore()
            .load({
              url:
                "http://localhost:9000/giro/showListBusinessLineTwoActivesComboBox.htm",
              params: {
                valueFind: row.get("businessLineOne")
              }
            });
          windows.down("#businessLineTwo").setDisabled(false);
          windows
            .down("#businessLineThree")
            .getStore()
            .load({
              url:
                "http://localhost:9000/giro/showListBusinessLineThreeActivesComboBox.htm",
              params: {
                idBusinessLineOne: row.get("businessLineOne"),
                idBusinessLineTwo: row.get("businessLineTwo")
              }
            });
          windows.down("#businessLineThree").setDisabled(false);
          if (windows.down("#indicatorIncentive") != undefined)
            windows
              .down("#indicatorIncentive")
              .getStore()
              .load();
          windows
            .down("#agency")
            .getStore()
            .load({
              callback: function() {
                if (category == "ANALISTASI") loadComboSecurity(windows);
                windows
                  .down("form")
                  .getForm()
                  .setValues(row.data);
                if (category == "ANALISTASI") {
                  windows
                    .down("#criterionSafety")
                    .setValue(record.get("criterionSafety").split(","));
                }
                isConfidential(windows, record);
                windows.show();
              }
            });
        }
        windows.title = windows.title + "-" + record.get("descriptionShort");
      }
    }
  }
);
