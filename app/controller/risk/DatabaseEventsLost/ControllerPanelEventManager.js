Ext.define(
  "DukeSource.controller.risk.DatabaseEventsLost.ControllerPanelEventManager",
  {
    alias: "ControllerPanelEventManager",
    extend: "Ext.app.Controller",
    stores: [],
    models: [],
    views: [],
    init: function() {
      this.control({
        "ViewPanelEventsManager grid": {
          itemcontextmenu: this.rightClickEventManager
        }
      });
    },

    _onLoadEventsRiskManager: function(record) {
      var win = Ext.create(
        "DukeSource.view.risk.DatabaseEventsLost.windows.WindowRegisterEventsManager",
        {
          actionType: "modify",
          modal: true
        }
      );
      DukeSource.lib.Ajax.request({
        method: "POST",
        url: "http://localhost:9000/giro/getEventMasterByIdEvent.htm",
        params: {
          idEvent: record.get("idEvent")
        },
        success: function(response) {
          response = Ext.decode(response.responseText);
          if (response.success) {
            win.down("#numberRelation").setValue(record.get("idEvent"));
            win
              .down("#currency")
              .getStore()
              .load();

            if (record.get("eventState") !== "") {
              win.down("#saveEventOperational").setVisible(false);
            }

            win.down("#codeEventTemp").setValue(record.get("codeEvent"));
            win.down("#dateRegisterTemp").setValue(record.get("dateRegister"));
            win
              .down("#fullNameUserRegisterTemp")
              .setValue(record.get("fullNameUserRegister"));
            win
              .down("#workAreaUserRegisterTemp")
              .setValue(record.get("workAreaUserRegister"));
            win.show();
            win
              .down("form")
              .getForm()
              .setValues(response.data);

            win.title = record.get("descriptionShort");
          } else {
            DukeSource.global.DirtyView.messageWarning(response.message);
          }
        },
        failure: function() {}
      });
    },

    rightClickEventManager: function(view, rec, node, index, e) {
      e.stopEvent();
      var addMenu = Ext.create(
        "DukeSource.view.risk.DatabaseEventsLost.AddMenuEvent",
        {}
      );
      addMenu.removeAll();
      addMenu.add({
        text: "Gestionar Adjuntos",
        iconCls: "gestionfile",
        handler: function() {
          var window = Ext.create(
            "DukeSource.view.risk.DatabaseEventsLost.windows.ViewWindowEventsFileAttach",
            {
              idEvent: rec.get("idEvent"),
              modal: true,
              buttonsDisabled:
                rec.get("sequenceEventState") !== 1 &&
                codexCompany === "es_cl_0001"
            }
          );
          window.title =
            rec.get("codeEvent") +
            " " +
            window.title +
            "-" +
            rec.get("descriptionShort");
          window.show();
        }
      });
      addMenu.showAt(e.getXY());
    }
  }
);
