Ext.define('Ext.ux.grid.GridPicker-4-2-0', {
    override: 'Ext.ux.grid.GridPicker'
    , getLoadOptions: function (queryString) {
        var filter = this.queryFilter;
        if (filter) {
            filter.disabled = false;
            filter.setValue(this.enableRegEx ? new RegExp(queryString) : queryString);
            return {
                filters: [filter]
            };
        }
    }

    , loadPage: function (pageNum) {
        this.store.loadPage(pageNum, this.getLoadOptions());
    }
    , doQuery: function (queryString, forceAll, rawQuery) {
        queryString = queryString || '';
        var me = this,
            qe = {
                query: queryString,
                forceAll: forceAll,
                combo: me,
                cancel: false
            },
            store = me.store,
            isLocalMode = me.queryMode === 'local';

        if (me.fireEvent('beforequery', qe) === false || qe.cancel) {
            return false;
        }
        queryString = qe.query;
        forceAll = qe.forceAll;

        if (forceAll || (queryString.length >= me.minChars)) {
            me.expand();
            if (!me.queryCaching || me.lastQuery !== queryString) {
                me.lastQuery = queryString;
                if (isLocalMode) {
                    if (me.queryFilter) {
                        if (queryString || !forceAll) {
                            me.queryFilter.disabled = false;
                            me.queryFilter.setValue(me.enableRegEx ? new RegExp(queryString) : queryString);
                        }
                        else {
                            me.queryFilter.disabled = true;
                        }
                        store.filter();
                    }
                } else {
                    me.rawQuery = rawQuery;
                    if (me.pageSize) {
                        me.loadPage(1);
                    } else {
                        store.load(this.getLoadOptions(queryString));
                    }
                }
            }
            if (me.getRawValue() !== me.getDisplayValue()) {
                me.ignoreSelection++;
                me.picker.getSelectionModel().deselectAll();
                me.ignoreSelection--;
            }
            if (isLocalMode) {
                me.doAutoSelect();
            }
            if (me.typeAhead) {
                me.doTypeAhead();
            }
        }
        return true;
    }
});
