/*

 Ext Gantt 2.5.5
 Copyright(c) 2009-2015 Bryntum AB
 http://bryntum.com/contact
 http://bryntum.com/license

 */
/**
 @class Gnt.data.CrudManager
 @extends Sch.data.CrudManager

 Class implementing  central collection of all project stores.
 It allows to load all of them by a single server request and persist all of their changes by one request as well.
 This class uses AJAX as a transport mechanism and JSON as a data encoding format.

 # Gantt stores

 The class supports gantt specific stores (namely: resource, assignment, dependency and task stores).
 For these stores the class has separate configs ({@link #resourceStore}, {@link #assignmentStore}, {@link #dependencyStore}, {@link #taskStore})
 to register them.

 var taskStore = Ext.create('Gnt.data.TaskStore', {
        calendarManager : calendarManager,
        resourceStore   : resourceStore,
        dependencyStore : dependencyStore,
        assignmentStore : assignmentStore
    });

 var crudManager = Ext.create('Gnt.data.CrudManager', {
        autoLoad        : true,
        // We specify TaskStore only. The rest stores will be taken from it.
        taskStore       : taskStore,
        transport       : {
            load    : {
                url     : 'php/read.php'
            },
            sync    : {
                url     : 'php/save.php'
            }
        }
    });

 # AJAX request configuration

 To configure AJAX request parameters please take a look at the {@link #transport} config.

 var crudManager = Ext.create('Sch.data.CrudManager', {
        autoLoad        : true,
        taskStore       : taskStore,
        transport       : {
            load    : {
                url         : 'php/read.php',
                // use GET request
                method      : 'GET',
                // pass request JSON in "rq" parameter
                paramName   : 'rq',
                // extra HTTP request parameters
                params      : {
                    foo     : 'bar'
                }
            },
            sync    : {
                url     : 'php/save.php'
            }
        }
    });


 # Loading order

 The class is aware of the proper loading order for gantt specific stores so you don't need to worry about it.
 And if you provide any extra stores (using {@link #stores} config) on a construction step
 they will get to the start of collection before the gantt specific stores.
 So if you need to change that loading order you should use {@link #addStore} method to register your store:

 var crudManager = Ext.create('Gnt.data.CrudManager', {
        // these stores will be loaded before gantt specific stores
        stores          : [ store1, store2 ],
        taskStore       : taskStore,
        transport       : {
            load    : {
                url     : 'php/read.php'
            },
            sync    : {
                url     : 'php/save.php'
            }
        }
    });

 // append store3 to the end so it will be loaded last
 crudManager.addStore(store3);

 // now when we registered all the stores let's load them
 crudManager.load();

 # Calendars

 Also the class supports a bulk loading of the project calendars.
 To do it the {@link #calendarManager} config has to be specified or it can be specified on the {@link Gnt.data.TaskStore#calendarManager task store}.

 var calendarManager   = Ext.create('Gnt.data.CalendarManager', {
        calendarClass   : 'Gnt.data.calendar.BusinessTime'
    });

 ...

 var taskStore     = Ext.create('MyTaskStore', {
        // taskStore calendar will automatically be set when calendarManager gets loaded
        calendarManager : calendarManager,
        resourceStore   : resourceStore,
        dependencyStore : dependencyStore,
        assignmentStore : assignmentStore
    });

 var crudManager   = Ext.create('Gnt.data.CrudManager', {
        autoLoad        : true,
        taskStore       : taskStore,
        transport       : {
            load    : {
                url     : 'php/read.php'
            },
            sync    : {
                url     : 'php/save.php'
            }
        }
    });


 */
Ext.define('Gnt.data.CrudManager', {
    extend: 'Sch.data.CrudManager',

    /**
     * @cfg {Gnt.data.CalendarManager/Object} calendarManager A calendar manager instance or its descriptor.
     */
    calendarManager: null,
    /**
     * @cfg {Gnt.data.TaskStore/Object} taskStore A store with tasks or its descriptor.
     */
    taskStore: null,
    /**
     * @cfg {Gnt.data.DependencyStore/Object} dependencyStore A store with dependencies or its descriptor.
     */
    dependencyStore: null,
    /**
     * @cfg {Gnt.data.ResourceStore/Object} resourceStore A store with resources or its descriptor.
     */
    resourceStore: null,
    /**
     * @cfg {Gnt.data.AssignmentStore/Object} assignmentStore A store with assignments or its descriptor.
     */
    assignmentStore: null,

    constructor: function (config) {
        config = config || {};

        var calendarManager = config.calendarManager,
            taskStore = config.taskStore,
            assignmentStore = config.assignmentStore,
            resourceStore = config.resourceStore,
            dependencyStore = config.dependencyStore,
        // list of stores to add
            stores = [];

        // retrieve stores registered on the provided taskStore
        if (taskStore) {
            var extracted = this.getTaskStoreInfo(taskStore, config);

            calendarManager = calendarManager || extracted.calendarManager;
            assignmentStore = assignmentStore || extracted.assignmentStore;
            resourceStore = resourceStore || extracted.resourceStore;
            dependencyStore = dependencyStore || extracted.dependencyStore;
        }


        // calendars go first in the stores loading order
        if (calendarManager) {
            // Call this early manually to be able to add listeners before calling the superclass constructor
            this.mixins.observable.constructor.call(this);

            this.addCalendarManager(calendarManager, stores);
        }

        // ..then resources, assignments, dependencies and finally tasks
        if (resourceStore) {
            // to not interfere w/ the Sch.data.CrudManager which also has "resourceStore" config
            delete config.resourceStore;
            stores.push(resourceStore);
        }
        if (assignmentStore) stores.push(assignmentStore);
        if (dependencyStore) stores.push(dependencyStore);
        if (taskStore) stores.push(taskStore);

        if (stores.length) {
            var syncSequence = [];

            // For applying sync results we have a different order:
            // calendars -> resources -> tasks -> assignments -> dependencies
            if (this.calendarManager) syncSequence.push(calendarManager);
            if (resourceStore) syncSequence.push(resourceStore);
            if (taskStore) syncSequence.push(taskStore);
            if (assignmentStore) syncSequence.push(assignmentStore);
            if (dependencyStore) syncSequence.push(dependencyStore);

            if (syncSequence.length) {
                config.syncApplySequence = (config.syncApplySequence || config.stores || []).concat(syncSequence);
            }

            // all the Gantt related stores will go after the user defined stores (specified in config.stores)
            config.stores = (config.stores || []).concat(stores);
        }


        this.callParent([config]);

        // make sure we have properties set to proper stores descriptors
        this.resourceStore = this.getStore(resourceStore);
        this.assignmentStore = this.getStore(assignmentStore);
        this.dependencyStore = this.getStore(dependencyStore);
        this.taskStore = this.getStore(taskStore);
    },


    getTaskStoreInfo: function (taskStore, config) {
        taskStore = taskStore instanceof Ext.data.AbstractStore ? taskStore : taskStore.store;

        var result = {},
            calendarManager = config.calendarManager,
            assignmentStore = config.assignmentStore,
            resourceStore = config.resourceStore,
            dependencyStore = config.dependencyStore;

        if (!calendarManager) result.calendarManager = taskStore.calendarManager;
        if (!assignmentStore) result.assignmentStore = taskStore.getAssignmentStore();
        if (!resourceStore) result.resourceStore = taskStore.getResourceStore();
        if (!dependencyStore) result.dependencyStore = taskStore.getDependencyStore();

        return result;
    },


    addCalendarManager: function (calendarManager, stores) {
        var store, descriptor;

        if (calendarManager instanceof Ext.data.AbstractStore) {
            store = calendarManager;
            descriptor = {store: calendarManager};
        } else {
            store = calendarManager.store;
            descriptor = calendarManager;
        }

        var model = (store.getModel && store.getModel() || store.model).prototype;

        // register calendar manager sub-stores being kept in "Days" field
        if (!descriptor.stores) {
            descriptor.stores = [{
                storeId: model.daysField,
                idProperty: model.idProperty
            }];
        }

        this.calendarManager = descriptor;

        // on calendar manager data get loaded we gonna set the project calendar
        store.on('load', this.onCalendarManagerLoad, this);

        // let's ignore calendars events during data loading since we don't want tasks to get moved after stores loading
        this.on({
            beforeloadapply: this.onBeforeLoadApply,
            load: this.onCrudLoad,
            scope: this
        });

        stores.push(descriptor);
    },


    onCalendarManagerLoad: function (store) {
        var projectCalendar = store.getProjectCalendar(),
            oldCalendarId = projectCalendar && projectCalendar.getCalendarId(),
            newCalendarId = store.metaData && store.metaData.projectCalendar;

        // if project calendar has changed
        if (oldCalendarId != newCalendarId) {
            store.setProjectCalendar(newCalendarId);
        }
    },


    onBeforeLoadApply: function () {
        var cm = this.getCalendarManager();
        cm && cm.suspendCalendarsEvents();
    },


    onCrudLoad: function () {
        var cm = this.getCalendarManager();
        cm && cm.resumeCalendarsEvents();
    },


    /**
     * Returns the calendar manager bound to the crud manager.
     * @return {Gnt.data.CalendarManager} The calendar manager bound to the crud manager.
     */
    getCalendarManager: function () {
        return this.calendarManager && this.calendarManager.store;
    },

    /**
     * Returns the resource store bound to the crud manager.
     * @return {Gnt.data.ResourceStore} The resource store bound to the crud manager.
     */
    getResourceStore: function () {
        return this.resourceStore && this.resourceStore.store;
    },

    /**
     * Returns the dependency store bound to the crud manager.
     * @return {Gnt.data.DependencyStore} The dependency store bound to the crud manager.
     */
    getDependencyStore: function () {
        return this.dependencyStore && this.dependencyStore.store;
    },

    /**
     * Returns the assignment store bound to the crud manager.
     * @return {Gnt.data.AssignmentStore} The assignment store bound to the crud manager.
     */
    getAssignmentStore: function () {
        return this.assignmentStore && this.assignmentStore.store;
    },

    /**
     * Returns the task store bound to the crud manager.
     * @return {Gnt.data.TaskStore} The task store bound to the crud manager.
     */
    getTaskStore: function () {
        return this.taskStore && this.taskStore.store;
    },

    prepareUpdated: function (list, stores) {
        if (list[0] instanceof Gnt.model.Task) {
            // Root should not be updated since the gantt doesn't modify this (though Ext JS might)
            list = Ext.Array.filter(list, function (node) {
                return !node.isRoot();
            });
        }

        return this.callParent([list, stores]);
    },

    applyChangesToTask: function (record, changes) {
        // apply changes to segments
        if (changes.hasOwnProperty(record.segmentsField)) {

            var segments = record.getSegments(),
                segmentsField = record.segmentsField,
                phantomIdField = segments && segments[0].phantomIdField,
                idProperty = segments && segments[0].idProperty,
                segmentsChanges = changes[segmentsField];

            // loop over transferred segments if any
            if (segmentsChanges && segmentsChanges.length) {

                for (var i = segmentsChanges.length - 1; i >= 0; i--) {
                    // get transferred segment change
                    var segmentChange = segmentsChanges[i],
                        phantomId = segmentChange[phantomIdField],
                        id = segmentChange[idProperty],
                        segment = null;

                    // let's find corresponding segment to update
                    for (var j = 0; j < segments.length; j++) {
                        segment = segments[j];

                        // we detect it using either phantom or real id
                        if ((segment.get(phantomIdField) == phantomId) || (segment.getId() == id)) {
                            // let's apply transferred changes to found segment
                            this.applyChangesToRecord(segment, segmentChange);
                            break;
                        }
                    }
                }

                // need to get rid of "Segments" field since we already loaded segments changes
                // (otherwise the task will do a simple setSegments() call)
                delete changes[segmentsField];
            }
        }
    },

    applyChangesToRecord: function (record, changes, stores) {
        // if we deal with a task let's call special applyChangesToTask method before
        // it will apply changes to the task segments (if they passed)
        if (record instanceof Gnt.model.Task) {
            this.ignoreUpdates++;

            this.applyChangesToTask.apply(this, arguments);

            this.ignoreUpdates--;
        }

        this.callParent(arguments);
    }
});
