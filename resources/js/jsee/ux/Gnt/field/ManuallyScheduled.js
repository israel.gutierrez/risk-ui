/*

 Ext Gantt 2.5.5
 Copyright(c) 2009-2015 Bryntum AB
 http://bryntum.com/contact
 http://bryntum.com/license

 */
/**

 A specialized field, allowing a user to also specify task manually scheduled value.
 This class inherits from the standard Ext JS "checkbox" field, so any usual `Ext.form.field.Checkbox` configs can be used.

 @class Gnt.field.ManuallyScheduled
 @extends Ext.form.field.Checkbox

 */
Ext.define('Gnt.field.ManuallyScheduled', {
    extend: 'Ext.form.field.Checkbox',

    mixins: ['Gnt.field.mixin.TaskField', 'Gnt.mixin.Localizable'],

    alias: 'widget.manuallyscheduledfield',

    alternateClassName: ['Gnt.column.manuallyscheduled.Field'],

    taskField: 'manuallyScheduledField',

    constructor: function (config) {
        var me = this;

        Ext.apply(this, config);

        this.setSuppressTaskUpdate(true);
        this.callParent(arguments);
        this.setSuppressTaskUpdate(false);

        if (this.task) this.setTask(this.task);
    },

    destroy: function () {
        this.destroyTaskListener();

        this.callParent();
    },

    onSetTask: function () {
        this.setValue(this.task.isManuallyScheduled());
    },

    valueToVisible: function (value) {
        return value ? this.L('yes') : this.L('no');
    },

    /**
     * This method applies the changes from the field to the bound task or to the task provided as 1st argument.
     * If {@link #instantUpdate} option is enabled this method is called automatically after any change in the field.
     *
     * @param {Gnt.model.Task} [toTask] The task to apply the changes to. If not provided, changes will be applied to the last bound task
     * (with {@link #task} config option or {@link #setTask) method)
     */
    applyChanges: function (toTask) {
        toTask = toTask || this.task;

        toTask.setManuallyScheduled(this.getValue());
    },

    getValue: function () {
        return this.value;
    },

    setValue: function (value) {

        this.callParent([value]);

        if (this.instantUpdate && !this.getSuppressTaskUpdate() && this.task) {
            // apply changes to task
            this.applyChanges();
            this.task.fireEvent('taskupdated', this.task, this);
        }
    }
});
