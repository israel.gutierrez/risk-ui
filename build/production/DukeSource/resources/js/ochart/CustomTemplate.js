Ext.define('Ext.ux.ochart.CustomTemplate', {
    extend: 'Ext.ux.ochart.Simple',
    requires: 'DukeSource.model.risk.parameter.grids.ModelTreeGridPanelRegisterWorkArea',
    alias: 'widget.ochartcustom',

    initComponent: function () {
        var me = this;

        me.store = Ext.create('DukeSource.store.risk.parameter.grids.StoreTreeGridPanelRegisterWorkArea');

        me.chartConfig = me.chartConfig || {};
        Ext.applyIf(me.chartConfig, {
            itemTpl: [
                '<div class="item-title">{description}</div>',
                '<div class="item-body">',
                '<div class="item-label">',
                'Parent:<br/>',
                'state:<br/>',
                '</div>',
                '<div class="item-value">',
                '{parent}<br/>',
                '{state}<br/>',
                '</div>',
                '</div>'
            ],

            itemCls: 'task-item'
        });

        me.callParent(arguments);
    },

    onItemDblClick: Ext.emptyFn
});
