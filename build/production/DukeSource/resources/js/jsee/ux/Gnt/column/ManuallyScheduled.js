/*

 Ext Gantt 2.5.5
 Copyright(c) 2009-2015 Bryntum AB
 http://bryntum.com/contact
 http://bryntum.com/license

 */
/**

 @class Gnt.column.ManuallyScheduled
 @extends Ext.grid.column.Column

 A Column showing the `Manually Scheduled` field of a task.
 */

Ext.define("Gnt.column.ManuallyScheduled", {
    extend: "Ext.grid.Column",
    alias: "widget.manuallyscheduledcolumn",

    requires: ['Gnt.field.ManuallyScheduled'],

    mixins: ['Gnt.mixin.Localizable'],

    width: 50,
    align: 'center',

    constructor: function (config) {
        config = config || {};

        config.editor = config.editor || new Gnt.field.ManuallyScheduled({
                instantUpdate: false
            });

        this.text = config.text || this.L('text');

        this.field = config.editor;

        this.callParent(arguments);

        this.scope = this;
    },

    renderer: function (value, meta, task) {
        return this.field.valueToVisible(task.isManuallyScheduled());
    }

});
